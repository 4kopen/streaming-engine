
/*
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301,
 * USA.
 */

#include "basetype.h"
#include "dwl_activity_trace.h"
#include "dwl.h"

u32 ActivityTraceInit(struct ActivityTrace* inst) {
#ifdef _DWL_ENABLE_ACTIVITY_TRACE
  if (inst == NULL) return 1;

  DWLmemset(inst, 0, sizeof(struct ActivityTrace));
#endif
  return 0;
}

u32 ActivityTraceStartDec(struct ActivityTrace* inst) {
#ifdef _DWL_ENABLE_ACTIVITY_TRACE
  if (inst == NULL) return 1;
  do_gettimeofday(&inst->start); /* SE_UPDATE - replace gettimeofday() call */

  if (inst->stop.tv_usec + inst->stop.tv_sec) {
    unsigned long idle = 1000000 * inst->start.tv_sec + inst->start.tv_usec -
                         1000000 * inst->stop.tv_sec - inst->stop.tv_usec;
    inst->idle_time += idle / 10;
  }
#endif
  return 0;
}

u32 ActivityTraceStopDec(struct ActivityTrace* inst) {

#ifdef _DWL_ENABLE_ACTIVITY_TRACE
  unsigned long active;
  if (inst == NULL) return 1;

  do_gettimeofday(&inst->stop); /* SE_UPDATE - replace gettimeofday() call */

  active = 1000000 * inst->stop.tv_sec + inst->stop.tv_usec -
           1000000 * inst->start.tv_sec - inst->start.tv_usec;
  inst->active_time += active / 10;
#endif
  return 0;
}

u32 ActivityTraceRelease(struct ActivityTrace* inst) {
#ifdef _DWL_ENABLE_ACTIVITY_TRACE
  if (inst == NULL) return 1;

  if (inst->active_time || inst->idle_time) {
    printf("\n_hardware active/idle statistics:\n");
    printf("Active: %9lu msec\n", inst->active_time / 100);
    printf("Idle: %11lu msec\n", inst->idle_time / 100);
    if (inst->active_time + inst->idle_time)
      printf("Hardware utilization: %lu%\n",
             inst->active_time / ((inst->active_time + inst->idle_time) / 100));
  }
#endif
  return 0;
}
