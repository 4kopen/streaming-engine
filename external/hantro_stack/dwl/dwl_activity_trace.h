
/*
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301,
 * USA.
 */

#ifndef __DWL_ACTIVITY_TRACE_H__
#define __DWL_ACTIVITY_TRACE_H__

#include <linux/time.h>
#include "basetype.h"

struct ActivityTrace
{
    struct timeval start;
    struct timeval stop;
    unsigned long active_time;
    unsigned long idle_time;
};

u32 ActivityTraceInit(struct ActivityTrace *inst);
u32 ActivityTraceRelease(struct ActivityTrace *inst);
u32 ActivityTraceStartDec(struct ActivityTrace *inst);
u32 ActivityTraceStopDec(struct ActivityTrace *inst);

#endif /* __DWL_ACTIVITY_TRACE_H__ */
