
/*
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301,
 * USA.
 */
/************************************************************************
Copyright (C) 2003-2014 STMicroelectronics. All Rights Reserved.
************************************************************************/


/* The implementation is platform dependent and API implementation is proprietary */

#include <linux/slab.h>
#include <linux/dma-mapping.h>

#include "../stack/inc/dwl.h"
#include "vp9inline.h"
#include "dwl_activity_trace.h"
#include "codec_mme_video_vp9_ext.h"
#include "dwl_defs.h"
#include "report.h"

#undef TRACE_TAG
#define TRACE_TAG "dwl_linux_Vp9_c"

DECLARE_WAIT_QUEUE_HEAD(dec_wait_queue);

static int n_dwl_instance_count;
static struct vp9_config_regs vp9_config;
/* a mutex protecting the wrapper init */
DEFINE_MUTEX(x170_init_mutex);


static i32 DWLMallocLinearNamed(const void *instance, u32 size, struct DWLLinearMem *info, const char *name);

void DWLRegisterCodecPtr(const void *instance, void *codecPtr)
{
    struct HX170DWL *dec_dwl = (struct HX170DWL *)instance;
    BUG_ON(dec_dwl == NULL);
    dec_dwl->codecptr = codecPtr;
}

void DWLGetDecodeBufferPtr(const void *instance, unsigned int index, void **ptr)
{
    struct HX170DWL *dec_dwl = (struct HX170DWL *)instance;
    BUG_ON(dec_dwl == NULL);
    *ptr = dec_dwl->decodebufferptr[index];
}


unsigned int DWLReadAsicCoreCount(void)
{
    unsigned int cores = 0, version = 0;
    unsigned int asicid;
    BUG_ON(!vp9_config.asic_id);

    asicid = vp9_config.asic_id;

    /* Get HW version */
    version = ((asicid & 0xFFFF0000) >> 16);
    if (version != 0x6732)
    {
        SE_ERROR("error : hw version %d, expected 0x6732 for G2\n", version);
    }
    else
    {
        cores = MAX_ASIC_CORES;
    }
    return (unsigned int)cores;
}


u32 DWLReadAsicID(void)
{
    unsigned int asicid;
    Vp9GetConfiguration(&vp9_config);
    asicid = vp9_config.asic_id;
    return asicid;
}

static void ReadCoreFuse(struct DWLHwFuseStatus *hw_fuse_sts)
{
    u32 config_reg, fuse_reg, fuse_reg_pp;

    /* Decoder fuse configuration */
    fuse_reg = vp9_config.dec_fuse;

    hw_fuse_sts->vp6_support_fuse = (fuse_reg >> DWL_VP6_FUSE_E) & 0x01U;
    hw_fuse_sts->vp7_support_fuse = (fuse_reg >> DWL_VP7_FUSE_E) & 0x01U;
    hw_fuse_sts->vp8_support_fuse = (fuse_reg >> DWL_VP8_FUSE_E) & 0x01U;
    hw_fuse_sts->vp9_support_fuse = (fuse_reg >> DWL_VP9_FUSE_E) & 0x01U;
    hw_fuse_sts->h264_support_fuse = (fuse_reg >> DWL_H264_FUSE_E) & 0x01U;
    hw_fuse_sts->hevc_support_fuse = (fuse_reg >> DWL_HEVC_FUSE_E) & 0x01U;
    hw_fuse_sts->mpeg4_support_fuse = (fuse_reg >> DWL_MPEG4_FUSE_E) & 0x01U;
    hw_fuse_sts->mpeg2_support_fuse = (fuse_reg >> DWL_MPEG2_FUSE_E) & 0x01U;
    hw_fuse_sts->sorenson_spark_support_fuse =
        (fuse_reg >> DWL_SORENSONSPARK_FUSE_E) & 0x01U;
    hw_fuse_sts->jpeg_support_fuse = (fuse_reg >> DWL_JPEG_FUSE_E) & 0x01U;
    hw_fuse_sts->vc1_support_fuse = (fuse_reg >> DWL_VC1_FUSE_E) & 0x01U;
    hw_fuse_sts->jpeg_prog_support_fuse = (fuse_reg >> DWL_PJPEG_FUSE_E) & 0x01U;
    hw_fuse_sts->rv_support_fuse = (fuse_reg >> DWL_RV_FUSE_E) & 0x01U;
    hw_fuse_sts->avs_support_fuse = (fuse_reg >> DWL_AVS_FUSE_E) & 0x01U;
    hw_fuse_sts->custom_mpeg4_support_fuse =
        (fuse_reg >> DWL_CUSTOM_MPEG4_FUSE_E) & 0x01U;
    hw_fuse_sts->mvc_support_fuse = (fuse_reg >> DWL_MVC_FUSE_E) & 0x01U;

    /* check max. decoder output width */
    /* Recommended by Soc Validation team inorder to be aligned with correct
    behavior on Soc - if one resolution fuse is blown, all higher resolution fuses
    must also be blown */
    hw_fuse_sts->max_dec_pic_width_fuse = 176;

    if (fuse_reg & 0x1000U)
    {
        hw_fuse_sts->max_dec_pic_width_fuse = 352;
    }
    if (fuse_reg & 0x2000U)
    {
        hw_fuse_sts->max_dec_pic_width_fuse = 720;
    }
    if (fuse_reg & 0x4000U)
    {
        hw_fuse_sts->max_dec_pic_width_fuse = 1280;
    }
    if (fuse_reg & 0x8000U)
    {
        hw_fuse_sts->max_dec_pic_width_fuse = 1920;
    }
    if (fuse_reg & 0x10000U)
    {
        hw_fuse_sts->max_dec_pic_width_fuse = 4096;
    }

    hw_fuse_sts->ref_buf_support_fuse = (fuse_reg >> DWL_REF_BUFF_FUSE_E) & 0x01U;

    /* Pp configuration */
    config_reg = vp9_config.pp_cfg;

    if ((config_reg >> DWL_PP_E) & 0x01U)
    {
        /* Pp fuse configuration */
        fuse_reg_pp = vp9_config.pp_fuse;

        if ((fuse_reg_pp >> DWL_PP_FUSE_E) & 0x01U)
        {
            hw_fuse_sts->pp_support_fuse = 1;

            /* check max. pp output width */
            if (fuse_reg_pp & 0x10000U)
            {
                hw_fuse_sts->max_pp_out_pic_width_fuse = 4096;
            }
            else if (fuse_reg_pp & 0x8000U)
            {
                hw_fuse_sts->max_pp_out_pic_width_fuse = 1920;
            }
            else if (fuse_reg_pp & 0x4000U)
            {
                hw_fuse_sts->max_pp_out_pic_width_fuse = 1280;
            }
            else if (fuse_reg_pp & 0x2000U)
            {
                hw_fuse_sts->max_pp_out_pic_width_fuse = 720;
            }
            else if (fuse_reg_pp & 0x1000U)
            {
                hw_fuse_sts->max_pp_out_pic_width_fuse = 352;
            }

            hw_fuse_sts->pp_config_fuse = fuse_reg_pp;
        }
        else
        {
            hw_fuse_sts->pp_support_fuse = 0;
            hw_fuse_sts->max_pp_out_pic_width_fuse = 0;
            hw_fuse_sts->pp_config_fuse = 0;
        }
    }
}

static void ReadCoreConfig(DWLHwConfig *hw_cfg)
{
    u32 config_reg;
    u32 asic_id = 0;
    struct DWLHwFuseStatus hw_fuse_sts;

    BUG_ON(!vp9_config.asic_id);
    asic_id = vp9_config.asic_id;

    /* Decoder configuration */
    config_reg = vp9_config.dec_cfg;

    hw_cfg->h264_support = (config_reg >> DWL_H264_E) & 0x3U;
    /* check jpeg */
    hw_cfg->jpeg_support = (config_reg >> DWL_JPEG_E) & 0x01U;
    if (hw_cfg->jpeg_support && ((config_reg >> DWL_PJPEG_E) & 0x01U))
    {
        hw_cfg->jpeg_support = JPEG_PROGRESSIVE;
    }
    hw_cfg->mpeg4_support = (config_reg >> DWL_MPEG4_E) & 0x3U;
    hw_cfg->vc1_support = (config_reg >> DWL_VC1_E) & 0x3U;
    hw_cfg->mpeg2_support = (config_reg >> DWL_MPEG2_E) & 0x01U;
    hw_cfg->sorenson_spark_support = (config_reg >> DWL_SORENSONSPARK_E) & 0x01U;
#ifndef DWL_REFBUFFER_DISABLE
    hw_cfg->ref_buf_support = (config_reg >> DWL_REF_BUFF_E) & 0x01U;
#else
    hw_cfg->ref_buf_support = 0;
#endif
    hw_cfg->vp6_support = (config_reg >> DWL_VP6_E) & 0x01U;
#ifdef DEC_X170_APF_DISABLE
    if (DEC_X170_APF_DISABLE)
    {
        hw_cfg->tiled_mode_support = 0;
    }
#endif /* DEC_X170_APF_DISABLE */

    hw_cfg->max_dec_pic_width = config_reg & 0x07FFU;

    /* 2nd Config register */
    config_reg = vp9_config.dec_cfg2;
    if (hw_cfg->ref_buf_support)
    {
        if ((config_reg >> DWL_REF_BUFF_ILACE_E) & 0x01U)
        {
            hw_cfg->ref_buf_support |= 2;
        }
        if ((config_reg >> DWL_REF_BUFF_DOUBLE_E) & 0x01U)
        {
            hw_cfg->ref_buf_support |= 4;
        }
    }
    hw_cfg->vp9_support = (config_reg >> DWL_VP9_E) & 0x3U;
    hw_cfg->hevc_support = (config_reg >> DWL_HEVC_E) & 0x3U;
    hw_cfg->custom_mpeg4_support = (config_reg >> DWL_MPEG4_CUSTOM_E) & 0x01U;
    hw_cfg->vp7_support = (config_reg >> DWL_VP7_E) & 0x01U;
    hw_cfg->vp8_support = (config_reg >> DWL_VP8_E) & 0x01U;
    hw_cfg->avs_support = (config_reg >> DWL_AVS_E) & 0x01U;

    /* JPEG extensions */
    if (((asic_id >> 16) >= 0x8190U) || ((asic_id >> 16) == 0x6731U))
    {
        hw_cfg->jpeg_esupport = (config_reg >> DWL_JPEG_EXT_E) & 0x01U;
    }
    else
    {
        hw_cfg->jpeg_esupport = JPEG_EXT_NOT_SUPPORTED;
    }

    if (((asic_id >> 16) >= 0x9170U) || ((asic_id >> 16) == 0x6731U))
    {
        hw_cfg->rv_support = (config_reg >> DWL_RV_E) & 0x03U;
    }
    else
    {
        hw_cfg->rv_support = RV_NOT_SUPPORTED;
    }

    hw_cfg->mvc_support = (config_reg >> DWL_MVC_E) & 0x03U;
    hw_cfg->webp_support = (config_reg >> DWL_WEBP_E) & 0x01U;
    hw_cfg->tiled_mode_support = (config_reg >> DWL_DEC_TILED_L) & 0x03U;
    hw_cfg->max_dec_pic_width += ((config_reg >> DWL_DEC_PIC_W_EXT) & 0x03U)
                                 << 11;

    hw_cfg->ec_support = (config_reg >> DWL_EC_E) & 0x03U;
    hw_cfg->stride_support = (config_reg >> DWL_STRIDE_E) & 0x01U;
    hw_cfg->field_dpb_support = (config_reg >> DWL_FIELD_DPB_E) & 0x01U;

    if (hw_cfg->ref_buf_support && ((asic_id >> 16) == 0x6731U))
    {
        hw_cfg->ref_buf_support |= 8; /* enable HW support for offset */
    }

    hw_cfg->double_buffer_support = (config_reg >> DWL_DOUBLEBUFFER_E) & 0x01U;

    /* 3rd Config register */
    config_reg = vp9_config.dec_cfg3;
    hw_cfg->max_dec_pic_height = config_reg & 0x0FFFU;

    /* Pp configuration */
    config_reg = vp9_config.pp_cfg;

    if ((config_reg >> DWL_PP_E) & 0x01U)
    {
        hw_cfg->pp_support = 1;
        /* Theoretical max range 0...8191; actual 48...4096 */
        hw_cfg->max_pp_out_pic_width = config_reg & 0x1FFFU;
        /*hw_cfg->pp_config = (config_reg >> DWL_CFG_E) & 0x0FU; */
        hw_cfg->pp_config = config_reg;
    }
    else
    {
        hw_cfg->pp_support = 0;
        hw_cfg->max_pp_out_pic_width = 0;
        hw_cfg->pp_config = 0;
    }

    /* check fuse status */
    memset((void *)&hw_fuse_sts, 0, sizeof(hw_fuse_sts));
    ReadCoreFuse(&hw_fuse_sts);
    if (!hw_fuse_sts.vp9_support_fuse)
    {
        hw_cfg->vp9_support = 0;
    }

    if (((asic_id >> 16) >= 0x8190U) || ((asic_id >> 16) == 0x6731U))
    {
        u32 de_interlace;
        u32 alpha_blend;
        u32 de_interlace_fuse;
        u32 alpha_blend_fuse;

        /* Maximum output width of Post-Processor */
        if (hw_cfg->max_pp_out_pic_width > hw_fuse_sts.max_pp_out_pic_width_fuse)
        {
            hw_cfg->max_pp_out_pic_width = hw_fuse_sts.max_pp_out_pic_width_fuse;
        }
        /* h264 */
        if (!hw_fuse_sts.h264_support_fuse)
        {
            hw_cfg->h264_support = H264_NOT_SUPPORTED;
        }
        /* mpeg-4 */
        if (!hw_fuse_sts.mpeg4_support_fuse)
        {
            hw_cfg->mpeg4_support = MPEG4_NOT_SUPPORTED;
        }
        /* custom mpeg-4 */
        if (!hw_fuse_sts.custom_mpeg4_support_fuse)
        {
            hw_cfg->custom_mpeg4_support = MPEG4_CUSTOM_NOT_SUPPORTED;
        }
        /* jpeg (baseline && progressive) */
        if (!hw_fuse_sts.jpeg_support_fuse)
        {
            hw_cfg->jpeg_support = JPEG_NOT_SUPPORTED;
        }
        if ((hw_cfg->jpeg_support == JPEG_PROGRESSIVE) &&
            !hw_fuse_sts.jpeg_prog_support_fuse)
        {
            hw_cfg->jpeg_support = JPEG_BASELINE;
        }
        /* mpeg-2 */
        if (!hw_fuse_sts.mpeg2_support_fuse)
        {
            hw_cfg->mpeg2_support = MPEG2_NOT_SUPPORTED;
        }
        /* vc-1 */
        if (!hw_fuse_sts.vc1_support_fuse) { hw_cfg->vc1_support = VC1_NOT_SUPPORTED; }
        /* vp6 */
        if (!hw_fuse_sts.vp6_support_fuse) { hw_cfg->vp6_support = VP6_NOT_SUPPORTED; }
        /* vp7 */
        if (!hw_fuse_sts.vp7_support_fuse) { hw_cfg->vp7_support = VP7_NOT_SUPPORTED; }
        /* vp8 */
        if (!hw_fuse_sts.vp8_support_fuse) { hw_cfg->vp8_support = VP8_NOT_SUPPORTED; }
        /* webp */
        if (!hw_fuse_sts.vp8_support_fuse)
        {
            hw_cfg->webp_support = WEBP_NOT_SUPPORTED;
        }
        /* pp */
        if (!hw_fuse_sts.pp_support_fuse) { hw_cfg->pp_support = PP_NOT_SUPPORTED; }
        /* check the pp config vs fuse status */
        if ((hw_cfg->pp_config & 0xFC000000) &&
            ((hw_fuse_sts.pp_config_fuse & 0xF0000000) >> 5))
        {
            /* config */
            de_interlace = ((hw_cfg->pp_config & PP_DEINTERLACING) >> 25);
            alpha_blend = ((hw_cfg->pp_config & PP_ALPHA_BLENDING) >> 24);
            /* fuse */
            de_interlace_fuse =
                (((hw_fuse_sts.pp_config_fuse >> 5) & PP_DEINTERLACING) >> 25);
            alpha_blend_fuse =
                (((hw_fuse_sts.pp_config_fuse >> 5) & PP_ALPHA_BLENDING) >> 24);

            /* check if */
            if (de_interlace && !de_interlace_fuse) { hw_cfg->pp_config &= 0xFD000000; }
            if (alpha_blend && !alpha_blend_fuse) { hw_cfg->pp_config &= 0xFE000000; }
        }
        /* sorenson */
        if (!hw_fuse_sts.sorenson_spark_support_fuse)
        {
            hw_cfg->sorenson_spark_support = SORENSON_SPARK_NOT_SUPPORTED;
        }
        /* ref. picture buffer */
        if (!hw_fuse_sts.ref_buf_support_fuse)
        {
            hw_cfg->ref_buf_support = REF_BUF_NOT_SUPPORTED;
        }

        /* rv */
        if (!hw_fuse_sts.rv_support_fuse) { hw_cfg->rv_support = RV_NOT_SUPPORTED; }
        /* avs */
        if (!hw_fuse_sts.avs_support_fuse) { hw_cfg->avs_support = AVS_NOT_SUPPORTED; }
        /* mvc */
        if (!hw_fuse_sts.mvc_support_fuse) { hw_cfg->mvc_support = MVC_NOT_SUPPORTED; }
    }

    /* Maximum decoding width supported by the HW */
    if (hw_cfg->max_dec_pic_width != hw_fuse_sts.max_dec_pic_width_fuse)
    {
        hw_cfg->max_dec_pic_width = hw_fuse_sts.max_dec_pic_width_fuse;
    }
}

/*------------------------------------------------------------------------------
    Function name   : DWLReadAsicConfig
    Description     : Read HW configuration. Does not need a DWL instance to run

    Return type     : DWLHwConfig - structure with HW configuration
------------------------------------------------------------------------------*/
void DWLReadAsicConfig(DWLHwConfig *hw_cfg)
{
    /* Decoder configuration */
    memset(hw_cfg, 0, sizeof(*hw_cfg));
    ReadCoreConfig(hw_cfg);
}

const void *DWLInit(struct DWLInitParam *param)
{
    struct HX170DWL *dec_dwl;
    int i = 0;

    BUG_ON(!param);
    BUG_ON(!vp9_config.reg_size);
    dec_dwl = DWLkzalloc(sizeof(struct HX170DWL));
    if (!dec_dwl)
    {
        return NULL;
    }


    memset(dec_dwl->dwl_shadow_regs, 0, sizeof(dec_dwl->dwl_shadow_regs));
    dec_dwl->client_type = param->client_type;

    for (i = 0; i < VP9DEC_MAX_PIC_BUFFERS; i++)
    {
        dec_dwl->decodebufferptr[i] = NULL;
    }

    /* init shadow regs */
    dec_dwl->num_cores = DWLReadAsicCoreCount();
    dec_dwl->codecptr = NULL;
    dec_dwl->decode_duration = 0;

    if (!n_dwl_instance_count)
    {
        mutex_init(&x170_init_mutex);
    }

    dec_dwl->reg_size = vp9_config.reg_size;
    mutex_lock(&x170_init_mutex);
    n_dwl_instance_count++;

    switch (dec_dwl->client_type)
    {
    case DWL_CLIENT_TYPE_HEVC_DEC:
    case DWL_CLIENT_TYPE_VP9_DEC:
    case DWL_CLIENT_TYPE_PP: { break; }
    default:
    {
        SE_ERROR("Unknown client type no. %d\n", dec_dwl->client_type);
        goto err;
    }
    }

    ActivityTraceInit(&dec_dwl->activity);
    mutex_unlock(&x170_init_mutex);

    return dec_dwl;

err:
    mutex_unlock(&x170_init_mutex);
    DWLRelease(dec_dwl);

    return NULL;
}


void DWLSetIRQCallback(const void *instance, i32 core_id,
                       DWLIRQCallbackFn *callback_fn, void *arg)
{
}

i32 DWLRelease(const void *instance)
{
    struct HX170DWL *dec_dwl = (struct HX170DWL *)instance;

    if (dec_dwl == NULL) { return DWL_OK; }

    mutex_lock(&x170_init_mutex);

    n_dwl_instance_count--;

#ifdef INTERNAL_TEST
    InternalTestFinalize();
#endif

    ActivityTraceRelease(&dec_dwl->activity);

    DWLfree(dec_dwl);

    mutex_unlock(&x170_init_mutex);

    return DWL_OK;
}


i32 DWLReserveHw(const void *instance, i32 *core_id)
{
    return Vp9ReserveHW(VP9_DEC_USED);
}

void DWLReleaseHw(const void *instance, i32 core_id)
{
    Vp9ReleaseHW(VP9_DEC_USED);
}

/* Frame buffers memory */
i32 DWLMallocRefFrmNamed(const void *instance, u32 size, struct DWLLinearMem *info,
                         const char *name)
{
    return DWLMallocLinearNamed(instance, size, info, name);
}

i32 DWLMallocRefFrm(const void *instance, u32 size, struct DWLLinearMem *info, unsigned int index, unsigned int type)
{
    struct HX170DWL *dec_dwl = (struct HX170DWL *)instance;
    u32   **vma = &info->virtual_address;
    BUG_ON(dec_dwl == NULL);
#ifndef  SET_EMPTY_PICTURE_DATA
    // virtual memory adress is needed only in debug mode , should be avoided to save vma address space
    vma = NULL;
#endif
    AllocateActualBuffer(dec_dwl->codecptr, dec_dwl->decodebufferptr[index], &info->bus_address, vma, &info->size, type);
    info->logical_size = size;
    if ((vma != NULL) && (*vma == NULL))
    {
        return DWL_ERROR;
    }
    return DWL_OK;
}

void DWLAssignDecodeBufferPtr(const void *instance, unsigned int index, unsigned int width, unsigned int height)
{
    struct HX170DWL *dec_dwl = (struct HX170DWL *)instance;
    void *ptr = NULL;
    BUG_ON(index >= VP9DEC_MAX_PIC_BUFFERS);
    BUG_ON(dec_dwl == NULL);

    InitializeDecodeBufferPools(dec_dwl->codecptr, &ptr, index, width, height);
    dec_dwl->decodebufferptr[index] = ptr;
}

void DWLFreeDecodeBufferPtr(const void *instance, unsigned int index)
{
    struct HX170DWL *dec_dwl = (struct HX170DWL *)instance;

    BUG_ON(index >= VP9DEC_MAX_PIC_BUFFERS);
    BUG_ON(dec_dwl == NULL);
    if (dec_dwl->decodebufferptr[index] != NULL)
    {
        DeInitializeDecodeBufferPools(dec_dwl->codecptr, &dec_dwl->decodebufferptr[index]);
        dec_dwl->decodebufferptr[index] = NULL;
    }
    else
    {
        SE_WARNING("Decode buffer pointer at index %d already NULL\n", index);
    }
}

void DWLFreeRefFrm(const void *instance, struct DWLLinearMem *info)
{
}

/* SW/HW shared memory */
static i32 DWLMallocLinearNamed(const void *instance, u32 size, struct DWLLinearMem *info,
                                const char *name)
{
    i32 ret = DWL_OK;
    ret = Vp9MallocLinearBuffer(&info->virtual_address, &info->bus_address, size);
    if (ret)
    {
        ret = DWL_ERROR;
    }
    info->size = size;
    return ret;
}

i32 DWLMallocLinear(const void *instance, u32 size, struct DWLLinearMem *info)
{
    return DWLMallocLinearNamed(instance, size, info, "");
}

void DWLFreeLinear(const void *instance, struct DWLLinearMem *info)
{
    Vp9FreeLinearBuffer(info->virtual_address);
}

static inline u32 CheckRegOffset(struct HX170DWL *dec_dwl, u32 offset)
{
    if (dec_dwl->client_type == DWL_CLIENT_TYPE_PP)
    {
        return offset < dec_dwl->reg_size && offset >= HANTRODECPP_REG_START;
    }
    else
    {
        return offset < dec_dwl->reg_size;
    }
}

void DWLWriteReg(const void *instance, i32 core_id, u32 offset, u32 value)
{
    struct HX170DWL *dec_dwl = (struct HX170DWL *)instance;
    BUG_ON(dec_dwl == NULL);

    offset = offset / 4;
    dec_dwl->dwl_shadow_regs[offset] = value;

#ifdef INTERNAL_TEST
    InternalTestDumpWriteSwReg(core_id, offset, value, dec_dwl->dwl_shadow_regs[core_id]);
#endif
}

u32 DWLReadReg(const void *instance, i32 core_id, u32 offset)
{
    struct HX170DWL *dec_dwl = (struct HX170DWL *)instance;
    u32 val;
    BUG_ON(dec_dwl == NULL);

    offset = offset / 4;

    val = dec_dwl->dwl_shadow_regs[offset];


#ifdef INTERNAL_TEST
    InternalTestDumpReadSwReg(core_id, offset, val, dec_dwl->dwl_shadow_regs[core_id]);
#endif

    return val;
}

/* HW starting/stopping */
void DWLEnableHw(const void *instance, i32 core_id, u32 offset, u32 value)
{
    struct HX170DWL *dec_dwl = (struct HX170DWL *)instance;
    int is_pp;

    BUG_ON(dec_dwl == NULL);

    is_pp = dec_dwl->client_type == DWL_CLIENT_TYPE_PP ? 1 : 0;

    DWLWriteReg(instance, core_id, offset, value);

    Vp9HwRun(&dec_dwl->dwl_shadow_regs[0]);


    SE_DEBUG(group_decoder_video, "%s %d enabled by previous DWLWriteReg\n", is_pp ? "PP" : "DEC",
             core_id);
    ActivityTraceStartDec(&dec_dwl->activity);
}

void DWLDisableHw(const void *instance, i32 core_id, u32 offset, u32 value)
{
    struct HX170DWL *dec_dwl = (struct HX170DWL *)instance;
    int is_pp;

    BUG_ON(dec_dwl == NULL);

    is_pp = dec_dwl->client_type == DWL_CLIENT_TYPE_PP ? 1 : 0;

    DWLWriteReg(dec_dwl, core_id, offset, value);

    SE_DEBUG(group_decoder_video, "%s %d disabled by previous DWLWriteReg\n", is_pp ? "PP" : "DEC",
             core_id);

    Vp9HwRun(&dec_dwl->dwl_shadow_regs[0]);
}

/* HW synchronization */
i32 DWLWaitHwReady(const void *instance, i32 core_id, u32 timeout)
{
    int ret;
    struct HX170DWL *dec_dwl = (struct HX170DWL *)instance;
    BUG_ON(dec_dwl == NULL);

    /* run hardware with pending write */
    ret = Vp9WaitDecReadyAndRefreshTags(&dec_dwl->dwl_shadow_regs[0], &dec_dwl->decode_duration);
    if (ret)
    {
        pr_err("DWLWaitHwReady error\n");
        return DWL_HW_WAIT_ERROR;
    }

    return DWL_OK;
}

void DWLGetDecodeDuration(const void *instance, u32 *hw_decode_duration)
{
    struct HX170DWL *dec_dwl = (struct HX170DWL *)instance;
    BUG_ON(dec_dwl == NULL);
    *hw_decode_duration = dec_dwl->decode_duration;
}

void *DWLmalloc(u32 n)
{
    return OS_Malloc(n);
}

void *DWLkzalloc(u32 n)
{
    void *ptr = DWLmalloc(n);
    if (ptr)
    {
        DWLmemset(ptr, 0, n);
    }
    return ptr;
}

void DWLfree(void *p)
{
    OS_Free(p);
}

void *DWLmemcpy(void *d, const void *s, u32 n)
{
    return memcpy(d, s, (size_t) n);
}

void *DWLmemset(void *d, i32 c, u32 n)
{
    return memset(d, (int)c, (size_t) n);
}
