
/*
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301,
 * USA.
 */


#include "fifo.h"
#include "dwl.h"
#include "sw_debug.h"
#include <linux/semaphore.h>
#include <linux/vmalloc.h>
#include <linux/slab.h>

/* Container for instance. */
struct Fifo {
  struct semaphore cs_semaphore;    /* Semaphore for critical section. */
  struct semaphore read_semaphore;  /* Semaphore for readers. */
  struct semaphore write_semaphore; /* Semaphore for writers. */
  u32 num_of_slots;
  u32 num_of_objects;
  u32 tail_index;
  FifoObject* nodes;
};

enum FifoRet FifoInit(u32 num_of_slots, FifoInst* instance) {
  struct Fifo* inst = DWLkzalloc(1 * sizeof(struct Fifo)); /* SE_UPDATE for using kzalloc */
  if (inst == NULL) return FIFO_ERROR_MEMALLOC;
  inst->num_of_slots = num_of_slots;
  /* Allocate memory for the objects. */
  inst->nodes = DWLkzalloc(num_of_slots *  sizeof(FifoObject)); /* SE_UPDATE for using kzalloc */
  if (inst->nodes == NULL) {
    DWLfree(inst);
    return FIFO_ERROR_MEMALLOC;
  }
  /* Initialize binary critical section semaphore. */
  sema_init(&inst->cs_semaphore, 1);  /* SE_UPDATE for using kernel semaphore */
  /* Then initialize the read and write semaphores. */
  sema_init(&inst->read_semaphore, 0);
  sema_init(&inst->write_semaphore, num_of_slots);
  *instance = inst;
  return FIFO_OK;
}

enum FifoRet FifoPush(FifoInst inst, FifoObject object, enum FifoException e) {
  struct Fifo* instance = (struct Fifo*)inst;
  int value;

  value = ((struct semaphore *)&instance->read_semaphore)->count; /* SE_UPDATE for using kernel semaphore */
  if ((e == FIFO_EXCEPTION_ENABLE) && (value == instance->num_of_slots) &&
      (instance->num_of_objects == instance->num_of_slots)) {
    return FIFO_FULL;
  }

  down(&instance->write_semaphore); /* SE_UPDATE */
  down(&instance->cs_semaphore); /* SE_UPDATE */
  instance->nodes[(instance->tail_index + instance->num_of_objects) %
                  instance->num_of_slots] = object;
  instance->num_of_objects++;
  up(&instance->cs_semaphore); /* SE_UPDATE */
  up(&instance->read_semaphore); /* SE_UPDATE */
  return FIFO_OK;
}

enum FifoRet FifoPop(FifoInst inst, FifoObject* object, enum FifoException e) {
  struct Fifo* instance = (struct Fifo*)inst;
  int value;

  value = ((struct semaphore *)&instance->write_semaphore)->count; /* SE_UPDATE */
  if ((e == FIFO_EXCEPTION_ENABLE) && (value == instance->num_of_slots) &&
      (instance->num_of_objects == 0)) {
    return FIFO_EMPTY;
  }

  down(&instance->read_semaphore); /* SE_UPDATE */
  down(&instance->cs_semaphore); /* SE_UPDATE */
  *object = instance->nodes[instance->tail_index % instance->num_of_slots];
  instance->tail_index++;
  instance->num_of_objects--;
  up(&instance->cs_semaphore); /* SE_UPDATE */
  up(&instance->write_semaphore); /* SE_UPDATE */
  return FIFO_OK;
}

u32 FifoCount(FifoInst inst) {
  u32 count;
  struct Fifo* instance = (struct Fifo*)inst;
  down(&instance->cs_semaphore); /* SE_UPDATE */
  count = instance->num_of_objects;
  up(&instance->cs_semaphore); /* SE_UPDATE */
  return count;
}

void FifoRelease(FifoInst inst) {
  struct Fifo* instance = (struct Fifo*)inst;
  assert(instance->num_of_objects == 0);
  down(&instance->cs_semaphore); /* SE_UPDATE */
  //sema_destroy(&instance->cs_semaphore);
  //sema_destroy(&instance->read_semaphore);
  //sema_destroy(&instance->write_semaphore);
  DWLfree(instance->nodes); /* SE_UPDATE */
  DWLfree(instance); /* SE_UPDATE */
}
