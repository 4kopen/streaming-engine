
/*
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301,
 * USA.
 */


#include "software/source/common/raster_buffer_mgr.h"

struct BufferPair {
  struct DWLLinearMem tiled_buffer;
  struct DWLLinearMem raster_buffer;
#ifdef DOWN_SCALER
  struct DWLLinearMem dscale_buffer;
#endif
};

typedef struct {
  u32 num_buffers;
  struct BufferPair* buffer_map;
  const void* dwl;
} RasterBufferMgrInst;

RasterBufferMgr RbmInit(struct RasterBufferParams params) {
  RasterBufferMgrInst* inst = DWLmalloc(sizeof(RasterBufferMgrInst));
  inst->buffer_map = DWLcalloc(params.num_buffers, sizeof(struct BufferPair));
  inst->num_buffers = params.num_buffers;
  inst->dwl = params.dwl;
  u32 size = params.width * params.height * 3 / 2;
#ifdef DOWN_SCALER
  u32 dscale_size = params.ds_width * params.ds_height * 3 / 2;
#endif
  for (int i = 0; i < inst->num_buffers; i++) {
    if (DWLMallocRefFrm(inst->dwl, size, &inst->buffer_map[i].raster_buffer)) {
      RbmRelease(inst);
      return NULL;
    }
    inst->buffer_map[i].tiled_buffer = params.tiled_buffers[i];
#ifdef DOWN_SCALER
    if (DWLMallocRefFrm(inst->dwl, dscale_size, &inst->buffer_map[i].dscale_buffer)) {
      RbmRelease(inst);
      return NULL;
    }
#endif
  }
  return inst;
}

struct DWLLinearMem RbmGetRasterBuffer(RasterBufferMgr instance,
                                       struct DWLLinearMem key) {
  RasterBufferMgrInst* inst = (RasterBufferMgrInst*)instance;
  for (int i = 0; i < inst->num_buffers; i++)
    if (inst->buffer_map[i].tiled_buffer.virtual_address == key.virtual_address)
      return inst->buffer_map[i].raster_buffer;
  struct DWLLinearMem empty = {0};
  return empty;
}

#ifdef DOWN_SCALER
struct DWLLinearMem RbmGetDscaleBuffer(RasterBufferMgr instance,
                                       struct DWLLinearMem key) {
  RasterBufferMgrInst* inst = (RasterBufferMgrInst*)instance;
  for (int i = 0; i < inst->num_buffers; i++)
    if (inst->buffer_map[i].tiled_buffer.virtual_address == key.virtual_address)
      return inst->buffer_map[i].dscale_buffer;
  struct DWLLinearMem empty = {0};
  return empty;
}
#endif

struct DWLLinearMem RbmGetTiledBuffer(RasterBufferMgr instance,
                                      struct DWLLinearMem key) {
  RasterBufferMgrInst* inst = (RasterBufferMgrInst*)instance;
  for (int i = 0; i < inst->num_buffers; i++)
    if (inst->buffer_map[i].raster_buffer.virtual_address ==
        key.virtual_address)
      return inst->buffer_map[i].tiled_buffer;
  struct DWLLinearMem empty = {0};
  return empty;
}

void RbmRelease(RasterBufferMgr instance) {
  RasterBufferMgrInst* inst = (RasterBufferMgrInst*)instance;
  for (int i = 0; i < inst->num_buffers; i++) {
    if (inst->buffer_map[i].raster_buffer.virtual_address != NULL)
      DWLFreeRefFrm(inst->dwl, &inst->buffer_map[i].raster_buffer);
#ifdef DOWN_SCALER
    if (inst->buffer_map[i].dscale_buffer.virtual_address != NULL)
      DWLFreeRefFrm(inst->dwl, &inst->buffer_map[i].dscale_buffer);
#endif
  }
  DWLfree(inst->buffer_map);
  DWLfree(inst);
}
