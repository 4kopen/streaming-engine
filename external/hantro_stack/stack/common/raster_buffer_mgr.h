
/*
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301,
 * USA.
 */

#ifndef RASTER_BUFFER_MGR_H
#define RASTER_BUFFER_MGR_H

#include "software/source/inc/basetype.h"
#include "software/source/inc/dwl.h"

typedef void* RasterBufferMgr;

struct RasterBufferParams {
  struct DWLLinearMem* tiled_buffers;
  u32 num_buffers;
  u32 width;
  u32 height;
#ifdef DOWN_SCALER
  u32 ds_width;
  u32 ds_height;
#endif
  const void* dwl;
};

RasterBufferMgr RbmInit(struct RasterBufferParams params);
struct DWLLinearMem RbmGetRasterBuffer(RasterBufferMgr instance,
                                       struct DWLLinearMem key);
struct DWLLinearMem RbmGetTiledBuffer(RasterBufferMgr instance,
                                      struct DWLLinearMem buffer);
#ifdef DOWN_SCALER
struct DWLLinearMem RbmGetDscaleBuffer(RasterBufferMgr instance,
                                       struct DWLLinearMem key);
#endif
void RbmRelease(RasterBufferMgr inst);

#endif /* RASTER_BUFFER_MGR_H */
