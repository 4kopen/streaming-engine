
/*
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301,
 * USA.
 */


#ifndef SW_DEBUG_H_
#define SW_DEBUG_H_
/* SE_UPDATE - to remove stdio file */

/* macro for assertion, used only when _ASSERT_USED is defined */
#ifdef _ASSERT_USED
#include "report.h"

#undef TRACE_TAG
#define TRACE_TAG "Hantro_Vp9_c"

#ifndef ASSERT

/* Assert functions definition */
#define assert(expr) SE_ASSERT(expr)
#define ASSERT(expr) SE_ASSERT(expr)
#endif
#else
#define assert(expr, args...)
#define ASSERT(expr)
#endif

/* macros for range checking used only when _RANGE_CHECK is defined */
#ifdef _RANGE_CHECK

/* macro for range checking an single value */
#define RANGE_CHECK(value, min_bound, max_bound)                   \
  {                                                                \
    if ((value) < (min_bound) || (value) > (max_bound))            \
      SE_WARNING("Value exceeds given limit(s)!\n"); \
  }

/* macro for range checking an array of values */
#define RANGE_CHECK_ARRAY(array, min_bound, max_bound, length)               \
  {                                                                          \
    i32 i;                                                                   \
    for (i = 0; i < (length); i++)                                           \
      if ((array)[i] < (min_bound) || (array)[i] > (max_bound))              \
        SE_WARNING("Value [%d] exceeds given limit(s)!\n", i); \
  }

#else /* _RANGE_CHECK */

#define RANGE_CHECK_ARRAY(array, min_bound, max_bound, length)
#define RANGE_CHECK(value, min_bound, max_bound)

#endif /* _RANGE_CHECK */

/* macro for debug printing, used only when _DEBUG_PRINT is defined */
#ifdef _SW_DEBUG_PRINT
#include <stdio.h>
#define DEBUG_PRINT(args) printf args
#else
#define DEBUG_PRINT(args)
#endif

/* macro for error printing, used only when _ERROR_PRINT is defined */
#ifdef _ERROR_PRINT
#include <stdio.h>
#define ERROR_PRINT(msg) fprintf(stderr, "ERROR: %s\n", msg)
#else
#define ERROR_PRINT(msg)
#endif

#endif /* SW_DEBUG_H_ */
