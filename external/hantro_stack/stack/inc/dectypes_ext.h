
/*
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301,
 * USA.
 */


#ifndef DECTYPES_EXT_H
#define DECTYPES_EXT_H

#include "basetype.h"
#include "dwl.h"

/** Decoder initialization params */
struct DecConfig {
  u32 disable_picture_reordering;
  enum DecPictureFormat output_format; /**< Format of the output picture */
  struct DWL dwl; /**< Pointers to the struct struct struct DWL functions. */
  const void *dwl_inst;       /**< struct struct struct DWL instance. */
  u32 max_num_pics_to_decode; /**< Limits the decoding to N pictures. 0 for
                                   unlimited. */
#ifdef DOWN_SCALER
  struct DecDownscaleCfg dscale_cfg;
#endif
};

#endif /* DECTYPES_EXT_H */
