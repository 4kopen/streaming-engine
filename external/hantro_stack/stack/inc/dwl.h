
/*
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301,
 * USA.
 */


/* Decoder Wrapper Layer for operating system services. */

#ifndef __DWL_H__
#define __DWL_H__

#ifdef __cplusplus
extern "C" {
#endif

#include "basetype.h"
#include "decapicommon.h"

#define DWL_OK 0
#define DWL_ERROR -1

#define DWL_HW_WAIT_OK DWL_OK
#define DWL_HW_WAIT_ERROR DWL_ERROR
#define DWL_HW_WAIT_TIMEOUT 1

#define DWL_CLIENT_TYPE_PP 4U
#define DWL_CLIENT_TYPE_VP9_DEC 11U
#define DWL_CLIENT_TYPE_HEVC_DEC 12U

/* Linear memory area descriptor */
struct DWLLinearMem {
  u32 *virtual_address;
  u32 bus_address;
  u32 size;         /* physical size (rounded to page multiple) */
  u32 logical_size; /* requested size in bytes */
};

/* DWLInitParam is used to pass parameters when initializing the DWL */
struct DWLInitParam {
  u32 client_type;
};

/* Hardware configuration description, same as in top API */
typedef struct DecHwConfig DWLHwConfig;

struct DWLHwFuseStatus {
  u32 vp6_support_fuse;            /* HW supports VP6 */
  u32 vp7_support_fuse;            /* HW supports VP7 */
  u32 vp8_support_fuse;            /* HW supports VP8 */
  u32 vp9_support_fuse;            /* HW supports VP9 */
  u32 h264_support_fuse;           /* HW supports H.264 */
  u32 hevc_support_fuse;           /* HW supports HEVC */
  u32 mpeg4_support_fuse;          /* HW supports MPEG-4 */
  u32 mpeg2_support_fuse;          /* HW supports MPEG-2 */
  u32 sorenson_spark_support_fuse; /* HW supports Sorenson Spark */
  u32 jpeg_support_fuse;           /* HW supports JPEG */
  u32 vc1_support_fuse;            /* HW supports VC-1 Simple */
  u32 jpeg_prog_support_fuse;      /* HW supports Progressive JPEG */
  u32 pp_support_fuse;             /* HW supports post-processor */
  u32 pp_config_fuse;              /* HW post-processor functions bitmask */
  u32 max_dec_pic_width_fuse;      /* Maximum video decoding width supported  */
  u32 max_pp_out_pic_width_fuse;   /* Maximum output width of Post-Processor */
  u32 ref_buf_support_fuse;        /* HW supports reference picture buffering */
  u32 avs_support_fuse;            /* HW supports AVS */
  u32 rv_support_fuse;             /* HW supports RealVideo codec */
  u32 mvc_support_fuse;            /* HW supports MVC */
  u32 custom_mpeg4_support_fuse;   /* Fuse for custom MPEG-4 */
};

/* HW ID retrieving, static implementation */
u32 DWLReadAsicID(void);

/* HW configuration retrieving, static implementation */
void DWLReadAsicConfig(DWLHwConfig *hw_cfg);
void DWLReadMCAsicConfig(DWLHwConfig hw_cfg[MAX_ASIC_CORES]);

/* Return number of ASIC cores, static implementation */
u32 DWLReadAsicCoreCount(void);

/* HW fuse retrieving, static implementation */
void DWLReadAsicFuseStatus(struct DWLHwFuseStatus *hw_fuse_sts);

/* struct struct struct DWL initialization and release */
const void *DWLInit(struct DWLInitParam *param);
i32 DWLRelease(const void *instance);

/* HW sharing */
i32 DWLReserveHw(const void *instance, i32 *core_id);
i32 DWLReserveHwPipe(const void *instance, i32 *core_id);
void DWLReleaseHw(const void *instance, i32 core_id);

/* Frame buffers memory */
/* SE_UPDATE - adding more arguments in-order to map buffers to decode buffer manager */
i32 DWLMallocRefFrm(const void *instance, u32 size, struct DWLLinearMem *info, unsigned int index, unsigned int type);
void DWLFreeRefFrm(const void *instance, struct DWLLinearMem *info);

/* SE_UPDATE - new apis added in-order to map buffers to decode buffer manager */
void DWLAssignDecodeBufferPtr(const void *instance, unsigned int index,unsigned int width,unsigned int height);
void DWLFreeDecodeBufferPtr(const void *instance, unsigned int index);
void DWLGetDecodeBufferPtr(const void *instance, unsigned int index,void **ptr);
void DWLRegisterCodecPtr(const void *instance, void *codecPtr);
/* SE_UPDATE - end of new apis added */

/* SW/HW shared memory */
i32 DWLMallocLinear(const void *instance, u32 size, struct DWLLinearMem *info);
void DWLFreeLinear(const void *instance, struct DWLLinearMem *info);

/* Register access */
void DWLWriteReg(const void *instance, i32 core_id, u32 offset, u32 value);
u32 DWLReadReg(const void *instance, i32 core_id, u32 offset);

/* HW starting/stopping */
void DWLEnableHw(const void *instance, i32 core_id, u32 offset, u32 value);
void DWLDisableHw(const void *instance, i32 core_id, u32 offset, u32 value);

/* HW synchronization */
i32 DWLWaitHwReady(const void *instance, i32 core_id, u32 timeout);

typedef void DWLIRQCallbackFn(void *arg, i32 core_id);

void DWLSetIRQCallback(const void *instance, i32 core_id,
                       DWLIRQCallbackFn *callback_fn, void *arg);

void DWLGetDecodeDuration(const void *instance, u32 *hw_decode_duration);

/* SW/SW shared memory */
void *DWLmalloc(u32 n);
void *DWLkzalloc(u32 n);
void DWLfree(void *p);
void *DWLmemcpy(void *d, const void *s, u32 n);
void *DWLmemset(void *d, i32 c, u32 n);

/* SE_UPDATE - struct DWL is defined in dwl_ext.h to fix compilation issues */

#ifdef __cplusplus
}
#endif

#endif /* __DWL_H__ */
