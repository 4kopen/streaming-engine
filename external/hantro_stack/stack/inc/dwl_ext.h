
/*
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301,
 * USA.
 */


/* Decoder Wrapper Layer for operating system services. */

#ifndef __DWL_EXT_H__
#define __DWL_EXT_H__

#ifdef __cplusplus
extern "C" {
#endif

#include "basetype.h"
#include "dwl_ext.h"
#include "decapicommon.h"

//#ifdef __linux__
//#include <pthread.h>
//#else
//#error "No pthread defined for the system, define prototypes."
//#endif /* HAVE_PTHREAD_H */
#include <linux/list.h>
#include <linux/mutex.h>


/* Decoder wrapper layer functionality. */
struct DWL {
  /* HW sharing */
  i32(*ReserveHw)(const void *instance, i32 *core_id);
  i32(*ReserveHwPipe)(const void *instance, i32 *core_id);
  void (*ReleaseHw)(const void *instance, i32 core_id);
  /* Physical, linear memory functions */
  i32(*MallocLinear)(const void *instance, u32 size,
                     struct DWLLinearMem *info);
  void (*FreeLinear)(const void *instance, struct DWLLinearMem *info);
  /* Register access */
  void (*WriteReg)(const void *instance, i32 core_id, u32 offset, u32 value);
  u32(*ReadReg)(const void *instance, i32 core_id, u32 offset);
  /* HW starting/stopping */
  void (*EnableHw)(const void *instance, i32 core_id, u32 offset, u32 value);
  void (*DisableHw)(const void *instance, i32 core_id, u32 offset, u32 value);
  /* HW synchronization */
  i32(*WaitHwReady)(const void *instance, i32 core_id, u32 timeout);
  void (*SetIRQCallback)(const void *instance, i32 core_id,
                         DWLIRQCallbackFn *callback_fn, void *arg);
  /* Virtual memory functions. */
  void *(*malloc)(u32 n);
  void (*free)(void *p);
  void *(*calloc)(u32 n, u32 s);
  void *(*memcpy)(void *d, const void *s, u32 n);
  void *(*memset)(void *d, i32 c, u32 n);
  /* SE_UPDATE - commented pthreads related api calls to use kernel mutex/semaphores */
  /* POSIX compatible threading functions. */
//i32 (*pthread_create)(pthread_t *tid, const pthread_attr_t *attr,
//                      void *(*start)(void *), void *arg);
//void (*pthread_exit)(void *value_ptr);
//i32 (*pthread_join)(pthread_t thread, void **value_ptr);
  i32(*mutex_init)(struct mutex *mutex, int type, void *arg);
  i32(*mutex_destroy)(struct mutex *mutex);
  i32(*mutex_lock)(struct mutex *mutex);
  i32(*mutex_unlock)(struct mutex *mutex);
//i32 (*pthread_cond_init)(pthread_cond_t *cond,
//                         const pthread_condattr_t *attr);
//i32 (*pthread_cond_destroy)(pthread_cond_t *cond);
//i32 (*pthread_cond_wait)(pthread_cond_t *cond, pthread_mutex_t *mutex);
//i32 (*pthread_cond_signal)(pthread_cond_t *cond);
  /* API trace function. Set to NULL if no trace wanted. */
  int (*printf)(const char *string, ...);
};

#ifdef __cplusplus
}
#endif

#endif /* __DWLEXT__H__ */

