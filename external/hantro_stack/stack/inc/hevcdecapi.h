
/*
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301,
 * USA.
 */


#ifndef HEVCDECAPI_H
#define HEVCDECAPI_H

#ifdef __cplusplus
extern "C" {
#endif

#include "basetype.h"
#include "decapicommon.h"
#include "dectypes.h"

/*------------------------------------------------------------------------------
    API type definitions
------------------------------------------------------------------------------*/

/* Decoder instance */
typedef const void *HevcDecInst;

/* Input structure */
struct HevcDecInput {
  u8 *stream;             /* Pointer to the input */
  u32 stream_bus_address; /* DMA bus address of the input stream */
  u32 data_len;           /* Number of bytes to be decoded         */
  u32 pic_id;             /* Identifier for the picture to be decoded */
  u32 *raster_out_y;
  u32 raster_out_bus_address_y;
  u32 *raster_out_c;
  u32 raster_out_bus_address_c;
};

/* Output structure */
struct HevcDecOutput {
  u8 *strm_curr_pos; /* Pointer to stream position where decoding ended */
  u32 strm_curr_bus_address; /* DMA bus address location where the decoding
                                ended */
  u32 data_left; /* how many bytes left undecoded */
};

/* cropping info */
struct HevcCropParams {
  u32 crop_left_offset;
  u32 crop_out_width;
  u32 crop_top_offset;
  u32 crop_out_height;
};

/* stream info filled by HevcDecGetInfo */
struct HevcDecInfo {
  u32 pic_width;   /* decoded picture width in pixels */
  u32 pic_height;  /* decoded picture height in pixels */
  u32 video_range; /* samples' video range */
  u32 matrix_coefficients;
  struct HevcCropParams crop_params;   /* display cropping information */
  enum DecPictureFormat output_format; /* format of the output picture */
  u32 sar_width;                       /* sample aspect ratio */
  u32 sar_height;                      /* sample aspect ratio */
  u32 mono_chrome;                     /* is sequence monochrome */
  u32 interlaced_sequence;             /* is sequence interlaced */
  u32 dpb_mode;      /* DPB mode; frame, or field interlaced */
  u32 pic_buff_size; /* number of picture buffers allocated&used by decoder */
  u32 multi_buff_pp_size; /* number of picture buffers needed in
                             decoder+postprocessor multibuffer mode */
};

/* Output structure for HevcDecNextPicture */
struct HevcDecPicture {
  u32 pic_width;  /* pixels width of the picture as stored in memory */
  u32 pic_height; /* pixel height of the picture as stored in memory */
  struct HevcCropParams crop_params; /* cropping parameters */
  const u32 *output_picture;         /* Pointer to the picture */
  u32 output_picture_bus_address;    /* DMA bus address of the output picture
                               buffer */
#ifdef DOWN_SCALER
  const u32 *output_downscale_picture;         /* Pointer to the picture */
  u32 output_downscale_picture_bus_address;    /* DMA bus address of the output picture
                               buffer */
#endif
  u32 pic_id;         /* Identifier of the picture to be displayed */
  u32 is_idr_picture; /* Indicates if picture is an IDR picture */
  u32 pic_corrupt;    /* Indicates that picture is corrupted */
  enum DecPictureFormat output_format;
  u32 cycles_per_mb;   /* Avarage cycle count per macroblock */
  struct HevcDecInfo dec_info; /* Stream info by HevcDecGetInfo */
};

typedef struct DecSwHwBuild HevcDecBuild;

/*------------------------------------------------------------------------------
    Prototypes of Decoder API functions
------------------------------------------------------------------------------*/

HevcDecBuild HevcDecGetBuild(void);

enum DecRet HevcDecInit(HevcDecInst *dec_inst, u32 no_output_reordering,
                        u32 use_video_freeze_concealment,
#ifdef DOWN_SCALER
                        struct DecDownscaleCfg *dscale_cfg,
#endif
                        enum DecPictureFormat output_format);

enum DecRet HevcDecUseExtraFrmBuffers(HevcDecInst dec_inst, u32 n);

void HevcDecRelease(HevcDecInst dec_inst);

enum DecRet HevcDecDecode(HevcDecInst dec_inst,
                          const struct HevcDecInput *input,
                          struct HevcDecOutput *output);

enum DecRet HevcDecNextPicture(HevcDecInst dec_inst,
                               struct HevcDecPicture *picture);

enum DecRet HevcDecPictureConsumed(HevcDecInst dec_inst,
                                   const struct HevcDecPicture *picture);

enum DecRet HevcDecEndOfStream(HevcDecInst dec_inst);

enum DecRet HevcDecGetInfo(HevcDecInst dec_inst, struct HevcDecInfo *dec_info);

enum DecRet HevcDecPeek(HevcDecInst dec_inst, struct HevcDecPicture *output);

#ifdef __cplusplus
}
#endif

#endif /* HEVCDECAPI_H */
