
/*
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301,
 * USA.
 */
/************************************************************************
Copyright (C) 2003-2014 STMicroelectronics. All Rights Reserved.
************************************************************************/

#ifndef VP9DECAPI_H
#define VP9DECAPI_H

#ifdef __cplusplus
extern "C" {
#endif

#include "basetype.h"
#include "decapicommon.h"
#include "dectypes.h"

/*------------------------------------------------------------------------------
    API type definitions
------------------------------------------------------------------------------*/

/* Decoder instance */
typedef const void *Vp9DecInst;

/* Input structure */
struct Vp9DecInput {
  u8 *stream;             /* Pointer to the input */
  u32 stream_bus_address; /* DMA bus address of the input stream */
  u32 data_len;           /* Number of bytes to be decoded         */
  u32 pic_id;             /* Identifier for the picture to be decoded */
};

/* Output structure */
struct Vp9DecOutput {
  u8 *strm_curr_pos; /* Pointer to stream position where decoding ended */
  u32 strm_curr_bus_address; /* DMA bus address location where the decoding
                                ended */
  u32 data_left; /* how many bytes left undecoded */
};

/* stream info filled by Vp9DecGetInfo */
struct Vp9DecInfo {
  u32 vp_version;
  u32 vp_profile;
  u32 coded_width;   /* coded width */
  u32 coded_height;  /* coded height */
  u32 frame_width;   /* pixels width of the frame as stored in memory */
  u32 frame_height;  /* pixel height of the frame as stored in memory */
  u32 scaled_width;  /* scaled width of the displayed video */
  u32 scaled_height; /* scaled height of the displayed video */
  u32 dpb_mode;      /* DPB mode; frame, or field interlaced */
  enum DecPictureFormat output_format; /* format of the output picture */
  u32 pic_buff_size; /* number of picture buffers allocated&used by decoder */
  u32 multi_buff_pp_size; /* number of picture buffers needed in
                             decoder+postprocessor multibuffer mode */
};

typedef struct DecSwHwBuild Vp9DecBuild;

/* Output structure for Vp9DecNextPicture */
struct Vp9DecPicture {
  u32 coded_width;  /* coded width of the picture */
  u32 coded_height; /* coded height of the picture */
  u32 frame_width;  /* pixels width of the frame as stored in memory */
  u32 frame_height; /* pixel height of the frame as stored in memory */
  const u32 *output_luma_base;   /* Pointer to the picture */
  u32 output_luma_bus_address;   /* Bus address of the luminance component */
  const u32 *output_chroma_base; /* Pointer to the picture */
  u32 output_chroma_bus_address; /* Bus address of the chrominance component */
  u32 pic_id;                    /* Identifier of the Frame to be displayed */
  u32 is_intra_frame;            /* Indicates if Frame is an Intra Frame */
  u32 is_golden_frame; /* Indicates if Frame is a Golden reference Frame */
  u32 nbr_of_err_mbs;  /* Number of concealed MB's in the frame  */
  u32 num_slice_rows;
  u32 cycles_per_mb;   /* Avarage cycle count per macroblock */
  enum DecPictureFormat output_format;
#ifdef DOWN_SCALER
  u32 dscale_width;  /* valid pixels width of the frame as stored in memory */
  u32 dscale_height; /* pixel height of the frame as stored in memory */
  u32 dscale_stride; /* pixel stride of each pixel line in memory. */
  const u32 *output_dscale_luma_base;   /* Pointer to the picture */
  u32 output_dscale_luma_bus_address;   /* Bus address of the luminance component */
  const u32 *output_dscale_chroma_base; /* Pointer to the picture */
  u32 output_dscale_chroma_bus_address; /* Bus address of the chrominance component */
#endif
  unsigned int index; /* SE UPDATE - to store index value for decde buffer ptr */
};

/* SE_UPDATE - introduced to identify the type of buffer to be allocated */
#define DECODECOPY_LUMA 0
#define DECODECOPY_CHROMA 1
#define MBSTRUCT_MVS 2
#define DISPLAY_LUMA 3
#define DISPLAY_CHROMA 4
#define DECIMATED_LUMA 5
#define DECIMATED_CHROMA 6


/*------------------------------------------------------------------------------
    Prototypes of Decoder API functions
------------------------------------------------------------------------------*/

Vp9DecBuild Vp9DecGetBuild(void);

enum DecRet Vp9DecInit(Vp9DecInst *dec_inst, u32 use_video_freeze_concealment,
                       u32 num_frame_buffers, enum DecDpbFlags dpb_flags,
#ifdef DOWN_SCALER
                       struct DecDownscaleCfg *dscale_cfg,
#endif
                       enum DecPictureFormat output_format);

void Vp9DecRelease(Vp9DecInst dec_inst);

enum DecRet Vp9DecDecode(Vp9DecInst dec_inst, const struct Vp9DecInput *input,
                         struct Vp9DecOutput *output);

enum DecRet Vp9DecNextPicture(Vp9DecInst dec_inst,
                              struct Vp9DecPicture *output);

enum DecRet Vp9DecPictureConsumed(Vp9DecInst dec_inst,
                                  const struct Vp9DecPicture *picture);

enum DecRet Vp9DecEndOfStream(Vp9DecInst dec_inst);

enum DecRet Vp9DecGetInfo(Vp9DecInst dec_inst, struct Vp9DecInfo *dec_info);

/* SE_UPDATE - new APIs added to allow buffer allocated by decode buffer manager */
/* Value of MAX_BUFF_ALLOCATION_ALLOWED is kept equal to Display/Reference buffer in SE */
#define MAX_BUFF_ALLOCATION_ALLOWED 9

enum DecRet Vp9DecDecodeWrapper(Vp9DecInst dec_inst, const struct Vp9DecInput *input,
                         struct Vp9DecOutput *output, u32 *sw_decode_duration,u32 *hw_decode_duration);

void Vp9RegisterClientPtr(Vp9DecInst dec_inst, void *ptr);

enum DecRet Vp9ExecuteClientMemoryInit(Vp9DecInst dec_inst);

void Vp9DecGetDecodeBufferPtr(Vp9DecInst dec_inst,unsigned int index, void **ptr);

void Vp9DecGetDecodeBufferIndex(Vp9DecInst dec_inst,unsigned int *index, void **ptr);

void Vp9DecInfoLastDecode(Vp9DecInst dec_inst,unsigned int *w,unsigned int *h,unsigned int *pic_id,unsigned int *is_intra_frame,unsigned int *idx);

/* SE_UPDATE - end of new APIs added */

#ifdef __cplusplus
}
#endif

#endif /* VP9DECAPI_H */
