
/*
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301,
 * USA.
 */

#ifndef VP9_COMMON_VP9_ENTROPYMV_H_
#define VP9_COMMON_VP9_ENTROPYMV_H_

#include "commonvp9.h"
#include "vp9_treecoder.h"
#include "vp9hwd_decoder.h"

void Vp9EntropyMvInit(void); /* SE_UPDATE - void added to avoid compilation issue */
void Vp9InitMvProbs(struct Vp9Decoder* x);

void Vp9AdaptNmvProbs(struct Vp9Decoder* cm);

#define LOW_PRECISION_MV_UPDATE /* Use 7 bit forward update */

extern const vp9_tree_index vp9_mv_class_tree[2 * MV_CLASSES - 2];
extern struct vp9_token vp9_mv_class_encodings[MV_CLASSES];

extern const vp9_tree_index vp9_mv_class0_tree[2 * CLASS0_SIZE - 2];
extern struct vp9_token vp9_mv_class0_encodings[CLASS0_SIZE];

extern const vp9_tree_index vp9_mv_fp_tree[2 * 4 - 2];
extern struct vp9_token vp9_mv_fp_encodings[4];

#endif  // VP9_COMMON_VP9_ENTROPYMV_H_
