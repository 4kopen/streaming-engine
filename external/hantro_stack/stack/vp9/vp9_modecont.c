
/*
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301,
 * USA.
 */

#include "commonvp9.h"

const u8 vp9_default_inter_mode_prob[INTER_MODE_CONTEXTS][4] = {
    {2, 173, 34, 0},  // 0 = both zero mv
    {7, 145, 85, 0},  // 1 = one zero mv + one a predicted mv
    {7, 166, 63, 0},  // 2 = two predicted mvs
    {7, 94, 66, 0},   // 3 = one predicted/zero and one new mv
    {8, 64, 46, 0},   // 4 = two new mvs
    {17, 81, 31, 0},  // 5 = one intra neighbour + x
    {25, 29, 30, 0},  // 6 = two intra neighbours
};
