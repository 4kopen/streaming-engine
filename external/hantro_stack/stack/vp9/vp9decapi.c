
/*
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301,
 * USA.
 */
/************************************************************************
Copyright (C) 2003-2014 STMicroelectronics. All Rights Reserved.
************************************************************************/

/* SE_UPDATE - this file is adapted for ST platform */

#include "basetype.h"
#include "decapicommon.h"
#include "fifo.h"
#include "vp9decapi.h"
#include "version.h"

#include "dwl_ext.h"
#include "regdrv.h"

#include "vp9hwd_container.h"
#include "vp9hwd_asic.h"
#include "vp9hwd_headers.h"
#include "vp9hwd_output.h"
#include "sw_util.h"

static u32 Vp9CheckSupport(struct Vp9DecContainer *dec_cont);
static void Vp9Freeze(struct Vp9DecContainer *dec_cont);
static i32 Vp9DecodeHeaders(struct Vp9DecContainer *dec_cont,
                            const struct Vp9DecInput *input);

Vp9DecBuild Vp9DecGetBuild(void) {
  Vp9DecBuild build_info;

  (void)DWLmemset(&build_info, 0, sizeof(build_info));

  build_info.sw_build = HANTRO_DEC_SW_BUILD;
  build_info.hw_build = DWLReadAsicID();

  DWLReadAsicConfig(&build_info.hw_config);

  return build_info;
}

enum DecRet Vp9DecInit(Vp9DecInst *dec_inst, u32 use_video_freeze_concealment,
                       u32 num_frame_buffers, enum DecDpbFlags dpb_flags,
#ifdef DOWN_SCALER
                       struct DecDownscaleCfg *dscale_cfg,
#endif
                       enum DecPictureFormat output_format) {
  struct Vp9DecContainer *dec_cont;
  const void *dwl;

  struct DWLInitParam dwl_init;
  DWLHwConfig hw_cfg;

  (void)dpb_flags;

/* check that right shift on negative numbers is performed signed */
#if (((-1) >> 1) != (-1))
#error Right bit-shifting (>>) does not preserve the sign
#endif

  if (dec_inst == NULL) {
    return DEC_PARAM_ERROR;
  }

  *dec_inst = NULL; /* return NULL instance for any error */

  /* check that decoding supported in HW */
  DWLReadAsicConfig(&hw_cfg);
  if (!hw_cfg.vp9_support) {
    return DEC_FORMAT_NOT_SUPPORTED;
  }

  /* init struct struct DWL for the specified client */
  dwl_init.client_type = DWL_CLIENT_TYPE_VP9_DEC;

  dwl = DWLInit(&dwl_init);

  if (dwl == NULL) {
    return DEC_DWL_ERROR;
  }

  /* allocate instance */
  dec_cont =
      (struct Vp9DecContainer *)DWLmalloc(sizeof(struct Vp9DecContainer));

  if (dec_cont == NULL) {
    (void)DWLRelease(dwl);
    return DEC_MEMFAIL;
  }

  (void)DWLmemset(dec_cont, 0, sizeof(struct Vp9DecContainer));
  dec_cont->dwl = dwl;

#ifdef DOWN_SCALER
  /* Down scaler ratio */
  if ((dscale_cfg->down_scale_x == 1) || (dscale_cfg->down_scale_y == 1)) {
    dec_cont->down_scale_enabled = 0;
    dec_cont->down_scale_x_shift = 0;
    dec_cont->down_scale_y_shift = 0;
  } else if ((dscale_cfg->down_scale_x != 2 &&
      dscale_cfg->down_scale_x != 4 &&
      dscale_cfg->down_scale_x != 8 ) ||
      (dscale_cfg->down_scale_y != 2 &&
      dscale_cfg->down_scale_y != 4 &&
      dscale_cfg->down_scale_y != 8 )) {
    DWLfree(dec_cont); /* SE_UPDATE - to fix coverity warning */
    (void)DWLRelease(dwl);
    return (DEC_PARAM_ERROR);
  } else {
    u32 scale_table[9] = {0, 0, 1, 0, 2, 0, 0, 0, 3};

    /* Down scale can only be enabled with output format in raster scan. */
    if (output_format != DEC_OUT_FRM_RASTER_SCAN) {
	  DWLfree(dec_cont);
	  (void)DWLRelease(dwl); /* SE_UPDATE - to fix coverity warning */
	  return (DEC_PARAM_ERROR);
	}

    dec_cont->down_scale_enabled = 1;
    dec_cont->down_scale_x_shift = scale_table[dscale_cfg->down_scale_x];
    dec_cont->down_scale_y_shift = scale_table[dscale_cfg->down_scale_y];
  }
#endif

  /* initial setup of instance */

  dec_cont->dec_stat = VP9DEC_INITIALIZED;
  dec_cont->checksum = dec_cont; /* save instance as a checksum */

  if (num_frame_buffers > VP9DEC_MAX_PIC_BUFFERS)
    num_frame_buffers = VP9DEC_MAX_PIC_BUFFERS;

  dec_cont->num_buffers = num_frame_buffers;

  Vp9AsicInit(dec_cont); /* Init ASIC */

  if (Vp9AsicAllocateMem(dec_cont) != 0) {
    DWLfree(dec_cont);
    (void)DWLRelease(dwl); /* SE_UPDATE - to fix coverity warning */
    return DEC_MEMFAIL;
  }

  dec_cont->pic_number = dec_cont->display_number = 1;
  dec_cont->intra_freeze = use_video_freeze_concealment;
  dec_cont->picture_broken = 0;
  dec_cont->decoder.refbu_pred_hits = 0;

  if (FifoInit(VP9DEC_MAX_PIC_BUFFERS, &dec_cont->fifo_out) != FIFO_OK) {
    DWLfree(dec_cont);
    (void)DWLRelease(dwl); /* SE_UPDATE - to fix coverity warning */
    return DEC_MEMFAIL;
  }

  if (FifoInit(VP9DEC_MAX_PIC_BUFFERS, &dec_cont->fifo_display) != FIFO_OK) {
	 DWLfree(dec_cont);
	 (void)DWLRelease(dwl); /* SE_UPDATE - to fix coverity warning */
	 return DEC_MEMFAIL;
   }

  mutex_init(&dec_cont->sync_out);   /* SE_UPDATE - using mutex instead of pthread_mutex */
  init_waitqueue_head(&dec_cont->sync_out_cv);

  DWLmemcpy(&dec_cont->hw_cfg, &hw_cfg, sizeof(DWLHwConfig));

  dec_cont->output_format = output_format;
  /* TODO this limit could later be given through the API. Dynamic reference
     frame allocation can allocate buffers dynamically up to this limit.
     Otherwise sw stops and waits for the free buffer to emerge to the fifo */
  dec_cont->dynamic_buffer_limit = VP9DEC_DYNAMIC_PIC_LIMIT;

  /* return new instance to application */
  *dec_inst = (Vp9DecInst)dec_cont;

  return DEC_OK;
}

void Vp9DecRelease(Vp9DecInst dec_inst) {

  struct Vp9DecContainer *dec_cont = (struct Vp9DecContainer *)dec_inst;
  const void *dwl;

  /* Check for valid decoder instance */
  if (dec_cont == NULL || dec_cont->checksum != dec_cont) {
    return;
  }

  dwl = dec_cont->dwl;

  if (dec_cont->asic_running) {
    DWLDisableHw(dwl, dec_cont->core_id, 1 * 4, 0); /* stop HW */
    DWLReleaseHw(dwl, dec_cont->core_id);           /* release HW lock */
    dec_cont->asic_running = 0;
  }

  Vp9AsicReleaseMem(dec_cont);
  Vp9AsicReleaseFilterBlockMem(dec_cont);
  Vp9AsicReleasePictures(dec_cont);

  if (dec_cont->fifo_out) FifoRelease(dec_cont->fifo_out);
  if (dec_cont->fifo_display) FifoRelease(dec_cont->fifo_display);

//pthread_cond_destroy(&dec_cont->sync_out_cv);
  mutex_destroy(&dec_cont->sync_out); /* SE_UPDATE - using mutex instead of pthread_mutex */

  dec_cont->checksum = NULL;
  DWLfree(dec_cont);

  {
    i32 dwlret = DWLRelease(dwl);

    ASSERT(dwlret == DWL_OK);
    (void)dwlret;
  }

  return;
}

enum DecRet Vp9DecGetInfo(Vp9DecInst dec_inst, struct Vp9DecInfo *dec_info) {
  const struct Vp9DecContainer *dec_cont = (struct Vp9DecContainer *)dec_inst;

  if (dec_inst == NULL || dec_info == NULL) {
    return DEC_PARAM_ERROR;
  }

  /* Check for valid decoder instance */
  if (dec_cont->checksum != dec_cont) {
    return DEC_NOT_INITIALIZED;
  }

  if (dec_cont->dec_stat == VP9DEC_INITIALIZED) {
    return DEC_HDRS_NOT_RDY;
  }

  dec_info->vp_version = dec_cont->decoder.vp_version;
  dec_info->vp_profile = dec_cont->decoder.vp_profile;
  dec_info->output_format = dec_cont->output_format;

  /* Fragments have 8 pixels */
  dec_info->coded_width = dec_cont->decoder.width;
  dec_info->coded_height = dec_cont->decoder.height;
  dec_info->frame_height = NEXT_MULTIPLE(dec_cont->decoder.height, 8);
  if (dec_cont->output_format == DEC_OUT_FRM_RASTER_SCAN)
    dec_info->frame_width = NEXT_MULTIPLE(dec_cont->decoder.width, 16);
  else
    dec_info->frame_width = NEXT_MULTIPLE(dec_cont->decoder.width, 8);

  dec_info->scaled_width = dec_cont->decoder.scaled_width;
  dec_info->scaled_height = dec_cont->decoder.scaled_height;
  dec_info->dpb_mode = DEC_DPB_FRAME;

  return DEC_OK;
}

enum DecRet Vp9DecDecode(Vp9DecInst dec_inst, const struct Vp9DecInput *input,
                         struct Vp9DecOutput *output) {
  struct Vp9DecContainer *dec_cont = (struct Vp9DecContainer *)dec_inst;
  i32 ret;
  /* Check that function input parameters are valid */
  if (input == NULL || output == NULL || dec_inst == NULL) {
    return DEC_PARAM_ERROR;
  }

  /* Check for valid decoder instance */
  if (dec_cont->checksum != dec_cont) {
    return DEC_NOT_INITIALIZED;
  }

  if ((input->data_len > DEC_X170_MAX_STREAM) ||
      X170_CHECK_VIRTUAL_ADDRESS(input->stream) ||
      X170_CHECK_BUS_ADDRESS(input->stream_bus_address)) {
    return DEC_PARAM_ERROR;
  }

  ret = VP9SyncAndOutput(dec_cont);
  if (ret) return ret;

  if (dec_cont->dec_stat == VP9DEC_NEW_HEADERS) {
    if (Vp9AsicAllocatePictures(dec_cont) != 0) {
      return DEC_MEMFAIL;
    }
    dec_cont->dec_stat = VP9DEC_DECODING;
  } else if (input->data_len) {
    /* Decode SW part of the frame */
    ret = Vp9DecodeHeaders(dec_cont, input);
    if (ret) return ret;
  }
  /* missing picture, conceal */
  else {
    if (dec_cont->force_intra_freeze || dec_cont->prev_is_key) {
      dec_cont->decoder.probs_decoded = 0;
      Vp9Freeze(dec_cont);
      return DEC_PIC_DECODED;
    }
  }

  /* Get free picture buffer */
  ret = Vp9GetRefFrm(dec_cont);
  if (ret) return ret;

  /* Verify we have enough auxilary buffer for filter tile edge data. */
  ret = Vp9AsicAllocateFilterBlockMem(dec_cont);
  if (ret) return ret;

  /* prepare asic */
  Vp9AsicProbUpdate(dec_cont);

  Vp9AsicInitPicture(dec_cont);

  Vp9AsicStrmPosUpdate(dec_cont, input->stream_bus_address, input->data_len);
  /* run the hardware */
  Vp9AsicRun(dec_cont);
  return DEC_PIC_DECODED;
}

u32 Vp9CheckSupport(struct Vp9DecContainer *dec_cont) {

  DWLHwConfig hw_config;

  /* SE_UPDATE - fixing coverity warning */
  DWLmemset(&hw_config, 0, sizeof(hw_config));

  if (dec_cont == NULL)
    return HANTRO_NOK;

  DWLReadAsicConfig(&hw_config);

  if ((dec_cont->asic_buff->width > hw_config.max_dec_pic_width) ||
      (dec_cont->asic_buff->height > hw_config.max_dec_pic_height) ||
      (dec_cont->asic_buff->width < MIN_PIC_WIDTH) ||
      (dec_cont->asic_buff->height < MIN_PIC_HEIGHT)) {
    return HANTRO_NOK;
  }

  return HANTRO_OK;
}

void Vp9Freeze(struct Vp9DecContainer *dec_cont) {
  /* Rollback entropy probabilities if refresh is not set */
  if (dec_cont->decoder.probs_decoded &&
      dec_cont->decoder.refresh_entropy_probs == HANTRO_FALSE) {
    DWLmemcpy(&dec_cont->decoder.entropy, &dec_cont->decoder.entropy_last,
              sizeof(struct Vp9EntropyProbs));
  }
  /* lost accumulated coeff prob updates -> force video freeze until next
   * keyframe received */
  else if (dec_cont->prob_refresh_detected) {
    dec_cont->force_intra_freeze = 1;
  }

  dec_cont->picture_broken = 1;

  /* TODO Reset mv memory if needed? */
}

i32 Vp9DecodeHeaders(struct Vp9DecContainer *dec_cont,
                     const struct Vp9DecInput *input) {
  i32 ret;
  struct DecAsicBuffers *asic_buff = dec_cont->asic_buff;
  struct Vp9Decoder *dec = &dec_cont->decoder;

  dec_cont->prev_is_key = dec->key_frame;
  dec->prev_is_key_frame = dec->key_frame;
  dec->prev_show_frame = dec->show_frame;
  dec->probs_decoded = 0;

  /* decode frame tag */
  ret = Vp9DecodeFrameTag(input->stream, input->data_len, dec_cont);
  if (ret != HANTRO_OK) {
    if (dec_cont->pic_number == 1 || dec_cont->dec_stat != VP9DEC_DECODING)
      return DEC_STRM_ERROR;
    else {
      Vp9Freeze(dec_cont);
      return DEC_PIC_DECODED;
    }
  } else if (ret == HANTRO_OK && dec->show_existing_frame) {
    asic_buff->out_buffer_i =
        Vp9BufferQueueGetRef(dec_cont->bq, dec->show_existing_frame_index);
    Vp9BufferQueueAddRef(dec_cont->bq, asic_buff->out_buffer_i);
    Vp9SetupPicToOutput(dec_cont);
    asic_buff->out_buffer_i = -1;
    Vp9PicToOutput(dec_cont);
    return DEC_PIC_DECODED;
  }
  /* Decode frame header (now starts bool coder as well) */
  ret = Vp9DecodeFrameHeader(input->stream + dec->frame_tag_size,
                             input->data_len - dec->frame_tag_size,
                             &dec_cont->bc, &dec_cont->decoder);
  if (ret != HANTRO_OK) {
    if (dec_cont->pic_number == 1 || dec_cont->dec_stat != VP9DEC_DECODING)
      return DEC_STRM_ERROR;
    else {
      Vp9Freeze(dec_cont);
      return DEC_PIC_DECODED;
    }
  }
  /* flag the stream as non "error-resilient" */
  else if (dec->refresh_entropy_probs)
    dec_cont->prob_refresh_detected = 1;

  ret = Vp9SetPartitionOffsets(input->stream, input->data_len,
                               &dec_cont->decoder);
  /* ignore errors in partition offsets if HW error concealment used
   * (assuming parts of stream missing -> partition start offsets may
   * be larger than amount of stream in the buffer) */
  if (ret != HANTRO_OK) {
    if (dec_cont->pic_number == 1 || dec_cont->dec_stat != VP9DEC_DECODING)
      return DEC_STRM_ERROR;
    else {
      Vp9Freeze(dec_cont);
      return DEC_PIC_DECODED;
    }
  }

  asic_buff->width = NEXT_MULTIPLE(dec->width, 8);
  asic_buff->height = NEXT_MULTIPLE(dec->height, 8);
  /* If the frame dimensions are not supported by HW,
     release allocated picture buffers and return error */
  if (((dec_cont->width != dec->width) || (dec_cont->height != dec->height)) &&
      (Vp9CheckSupport(dec_cont) != HANTRO_OK)) {
    /* SE_UPDATE avoid freeing up here since cleanup is handled at the end in streaming engine */
    //Vp9AsicReleaseFilterBlockMem(dec_cont);
    //Vp9AsicReleasePictures(dec_cont);
    dec_cont->dec_stat = VP9DEC_INITIALIZED;
    return DEC_STREAM_NOT_SUPPORTED;
  }

  dec_cont->width = dec->width;
  dec_cont->height = dec->height;

  if (dec_cont->dec_stat == VP9DEC_INITIALIZED) {
    dec_cont->dec_stat = VP9DEC_NEW_HEADERS;
    return DEC_HDRS_RDY;
  }

  /* If we are here and dimensions are still 0, it means that we have
   * yet to decode a valid keyframe, in which case we must give up. */
  if (dec_cont->width == 0 || dec_cont->height == 0) {
    return DEC_STRM_PROCESSED;
  }

  /* If output picture is broken and we are not decoding a base frame,
   * don't even start HW, just output same picture again. */
  if (!dec->key_frame && dec_cont->picture_broken &&
      (dec_cont->intra_freeze || dec_cont->force_intra_freeze)) {
    Vp9Freeze(dec_cont);
    return DEC_PIC_DECODED;
  }

  return DEC_OK;
}
