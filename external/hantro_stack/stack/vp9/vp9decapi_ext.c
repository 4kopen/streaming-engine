/************************************************************************
Copyright (C) 2003-2014 STMicroelectronics. All Rights Reserved.

This file is part of the Streaming Engine.

Streaming Engine is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License version 2 as
published by the Free Software Foundation.

Streaming Engine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with Streaming Engine; see the file COPYING.  If not, write to the Free
Software Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307,
USA.

The Streaming Engine Library may alternatively be licensed under a
proprietary license from ST.
************************************************************************/

/* SE UPDATE - new file added to add additional apis inorder to map buffers required by vp9 decode to
    streaming engine decode buffer manager class */

#include "basetype.h"
#include "decapicommon.h"
#include "fifo.h"
#include "vp9decapi.h"
#include "version.h"

#include "dwl.h"
#include "dwl_ext.h"
#include "regdrv.h"

#include "vp9hwd_container.h"
#include "vp9hwd_asic.h"
#include "vp9hwd_headers.h"
#include "vp9hwd_output.h"
#include "sw_util.h"
#include "osinline.h"

void Vp9RegisterClientPtr(Vp9DecInst dec_inst, void *ptr)
{
    struct Vp9DecContainer *dec_cont = (struct Vp9DecContainer *)dec_inst;
    /* Check for valid decoder instance */
    if (dec_cont == NULL || dec_cont->checksum != dec_cont)
    {
        return;
    }
    DWLRegisterCodecPtr(dec_cont->dwl, ptr);
}

enum DecRet Vp9ExecuteClientMemoryInit(Vp9DecInst dec_inst)
{
    return DEC_OK;
}

void Vp9DecGetDecodeBufferPtr(Vp9DecInst dec_inst, unsigned int index, void **ptr)
{
    struct Vp9DecContainer *dec_cont = (struct Vp9DecContainer *)dec_inst;
    if (dec_cont == NULL || dec_cont->checksum != dec_cont)
    {
        return;
    }
    DWLGetDecodeBufferPtr(dec_cont->dwl, index, ptr);
}

/*  SE_UPDATE - wrapper api added inorder to call OS_GetTimeInMicroSeconds, as we also capture
    HwDecodeDuration from LLD which is retrieved using DWLGetDecodeDuration api so its better to
    add wrapper api instead of modifying orignal hantro VP9DecDecode()
*/
enum DecRet Vp9DecDecodeWrapper(Vp9DecInst dec_inst, const struct Vp9DecInput *input,
                                struct Vp9DecOutput *output, u32 *sw_decode_duration, u32 *hw_decode_duration)
{
    struct Vp9DecContainer *dec_cont = (struct Vp9DecContainer *)dec_inst;
    enum DecRet rv;
    u32 first_time = 0;
    u32 end_time = 0;

    /* Check that function input parameters are valid */
    if (input == NULL || output == NULL || dec_inst == NULL)
    {
        return DEC_PARAM_ERROR;
    }

    /* Check for valid decoder instance */
    if (dec_cont->checksum != dec_cont)
    {
        return DEC_NOT_INITIALIZED;
    }

    first_time = OS_GetTimeInMicroSeconds();
    rv = Vp9DecDecode(dec_inst, input, output);
    if (rv == DEC_PIC_DECODED)
    {
        end_time = OS_GetTimeInMicroSeconds();
        *sw_decode_duration = end_time - first_time;
    }
    else
    {
        *sw_decode_duration = 0;
    }

    DWLGetDecodeDuration(dec_cont->dwl, hw_decode_duration);

    return rv;
}


