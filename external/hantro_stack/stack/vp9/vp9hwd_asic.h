
/*
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301,
 * USA.
 */

#ifndef __VP9_ASIC_H__
#define __VP9_ASIC_H__

#include "basetype.h"
#include "vp9hwd_container.h"
#include "regdrv.h"

#define DEC_8190_ALIGN_MASK 0x07U

#define VP9HWDEC_HW_RESERVED 0x0100
#define VP9HWDEC_SYSTEM_ERROR 0x0200
#define VP9HWDEC_SYSTEM_TIMEOUT 0x0300

void Vp9AsicInit(struct Vp9DecContainer *dec_cont);
i32 Vp9AsicAllocateMem(struct Vp9DecContainer *dec_cont);
void Vp9AsicReleaseMem(struct Vp9DecContainer *dec_cont);
i32 Vp9AsicAllocateFilterBlockMem(struct Vp9DecContainer *dec_cont);
void Vp9AsicReleaseFilterBlockMem(struct Vp9DecContainer *dec_cont);
i32 Vp9AsicAllocatePictures(struct Vp9DecContainer *dec_cont);
void Vp9AsicReleasePictures(struct Vp9DecContainer *dec_cont);

i32 Vp9AllocateFrame(struct Vp9DecContainer *dec_cont, u32 index);
i32 Vp9ReallocateFrame(struct Vp9DecContainer *dec_cont, u32 index);
void Vp9AsicInitPicture(struct Vp9DecContainer *dec_cont);
void Vp9AsicStrmPosUpdate(struct Vp9DecContainer *dec_cont, u32 bus_address,
                          u32 data_len);
u32 Vp9AsicRun(struct Vp9DecContainer *dec_cont);
u32 Vp9AsicSync(struct Vp9DecContainer *dec_cont);

void Vp9AsicProbUpdate(struct Vp9DecContainer *dec_cont);

void Vp9UpdateRefs(struct Vp9DecContainer *dec_cont, u32 corrupted);

i32 Vp9GetRefFrm(struct Vp9DecContainer *dec_cont);
void Vp9UpdateProbabilities(struct Vp9DecContainer *dec_cont);

#endif /* __VP9_ASIC_H__ */
