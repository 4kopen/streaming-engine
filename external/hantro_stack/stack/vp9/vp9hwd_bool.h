
/*
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301,
 * USA.
 */

#ifndef __VP9_BOOL_H__
#define __VP9_BOOL_H__

#include "basetype.h"

#if 0
#include <stdio.h>
#define STREAM_TRACE(x, y) printf("%-30s-%9d\n", x, y);
#define VP9DEC_DEBUG(x) printf x
#else
#define STREAM_TRACE(x, y)
#define VP9DEC_DEBUG(x)
#endif

#define CHECK_END_OF_STREAM(s) \
  if ((s) == END_OF_STREAM) return (s)

struct VpBoolCoder {
  u32 lowvalue;
  u32 range;
  u32 value;
  i32 count;
  u32 pos;
  const u8 *buffer;
  u32 BitCounter;
  u32 stream_end_pos;
  u32 strm_error;
};

extern void Vp9BoolStart(struct VpBoolCoder *bc, const u8 *buffer, u32 len);
extern u32 Vp9DecodeBool(struct VpBoolCoder *bc, i32 probability);
extern u32 Vp9DecodeBool128(struct VpBoolCoder *bc);
extern void Vp9BoolStop(struct VpBoolCoder *bc);

u32 Vp9DecodeSubExp(struct VpBoolCoder *bc, u32 k, u32 num_syms);
u32 Vp9ReadBits(struct VpBoolCoder *br, i32 bits);

#endif /* __VP9_BOOL_H__ */
