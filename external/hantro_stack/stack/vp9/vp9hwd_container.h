
/*
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301,
 * USA.
 */
/************************************************************************
Copyright (C) 2003-2014 STMicroelectronics. All Rights Reserved.
************************************************************************/

#ifndef VP9HWD_CONTAINER_H
#define VP9HWD_CONTAINER_H

#include "basetype.h"
#include "commonvp9.h"
#include "../config/deccfg.h"
#include "decppif.h"
#include "dwl.h"
#include "dwl_ext.h"
#include "fifo.h"
#include "sw_debug.h"
#include "vp9decapi.h"
#include "vp9hwd_bool.h"
#include "vp9hwd_buffer_queue.h"
#include "vp9hwd_decoder.h"

#include <linux/sched.h>
#include <linux/wait.h>

#define VP9DEC_UNINITIALIZED 0U
#define VP9DEC_INITIALIZED 1U
#define VP9DEC_NEW_HEADERS 3U
#define VP9DEC_DECODING 4U
#define VP9DEC_END_OF_STREAM 5U

#define VP9DEC_MAX_PIC_BUFFERS 16
#define VP9DEC_DYNAMIC_PIC_LIMIT 9

#define VP9_UNDEFINED_BUFFER (i32)(-1)

struct PicCallbackArg {
  u32 display_number;
  i32 index; /* Buffer index of the output buffer. */
  i32 index_a;
  i32 index_g;
  i32 index_p;
  u32 show_frame;
  FifoInst fifo_out;        /* Output FIFO instance. */
  struct Vp9DecPicture pic; /* Information needed for outputting the frame. */
};

/* asic interface */
struct DecAsicBuffers {
  u32 width, height;

  struct DWLLinearMem prob_tbl;
  struct DWLLinearMem ctx_counters;
  struct DWLLinearMem segment_map[2];
  struct DWLLinearMem tile_info;
  struct DWLLinearMem filter_mem;
  struct DWLLinearMem bsd_control_mem;
  u32 display_index[VP9DEC_MAX_PIC_BUFFERS];

  /* Concurrent access to following picture arrays is controlled indirectly
   * through buffer queue. */
  struct DWLLinearMem pictures[VP9DEC_MAX_PIC_BUFFERS];
  struct DWLLinearMem pictures_c[VP9DEC_MAX_PIC_BUFFERS];
  struct DWLLinearMem dir_mvs[VP9DEC_MAX_PIC_BUFFERS]; /* colocated MVs */
  struct DWLLinearMem raster_luma[VP9DEC_MAX_PIC_BUFFERS];
  struct DWLLinearMem raster_chroma[VP9DEC_MAX_PIC_BUFFERS];
#ifdef DOWN_SCALER
  struct DWLLinearMem dscale_luma[VP9DEC_MAX_PIC_BUFFERS];
  struct DWLLinearMem dscale_chroma[VP9DEC_MAX_PIC_BUFFERS];
#endif
  void *decodeBufferPtr[VP9DEC_MAX_PIC_BUFFERS]; /* SE_UPDATE - pointer to store decode buffer manager pointer currently used */

  struct Vp9DecPicture picture_info[VP9DEC_MAX_PIC_BUFFERS];
  i32 reference_list[VP9_REF_LIST_SIZE]; /* Contains indexes to full list of
                                            picture */

  /* Indexes for picture buffers in pictures[] array */
  i32 out_buffer_i;
  i32 prev_out_buffer_i;

  u32 whole_pic_concealed;
  u32 disable_out_writing;
  u32 segment_map_size;
  u32 partition1_base;
  u32 partition1_bit_offset;
  u32 partition2_base;
};

struct Vp9DecContainer {
  const void *checksum;
  u32 dec_mode;
  u32 dec_stat;
  u32 pic_number;
  u32 asic_running;
  u32 width;
  u32 height;
  u32 vp9_regs[DEC_X170_REGISTERS];
  struct DecAsicBuffers asic_buff[1];
  const void *dwl; /* struct DWL instance */
  i32 core_id;

  struct Vp9Decoder decoder;
  struct VpBoolCoder bc;

  u32 picture_broken;
  u32 intra_freeze;
  u32 out_count;
  u32 num_buffers;
  u32 active_segment_map;

  BufferQueue bq;

  u32 intra_only;
  u32 conceal;
  u32 prev_is_key;
  u32 force_intra_freeze;
  u32 prob_refresh_detected;
  struct PicCallbackArg pic_callback_arg[MAX_ASIC_CORES];
  /* Output related variables. */
  FifoInst fifo_out;     /* Fifo for output indices. */
  FifoInst fifo_display; /* Store of output indices for display reordering. */
  u32 display_number;
  /* SE_UPDATE - using mutex instead of pthread_mutex */
  struct mutex sync_out; /* protects access to pictures in output fifo. */
  wait_queue_head_t sync_out_cv;

  DWLHwConfig hw_cfg;

  enum DecPictureFormat output_format;
  u32 dynamic_buffer_limit; /* limit for dynamic frame buffer count */

#ifdef DOWN_SCALER
  u32 down_scale_enabled;
  u32 down_scale_x_shift;
  u32 down_scale_y_shift;
#endif
};

#endif /* #ifdef VP9HWD_CONTAINER_H */
