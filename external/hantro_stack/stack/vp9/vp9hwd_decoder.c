
/*
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301,
 * USA.
 */

#include "basetype.h"
#include "dwl.h"
#include "dwl_ext.h"
#include "vp9hwd_headers.h"
#include "vp9hwd_probs.h"
#include "vp9hwd_container.h"

void Vp9ResetDecoder(struct Vp9Decoder *dec, struct DecAsicBuffers *asic_buff) {
  i32 i;

  /* Clear all previous segment data */
  DWLmemset(dec->segment_feature_enable, 0,
            sizeof(dec->segment_feature_enable));
  DWLmemset(dec->segment_feature_data, 0, sizeof(dec->segment_feature_data));

  if (asic_buff->segment_map[0].virtual_address)
    DWLmemset(asic_buff->segment_map[0].virtual_address, 0,
              asic_buff->segment_map_size);
  if (asic_buff->segment_map[1].virtual_address)
    DWLmemset(asic_buff->segment_map[1].virtual_address, 0,
              asic_buff->segment_map_size);
  Vp9ResetProbs(dec);

  for (i = 0; i < NUM_REF_FRAMES; i++) {
    dec->ref_frame_map[i] = i;
  }
}
