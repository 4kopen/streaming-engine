
/*
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301,
 * USA.
 */

#ifndef __VP9_HEADERS_H__
#define __VP9_HEADERS_H__

#include "basetype.h"

#include "vp9hwd_container.h"
#include "vp9hwd_decoder.h"
#include "vp9hwd_bool.h"

u32 Vp9DecodeFrameTag(const u8 *strm, u32 data_len,
                      struct Vp9DecContainer *dec_cont);
u32 Vp9SetPartitionOffsets(const u8 *stream, u32 len, struct Vp9Decoder *dec);
u32 Vp9DecodeFrameHeader(const u8 *strm, u32 strm_len, struct VpBoolCoder *bc,
                         struct Vp9Decoder *dec);

#endif
