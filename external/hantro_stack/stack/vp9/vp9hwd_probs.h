
/*
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301,
 * USA.
 */

#ifndef __VP9_PROBS_H__
#define __VP9_PROBS_H__

#include "basetype.h"
#include "vp9hwd_bool.h"
#include "vp9hwd_decoder.h"

void Vp9ResetProbs(struct Vp9Decoder* dec);
void Vp9GetProbs(struct Vp9Decoder* dec);
void Vp9StoreProbs(struct Vp9Decoder* dec);
void Vp9StoreAdaptProbs(struct Vp9Decoder* dec);
void Vp9ComputeModRefProbs(struct Vp9Decoder* dec);
u32 Vp9DecodeMvUpdate(struct VpBoolCoder* bc, struct Vp9Decoder* dec);
u32 Vp9DecodeCoeffUpdate(
    struct VpBoolCoder* bc,
    u8 prob_coeffs[BLOCK_TYPES][REF_TYPES][COEF_BANDS][PREV_COEF_CONTEXTS]
                  [ENTROPY_NODES_PART1]);
void Vp9AdaptCoefProbs(struct Vp9Decoder* cm);
void Vp9AdaptModeProbs(struct Vp9Decoder* cm);
void Vp9AdaptModeContext(struct Vp9Decoder* cm);
vp9_prob Vp9ReadProbDiffUpdate(struct VpBoolCoder* bc, int oldp);

#endif /* __VP9_PROBS_H__ */
