/************************************************************************
Copyright (C) 2003-2014 STMicroelectronics. All Rights Reserved.

This file is part of the Streaming Engine.

Streaming Engine is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License version 2 as
published by the Free Software Foundation.

Streaming Engine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with Streaming Engine; see the file COPYING.  If not, write to the Free
Software Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307,
USA.

The Streaming Engine Library may alternatively be licensed under a
proprietary license from ST.
************************************************************************/

#include <linux/of.h>
#include <linux/sysfs.h>
#include <linux/stat.h>
#include <linux/platform_device.h>
#include <linux/reset.h>
#include <linux/pm_runtime.h>
#include <linux/clk.h>

#include "osdev_time.h"
#include "osdev_mem.h"
#include "osdev_int.h"

#include "hva_registers.h"
#include "h264_encode.h"
#include "utilisation.h"

static DEFINE_MUTEX(H264EncoderGlobalLock);

struct semaphore                      h264EncInstances;
struct semaphore                      h264CmdFifoDepth;

//Manage list of clients connected and resource available
static H264EncodeHardCodecContext_t   *ClientContextList[MAX_HVA_ENCODE_INSTANCE];
static char h264enc_irq_names[][16] = { "h264enc_HVA_ITS", "h264enc_HVA_ERR" };

extern struct HvaDriverData *pDriverData;

#define SYSFS_DECLARE_UINT( VarName )                                                              \
static unsigned int VarName = 0;                                                                   \
static ssize_t SysfsShow_##VarName( struct device *dev, struct device_attribute *attr, char *buf ) \
{                                                                                                  \
    (void)dev; \
    (void)attr; \
    return snprintf(buf, 128, "%d\n", VarName);                                                    \
}                                                                                                  \
                                                                                                   \
static DEVICE_ATTR(VarName, S_IRUGO, SysfsShow_##VarName, NULL)

// For displaying values in Semaphores. For monitoring and Debug use only
#define SYSFS_DECLARE_SEMAPHORE( VarName )                                                         \
static ssize_t SysfsShow_##VarName( struct device *dev, struct device_attribute *attr, char *buf ) \
{                                                                                                  \
    unsigned int count;                                                                            \
    unsigned long flags;                                                                           \
    (void)dev; \
    (void)attr; \
    raw_spin_lock_irqsave( &VarName.lock, flags );                                                 \
    count = VarName.count;                                                                         \
    raw_spin_unlock_irqrestore( &VarName.lock, flags );                                            \
                                                                                                   \
    return snprintf(buf, 128, "%d\n", count);                                                      \
}                                                                                                  \
                                                                                                   \
static DEVICE_ATTR(VarName, S_IRUGO, SysfsShow_##VarName, NULL)


SYSFS_DECLARE_UINT(h264_frames_queued);
SYSFS_DECLARE_UINT(h264_frames_completed);
SYSFS_DECLARE_UINT(hw_encode_duration);
SYSFS_DECLARE_UINT(cpu_encode_duration);
SYSFS_DECLARE_UINT(WakeUpLatency);
SYSFS_DECLARE_UINT(WakeUpLatencyAvg);
SYSFS_DECLARE_UINT(WakeUpLatencyMax);

SYSFS_DECLARE_UTILISATION(hw_utilisation);
SYSFS_DECLARE_UTILISATION(sw_utilisation);

SYSFS_DECLARE_SEMAPHORE(h264EncInstances);
SYSFS_DECLARE_SEMAPHORE(h264CmdFifoDepth);

inline unsigned int HVA_ALIGNED_256(unsigned int address)
{
	address = ((address + 0xff) & 0xffffff00);
	return address;
}

inline unsigned int HVA_SET_BUFFER_AS_SDRAM(unsigned int address)
{
	return address;
}

/* Reverse HVA_SET_BUFFER_AS_SDRAM(). */
inline unsigned int HVA_SDRAM_BUFFER_TO_PHYS_ADDR(unsigned int address)
{
	return address;
}

inline unsigned int HVA_SET_BUFFER_AS_ESRAM(unsigned int address)
{
	address += 1;
	return address;
}

int H264HwEncodeProbe(struct device *dev)
{
	unsigned int maxInstance = UINT_MAX;
	int result;

	if (dev->of_node) {
		if (of_property_read_u32(dev->of_node, "st,max_enc", &maxInstance)) {
			dev_dbg(pDriverData->dev, "st,max_enc property not found => no restriction on the number of encode instance\n");
			maxInstance = UINT_MAX;
		}
	} else {
		dev_err(pDriverData->dev, "Error: device tree not working\n");
		return -1;
	}

	//create the attribute files
	result = device_create_file(dev, &dev_attr_h264_frames_queued);
	if (result) {
		dev_err(pDriverData->dev, "Error: device_create_file failed for h264_frames_queued (%d)\n", result);
		return result;
	}

	result = device_create_file(dev, &dev_attr_h264_frames_completed);
	if (result) {
		dev_err(pDriverData->dev, "Error: device_create_file failed for h264_frames_completed (%d)\n", result);
		goto devcreate_fail1;
	}

	result = device_create_file(dev, &dev_attr_hw_encode_duration);
	if (result) {
		dev_err(pDriverData->dev, "Error: device_create_file failed for hw_encode_duration (%d)\n", result);
		goto devcreate_fail2;
	}

	result = device_create_file(dev, &dev_attr_cpu_encode_duration);
	if (result) {
		dev_err(pDriverData->dev, "Error: device_create_file failed for cpu_encode_duration (%d)\n", result);
		goto devcreate_fail3;
	}

	result = device_create_file(dev, &dev_attr_WakeUpLatency);
	if (result) {
		dev_err(pDriverData->dev, "Error: device_create_file failed for WakeUpLatency (%d)\n", result);
		goto devcreate_fail4;
	}

	result = device_create_file(dev, &dev_attr_WakeUpLatencyAvg);
	if (result) {
		dev_err(pDriverData->dev, "Error: device_create_file failed for WakeUpLatencyAvg (%d)\n", result);
		goto devcreate_fail5;
	}

	result = device_create_file(dev, &dev_attr_WakeUpLatencyMax);
	if (result) {
		dev_err(pDriverData->dev, "Error: device_create_file failed for WakeUpLatencyMax (%d)\n", result);
		goto devcreate_fail6;
	}
	result = device_create_file(dev, &dev_attr_hw_utilisation);
	if (result) {
		dev_err(pDriverData->dev, "Error: device_create_file failed for hw_utilisation (%d)\n", result);
		goto devcreate_fail7;
	}
	result = device_create_file(dev, &dev_attr_sw_utilisation);
	if (result) {
		dev_err(pDriverData->dev, "Error: device_create_file failed for sw_utilisation (%d)\n", result);
		goto devcreate_fail8;
	}

	maxInstance = min(MAX_HVA_ENCODE_INSTANCE, maxInstance);
	dev_info(pDriverData->dev, "Setting hva max instance number to %d\n", maxInstance);
	sema_init(&h264EncInstances, maxInstance);

	pDriverData->h264EncInstanceNb = 0;
	pDriverData->globalTaskId = 0;
	pDriverData->searchWindowBufPhysAddress = 0;
	pDriverData->localRecBufPhysAddress = 0;
	pDriverData->contextMBBufPhysAddress = 0;
	pDriverData->cabacContextBufPhysAddress = 0;
	pDriverData->HvaSramBase = 0;
	pDriverData->newHvaSramFreePhysAddress = 0;
	pDriverData->HvaSramSize = 0;

	// These must be created AFTER the semaphores are initialized
	result = device_create_file(dev, &dev_attr_h264EncInstances);
	if (result) {
		dev_err(pDriverData->dev, "Error: device_create_file failed for h264EncInstances (%d)\n", result);
		goto devcreate_fail9;
	}

	return result;

devcreate_fail9:
	device_remove_file(dev, &dev_attr_sw_utilisation);
devcreate_fail8:
	device_remove_file(dev, &dev_attr_hw_utilisation);
devcreate_fail7:
	device_remove_file(dev, &dev_attr_WakeUpLatencyMax);
devcreate_fail6:
	device_remove_file(dev, &dev_attr_WakeUpLatencyAvg);
devcreate_fail5:
	device_remove_file(dev, &dev_attr_WakeUpLatency);
devcreate_fail4:
	device_remove_file(dev, &dev_attr_cpu_encode_duration);
devcreate_fail3:
	device_remove_file(dev, &dev_attr_hw_encode_duration);
devcreate_fail2:
	device_remove_file(dev, &dev_attr_h264_frames_completed);
devcreate_fail1:
	device_remove_file(dev, &dev_attr_h264_frames_queued);

	return result;
}

void H264HwEncodeRemove(struct device *dev)
{
	device_remove_file(dev, &dev_attr_h264EncInstances);

	device_remove_file(dev, &dev_attr_hw_utilisation);
	device_remove_file(dev, &dev_attr_sw_utilisation);

	//remove the attribute files
	device_remove_file(dev, &dev_attr_h264_frames_queued);
	device_remove_file(dev, &dev_attr_h264_frames_completed);
	device_remove_file(dev, &dev_attr_hw_encode_duration);
	device_remove_file(dev, &dev_attr_cpu_encode_duration);
	device_remove_file(dev, &dev_attr_WakeUpLatency);
	device_remove_file(dev, &dev_attr_WakeUpLatencyAvg);
	device_remove_file(dev, &dev_attr_WakeUpLatencyMax);
}

#define LA_SAMPLES 240 // Number of latency samples to average. Time period = LA_SAMPLES/number of frames encoded per sec
static unsigned long long LatencyTotal = 0;
static unsigned int LatencyFrame = 0;


// ////////////////////////////////////////////////////////////////////////////////
//
//    Prototypes

static OSDEV_InterruptHandlerEntrypoint(H264HwEncodeInterruptHandler);
static OSDEV_InterruptHandlerEntrypoint(H264HwEncodeErrorInterruptHandler);

static void H264HwEncodeLoadStaticParameters(H264EncodeHardCodecContext_t  *Context);
static void H264HwEncodeDebugOptionHack(H264EncodeHardFrameParams_t    *FrameParams,
                                        H264EncodeHardTaskDescriptor_t *pTaskDescriptor,
                                        unsigned int StreamId);
static H264EncodeHardStatus_t H264HwEncodeInterruptInstall(unsigned int StreamId);
static void                   H264HwEncodeInterruptUnInstall(unsigned int StreamId);
static H264EncodeHardStatus_t H264HwEncodeAllocateSingletonHwBuffers(unsigned int StreamId);
static void                   H264HwEncodeFreeSingletonHwBuffers(void);
static H264EncodeHardStatus_t H264HwEncodeAllocateContextHwBuffers(H264EncodeHardCodecContext_t  *Context ,
                                                                   uint32_t MaxFrameWidth, uint32_t MaxFrameHeight);
static void                   H264HwEncodeFreeContextHwBuffers(H264EncodeHardCodecContext_t  *Context);
static void H264HwEncodeLoadStreamContext(H264EncodeHardCodecContext_t  *Context);
static void H264HwEncodeDisplayTaskDescriptor(H264EncodeHardCodecContext_t  *Context);
static unsigned int H264HwEncodeBuildHVACommand(HVACmdType_t cmdType, H264EncodeHardCodecContext_t *Context,
                                                unsigned short taskId);
static H264EncodeHardStatus_t H264HwEncodeLaunchEncodeTask(H264EncodeHardCodecContext_t  *Context);
static H264EncodeHardStatus_t H264HwEncodeCompletion(H264EncodeHardCodecContext_t *Context);
static void H264HwEncodeUpdateContextAtEncodeEnd(H264EncodeHardCodecContext_t  *Context);
static void H264HwEncodeHVAStaticConfig(unsigned int StreamId);
static void H264HwEncodeCheckFramerate(H264EncodeHardCodecContext_t  *Context);
static void H264HwEncodeCheckBitrateAndCpbSize(H264EncodeHardCodecContext_t  *Context);
static void H264HwEncodeGetFrameDimensionFromProfile(H264EncodeHardInitParams_t *InitParams, uint32_t *maxFrameWidth,
                                                     uint32_t *maxFrameHeight);
static void H264HwEncodeBrcPreprocessing(H264EncodeHardCodecContext_t *Context,
                                         H264EncodeHardFrameParams_t *FrameParams);
static void H264HwEncodeBrcPostprocessing(H264EncodeHardCodecContext_t *Context);

#ifndef CONFIG_STM_VIRTUAL_PLATFORM /* VSOC WORKAROUND : inlining not currently supported in VSOC environment */
inline unsigned int HVA_ALIGNED_256(unsigned int address);
inline unsigned int HVA_SET_BUFFER_AS_SDRAM(unsigned int address);
inline unsigned int HVA_SET_BUFFER_AS_ESRAM(unsigned int address);
#else
unsigned int HVA_ALIGNED_256(unsigned int address);
unsigned int HVA_SET_BUFFER_AS_SDRAM(unsigned int address);
unsigned int HVA_SET_BUFFER_AS_ESRAM(unsigned int address);
#endif

//{{{  H264HwEncodeInit
//{{{  doxynote
/// \brief      Initialize HVA HW & allocate requested internal buffers
///
/// \return     H264EncodeHardStatus_t
//}}}
H264EncodeHardStatus_t H264HwEncodeInit(H264EncodeHardHandle_t        *Handle,
                                        H264EncodeHardInitParams_t    *InitParams)
{
	H264EncodeHardCodecContext_t  *Context = (H264EncodeHardCodecContext_t *) Handle;
	H264EncodeHardStatus_t status = H264ENCHARD_NO_ERROR;
	uint32_t i;
	uint8_t clientId;
	uint32_t maxFrameWidth;
	uint32_t maxFrameHeight;
	int result;
	unsigned int StreamId = InitParams->StreamId;

	dev_info(pDriverData->dev, "Stream 0x%x Hva encode init\n", StreamId);

	mutex_lock(&H264EncoderGlobalLock);

	if (down_timeout(&h264EncInstances, msecs_to_jiffies(1000)) != 0) {
		dev_err(pDriverData->dev, "Stream 0x%x Error: max hva encoder client reached\n", StreamId);
		status = H264ENCHARD_ERROR;
		goto endinit;
	}

	//Retrieve frame max dimension from Profile
	H264HwEncodeGetFrameDimensionFromProfile(InitParams, &maxFrameWidth, &maxFrameHeight);

	//perform "one shot" init like HW init if required
	if (pDriverData->h264EncInstanceNb == 0) {
		dev_dbg(pDriverData->dev, "Stream 0x%x Hva esram physical address is 0x%x and size is %u bytes\n", StreamId,
		        pDriverData->platformData.BaseAddress[1],
		        pDriverData->platformData.Size[1]);
		pDriverData->HvaSramBase = pDriverData->platformData.BaseAddress[1];
		pDriverData->newHvaSramFreePhysAddress = pDriverData->platformData.BaseAddress[1];
		pDriverData->HvaSramSize = pDriverData->platformData.Size[1];

		// Install interrupt handler
		status = H264HwEncodeInterruptInstall(StreamId);
		if (status != H264ENCHARD_NO_ERROR) {
			dev_err(pDriverData->dev, "Stream 0x%x Error: installation of interrupt handler failed\n", StreamId);
			goto fail_inth;
		}

		// Allocate mono instantiated buffers (no context required)
		// Physically contiguous Addresses required!!!
		status = H264HwEncodeAllocateSingletonHwBuffers(StreamId);
		if (status != H264ENCHARD_NO_ERROR) {
			dev_err(pDriverData->dev, "Stream 0x%x Error: allocation of singleton buffers failed\n", StreamId);
			goto fail_allocateshw;
		}

		pDriverData->errorInterruptStatus = H264ENCHARD_HW_NO_ERROR;
		pDriverData->spurious_irq_count = 0;

		sema_init(&h264CmdFifoDepth, HVA_CMD_FIFO_DEPTH);

		// These must be created AFTER the semaphores are initialized
		result = device_create_file(pDriverData->dev, &dev_attr_h264CmdFifoDepth);
		if (result) {
			dev_err(pDriverData->dev, "Stream 0x%x Error: device_create_file failed for h264CmdFifoDepth (%d)\n", StreamId,
			        result);
			goto fail_create_file;
		}

		// initialize client context to NULL
		for (i = 0; i < MAX_HVA_ENCODE_INSTANCE; i++) {
			ClientContextList[i] = NULL;
		}
	} else { dev_dbg(pDriverData->dev, "Stream 0x%x Hva encode already initialized\n", StreamId); }

	for (clientId = 0; clientId < MAX_HVA_ENCODE_INSTANCE; clientId++) {
		if (ClientContextList[clientId] == NULL) {
			break;
		}
	}

	// Check for error condition
	if (clientId == MAX_HVA_ENCODE_INSTANCE) {
		dev_err(pDriverData->dev, "Stream 0x%x Error: unable to find free client id\n", StreamId);
		status = H264ENCHARD_ERROR;
		goto endinitup;  // assuming that in this case h264EncInstanceNb != 0
	}

	ClientContextList[clientId] = Context;
	Context->clientId           = clientId;
	Context->StreamId           = StreamId; //TODO Save whole init params
	Context->transformState     = H264ENCHARD_RUNNING;

	sema_init(&Context->h264EncTask, 0);

	//Allocate multi instance internal buffers: 1 set per encode instance
	//Design choice to allocate the max size for internal buffers at init, bypassing need to reallocate for each size update
	status = H264HwEncodeAllocateContextHwBuffers(Context, maxFrameWidth, maxFrameHeight);
	if (status != H264ENCHARD_NO_ERROR) {
		dev_err(pDriverData->dev, "Stream 0x%x Error: allocation of multi instance buffers failed\n", StreamId);
		if (pDriverData->h264EncInstanceNb != 0) {
			goto endinitup;
		} else {
			goto fail_instance;
		}
	}

	//Fill task descriptor static parameters
	H264HwEncodeLoadStaticParameters(Context);

	pDriverData->h264EncInstanceNb++;

	//Initialize past bitstream size
	for (i = 0; i < 4; i++) {
		Context->PastBitstreamSize[i] = 0;
	}

	goto endinit;

fail_instance:
	device_remove_file(pDriverData->dev, &dev_attr_h264CmdFifoDepth);
fail_create_file:
	H264HwEncodeFreeSingletonHwBuffers();
fail_allocateshw:
	H264HwEncodeInterruptUnInstall(StreamId);
fail_inth:
endinitup:
	up(&h264EncInstances);

endinit:
	mutex_unlock(&H264EncoderGlobalLock);

	return status;
}

//{{{  H264HwEncodeClose
//{{{  doxynote
/// \brief      DeInitialize HVA
///
/// \return     H264EncodeHardStatus_t
//}}}
H264EncodeHardStatus_t H264HwEncodeClose(H264EncodeHardHandle_t Handle)
{
	H264EncodeHardCodecContext_t  *Context = (H264EncodeHardCodecContext_t *) Handle;
	unsigned int StreamId = Context->StreamId;

	dev_info(pDriverData->dev, "Stream 0x%x Hva encode close\n", StreamId);

	if (Context) {
		kfree(Context);
	}

	return H264ENCHARD_NO_ERROR;
}

void DumpZone(H264EncodeZoneParameter_t *Zone, unsigned int ZoneNb, int StreamId)
{
	int i = 0;

	dev_dbg(pDriverData->dev, "Stream 0x%x [ZONE PARAMETER]\n", StreamId);
	for (i = 0 ; i < ZoneNb; i++) {
		dev_dbg(pDriverData->dev, "Stream 0x%x |-[ZONE %d]\n", StreamId, i);
		dev_dbg(pDriverData->dev, "Stream 0x%x | |-maxBufferFullness[UP] = %u\n", StreamId, Zone[i].maxBufferFullness[UP]);
		dev_dbg(pDriverData->dev, "Stream 0x%x | |-maxBufferFullness[DOWN] = %u\n", StreamId,
		        Zone[i].maxBufferFullness[DOWN]);
		dev_dbg(pDriverData->dev, "Stream 0x%x | |-samplingPeriod = %u\n", StreamId, Zone[i].samplingPeriod);
		dev_dbg(pDriverData->dev, "Stream 0x%x | |-qpMin = %u\n", StreamId, Zone[i].qpMin);
		dev_dbg(pDriverData->dev, "Stream 0x%x | |-qpMax = %u\n", StreamId, Zone[i].qpMax);
		dev_dbg(pDriverData->dev, "Stream 0x%x | |-qpHysteresisMin[UP] = %u\n", StreamId, Zone[i].qpHysteresisMin[UP]);
		dev_dbg(pDriverData->dev, "Stream 0x%x | |-qpHysteresisMax[UP] = %u\n", StreamId, Zone[i].qpHysteresisMax[UP]);
		dev_dbg(pDriverData->dev, "Stream 0x%x | |-qpHysteresisMin[DOWN] = %u\n", StreamId, Zone[i].qpHysteresisMin[DOWN]);
		dev_dbg(pDriverData->dev, "Stream 0x%x | |-qpHysteresisMax[DOWN] = %u\n", StreamId, Zone[i].qpHysteresisMax[DOWN]);
		dev_dbg(pDriverData->dev, "Stream 0x%x |\n", StreamId);
	}
	dev_dbg(pDriverData->dev, "Stream 0x%x \n", StreamId);
}

void H264HwEncodeInitParamsBRC(H264EncodeHardCodecContext_t *Context)
{
	H264EncodeHardSequenceParams_t *SequenceParams = &Context->globalParameters;
	unsigned int CpbBufferSizeInFrame = 0;
	unsigned int BitratePerFrame = 0;
	unsigned int ZoneCount = 0;
	unsigned int ZoneLimitInFrame = 0;
	H264EncodeZoneParameter_t *Zone = Context->brc.zone;
	int qpMinUp = 0;
	int qpMaxUp = 0;
	int qpMinDown = 0;
	int qpMaxDown = 0;
	int StreamId = Context->StreamId;

	// BRC WINDOW SIZE PROCESSING PARAMETERS
	memset(Context->brc.lastPicSizeInBits, 0, sizeof(Context->brc.lastPicSizeInBits));
	Context->brc.lastGopSize = 1;

	// BRC SAMPLING PERIOD PARAMETERS
	Context->brc.samplingPeriod = BRC_WINDOW_SIZE;
	Context->brc.samplingQpUpdated = false;
	Context->brc.forceSampling = false;
	Context->brc.samplingQp = 0;
	Context->brc.samplingQpDelta = 0;
	memset(Context->brc.lastPicWithFillerData, 0, sizeof(Context->brc.lastPicWithFillerData));
	Context->brc.lastSkippedFrameCount = 0;

	// Brc zone parameters
	Context->brc.frameCountOnZone = 0;
	if (SequenceParams->framerateNum) {
		BitratePerFrame = (SequenceParams->bitRate * SequenceParams->framerateDen) / SequenceParams->framerateNum;
		dev_dbg(pDriverData->dev, "Stream 0x%x BitratePerFrame = %u\n", StreamId, BitratePerFrame);
		if (BitratePerFrame) {
			CpbBufferSizeInFrame = SequenceParams->cpbBufferSize / BitratePerFrame;
			dev_dbg(pDriverData->dev, "Stream 0x%x CpbBufferSizeInFrame = %u\n", StreamId, CpbBufferSizeInFrame);
		}
	}
	qpMinUp = H264_ENC_OPTIMAL_QP_MAX; //35
	qpMaxUp = SequenceParams->qpmax - 1; //50
	qpMinDown = qpMinUp - MAX_QP_HYSTERESIS; //20
	qpMaxDown = qpMaxUp - MAX_QP_HYSTERESIS; //35

	// Brc first zone
	ZoneLimitInFrame = BRC_CONVERGENCE_PERIOD;
	Zone[ZoneCount].maxBufferFullness[UP] = (ZoneLimitInFrame + 3) * BitratePerFrame;
	Zone[ZoneCount].maxBufferFullness[DOWN] = ZoneLimitInFrame * BitratePerFrame;
	Zone[ZoneCount].samplingPeriod = 1;
	Zone[ZoneCount].qpMin = H264_ENC_OPTIMAL_QP_MIN; //25
	Zone[ZoneCount].qpMax = SequenceParams->qpmax; //51
	Zone[ZoneCount].qpHysteresisMin[UP] = qpMinUp; //35
	Zone[ZoneCount].qpHysteresisMax[UP] = qpMaxUp; //50
	Zone[ZoneCount].qpHysteresisMin[DOWN] = qpMinDown; //20
	Zone[ZoneCount].qpHysteresisMax[DOWN] = qpMaxDown; //35
	ZoneCount++;

	// Brc second zone
	// This zone exists if its lower limit is less than 90% of cpb size
	if ((CpbBufferSizeInFrame * 9) > (10 * ZoneLimitInFrame)) {
		ZoneLimitInFrame = BRC_CONVERGENCE_PERIOD * 2;
		Zone[ZoneCount].maxBufferFullness[UP] = (ZoneLimitInFrame + 3) * BitratePerFrame;
		Zone[ZoneCount].maxBufferFullness[DOWN] = ZoneLimitInFrame * BitratePerFrame;
		Zone[ZoneCount].samplingPeriod = max(1U , BRC_WINDOW_SIZE / 3U);
		Zone[ZoneCount].qpMin = H264_ENC_OPTIMAL_QP_MIN; //25
		Zone[ZoneCount].qpMax = (H264_ENC_OPTIMAL_QP_MAX + 2 * SequenceParams->qpmax) / 3; //45
		Zone[ZoneCount].qpHysteresisMin[UP] = qpMinUp; //35
		Zone[ZoneCount].qpHysteresisMax[UP] = qpMaxUp; //50
		Zone[ZoneCount].qpHysteresisMin[DOWN] = (qpMinUp + 2 * qpMinDown) / 3; //25
		Zone[ZoneCount].qpHysteresisMax[DOWN] = (qpMaxUp + 2 * qpMaxDown) / 3; //40
		ZoneCount++;

		// Brc third zone
		// This zone exists if its lower limit is less than 90% of cpb size
		if ((CpbBufferSizeInFrame * 9) > (10 * ZoneLimitInFrame)) {
			ZoneLimitInFrame = BRC_CONVERGENCE_PERIOD * 4;
			Zone[ZoneCount].maxBufferFullness[UP] = (ZoneLimitInFrame + 3) * BitratePerFrame;
			Zone[ZoneCount].maxBufferFullness[DOWN] = ZoneLimitInFrame * BitratePerFrame;
			Zone[ZoneCount].samplingPeriod = max(1U , BRC_WINDOW_SIZE / 2U);
			Zone[ZoneCount].qpMin = H264_ENC_OPTIMAL_QP_MIN; //25
			Zone[ZoneCount].qpMax = (2 * H264_ENC_OPTIMAL_QP_MAX + SequenceParams->qpmax) / 3; //40
			Zone[ZoneCount].qpHysteresisMin[UP] = qpMinUp; //35
			Zone[ZoneCount].qpHysteresisMax[UP] = qpMaxUp; //50
			Zone[ZoneCount].qpHysteresisMin[DOWN] = (2 * qpMinUp + qpMinDown) / 3; //30
			Zone[ZoneCount].qpHysteresisMax[DOWN] = (2 * qpMaxUp + qpMaxDown) / 3; //45
			ZoneCount++;

			// Brc fourth zone
			// This zone exists if its lower limit is less than 90% of cpb size
			if ((CpbBufferSizeInFrame * 9) > (10 * ZoneLimitInFrame)) {
				Zone[ZoneCount].maxBufferFullness[UP] = SequenceParams->cpbBufferSize; // Filler data detected
				Zone[ZoneCount].maxBufferFullness[DOWN] = (CpbBufferSizeInFrame * 9 / 10) * BitratePerFrame;
				Zone[ZoneCount].samplingPeriod = BRC_WINDOW_SIZE;
				Zone[ZoneCount].qpMin = H264_ENC_OPTIMAL_QP_MIN; //25
				Zone[ZoneCount].qpMax = H264_ENC_OPTIMAL_QP_MAX; //35
				Zone[ZoneCount].qpHysteresisMin[UP] = qpMinUp; //35
				Zone[ZoneCount].qpHysteresisMax[UP] = qpMaxUp; //50
				Zone[ZoneCount].qpHysteresisMin[DOWN] = qpMinUp; //35
				Zone[ZoneCount].qpHysteresisMax[DOWN] = qpMaxUp; //50
				ZoneCount++;
			}
		}
	}

	// Brc last zone
	Zone[ZoneCount].maxBufferFullness[UP] = SequenceParams->cpbBufferSize; // Filler data detected
	Zone[ZoneCount].maxBufferFullness[DOWN] = SequenceParams->cpbBufferSize; // Filler data detected
	Zone[ZoneCount].samplingPeriod = 1;
	Zone[ZoneCount].qpMin = SequenceParams->qpmin; //18
	Zone[ZoneCount].qpMax = H264_ENC_OPTIMAL_QP_MAX; //35
	Zone[ZoneCount].qpHysteresisMin[UP] = qpMinUp; //35
	Zone[ZoneCount].qpHysteresisMax[UP] = qpMaxUp; //50
	Zone[ZoneCount].qpHysteresisMin[DOWN] = qpMinUp; //35
	Zone[ZoneCount].qpHysteresisMax[DOWN] = qpMaxUp; //50
	Context->brc.lastZone = ZoneCount;
	ZoneCount++;

	Context->brc.zoneNb = ZoneCount;
	Context->brc.lastBufferFullness = 0;

	DumpZone(Context->brc.zone, Context->brc.zoneNb, StreamId);

	// BRC FILTER PARAMETERS
	Context->brc.lastQp = 0;
	Context->brc.nextFilterFromQp = 0;
	Context->brc.nextFilterFromQpSkip = 0;
	Context->brc.accumulatedQp = 0;
	Context->brc.nextFilterFromQpAverage = 0;
	Context->brc.accumulatedBufferFullness = 0;
	Context->brc.bufferFullnessCount = 0;
}

//{{{  H264HwEncodeSetSequenceParams
//{{{  doxynote
/// \brief      Provide sequence related encode parameters
///
/// \return     H264EncodeHardStatus_t
//}}}
H264EncodeHardStatus_t H264HwEncodeSetSequenceParams(H264EncodeHardHandle_t Handle)
{
	H264EncodeHardCodecContext_t  *Context = (H264EncodeHardCodecContext_t *)Handle;
	H264EncodeHardSequenceParams_t *SequenceParams = &Context->globalParameters;
	H264EncodeNVCLContext_t       *nVCLContext = &Context->nVCLContext;
	unsigned int StreamId = Context->StreamId;

	// Initialize reference frame rate as set sequence params
	Context->framerateNumRef = Context->globalParameters.framerateNum;
	Context->framerateDenRef = Context->globalParameters.framerateDen;
	// Initialize reference bitrate as set sequence params, will be used in future
	Context->bitrateRef      = Context->globalParameters.bitRate;
	Context->cpbBufferSizeRef = Context->globalParameters.cpbBufferSize;
	H264HwEncodeCheckBitrateAndCpbSize(Context);

	Context->qpmin = Context->globalParameters.qpmin & QP_BITS_MASK;
	Context->qpmax = Context->globalParameters.qpmax & QP_BITS_MASK;

	dev_dbg(pDriverData->dev, "Stream 0x%x [Sequence parameters]\n", StreamId);
	dev_dbg(pDriverData->dev, "Stream 0x%x |-frameWidth              = %u pixels\n", StreamId,
	        Context->globalParameters.frameWidth);
	dev_dbg(pDriverData->dev, "Stream 0x%x |-frameHeight             = %u pixels\n", StreamId,
	        Context->globalParameters.frameHeight);
	dev_dbg(pDriverData->dev, "Stream 0x%x |-log2MaxFrameNumMinus4   = %u\n", StreamId,
	        Context->globalParameters.log2MaxFrameNumMinus4);
	dev_dbg(pDriverData->dev, "Stream 0x%x |-useConstrainedIntraFlag = %u\n", StreamId,
	        Context->globalParameters.useConstrainedIntraFlag);
	dev_dbg(pDriverData->dev, "Stream 0x%x |-MaxSumNumBitsInNALU     = %u bits\n", StreamId,
	        Context->globalParameters.maxSumNumBitsInNALU);
	dev_dbg(pDriverData->dev, "Stream 0x%x |-intraRefreshType        = %u (%s)\n", StreamId,
	        Context->globalParameters.intraRefreshType,
	        StringifyIntraRefreshType(Context->globalParameters.intraRefreshType));
	if (Context->globalParameters.irParamOption == HVA_ENCODE_DISABLE_INTRA_REFRESH) {
		dev_dbg(pDriverData->dev, "Stream 0x%x |-irParamOption           = %u\n", StreamId,
		        Context->globalParameters.irParamOption);
	} else {
		dev_dbg(pDriverData->dev, "Stream 0x%x |-irParamOption           = %u %s\n", StreamId,
		        Context->globalParameters.irParamOption,
		        (Context->globalParameters.intraRefreshType == HVA_ENCODE_ADAPTIVE_INTRA_REFRESH) ? "macroblocks/frame refreshed" :
		        "frames (refresh period)");
	}
	dev_dbg(pDriverData->dev, "Stream 0x%x |-brcType                 = %u (%s)\n", StreamId,
	        Context->globalParameters.brcType,
	        StringifyBrcType(Context->globalParameters.brcType));
	dev_dbg(pDriverData->dev, "Stream 0x%x |-bitRate                 = %u bps\n", StreamId,
	        Context->globalParameters.bitRate);
	dev_dbg(pDriverData->dev, "Stream 0x%x |-cpbBufferSize           = %u bits\n", StreamId,
	        Context->globalParameters.cpbBufferSize);
	dev_dbg(pDriverData->dev, "Stream 0x%x |-framerateNum            = %u\n", StreamId,
	        Context->globalParameters.framerateNum);
	dev_dbg(pDriverData->dev, "Stream 0x%x |-framerateDen            = %u\n", StreamId,
	        Context->globalParameters.framerateDen);
	dev_dbg(pDriverData->dev, "Stream 0x%x |-TransformMode           = %u (%s)\n", StreamId,
	        Context->globalParameters.transformMode,
	        StringifyTransformMode(Context->globalParameters.transformMode));
	dev_dbg(pDriverData->dev, "Stream 0x%x |-encoderComplexity       = %u (%s)\n", StreamId,
	        Context->globalParameters.encoderComplexity,
	        StringifyEncoderComplexity(Context->globalParameters.encoderComplexity));
	dev_dbg(pDriverData->dev, "Stream 0x%x |-samplingMode            = %u (%s)\n", StreamId,
	        Context->globalParameters.samplingMode,
	        StringifySamplingMode(Context->globalParameters.samplingMode));
	dev_dbg(pDriverData->dev, "Stream 0x%x |-cbrDelay                = %u ms\n", StreamId,
	        Context->globalParameters.cbrDelay);
	dev_dbg(pDriverData->dev, "Stream 0x%x |-QPmin                   = %u\n", StreamId,
	        Context->globalParameters.qpmin);
	dev_dbg(pDriverData->dev, "Stream 0x%x |-QPmax                   = %u\n", StreamId,
	        Context->globalParameters.qpmax);
	dev_dbg(pDriverData->dev, "Stream 0x%x \n", StreamId);

	// Initialize vcl context parameter
	Context->mvToggle = 1;
	Context->lastIdrPicId = -1;
	Context->idrToggle = 1;

	Context->qp = 0;
	Context->frameCount = 0;
	Context->nextFilterLevel = 0;

	H264HwEncodeInitParamsBRC(Context);

	H264HwEncodeInitParamsNAL(nVCLContext, SequenceParams, true);

	// Reload the StreamContext values.
	H264HwEncodeLoadStreamContext(Context);

	return H264ENCHARD_NO_ERROR;
}


//{{{  H264HwEncodeLoadStreamContext
//{{{  doxynote
/// \brief      Restore HVA task descriptor with a managed context
///
//}}}
static void H264HwEncodeLoadStreamContext(H264EncodeHardCodecContext_t  *Context)
{
	unsigned int StreamId = Context->StreamId;
	H264EncodeHardTaskDescriptor_t *pTaskDescriptor = (H264EncodeHardTaskDescriptor_t *)Context->taskDescriptorHandler;

	dev_dbg(pDriverData->dev, "Stream 0x%x Reload stream context for client %d\n", StreamId, Context->clientId);

	//No specific HW registers to set but task descriptor vs global sequence parameters & static parameters
	pTaskDescriptor->frameWidth              = (((Context->globalParameters.frameWidth + 15) >> 4) << 4) &
	                                           FRAME_DIMENSION_BITS_MASK;
	pTaskDescriptor->frameHeight             = (((Context->globalParameters.frameHeight + 15) >> 4) << 4) &
	                                           FRAME_DIMENSION_BITS_MASK;
	pTaskDescriptor->windowWidth             = pTaskDescriptor->frameWidth;
	pTaskDescriptor->windowHeight            = pTaskDescriptor->frameHeight;
	pTaskDescriptor->windowHorizontalOffset  = 0;
	pTaskDescriptor->windowVerticalOffset    = 0;
	/*pTaskDescriptor->pictureCodingType is frame parameter
	  pTaskDescriptor->idrFlag is not used by hva
	  pTaskDescriptor->frameNum is not used by hva */
	pTaskDescriptor->picOrderCntType         = Context->globalParameters.picOrderCntType & PIC_ORDER_CNT_BITS_MASK;
	/*pTaskDescriptor->FirstPictureInSequence is frame parameter */
	pTaskDescriptor->useConstrainedIntraFlag = Context->globalParameters.useConstrainedIntraFlag & INTRA_FLAG_BITS_MASK;
	//FIXME: slice management to clarify
	//pTaskDescriptor->sliceSizeType           = HVA_ENCODE_SLICE_BYTE_SIZE_LIMIT; /*Fixed value and use sliceByteSize*/
	//pTaskDescriptor->sliceByteSize           = HVA_ENCODE_RECOMMENDED_SLICE_SIZE;
	pTaskDescriptor->sliceSizeType           = HVA_ENCODE_NO_CONSTRAINT_TO_CLOSE_SLICE;
	//Number of MB per slice not used with HVA_ENCODE_SLICE_BYTE_SIZE_LIMIT sliceSizeType
	/*pTaskDescriptor->sliceMbSize             = (((pTaskDescriptor->frameWidth / 16) * (pTaskDescriptor->frameHeight / 16)) / Context->globalParameters.sliceNumber) & SLICE_MB_SIZE_BITS_MASK;*/
	pTaskDescriptor->intraRefreshType        = Context->globalParameters.intraRefreshType;
	pTaskDescriptor->irParamOption           = Context->globalParameters.irParamOption & IR_PARAM_BITS_MASK;
	/*pTaskDescriptor->disableDeblockingFilterIdc
	pTaskDescriptor->sliceAlphaC0OffsetDiv2
	pTaskDescriptor->sliceBetaOffsetDiv2 are frame parameters */
	pTaskDescriptor->brcType                 = Context->globalParameters.brcType;
	/*pTaskDescriptor->lastBPAUts
	pTaskDescriptor->NALfinalArrivalTime are not used by hva */
	//pTaskDescriptor->nonVCLNALUSize is frame parameter
	pTaskDescriptor->cpbBufferSize           = Context->globalParameters.cpbBufferSize;
	pTaskDescriptor->bitRate                 = Context->globalParameters.bitRate;
	//pTaskDescriptor->timestamp is not used by hva
	pTaskDescriptor->samplingMode            = Context->globalParameters.samplingMode;
	pTaskDescriptor->transformMode           = Context->globalParameters.transformMode;
	pTaskDescriptor->encoderComplexity       = Context->globalParameters.encoderComplexity;
	pTaskDescriptor->quant                   = Context->globalParameters.vbrInitQp;
	H264HwEncodeCheckFramerate(Context);
	pTaskDescriptor->delay                   = Context->globalParameters.cbrDelay;
	pTaskDescriptor->strictHRDCompliancy     = HVA_ENCODE_USE_STRICT_HRD_COMPLIANCY;

	//Buffers addresses: only deal here with instance static buffers (full static programmed at task desc creation and frame related at encode frame stage)
	//WARNING: Reference and Reconstructed addresses have to be swapped after each frame encoding as REC(N) will be REF(N+1)

	/* pTaskDescriptor->addrSourceBuffer is a frame parameter */
	pTaskDescriptor->addrFwdRefBuffer        = Context->referenceFrameBufPhysAddress;
	pTaskDescriptor->addrRecBuffer           = Context->reconstructedFrameBufPhysAddress;
	/*pTaskDescriptor->addrOutputBitstreamStart
	  pTaskDescriptor->addrOutputBitstreamEnd
	  pTaskDescriptor->bitstreamOffset              are frame parameters */
	/*pTaskDescriptor->addrLctx                     is static parameter */
	pTaskDescriptor->addrParamInout          = Context->paramOutPhysAddress;
	/*pTaskDescriptor->addrExternalSw
	  pTaskDescriptor->addrLocalRecBuffer
	  pTaskDescriptor->addrExternalCwi              are static params */
	//Swap spatial and temporal context (replace mvToggle)
	if (Context->mvToggle == 0) {
		pTaskDescriptor->addrSpatialContext = Context->spatialContextBufPhysAddress;
		pTaskDescriptor->addrTemporalContext = Context->temporalContextBufPhysAddress;
	} else {
		pTaskDescriptor->addrSpatialContext = Context->temporalContextBufPhysAddress;
		pTaskDescriptor->addrTemporalContext = Context->spatialContextBufPhysAddress;
	}

	pTaskDescriptor->addrBrcInOutParameter  = Context->brcInOutBufPhysAddress;
	pTaskDescriptor->addrSliceHeader = Context->sliceHeaderBufPhysAddress;
	pTaskDescriptor->maxSliceNumber = 1;
	pTaskDescriptor->brcNoSkip = 0;

	//From here, instance's task descriptor is in line with latest provided global parameters for a given MME instance AND current context
	//Only Frame related parameters missing

}

// ////////////////////////////////////////////////////////////////////////////////
//
//    Local function to load static context: update related instance task descriptor

static void H264HwEncodeLoadStaticParameters(H264EncodeHardCodecContext_t  *Context)
{
	H264EncodeHardTaskDescriptor_t *pTaskDescriptor = (H264EncodeHardTaskDescriptor_t *)Context->taskDescriptorHandler;

	//Full static params: static among MME instance
	pTaskDescriptor->addrExternalSw         = pDriverData->searchWindowBufPhysAddress;
	pTaskDescriptor->addrLocalRecBuffer     = pDriverData->localRecBufPhysAddress;
	pTaskDescriptor->addrLctx               = pDriverData->contextMBBufPhysAddress;
	pTaskDescriptor->addrCabacContextBuffer = pDriverData->cabacContextBufPhysAddress;

}

void H264HwEncodeDebugOptionHack(H264EncodeHardFrameParams_t    *FrameParams,
                                 H264EncodeHardTaskDescriptor_t *pTaskDescriptor,
                                 unsigned int StreamId)
{
	if (FrameParams->debugOption != HVA_ENCODE_DEBUG_OPTION_IGNORE) {
		dev_warn(pDriverData->dev, "Stream 0x%x Force hva hang, debugOption = 0x%x\n", StreamId, FrameParams->debugOption);

		if (FrameParams->debugOption & HVA_ENCODE_DEBUG_OPTION_FORCE_DDR_ADDR_0) {
			dev_warn(pDriverData->dev, "Stream 0x%x Change addrOutputBitstreamStart from 0x%x to 0x0\n", StreamId,
			         pTaskDescriptor->addrOutputBitstreamStart);
			pTaskDescriptor->addrOutputBitstreamStart = 0x0; // same behaviour with 0x500000
		}

		if (FrameParams->debugOption & HVA_ENCODE_DEBUG_OPTION_FORCE_ERAM_ADDR_0) {
			dev_warn(pDriverData->dev, "Stream 0x%x Change addrExternalSw from 0x%x to 0x0\n", StreamId,
			         pTaskDescriptor->addrExternalSw);
			pTaskDescriptor->addrExternalSw           = 0;
		}

		if (FrameParams->debugOption > HVA_ENCODE_DEBUG_OPTION_LAST) {
			dev_err(pDriverData->dev, "Stream 0x%x Debug option not found, debugOption = 0x%x\n", StreamId,
			        FrameParams->debugOption);
		}
	}
}

void DumpBrcData(unsigned int addr_sequence_info_buffer, int StreamId)
{
	ts_hvc_brc_inout *addr_brc_data = (ts_hvc_brc_inout *)addr_sequence_info_buffer;
	dev_dbg(pDriverData->dev, "Stream 0x%x [BRC_DATA]\n", StreamId);
	dev_dbg(pDriverData->dev, "Stream 0x%x |-buffer_fullness base = %u, div = %u, rem = %u\n", StreamId,
	        addr_brc_data->buffer_fullness_base,
	        addr_brc_data->buffer_fullness_div,
	        addr_brc_data->buffer_fullness_rem);
	dev_dbg(pDriverData->dev, "Stream 0x%x |-old_buffer_fullness base = %u, div = %u, rem = %u\n", StreamId,
	        addr_brc_data->old_buffer_fullness_base,
	        addr_brc_data->old_buffer_fullness_div,
	        addr_brc_data->old_buffer_fullness_rem);
	dev_dbg(pDriverData->dev, "Stream 0x%x |-old_fps num = %u, den = %u\n", StreamId, addr_brc_data->old_fps_num,
	        addr_brc_data->old_fps_den);
	dev_dbg(pDriverData->dev, "Stream 0x%x |-old_bit_rate = %u\n", StreamId, addr_brc_data->old_bit_rate);
	dev_dbg(pDriverData->dev, "Stream 0x%x |-ts = %u\n", StreamId, addr_brc_data->ts);
	dev_dbg(pDriverData->dev, "Stream 0x%x |-old_ts = %u\n", StreamId, addr_brc_data->old_ts);
	dev_dbg(pDriverData->dev, "Stream 0x%x |-skip_current = %u\n", StreamId, addr_brc_data->skip_current);
	dev_dbg(pDriverData->dev, "Stream 0x%x |-luma_adapt_on = %u\n", StreamId, addr_brc_data->luma_adapt_on);
	dev_dbg(pDriverData->dev, "Stream 0x%x |-mad_allowed_band = %u\n", StreamId, addr_brc_data->mad_allowed_band);
	dev_dbg(pDriverData->dev, "Stream 0x%x |-avg_mad = %u\n", StreamId, addr_brc_data->avg_mad);
	dev_dbg(pDriverData->dev, "Stream 0x%x |-pict_qp = %u\n", StreamId, addr_brc_data->prev_pict_qp);
	dev_dbg(pDriverData->dev, "Stream 0x%x |-prev_pict_qp = %u\n", StreamId, addr_brc_data->prev_pict_qp);
	dev_dbg(pDriverData->dev, "Stream 0x%x |-[PICTURE STATISTICS]\n", StreamId);
	dev_dbg(pDriverData->dev, "Stream 0x%x | |-size0 = %u\n", StreamId, addr_brc_data->size0);
	dev_dbg(pDriverData->dev, "Stream 0x%x | |-type0 = %u\n", StreamId, addr_brc_data->type0);
	dev_dbg(pDriverData->dev, "Stream 0x%x | |-sc_detected0 = %u\n", StreamId, addr_brc_data->sc_detected0);
	dev_dbg(pDriverData->dev, "Stream 0x%x | |-qp0 = %u\n", StreamId, addr_brc_data->qp0);
	dev_dbg(pDriverData->dev, "Stream 0x%x | |-size1 = %u\n", StreamId, addr_brc_data->size1);
	dev_dbg(pDriverData->dev, "Stream 0x%x | |-type1 = %u\n", StreamId, addr_brc_data->type1);
	dev_dbg(pDriverData->dev, "Stream 0x%x | |-sc_detected1 = %u\n", StreamId, addr_brc_data->sc_detected1);
	dev_dbg(pDriverData->dev, "Stream 0x%x | |-qp1 = %u\n", StreamId, addr_brc_data->qp1);
	dev_dbg(pDriverData->dev, "Stream 0x%x | |-size2 = %u\n", StreamId, addr_brc_data->size2);
	dev_dbg(pDriverData->dev, "Stream 0x%x | |-type2 = %u\n", StreamId, addr_brc_data->type2);
	dev_dbg(pDriverData->dev, "Stream 0x%x | |-sc_detected2 = %u\n", StreamId, addr_brc_data->sc_detected2);
	dev_dbg(pDriverData->dev, "Stream 0x%x | |-qp2 = %u\n", StreamId, addr_brc_data->qp2);
	dev_dbg(pDriverData->dev, "Stream 0x%x | |-size3 = %u\n", StreamId, addr_brc_data->size3);
	dev_dbg(pDriverData->dev, "Stream 0x%x | |-type3 = %u\n", StreamId, addr_brc_data->type3);
	dev_dbg(pDriverData->dev, "Stream 0x%x | |-sc_detected3 = %u\n", StreamId, addr_brc_data->sc_detected3);
	dev_dbg(pDriverData->dev, "Stream 0x%x | |-qp3 = %u\n", StreamId, addr_brc_data->qp3);
	dev_dbg(pDriverData->dev, "Stream 0x%x |\n", StreamId);
	dev_dbg(pDriverData->dev, "Stream 0x%x |-mad_adapt_on = %u\n", StreamId, addr_brc_data->mad_adapt_on);
	dev_dbg(pDriverData->dev, "Stream 0x%x |-avg_luma = %u\n", StreamId, addr_brc_data->avg_luma);
	dev_dbg(pDriverData->dev, "Stream 0x%x |-middle_luma_mb_num = %u\n", StreamId, addr_brc_data->middle_luma_mb_num);
	dev_dbg(pDriverData->dev, "Stream 0x%x |-total_mad = %u\n", StreamId, addr_brc_data->total_mad);
	dev_dbg(pDriverData->dev, "Stream 0x%x |-total_luma_sum = %u\n", StreamId, addr_brc_data->total_luma_sum);
	dev_dbg(pDriverData->dev, "Stream 0x%x \n", StreamId);
}

void HackBrcWindowPicStat(unsigned int addr_sequence_info_buffer, unsigned int SizeInBits, int StreamId)
{
	ts_hvc_brc_inout *addr_brc_data = (ts_hvc_brc_inout *)addr_sequence_info_buffer;
	dev_dbg(pDriverData->dev, "Stream 0x%x Hack brc window picture statistics to %u\n", StreamId, SizeInBits);
	addr_brc_data->size0 = SizeInBits;
	addr_brc_data->size1 = SizeInBits;
	addr_brc_data->size2 = SizeInBits;
	addr_brc_data->size3 = SizeInBits;
}

unsigned int BrcWindowPicSizeAverageInBits(H264EncodeHardCodecContext_t *Context, int StreamId)
{
	int i = 0;
	unsigned int sum = 0;
	unsigned int res = 0;
	for (i = 0 ; i < BRC_WINDOW_SIZE ; i++) {
		sum += Context->brc.lastPicSizeInBits[i];
	}
	res = sum / BRC_WINDOW_SIZE;
	dev_dbg(pDriverData->dev, "Stream 0x%x Brc window picture size average is %u bits\n", StreamId, res);
	return res;
}

unsigned int GetHwQp(unsigned int addr_sequence_info_buffer, int StreamId)
{
	ts_hvc_brc_inout *addr_brc_data = (ts_hvc_brc_inout *)addr_sequence_info_buffer;
	unsigned int hwQp = addr_brc_data->pict_qp;
	dev_dbg(pDriverData->dev, "Stream 0x%x qp = %u\n", StreamId, hwQp);
	return hwQp;
}

bool BrcWindowFillerDataDetected(H264EncodeHardCodecContext_t *Context, int StreamId)
{
	int i = 0;
	unsigned int sum = 0;
	bool FillerDataDetected = false;
	for (i = 0 ; i < BRC_WINDOW_SIZE ; i++) {
		sum += Context->brc.lastPicWithFillerData[i];
	}
	FillerDataDetected = ((2 * sum) >= BRC_WINDOW_SIZE) ? true : false;
	if (FillerDataDetected) {
		dev_dbg(pDriverData->dev, "Stream 0x%x Filler data detected, sum = %u\n", StreamId, sum);
	}
	return FillerDataDetected;
}

static void H264HwEncodeBrcPreprocessing(H264EncodeHardCodecContext_t *Context,
                                         H264EncodeHardFrameParams_t *FrameParams)
{
	H264EncodeHardTaskDescriptor_t *pTaskDescriptor = (H264EncodeHardTaskDescriptor_t *)Context->taskDescriptorHandler;
	H264EncodeHardSequenceParams_t *SequenceParams = &Context->globalParameters;
	unsigned int StreamId = Context->StreamId;

	// BRC SAMPLING PERIOD PROCESSING
	// If we are on a new sampling period or previous frame was an intra, qp is clipped to default value.
	// Else, if it is an inter frame, we force the same qp as previous encoded frame.
	// If it is an intra frame, we do the same but also increase quality to reduce pumping effect.
	if (Context->brc.samplingPeriod != 0) {
		if ((Context->brc.frameCountOnZone % Context->brc.samplingPeriod == 0) || (Context->brc.forceSampling == true)) {
			if (Context->brc.lastSkippedFrameCount) {
				pTaskDescriptor->qpmin = min(max(Context->brc.samplingQp + 2 * Context->brc.lastSkippedFrameCount, Context->qpmin),
				                             Context->qpmax);
				pTaskDescriptor->qpmax = min(max(Context->brc.samplingQp + 2 * Context->brc.lastSkippedFrameCount, Context->qpmin),
				                             Context->qpmax);
			} else {
				// Forbid low qp to handle scene change smoothly
				pTaskDescriptor->qpmin = (FrameParams->sceneChange ? H264_ENC_OPTIMAL_QP_MIN : Context->qpmin);
				pTaskDescriptor->qpmax = Context->qpmax;
			}
			Context->brc.samplingQpDelta = 0;
		} else {
			if (FrameParams->pictureCodingType == HVA_ENCODE_P_FRAME) {
				Context->brc.samplingQpDelta = 0;
			} else {
				// We decrease the qp on low sampling qp to compensate lack of intra quality
				// We also decrease the qp on higher sampling qp if scene change is detected
				int gradientNum = LUTQpVsIntraDeltaQp[1][1] - LUTQpVsIntraDeltaQp[0][1]; //-6
				int gradientDen = LUTQpVsIntraDeltaQp[1][0] - LUTQpVsIntraDeltaQp[0][0]; //15
				int yIntercept = LUTQpVsIntraDeltaQp[0][1] - (LUTQpVsIntraDeltaQp[0][0] * gradientNum / gradientDen); //14
				int minThreshold = LUTQpVsIntraDeltaQp[1][1] + (FrameParams->sceneChange ? QP_DELTA_OFFSET_ON_SCD : 0);
				int maxThreshold = LUTQpVsIntraDeltaQp[0][1];
				Context->brc.samplingQpDelta = min(maxThreshold,
				                                   yIntercept + ((int)Context->brc.samplingQp * gradientNum / gradientDen));
				Context->brc.samplingQpDelta = max(minThreshold, (int)Context->brc.samplingQpDelta);
				dev_dbg(pDriverData->dev, "Stream 0x%x sceneChange %d, samplingQp %u, samplingQpDelta %u, ((%d,%d), (%d,%d))\n",
				        StreamId, FrameParams->sceneChange, Context->brc.samplingQp, Context->brc.samplingQpDelta, LUTQpVsIntraDeltaQp[0][0],
				        LUTQpVsIntraDeltaQp[0][1], LUTQpVsIntraDeltaQp[1][0], LUTQpVsIntraDeltaQp[1][1]);
			}
			pTaskDescriptor->qpmin = max((int)SequenceParams->qpmin,
			                             (int)Context->brc.samplingQp - (int)Context->brc.samplingQpDelta);
			pTaskDescriptor->qpmax = max((int)SequenceParams->qpmin,
			                             (int)Context->brc.samplingQp - (int)Context->brc.samplingQpDelta);
		}
	} else {
		pTaskDescriptor->qpmin = Context->qpmin;
		pTaskDescriptor->qpmax = Context->qpmax;
	}
}

//{{{  H264HwEncodeLoadStreamContext
//{{{  doxynote
/// \brief      Encode frame function
///
/// \return     H264EncodeHardStatus_t
//}}}
H264EncodeHardStatus_t H264HwEncodeEncodeFrame(H264EncodeHardHandle_t Handle)
{
	H264EncodeHardCodecContext_t  *Context = (H264EncodeHardCodecContext_t *)Handle;
	H264EncodeHardTaskDescriptor_t *pTaskDescriptor = (H264EncodeHardTaskDescriptor_t *)Context->taskDescriptorHandler;
	FLEXIBLE_SLICE_HEADER flexible_slice_header;
	H264EncodeHardSequenceParams_t *SequenceParams = &Context->globalParameters;
	H264EncodeHardFrameParams_t *FrameParams = &Context->FrameParams;
	H264EncodeNVCLContext_t *nVCLContext = &Context->nVCLContext;
	ESBufferContext_t *ESContext = &Context->ESContext;
	uint8_t *streamBuf_p;
	uint32_t addrOutputBitstreamStartByteAlign;
	uint32_t addrOutputBitstreamStart;
	uint32_t addrOutputBitstreamEnd;
	uint32_t bitstreamOffset;
	uint32_t estSEISize;
	bool isNVclPresent;
	unsigned int StreamId = Context->StreamId;

	dev_dbg(pDriverData->dev, "Stream 0x%x Encode frame for client %d\n", StreamId, Context->clientId);

	// Fill the StreamContext descriptor
	H264HwEncodeLoadStreamContext(Context);

	H264HwEncodeCheckFramerate(Context);

	/* NVCL NALU Generation */
	estSEISize                = 0;
	ESContext->offset         = 0;
	ESContext->overEstSEISize = 0;

	H264HwEncodeInitParamsNAL(nVCLContext, SequenceParams, false);
	if ((FrameParams->addrOutputBitstreamPhysStart == 0) || (FrameParams->addrOutputBitstreamStart == 0)) {
		dev_err(pDriverData->dev, "Stream 0x%x Error: output bitstream address is not defined (0x%x:0x%x)\n",
		        StreamId, FrameParams->addrOutputBitstreamPhysStart, FrameParams->addrOutputBitstreamStart);
		return H264ENCHARD_ERROR;
	}
	ESContext->ESStart_p = (uint8_t *)FrameParams->addrOutputBitstreamStart;

	ESContext->offset += FrameParams->bitstreamOffset;
#if (ENABLE_H264ENC_ACCESS_UNIT_DELIMITER == 1)
	streamBuf_p  = ESContext->ESStart_p + (ESContext->offset >> 3);
	ESContext->offset += H264HwEncodeCreateAccessUnitDelimiterNAL(nVCLContext, FrameParams, streamBuf_p, StreamId);
#endif
#if (ENABLE_H264ENC_SPS_PPS_AT_EACH_IDR == 1)
	isNVclPresent = (FrameParams->idrFlag == 1) || (FrameParams->firstPictureInSequence == 1);
#else
	isNVclPresent = (FrameParams->firstPictureInSequence == 1);
#endif
	if (isNVclPresent) {
		streamBuf_p  = ESContext->ESStart_p + (ESContext->offset >> 3);
		ESContext->offset += H264HwEncodeCreateSeqParamSetNAL(nVCLContext, SequenceParams, streamBuf_p, StreamId);

		streamBuf_p  = ESContext->ESStart_p + (ESContext->offset >> 3);
		ESContext->offset += H264HwEncodeCreatePicParamSetNAL(nVCLContext, FrameParams, streamBuf_p, StreamId);
	}
	if (SequenceParams->brcType != HVA_ENCODE_NO_BRC &&
	    (SequenceParams->seiBufPeriodPresentFlag || SequenceParams->seiPicTimingPresentFlag ||
	     SequenceParams->seiSceneInfoPresentFlag)) {
		/* We need to estimate the SEI Size because the timing parameters are only available after encoding */
		estSEISize = H264HwEncodeEstimateSEISize(nVCLContext, FrameParams, StreamId);
		/* We overestimate SEI size for emulation start code prediction *3/8 */
		ESContext->overEstSEISize  = estSEISize + ((estSEISize * 3) >> 3);
	}

	Context->mvToggle = !Context->mvToggle;
	dev_dbg(pDriverData->dev, "Stream 0x%x mvToggle = %u => %u\n", StreamId, !Context->mvToggle, Context->mvToggle);

	Context->idrToggle = !Context->idrToggle;
	if ((FrameParams->pictureCodingType == HVA_ENCODE_I_FRAME) && (FrameParams->idrFlag == 1) &&
	    (Context->lastIdrPicId == Context->idrToggle)) {
		Context->idrToggle = !Context->idrToggle;
	} else {
		dev_dbg(pDriverData->dev, "Stream 0x%x idrToggle = %u => %u\n", StreamId, !Context->idrToggle, Context->idrToggle);
	}

	memset(&flexible_slice_header, 0, sizeof(FLEXIBLE_SLICE_HEADER));
	flexible_slice_header.frame_num = FrameParams->frameNum;
	flexible_slice_header.buffer = (uint8_t *)Context->sliceHeaderBufHandler;
	FrameParams->idrPicId = Context->idrToggle;
	pTaskDescriptor->SliceHeaderSizeInBits = H264HwEncodeFillFlexibleSliceHeader(&flexible_slice_header, FrameParams,
	                                                                             nVCLContext, StreamId);
	pTaskDescriptor->SliceHeaderOffset0 = flexible_slice_header.offset0_bitsize;
	pTaskDescriptor->SliceHeaderOffset1 = flexible_slice_header.offset1_bitsize;
	pTaskDescriptor->SliceHeaderOffset2 = 0;
	pTaskDescriptor->entropyCodingMode = nVCLContext->entropyCodingMode & ENTROPY_CODING_MODE_MASK;
	pTaskDescriptor->chromaQpIndexOffset = nVCLContext->chromaQpIndexOffset & CHROMA_QP_INDEX_OFFSET_MASK;

	if (SequenceParams->brcType == HVA_ENCODE_VBR) {
		H264HwEncodeBrcPreprocessing(Context, FrameParams);
	} else if (SequenceParams->brcType == HVA_ENCODE_CBR) {
		pTaskDescriptor->qpmin = Context->qpmin;
		pTaskDescriptor->qpmax = Context->qpmax;
	} else { // HVA_ENCODE_NO_BRC (qpmin/qpmax not used in this mode)
		if ((FrameParams->pictureCodingType == HVA_ENCODE_I_FRAME) && (FrameParams->idrFlag == 1)) {
			pTaskDescriptor->quant = FORCE_FIX_QPI;
		} else {
			pTaskDescriptor->quant = FORCE_FIX_QPP;
		}
	}

	/* update address for HVA to write */
	/* If we overestimate the SEI size to provide for emulation 3 bytes, we can shift the VCL NALU */
	addrOutputBitstreamStartByteAlign = FrameParams->addrOutputBitstreamPhysStart +
	                                    ((ESContext->offset + ESContext->overEstSEISize) >> 3);
	addrOutputBitstreamStart          = (addrOutputBitstreamStartByteAlign & 0xfffffff0); // 128 bit alignment
	bitstreamOffset                   = (addrOutputBitstreamStartByteAlign & 0xF) << 3; /* offset in bits */
	addrOutputBitstreamEnd            = FrameParams->addrOutputBitstreamPhysEnd;

	//Fill task descriptor with suitable Frame parameters
	pTaskDescriptor->pictureCodingType           = FrameParams->pictureCodingType           & PICTURE_CODING_TYPE_MASK;
	pTaskDescriptor->firstPictureInSequence      = FrameParams->firstPictureInSequence      & FIRST_PICT_MASK;
	pTaskDescriptor->disableDeblockingFilterIdc  = FrameParams->disableDeblockingFilterIdc  & DISABLE_DEBLOCKING_MASK;
	pTaskDescriptor->sliceAlphaC0OffsetDiv2      = FrameParams->sliceAlphaC0OffsetDiv2      & SLICE_ALPHA_MASK;
	pTaskDescriptor->sliceBetaOffsetDiv2         = FrameParams->sliceBetaOffsetDiv2         & SLICE_BETA_MASK;
	pTaskDescriptor->nonVCLNALUSize              = (ESContext->offset - FrameParams->bitstreamOffset) +
	                                               estSEISize; /* SEI exact size configured for task descriptor */
	pTaskDescriptor->addrSourceBuffer            = FrameParams->addrSourceBuffer;
	pTaskDescriptor->addrOutputBitstreamStart    = addrOutputBitstreamStart;
	pTaskDescriptor->addrOutputBitstreamEnd      = addrOutputBitstreamEnd;
	pTaskDescriptor->bitstreamOffset             = bitstreamOffset &
	                                               BITSTREAM_OFFSET_MASK; //FrameParams->bitstreamOffset             & BITSTREAM_OFFSET_MASK;

	// Check if debug option is set to hack some task descriptor fields
	H264HwEncodeDebugOptionHack(FrameParams, pTaskDescriptor, StreamId);

	//For debug, monitor task descriptor before launching task
	H264HwEncodeDisplayTaskDescriptor(Context);

	Context->frameEncodeStart = OSDEV_GetTimeInMicroSeconds();

	//Launch an encode task
	if (H264HwEncodeLaunchEncodeTask(Context) != H264ENCHARD_NO_ERROR) {
		//specific management for early discarded command where no callback will be received
		dev_err(pDriverData->dev, "Stream 0x%x Error: command discarded due to risk of fifo overflow\n", StreamId);
		return H264ENCHARD_CMD_DISCARDED;
	}

	// Wait for the end of task
	if (down_timeout(&Context->h264EncTask, msecs_to_jiffies(MME_COMMAND_TIMEOUT_MS)) != 0) {
		dev_err(pDriverData->dev, "Stream 0x%x Error: hva encoder task timeout\n", StreamId);
		return H264ENCHARD_CMD_DISCARDED;
	}

	// Complete the Encode
	Context->encodeStatus = H264HwEncodeCompletion(Context);

	// Increment global frame counter for Sysfs Statistics
	h264_frames_completed++;

	return H264ENCHARD_NO_ERROR;
}

//{{{  H264HwEncodeCompletion
//{{{  doxynote
/// \brief      Provide any work following an encode frame completion event.
///
/// \return     H264EncodeHardStatus_t
//}}}
static H264EncodeHardStatus_t H264HwEncodeCompletion(H264EncodeHardCodecContext_t *Context)
{
	H264EncodeHardSequenceParams_t *SequenceParams = &Context->globalParameters;
	H264EncodeNVCLContext_t *nVCLContext = &Context->nVCLContext;
	ESBufferContext_t *ESContext = &Context->ESContext;
	H264EncodeHardFrameParams_t *FrameParams = &Context->FrameParams;
	H264EncodeHardParamOut_t encParamOut;
	uint8_t *streamBuf_p;
	H264EncodeHardParamOut_t *paramOut = (H264EncodeHardParamOut_t *) Context->paramOutHandler;
	uint32_t i;
	uint32_t ESSize;
	uint32_t ESBufferSize;
	unsigned int StreamId = Context->StreamId;
	unsigned int Hva_Freq_Mhz;
	Context->frameEncodeEnd = OSDEV_GetTimeInMicroSeconds();

	WakeUpLatency = Context->frameEncodeEnd - Context->LastInterruptTime;

	if (WakeUpLatency > WakeUpLatencyMax) {
		WakeUpLatencyMax = WakeUpLatency;
	}

	// Running Total
	LatencyTotal += WakeUpLatency;

	if (++LatencyFrame % LA_SAMPLES == 0) {
		// WakeUpLatencyAvg =  LatencyTotal / LA_SAMPLES;
		do_div(LatencyTotal, LA_SAMPLES);
		WakeUpLatencyAvg = (unsigned int) LatencyTotal;
		LatencyTotal = 0;
		WakeUpLatencyMax = 0;
	}

	dev_dbg(pDriverData->dev, "Stream 0x%x Hva command completed\n", StreamId);

	// Store hva output parameters in context
	Context->statusBitstreamSize = paramOut->bitstreamSize;
	Context->statusRemovalTime   = paramOut->removalTime;
	Context->statusStuffingBits  = paramOut->stuffingBits;
	Context->frameEncodeDuration = (unsigned int)(Context->frameEncodeEnd - Context->frameEncodeStart);
	if (pDriverData->max_freq) {
		Hva_Freq_Mhz    = pDriverData->max_freq / CLOCK_FREQUENCY_MHZ;   // clock frequency from Device Tree
	} else {
		Hva_Freq_Mhz    = DEFAULT_HVA_CLOCK_FREQUENCY_MHZ;
	}
	if (paramOut->hvcStopTime > paramOut->hvcStartTime) {
		Context->hvcEncodeDuration   = (unsigned int)((paramOut->hvcStopTime - paramOut->hvcStartTime) /
		                                              Hva_Freq_Mhz); // Convert Ticks to us
	} else {
		unsigned int max_time = -1;
		Context->hvcEncodeDuration   = (unsigned int)((max_time - paramOut->hvcStartTime + paramOut->hvcStopTime) /
		                                              Hva_Freq_Mhz);
	}

	// Clear hva output parameters
	memset((void *)Context->paramOutHandler, 0, Context->paramOutSize);

	dev_dbg(pDriverData->dev, "Stream 0x%x [Hva output parameters]\n", StreamId);
	dev_dbg(pDriverData->dev, "Stream 0x%x |-statusBitstreamSize = %d bytes\n", StreamId, Context->statusBitstreamSize);
	dev_dbg(pDriverData->dev, "Stream 0x%x |-statusStuffingBits  = %d bits\n", StreamId, Context->statusStuffingBits);
	dev_dbg(pDriverData->dev, "Stream 0x%x |-statusRemovalTime   = %d\n", StreamId, Context->statusRemovalTime);
	dev_dbg(pDriverData->dev, "Stream 0x%x |-hvcEncodeDuration   = %d hva ticks\n", StreamId,
	        Context->hvcEncodeDuration);
	dev_dbg(pDriverData->dev, "Stream 0x%x |-frameEncodeDuration = %d us\n", StreamId, Context->frameEncodeDuration);
	dev_dbg(pDriverData->dev, "Stream 0x%x |-transformStatus     = %d (%s)\n", StreamId, Context->transformStatus,
	        StringifyTransformStatus(Context->transformStatus));
	dev_dbg(pDriverData->dev, "Stream 0x%x |-qp                  = %d\n", StreamId, Context->qp);
	dev_dbg(pDriverData->dev, "Stream 0x%x |-nextFilterLevel     = %d\n", StreamId, Context->nextFilterLevel);
	dev_dbg(pDriverData->dev, "Stream 0x%x \n", StreamId);

	// Report frame statistics through Sysfs
	cpu_encode_duration = Context->frameEncodeDuration;
	hw_encode_duration  = Context->hvcEncodeDuration;

	// Calculate Utilisation Statistics
	utilisation_update(&hw_utilisation, paramOut->hvcStartTime, paramOut->hvcStopTime);
	utilisation_update(&sw_utilisation, Context->frameEncodeStart, Context->frameEncodeEnd);

	//manage context / task desc update at process end like mv_toggle, rec/ref buffer swap
	H264HwEncodeUpdateContextAtEncodeEnd(Context);

	encParamOut.bitstreamSize = Context->statusBitstreamSize;
	encParamOut.removalTime   = Context->statusRemovalTime;
	encParamOut.stuffingBits  = Context->statusStuffingBits;

	if ((SequenceParams->brcType != HVA_ENCODE_NO_BRC) &&
	    (Context->transformStatus == HVA_ENCODE_OK) &&
	    (SequenceParams->seiBufPeriodPresentFlag || SequenceParams->seiPicTimingPresentFlag ||
	     SequenceParams->seiSceneInfoPresentFlag)) {
		uint32_t SEISize = 0;
		streamBuf_p = ESContext->ESStart_p + (ESContext->offset >> 3);

		SEISize = H264HwEncodeCreateSEINAL(nVCLContext, FrameParams, &encParamOut, streamBuf_p, StreamId);
		/* The SEI size will be different from estimation if there is some emulation code inserted */
		if (SEISize != ESContext->overEstSEISize) {
			dev_dbg(pDriverData->dev,
			        "Stream 0x%x Different length between current sei size (%d bytes) and over estimated sei size (%d bytes)\n",
			        StreamId, SEISize,
			        ESContext->overEstSEISize);
			if (SEISize < ESContext->overEstSEISize) {
				uint8_t *ESStartVCL_p; // Hardware Bitstream Buffer Virtual Address Pointer
				ESStartVCL_p = ESContext->ESStart_p + ((ESContext->offset + ESContext->overEstSEISize) >> 3);

				streamBuf_p = ESContext->ESStart_p + ((ESContext->offset + SEISize) >> 3); /* start address */
				memcpy((void *)streamBuf_p, (void *)ESStartVCL_p, (size_t)(encParamOut.bitstreamSize));
			} else {
				/* If we underestimate the SEI size, error will be generated */
				dev_err(pDriverData->dev, "Stream 0x%x Error: current sei size (%d) is larger than over estimated sei size (%d)\n",
				        StreamId, SEISize,
				        ESContext->overEstSEISize);
				return H264ENCHARD_ERROR;
			}
		}
		ESContext->offset += SEISize;
	}
	ESBufferSize = (FrameParams->addrOutputBitstreamEnd - FrameParams->addrOutputBitstreamStart);
	ESSize = (ESContext->offset + encParamOut.bitstreamSize + (encParamOut.stuffingBits ?
	                                                           (encParamOut.stuffingBits >> 3) + FILLER_DATA_HEADER_SIZE : 0));
	dev_dbg(pDriverData->dev, "Stream 0x%x ESSize = %u, ESBufferSize = %u\n", StreamId, ESSize, ESBufferSize);
	if (ESSize <= ESBufferSize) {
		if (encParamOut.stuffingBits > 0) {
			streamBuf_p = ESContext->ESStart_p + (ESContext->offset >> 3) + encParamOut.bitstreamSize;
			ESContext->offset += H264HwEncodeCreateFillerNAL(nVCLContext, &encParamOut, streamBuf_p, StreamId);
		}
	} else {
		dev_warn(pDriverData->dev, "Stream 0x%x Warning: buffer too small to add filler data\n", StreamId);
	}
	dev_dbg(pDriverData->dev, "Stream 0x%x [Frame composition]\n", StreamId);
	dev_dbg(pDriverData->dev, "Stream 0x%x |-AU   = %d bytes\n", StreamId,
	        (((ESContext->offset - FrameParams->bitstreamOffset) >> 3) - 1) + encParamOut.bitstreamSize +
	        (encParamOut.stuffingBits ? (encParamOut.stuffingBits >> 3) + FILLER_DATA_HEADER_SIZE : 0));
	dev_dbg(pDriverData->dev, "Stream 0x%x |-NVCL = %d bytes\n", StreamId,
	        ((ESContext->offset - FrameParams->bitstreamOffset) >> 3) - 1);
	dev_dbg(pDriverData->dev, "Stream 0x%x |-VCL  = %d bytes\n", StreamId, encParamOut.bitstreamSize);
	dev_dbg(pDriverData->dev, "Stream 0x%x |-FD   = %d bytes\n", StreamId, (encParamOut.stuffingBits > 0 &&
	                                                                        ESSize <= ESBufferSize) ? (encParamOut.stuffingBits >> 3) + FILLER_DATA_HEADER_SIZE : 0);
	dev_dbg(pDriverData->dev, "Stream 0x%x \n", StreamId);
	Context->statusNonVCLNALUSize = ((ESContext->offset - FrameParams->bitstreamOffset) >> 3);

	for (i = 3; i > 0; i--) {
		Context->PastBitstreamSize[i] = Context->PastBitstreamSize[i - 1];
	}
	Context->PastBitstreamSize[0] = encParamOut.bitstreamSize;

	return H264ENCHARD_NO_ERROR;
}


//{{{  H264HwEncodeTerminate
//{{{  doxynote
/// \brief      Terminate one encoder instance
///
/// \return     H264EncodeHardStatus_t
//}}}
H264EncodeHardStatus_t       H264HwEncodeTerminate(H264EncodeHardHandle_t                    Handle)
{
	H264EncodeHardCodecContext_t  *Context = (H264EncodeHardCodecContext_t *)Handle;
	unsigned int StreamId = Context->StreamId;

	dev_info(pDriverData->dev, "Stream 0x%x Hva encode terminate\n", StreamId);

	mutex_lock(&H264EncoderGlobalLock);

	pDriverData->h264EncInstanceNb--;
	up(&h264EncInstances);

	//Free instance related Buffers

	H264HwEncodeFreeContextHwBuffers(Context);

	ClientContextList[Context->clientId] = NULL;

	//if last instance of encode, should free all resources like mono instances buffers
	if (pDriverData->h264EncInstanceNb == 0) {
		device_remove_file(pDriverData->dev, &dev_attr_h264CmdFifoDepth);
		H264HwEncodeInterruptUnInstall(StreamId);
		H264HwEncodeFreeSingletonHwBuffers();
	}

	mutex_unlock(&H264EncoderGlobalLock);

	return H264ENCHARD_NO_ERROR;
}

///////////////////////////////////////////////////////////////
//    The interrupt handler installation function

static H264EncodeHardStatus_t H264HwEncodeInterruptInstall(unsigned int StreamId)
{
	if (request_irq(pDriverData->platformData.Interrupt[0], H264HwEncodeInterruptHandler, 0, h264enc_irq_names[0],
	                NULL) != 0) {
		dev_err(pDriverData->dev, "Stream 0x%x Error: hva interrupt request failed\n", StreamId);
		return H264ENCHARD_ERROR;
	}
	if (request_irq(pDriverData->platformData.Interrupt[1], H264HwEncodeErrorInterruptHandler, 0, h264enc_irq_names[1],
	                NULL) != 0) {
		dev_err(pDriverData->dev, "Stream 0x%x Error: hva error interrupt request failed\n", StreamId);
		return H264ENCHARD_ERROR;
	}

	return H264ENCHARD_NO_ERROR;
}


// ///////////////////////////////////////////////////////////////////////////////
//
//    The interrupt handler removal function

static void H264HwEncodeInterruptUnInstall(unsigned int StreamId)
{
	dev_dbg(pDriverData->dev, "Stream 0x%x Free hva interrupts (CURRENTLY DISABLED)\n", StreamId);
	free_irq(pDriverData->platformData.Interrupt[0], NULL);
	free_irq(pDriverData->platformData.Interrupt[1], NULL);
}

// ///////////////////////////////////////////////////////////////////////////////
//
//    The interrupt handler code
OSDEV_InterruptHandlerEntrypoint(H264HwEncodeInterruptHandler)
{
	unsigned int fifoLevel;
	unsigned int fifoStatus = 0;

	unsigned long flags;

	H264EncodeHardCodecContext_t   *Context = NULL;
	unsigned int CommandStatus;
	unsigned int TaskId;
	unsigned int ClientId;
	unsigned int StreamId = 0;

	(void)Irq; // warning removal
	(void)Parameter; // warning removal

	spin_lock_irqsave(&pDriverData->platformData.hw_lock, flags);

	fifoLevel = ReadHvaRegister(HVA_HIF_REG_SFL);
	if ((fifoLevel & 0xF) > 0) { //at least one element in status fifo
		fifoStatus = ReadHvaRegister(HVA_HIF_FIFO_STS);

		// Hva periodically raises an interrupt unless it receives an acknowledge.
		// Acknowledge hva only if fifo level is greater than 0 and do it as quick
		// as possible to limit interrupt reply.
		WriteHvaRegister(HVA_HIF_REG_IT_ACK, 0x1);

		// We have read the FIFO register, and therefore removed an entry from the queue...
		up(&h264CmdFifoDepth);

		CommandStatus = (fifoStatus) & 0xFF;
		ClientId = (fifoStatus >> 8) & 0xFF;
		TaskId = (fifoStatus >> 16) & 0xFFFF;

		if (ClientId < MAX_HVA_ENCODE_INSTANCE) {
			Context = ClientContextList[ClientId];
			StreamId = Context->StreamId;

			// Assert that we have obtained the correct Context
			if (ClientId != Context->clientId) {
				dev_err(pDriverData->dev,
				        "Stream 0x%x Error: context id do not match (%u != %u), CommandStatus = %u, TaskId = %u\n",
				        StreamId, ClientId, Context->clientId, CommandStatus, TaskId);
			}
		} else {
			dev_err(pDriverData->dev,
			        "Stream 0x%x Error: ClientId %u >= MAX_HVA_ENCODE_INSTANCE %u, CommandStatus = %u, TaskId = %u\n",
			        StreamId, ClientId, MAX_HVA_ENCODE_INSTANCE, CommandStatus, TaskId);
		}
	} else {
		pDriverData->spurious_irq_count++;
		dev_warn(pDriverData->dev, "Stream 0x%x Warning: hva fifo is empty, fifoLevel = %u, spurious_irq_count = %d\n",
		         StreamId, fifoLevel,
		         pDriverData->spurious_irq_count);
	}

	// Unlock as soon as possible to limit time of disabled interrupt
	spin_unlock_irqrestore(&pDriverData->platformData.hw_lock, flags);

	if (Context) {
		// We can update our Context Specifics ... before completing the frame
		Context->transformStatus = (unsigned char) CommandStatus;
		Context->LastInterruptTime = OSDEV_GetTimeInMicroSeconds();

		// Wake up pending task
		up(&(Context->h264EncTask));
	} else {
		dev_warn(pDriverData->dev, "Stream 0x%x Warning: context not found, fifoLevel = %u, fifoStatus = %u\n", StreamId,
		         fifoLevel, fifoStatus);
	}

	return IRQ_HANDLED;
}


// ///////////////////////////////////////////////////////////////////////////////
//
//    The interrupt handler code
OSDEV_InterruptHandlerEntrypoint(H264HwEncodeErrorInterruptHandler)
{
	unsigned int LMIError;
	unsigned int EMIError;

	unsigned long flags;

	(void)Irq; // warning removal
	(void)Parameter; // warning removal

	dev_err(pDriverData->dev, "Error: hva error interrupt raised\n");

	spin_lock_irqsave(&pDriverData->platformData.hw_lock, flags);

	//Acknowledge HW error interrupt
	WriteHvaRegister(HVA_HIF_REG_ERR_IT_ACK, 0x1);

	//Track local memory interface error
	LMIError = ReadHvaRegister(HVA_HIF_REG_LMI_ERR);
	if (LMIError != 0) {
		pDriverData->errorInterruptStatus = H264ENCHARD_HW_LMI_ERR;
	}
	//Track external memory interface error
	EMIError = ReadHvaRegister(HVA_HIF_REG_EMI_ERR);
	if (EMIError != 0) {
		pDriverData->errorInterruptStatus = H264ENCHARD_HW_EMI_ERR;
	}

	spin_unlock_irqrestore(&pDriverData->platformData.hw_lock, flags);

	dev_err(pDriverData->dev, "Error: HVA_HIF_REG_LMI_ERR = 0x%x, HVA_HIF_REG_EMI_ERR = 0x%x\n", LMIError, EMIError);

	return IRQ_HANDLED;
}

/////////////////////////////////////////////////////////////////////////////////
//
//Allocate here all mono instance HVA related buffers
//Physical contiguous addresses are required

static H264EncodeHardStatus_t H264HwEncodeAllocateSingletonHwBuffers(unsigned int StreamId)
{
	unsigned int misalignment = 0;

	dev_dbg(pDriverData->dev, "Stream 0x%x [Allocate singleton buffers]\n", StreamId);

	//
	//SRAM buffers: single instance buffer as no context required
	//
	dev_dbg(pDriverData->dev, "Stream 0x%x |-Hva esram start address is 0x%x\n", StreamId,
	        pDriverData->newHvaSramFreePhysAddress);

	//IMPORTANT NOTICE:
	//Brc in/out (ddr), spatial/temporal context (ddr), context MXB data (esram) and cabac context (esram) have the same hva default cid.
	//That means that these esram buffers can't be secured. It is then reserved statically and independantly of the others by crypto core in UZ
	//at the beginning of the esram. This is mainly a workaround to secure other esram buffers that contain sensitive memory.
	//WARNING: Be careful not to change size of these 2 esram buffers that are aligned with crypto core configuration.

	//UNSECURED BUFFER
	//context MXB data in Raw format
	pDriverData->contextMBBufPhysAddress = HVA_ALIGNED_256(pDriverData->newHvaSramFreePhysAddress);
	misalignment = pDriverData->contextMBBufPhysAddress - pDriverData->newHvaSramFreePhysAddress;
	dev_dbg(pDriverData->dev, "Stream 0x%x |-ctxMB buffer misalignment is 0x%x bytes\n", StreamId, misalignment);
	//Update free memory address pool
	pDriverData->newHvaSramFreePhysAddress += (CTX_MB_BUFFER_MAX_SIZE + misalignment);
	if (pDriverData->newHvaSramFreePhysAddress > (pDriverData->HvaSramBase + pDriverData->HvaSramSize)) {
		return H264ENCHARD_NO_HVA_ERAM_MEMORY;
	}
	dev_dbg(pDriverData->dev, "Stream 0x%x |-contextMBBufPhysAddress = 0x%x\n", StreamId,
	        pDriverData->contextMBBufPhysAddress);
	dev_dbg(pDriverData->dev, "Stream 0x%x |-CTX_MB_BUFFER_MAX_SIZE = %u bytes\n", StreamId, CTX_MB_BUFFER_MAX_SIZE);

	//UNSECURED BUFFER
	//Cabac context
	pDriverData->cabacContextBufPhysAddress = HVA_ALIGNED_256(pDriverData->newHvaSramFreePhysAddress);
	misalignment = pDriverData->cabacContextBufPhysAddress - pDriverData->newHvaSramFreePhysAddress;
	dev_dbg(pDriverData->dev, "Stream 0x%x |-cabacContext buffer misalignment is 0x%x bytes\n", StreamId, misalignment);
	//Update free memory address pool
	pDriverData->newHvaSramFreePhysAddress += (CABAC_CONTEXT_BUFFER_MAX_SIZE + misalignment);
	if (pDriverData->newHvaSramFreePhysAddress > (pDriverData->HvaSramBase + pDriverData->HvaSramSize)) {
		return H264ENCHARD_NO_HVA_ERAM_MEMORY;
	}
	dev_dbg(pDriverData->dev, "Stream 0x%x |-cabacContextBufPhysAddress = 0x%x\n", StreamId,
	        pDriverData->cabacContextBufPhysAddress);
	dev_dbg(pDriverData->dev, "Stream 0x%x |-CABAC_CONTEXT_BUFFER_MAX_SIZE = %u bytes\n", StreamId,
	        CABAC_CONTEXT_BUFFER_MAX_SIZE);

	//SECURED BUFFER
	//search window buffer
	//Window in reference picture in YUV 420 MB-tiled format with size=(4*MBx+6)*256*3/2
	pDriverData->searchWindowBufPhysAddress = HVA_ALIGNED_256(pDriverData->newHvaSramFreePhysAddress);
	misalignment = pDriverData->searchWindowBufPhysAddress - pDriverData->newHvaSramFreePhysAddress;
	dev_dbg(pDriverData->dev, "Stream 0x%x |-searchWindow buffer misalignment is 0x%x bytes\n", StreamId, misalignment);
	//Update free memory address pool
	pDriverData->newHvaSramFreePhysAddress += (SEARCH_WINDOW_BUFFER_MAX_SIZE + misalignment);
	if (pDriverData->newHvaSramFreePhysAddress > (pDriverData->HvaSramBase + pDriverData->HvaSramSize)) {
		return H264ENCHARD_NO_HVA_ERAM_MEMORY;
	}
	dev_dbg(pDriverData->dev, "Stream 0x%x |-searchWindowBufPhysAddress = 0x%x\n", StreamId,
	        pDriverData->searchWindowBufPhysAddress);
	dev_dbg(pDriverData->dev, "Stream 0x%x |-SEARCH_WINDOW_BUFFER_MAX_SIZE = %u bytes\n", StreamId,
	        SEARCH_WINDOW_BUFFER_MAX_SIZE);

	//SECURED BUFFER
	//local_rec_buffer
	//4 lines of pixels (in Luma, Chroma blue and Chroma red) of top MB for deblocking with size=4*16*MBx*2
	pDriverData->localRecBufPhysAddress = HVA_ALIGNED_256(pDriverData->newHvaSramFreePhysAddress);
	misalignment = pDriverData->localRecBufPhysAddress - pDriverData->newHvaSramFreePhysAddress;
	dev_dbg(pDriverData->dev, "Stream 0x%x |-localRec buffer misalignment is 0x%x bytes\n", StreamId, misalignment);
	//Update free memory address pool
	pDriverData->newHvaSramFreePhysAddress += (LOCAL_RECONSTRUCTED_BUFFER_MAX_SIZE + misalignment);
	if (pDriverData->newHvaSramFreePhysAddress > (pDriverData->HvaSramBase + pDriverData->HvaSramSize)) {
		return H264ENCHARD_NO_HVA_ERAM_MEMORY;
	}
	dev_dbg(pDriverData->dev, "Stream 0x%x |-localRecBufPhysAddress = 0x%x\n", StreamId,
	        pDriverData->localRecBufPhysAddress);
	dev_dbg(pDriverData->dev, "Stream 0x%x |-LOCAL_RECONSTRUCTED_BUFFER_MAX_SIZE = %u bytes\n", StreamId,
	        LOCAL_RECONSTRUCTED_BUFFER_MAX_SIZE);
	dev_dbg(pDriverData->dev, "Stream 0x%x |-Hva esram end address is 0x%x\n", StreamId,
	        pDriverData->newHvaSramFreePhysAddress);
	dev_dbg(pDriverData->dev, "Stream 0x%x \n", StreamId);

	if (pDriverData->newHvaSramFreePhysAddress > pDriverData->HvaSramBase + pDriverData->HvaSramSize) {
		dev_err(pDriverData->dev,
		        "Stream 0x%x Error: esram overflow => newHvaSramFreePhysAddress 0x%x > 0x%x (HvaSramBase 0x%x + HvaSramSize %u bytes)\n",
		        StreamId,
		        pDriverData->newHvaSramFreePhysAddress, pDriverData->HvaSramBase + pDriverData->HvaSramSize, pDriverData->HvaSramBase,
		        pDriverData->HvaSramSize);
	}

	return H264ENCHARD_NO_ERROR;
}

/////////////////////////////////////////////////////////////////////////////////
//
//Free here all mono instance HVA related buffers
//Physical contiguous addresses are required

static void H264HwEncodeFreeSingletonHwBuffers(void)
{
	//
	//reset SRAM buffers base address
	//
	pDriverData->newHvaSramFreePhysAddress = pDriverData->platformData.BaseAddress[1];
}

/////////////////////////////////////////////////////////////////////////////////
//
//Allocate here all multi instance HVA related buffers
//Physical contiguous addresses are required

static H264EncodeHardStatus_t H264HwEncodeAllocateContextHwBuffers(H264EncodeHardCodecContext_t  *Context ,
                                                                   uint32_t MaxFrameWidth, uint32_t MaxFrameHeight)
{
	/*Temporary SDRAM buffer addresses (multi instance only) used for HVA internal processing before storage in instance context */
	unsigned int spatialContextBufAddress = 0;
	unsigned int temporalContextBufAddress = 0;
	unsigned int brcInOutBufAddress = 0;
	unsigned long spatialContextBufPhysAddress = 0;
	unsigned long temporalContextBufPhysAddress = 0;
	unsigned long brcInOutBufPhysAddress = 0;
	unsigned int paramOutAddress = 0;
	unsigned int taskDescAddress = 0;
	unsigned long referenceFrameBufPhysAddress = 0;
	unsigned long reconstructedFrameBufPhysAddress = 0;
	unsigned long paramOutPhysAddress = 0;
	unsigned long taskDescPhysAddress = 0;
	unsigned int sliceHeaderBufAddress = 0;
	unsigned long sliceHeaderBufPhysAddress = 0;
	char EncodePartitionName[MAX_PARTITION_NAME_SIZE];
	unsigned int StreamId = Context->StreamId;

	dev_dbg(pDriverData->dev, "Stream 0x%x Allocate hva context buffers for client %u\n", StreamId, Context->clientId);

	snprintf(EncodePartitionName, sizeof(EncodePartitionName), "%s-%d", HVA_MME_PARTITION, ((Context->clientId) % 2));
	EncodePartitionName[sizeof(EncodePartitionName) - 1] = '\0';

	//Record frame max dimension information in Context
	Context->maxFrameWidth = MaxFrameWidth;
	Context->maxFrameHeight = MaxFrameHeight;
	dev_dbg(pDriverData->dev, "Stream 0x%x Max frame dimension: width = %u pixels, height = %u pixels\n", StreamId,
	        MaxFrameWidth,
	        MaxFrameHeight);

	dev_dbg(pDriverData->dev, "Stream 0x%x [Hva context buffers]\n", StreamId);

	//Reference Frame with size=MBx*MBy*256*3/2 (addr_fw_ref_buffer) => Multi instance
	Context->referenceFrameBufSize = MaxFrameWidth * MaxFrameHeight * 3 / 2;
	// Reference frame is accessed only by HVA so allocate unmapped physical
	// memory.
	referenceFrameBufPhysAddress = (unsigned long)OSDEV_MallocPartitioned(EncodePartitionName,
	                                                                      Context->referenceFrameBufSize,
	                                                                      OSDEV_DEFAULT_PAGEALIGN);
	if (referenceFrameBufPhysAddress == 0) {
		dev_err(pDriverData->dev, "Stream 0x%x Error: reference frame buffer allocation failed\n", StreamId);
		return H264ENCHARD_NO_SDRAM_MEMORY;
	}
	Context->referenceFrameBufPhysAddress = HVA_SET_BUFFER_AS_SDRAM(referenceFrameBufPhysAddress);
	dev_dbg(pDriverData->dev, "Stream 0x%x |-referenceFrameBufSize            = %u bytes\n", StreamId,
	        Context->referenceFrameBufSize);
	dev_dbg(pDriverData->dev, "Stream 0x%x |-referenceFrameBufPhysAddress     = 0x%x\n", StreamId,
	        Context->referenceFrameBufPhysAddress);

	Context->spatialContextBufSize = H264_ENCODE_SPATIAL_DATA_SIZE;
	spatialContextBufAddress = (unsigned int) OSDEV_AllignedMallocHwBuffer(ENCODE_BUFFER_ALIGNMENT,
	                                                                       Context->spatialContextBufSize, EncodePartitionName, &spatialContextBufPhysAddress, UNCACHED_TYPE);
	if (spatialContextBufAddress == 0) {
		dev_err(pDriverData->dev, "Stream 0x%x Error: spatial context buffer allocation failed\n", StreamId);
		goto spatial_context_alloc_fail;
	}
	Context->temporalContextBufSize = H264_ENCODE_TEMPORAL_DATA_SIZE;
	temporalContextBufAddress = (unsigned int) OSDEV_AllignedMallocHwBuffer(ENCODE_BUFFER_ALIGNMENT,
	                                                                        Context->temporalContextBufSize, EncodePartitionName, &temporalContextBufPhysAddress, UNCACHED_TYPE);
	if (temporalContextBufAddress == 0) {
		dev_err(pDriverData->dev, "Stream 0x%x Error: temporal context buffer allocation failed\n", StreamId);
		goto temporal_context_alloc_fail;
	}
	Context->brcInOutBufSize = H264_ENCODE_BRC_DATA_SIZE;
	brcInOutBufAddress = (unsigned int) OSDEV_AllignedMallocHwBuffer(ENCODE_BUFFER_ALIGNMENT, Context->brcInOutBufSize,
	                                                                 EncodePartitionName, &brcInOutBufPhysAddress, UNCACHED_TYPE);
	if (brcInOutBufAddress == 0) {
		dev_err(pDriverData->dev, "Stream 0x%x Error: brc in/out buffer allocation failed\n", StreamId);
		goto brc_in_out_alloc_fail;
	}
	Context->spatialContextBufHandler = spatialContextBufAddress;
	Context->spatialContextBufPhysAddress = (unsigned int) spatialContextBufPhysAddress;
	Context->temporalContextBufHandler = temporalContextBufAddress;
	Context->temporalContextBufPhysAddress = (unsigned int) temporalContextBufPhysAddress;
	Context->brcInOutBufHandler = brcInOutBufAddress;
	Context->brcInOutBufPhysAddress = (unsigned int) brcInOutBufPhysAddress;
	//Init Buffer content with '0'
	memset((void *)spatialContextBufAddress, 0, Context->spatialContextBufSize);
	memset((void *)temporalContextBufAddress, 0, Context->temporalContextBufSize);
	memset((void *)brcInOutBufAddress, 0, Context->brcInOutBufSize);
	Context->spatialContextBufPhysAddress = HVA_SET_BUFFER_AS_SDRAM(Context->spatialContextBufPhysAddress);
	Context->temporalContextBufPhysAddress = HVA_SET_BUFFER_AS_SDRAM(Context->temporalContextBufPhysAddress);
	Context->brcInOutBufPhysAddress = HVA_SET_BUFFER_AS_SDRAM(Context->brcInOutBufPhysAddress);
	dev_dbg(pDriverData->dev, "Stream 0x%x |-spatialContextBufHandler         = 0x%x\n", StreamId,
	        Context->spatialContextBufHandler);
	dev_dbg(pDriverData->dev, "Stream 0x%x |-spatialContextBufSize            = %u bytes\n", StreamId,
	        Context->spatialContextBufSize);
	dev_dbg(pDriverData->dev, "Stream 0x%x |-spatialContextBufPhysAddress     = 0x%x\n", StreamId,
	        Context->spatialContextBufPhysAddress);
	dev_dbg(pDriverData->dev, "Stream 0x%x |-temporalContextBufHandler        = 0x%x\n", StreamId,
	        Context->temporalContextBufHandler);
	dev_dbg(pDriverData->dev, "Stream 0x%x |-temporalContextBufSize           = %u bytes\n", StreamId,
	        Context->temporalContextBufSize);
	dev_dbg(pDriverData->dev, "Stream 0x%x |-temporalContextBufPhysAddress    = 0x%x\n", StreamId,
	        Context->temporalContextBufPhysAddress);
	dev_dbg(pDriverData->dev, "Stream 0x%x |-brcInOutBufHandler               = 0x%x\n", StreamId,
	        Context->brcInOutBufHandler);
	dev_dbg(pDriverData->dev, "Stream 0x%x |-brcInOutBufSize                  = %u bytes\n", StreamId,
	        Context->brcInOutBufSize);
	dev_dbg(pDriverData->dev, "Stream 0x%x |-brcInOutBufPhysAddress           = 0x%x\n", StreamId,
	        Context->brcInOutBufPhysAddress);

	//ParamOut buffer with size = 8 * 4 + <number_of_slice> * 4 * 4 = 48 bytes per instance for one slice
	//As multi-slice encoding is not supported, paramOut structure only contains one slice
	Context->paramOutSize = sizeof(H264EncodeHardParamOut_t) + sizeof(H264EncodeHardParamOutSlice_t);
	paramOutAddress = (unsigned int) OSDEV_AllignedMallocHwBuffer(ENCODE_BUFFER_ALIGNMENT, Context->paramOutSize,
	                                                              EncodePartitionName, &paramOutPhysAddress, UNCACHED_TYPE);
	if (paramOutAddress == 0) {
		dev_err(pDriverData->dev, "Stream 0x%x Error: param out buffer allocation failed\n", StreamId);
		goto param_out_alloc_fail;
	}
	Context->paramOutHandler = paramOutAddress;
	Context->paramOutPhysAddress = (unsigned int) paramOutPhysAddress;
	Context->paramOutPhysAddress = HVA_SET_BUFFER_AS_SDRAM(Context->paramOutPhysAddress);
	memset((void *)paramOutAddress, 0, Context->paramOutSize);
	dev_dbg(pDriverData->dev, "Stream 0x%x |-paramOutHandler                  = 0x%x\n", StreamId,
	        Context->paramOutHandler);
	dev_dbg(pDriverData->dev, "Stream 0x%x |-paramOutSize                     = %u bytes\n", StreamId,
	        Context->paramOutSize);
	dev_dbg(pDriverData->dev, "Stream 0x%x |-paramOutPhysAddress              = 0x%x\n", StreamId,
	        Context->paramOutPhysAddress);

	//HVA Task descriptor that embeds all task programming
	Context->taskDescriptorSize = sizeof(H264EncodeHardTaskDescriptor_t);
	taskDescAddress = (unsigned int) OSDEV_AllignedMallocHwBuffer(ENCODE_BUFFER_ALIGNMENT, Context->taskDescriptorSize,
	                                                              EncodePartitionName, &taskDescPhysAddress, UNCACHED_TYPE);
	if (taskDescAddress == 0) {
		dev_err(pDriverData->dev, "Stream 0x%x Error: task descriptor buffer allocation failed\n", StreamId);
		goto task_desc_alloc_fail;
	}
	Context->taskDescriptorHandler = taskDescAddress;
	Context->taskDescriptorPhysAddress = (unsigned int) taskDescPhysAddress;
	Context->taskDescriptorPhysAddress = HVA_SET_BUFFER_AS_SDRAM(Context->taskDescriptorPhysAddress);
	memset((void *)taskDescAddress, 0, Context->taskDescriptorSize);
	dev_dbg(pDriverData->dev, "Stream 0x%x |-taskDescriptorHandler            = 0x%x\n", StreamId,
	        Context->taskDescriptorHandler);
	dev_dbg(pDriverData->dev, "Stream 0x%x |-taskDescriptorSize               = %u bytes\n", StreamId,
	        Context->taskDescriptorSize);
	dev_dbg(pDriverData->dev, "Stream 0x%x |-taskDescriptorPhysAddress        = 0x%x\n", StreamId,
	        Context->taskDescriptorPhysAddress);

	//Reconstructed Frame should also multi instantiated to enable reference by
	//Reconstructed Frame with size=MBx*MBy*256*3/2 (addr_fw_ref_buffer) => Multi instance
	Context->reconstructedFrameBufSize = MaxFrameWidth * MaxFrameHeight * 3 / 2;
	// Reconstructed frame is accessed only by HVA so allocate unmapped physical
	// memory.
	reconstructedFrameBufPhysAddress = (unsigned long) OSDEV_MallocPartitioned(EncodePartitionName,
	                                                                           Context->reconstructedFrameBufSize,
	                                                                           OSDEV_DEFAULT_PAGEALIGN);
	if (reconstructedFrameBufPhysAddress == 0) {
		dev_err(pDriverData->dev, "Stream 0x%x Error: reconstructed frame buffer allocation failed\n", StreamId);
		goto rec_buf_alloc_fail;
	}
	Context->reconstructedFrameBufPhysAddress = HVA_SET_BUFFER_AS_SDRAM(reconstructedFrameBufPhysAddress);
	dev_dbg(pDriverData->dev, "Stream 0x%x |-reconstructedFrameBufSize        = %u bytes\n", StreamId,
	        Context->reconstructedFrameBufSize);
	dev_dbg(pDriverData->dev, "Stream 0x%x |-reconstructedFrameBufPhysAddress = 0x%x\n", StreamId,
	        Context->reconstructedFrameBufPhysAddress);

	//Slice header
	Context->sliceHeaderBufSize = 4 * 16;
	sliceHeaderBufAddress = (unsigned int) OSDEV_AllignedMallocHwBuffer(ENCODE_BUFFER_ALIGNMENT,
	                                                                    Context->sliceHeaderBufSize, EncodePartitionName, &sliceHeaderBufPhysAddress, UNCACHED_TYPE);
	if (sliceHeaderBufAddress == 0) {
		dev_err(pDriverData->dev, "Stream 0x%x Error: slice header buffer allocation failed\n", StreamId);
		goto slice_header_alloc_fail;
	}
	Context->sliceHeaderBufHandler = sliceHeaderBufAddress;
	Context->sliceHeaderBufPhysAddress = (unsigned int) sliceHeaderBufPhysAddress;
	Context->sliceHeaderBufPhysAddress = HVA_SET_BUFFER_AS_SDRAM(Context->sliceHeaderBufPhysAddress);
	dev_dbg(pDriverData->dev, "Stream 0x%x |-sliceHeaderBufHandler            = 0x%x\n", StreamId,
	        Context->sliceHeaderBufHandler);
	dev_dbg(pDriverData->dev, "Stream 0x%x |-sliceHeaderBufSize               = %u bytes\n", StreamId,
	        Context->sliceHeaderBufSize);
	dev_dbg(pDriverData->dev, "Stream 0x%x |-sliceHeaderBufPhysAddress        = 0x%x\n", StreamId,
	        Context->sliceHeaderBufPhysAddress);

	//TODO add scaling matrix buffers
	//TODO add region of interest buffer
	//TODO add motion vector map buffer

	dev_dbg(pDriverData->dev, "Stream 0x%x \n", StreamId);

	return H264ENCHARD_NO_ERROR;

slice_header_alloc_fail:
	OSDEV_FreePartitioned(EncodePartitionName, (void *)reconstructedFrameBufPhysAddress);
rec_buf_alloc_fail:
	OSDEV_AllignedFreeHwBuffer((void *)taskDescAddress, EncodePartitionName);
task_desc_alloc_fail:
	OSDEV_AllignedFreeHwBuffer((void *)paramOutAddress, EncodePartitionName);
param_out_alloc_fail:
	OSDEV_AllignedFreeHwBuffer((void *)brcInOutBufAddress, EncodePartitionName);
brc_in_out_alloc_fail:
	OSDEV_AllignedFreeHwBuffer((void *)temporalContextBufAddress, EncodePartitionName);
temporal_context_alloc_fail:
	OSDEV_AllignedFreeHwBuffer((void *)spatialContextBufAddress, EncodePartitionName);
spatial_context_alloc_fail:
	OSDEV_FreePartitioned(EncodePartitionName, (void *)referenceFrameBufPhysAddress);

	return H264ENCHARD_NO_SDRAM_MEMORY;
}

/////////////////////////////////////////////////////////////////////////////////
//
//Free here all multi instance HVA related buffers

static void H264HwEncodeFreeContextHwBuffers(H264EncodeHardCodecContext_t  *Context)
{
	char EncodePartitionName[MAX_PARTITION_NAME_SIZE];
	unsigned int StreamId = Context->StreamId;
	unsigned long referenceFrameBufPhysAddress = 0;
	unsigned long reconstructedFrameBufPhysAddress = 0;

	snprintf(EncodePartitionName, sizeof(EncodePartitionName), "%s-%d", HVA_MME_PARTITION, ((Context->clientId) % 2));
	EncodePartitionName[sizeof(EncodePartitionName) - 1] = '\0';

	dev_dbg(pDriverData->dev, "Stream 0x%x Free hva context buffers for client %u\n", StreamId, Context->clientId);

	referenceFrameBufPhysAddress = HVA_SDRAM_BUFFER_TO_PHYS_ADDR(Context->referenceFrameBufPhysAddress);
	dev_dbg(pDriverData->dev, "Stream 0x%x Free reference frame buffer at physical address 0x%lx\n", StreamId,
	        referenceFrameBufPhysAddress);
	OSDEV_FreePartitioned(EncodePartitionName, (void *)referenceFrameBufPhysAddress);

	reconstructedFrameBufPhysAddress = HVA_SDRAM_BUFFER_TO_PHYS_ADDR(Context->reconstructedFrameBufPhysAddress);
	dev_dbg(pDriverData->dev, "Stream 0x%x Free reconstructed frame buffer at physical address 0x%lx\n", StreamId,
	        reconstructedFrameBufPhysAddress);
	OSDEV_FreePartitioned(EncodePartitionName, (void *)reconstructedFrameBufPhysAddress);

	dev_dbg(pDriverData->dev, "Stream 0x%x Free spatial context buffer at virtual address 0x%x\n", StreamId,
	        (unsigned int)Context->spatialContextBufHandler);
	OSDEV_AllignedFreeHwBuffer((void *)Context->spatialContextBufHandler, EncodePartitionName);

	dev_dbg(pDriverData->dev, "Stream 0x%x Free temporal context buffer at virtual address 0x%x\n", StreamId,
	        (unsigned int)Context->temporalContextBufHandler);
	OSDEV_AllignedFreeHwBuffer((void *)Context->temporalContextBufHandler, EncodePartitionName);

	dev_dbg(pDriverData->dev, "Stream 0x%x Free brc in/out buffer at virtual address 0x%x\n", StreamId,
	        (unsigned int)Context->brcInOutBufHandler);
	OSDEV_AllignedFreeHwBuffer((void *)Context->brcInOutBufHandler, EncodePartitionName);

	dev_dbg(pDriverData->dev, "Stream 0x%x Free output parameter buffer at virtual address 0x%x\n", StreamId,
	        (unsigned int)Context->paramOutHandler);
	OSDEV_AllignedFreeHwBuffer((void *)Context->paramOutHandler, EncodePartitionName);

	dev_dbg(pDriverData->dev, "Stream 0x%x Free task descriptor buffer at virtual address 0x%x\n", StreamId,
	        (unsigned int)Context->taskDescriptorHandler);
	OSDEV_AllignedFreeHwBuffer((void *)Context->taskDescriptorHandler, EncodePartitionName);

	dev_dbg(pDriverData->dev, "Stream 0x%x Free slice header buffer at virtual address 0x%x\n", StreamId,
	        (unsigned int)Context->sliceHeaderBufHandler);
	OSDEV_AllignedFreeHwBuffer((void *)Context->sliceHeaderBufHandler, EncodePartitionName);
}

/////////////////////////////////////////////////////////////////////////////////
//
//For debug puprose, display instance task descriptor

static void H264HwEncodeDisplayTaskDescriptor(H264EncodeHardCodecContext_t  *Context)
{
	H264EncodeHardTaskDescriptor_t *pTaskDescriptor = (H264EncodeHardTaskDescriptor_t *)Context->taskDescriptorHandler;
	unsigned int StreamId = Context->StreamId;

	dev_dbg(pDriverData->dev, "Stream 0x%x [Task descriptor for client %d]\n", StreamId, Context->clientId);
	dev_dbg(pDriverData->dev, "Stream 0x%x |-frameWidth                 = %u pixels\n", StreamId,
	        pTaskDescriptor->frameWidth);
	dev_dbg(pDriverData->dev, "Stream 0x%x |-frameHeight                = %u pixels\n", StreamId,
	        pTaskDescriptor->frameHeight);
	dev_dbg(pDriverData->dev, "Stream 0x%x |-windowWidth                = %u pixels\n", StreamId,
	        pTaskDescriptor->windowWidth);
	dev_dbg(pDriverData->dev, "Stream 0x%x |-windowHeight               = %u pixels\n", StreamId,
	        pTaskDescriptor->windowHeight);
	dev_dbg(pDriverData->dev, "Stream 0x%x |-windowHorizontalOffset     = %u pixels\n", StreamId,
	        pTaskDescriptor->windowHorizontalOffset);
	dev_dbg(pDriverData->dev, "Stream 0x%x |-windowVerticalOffset       = %u pixels\n", StreamId,
	        pTaskDescriptor->windowVerticalOffset);
	dev_dbg(pDriverData->dev, "Stream 0x%x |-pictureCodingType          = %u (%s)\n", StreamId,
	        pTaskDescriptor->pictureCodingType,
	        StringifyPictureCodingType(pTaskDescriptor->pictureCodingType));
	dev_dbg(pDriverData->dev, "Stream 0x%x |-reserved0                  = %u\n", StreamId, pTaskDescriptor->reserved0);
	dev_dbg(pDriverData->dev, "Stream 0x%x |-reserved1                  = %u\n", StreamId, pTaskDescriptor->reserved1);
	dev_dbg(pDriverData->dev, "Stream 0x%x |-picOrderCntType            = %u\n", StreamId,
	        pTaskDescriptor->picOrderCntType);
	dev_dbg(pDriverData->dev, "Stream 0x%x |-firstPictureInSequence     = %u\n", StreamId,
	        pTaskDescriptor->firstPictureInSequence);
	dev_dbg(pDriverData->dev, "Stream 0x%x |-useConstrainedIntraFlag    = %u\n", StreamId,
	        pTaskDescriptor->useConstrainedIntraFlag);
	dev_dbg(pDriverData->dev, "Stream 0x%x |-sliceSizeType              = %u\n", StreamId,
	        pTaskDescriptor->sliceSizeType);
	dev_dbg(pDriverData->dev, "Stream 0x%x |-sliceByteSize              = %u bytes\n", StreamId,
	        pTaskDescriptor->sliceByteSize);
	dev_dbg(pDriverData->dev, "Stream 0x%x |-sliceMbSize                = %u macroblocks\n", StreamId,
	        pTaskDescriptor->sliceMbSize);
	dev_dbg(pDriverData->dev, "Stream 0x%x |-intraRefreshType           = %u (%s)\n", StreamId,
	        pTaskDescriptor->intraRefreshType,
	        StringifyIntraRefreshType(pTaskDescriptor->intraRefreshType));
	if (pTaskDescriptor->intraRefreshType == HVA_ENCODE_DISABLE_INTRA_REFRESH) {
		dev_dbg(pDriverData->dev, "Stream 0x%x |-irParamOption              = %u\n", StreamId,
		        pTaskDescriptor->irParamOption);
	} else {
		dev_dbg(pDriverData->dev, "Stream 0x%x |-irParamOption              = %u %s\n", StreamId,
		        pTaskDescriptor->irParamOption,
		        (pTaskDescriptor->intraRefreshType == HVA_ENCODE_ADAPTIVE_INTRA_REFRESH) ? "macroblocks/frame refreshed" :
		        "frames (refresh period)");
	}
	dev_dbg(pDriverData->dev, "Stream 0x%x |-reserved2                  = %u\n", StreamId, pTaskDescriptor->reserved2);
	dev_dbg(pDriverData->dev, "Stream 0x%x |-disableDeblockingFilterIdc = %u (%s)\n", StreamId,
	        pTaskDescriptor->disableDeblockingFilterIdc,
	        StringifyDeblocking(pTaskDescriptor->disableDeblockingFilterIdc));
	dev_dbg(pDriverData->dev, "Stream 0x%x |-sliceAlphaC0OffsetDiv2     = %d\n", StreamId,
	        pTaskDescriptor->sliceAlphaC0OffsetDiv2);
	dev_dbg(pDriverData->dev, "Stream 0x%x |-sliceBetaOffsetDiv2        = %d\n", StreamId,
	        pTaskDescriptor->sliceBetaOffsetDiv2);
	dev_dbg(pDriverData->dev, "Stream 0x%x |-brcType                    = %d (%s)\n", StreamId,
	        pTaskDescriptor->brcType,
	        StringifyBrcType(pTaskDescriptor->brcType));
	dev_dbg(pDriverData->dev, "Stream 0x%x |-NonVCLNALUSize             = %u bits\n", StreamId,
	        pTaskDescriptor->nonVCLNALUSize);
	dev_dbg(pDriverData->dev, "Stream 0x%x |-cpbBufferSize              = %u bits\n", StreamId,
	        pTaskDescriptor->cpbBufferSize);
	dev_dbg(pDriverData->dev, "Stream 0x%x |-bitRate                    = %u bps\n", StreamId,
	        pTaskDescriptor->bitRate);
	dev_dbg(pDriverData->dev, "Stream 0x%x |-samplingMode               = %u (%s)\n", StreamId,
	        pTaskDescriptor->samplingMode,
	        StringifySamplingMode(pTaskDescriptor->samplingMode));
	dev_dbg(pDriverData->dev, "Stream 0x%x |-transformMode              = %u (%s)\n", StreamId,
	        pTaskDescriptor->transformMode,
	        StringifyTransformMode(pTaskDescriptor->transformMode));
	dev_dbg(pDriverData->dev, "Stream 0x%x |-encoderComplexity          = %u (%s)\n", StreamId,
	        pTaskDescriptor->encoderComplexity,
	        StringifyEncoderComplexity(pTaskDescriptor->encoderComplexity));
	dev_dbg(pDriverData->dev, "Stream 0x%x |-quant                      = %u\n", StreamId, pTaskDescriptor->quant);
	dev_dbg(pDriverData->dev, "Stream 0x%x |-framerateNum               = %u\n", StreamId,
	        pTaskDescriptor->framerateNum);
	dev_dbg(pDriverData->dev, "Stream 0x%x |-framerateDen               = %u\n", StreamId,
	        pTaskDescriptor->framerateDen);
	dev_dbg(pDriverData->dev, "Stream 0x%x |-delay                      = %u ms\n", StreamId, pTaskDescriptor->delay);
	dev_dbg(pDriverData->dev, "Stream 0x%x |-strictHRDCompliancy        = %u\n", StreamId,
	        pTaskDescriptor->strictHRDCompliancy);
	dev_dbg(pDriverData->dev, "Stream 0x%x |-qpmin                      = %u\n", StreamId, pTaskDescriptor->qpmin);
	dev_dbg(pDriverData->dev, "Stream 0x%x |-qpmax                      = %u\n", StreamId, pTaskDescriptor->qpmax);
	dev_dbg(pDriverData->dev, "Stream 0x%x |-addrSourceBuffer           = 0x%x\n", StreamId,
	        pTaskDescriptor->addrSourceBuffer);
	dev_dbg(pDriverData->dev, "Stream 0x%x |-addrFwdRefBuffer           = 0x%x\n", StreamId,
	        pTaskDescriptor->addrFwdRefBuffer);
	dev_dbg(pDriverData->dev, "Stream 0x%x |-addrRecBuffer              = 0x%x\n", StreamId,
	        pTaskDescriptor->addrRecBuffer);
	dev_dbg(pDriverData->dev, "Stream 0x%x |-addrOutputBitstreamStart   = 0x%x\n", StreamId,
	        pTaskDescriptor->addrOutputBitstreamStart);
	dev_dbg(pDriverData->dev, "Stream 0x%x |-addrOutputBitstreamEnd     = 0x%x\n", StreamId,
	        pTaskDescriptor->addrOutputBitstreamEnd);
	dev_dbg(pDriverData->dev, "Stream 0x%x |-bitstreamOffset            = %u bits\n", StreamId,
	        pTaskDescriptor->bitstreamOffset);
	dev_dbg(pDriverData->dev, "Stream 0x%x |-addrLctx                   = 0x%x\n", StreamId, pTaskDescriptor->addrLctx);
	dev_dbg(pDriverData->dev, "Stream 0x%x |-addrParamInout             = 0x%x\n", StreamId,
	        pTaskDescriptor->addrParamInout);
	dev_dbg(pDriverData->dev, "Stream 0x%x |-addrExternalSw             = 0x%x\n", StreamId,
	        pTaskDescriptor->addrExternalSw);
	dev_dbg(pDriverData->dev, "Stream 0x%x |-addrLocalRecBuffer         = 0x%x\n", StreamId,
	        pTaskDescriptor->addrLocalRecBuffer);
	dev_dbg(pDriverData->dev, "Stream 0x%x |-addrSpatialContext         = 0x%x\n", StreamId,
	        pTaskDescriptor->addrSpatialContext);
	dev_dbg(pDriverData->dev, "Stream 0x%x |-addrTemporalContext        = 0x%x\n", StreamId,
	        pTaskDescriptor->addrTemporalContext);
	dev_dbg(pDriverData->dev, "Stream 0x%x |-addrBrcInOutParameter      = 0x%x\n", StreamId,
	        pTaskDescriptor->addrBrcInOutParameter);
	dev_dbg(pDriverData->dev, "Stream 0x%x |-chromaQpIndexOffset        = %d\n", StreamId,
	        pTaskDescriptor->chromaQpIndexOffset);
	dev_dbg(pDriverData->dev, "Stream 0x%x |-entropyCodingMode          = %u (%s)\n", StreamId,
	        pTaskDescriptor->entropyCodingMode,
	        StringifyEntropyCodingMode(pTaskDescriptor->entropyCodingMode));
	dev_dbg(pDriverData->dev, "Stream 0x%x |-addrScalingMatrix          = 0x%x\n", StreamId,
	        pTaskDescriptor->addrScalingMatrix);
	dev_dbg(pDriverData->dev, "Stream 0x%x |-addrScalingMatrixDir       = 0x%x\n", StreamId,
	        pTaskDescriptor->addrScalingMatrixDir);
	dev_dbg(pDriverData->dev, "Stream 0x%x |-addrCabacContextBuffer     = 0x%x\n", StreamId,
	        pTaskDescriptor->addrCabacContextBuffer);
	dev_dbg(pDriverData->dev, "Stream 0x%x |-reserved3                  = %u\n", StreamId, pTaskDescriptor->reserved3);
	dev_dbg(pDriverData->dev, "Stream 0x%x |-reserved4                  = %u\n", StreamId, pTaskDescriptor->reserved4);
	dev_dbg(pDriverData->dev, "Stream 0x%x |-GmvX                       = %d pixels\n", StreamId,
	        pTaskDescriptor->GmvX);
	dev_dbg(pDriverData->dev, "Stream 0x%x |-GmvY                       = %d pixels\n", StreamId,
	        pTaskDescriptor->GmvY);
	dev_dbg(pDriverData->dev, "Stream 0x%x |-addrRoi                    = 0x%x\n", StreamId, pTaskDescriptor->addrRoi);
	dev_dbg(pDriverData->dev, "Stream 0x%x |-addrSliceHeader            = 0x%x\n", StreamId,
	        pTaskDescriptor->addrSliceHeader);
	dev_dbg(pDriverData->dev, "Stream 0x%x |-SliceHeaderSizeInBits      = %u bits\n", StreamId,
	        pTaskDescriptor->SliceHeaderSizeInBits);
	dev_dbg(pDriverData->dev, "Stream 0x%x |-SliceHeaderOffset0         = %u bits\n", StreamId,
	        pTaskDescriptor->SliceHeaderOffset0);
	dev_dbg(pDriverData->dev, "Stream 0x%x |-SliceHeaderOffset1         = %u bits\n", StreamId,
	        pTaskDescriptor->SliceHeaderOffset1);
	dev_dbg(pDriverData->dev, "Stream 0x%x |-SliceHeaderOffset2         = %u bits\n", StreamId,
	        pTaskDescriptor->SliceHeaderOffset2);
	dev_dbg(pDriverData->dev, "Stream 0x%x |-reserved5                  = 0x%x\n", StreamId,
	        pTaskDescriptor->reserved5);
	dev_dbg(pDriverData->dev, "Stream 0x%x |-reserved6                  = 0x%x\n", StreamId,
	        pTaskDescriptor->reserved6);
	dev_dbg(pDriverData->dev, "Stream 0x%x |-reserved7                  = %u\n", StreamId, pTaskDescriptor->reserved7);
	dev_dbg(pDriverData->dev, "Stream 0x%x |-reserved8                  = %u\n", StreamId, pTaskDescriptor->reserved8);
	dev_dbg(pDriverData->dev, "Stream 0x%x |-sliceSynchroEnable         = %u\n", StreamId,
	        pTaskDescriptor->sliceSynchroEnable);
	dev_dbg(pDriverData->dev, "Stream 0x%x |-maxSliceNumber             = %u\n", StreamId,
	        pTaskDescriptor->maxSliceNumber);
	dev_dbg(pDriverData->dev, "Stream 0x%x |-rgb2YuvYCoeff              = 0x%x\n", StreamId,
	        pTaskDescriptor->rgb2YuvYCoeff);
	dev_dbg(pDriverData->dev, "Stream 0x%x |-rgb2YuvUCoeff              = 0x%x\n", StreamId,
	        pTaskDescriptor->rgb2YuvUCoeff);
	dev_dbg(pDriverData->dev, "Stream 0x%x |-rgb2YuvVCoeff              = 0x%x\n", StreamId,
	        pTaskDescriptor->rgb2YuvVCoeff);
	dev_dbg(pDriverData->dev, "Stream 0x%x |-maxAirIntraMbNb            = %u intra macroblocks/frame\n", StreamId,
	        pTaskDescriptor->maxAirIntraMbNb);
	dev_dbg(pDriverData->dev, "Stream 0x%x |-brcNoSkip                  = %u\n", StreamId, pTaskDescriptor->brcNoSkip);
	dev_dbg(pDriverData->dev, "Stream 0x%x \n", StreamId);
}

static unsigned int H264HwEncodeBuildHVACommand(HVACmdType_t cmdType, H264EncodeHardCodecContext_t *Context,
                                                unsigned short taskId)
{
	unsigned int cmd;
	unsigned int StreamId = Context->StreamId;

	cmd = (unsigned int)(cmdType & 0xFF);
	cmd |= (Context->clientId << 8);
	cmd |= (taskId << 16);

	dev_dbg(pDriverData->dev, "Stream 0x%x [Launch hva command]\n", StreamId);
	dev_dbg(pDriverData->dev, "Stream 0x%x |-cmdType = 0x%x (%s)\n", StreamId, cmdType, StringifyHvaCmdType(cmdType));
	dev_dbg(pDriverData->dev, "Stream 0x%x |-clientId = %u\n", StreamId, Context->clientId);
	dev_dbg(pDriverData->dev, "Stream 0x%x |-taskId = %u\n", StreamId, taskId);
	dev_dbg(pDriverData->dev, "Stream 0x%x |-cmd = 0x%x\n", StreamId, cmd);
	dev_dbg(pDriverData->dev, "Stream 0x%x \n", StreamId);

	return cmd;
}

static H264EncodeHardStatus_t H264HwEncodeLaunchEncodeTask(H264EncodeHardCodecContext_t  *Context)
{
	volatile unsigned int fifoLevel = 0;
	unsigned int  hvaCmd = 0;
	unsigned int status = 0;
	unsigned long hva_command_timeout_in_ms;
	unsigned int StreamId = Context->StreamId;

	// hva_command_timeout_in_ms is the maximum time to wait before aborting sending
	// a new command to the hva in the event that the command fifo is full.
	// This allows to support more than "h264CmdFifoDepth" instances.
	// Today, as we only support real time encode, hva_command_timeout_in_ms is set to 1/framerate ms.
	// TODO Support non-real time encode
	hva_command_timeout_in_ms = Context->globalParameters.framerateDen * 1000 / Context->globalParameters.framerateNum;
	status = down_timeout(&h264CmdFifoDepth, msecs_to_jiffies(hva_command_timeout_in_ms));
	if (status == 0) {
		// Take our hardware lock for register access to the Fifo Queue.
		unsigned long flags;
		spin_lock_irqsave(&pDriverData->platformData.hw_lock, flags);

		//Build HVA command
		hvaCmd = H264HwEncodeBuildHVACommand(HVA_H264_ENC, Context, pDriverData->globalTaskId);
		pDriverData->globalTaskId++;
		(Context->instanceTaskId)++;

		// Assert no risk to overflow HVA command queue
		fifoLevel = ReadHvaRegister(HVA_HIF_REG_CFL);

		if ((fifoLevel & LEVEL_SFL_BITS_MASK) < HVA_CMD_FIFO_DEPTH) {
			//First write cmd value
			WriteHvaRegister(HVA_HIF_FIFO_CMD, hvaCmd);
			//Then write task descriptor address
			WriteHvaRegister(HVA_HIF_FIFO_CMD, Context->taskDescriptorPhysAddress);

			h264_frames_queued++; // Record statistics
		} else {
			// We don't release this semaphore here, as we have discovered
			// that we don't actually have enough resource ... so we become
			// ' Self - Limiting ' rather than simply discarding frames.
			// up(&h264CmdFifoDepth);

			status = -EINTR; // Enter the Error Handling below.
		}

		// Release our Hardware Lock
		spin_unlock_irqrestore(&pDriverData->platformData.hw_lock, flags);
	}

	/* This is not an if/else due to the error handling in the above statement */
	if (status != 0) {
		// Failed to insert a command on the hardware queue.
		Context->discardCommandCounter++;
		dev_err(pDriverData->dev, "Stream 0x%x Error: claim h264CmdFifoDepth semaphore failed\n", StreamId);
		return H264ENCHARD_CMD_DISCARDED;
	}

	return H264ENCHARD_NO_ERROR;
}

int32_t ComputeBufferFullness(H264EncodeHardCodecContext_t  *Context)
{
	uint32_t pictureSize = 0;
	H264EncodeHardRational_t depletion;
	H264EncodeHardFps_t framerate;
	H264EncodeHardRational_t firstPictDpl;
	H264EncodeHardTaskDescriptor_t *pTaskDescriptor = (H264EncodeHardTaskDescriptor_t *)Context->taskDescriptorHandler;
	unsigned int StreamId = Context->StreamId;

	dev_dbg(pDriverData->dev, "Stream 0x%x [Buffer fullnesss input]\n", StreamId);
	dev_dbg(pDriverData->dev, "Stream 0x%x |-bitRate             = %u bps\n", StreamId, pTaskDescriptor->bitRate);
	dev_dbg(pDriverData->dev, "Stream 0x%x |-framerateNum        = %u\n", StreamId, pTaskDescriptor->framerateNum);
	dev_dbg(pDriverData->dev, "Stream 0x%x |-framerateDen        = %u\n", StreamId, pTaskDescriptor->framerateDen);
	dev_dbg(pDriverData->dev, "Stream 0x%x |-statusRemovalTime   = %u\n", StreamId, Context->statusRemovalTime);
	dev_dbg(pDriverData->dev, "Stream 0x%x |-statusBitstreamSize = %u bytes\n", StreamId, Context->statusBitstreamSize);
	dev_dbg(pDriverData->dev, "Stream 0x%x |-nonVCLNALUSize      = %u bytes\n", StreamId,
	        pTaskDescriptor->nonVCLNALUSize / 8);
	dev_dbg(pDriverData->dev, "Stream 0x%x \n", StreamId);

	pictureSize = 8 * Context->statusBitstreamSize + pTaskDescriptor->nonVCLNALUSize + (Context->statusStuffingBits ?
	                                                                                    Context->statusStuffingBits + FILLER_DATA_HEADER_SIZE * 8 : 0);
	framerate.num = pTaskDescriptor->framerateNum;
	framerate.den = pTaskDescriptor->framerateDen;
	if (pTaskDescriptor->firstPictureInSequence) {
		BRC_ASSIGN_VALUE_TO_A_WITH_BASE((pTaskDescriptor->cpbBufferSize * 9) / 10, depletion, pTaskDescriptor->framerateNum);
		BRC_SET_DEPLETION(depletion, framerate, pTaskDescriptor->bitRate);
		firstPictDpl.base = depletion.base;
		firstPictDpl.div = depletion.div;
		firstPictDpl.rem = depletion.rem;
		BRC_A_TIMES_N(firstPictDpl, Context->statusRemovalTime);
		BRC_SUB_VALUE_FROM_A(firstPictDpl, pictureSize);
		Context->bufferFullness.base = firstPictDpl.base;
		Context->bufferFullness.div = firstPictDpl.div;
		Context->bufferFullness.rem = firstPictDpl.rem;
		Context->lastRemovalTime = Context->statusRemovalTime;
	} else {
		BRC_SET_DEPLETION(depletion, framerate, pTaskDescriptor->bitRate);
		do {
			BRC_ADD_B_TO_A(Context->bufferFullness, depletion);
			Context->lastRemovalTime++;
		} while (Context->lastRemovalTime < Context->statusRemovalTime);
		BRC_SUB_VALUE_FROM_A(Context->bufferFullness, pictureSize);
	}

	dev_dbg(pDriverData->dev, "Stream 0x%x Estimated buffer fullness is %d bits\n", StreamId,
	        BRC_GET_INTEGER_PART(Context->bufferFullness));

	if (Context->transformStatus == HVA_ENCODE_FRAME_SKIPPED) {
		BRC_ADD_VALUE_FROM_A(Context->bufferFullness, pictureSize);
	}

	return BRC_GET_INTEGER_PART(Context->bufferFullness);
}

static void H264HwEncodeBrcPostprocessing(H264EncodeHardCodecContext_t *Context)
{
	H264EncodeHardSequenceParams_t *SequenceParams = &Context->globalParameters;
	H264EncodeHardFrameParams_t *FrameParams = &Context->FrameParams;
	H264EncodeHardTaskDescriptor_t *pTaskDescriptor = (H264EncodeHardTaskDescriptor_t *)Context->taskDescriptorHandler;
	unsigned int StreamId = Context->StreamId;
	int i = 0;
	int bufferFullness = 0;
	unsigned int frameSize = 8 * Context->statusBitstreamSize + pTaskDescriptor->nonVCLNALUSize +
	                         (Context->statusStuffingBits ? Context->statusStuffingBits + FILLER_DATA_HEADER_SIZE * 8 : 0);
	unsigned int newZone = 0;
	H264EncodeDirection_t direction = UP;
	int newQp = 0;
	int qpMaxUp = 0;
	int qpMinUp = 0;
	int qpMinDown = 0;
	int qpMaxDown = 0;
	int qpDirection = UP;
	int qpAverage = 0;
	int bufferFullnessAverage = 0;

	// CODED PICTURE BUFFER FULLNESS
	bufferFullness = ComputeBufferFullness(Context);
	dev_dbg(pDriverData->dev, "Stream 0x%x Buffer fullness is %d bits\n", StreamId, bufferFullness);

	// If first frame has been encoded in vbr with a coded picture
	// buffer overflow, it could lead to encoding quality drop.
	// TODO re-encode frame by increasing qp until 51
	if (pTaskDescriptor->firstPictureInSequence && (bufferFullness < 0)) {
		dev_err(pDriverData->dev,
		        "Stream 0x%x Error: cpb overflow, buffer fullness is %d bits, qp is %u, cpb size is %u bits\n", StreamId,
		        bufferFullness, pTaskDescriptor->quant, pTaskDescriptor->cpbBufferSize);
	}

	DumpBrcData(Context->brcInOutBufHandler, StreamId);

	dev_dbg(pDriverData->dev, "Stream 0x%x [BRC CONTEXT]\n", StreamId);
	dev_dbg(pDriverData->dev, "Stream 0x%x |-frame type is %s\n", StreamId,
	        StringifyPictureCodingType(FrameParams->pictureCodingType));
	dev_dbg(pDriverData->dev, "Stream 0x%x |-frame status is %s\n", StreamId,
	        StringifyTransformStatus(Context->transformStatus));
	dev_dbg(pDriverData->dev, "Stream 0x%x |-frameCount %u\n", StreamId, Context->frameCount);
	dev_dbg(pDriverData->dev, "Stream 0x%x |-qp %u\n", StreamId, Context->qp);
	dev_dbg(pDriverData->dev, "Stream 0x%x |-bufferFullness %d\n", StreamId, bufferFullness);

	// BRC WINDOW SIZE PROCESSING
	// Shift picture size array
	for (i = BRC_WINDOW_SIZE - 1 ; i > 0 ; i--) {
		Context->brc.lastPicSizeInBits[i] = Context->brc.lastPicSizeInBits[i - 1];
	}
	// Update last picture size
	if ((Context->transformStatus == HVA_ENCODE_FRAME_SKIPPED) && (SequenceParams->framerateNum != 0)) {
		Context->brc.lastPicSizeInBits[0] = (SequenceParams->bitRate * SequenceParams->framerateDen) /
		                                    SequenceParams->framerateNum;
	} else {
		// Brc regulation on intra frame
		if ((FrameParams->pictureCodingType == HVA_ENCODE_I_FRAME) && ((Context->brc.lastGopSize + 5) != 0)) {
			// Weight intra frame size respect to gop size
			// Hypothesis: intra size is approximately 5 times the inter size with gop size of 25
			Context->brc.lastPicSizeInBits[0] = frameSize * 6 / (Context->brc.lastGopSize + 5);
			Context->brc.lastGopSize = 1;
		} else {
			Context->brc.lastPicSizeInBits[0] = frameSize;
			Context->brc.lastGopSize++;
		}
	}
	HackBrcWindowPicStat(Context->brcInOutBufHandler, BrcWindowPicSizeAverageInBits(Context, StreamId), StreamId);
	dev_dbg(pDriverData->dev, "Stream 0x%x |-frameSize %u (brc hack %u)\n", StreamId, frameSize,
	        Context->brc.lastPicSizeInBits[0]);
	dev_dbg(pDriverData->dev, "Stream 0x%x |-BrcWindowPicSizeAverageInBits %u\n", StreamId,
	        BrcWindowPicSizeAverageInBits(Context, StreamId));


	// BRC SAMPLING PERIOD PROCESSING
	direction = (bufferFullness > Context->brc.lastBufferFullness) ? UP : DOWN;
	for (i = BRC_WINDOW_SIZE - 1 ; i > 0 ; i--) {
		Context->brc.lastPicWithFillerData[i] = Context->brc.lastPicWithFillerData[i - 1];
	}
	// Update last picture filler data
	if (Context->transformStatus == HVA_ENCODE_FRAME_SKIPPED) {
		Context->brc.lastPicWithFillerData[0] = 0;
	} else {
		Context->brc.lastPicWithFillerData[0] = (Context->statusStuffingBits ? 1 : 0);
	}
	// ZONE
	if ((BrcWindowFillerDataDetected(Context, StreamId) && (direction == UP)) ||
	    ((10 * bufferFullness > (int)SequenceParams->cpbBufferSize * 9) && (direction == DOWN))) {
		newZone = Context->brc.zoneNb - 1;
	} else { //TODO In case of scene change detection
		for (newZone = 0 ; newZone < Context->brc.zoneNb ; newZone++) {
			if (bufferFullness <= Context->brc.zone[newZone].maxBufferFullness[direction]) {
				break;
			}
		}
	}

	// If buffer fullness is less than 90% of cpb and frame size is more than 4 times the average bitrate per frame, brc sampling period is set to 1
	if ((10 * bufferFullness <= (int)SequenceParams->cpbBufferSize * 9) &&
	    (frameSize * SequenceParams->framerateNum > 4 * SequenceParams->bitRate * SequenceParams->framerateDen)) {
		Context->brc.samplingPeriod = 1;
	} else {
		Context->brc.samplingPeriod = Context->brc.zone[newZone].samplingPeriod;
	}

	// Adapt qpmin to increase quality on low complexity sequences
	if (Context->brc.zoneNb == BRC_MAX_ZONE) {
		if (newZone == (BRC_MAX_ZONE - 2)) {
			// On middle zone, update qpmin to get buffer fullness between 40%-60% pivotal point
			if (Context->brc.samplingPeriod > 0) {
				if (Context->brc.frameCountOnZone % Context->brc.samplingPeriod == 0) {
					if ((10 * bufferFullness > SequenceParams->cpbBufferSize * 6) &&
					    (BrcWindowPicSizeAverageInBits(Context, StreamId) * SequenceParams->framerateNum >
					     SequenceParams->bitRate * SequenceParams->framerateDen)) {
						Context->brc.zone[newZone].qpMin = max((int)Context->brc.zone[newZone].qpMin - 1,
						                                       (int)Context->qp + (int)Context->brc.samplingQpDelta - MAX_QP_MIN_DELTA_DOWN);
						Context->brc.zone[newZone].qpMin = max((int)Context->brc.zone[newZone].qpMin, (int)SequenceParams->qpmin);
					} else if ((10 * bufferFullness < SequenceParams->cpbBufferSize * 4) &&
					           (BrcWindowPicSizeAverageInBits(Context, StreamId) * SequenceParams->framerateNum <
					            SequenceParams->bitRate * SequenceParams->framerateDen)) {
						Context->brc.zone[newZone].qpMin = H264_ENC_OPTIMAL_QP_MIN;
					}
				}
			}
		} else {
			// Reset qpmin when moving to other zones
			Context->brc.zone[BRC_MAX_ZONE - 2].qpMin = H264_ENC_OPTIMAL_QP_MIN;
		}
		dev_dbg(pDriverData->dev, "Stream 0x%x |-qpMin on current zone %d\n", StreamId, Context->brc.zone[newZone].qpMin);
	}

	// When moving from top to lower zone, next minimum qp setting is limited to current qp to avoid quality gap
	Context->qpmin = min(Context->qp + Context->brc.samplingQpDelta + MAX_QP_MIN_DELTA_UP,
	                     Context->brc.zone[newZone].qpMin);
	Context->qpmax = Context->brc.zone[newZone].qpMax;

	if ((SequenceParams->cpbBufferSize > 0) && (Context->frameCount > 0)) {
		Context->brc.accumulatedBufferFullness += bufferFullness * 100 / SequenceParams->cpbBufferSize;
		bufferFullnessAverage = Context->brc.accumulatedBufferFullness / Context->frameCount;
		dev_dbg(pDriverData->dev, "Stream 0x%x |-bufferFullnessAverage %d\n", StreamId, bufferFullnessAverage);
	}

	if (Context->brc.samplingPeriod != 0) {
		if (Context->brc.frameCountOnZone % Context->brc.samplingPeriod == 0) { // old brcSamplingPeriod should be used ?
			Context->brc.samplingQpUpdated = false; // ask for qp update
			if ((FrameParams->pictureCodingType == HVA_ENCODE_I_FRAME) && (Context->transformStatus == HVA_ENCODE_OK)) {
				Context->brc.forceSampling = true;
			}
		}
	}
	if ((Context->brc.samplingQpUpdated == false) && (FrameParams->pictureCodingType == HVA_ENCODE_P_FRAME)) {
		Context->brc.samplingQp = Context->qp;
		Context->brc.samplingQpUpdated = true;
		Context->brc.forceSampling = false;
	}
	if (Context->transformStatus == HVA_ENCODE_FRAME_SKIPPED) {
		Context->brc.lastSkippedFrameCount++;
	} else {
		Context->brc.lastSkippedFrameCount = 0;
	}
	if (Context->brc.lastZone != newZone) { // Change on sampling period only
		Context->brc.frameCountOnZone = 0;
	} else {
		Context->brc.frameCountOnZone++;
	}
	Context->brc.lastZone = newZone;
	Context->brc.lastBufferFullness = bufferFullness;
	dev_dbg(pDriverData->dev, "Stream 0x%x |-BrcWindowFillerDataDetected %u\n", StreamId,
	        BrcWindowFillerDataDetected(Context, StreamId));
	dev_dbg(pDriverData->dev, "Stream 0x%x |-direction %s\n", StreamId, StringifyDirection(direction));
	dev_dbg(pDriverData->dev, "Stream 0x%x |-zone %u\n", StreamId, newZone);
	dev_dbg(pDriverData->dev, "Stream 0x%x |-zoneNb %u\n", StreamId, Context->brc.zoneNb);
	dev_dbg(pDriverData->dev, "Stream 0x%x |-samplingPeriod %u\n", StreamId, Context->brc.samplingPeriod);
	dev_dbg(pDriverData->dev, "Stream 0x%x |-qpmin %u\n", StreamId, Context->qpmin);
	dev_dbg(pDriverData->dev, "Stream 0x%x |-qpmax %u\n", StreamId, Context->qpmax);
	dev_dbg(pDriverData->dev, "Stream 0x%x |-forceSampling %u\n", StreamId, Context->brc.forceSampling);
	dev_dbg(pDriverData->dev, "Stream 0x%x |-samplingQp %u (updated %u)\n", StreamId, Context->brc.samplingQp,
	        Context->brc.samplingQpUpdated);
	dev_dbg(pDriverData->dev, "Stream 0x%x |-frameCountOnZone %u\n", StreamId, Context->brc.frameCountOnZone);
	dev_dbg(pDriverData->dev, "Stream 0x%x |-lastSkippedFrameCount %u\n", StreamId, Context->brc.lastSkippedFrameCount);

	// BRC FILTER PROCESSING
	// Dynamic hysteresis F = f(qp) with 20 < qp < 50 and 0 < F < 4
	// qpMinUp = 35, qpMaxUp = 50, 20 < qpMinDown < 35 and 35 < qpMaxDown < 50 according to current zone
	if (SequenceParams->cpbBufferSize > 0) {
		newQp = Context->qp + Context->brc.samplingQpDelta;
		if (newQp != Context->brc.lastQp) {
			qpMinUp = Context->brc.zone[newZone].qpHysteresisMin[UP];
			qpMaxUp = Context->brc.zone[newZone].qpHysteresisMax[UP];
			qpMinDown = Context->brc.zone[newZone].qpHysteresisMin[DOWN];
			qpMaxDown = Context->brc.zone[newZone].qpHysteresisMax[DOWN];
			qpDirection = (newQp > Context->brc.lastQp) ? UP : DOWN;
			if ((qpDirection == UP) && (qpMaxUp != qpMinUp)) {
				Context->brc.nextFilterFromQp = max(0, max((newQp - qpMinUp) * MAX_FILTER_LEVEL / (qpMaxUp - qpMinUp),
				                                           FrameParams->filterLevel));
			} else if ((qpDirection == DOWN) && (qpMaxDown != qpMinDown)) {
				Context->brc.nextFilterFromQp = max(0, min((newQp - qpMinDown) * MAX_FILTER_LEVEL / (qpMaxDown - qpMinDown),
				                                           FrameParams->filterLevel));
			}
			dev_dbg(pDriverData->dev, "Stream 0x%x |-qpMaxUp %d\n", StreamId, qpMaxUp);
			dev_dbg(pDriverData->dev, "Stream 0x%x |-qpMinUp %d\n", StreamId, qpMinUp);
			dev_dbg(pDriverData->dev, "Stream 0x%x |-qpMinDown %d\n", StreamId, qpMinDown);
			dev_dbg(pDriverData->dev, "Stream 0x%x |-qpMaxDown %d\n", StreamId, qpMaxDown);
			dev_dbg(pDriverData->dev, "Stream 0x%x |-qpDirection %s\n", StreamId, StringifyDirection(qpDirection));
		}
		dev_dbg(pDriverData->dev, "Stream 0x%x |-newQp %d\n", StreamId, newQp);
		dev_dbg(pDriverData->dev, "Stream 0x%x |-lastQp %u\n", StreamId, Context->brc.lastQp);
		dev_dbg(pDriverData->dev, "Stream 0x%x |-nextFilterFromQp %d\n", StreamId, Context->brc.nextFilterFromQp);
		Context->brc.lastQp = newQp;
	}

	// Filtering to limit number of successive skipped frame
	Context->brc.nextFilterFromQpSkip = min((int)Context->brc.lastSkippedFrameCount, MAX_FILTER_LEVEL);
	dev_dbg(pDriverData->dev, "Stream 0x%x |-nextFilterFromQpSkip %d\n", StreamId, Context->brc.nextFilterFromQpSkip);

	// Global filtering to deal with high qp average
	// in order to get qp between 25 and 35 on 40%-60% BF pivotal point
	if (Context->frameCount > 0) {
		Context->brc.accumulatedQp += Context->qp + Context->brc.samplingQpDelta;
		qpAverage = Context->brc.accumulatedQp / Context->frameCount;
		if ((Context->frameCount % BRC_CONVERGENCE_PERIOD) == 1) {
			if ((qpAverage > H264_ENC_OPTIMAL_QP_MAX)) {
				Context->brc.nextFilterFromQpAverage = min((int)Context->brc.nextFilterFromQpAverage + 1, MAX_FILTER_LEVEL);
			} else if (qpAverage < H264_ENC_OPTIMAL_QP_MIN) {
				Context->brc.nextFilterFromQpAverage = max(0, (int)Context->brc.nextFilterFromQpAverage - 1);
			}
		}
		dev_dbg(pDriverData->dev, "Stream 0x%x |-qpAverage %d\n", StreamId, qpAverage);
		dev_dbg(pDriverData->dev, "Stream 0x%x |-nextFilterFromQpAverage %d\n", StreamId,
		        Context->brc.nextFilterFromQpAverage);
	}

	Context->nextFilterLevel = max3(Context->brc.nextFilterFromQp, Context->brc.nextFilterFromQpSkip,
	                                Context->brc.nextFilterFromQpAverage);
	dev_dbg(pDriverData->dev, "Stream 0x%x |-nextFilterLevel %d\n", StreamId, Context->nextFilterLevel);
	dev_dbg(pDriverData->dev, "Stream 0x%x |-filterLevel %d\n", StreamId, FrameParams->filterLevel);
	dev_dbg(pDriverData->dev, "Stream 0x%x |-sceneChange %u\n", StreamId, FrameParams->sceneChange);
}

/////////////////////////////////////////////////////////////////////////////////
//
//Update current context / task descriptor at the end of the encode task
//  *  mv_toggle parameter: should toggle between 0-1 between each frame encode (whatever encode status)
//  *  swap reconstructed and reference buffer if previous encode successful (in particular , no frame skipped)

static void H264HwEncodeUpdateContextAtEncodeEnd(H264EncodeHardCodecContext_t  *Context)
{
	unsigned int tempAddress = 0;
	H264EncodeHardSequenceParams_t *SequenceParams = &Context->globalParameters;
	H264EncodeHardFrameParams_t *FrameParams = &Context->FrameParams;
	H264EncodeHardTaskDescriptor_t *pTaskDescriptor = (H264EncodeHardTaskDescriptor_t *)Context->taskDescriptorHandler;
	unsigned int StreamId = Context->StreamId;

	dev_dbg(pDriverData->dev, "Stream 0x%x Update context at encode end\n", StreamId);
	dev_dbg(pDriverData->dev, "Stream 0x%x idrToggle = %u, mvToggle = %u, lastIdrPicId = %d\n", StreamId,
	        Context->idrToggle, Context->mvToggle,
	        Context->lastIdrPicId);

	Context->qp = GetHwQp(Context->brcInOutBufHandler, StreamId);

	if (SequenceParams->brcType == HVA_ENCODE_VBR) {
		H264HwEncodeBrcPostprocessing(Context);
	}

	if (Context->transformStatus == HVA_ENCODE_OK) {
		Context->frameCount++;
		if (FrameParams->pictureCodingType == HVA_ENCODE_I_FRAME) {
			Context->lastIdrPicId = Context->idrToggle;
		} else {
			Context->lastIdrPicId = -1;
		}
	}

	//should swap buffers in task descriptor IF frame not skipped:
	//reconstructed and ref frame Phys address for a given instance!
	if (Context->transformStatus == HVA_ENCODE_OK) {
		dev_dbg(pDriverData->dev, "Stream 0x%x [Swap rec <=> ref buffers]\n", StreamId);
		tempAddress = Context->referenceFrameBufPhysAddress;
		Context->referenceFrameBufPhysAddress     = Context->reconstructedFrameBufPhysAddress;
		Context->reconstructedFrameBufPhysAddress = tempAddress;
		dev_dbg(pDriverData->dev, "Stream 0x%x |-referenceFrameBufPhysAddress     = 0x%x => 0x%x\n", StreamId,
		        Context->reconstructedFrameBufPhysAddress, Context->referenceFrameBufPhysAddress);
		dev_dbg(pDriverData->dev, "Stream 0x%x |-reconstructedFrameBufPhysAddress = 0x%x => 0x%x\n", StreamId,
		        Context->referenceFrameBufPhysAddress,
		        Context->reconstructedFrameBufPhysAddress);
		dev_dbg(pDriverData->dev, "Stream 0x%x \n", StreamId);
		pTaskDescriptor->addrFwdRefBuffer         = Context->referenceFrameBufPhysAddress;
		pTaskDescriptor->addrRecBuffer            = Context->reconstructedFrameBufPhysAddress;
	}

}

static void H264HwEncodeHVAStaticConfig(unsigned int StreamId)
{
	unsigned long flags;
	unsigned int StBusPlugConfig;

	dev_dbg(pDriverData->dev, "Stream 0x%x Disable clock gating\n", StreamId);
	dev_dbg(pDriverData->dev, "Stream 0x%x Define max opcode size and max message size for esram and ddr\n", StreamId);

	spin_lock_irqsave(&pDriverData->platformData.hw_lock, flags);
	WriteHvaRegister(HVA_HIF_REG_CLK_GATING, HVA_HIF_REG_CLK_GATING_HVC_CLK_EN |
	                 HVA_HIF_REG_CLK_GATING_HEC_CLK_EN |
	                 HVA_HIF_REG_CLK_GATING_HJE_CLK_EN);
	StBusPlugConfig = ReadHvaRegister(HVA_HIF_REG_MIF_CFG);
	if (StBusPlugConfig != HVA_HIF_MIF_CFG_RECOMMENDED_VALUE) {
		WriteHvaRegister(HVA_HIF_REG_MIF_CFG, HVA_HIF_MIF_CFG_RECOMMENDED_VALUE);
	}
	spin_unlock_irqrestore(&pDriverData->platformData.hw_lock, flags);

	if (StBusPlugConfig != HVA_HIF_MIF_CFG_RECOMMENDED_VALUE) {
		dev_warn(pDriverData->dev, "Stream 0x%x Incorrect stbus plug configuration, StBusPlugConfig = 0x%x\n", StreamId,
		         StBusPlugConfig);
		dev_warn(pDriverData->dev,
		         "Stream 0x%x Force stbus plug configuration to recommended value 0x%x\n", StreamId,
		         HVA_HIF_MIF_CFG_RECOMMENDED_VALUE);
	}
}

// Function to set all hva static registers configuration
void H264HwEncodeSetRegistersConfig(unsigned int StreamId)
{
	dev_dbg(pDriverData->dev, "Stream 0x%x Reset hva and configure registers\n", StreamId);

	H264HwEncodeHVAStaticConfig(StreamId);

	dev_info(pDriverData->dev, "Stream 0x%x Reset hva and configure registers done\n", StreamId);
}

//{{{  H264HwEncodeCheckFramerate
//{{{  doxynote
/// \brief      Check and correct frame rate parameters to prevent overflow (Bug 23112)
///
/// \return     H264EncodeHardStatus_t
//}}}

static void H264HwEncodeCheckFramerate(H264EncodeHardCodecContext_t  *Context)
{
	H264EncodeHardTaskDescriptor_t *pTaskDescriptor = (H264EncodeHardTaskDescriptor_t *)Context->taskDescriptorHandler;
	unsigned int StreamId = Context->StreamId;
	uint32_t maxBitstreamSize;
	uint32_t i;
	uint32_t BitstreamSize;
	uint32_t framerateNum;
	uint32_t framerateDen;
	uint32_t maxframerateNum;
	uint64_t bitrateNum;
	uint64_t maxbitrateNum;
	uint32_t framerateNumOpt;
	uint32_t framerateDenOpt;
	uint32_t framerateNumRef;
	uint32_t framerateDenRef;
	bool overflowNum, overflowDen;
	uint64_t bitrateDen;
	uint64_t maxbitrateDen;

	// Previous frame rate to limit the frame rate correction frequency
	framerateNum = (uint32_t)Context->globalParameters.framerateNum;
	framerateDen = (uint32_t)Context->globalParameters.framerateDen;

	// Ref frame rate to compute the new optimal frame rate so that we do not lose accuracy in previous rounding
	framerateNumRef = (uint32_t)Context->framerateNumRef;
	framerateDenRef = (uint32_t)Context->framerateDenRef;

	overflowNum = false;
	overflowDen = false;

	dev_dbg(pDriverData->dev, "Stream 0x%x Framerate is (%d/%d) fps\n", StreamId, framerateNum, framerateDen);
	dev_dbg(pDriverData->dev, "Stream 0x%x Reference framerate is (%d/%d) fps\n", StreamId, framerateNumRef,
	        framerateDenRef);

	// Checking overflow for framerateDen
	bitrateDen = (uint64_t) Context->globalParameters.bitRate * framerateDen;
	maxbitrateDen = 1;
	maxbitrateDen <<= OVERFLOW_LIMIT_DEN;
	maxbitrateDen -= 1;
	if (bitrateDen >= maxbitrateDen) {
		dev_dbg(pDriverData->dev,
		        "Stream 0x%x Overflow bitrateDen %llu (bitRate %d * framerateDen %d) >= maxbitrateDen %llu\n", StreamId,
		        bitrateDen, Context->globalParameters.bitRate, framerateDen, maxbitrateDen);
		overflowDen = true;
	}

	if (overflowDen) {

		// optimal accuracy
		framerateDenOpt = (int32_t)maxbitrateDen / (Context->globalParameters.bitRate);
		framerateNumOpt = ((framerateNumRef * framerateDenOpt) + (framerateDenRef >> 1)) / framerateDenRef;

		dev_dbg(pDriverData->dev, "Stream 0x%x Optimal framerate num is %d\n", StreamId, framerateNumOpt);
		dev_dbg(pDriverData->dev, "Stream 0x%x Optimal framerate den is %d\n", StreamId, framerateDenOpt);
		Context->globalParameters.framerateNum = (uint16_t)framerateNumOpt;
		Context->globalParameters.framerateDen = (uint16_t)framerateDenOpt;

		framerateNum = framerateNumOpt;
		framerateDen = framerateDenOpt;
	}

	// Checking overflow for framerateNum
	maxBitstreamSize = Context->PastBitstreamSize[0];
	for (i = 1; i < 4; i++) {
		if (Context->PastBitstreamSize[i] > maxBitstreamSize) {
			maxBitstreamSize = Context->PastBitstreamSize[i];
		}
	}

	//if(maxBitstreamSize>(1<<22))
	//    dev_err(pDriverData->dev, "Stream 0x%x Error: Max picture ES size exceeded: maxBitstreamSize %d\n", StreamId, maxBitstreamSize );

	bitrateNum = (uint64_t) maxBitstreamSize * framerateNum;
	maxbitrateNum = 1;
	maxbitrateNum <<= OVERFLOW_LIMIT_NUM;
	maxbitrateNum -= 1;
	if (bitrateNum >= maxbitrateNum) {
		dev_dbg(pDriverData->dev,
		        "Stream 0x%x Overflow bitrateNum %llu (maxBitstreamSize %d * framerateNum %d) >= maxbitrateNum %llu\n", StreamId,
		        bitrateNum, maxBitstreamSize, framerateNum, maxbitrateNum);
		overflowNum = true;
	}

	if (!overflowDen && !overflowNum) {
		pTaskDescriptor->framerateNum = Context->globalParameters.framerateNum;
		pTaskDescriptor->framerateDen = Context->globalParameters.framerateDen;
		return;
	}

	if (overflowNum) {

		// compute number of bit used bitstream
		// simplify to prevent a division > 32 bits
		BitstreamSize = maxBitstreamSize;
		i = 0;
		while (BitstreamSize > 0) {
			BitstreamSize >>= 1;
			i++;
		}
		maxframerateNum = (1 << ((int32_t)OVERFLOW_LIMIT_NUM - i)) - 1;
		//maxframerateNum = maxbitrateNum;
		//do_div(maxframerateNum, maxBitstreamSize);

		dev_dbg(pDriverData->dev, "Stream 0x%x Max bitstream size is %d (%d bits representation)\n", StreamId,
		        maxBitstreamSize, i);
		dev_dbg(pDriverData->dev, "Stream 0x%x Max framerate num is %d vs %d (%d/%d))\n", StreamId, maxframerateNum,
		        (uint32_t)maxbitrateNum / maxBitstreamSize,
		        (uint32_t)maxbitrateNum, maxBitstreamSize);

		// optimal accuracy
		framerateDenOpt = (framerateDenRef * maxframerateNum) / framerateNumRef;
		framerateNumOpt = ((framerateNumRef * framerateDenOpt) + (framerateDenRef >> 1)) / framerateDenRef;

		dev_dbg(pDriverData->dev, "Stream 0x%x Optimal framerate num is %d\n", StreamId, framerateNumOpt);
		dev_dbg(pDriverData->dev, "Stream 0x%x Optimal framerate den is %d\n", StreamId, framerateDenOpt);
		Context->globalParameters.framerateNum = (uint16_t)framerateNumOpt;
		Context->globalParameters.framerateDen = (uint16_t)framerateDenOpt;
	}

	pTaskDescriptor->framerateNum = Context->globalParameters.framerateNum;
	pTaskDescriptor->framerateDen = Context->globalParameters.framerateDen;
}

static void H264HwEncodeCheckBitrateAndCpbSize(H264EncodeHardCodecContext_t  *Context)
{
	unsigned int StreamId = Context->StreamId;
	value_scale_deconstruction_t deconstructed_parameter;
	uint32_t reconstructed_parameter;

	if (Context->globalParameters.vuiParametersPresentFlag) {
		// Compute bit rate
		deconstructed_parameter = H264HwEncodeDeconstructBitrate(Context->bitrateRef);
		reconstructed_parameter = H264HwEncodeReconstructBitrate(deconstructed_parameter.value, deconstructed_parameter.scale);
		dev_dbg(pDriverData->dev, "Stream 0x%x Bitrate is %d bps, reconstructed bitrate is %d bps\n", StreamId,
		        Context->bitrateRef,
		        reconstructed_parameter);
		if (reconstructed_parameter != Context->bitrateRef) {
			Context->globalParameters.bitRate = reconstructed_parameter;
			dev_dbg(pDriverData->dev, "Stream 0x%x Bitrate is %d bps, reconstructed bitrate is %d bps\n", StreamId,
			        Context->bitrateRef,
			        reconstructed_parameter);
		}

		// Compute cpb buffer size
		deconstructed_parameter = H264HwEncodeDeconstructCpbSize(Context->cpbBufferSizeRef);
		reconstructed_parameter = H264HwEncodeReconstructCpbSize(deconstructed_parameter.value, deconstructed_parameter.scale);
		dev_dbg(pDriverData->dev, "Stream 0x%x Cpb size is %d bits, reconstructed cpb size is %d bits\n", StreamId,
		        Context->cpbBufferSizeRef,
		        reconstructed_parameter);
		if (reconstructed_parameter != Context->cpbBufferSizeRef) {
			Context->globalParameters.cpbBufferSize = reconstructed_parameter;
			dev_dbg(pDriverData->dev, "Stream 0x%x Cpb size is %d bits, reconstructed cpb size is %d bits\n", StreamId,
			        Context->cpbBufferSizeRef,
			        reconstructed_parameter);
		}
	}
}

static void H264HwEncodeGetFrameDimensionFromProfile(H264EncodeHardInitParams_t *InitParams, uint32_t *maxFrameWidth,
                                                     uint32_t *maxFrameHeight)
{
	unsigned int StreamId = InitParams->StreamId;

	switch (InitParams->MemoryProfile) {
	case HVA_ENCODE_PROFILE_CIF:
		*maxFrameWidth  = 352;
		*maxFrameHeight = 288;
		break;
	case HVA_ENCODE_PROFILE_SD:
		*maxFrameWidth  = 720;
		*maxFrameHeight = 576;
		break;
	case HVA_ENCODE_PROFILE_720P:
		*maxFrameWidth  = 1280;
		*maxFrameHeight = 720;
		break;
	case HVA_ENCODE_PROFILE_HD:
		*maxFrameWidth  = 1920;
		*maxFrameHeight = 1088;
		break;
	default:
		dev_err(pDriverData->dev, "Stream 0x%x Error: unknown memory profile, use 1080p frame size\n", StreamId);
		*maxFrameWidth  = 1920;
		*maxFrameHeight = 1088;
		break;
	}
}

int HvaPowerOn(unsigned int StreamId)
{
	int ret = 0;
#ifdef CONFIG_PM_RUNTIME
	dev_dbg(pDriverData->dev, "Stream 0x%x Call pm_runtime_get_sync() service\n", StreamId);
	if (pm_runtime_get_sync(pDriverData->dev) < 0) {
		dev_err(pDriverData->dev, "Stream 0x%x Error: pm_runtime_get_sync failed\n", StreamId);
		ret = -EINVAL;
	}
#endif

	return ret;
}

void HvaPowerOff(unsigned int StreamId)
{
#ifdef CONFIG_PM_RUNTIME
	dev_dbg(pDriverData->dev, "Stream 0x%x Call pm_runtime_put() service\n", StreamId);
	pm_runtime_mark_last_busy(pDriverData->dev);
	pm_runtime_put(pDriverData->dev);
#endif
}

//function to enable HVA clock
int HvaClkOn(unsigned int StreamId)
{
#ifdef CONFIG_STM_VIRTUAL_PLATFORM
	return 0;
#else
	int ret = 0;

	dev_dbg(pDriverData->dev, "Stream 0x%x Call clk_enable() service\n", StreamId);
	ret = clk_enable(pDriverData->clk);
	if (ret) {
		dev_err(pDriverData->dev, "Stream 0x%x Error: clock enable failed (%d)\n", StreamId, ret);
		return ret;
	}

	return ret;
#endif
}

//function to disable HVA clock
void HvaClkOff(unsigned int StreamId)
{
#ifdef CONFIG_STM_VIRTUAL_PLATFORM

#else
	dev_dbg(pDriverData->dev, "Stream 0x%x Call clk_disable() service\n", StreamId);
	clk_disable(pDriverData->clk);
#endif
}
