/************************************************************************
Copyright (C) 2003-2014 STMicroelectronics. All Rights Reserved.

This file is part of the Streaming Engine.

Streaming Engine is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License version 2 as
published by the Free Software Foundation.

Streaming Engine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with Streaming Engine; see the file COPYING.  If not, write to the Free
Software Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307,
USA.

The Streaming Engine Library may alternatively be licensed under a
proprietary license from ST.
************************************************************************/

#include <linux/module.h>
#include <linux/device.h>
#include <linux/platform_device.h>
#include <linux/init.h>    /* Initialisation support */
#include <linux/kernel.h>  /* Kernel support */
#include <linux/clk.h>
#include <linux/err.h>
#include <linux/of.h>
#include <linux/suspend.h>
#include <linux/pm_runtime.h>
#include <linux/pm.h>

#include "osdev_mem.h"
#include "osdev_device.h"

#include "hva_registers.h"
#include "h264_encode.h"
#include "h264_encode_hard_host_transformer.h"

#include "H264ENCHW_VideoTransformerTypes.h"

#define MODULE_NAME "H264 encode hardware host transformer"
#define MAJOR_NUMBER 249

static char *mmeName = HVA_DEVICE_NAME;

MODULE_DESCRIPTION("H264 encode hardware cell platform driver");
MODULE_AUTHOR("STMicroelectronics");
MODULE_LICENSE("GPL");

module_param(mmeName , charp, S_IRUGO);
MODULE_PARM_DESC(mmeName, "Name to use for MME Transformer registration");

static int HvaInit(void);
static int stm_pm_hva_notifier_call(struct notifier_block *this, unsigned long event, void *ptr);

/* uncomment this to print CLK_HVA clock tree values */
//#define DISPLAY_CLKVALUES
static void DisplayClkValues(void);
static int HvaClockSetRate(struct device_node *of_node);

//Global hva driver data
struct HvaDriverData *pDriverData;

// PM notifier callback
// At entry of low power, multicom is terminated in .freeze callback. When system exit from
// low power, multicom is initialized through pm_notifier, so we have to register HVA transformer
// again. After HVA pm_notifier, SE module notifier would be called in which other multicom apis
// are called.
// This callback is used to register the HVA transformer after low power exit
#define ENTRY(enum) case enum: return #enum
static inline const char *StringifyPmEvent(int aPmEvent)
{
	switch (aPmEvent) {
		ENTRY(PM_HIBERNATION_PREPARE);
		ENTRY(PM_POST_HIBERNATION);
		ENTRY(PM_SUSPEND_PREPARE);
		ENTRY(PM_POST_SUSPEND);
		ENTRY(PM_RESTORE_PREPARE);
		ENTRY(PM_POST_RESTORE);
	default: return "<unknown pm event>";
	}
}


static int HvaClockSetRate(struct device_node *of_node)
{
	int ret = 0;

	(void)of_node; // warning removal

	/* check incase 0 is set for max frequency property in DT */
	if (pDriverData->max_freq) {
		ret = clk_set_rate(pDriverData->clk, pDriverData->max_freq);
		if (ret) {
			dev_err(pDriverData->dev, "Error: setting max frequency failed (%d)\n", ret);
			return -EINVAL;
		}
		dev_info(pDriverData->dev, "Hva clock set to %u\n", pDriverData->max_freq);
	}
	return ret;
}

static int stm_pm_hva_notifier_call(struct notifier_block *this, unsigned long event, void *ptr)
{
	(void)this; // warning removal
	(void)ptr; // warning removal

	switch (event) {
	case PM_POST_SUSPEND:
	// fallthrough
	case PM_POST_HIBERNATION:
		dev_info(pDriverData->dev, "Hva notifier: %s\n", StringifyPmEvent(event));
		break;

	default:
		break;
	}

	return NOTIFY_DONE;
}

static int HvaInit(void)
{
	int ret = 0;
	if (HvaPowerOn(0)) {
		dev_err(pDriverData->dev, "Error: power on failed\n");
		return -EINVAL;
	}

	if (HvaClkOn(0)) {
		dev_err(pDriverData->dev, "Error: clock on failed\n");
		ret = -EINVAL;
		goto fail_clockon_ver;
	}

	pDriverData->HvaIPVersion = ReadHvaRegister(HVA_HIF_REG_VERSION);
	dev_info(pDriverData->dev, "Hva version is 0x%x\n", pDriverData->HvaIPVersion);

	H264HwEncodeSetRegistersConfig(0);

	DisplayClkValues();

	HvaClkOff(0);
	HvaPowerOff(0);

	if (pDriverData->HvaIPVersion != HVA_HIF_REG_EXPECTED_VERSION) {
		dev_dbg(pDriverData->dev, "Incorrect hva version 0x%x, expected version 0x%x\n", pDriverData->HvaIPVersion,
		        HVA_HIF_REG_EXPECTED_VERSION);
	}

	return ret;

fail_clockon_ver:
	HvaPowerOff(0);
	return ret;
}

MME_ERROR initTransformer(void *context, MME_TransformerInitParams_t *initParams)
{
	H264EncodeHardHandle_t      encodeHandle = (H264EncodeHardHandle_t) context;
	H264EncodeHardInitParams_t *InitParams = (H264EncodeHardInitParams_t *) initParams->TransformerInitParams_p;
	H264EncodeHardStatus_t status;

	// We can ignore the "params" and give back any value for a handle.
	if (context == NULL) {
		return MME_INVALID_HANDLE;
	}

	status = H264HwEncodeInit(encodeHandle, InitParams);

	switch (status) {
	case H264ENCHARD_NO_ERROR:
		return MME_SUCCESS;
	case H264ENCHARD_ERROR:
		return MME_INTERNAL_ERROR;
	case H264ENCHARD_NO_SDRAM_MEMORY:
		return MME_NOMEM;
	case H264ENCHARD_NO_HVA_ERAM_MEMORY:
		return MME_NOMEM;
	default:
		return MME_INTERNAL_ERROR;
	}
}

MME_ERROR closeTransformer(void *context)
{
	H264EncodeHardHandle_t      encodeHandle = (H264EncodeHardHandle_t) context;
	H264EncodeHardStatus_t status;

	// We can ignore the "params" and give back any value for a handle.
	if (context == NULL) {
		return MME_INVALID_HANDLE;
	}

	status = H264HwEncodeClose(encodeHandle);

	switch (status) {
	case H264ENCHARD_NO_ERROR:
		return MME_SUCCESS;
	case H264ENCHARD_ERROR:
		return MME_INTERNAL_ERROR;
	case H264ENCHARD_NO_SDRAM_MEMORY:
		return MME_NOMEM;
	case H264ENCHARD_NO_HVA_ERAM_MEMORY:
		return MME_NOMEM;
	default:
		return MME_INTERNAL_ERROR;
	}
}

static OSDEV_OpenEntrypoint(HvaOpen)
{
	void *HvaContext = NULL;
	OSDEV_OpenEntry();

	// Obtain an initialized encoder context
	HvaContext = kzalloc(sizeof(H264EncodeHardCodecContext_t), GFP_KERNEL);
	if (HvaContext == NULL) {
		dev_err(pDriverData->dev, "Error: memory allocation for context structure failed\n");
		OSDEV_OpenExit(OSDEV_Error);
	}

	OSDEV_PrivateData = HvaContext;

	OSDEV_OpenExit(OSDEV_NoError);
}

void FillMMEInfo(H264EncodeHard_AddInfo_CommandStatus_t *AddInfo_p, H264EncodeHardCodecContext_t    *h264CodecContext)
{
	AddInfo_p->StructSize          = sizeof(H264EncodeHard_AddInfo_CommandStatus_t);
	AddInfo_p->bitstreamSize       = h264CodecContext->statusBitstreamSize;
	AddInfo_p->removalTime         = h264CodecContext->statusRemovalTime;
	AddInfo_p->stuffingBits        = h264CodecContext->statusStuffingBits;
	AddInfo_p->nonVCLNALUSize      = h264CodecContext->statusNonVCLNALUSize;
	AddInfo_p->transformStatus     = h264CodecContext->transformStatus;
	AddInfo_p->frameEncodeDuration = h264CodecContext->frameEncodeDuration;
	AddInfo_p->hvcEncodeDuration   = h264CodecContext->hvcEncodeDuration;
	AddInfo_p->qp                  = h264CodecContext->qp;
	AddInfo_p->nextFilterLevel     = h264CodecContext->nextFilterLevel;
}

MME_ERROR processCommand(void *context, MME_Command_t *cmd)
{
	H264EncodeHard_AddInfo_CommandStatus_t *AddInfo_p = cmd->CmdStatus.AdditionalInfo_p;
	H264EncodeHardCodecContext_t    *h264CodecContext = (H264EncodeHardCodecContext_t *)context;
	H264EncodeHardStatus_t               encodeStatus = H264ENCHARD_ERROR;
	MME_ERROR status = MME_SUCCESS;
	unsigned int StreamId = h264CodecContext->StreamId;

	switch (cmd->CmdCode) {
	case MME_TRANSFORM:
		OSDEV_CopyToDeviceSpace(&h264CodecContext->FrameParams, (unsigned int)cmd->Param_p, cmd->ParamSize);

		if (HvaPowerOn(StreamId)) {
			dev_err(pDriverData->dev, "Stream 0x%x Error: power on failed\n", StreamId);
			cmd->CmdStatus.Error = MME_COMMAND_ABORTED;
			return MME_COMMAND_ABORTED;
		}

		if (HvaClkOn(StreamId)) {
			dev_err(pDriverData->dev, "Stream 0x%x Error: clock on failed\n", StreamId);
			HvaPowerOff(StreamId);
			cmd->CmdStatus.Error = MME_COMMAND_ABORTED;
			return MME_COMMAND_ABORTED;
		}

		encodeStatus = H264HwEncodeEncodeFrame((H264EncodeHardHandle_t)context);
		if (encodeStatus != H264ENCHARD_NO_ERROR) {
			dev_err(pDriverData->dev, "Stream 0x%x Error: encode frame failed 0x%x (%s)\n", StreamId, encodeStatus,
			        StringifyEncodeStatus(encodeStatus));
			//Fill AddInfo field of CmdStatus with output parameter of encode task
			FillMMEInfo(AddInfo_p, h264CodecContext);
			HvaClkOff(StreamId);
			HvaPowerOff(StreamId);
			cmd->CmdStatus.Error = MME_COMMAND_ABORTED;
			return MME_COMMAND_ABORTED;
		}

		HvaClkOff(StreamId);
		HvaPowerOff(StreamId);

		// Update the AdditionalInfo
		FillMMEInfo(cmd->CmdStatus.AdditionalInfo_p, h264CodecContext);

		cmd->CmdStatus.Error = MME_SUCCESS;
		break;

	case MME_SET_PARAMS:
		OSDEV_CopyToDeviceSpace(&h264CodecContext->globalParameters, (unsigned int)cmd->Param_p, cmd->ParamSize);
		encodeStatus = H264HwEncodeSetSequenceParams((H264EncodeHardHandle_t)context);
		if (encodeStatus != H264ENCHARD_NO_ERROR) {
			dev_err(pDriverData->dev, "Stream 0x%x Error: set sequence parameters failed 0x%x (%s)\n", StreamId, encodeStatus,
			        StringifyEncodeStatus(encodeStatus));
			cmd->CmdStatus.Error = MME_COMMAND_ABORTED;
			return MME_COMMAND_ABORTED;
		}
		cmd->CmdStatus.Error = MME_SUCCESS;
		break;

	default:
		dev_err(pDriverData->dev, "Stream 0x%x Error: return status MME_NOT_IMPLEMENTED inside multicom callback\n",
		        StreamId);
		cmd->CmdStatus.Error = MME_NOT_IMPLEMENTED;
		return MME_NOT_IMPLEMENTED;
	}

	return status;
}

MME_ERROR getTransformerCapability(MME_TransformerCapability_t *capability)
{
	H264EncodeHard_TransformerCapability_t *pCap = (H264EncodeHard_TransformerCapability_t *)capability->TransformerInfo_p;

	capability->Version = 0x1;

	pCap = (H264EncodeHard_TransformerCapability_t *)capability->TransformerInfo_p;

	pCap->MaxXSize                    = H264_ENCODE_MAX_SIZE_X;
	pCap->MaxYSize                    = H264_ENCODE_MAX_SIZE_Y;
	pCap->BufferAlignment             = ENCODE_BUFFER_ALIGNMENT;
	pCap->areBFramesSupported         = false;
	pCap->MaxFrameRate                = 60;
	pCap->isT8x8Supported             = true;
	pCap->isSliceLossSupported        = false;
	pCap->isMultiSliceEncodeSupported = true;
	pCap->isIntraRefreshSupported     = true;

	capability->TransformerInfoSize = sizeof(H264EncodeHard_TransformerCapability_t);

	return MME_SUCCESS;
}

MME_ERROR termTransformer(void *context)
{
	H264EncodeHardStatus_t encodeStatus;
	H264EncodeHardCodecContext_t *h264CodecContext = (H264EncodeHardCodecContext_t *)context;
	unsigned int StreamId = h264CodecContext->StreamId;

	encodeStatus = H264HwEncodeTerminate((H264EncodeHardHandle_t)context);

	dev_dbg(pDriverData->dev, "Stream 0x%x Delete one encode instance: termTransformer called\n", StreamId);

	if (encodeStatus == H264ENCHARD_NO_ERROR) {
		return MME_SUCCESS;
	} else {
		return MME_INTERNAL_ERROR;
	}
}

static OSDEV_IoctlEntrypoint(HvaIoctl)
{
	MME_ERROR status;
	OSDEV_Status_t Status = OSDEV_NoError;
	struct H264EncodeHardCodecContext_t *HvaContext;

	OSDEV_IoctlEntry();
	HvaContext = (struct H264EncodeHardCodecContext_t *)OSDEV_PrivateData;
	BUG_ON(!HvaContext);
	switch (OSDEV_IoctlCode) {
	case MME_INIT:
		status = initTransformer(HvaContext, (MME_TransformerInitParams_t *)OSDEV_ParameterAddress);
		if (status != MME_SUCCESS) {
			dev_err(pDriverData->dev, "Error: hva init transformer failed\n");
			Status = OSDEV_Error;
		}
		break;

	case MME_GET_CAPABILITY:
		status = getTransformerCapability((MME_TransformerCapability_t *)OSDEV_ParameterAddress);
		if (status != MME_SUCCESS) {
			dev_err(pDriverData->dev, "Error: hva get transformer capability failed\n");
			Status = OSDEV_Error;
		}
		break;

	case MME_TRANSFORM:
	case MME_SET_PARAMS:
		status = processCommand(HvaContext, (MME_Command_t *)OSDEV_ParameterAddress);
		if (status != MME_SUCCESS) {
			dev_err(pDriverData->dev, "Error: %s Unable to process command\n", __func__);
			Status = OSDEV_Error;
		}
		break;

	case MME_TERM:
		status = termTransformer(HvaContext);
		if (status != MME_SUCCESS) {
			dev_err(pDriverData->dev, "Error: hva deinit transformer failed\n");
			Status = OSDEV_Error;
		}
		break;

	default:
		dev_err(pDriverData->dev, "Error: %s Invalid ioctl %08x\n", __func__, OSDEV_IoctlCode);
		Status = OSDEV_Error;
		break;
	}

	OSDEV_IoctlExit(Status);
}

static OSDEV_CloseEntrypoint(HvaClose)
{
	MME_ERROR status;
	struct H264EncodeHardCodecContext_t *HvaContext;

	OSDEV_CloseEntry();

	HvaContext = (struct H264EncodeHardCodecContext_t *)OSDEV_PrivateData;

	BUG_ON(!HvaContext);

	status = closeTransformer(HvaContext);
	if (status != MME_SUCCESS) {
		dev_err(pDriverData->dev, "Error: hva close transformer failed\n");
		OSDEV_CloseExit(OSDEV_Error);
	}

	OSDEV_CloseExit(OSDEV_NoError);
}

/* this API is for debug purpose to print clk values for stih407/410 board*/
static void DisplayClkValues()
{
#ifdef DISPLAY_CLKVALUES
	int err = 0;
	unsigned long clk_sysin_rate = 0;
	unsigned long clk_co_ref_rate = 0;
	unsigned long clk_co_pll1_rate = 0;
	unsigned long clk_hva_rate = 0;
	struct clk *clk_sysin = 0;
	struct clk *clk_co_ref = 0;
	struct clk *clk_co_pll1 = 0;
	struct clk *clk_hva = 0;

	dev_info(pDriverData->dev, "[Hva clock tree]\n");

	err = clk_add_alias("clk_sysin", NULL, "CLK_SYSIN", NULL);
	if (err) {
		dev_err(pDriverData->dev, "Error: add clock alias failed (%d)\n", err);
		return;
	}
	clk_sysin = devm_clk_get(pDriverData->dev, "clk_sysin");
	if (!clk_sysin) {
		dev_err(pDriverData->dev, "Error: get clock handle failed\n");
		return;
	}
	clk_sysin_rate = clk_get_rate(clk_sysin);
	dev_info(pDriverData->dev, "|-clk_sysin_rate   = %lu Hz\n", clk_sysin_rate);

	err = clk_add_alias("clk_co_ref", NULL, "CLK_C0_REF", NULL);
	if (err) {
		dev_err(pDriverData->dev, "Error: add clock alias failed (%d)\n", err);
		return;
	}
	clk_co_ref = devm_clk_get(pDriverData->dev, "clk_co_ref");
	if (!clk_sysin) {
		dev_err(pDriverData->dev, "Error: get clock handle failed\n");
		return;
	}
	clk_co_ref_rate = clk_get_rate(clk_co_ref);
	dev_info(pDriverData->dev, "|-clk_co_ref_rate  = %lu Hz\n", clk_co_ref_rate);

	err = clk_add_alias("clk_co_pll1", NULL, "CLK_C0_PLL1", NULL);
	if (err) {
		dev_err(pDriverData->dev, "Error: add clock alias failed (%d)\n", err);
		return;
	}
	clk_co_pll1 = devm_clk_get(pDriverData->dev, "clk_co_pll1");
	if (!clk_sysin) {
		dev_err(pDriverData->dev, "Error: get clock handle failed\n");
		return;
	}
	clk_co_pll1_rate = clk_get_rate(clk_co_pll1);
	dev_info(pDriverData->dev, "|-clk_co_pll1_rate = %lu Hz\n", clk_co_pll1_rate);

	err = clk_add_alias("clk_hva", NULL, "CLK_HVA", NULL);
	if (err) {
		dev_err(pDriverData->dev, "Error: add clock alias failed (%d)\n", err);
		return;
	}
	clk_hva = devm_clk_get(pDriverData->dev, "clk_hva");
	if (!clk_sysin) {
		dev_err(pDriverData->dev, "Error: get clock handle failed\n");
		return;
	}
	clk_hva_rate = clk_get_rate(clk_hva);
	dev_info(pDriverData->dev, "|-clk_hva_rate     = %lu Hz\n", clk_hva_rate);
	dev_info(pDriverData->dev, "\n");
#endif
}

#ifdef CONFIG_PM
// PM suspend callback
// For Gateway profile: called on Host Passive Standby entry (HPS entry)
// For STB profile: called on Controller Passive Standby entry (CPS entry)
static int HvaPmSuspend(struct device *dev)
{
	(void)dev; // warning removal
	dev_info(pDriverData->dev, "Hva pm suspend\n");

	// Here we assume that there are no more jobs in progress.
	// This is ensured at SE level, which has already entered "low power state", in PM notifier callback of player2 module (on PM_SUSPEND_PREPARE event).
	// nothing to be done here, clk On/Off is managed dynamically at each task level
	return 0;
}

// PM resume callback
// For Gateway profile: called on Host Passive Standby exit (HPS exit)
// For STB profile: called on Controller Passive Standby exit (CPS exit)
static int HvaPmResume(struct device *dev)
{
	(void)dev; // warning removal
	dev_info(pDriverData->dev, "Hva pm resume\n");

	// Clock on to verify version, set static configuration of registers and clock off
	HvaInit();

	// Here we assume that no new jobs have been posted yet.
	// This is ensured at SE level, which will exit "low power state" later on, in PM notifier callback of player2 module (on PM_POST_SUSPEND event).
	// nothing to be done here, clk On/Off is managed dynamically at each task level
	return 0;
}

#ifdef CONFIG_PM_RUNTIME
// runtime_suspend callback for Active Standby
static int HvaPmRuntimeSuspend(struct device *dev)
{
	(void)dev; // warning removal
	dev_dbg(pDriverData->dev, "Hva pm runtime suspend\n");

	// Nothing to do, since HVA clocks are managed dynamically
	return 0;
}

// runtime_resume callback for Active Standby
static int HvaPmRuntimeResume(struct device *dev)
{
	(void)dev; // warning removal
	dev_dbg(pDriverData->dev, "Hva pm runtime resume\n");

	// Nothing to do, since HVA clocks are managed dynamically
	return 0;
}
#endif // CONFIG_PM_RUNTIME
#endif // CONFIG_PM

//Store platform specific information in platformData
static int HvaGetPlatformInfo(struct platform_device *pdev)
{
	struct resource *pResource;

	if (pdev == NULL) {
		dev_err(pDriverData->dev, "Error: incorrect platform device pointer\n");
		return -EINVAL;
	}
	if (!pdev->name) {
		dev_err(pDriverData->dev, "Error: incorrect platform device name\n");
		return -EINVAL;
	}
	//retrieve HVA register base address from platform file
	pResource = platform_get_resource_byname(pdev, IORESOURCE_MEM, "hva_registers");
	if (pResource) {
		pDriverData->platformData.BaseAddress[0] = pResource->start;
		pDriverData->platformData.Size[0] = resource_size(pResource) ;
		dev_info(pDriverData->dev, "Hva registers base address is 0x%x and size is 0x%x\n",
		         pDriverData->platformData.BaseAddress[0],
		         pDriverData->platformData.Size[0]);
	} else {
		dev_err(pDriverData->dev, "Error: can't retrieve hva register base address and size\n");
		return -EINVAL;
	}

	//retrieve ESRAM base address from platform file
	pResource = platform_get_resource_byname(pdev, IORESOURCE_MEM, "hva_esram");
	if (pResource) {
		pDriverData->platformData.BaseAddress[1] = pResource->start;
		pDriverData->platformData.Size[1] = resource_size(pResource);
		dev_info(pDriverData->dev, "Hva esram base address is 0x%x and size is 0x%x\n",
		         pDriverData->platformData.BaseAddress[1],
		         pDriverData->platformData.Size[1]);
	} else {
		dev_err(pDriverData->dev, "Error: can't retrieve hva esram base address\n");
		return -EINVAL;
	}

	//retrieve HVA ITS_IRQ interrupt number from platform file
	pDriverData->platformData.Interrupt[0] = platform_get_irq_byname(pdev, "hva_its_irq");
	if (pDriverData->platformData.Interrupt[0]) {
		dev_info(pDriverData->dev, "Hva interrupt number is %d\n", pDriverData->platformData.Interrupt[0]);
	} else {
		dev_err(pDriverData->dev, "Error: can't retrieve hva status interrupt number\n");
		return -EINVAL;
	}

	//retrieve HVA ERR_IRQ interrupt number from platform file
	pDriverData->platformData.Interrupt[1] = platform_get_irq_byname(pdev, "hva_err_irq");
	if (pDriverData->platformData.Interrupt[1]) {
		dev_info(pDriverData->dev, "Hva error interrupt number is %d\n", pDriverData->platformData.Interrupt[1]);
	} else {
		dev_err(pDriverData->dev, "Error: can't retrieve hva error interrupt number\n");
		return -EINVAL;
	}

	// We must have a lock per HVA device.
	spin_lock_init(&pDriverData->platformData.hw_lock);

	return 0;
}

#ifdef CONFIG_OF
/**
 * Get platform data from Device Tree
 */
static int hva_get_of_pdata(struct platform_device *pdev,
                            struct HvaDriverData *pDriverData)
{
#ifndef CONFIG_ARCH_STI
	struct device_node *np = pdev->dev.of_node;
	const char *clkName;

	of_property_read_string_index(np, "st,dev_clk", 0, &clkName);
	//map clk virtual name to clock specific platform HW name
	clk_add_alias("clk_hva", NULL, (char *)clkName, NULL);

#else
	(void)pdev; // warning removal
#endif
	(void)pDriverData; // warning removal
	return 0;
}
#else
int hva_get_of_pdata(struct platform_device *pdev,
                     struct HvaDriverData *pDriverData)
{
	(void)pdev; // warning removal
	(void)pDriverData; // warning removal
	return 0;
}
#endif //CONFIG_OF

// Power management callbacks
static struct notifier_block stm_pm_hva_notifier = {
	.notifier_call = stm_pm_hva_notifier_call,
};

static OSDEV_Descriptor_t HvaDeviceDescriptor = {
	.Name = "Hva Module",
	.MajorNumber = MAJOR_NUMBER,
	.OpenFn  = HvaOpen,
	.CloseFn = HvaClose,
	.IoctlFn = HvaIoctl,
	.MmapFn  = NULL
};

// probe function called on module load
static int HvaProbe(struct platform_device *pdev)
{
	int ret = 0;
	OSDEV_Status_t Status = OSDEV_NoError;

	dev_dbg(&pdev->dev, "Run hva probe function\n");

	//NB: Memory allocated with devm_kzalloc() is automatically freed on driver detach
	pDriverData = devm_kzalloc(&pdev->dev, sizeof(*pDriverData), GFP_KERNEL);
	if (!pDriverData) {
		dev_err(&pdev->dev, "Error: memory allocation failed\n");
		ret = -ENOMEM;
		goto fail_out;
	}

	// Save hva device pointer
	pDriverData->dev = &pdev->dev;

	Status = OSDEV_RegisterDevice(&HvaDeviceDescriptor);
	if (Status != OSDEV_NoError) {
		dev_err(pDriverData->dev, "Error: %s Unable to get major %d\n", __func__, MAJOR_NUMBER);
		ret = -ENODEV;
		goto fail_out;
	}

	Status = OSDEV_LinkDevice(HVA_DEVICE, MAJOR_NUMBER, 0);
	if (Status != OSDEV_NoError) {
		dev_err(pDriverData->dev, "Error: %s OSDEV_LinkDevice failed (major %d)\n", __func__, MAJOR_NUMBER);
		ret = -ENODEV;
		goto fail_linkdevice;
	}

	//Retrieve platform specific parameters from platform file & store in platformData
	HvaGetPlatformInfo(pdev);

	//Store driver data: easy access then
	platform_set_drvdata(pdev, pDriverData);

	/* Check DT usage */
	if (pdev->dev.of_node) {
		ret = hva_get_of_pdata(pdev, pDriverData);
		if (ret) {
			dev_err(pDriverData->dev, "Error: hva probing device with device tree failed (%d)\n", ret);
			goto fail_hvaprobe;
		}
	} else {
		dev_err(pDriverData->dev, "Error: device tree not working\n");
		goto fail_hvaprobe;
	}

	pDriverData->max_freq = 0;
	if (of_property_read_u32(pdev->dev.of_node, "clock-frequency", &pDriverData->max_freq)) {
		dev_dbg(pDriverData->dev, "Maximum hva clock frequency not retrieved from DT\n");
	}

	pDriverData->HvaRegisterBase = (unsigned int)devm_ioremap_nocache(pDriverData->dev,
	                                                                  pDriverData->platformData.BaseAddress[0],
	                                                                  pDriverData->platformData.Size[0]);
	if (!pDriverData->HvaRegisterBase) {
		dev_err(pDriverData->dev, "Error: ioremap failed\n");
		ret = -EIO;
		goto fail_hvaprobe;
	}

	//clk_get() is a costly processing: interesting to perform it one shot & record clocks structure as device driver data
	pDriverData->clk = devm_clk_get(&pdev->dev, "clk_hva");
	if (IS_ERR(pDriverData->clk)) {
		ret = PTR_ERR(pDriverData->clk);
		dev_err(pDriverData->dev, "Error: get hva clock handle failed (%d)\n", ret);
		goto fail_hvaprobe;
	}

	ret = HvaClockSetRate(pdev->dev.of_node);
	if (ret) {
		dev_err(pDriverData->dev, "Error: setting maximum clock frequency failed (%d)\n", ret);
		goto fail_hvaprobe;
	}

	ret = clk_prepare(pDriverData->clk);
	if (ret) {
		dev_err(pDriverData->dev, "Error: prepare hva clock failed (%d)\n", ret);
		goto fail_hvaprobe;
	}

#ifdef CONFIG_PM_RUNTIME
	/* Clear the device's 'power.runtime_error' flag and set the device's runtime PM status to 'suspended' */
	pm_runtime_set_suspended(&pdev->dev);
	pm_suspend_ignore_children(&pdev->dev, 1);
	//decrement the device's 'power.disable_depth' field: if that field is equal to zero, the runtime PM helper functions can execute
	pm_runtime_enable(&pdev->dev);
#endif

	ret = HvaInit();
	if (ret) {
		goto hva_fail_unprep;
	}

	// Pass the initialisation stage to the h264_encode_hard layer
	ret = H264HwEncodeProbe(&pdev->dev);
	if (ret) {
		goto hva_fail_unprep;
	}

	// Register with PM notifiers (for HPS/CPS support)
	register_pm_notifier(&stm_pm_hva_notifier);

	OSDEV_Dump_MemCheckCounters(__func__);
	dev_info(pDriverData->dev, "%s probe done ok\n", MODULE_NAME);

	return 0;

hva_fail_unprep:
	clk_unprepare(pDriverData->clk);
#ifdef CONFIG_PM_RUNTIME
	pm_runtime_disable(&pdev->dev);
#endif
fail_hvaprobe:
	OSDEV_UnLinkDevice(HVA_DEVICE);
fail_linkdevice:
	OSDEV_DeRegisterDevice(&HvaDeviceDescriptor);
fail_out:
	OSDEV_Dump_MemCheckCounters(__func__);
	dev_err(&pdev->dev, "Error: %s probe failed\n", MODULE_NAME);

	return ret;
}

// remove function called on module unload
static int HvaRemove(struct platform_device *pdev)
{
	dev_dbg(pDriverData->dev, "Run hva remove function\n");

	pDriverData = dev_get_drvdata(&pdev->dev);

	H264HwEncodeRemove(&pdev->dev);

	clk_unprepare(pDriverData->clk);

	unregister_pm_notifier(&stm_pm_hva_notifier);

#ifdef CONFIG_PM_RUNTIME
	pm_runtime_disable(&pdev->dev);
#endif

	// Remove HVA device pointer
	pDriverData->dev = 0;

	OSDEV_UnLinkDevice(HVA_DEVICE);

	OSDEV_DeRegisterDevice(&HvaDeviceDescriptor);

	OSDEV_Dump_MemCheckCounters(__func__);
	pr_info("%s remove done\n", MODULE_NAME);

	return 0;
}

// Power management callbacks
#ifdef CONFIG_PM
static struct dev_pm_ops HvaPmOps = {
	.suspend = HvaPmSuspend,
	.resume  = HvaPmResume,
#ifdef CONFIG_PM_RUNTIME
	.runtime_suspend  = HvaPmRuntimeSuspend,
	.runtime_resume   = HvaPmRuntimeResume,
#endif
};
#endif // CONFIG_PM

#ifdef CONFIG_OF
static struct of_device_id stm_hva_match[] = {
	{
		.compatible = "st,se-hva",
	},
	{},
};
//MODULE_DEVICE_TABLE(of, stm_hva_match);
#endif

static struct platform_driver HvaDriver = {
	.driver = {
		.name = "h264encoder",
		.owner = THIS_MODULE,
		.of_match_table = of_match_ptr(stm_hva_match),
#ifdef CONFIG_PM
		.pm = &HvaPmOps,
#endif
	},
	.probe = HvaProbe,
	.remove = HvaRemove,
};

module_platform_driver(HvaDriver);
