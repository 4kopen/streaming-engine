/************************************************************************
Copyright (C) 2003-2014 STMicroelectronics. All Rights Reserved.

This file is part of the Streaming Engine.

Streaming Engine is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License version 2 as
published by the Free Software Foundation.

Streaming Engine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with Streaming Engine; see the file COPYING.  If not, write to the Free
Software Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307,
USA.

The Streaming Engine Library may alternatively be licensed under a
proprietary license from ST.
************************************************************************/

#ifndef H_H264_ENCODE_HARD_HOST_TRANSFORMER
#define H_H264_ENCODE_HARD_HOST_TRANSFORMER

typedef unsigned int  MME_UINT;

/* MME_ScatterPage_t:
 * Structure describing a single scatter page - see MME_DataBuffer_t
 */
typedef struct MME_ScatterPage_t {
	void                 *Page_p;
	MME_UINT              Size;
	MME_UINT              BytesUsed;
	MME_UINT              FlagsIn;
	MME_UINT              FlagsOut;
} MME_ScatterPage_t;

/* MME_DataBuffer_t:
 * Structure describing a data buffer
 */
typedef struct MME_DataBuffer_t {
	MME_UINT              StructSize;
	void                 *UserData_p;
	MME_UINT              Flags;
	MME_UINT              StreamNumber;
	MME_UINT              NumberOfScatterPages;
	MME_ScatterPage_t    *ScatterPages_p;
	MME_UINT              TotalSize;
	MME_UINT              StartOffset;
} MME_DataBuffer_t;


/* MME_CommandId_t:
 * The identifier assigned by MME to a submitted command
 */
typedef MME_UINT MME_CommandId_t;

/* MME_CommandState_t:
 * Used to specify the current state of a submitted command
 */
typedef enum MME_CommandState_t {
	MME_COMMAND_IDLE,
	MME_COMMAND_PENDING,
	MME_COMMAND_EXECUTING,
	MME_COMMAND_COMPLETED,
	MME_COMMAND_FAILED
} MME_CommandState_t;

/* Architecture neutral */
/* MME_Time_t:
 * MME time
 */
typedef MME_UINT MME_Time_t;

/* MME_ERROR:
 * The errors that may be returned by MME API functions or by
 * transformer entry point functions
 */
typedef enum MME_ERROR {
	MME_SUCCESS                     = 0,
	MME_DRIVER_NOT_INITIALIZED      = 1,
	MME_DRIVER_ALREADY_INITIALIZED  = 2,
	MME_NOMEM                       = 3,
	MME_INVALID_TRANSPORT           = 4,	/* DEPRECATED */
	MME_INVALID_HANDLE              = 5,
	MME_INVALID_ARGUMENT            = 6,
	MME_UNKNOWN_TRANSFORMER         = 7,
	MME_TRANSFORMER_NOT_RESPONDING  = 8,
	MME_HANDLES_STILL_OPEN          = 9,
	MME_COMMAND_STILL_EXECUTING     = 10,
	MME_COMMAND_ABORTED             = 11,
	MME_DATA_UNDERFLOW              = 12,
	MME_DATA_OVERFLOW               = 13,
	MME_TRANSFORM_DEFERRED          = 14,
	MME_SYSTEM_INTERRUPT            = 15,
	MME_ICS_ERROR                   = 16,
	MME_INTERNAL_ERROR              = 17,
	MME_NOT_IMPLEMENTED             = 18,
	MME_COMMAND_TIMEOUT             = 19,	/* MME4 API extension */
	MME_NOMIPS                      = 20	/* MME4 API extension */
#if defined (__arm__) && defined __KERNEL__
	                                  /* add this return value for creating specific port for userspace transformer */
	                                  , MME_USERSPACE_INIT_SUCCESS           = 21
#endif
} MME_ERROR;


/* MME_GenericParams_t:
 * A transformer specific parameter
 */
typedef void *MME_GenericParams_t;

/* MME_CommandStatus_t:
 * Structure containing state associated with a command
 */
typedef struct MME_CommandStatus_t {
	MME_CommandId_t       CmdId;
	MME_CommandState_t    State;
	MME_Time_t            ProcessedTime;
	MME_ERROR             Error;
	MME_UINT              AdditionalInfoSize;
	MME_GenericParams_t   AdditionalInfo_p;
} MME_CommandStatus_t;

/* MME_CommandCode_t:
 * Used to specify a command to a transformer
 */
typedef enum MME_CommandCode_t {
	MME_SET_PARAMS,
	MME_TRANSFORM,
	MME_INIT,
	MME_GET_CAPABILITY,
	MME_TERM,
} MME_CommandCode_t;

/* MME_Event_t:
 * Used to indicate a transform event type to the Client callback
 */
typedef enum MME_Event_t {
	MME_COMMAND_COMPLETED_EVT,
	MME_DATA_UNDERFLOW_EVT,
	MME_NOT_ENOUGH_MEMORY_EVT,
	MME_NEW_COMMAND_EVT,
	MME_TRANSFORMER_TIMEOUT
} MME_Event_t;

/* MME_Priority_t:
 * The MME scheduler priority at which a transform is executed
 */
typedef enum MME_Priority_t {
	MME_PRIORITY_HIGHEST      = 5000,
	MME_PRIORITY_ABOVE_NORMAL = 4000,
	MME_PRIORITY_NORMAL       = 3000,
	MME_PRIORITY_BELOW_NORMAL = 2000,
	MME_PRIORITY_LOWEST       = 1000
} MME_Priority_t;

/* MME_CommandEndType_t:
 * Used to specify whether the Client callback or Wake
 * should be actioned on command completion
 *
 * MME_COMMAND_END_RETURN_NO_INFO - No Callback/Wake
 * MME_COMMAND_END_RETURN_NOTIFY  - Call Client callback
 * MME_COMMAND_END_RETURN_WAKE    - Wakeup MME_Wait() call
 */
typedef enum MME_CommandEndType_t {
	MME_COMMAND_END_RETURN_NO_INFO,
	MME_COMMAND_END_RETURN_NOTIFY,
	MME_COMMAND_END_RETURN_WAKE		/* MME4 API extension */
} MME_CommandEndType_t;

/* MME_Command_t:
 * Structure to pass a command to a transformer
 */
typedef struct MME_Command_t {
	MME_UINT              StructSize;
	MME_CommandCode_t     CmdCode;
	MME_CommandEndType_t  CmdEnd;
	MME_Time_t            DueTime;
	MME_UINT              NumberInputBuffers;
	MME_UINT              NumberOutputBuffers;
	MME_DataBuffer_t    **DataBuffers_p;
	MME_CommandStatus_t   CmdStatus;
	MME_UINT              ParamSize;
	MME_GenericParams_t   Param_p;
} MME_Command_t;

/* MME_DataFormat_t:
 * Structure describing the data format that a transformer supports
 */
typedef struct MME_DataFormat_t {
	unsigned char         FourCC[4];
} MME_DataFormat_t;

/* MME_TransformerCapability_t:
 * Structure containing information pertaining to a transformer's capability
 */
typedef struct MME_TransformerCapability_t {
	MME_UINT              StructSize;
	MME_UINT              Version;
	MME_DataFormat_t      InputType;
	MME_DataFormat_t      OutputType;
	MME_UINT              TransformerInfoSize;
	MME_GenericParams_t   TransformerInfo_p;
	MME_UINT              AdditionalStatus;
} MME_TransformerCapability_t;

/* Transformer Client callback function */
typedef void (*MME_GenericCallback_t)(MME_Event_t event, MME_Command_t *callbackData, void *userData);

/* MME_TransformerInitParams_t:
 * The parameters with which to initialize a transformer
 */
typedef struct MME_TransformerInitParams_t {
	MME_UINT              StructSize;
	MME_Priority_t        Priority;
	MME_GenericCallback_t Callback;
	void                 *CallbackUserData;
	MME_UINT              TransformerInitParamsSize;
	MME_GenericParams_t   TransformerInitParams_p;
} MME_TransformerInitParams_t;

#endif // H_H264_ENCODE_HARD_HOST_TRANSFORMER
