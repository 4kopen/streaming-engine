/************************************************************************
Copyright (C) 2003-2014 STMicroelectronics. All Rights Reserved.

This file is part of the Streaming Engine.

Streaming Engine is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License version 2 as
published by the Free Software Foundation.

Streaming Engine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with Streaming Engine; see the file COPYING.  If not, write to the Free
Software Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307,
USA.

The Streaming Engine Library may alternatively be licensed under a
proprietary license from ST.
************************************************************************/
#ifndef _AVSP_PP_H_
#define _AVSP_PP_H_

#define AVSP_PP_MAX_CHUNK_SIZE  (0x02U)
#define AVSP_PP_MAX_OPCODE_SIZE (0x03U << 2)

#define AVSP_PP_MAX_SLICES      (137)
#define AVSP_PP_MAX_SESB_SIZE   (0x3C8)   // TODO FW needs this...
// TODO
/*#define AVSP_PP_MAX_SESB_SIZE   BUF_ALIGN_UP((8 * AVSP_PP_MAX_SLICES), HADESPP_BUFFER_ALIGNMENT)  //8-byte aligned*/

// AVSP intermediate buffer indices
#define AVSP_SESB                           0
#define AVSP_SLICE_DATA                     1
#define AVSP_NUM_INTERMEDIATE_POOLS         2

#define AVSP_PP_CHKSYN_STS_FORBIDDEN_BYTE_SEQUENCE (1U<<0)
#define AVSP_PP_CHKSYN_STS_HEADER_RANGE            (1U<<1)
#define AVSP_PP_CHKSYN_STS_HEADER_COHERENCY        (1U<<2)
#define AVSP_PP_CHKSYN_STS_HEADER_ALIGNMENT        (1U<<3)
#define AVSP_PP_CHKSYN_STS_WEIGHTED_PREDICTION     (1U<<4)
#define AVSP_PP_CHKSYN_STS_ENTRY_POINT_OFFSETS     (1U<<5)
#define AVSP_PP_CHKSYN_STS_DATA_RANGE              (1U<<6)
#define AVSP_PP_CHKSYN_STS_DATA_RENORM             (1U<<7)
#define AVSP_PP_CHKSYN_STS_DATA_ALIGNMENT          (1U<<8)
#define AVSP_PP_CHKSYN_STS_RBSP_TRAILING_BITS      (1U<<9)
#define AVSP_PP_CHKSYN_STS_RBSP_TRAILING_DATA      (1U<<10)

#define AVSP_PP_ERROR_STATUS_END_OF_PROCESS             (1U<<0)
#define AVSP_PP_ERROR_STATUS_END_OF_PICTURE_DETECTED    (1U<<1)
#define AVSP_PP_ERROR_STATUS_END_OF_DMA                 (1U<<2)
#define AVSP_PP_ERROR_STATUS_MOVE_OUT_OF_DMA            (1U<<3)
#define AVSP_PP_ERROR_STATUS_ERROR_ON_HEADER_FLAG       (1U<<4)
#define AVSP_PP_ERROR_STATUS_ERROR_ON_DATA_FLAG         (1U<<5)
#define AVSP_PP_ERROR_STATUS_UNEXPECTED_SC_DETECTED     (1U<<6)
#define AVSP_PP_ERROR_STATUS_BUFFER1_OVERFLOW           (1U<<7)
#define AVSP_PP_ERROR_STATUS_BUFFER2_OVERFLOW           (1U<<8)
#define AVSP_PP_ERROR_STATUS_BUFFER1_DONE               (1U<<16)
#define AVSP_PP_ERROR_STATUS_BUFFER2_DONE               (1U<<17)

#ifdef HEVC_HADES_CANNESWIFI

#define AVSP_PP_REGISTER_OFFSET 0x2000

#define AVSP_PP_CHKSYN_DIS_FORBIDDEN_BYTE_SEQUENCE (1U<<0) // if 1, forbidden_byte_sequence errors not checked
#define AVSP_PP_CHKSYN_DIS_MAX_BIN_CBAC            (1U<<1) // if 1, MAX_BIN_CBAC errors are never signalled (must be 0)
#define AVSP_PP_CHKSYN_DIS_MAX_LEVEL_EX_GOLOMB     (1U<<2) // if 1, MAX_LEVEL_EX_GOLOMB never signalled (must be 0)
#define AVSP_PP_CHKSYN_DIS_HEADER_ALIGNMENT        (1U<<3) // if 1, HEADER_ALIGNMENT not checked
#define AVSP_PP_CHKSYN_DIS_TRAILING_BITS           (1U<<4) // if 1, TRAILING_BITS not checked
#define AVSP_PP_CHKSYN_DIS_DATA_RANGE              (1U<<8) // if 1, DATA_RANGE not checked
#define AVSP_PP_CHKSYN_DIS_DATA_ALIGNMENT          (1U<<9) // if 1, DATA_ALIGNMENT not checked
#define AVSP_PP_CHKSYN_DIS_MARKER_BIT              (1U<<10) // if 1, MARKER_BIT not checked
#define AVSP_PP_CHKSYN_DIS_FORBIDDEN_BYTE_SEQUENCE_MODE (1U<<11) // if 1, special mode is disabled

#else  /* !HEVC_HADES_CANNESWIFI */

#define AVSP_PP_REGISTER_OFFSET (0x200)

#define AVSP_PP_RD2_CID                (0x818)
#define AVSP_PP_WR6_CID                (0x81C)
#define AVSP_PP_WR7_CID                (0x820)

#define AVSP_PP_RD2_SID                (0x1818)
#define AVSP_PP_WR6_SID                (0x181C)
#define AVSP_PP_WR7_SID                (0x1820)

#endif /* !HEVC_HADES_CANNESWIFI */

#define AVSP_PP_CFG1                   (AVSP_PP_REGISTER_OFFSET + 0x000)
#define AVSP_PP_CFG2                   (AVSP_PP_REGISTER_OFFSET + 0x004)
#define AVSP_PP_CFG3                   (AVSP_PP_REGISTER_OFFSET + 0x008)
#define AVSP_PP_CFG4                   (AVSP_PP_REGISTER_OFFSET + 0x00C)
#define AVSP_PP_CFG5                   (AVSP_PP_REGISTER_OFFSET + 0x010)
#define AVSP_PP_CFG6                   (AVSP_PP_REGISTER_OFFSET + 0x014)

#define AVSP_PP_SESB_BASE_ADDR         (AVSP_PP_REGISTER_OFFSET + 0x018)
#define AVSP_PP_SESB_LENGTH            (AVSP_PP_REGISTER_OFFSET + 0x01C)
#define AVSP_PP_SESB_END_OFFSET        (AVSP_PP_REGISTER_OFFSET + 0x020)
#define AVSP_PP_SESB_STOP_OFFSET       (AVSP_PP_REGISTER_OFFSET + 0x024)
#define AVSP_PP_SESB_CRC               (AVSP_PP_REGISTER_OFFSET + 0x028)

#define AVSP_PP_SLICE_DATA_BASE_ADDR   (AVSP_PP_REGISTER_OFFSET + 0x02C)
#define AVSP_PP_SLICE_DATA_LENGTH      (AVSP_PP_REGISTER_OFFSET + 0x030)
#define AVSP_PP_SLICE_DATA_END_OFFSET  (AVSP_PP_REGISTER_OFFSET + 0x034)
#define AVSP_PP_SLICE_DATA_STOP_OFFSET (AVSP_PP_REGISTER_OFFSET + 0x038)
#define AVSP_PP_SLICE_DATA_CRC         (AVSP_PP_REGISTER_OFFSET + 0x03C)

#define AVSP_PP_RESIDUALS_BASE_ADDR    (AVSP_PP_REGISTER_OFFSET + 0x040)
#define AVSP_PP_RESIDUALS_LENGTH       (AVSP_PP_REGISTER_OFFSET + 0x044)
#define AVSP_PP_RESIDUALS_END_OFFSET   (AVSP_PP_REGISTER_OFFSET + 0x048)
#define AVSP_PP_RESIDUALS_STOP_OFFSET  (AVSP_PP_REGISTER_OFFSET + 0x04C)
#define AVSP_PP_RESIDUALS_CRC          (AVSP_PP_REGISTER_OFFSET + 0x050)

#define AVSP_PP_CHKSYN_DIS             (AVSP_PP_REGISTER_OFFSET+0x054)
#define AVSP_PP_CHKSYN_STATUS          (AVSP_PP_REGISTER_OFFSET+0x058)
#define AVSP_PP_GENERAL_STATUS         (AVSP_PP_REGISTER_OFFSET+0x05C)
#define AVSP_PP_ERROR_STATUS           (AVSP_PP_REGISTER_OFFSET+0x060)
#define AVSP_PP_DEBUG                  (AVSP_PP_REGISTER_OFFSET+0x064)

#ifdef HEVC_HADES_CANNESWIFI
#define AVSP_PP_CTRL_OBS               (AVSP_PP_REGISTER_OFFSET+0x068)
#else
#define AVSP_PP_CTRL_OBS               (0x11C)
#endif // HEVC_HADES_CANNESWIFI
#endif
