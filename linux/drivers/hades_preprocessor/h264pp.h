/************************************************************************
Copyright (C) 2003-2014 STMicroelectronics. All Rights Reserved.

This file is part of the Streaming Engine.

Streaming Engine is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License version 2 as
published by the Free Software Foundation.

Streaming Engine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with Streaming Engine; see the file COPYING.  If not, write to the Free
Software Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307,
USA.

The Streaming Engine Library may alternatively be licensed under a
proprietary license from ST.
************************************************************************/
#ifndef _H264PP_H_
#define _H264PP_H_

#define H264_PP_MAX_CHUNK_SIZE  (0x02U)
#define H264_PP_MAX_OPCODE_SIZE (0x03U << 2)

#define H264_PP_MAX_SLICES      140   // 137 actually
#define H264_PP_SESB_SIZE       (8 * H264_PP_MAX_SLICES)   // 2 words per slice

#ifdef HEVC_HADES_CANNESWIFI
#define H264_PP_REGISTER_OFFSET 0x3000

#define H264_PP_SESB_BASE_ADDR         (H264_PP_REGISTER_OFFSET + 0x0)
#define H264_PP_SESB_LENGTH            (H264_PP_REGISTER_OFFSET + 0x04)
#define H264_PP_SESB_END_OFFSET        (H264_PP_REGISTER_OFFSET + 0x08)
#define H264_PP_SESB_STOP_OFFSET       (H264_PP_REGISTER_OFFSET + 0x0C)  //RO
#define H264_PP_SESB_CRC               (H264_PP_REGISTER_OFFSET + 0x10)
#define H264_PP_SLICE_DATA_BASE_ADDR   (H264_PP_REGISTER_OFFSET + 0x14)
#define H264_PP_SLICE_DATA_LENGTH      (H264_PP_REGISTER_OFFSET + 0x18)
#define H264_PP_SLICE_DATA_END_OFFSET  (H264_PP_REGISTER_OFFSET + 0x1C)
#define H264_PP_SLICE_DATA_STOP_OFFSET (H264_PP_REGISTER_OFFSET + 0x20)  // RO
#define H264_PP_SLICE_DATA_CRC         (H264_PP_REGISTER_OFFSET + 0x24)
#define H264_PP_RESIDUALS_BASE_ADDR    (H264_PP_REGISTER_OFFSET + 0x28)
#define H264_PP_RESIDUALS_LENGTH       (H264_PP_REGISTER_OFFSET + 0x2C)
#define H264_PP_RESIDUALS_END_OFFSET   (H264_PP_REGISTER_OFFSET + 0x30)
#define H264_PP_RESIDUALS_STOP_OFFSET  (H264_PP_REGISTER_OFFSET + 0x34)
#define H264_PP_RESIDUALS_CRC          (H264_PP_REGISTER_OFFSET + 0x38)
#define H264_PP_CFG                    (H264_PP_REGISTER_OFFSET + 0x40)
#define H264_PP_PICWIDTH               (H264_PP_REGISTER_OFFSET + 0x44)
#define H264_PP_CODELENGTH             (H264_PP_REGISTER_OFFSET + 0x48)
#define H264_CHKSYN_DIS                (H264_PP_REGISTER_OFFSET + 0x4C)
#define H264_PP_ERROR_STATUS           (H264_PP_REGISTER_OFFSET + 0x50)
#define H264_PP_DEBUG                  (H264_PP_REGISTER_OFFSET + 0x54)
#define H264_PP_CTRL_OBS               (H264_PP_REGISTER_OFFSET + 0x58)
#define H264_PP_NUT_DIS                (H264_PP_REGISTER_OFFSET + 0x5C)

/*
   [17] h264_buffer2_done: macroblock structure buffer written
   [16] h264_buffer1_done: slice error & status buffer written
   [15:9] RESERVED
   [8] h264_buffer2_overflow: when 1, macroblock structure have been written beyond buffer limit
   [7] h264_buffer1_overflow: when 1, slice error & status data have been written beyond buffer
   limit
   [6] h264_unexpected_sc_detected: when 1, a startcode has been detected while parsing slice
   header or slice data
   [5] h264_error_on_data_flag: when 1, error has been detected during slice data parsing
   [4] h264_error_on_header_flag: when 1, error has been detected during slice header parsing
   [3] RESERVED
   [2] h264_end_of_dma: when 1, the end of the programmed DMA corresponding to the current
   picture has been reached
   [1] h264_end_of_picture_not_detected
   [0] RESERVED
 */

#define H264PP_ERROR_STATUS_END_OF_PIC_NOT_DETECTED    (1U<<1)
#define H264PP_ERROR_STATUS_END_OF_DMA                 (1U<<2)
#define H264PP_ERROR_STATUS_ERROR_ON_HEADER            (1U<<4)
#define H264PP_ERROR_STATUS_ERROR_ON_DATA              (1U<<5)
#define H264PP_ERROR_STATUS_UNEXPECTED_SC              (1U<<6)
#define H264PP_ERROR_STATUS_BUFFER1_OVERFLOW           (1U<<7)
#define H264PP_ERROR_STATUS_BUFFER2_OVERFLOW           (1U<<8)
#define H264PP_ERROR_STATUS_BUFFER1_DONE               (1U<<16)
#define H264PP_ERROR_STATUS_BUFFER2_DONE               (1U<<17)

/*
  [13] h264_wrplug_dma_2_val: when 1, H264 write client 2 has a dma command ready for writting
  [12] h264_wrplug_dma_2_ack: when 1, H264 write IPplug can accept dma command from client 2
  [11] h264_wrplug_proc_2_val: when 1, H264 write client 2 has a data ready for writting
  [10] h264_wrplug_proc_2_ack: when 1, H264 write IPplug can accept data from client 2
  [9] h264_wrplug_dma_1_val: when 1, H264 write client 1 has a dma command ready for writting
  [8] h264_wrplug_dma_1_ack: when 1, H264 write IPplug can accept dma command from client 1
  [7] h264_wrplug_proc_1_val: when 1, H264 write client 1 has a data ready for writting
  [6] h264_wrplug_proc_1_ack: when 1, H264 write IPplug can accept data from client 1
  [5] h264_rdplug_dma_1_val: when 1, H264 read client 1 has a dma command ready for writting
  [4] h264_rdplug_dma_1_ack: when 1, H264 read IPplug can accept dma command from client 1
  [3] h264_rdplug_proc_1_val: when 1, H264 read client 1 is ready to read data
  [2] h264_rdplug_proc_1_ack: when 1, H264 read IPplug has a data ready for reading
  [1] h264_clear_pp_it_autoclear: when writting 1, H264 PP interrupt output is lowered
  [0] h264_force_pp_it_autoclear: when writting 1, H264 PP interrupt output is raised
 */

#define H264PP_CTRL_OBS_FORCE_PP_IT_AUTOCLEAR        (1U<<0)
#define H264PP_CTRL_OBS_CLEAR_PP_IT_AUTOCLEAR        (1U<<1)
#define H264PP_CTRL_OBS_RDPLUG_PROC1_ACK             (1U<<2)
#define H264PP_CTRL_OBS_RDPLUG_PROC1_VAL             (1U<<3)
#define H264PP_CTRL_OBS_RDPLUG_DMA1_ACK              (1U<<4)
#define H264PP_CTRL_OBS_RDPLUG_DMA1_VAL              (1U<<5)
#define H264PP_CTRL_OBS_WRPLUG_PROC1_ACK             (1U<<6)
#define H264PP_CTRL_OBS_WRPLUG_PROC1_VAL             (1U<<7)
#define H264PP_CTRL_OBS_WRPLUG_DMA1_ACK              (1U<<8)
#define H264PP_CTRL_OBS_WRPLUG_DMA1_VAL              (1U<<9)
#define H264PP_CTRL_OBS_WRPLUG_PROC2_ACK             (1U<<10)
#define H264PP_CTRL_OBS_WRPLUG_PROC2_VAL             (1U<<11)
#define H264PP_CTRL_OBS_WRPLUG_DMA2_ACK              (1U<<12)
#define H264PP_CTRL_OBS_WRPLUG_DMA2_VAL              (1U<<13)

#endif /* HEVC_HADES_CANNESWIFI */

#endif /* _H264PP_H_ */
