/************************************************************************
  Copyright (C) 2003-2014 STMicroelectronics. All Rights Reserved.

  This file is part of the Streaming Engine.

  Streaming Engine is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License version 2 as
  published by the Free Software Foundation.

  Streaming Engine is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
  See the GNU General Public License for more details.

  You should have received a copy of the GNU General Public License along
  with Streaming Engine; see the file COPYING.  If not, write to the Free
  Software Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307,
  USA.

  The Streaming Engine Library may alternatively be licensed under a
  proprietary license from ST.
 ************************************************************************/

#include <linux/module.h>
#include <linux/clk.h>
#include <linux/slab.h>
#include <linux/semaphore.h>
#include <linux/interrupt.h>
#include <linux/pm_runtime.h>
#include <linux/of.h>

#ifdef HEVC_HADES_CANNESWIFI
#include <linux/ibi.h>
#endif
#include <linux/reset.h>

#include "osdev_time.h"
#include "osdev_sched.h"
#include "osdev_device.h"

#include "hadesppio.h"

#ifdef CONFIG_STM_HADES_DEBUG_TOOLS
#include "crcdriver.h"
#endif
#include "st_relayfs_se.h"
#define MAJOR_NUMBER    247

MODULE_DESCRIPTION("Hades pre-processor platform driver");
MODULE_AUTHOR("STMicroelectronics");
MODULE_LICENSE("GPL");

static unsigned int outputpreprocessingtime = 0;
module_param(outputpreprocessingtime, uint, S_IRUGO | S_IWUSR);
MODULE_PARM_DESC(outputpreprocessingtime, "Set to 1 to output decoding time for each frame via strelay");

/* How long we should wait for PP to complete single frame/field preprocessing before we give up */
#define HADESPP_TIMEOUT_MS 300
/* Size of sw task fifo (per instance)
   must be >= 2 to optimize PP usage */

//#define DEBUG_MODE

#ifdef HEVC_HADES_CANNES25
#define HADESPP_TASK_FIFO_SIZE 5
#else
#define HADESPP_TASK_FIFO_SIZE 4
#endif

/**
 * Task descriptor
 * @InParam: input parameters
 * @OutParam: output parameters
 * @OutputSize: output buffer size
 * @decode_time_sw: task decoding time (spent in driver)
 * @hw_ended:  task completion in HW
 * @completed: task full completion
 * @core: core where the task has been processed
 *        if 0 -> termination signal for end of task thread
 * @id: informative field
 */
struct HadesppTask {
	struct hadespp_ioctl_queue_t    InParam;
	struct hadespp_ioctl_dequeue_t  OutParam;

	unsigned int            OutputSize;
	unsigned int            decode_time_sw;
	struct completion       hw_ended;
	struct completion       completed;
	struct HadesppCore      *core;
	unsigned int            id;
#ifdef CONFIG_STM_HADES_DEBUG_TOOLS
	FrameCRC_t              crcs;
#endif
};

/**
 * Main struct for 1 PP core
 * @name: name of core
 * @clk_hevc_pp: clocks for both hevc/h264 hw
 * @clk_avsp_pp: clock for avsp hw
 * @clk_rate: pp clock frequency
 * @clk_lock: spinlock on clk_ref_count
 * @clk_ref_count: ref counter on clock to call core initialisation
 * @hw_lock: lock on reg writes for current core
 * @irq_its: end of task IRQ
 * @hw_base_addr: remapped base address of core
 * @regs_size: size of IO ressource

 * @inuse: PP core is currently used
 * @current_task: pointer to current task processed in core
 * @last_mb_adaptive_frame_field_flag: mb_adaptive_frame_field_flag backup
 * @need_soft_reset: force soft reset
 */
struct HadesppCore {
	char                  name[16];
	struct clk           *clk_hevc_pp;
	struct clk           *clk_avsp_pp;
	uint32_t              clk_rate;
	int                   clk_ref_count;
	spinlock_t            clk_lock;
	struct mutex          hw_lock;
	u32                   irq_its;
	void __iomem         *hw_base_addr;
	void __iomem         *hw_hades_addr;
	int                   regs_size;
	bool                  inuse;
	struct HadesppTask   *current_task;
	bool                  need_soft_reset;
};

/**
 * Main struct for Hadespp multi-core
 * @dev: device pointer
 * @pp_nb: number of core
 * @pp_unactive_lock: semaphore dedicated to *unactive* core number
 * @core: array of cores
 * @core_lock: protect access to core array
 * @instance_nb: client (open) number
 * @reset_ctl: pointer to device reset driver
 */
struct Hadespp {
	struct device      *dev;
	int                pp_nb;
	struct semaphore   pp_unactive_lock;
	struct HadesppCore *core;
	spinlock_t         core_lock;
	atomic_t           instance_nb;
	unsigned int       hw_version;
	unsigned int       stbus_max_opcode_size;
	unsigned int       stbus_max_chunk_size;
	unsigned int       stbus_max_msg_size;
#ifdef CONFIG_STM_HADES_DEBUG_TOOLS
	CheckerHandle_t    crc_checker;
#endif
	struct reset_control *reset_ctl;
};

/**
 * Open context -> client instance
 * @data: private data used to store pointer to Hadespp device
 * @fifo_task_lock: lock on fifo task variables
 * @task: task fifo
 * @task_wq: wait queue on task fifo availability
 * @input_task_idx: write index in task fifo
 * @hw_done_task_idx: read index in task fifo for task completed by HW
 * @output_task_idx: read index in task fifo for task fully completed
 * @pending_task_nb: task nb in fifo
 * @in_progress_task_nb:  number of task needing postprocessing
 * @current_task_nb: total task number pre-processed (used for task->id field)
 * @task_completed_thread: perform (asap) required actions at the end of one task
 */
struct HadesppOpenContext {
	void               *data;
	spinlock_t         fifo_task_lock;
	struct HadesppTask task[HADESPP_TASK_FIFO_SIZE];
	wait_queue_head_t  task_wq;
	unsigned int       input_task_idx;
	unsigned int       hw_done_task_idx;
	unsigned int       output_task_idx;
	atomic_t           pending_task_nb;
	atomic_t           in_progress_task_nb;
	unsigned int       current_task_nb;
};

/**
 * Pointer to Hadespp global struct. Should be accessible in open/close/... functions
 * (OSDEV_*Entrypoint abstraction prevents or masks access to struct file* which
 * can be used to retrieve pointer to hadespp, and then remove this global variable)
 */
static struct Hadespp *hadespp;

/**
 * Dump all status/debug/stats/... registers
 */
#define DUMP_REG(REG) { \
    if (core == 0) dev_err("DUMP_REG: Unable to dump register #REG\n"); \
    else { uint32_t value = readl((volatile void *)(core->hw_base_addr + REG)); \
           dev_info(hadespp->dev, "\t" #REG "= %#08x\n", value);  }             \
}

#ifdef DEBUG_MODE
static void HadesppPlugStats(struct HadesppCore *core)
{
#ifdef HEVC_HADES_CANNES25
	dev_info(hadespp->dev, "\t*** plug stats:\n");
	DUMP_REG(HADESPP_PLUG_STAT0);
	DUMP_REG(HADESPP_PLUG_STAT1);
	DUMP_REG(HADESPP_PLUG_STAT2);
	DUMP_REG(HADESPP_PLUG_STAT3);
	DUMP_REG(HADESPP_PLUG_STAT4);
	DUMP_REG(HADESPP_PLUG_STAT5);
	DUMP_REG(HADESPP_PLUG_STAT6);
	DUMP_REG(HADESPP_PLUG_STAT7);
	DUMP_REG(HADESPP_PLUG_STAT8);
#endif
}
#endif

static void HadesppDebug(struct HadesppTask *task)
{
#ifdef DEBUG_MODE
	struct HadesppCore *core = task->core;
	dev_info(hadespp->dev, "%s - debug PP registers(codec:%d):\n",
	         __func__, task->InParam.codec);

	switch (task->InParam.codec) {
	case CODEC_HEVC:
		DUMP_REG(HEVCPP_SLICE_TABLE_CRC);
		DUMP_REG(HEVCPP_SLICE_HEADERS_CRC);
		DUMP_REG(HEVCPP_CTB_TABLE_CRC);
		DUMP_REG(HEVCPP_CTB_COMMANDS_CRC);
		DUMP_REG(HEVCPP_CTB_RESIDUALS_CRC);
		DUMP_REG(HEVCPP_ERROR_STATUS);
		DUMP_REG(HEVCPP_CTRL_OBS);
		DUMP_REG(HEVCPP_GENERAL_STATUS);
		DUMP_REG(HEVCPP_CHKSYN_STATUS);
		DUMP_REG(HEVCPP_DEBUG);
		break;
	case CODEC_H264:
#ifdef HEVC_HADES_CANNESWIFI
		DUMP_REG(H264_PP_SESB_CRC);
		DUMP_REG(H264_PP_SLICE_DATA_CRC);
		DUMP_REG(H264_PP_RESIDUALS_CRC);
		DUMP_REG(H264_PP_ERROR_STATUS);
		DUMP_REG(H264_PP_DEBUG);
		DUMP_REG(H264_PP_CTRL_OBS);
#endif
		break;
	case CODEC_AVSP:
		DUMP_REG(AVSP_PP_SESB_CRC);
		DUMP_REG(AVSP_PP_SLICE_DATA_CRC);
		DUMP_REG(AVSP_PP_RESIDUALS_CRC);
		DUMP_REG(AVSP_PP_GENERAL_STATUS);
		DUMP_REG(AVSP_PP_ERROR_STATUS);
		DUMP_REG(AVSP_PP_DEBUG);
		DUMP_REG(AVSP_PP_CTRL_OBS);
		break;
	default:
		break;
	}

	DUMP_REG(HADESPP_START);
	DUMP_REG(HADESPP_RESET);
	DUMP_REG(HADESPP_IP_VERSION);
	DUMP_REG(HADESPP_DECODE_TIME);
	// writel(0, (volatile void *)(core->hw_base_addr + HEVCPP_PLUG_STAT_CFG));
	HadesppPlugStats(core);

	/*
	   writel(1 + (0 << 3), (volatile void *)(core->hw_base_addr + HEVCPP_PLUG_STAT_CFG));
	   HevcppPlugStats("write 0", core);

	   writel(1 + (1 << 3), (volatile void *)(core->hw_base_addr + HEVCPP_PLUG_STAT_CFG));
	   HevcppPlugStats("write 1", core);

	   writel(1 + (2 << 3), (volatile void *)(core->hw_base_addr + HEVCPP_PLUG_STAT_CFG));
	   HevcppPlugStats("write 2", core);

	   writel(1 + (3 << 3), (volatile void *)(core->hw_base_addr + HEVCPP_PLUG_STAT_CFG));
	   HevcppPlugStats("write 3", core);

	   writel(1 + (4 << 3), (volatile void *)(core->hw_base_addr + HEVCPP_PLUG_STAT_CFG));
	   HevcppPlugStats("write 4", core);
	 */
#else
	(void)task; // warning removal
#endif
}


/**
 * Initialise one core (i.e. resets registers to default value)
 */
static void HadesppInitCore(struct HadesppCore *core)
{
	BUG_ON(!core);

	mutex_lock(&core->hw_lock);
	if (core->need_soft_reset) {
		writel(1, (volatile void *)(core->hw_base_addr + HADESPP_RESET));
		core->need_soft_reset = false;
	}
	mutex_unlock(&core->hw_lock);
}

#ifndef CONFIG_STM_VIRTUAL_PLATFORM
static int HadesppClockSetRate(struct device_node *of_node)
{
	int i;
	int ret = 0;
	unsigned int max_freq = 0;
	if (!of_property_read_u32_index(of_node, "clock-frequency", 0 , &max_freq)) {
		/* check incase 0 is set for max frequency property in DT */
		if (max_freq) {
			ret = clk_set_rate(hadespp->core[0].clk_hevc_pp, max_freq);
			if (ret) {
				pr_err("Error: %s Failed to set max frequency for hevc pp clock\n", __func__);
				ret = -EINVAL;
			}
		}
	}
	for (i = 0; i < hadespp->pp_nb; i++) {
		hadespp->core[i].clk_rate = max_freq;
	}
#if defined (HEVC_HADES_CANNES25) || defined (HEVC_HADES_CANNESWIFI)
	pr_info("ret value : 0x%x hevc pp clock max frequency value : %u\n", (unsigned int)ret, max_freq);
	max_freq = 0;
	if (!of_property_read_u32_index(of_node, "clock-frequency", 1 , &max_freq)) {
		/* check incase 0 is set for max frequency property in DT */
		if (max_freq) {
			ret = clk_set_rate(hadespp->core[0].clk_avsp_pp, max_freq);
			if (ret) {
				pr_err("Error: %s Failed to set max frequency for avsp pp clock\n", __func__);
				ret = -EINVAL;
			}
		}
	}
	pr_info("ret value : 0x%x avsp pp clock max frequency value : %u\n", (unsigned int)ret, max_freq);
#endif
	return ret;
}
#endif

/**
 * Disable clock for H264/HEVC of one core
 */
void HadesppClockOff_Hevc(struct HadesppCore *core)
{
	unsigned long flags;

	spin_lock_irqsave(&core->clk_lock, flags);
#ifndef CONFIG_STM_VIRTUAL_PLATFORM
	clk_disable(core->clk_hevc_pp);
#endif
	core->clk_ref_count--;
	spin_unlock_irqrestore(&core->clk_lock, flags);
}

/**
 * Enable clock for H264/HEVC of one core and reinit its registers if clk_ref_count==1
 */
int HadesppClockOn_Hevc(struct HadesppCore *core)
{
	bool need_reinit = false;
	unsigned long flags;
	int ret = 0;

	spin_lock_irqsave(&core->clk_lock, flags);
	//TODO To be restored once moved to kernel 3.10 (!!!assumes clock is on)
#ifndef CONFIG_STM_VIRTUAL_PLATFORM
	ret = clk_enable(core->clk_hevc_pp);
#endif
	if (!ret) {
		core->clk_ref_count++;
		if (1 == core->clk_ref_count) {
			need_reinit = true;
		}
	}
	spin_unlock_irqrestore(&core->clk_lock, flags);
	if (need_reinit) {
		HadesppInitCore(core);
	}
	return ret;
}

/**
 * Enable clock for AVSP of one core and reinit its registers if clk_ref_count==1
 */
int HadesppClockOn_Avsp(struct HadesppCore *core)
{
	int ret = 0;
#if defined (HEVC_HADES_CANNES25) || defined (HEVC_HADES_CANNESWIFI)
	unsigned long flags;
	spin_lock_irqsave(&core->clk_lock, flags);
	ret = clk_enable(core->clk_avsp_pp);
	if (!ret) {
		core->clk_ref_count++;
	}
	spin_unlock_irqrestore(&core->clk_lock, flags);
#endif
	return ret;
}

/**
 * Disable clock for AVSP of one core
 */
void HadesppClockOff_Avsp(struct HadesppCore *core)
{
#if defined (HEVC_HADES_CANNES25) || defined (HEVC_HADES_CANNESWIFI)
	unsigned long flags;
	spin_lock_irqsave(&core->clk_lock, flags);
	clk_disable(core->clk_avsp_pp);
	core->clk_ref_count--;
	spin_unlock_irqrestore(&core->clk_lock, flags);
#else
	(void)core; // warning removal
#endif
}

static int ConfigureIBIFramework(struct Hadespp *hadespp)
{
#ifdef HEVC_HADES_CANNESWIFI
	int ret = -EINVAL;
	struct ibi *ibi_dev = NULL;
	struct ibi_state *config = NULL;
	struct HadesppCore *core = &hadespp->core[0];

	ibi_dev = devm_ibi_get(hadespp->dev);
	if (IS_ERR(ibi_dev)) {
		pr_info("Unable to get IBI structure from device\n");
		goto err_end;
	}
	pr_info("IBI structure retrieved - %p\n", ibi_dev);

	config = ibi_lookup_state(ibi_dev, "default");
	if (!config) {
		pr_err("Error: %s - unable to get IBI configuration\n", __func__);
		goto err_end;
	}
	pr_info("IBI configuration for %p structure retrieved - %p\n", ibi_dev, config);

	if (pm_runtime_get_sync(hadespp->dev) < 0) {
		pr_err("Error: %s pm_runtime_get_sync failed\n", __func__);
		goto err_end;
	}

	ret = HadesppClockOn_Hevc(core);
	if (ret) {
		pr_err("Error: %s HadesppClockOn failed\n", __func__);
		goto err_dyn;
	}

	ret = HadesppClockOn_Avsp(core);
	if (ret) {
		pr_err("Error: %s HadesppClockOn failed\n", __func__);
		goto err_dyn1;
	}

	ret = ibi_set_state(ibi_dev, config);
	if (ret) {
		pr_err("Error: %s - failed to set ibi state - %d\n", __func__, ret);
		goto err_dyn2;
	}
	pr_info("IBI state set\n");

err_dyn2:
	HadesppClockOff_Avsp(core);
err_dyn1:
	HadesppClockOff_Hevc(core);
err_dyn:
	pm_runtime_mark_last_busy(hadespp->dev);
	pm_runtime_put(hadespp->dev);
err_end:
	return ret;
#else
	(void)hadespp; // warning removal
	return 0;
#endif
}

/**
 * Adds and returns one task to SW fifo
 * May blocks on task fifo fullness
 * @instance pointer to instance
 * @param pointer to input parameter
 */
struct HadesppTask *HadesppPushTask(struct HadesppOpenContext *instance,
                                    struct hadespp_ioctl_queue_t *p)
{
	unsigned int write_idx = 0;
	struct Hadespp *hadespp;
	struct HadesppCore *core = NULL;
	struct HadesppTask *task = NULL;
	unsigned long flags;
	int ret = 0;
	int i;

	hadespp = (struct Hadespp *)instance->data;
	BUG_ON(!hadespp);

	/**
	 * Block calling thread if no core available
	 * Timeout to push a new task (pp_nb+1) depends on scheduling policy:
	 * (hadespp->pp_nb(marging:+1))*HADESPP_TIMEOUT_MS in FIFO scheduling policy
	 */
	ret = down_timeout(&hadespp->pp_unactive_lock, msecs_to_jiffies((hadespp->pp_nb + 1) * HADESPP_TIMEOUT_MS));
	if (ret == -ETIME) {
		pr_err("Error: %s Waiting for task semaphore timeout!", __func__);
		goto bail;
	} else if (ret < 0) {
		pr_err("Error: %s Waiting for task semaphore interrupted!", __func__);
		goto bail;
	}

	/* Assign core where the task is processed */
	spin_lock_irqsave(&hadespp->core_lock, flags);
	for (i = 0; i < hadespp->pp_nb ; i++) {
		core = &hadespp->core[i];
		if (!core->inuse) {
			core->inuse = true;
			break;
		}
	}
	spin_unlock_irqrestore(&hadespp->core_lock, flags);
	/* Should not happen -> sem pp_unactive_lock protected*/
	BUG_ON(i == hadespp->pp_nb);

	/* Ensures that we can store task in FIFO
	   No timeout as it will depends on codec buffer availability -> unpredictible */
	ret = wait_event_interruptible(
	              instance->task_wq,
	              HADESPP_TASK_FIFO_SIZE > atomic_read(&instance->pending_task_nb));

	if (ret < 0) {
		pr_err("Error: %s push task in fifo interrupted (instance:%p)", __func__, instance);
		return NULL;
	}

	spin_lock_irqsave(&instance->fifo_task_lock, flags);
	write_idx = instance->input_task_idx;
	task = &instance->task[write_idx];
	memset(task, 0, sizeof(*task));
	init_completion(&task->hw_ended);
	init_completion(&task->completed);

	task->core = core;
	task->id = ++instance->current_task_nb;
	if (p) {
		task->InParam = *p;
	} else {
		/* Fake task to terminate end of task thread */
		task->core = 0;
	}

	atomic_inc(&instance->pending_task_nb);
	atomic_inc(&instance->in_progress_task_nb);
	instance->input_task_idx++;
	if (instance->input_task_idx == HADESPP_TASK_FIFO_SIZE) {
		instance->input_task_idx = 0;
	}
	spin_unlock_irqrestore(&instance->fifo_task_lock, flags);

	wake_up_interruptible(&instance->task_wq);

bail:
	return task;
}

/**
 * Returns hw_done_task_idx task of SW fifo
 * May blocks on hw task completion
 * @instance pointer to instance
 * if successful returns 0, else <0
 */
int HadesppGetEndedTask(struct HadesppOpenContext *instance, struct HadesppTask **t)
{
	struct HadesppTask *task = NULL;
	unsigned int read_idx = 0;
	unsigned long flags;
	int ret = 0;

	/**
	 * Ensures one task is available in FIFO
	 * (prevent from blocking on uninitialized task completion)
	 */
	ret = wait_event_interruptible(instance->task_wq,
	                               atomic_read(&instance->in_progress_task_nb));

	if (ret < 0) {
		pr_err("Error: %s get task from fifo interrupted (instance:%p)", __func__, instance);
		goto bail;
	}

	spin_lock_irqsave(&instance->fifo_task_lock, flags);
	read_idx = instance->hw_done_task_idx;
	task = &instance->task[read_idx];
	atomic_dec(&instance->in_progress_task_nb);
	instance->hw_done_task_idx++;
	if (instance->hw_done_task_idx == HADESPP_TASK_FIFO_SIZE) {
		instance->hw_done_task_idx = 0;
	}
	spin_unlock_irqrestore(&instance->fifo_task_lock, flags);
	//pr_info("%s wait for task %d\n", __func__, task->id);

	/* Waiting for task completion */
	if (!wait_for_completion_timeout
	    (&task->hw_ended, msecs_to_jiffies(HADESPP_TIMEOUT_MS))) {
		pr_info("%s: Wait for task %d: %p -> TIMEOUT\n", __func__, task->id, task);
		if (task->core != 0) {
			task->core->need_soft_reset = true;
		}
		task->OutParam.hwStatus = hadespp_timeout;
		ret = -ETIMEDOUT;
	}

bail:
	*t = task;
	return ret;
}

/**
 * Remove and returns one task of SW fifo
 * May blocks on task completion
 * @instance pointer to instance
 * if successful returns 0, else <0
 */
int HadesppPopTask(struct HadesppOpenContext *instance, struct HadesppTask *t)
{
	unsigned int read_idx = 0;
	struct HadesppTask *task;
	unsigned long flags;
	int ret = 0;
	bool taskTimeout = false;

	spin_lock_irqsave(&instance->fifo_task_lock, flags);
	read_idx = instance->output_task_idx;
	task = &instance->task[read_idx];
	spin_unlock_irqrestore(&instance->fifo_task_lock, flags);

	//pr_info("%s wait for task %d\n", __func__, task->id);

	/* Waiting for task completion */
	if (!wait_for_completion_timeout
	    (&task->completed, msecs_to_jiffies(HADESPP_TIMEOUT_MS))) {
		pr_info("Hadespp: %s wait for task %d: %p -> TIMEOUT\n",
		        __func__, task->id, task);
		taskTimeout = true;
		ret = -ETIMEDOUT;
	}

	spin_lock_irqsave(&instance->fifo_task_lock, flags);
	if (taskTimeout) {
		if (task->core != 0) {
			task->core->need_soft_reset = true;
		}
		task->OutParam.hwStatus = hadespp_timeout;
	}
	atomic_dec(&instance->pending_task_nb);
	instance->output_task_idx++;
	if (instance->output_task_idx == HADESPP_TASK_FIFO_SIZE) {
		instance->output_task_idx = 0;
	}
	spin_unlock_irqrestore(&instance->fifo_task_lock, flags);

	/* memcpy is NEEDED as task[i] may be overwritten as soon as task_wq event is raised ! */
	*t = *task;
	wake_up_interruptible(&instance->task_wq);

	return ret;
}

/**
 * Wait for end of task from HW PP (+ deal with clock and power of selected core)
 */
void HadesppHandleEndOfTask(struct HadesppOpenContext *instance, struct HadesppTask *task)
{
	struct Hadespp *hadespp = (struct Hadespp *)instance->data;
	struct HadesppCore *core = task->core;
	unsigned long flags;

	BUG_ON(!core);

	HadesppDebug(task);

	if (task->InParam.codec == CODEC_AVSP) {
		HadesppClockOff_Avsp(core);
	} else {
		HadesppClockOff_Hevc(core);
	}

	/** pm_runtime_put -> decrement pm ref_counter and if == 0 calls HadesppPmRuntimeSuspend */
	pm_runtime_mark_last_busy(hadespp->dev);
	pm_runtime_put(hadespp->dev);
	task->decode_time_sw = OSDEV_GetTimeInMilliSeconds() - task->decode_time_sw;

#ifdef CONFIG_STM_HADES_DEBUG_TOOLS
	if (hadespp->crc_checker != NULL) {
		CHK_CheckPPCRC(hadespp->crc_checker, &task->crcs);
	}
#endif
	if (outputpreprocessingtime) {
		uint32_t error_sts = task->OutParam.iStatus.hevc_iStatus.error;
		if (task->InParam.codec == CODEC_AVSP) {
			error_sts = task->OutParam.iStatus.avsp_iStatus.error;
		} else {
			error_sts = task->OutParam.iStatus.h264_iStatus.error;
		}
		st_relayfs_print_se(ST_RELAY_TYPE_HEVC_HW_DECODING_TIME, ST_RELAY_SOURCE_SE,
		                    "preprocessor,%d,%d,%d,%d,%d\n",
		                    0,
		                    task->InParam.iDecodeIndex,
		                    error_sts,
		                    task->OutParam.decode_time_hw,  // HW time
		                    task->decode_time_sw); // Linux: milliseconds
	}
	//pr_info("%s  HadesppCheckStatus task %d (decode_time_hw: %u us)\n",
	// __func__, task->id, task->OutParam.decode_time_hw);
	/* Up semaphore as soon as possible : to start new task */
	spin_lock_irqsave(&hadespp->core_lock, flags);
	core->inuse = false;
	spin_unlock_irqrestore(&hadespp->core_lock, flags);
	up(&hadespp->pp_unactive_lock);
	complete(&task->completed);
}

/**
 * Open function creates a new internal instance
 */
static OSDEV_OpenEntrypoint(HadesppOpen)
{
	struct HadesppOpenContext *instance;

	OSDEV_OpenEntry();

	BUG_ON(!hadespp);

	instance = kzalloc(sizeof(*instance), GFP_KERNEL);
	if (!instance) {
		pr_err("Error: %s Unable to allocate instance\n", __func__);
		OSDEV_OpenExit(OSDEV_Error);
	}

	spin_lock_init(&instance->fifo_task_lock);

	init_waitqueue_head(&instance->task_wq);
	instance->data = hadespp;
	OSDEV_PrivateData = instance;

	atomic_inc(&hadespp->instance_nb);

#ifdef CONFIG_STM_HADES_DEBUG_TOOLS
	CRCDriver_GetChecker(0, &hadespp->crc_checker); // JLX: minor set to 0, assuming there is only 1 stream playing
#endif
	OSDEV_OpenExit(OSDEV_NoError);
}

/**
 * Close function removes instance after ensuring no task is pending
 */
static OSDEV_CloseEntrypoint(HadesppClose)
{
	struct HadesppOpenContext *instance;
	struct HadesppTask task;

	OSDEV_CloseEntry();

	BUG_ON(!hadespp);

	instance = (struct HadesppOpenContext *)OSDEV_PrivateData;

	/* Remove pending tasks */
	while (atomic_read(&instance->pending_task_nb)) {
		HadesppPopTask(instance, &task);
	}

	atomic_dec(&hadespp->instance_nb);
	kfree(instance);
	OSDEV_PrivateData = NULL;
#ifdef CONFIG_STM_HADES_DEBUG_TOOLS
	// JLX: minor set to 0, assuming there is only 1 stream playing
	if (hadespp->crc_checker != NULL) { CRCDriver_ReleaseChecker(0); }
#endif

	OSDEV_CloseExit(OSDEV_NoError);
}

#ifdef HEVC_HADES_CANNESWIFI
/**
 * Program H264 PP registers
 */
void H264ppTaskSet(struct HadesppCore *core, h264preproc_transform_param_t *cmd)
{
	uint32_t word;
	uint32_t PicWidthInMbs, PicHeightInMbs;

	word  = ((cmd->mb_adaptive_frame_field_flag) ? 1 : 0);
	word |= ((cmd->entropy_coding_mode_flag) ? (1 << 1) : 0);
	word |= ((cmd->frame_mbs_only_flag) ? (1 << 2) : 0);
	word |= ((cmd->pic_order_cnt_type & 0x3) << 3);
	word |= ((cmd->pic_order_present_flag) ? (1 << 5) : 0);
	word |= ((cmd->delta_pic_order_always_zero_flag) ? (1 << 6) : 0);
	word |= ((cmd->redundant_pic_cnt_present_flag) ? (1 << 7) : 0);
	word |= ((cmd->weighted_pred_flag) ? (1 << 8) : 0);
	word |= ((cmd->weighted_bipred_idc == 1) ? (1 << 9) : 0);
	word |= ((cmd->deblocking_filter_control_present_flag) ? (1 << 10) : 0);
	word |= ((cmd->num_ref_idx_l0_active_minus1 & 0x1F) << 11);
	word |= ((cmd->num_ref_idx_l1_active_minus1 & 0x1F) << 16);
	word |= (((cmd->pic_init_qp_minus26 + 26) & 0x3F) << 21);
	word |= ((cmd->transform_8x8_mode_flag) ? (1 << 27) : 0);
	word |= (0 << 28); //controle mode: Start/stop
	word |= ((cmd->direct_8x8_inference_flag) ? (1 << 30) : 0);
	word |= ((cmd->chroma_format_idc == 0) ? (1 << 31) : (0 << 31));

	writel(word, (volatile void *)(core->hw_base_addr + H264_PP_CFG));

	PicWidthInMbs = cmd->pic_width_in_mbs_minus1 + 1;
	PicHeightInMbs = ((2 - (cmd->frame_mbs_only_flag ? 1 : 0)) * (cmd->pic_height_in_map_units_minus1 + 1)) /
	                 (1 + (cmd->field_pic_flag ? 1 : 0));
	word = (((PicWidthInMbs * PicHeightInMbs - 1) & 0xFFFF) << 16) | (PicWidthInMbs & 0xFFFF);

	writel(word, (volatile void *)(core->hw_base_addr + H264_PP_PICWIDTH));

	word  = ((cmd->log2_max_frame_num_minus4 + 4) & 0x1F);
	word |= (((cmd->log2_max_pic_order_cnt_lsb_minus4 + 4) & 0x1F) << 5);
	word |= ((cmd->slice_group_change_cycle_size & 0x1F) << 10);
	word |= ((cmd->num_slice_group_minus1 & 0xF) << 15);
	word |= ((cmd->slice_group_map_type & 0x7) << 18);
	word |= ((cmd->fmo_aso_en & 0x1) << 20);

	writel(word, (volatile void *)(core->hw_base_addr + H264_PP_CODELENGTH));

	//pr_info("%s bit_buf.base_addr: 0x%x (offset 0x%x) length: %d\n", __func__,
	//cmd->bit_buf.base_addr, cmd->bit_buf.start_offset, cmd->bit_buf.length);
	//TODO use buffer struct
	writel(cmd->bit_buf.base_addr, (volatile void *)(core->hw_base_addr + HADESPP_BIT_BUFFER_BASE_ADDR));
	writel(cmd->bit_buf.length, (volatile void *)(core->hw_base_addr + HADESPP_BIT_BUFFER_LENGTH));
	writel(cmd->bit_buf.start_offset, (volatile void *)(core->hw_base_addr + HADESPP_BIT_BUFFER_START_OFFSET));
	writel(cmd->bit_buf.end_offset, (volatile void *)(core->hw_base_addr + HADESPP_BIT_BUFFER_END_OFFSET));

	//pr_info("%s sesb_start: 0x%x sesb_length: %d\n", __func__, cmd->sesb_start, cmd->sesb_length);
	writel(cmd->sesb_start, (volatile void *)(core->hw_base_addr + H264_PP_SESB_BASE_ADDR));
	writel(cmd->sesb_length, (volatile void *)(core->hw_base_addr + H264_PP_SESB_LENGTH));
	writel(cmd->sesb_length, (volatile void *)(core->hw_base_addr + H264_PP_SESB_END_OFFSET));

	//pr_info("%s slice_data_start: 0x%x slice_data_length: %d\n", __func__,
	//cmd->slice_data_start, cmd->slice_data_length);
	writel(cmd->slice_data_start, (volatile void *)(core->hw_base_addr + H264_PP_SLICE_DATA_BASE_ADDR));
	writel(cmd->slice_data_length, (volatile void *)(core->hw_base_addr + H264_PP_SLICE_DATA_LENGTH));
	writel(cmd->slice_data_length, (volatile uint32_t *)(core->hw_base_addr + H264_PP_SLICE_DATA_END_OFFSET));
#ifdef DEBUG_MODE
	DUMP_REG(H264_PP_CFG);
	DUMP_REG(H264_PP_PICWIDTH);
	DUMP_REG(H264_PP_CODELENGTH);
#endif

	//TODO unused for cannes wifi
	//pr_info("%s res_start: 0x%x length: %d\n", __func__,
	//cmd->slice_data_start+0x1000, 0x1000);
	//writel(cmd->slice_data_start+0x1000, (volatile void *)(core->hw_base_addr + H264_PP_RESIDUALS_BASE_ADDR));
	//writel(0x10000, (volatile void *)(core->hw_base_addr + H264_PP_RESIDUALS_LENGTH));
	//writel(0x10000, (volatile uint32_t *)(core->hw_base_addr + H264_PP_RESIDUALS_END_OFFSET));
	/* TBC !!!!!
	   H264_CHKSYN_DIS -> 0 ok (all checks enabled)
	   H264_PP_DEBUG
	   H264_PP_NUT_DIS -> disable certain NAL types */
}
#endif /* HEVC_HADES_CANNESWIFI */

#if defined (HEVC_HADES_CANNES25) || defined (HEVC_HADES_CANNESWIFI)
/**
 * Program AVS+ PP registers
 */
void AvsplusppTaskSet(struct HadesppCore *core, avspluspreproc_transform_param_t *cmd)
{
	uint32_t word;

	word = (uint32_t)cmd->InputBufferPhysicalAddress  & (~HADESPP_BUFFER_ALIGNMENT);
	writel(word, (volatile void *)(core->hw_base_addr + HADESPP_BIT_BUFFER_BASE_ADDR));
	//pr_err("AVSPDEBUG HADESPP_BIT_BUFFER_BASE_ADDR 0x%08x\n",word);

	word = ((cmd->InputBufferSize - 4) & 0xffffffff);
	writel(word, (volatile void *)(core->hw_base_addr + HADESPP_BIT_BUFFER_LENGTH));
	//pr_err("AVSPDEBUG HADESPP_BIT_BUFFER_LENGTH 0x%08x\n",word);

	word = (uint32_t)cmd->InputBufferPhysicalAddress & (HADESPP_BUFFER_ALIGNMENT);
	writel(word, (volatile void *)(core->hw_base_addr + HADESPP_BIT_BUFFER_START_OFFSET));
	//pr_err("AVSPDEBUG HADESPP_BIT_BUFFER_START_OFFSET 0x%08x\n",word);

	word = ((cmd->InputBufferSize + HADESPP_BUFFER_ALIGNMENT) & 0xffffffff);
	writel(word, (volatile void *)(core->hw_base_addr + HADESPP_BIT_BUFFER_END_OFFSET));
	//pr_err("AVSPDEBUG HADESPP_BIT_BUFFER_END_OFFSET 0x%08x\n",word);

	word = 0;
	writel(word, (volatile void *)(core->hw_base_addr + AVSP_PP_CFG1));

	word = 0;
	word |= ((((cmd->avsp_mb_horizontal_size + 15) >> 4) & 0x7f) << 0);
	word |= ((((cmd->avsp_mb_vertical_size + 15) >> 4) & 0x7f) << 7);
	word |= (((cmd->avsp_aec_enable) & 0x1) << 14);
	word |= (((cmd->avsp_skip_mode_flag) & 0x1) << 15);
	word |= (((cmd->avsp_chroma_format) & 0x3) << 16);
	word |= (((cmd->avsp_picture_structure) & 0x1) << 18);
	word |= (((cmd->avsp_picture_type) & 0x7) << 19);
	word |= (((cmd->avsp_picture_ref_flag) & 0x1) << 22);
	word |= (((cmd->avsp_prevPictureType) & 0x7) << 23);
	word |= (((cmd->avsp_outEndianness) & 0x1) << 26);
	writel(word, (volatile void *)(core->hw_base_addr + AVSP_PP_CFG2));
	//pr_err("AVSPDEBUG CFG2 0x%08x\n",word);

	word = 0;
	word |= (((cmd->avsp_fixed_pic_qp) & 0x1) << 0);
	word |= (((cmd->avsp_wq_flag) & 0x1) << 1);
	word |= (((cmd->avsp_mb_adapt_wq_disable) & 0x1) << 2);
	word |= (((cmd->avsp_wq_model) & 0x3) << 3);
	word |= (((cmd->avsp_wq_param_idx) & 0x3) << 5);
	word |= (((cmd->avsp_picture_qp) & 0x3f) << 7);
	word |= (((cmd->avsp_chroma_quant_param_disable) & 0x1) << 16);
	word |= (((cmd->avsp_chroma_quant_param_delta_cb) & 0x3f) << 17);
	word |= (((cmd->avsp_chroma_quant_param_delta_cr) & 0x3f) << 23);
	writel(word, (volatile void *)(core->hw_base_addr + AVSP_PP_CFG3));
	//pr_err("AVSPDEBUG CFG3 0x%08x\n",word);

	word = 0;
	word |= (((cmd->avsp_weighting_quant_param_delta1[0]) & 0xff) << 0);
	word |= (((cmd->avsp_weighting_quant_param_delta1[1]) & 0xff) << 8);
	word |= (((cmd->avsp_weighting_quant_param_delta1[2]) & 0xff) << 16);
	word |= (((cmd->avsp_weighting_quant_param_delta1[3]) & 0xff) << 24);
	writel(word, (volatile void *)(core->hw_base_addr + AVSP_PP_CFG4));

	word = 0;
	word |= (((cmd->avsp_weighting_quant_param_delta1[4]) & 0xff) << 0);
	word |= (((cmd->avsp_weighting_quant_param_delta1[5]) & 0xff) << 8);
	word |= (((cmd->avsp_weighting_quant_param_delta2[0]) & 0xff) << 16);
	word |= (((cmd->avsp_weighting_quant_param_delta2[1]) & 0xff) << 24);
	writel(word, (volatile void *)(core->hw_base_addr + AVSP_PP_CFG5));

	word = 0;
	word |= (((cmd->avsp_weighting_quant_param_delta2[2]) & 0xff) << 0);
	word |= (((cmd->avsp_weighting_quant_param_delta2[3]) & 0xff) << 8);
	word |= (((cmd->avsp_weighting_quant_param_delta2[4]) & 0xff) << 16);
	word |= (((cmd->avsp_weighting_quant_param_delta2[5]) & 0xff) << 24);
	writel(word, (volatile void *)(core->hw_base_addr + AVSP_PP_CFG6));

	word = ((((unsigned int)cmd->sesb_start) & 0xffffffff));
	writel(word, (volatile void *)(core->hw_base_addr + AVSP_PP_SESB_BASE_ADDR));
	//pr_err("AVSPDEBUG AVSP_PP_SESB_BASE_ADDR 0x%08x\n",word);

	word = cmd->sesb_length;
	writel(word, (volatile void *)(core->hw_base_addr + AVSP_PP_SESB_LENGTH));
	//pr_err("AVSPDEBUG AVSP_PP_SESB_LENGTH 0x%08x\n",word);

	word = cmd->sesb_length;
	writel(word, (volatile void *)(core->hw_base_addr + AVSP_PP_SESB_END_OFFSET));
	//pr_err("AVSPDEBUG AVSP_PP_SESB_END_OFFSET 0x%08x\n",word);

	word = ((((unsigned int)cmd->slice_data_start) & 0xffffffff)); // + AVSP_PP_MAX_SESB_SIZE
	writel(word, (volatile void *)(core->hw_base_addr + AVSP_PP_SLICE_DATA_BASE_ADDR));
	//pr_err("AVSPDEBUG AVSP_PP_SLICE_DATA_BASE_ADDR 0x%08x\n",word);

	word = cmd->slice_data_length;
	writel(word, (volatile void *)(core->hw_base_addr + AVSP_PP_SLICE_DATA_LENGTH));
	//pr_err("AVSPDEBUG AVSP_PP_SLICE_DATA_LENGTH 0x%08x\n",word);

	word = cmd->slice_data_length;
	writel(word, (volatile void *)(core->hw_base_addr + AVSP_PP_SLICE_DATA_END_OFFSET));
	//pr_err("AVSPDEBUG AVSP_PP_SLICE_DATA_END_OFFSET 0x%08x\n",word);

	word = 0;
#if defined (HEVC_HADES_CANNESWIFI)
	// AVS+ ERC is disabled on CANNES 25 -> TBC for cut 2
	// Recommended value if 0x619
	word = (AVSP_PP_CHKSYN_DIS_MARKER_BIT | AVSP_PP_CHKSYN_DIS_DATA_ALIGNMENT |
	        AVSP_PP_CHKSYN_DIS_TRAILING_BITS | AVSP_PP_CHKSYN_DIS_HEADER_ALIGNMENT |
	        AVSP_PP_CHKSYN_DIS_FORBIDDEN_BYTE_SEQUENCE);
#endif
	word &= 0xffffffff;
	cmd->avsp_chksyn_dis = word;
	writel(word, (volatile void *)(core->hw_base_addr + AVSP_PP_CHKSYN_DIS));
}
#endif /* (HEVC_HADES_CANNES25 || HEVC_HADES_CANNESWIFI)*/

/**
 * Program HEVC PP registers
 */
void HevcppTaskSet(struct HadesppCore *core, hevcpreproc_transform_param_t *cmd)
{
	int width_computed = 0;
	int height_computed = 0;
	int tile_col_end_prev = -1;
	int tile_row_end_prev = -1;
	uint32_t word;
#if defined (HEVC_HADES_CANNES25) || defined (HEVC_HADES_CANNESWIFI)
	int max_renorm_bits, offset;
#endif
	word = 0;
	word |= ((cmd->pic_width_in_luma_samples) & 0xffff);
	word |= (((cmd->pic_height_in_luma_samples) & 0xffff) << 16);
	writel(word, (volatile void *)(core->hw_base_addr + HADESPP_PICTURE_SIZE));

	word = 0;
	word |= ((cmd->chroma_format_idc) & 0x3);
	word |= (((cmd->separate_colour_plane_flag) & 0x1) << 2);
	word |= (((cmd->log2_min_coding_block_size_minus3) & 0x3) << 8);
	word |= (((cmd->log2_diff_max_min_coding_block_size) & 0x3) << 10);
	word |= (((cmd->amp_enabled_flag) & 0x1) << 16);
#if defined (HEVC_HADES_CANNES25) || defined (HEVC_HADES_CANNESWIFI)
	word |= (((cmd->bit_depth_luma_minus8) & 0x7) << 24);
	word |= (((cmd->bit_depth_chroma_minus8) & 0x7) << 27);
#endif
	writel(word, (volatile void *)(core->hw_base_addr + HEVCPP_CTB_CONFIG));

	word = 0;
	word |= ((cmd->tiles_enabled_flag) & 0x1);
	word |= (((cmd->entropy_coding_sync_enabled_flag) & 0x1) << 1);
	word |= (((cmd->num_tile_columns) & 0x1F) << 8);
	word |= (((cmd->num_tile_rows) & 0x1F) << 16);
	writel(word, (volatile void *)(core->hw_base_addr + HEVCPP_PARALLEL_CONFIG));

#define TILE_COL_END(i, reg) \
	word = 0; \
	width_computed = cmd->tile_column_width[(i) * 2]; \
	word |= ((width_computed + tile_col_end_prev) & 0xffff ); \
	tile_col_end_prev = (word & 0xffff) ; \
	width_computed = cmd->tile_column_width[(i) * 2 + 1]; \
	word |= (((width_computed + tile_col_end_prev) & 0xffff )<< 16); \
	tile_col_end_prev = (word >> 16) & 0xffff ; \
	writel(word, (volatile void *)(core->hw_base_addr + reg));

	TILE_COL_END(0, HEVCPP_TILE_COL_END_0);
	TILE_COL_END(1, HEVCPP_TILE_COL_END_1);
	TILE_COL_END(2, HEVCPP_TILE_COL_END_2);
	TILE_COL_END(3, HEVCPP_TILE_COL_END_3);
	TILE_COL_END(4, HEVCPP_TILE_COL_END_4);

#define TILE_ROW_END_0(i, reg) \
	word = 0; \
	height_computed = cmd->tile_row_height[(i)*2]; \
	word |= ((height_computed + tile_row_end_prev) & 0xffff); \
	tile_row_end_prev = (word & 0xffff) ; \
	writel(word, (volatile void *)(core->hw_base_addr + reg));

#define TILE_ROW_END_16(i, reg) \
	height_computed = cmd->tile_row_height[(i)*2+1]; \
	word |= (((height_computed + tile_row_end_prev) & 0xffff ) << 16); \
	tile_row_end_prev = (word >> 16) & 0xffff ; \
	writel(word, (volatile void *)(core->hw_base_addr + reg));

#define TILE_ROW_END(i, reg) TILE_ROW_END_0 (i, reg); TILE_ROW_END_16(i, reg);

	TILE_ROW_END(0, HEVCPP_TILE_ROW_END_0);
	TILE_ROW_END(1, HEVCPP_TILE_ROW_END_1);
	TILE_ROW_END(2, HEVCPP_TILE_ROW_END_2);
	TILE_ROW_END(3, HEVCPP_TILE_ROW_END_3);
	TILE_ROW_END(4, HEVCPP_TILE_ROW_END_4);
	TILE_ROW_END_0(5, HEVCPP_TILE_ROW_END_5);

	word = 0;
	word |= ((cmd->dependent_slice_segments_enabled_flag) & 0x1);
	word |= (((cmd->output_flag_present_flag) & 0x1) << 1);
	word |= (((cmd->cabac_init_present_flag) & 0x1) << 2);
	word |= (((cmd->slice_segment_header_extension_flag) & 0x1) << 3);
	word |= (((cmd->log2_max_pic_order_cnt_lsb_minus4) & 0xF) << 8);
	word |= (((cmd->weighted_pred_flag) & 0x1) << 16);
	word |= (((cmd->weighted_bipred_flag) & 0x1) << 17);
	word |= (((cmd->sps_temporal_mvp_enabled_flag) & 0x1) << 18);
	word |= (((cmd->num_extra_slice_header_bits) & 0x7) << 19);
	word |= (((cmd->pps_slice_chroma_qp_offsets_present_flag) & 0x1) << 22);
	writel(word, (volatile void *)(core->hw_base_addr + HEVCPP_SLICE_CONFIG));

	word = 0;
	word |= ((cmd->num_ref_idx_l0_default_active_minus1) & 0xF);
	word |= (((cmd->num_ref_idx_l1_default_active_minus1) & 0xF) << 4);
	word |= (((cmd->long_term_ref_pics_present_flag) & 0x1) << 8);
	word |= (((cmd->num_long_term_ref_pics_sps) & 0x3F) << 9);
	word |= (((cmd->lists_modification_present_flag) & 0x1) << 15);
	word |= (((cmd->num_poc_total_curr) & 0xF) << 16);
	word |= (((cmd->num_short_term_ref_pic_sets) & 0x7F) << 24);
	writel(word, (volatile void *)(core->hw_base_addr + HEVCPP_REF_LIST_CONFIG));

#define NUM_DELTA_POC(i, reg) \
	word = 0; \
	word |= ((cmd->num_delta_pocs[(i) * 4 + 0]) & 0x1f ); \
	word |= (((cmd->num_delta_pocs[(i) * 4 + 1]) & 0x1f )<< 8); \
	word |= (((cmd->num_delta_pocs[(i) * 4 + 2]) & 0x1f )<< 16); \
	word |= (((cmd->num_delta_pocs[(i) * 4 + 3]) & 0x1f )<< 24); \
	writel(word, (volatile void *)(core->hw_base_addr + reg));

	NUM_DELTA_POC(0, HEVCPP_NUM_DELTA_POC_0);
	NUM_DELTA_POC(1, HEVCPP_NUM_DELTA_POC_1);
	NUM_DELTA_POC(2, HEVCPP_NUM_DELTA_POC_2);
	NUM_DELTA_POC(3, HEVCPP_NUM_DELTA_POC_3);
	NUM_DELTA_POC(4, HEVCPP_NUM_DELTA_POC_4);
	NUM_DELTA_POC(5, HEVCPP_NUM_DELTA_POC_5);
	NUM_DELTA_POC(6, HEVCPP_NUM_DELTA_POC_6);
	NUM_DELTA_POC(7, HEVCPP_NUM_DELTA_POC_7);
	NUM_DELTA_POC(8, HEVCPP_NUM_DELTA_POC_8);
	NUM_DELTA_POC(9, HEVCPP_NUM_DELTA_POC_9);
	NUM_DELTA_POC(10, HEVCPP_NUM_DELTA_POC_10);
	NUM_DELTA_POC(11, HEVCPP_NUM_DELTA_POC_11);
	NUM_DELTA_POC(12, HEVCPP_NUM_DELTA_POC_12);
	NUM_DELTA_POC(13, HEVCPP_NUM_DELTA_POC_13);
	NUM_DELTA_POC(14, HEVCPP_NUM_DELTA_POC_14);
	NUM_DELTA_POC(15, HEVCPP_NUM_DELTA_POC_15);

	word = 0;

	word |= ((cmd->log2_min_transform_block_size_minus2) & 0x3);
	word |= (((cmd->log2_diff_max_min_transform_block_size) & 0x3) << 2);
	word |= (((cmd->max_transform_hierarchy_depth_intra) & 0x7) << 4);
	word |= (((cmd->max_transform_hierarchy_depth_inter) & 0x7) << 7);
	word |= (((cmd->transquant_bypass_enable_flag) & 0x1) << 13);
	word |= (((cmd->transform_skip_enabled_flag) & 0x1) << 14);
	word |= (((cmd->sign_data_hiding_flag) & 0x1) << 15);
#if defined(HEVC_HADES_CANNES25) || defined(HEVC_HADES_CANNESWIFI)
	word |= (((cmd->init_qp_minus26) & 0x7F) << 16);
#else
	word |= (((cmd->init_qp_minus26) & 0x3F) << 16);
#endif
	word |= (((cmd->cu_qp_delta_enabled_flag) & 0x1) << 24);
	word |= (((cmd->diff_cu_qp_delta_depth) & 0x3) << 25);
	writel(word, (volatile void *)(core->hw_base_addr + HEVCPP_RESIDUAL_CONFIG));

	word = 0;
	word |= ((cmd->pcm_enabled_flag) & 0x1);
	word |= (((cmd->log2_min_pcm_coding_block_size_minus3) & 0x3) << 8);
	word |= (((cmd->log2_diff_max_min_pcm_coding_block_size) & 0x3) << 10);
	word |= (((cmd->pcm_bit_depth_luma_minus1) & 0xF) << 16);
	word |= (((cmd->pcm_bit_depth_chroma_minus1) & 0xF) << 24);
	writel(word, (volatile void *)(core->hw_base_addr + HEVCPP_IPCM_CONFIG));

	word = 0;
	word |= ((cmd->pps_loop_filter_across_slices_enabled_flag) & 0x1);
	word |= (((cmd->deblocking_filter_control_present_flag) & 0x1) << 1);
	word |= (((cmd->sample_adaptive_offset_enabled_flag) & 0x1) << 2);
	word |= (((cmd->deblocking_filter_override_enabled_flag) & 0x1) << 8);
	word |= (((cmd->pps_deblocking_filter_disable_flag) & 0x1) << 9);
	word |= (((cmd->pps_beta_offset_div2) & 0xF) << 16);
	word |= (((cmd->pps_tc_offset_div2) & 0xF) << 24);
	writel(word, (volatile void *)(core->hw_base_addr + HEVCPP_LOOPFILTER_CONFIG));

	word = ((cmd->bit_buffer.base_addr) & 0xffffffff);
	writel(word, (volatile void *)(core->hw_base_addr + HADESPP_BIT_BUFFER_BASE_ADDR));

	word = ((cmd->bit_buffer.length) & 0xffffffff);
	writel(word, (volatile void *)(core->hw_base_addr + HADESPP_BIT_BUFFER_LENGTH));

	word = ((cmd->bit_buffer.start_offset) & 0xffffffff);
	writel(word, (volatile void *)(core->hw_base_addr + HADESPP_BIT_BUFFER_START_OFFSET));

#if defined(HEVC_HADES_CANNES25) || defined(HEVC_HADES_CANNESWIFI)
	word = ((cmd->bit_buffer.end_offset - cmd->bit_buffer.start_offset) & 0xffffffff);
	writel(word, (volatile void *)(core->hw_base_addr + HADESPP_BIT_BUFFER_END_OFFSET));
#else
	word = ((cmd->bit_buffer.end_offset) & 0xffffffff);
	writel(word, (volatile void *)(core->hw_base_addr + HADESPP_BIT_BUFFER_STOP_OFFSET));
#endif

	word = ((cmd->intermediate_buffer.slice_table_base_addr) & 0xffffffff);
	writel(word, (volatile void *)(core->hw_base_addr + HEVCPP_SLICE_TABLE_BASE_ADDR));

	word = ((cmd->intermediate_buffer.ctb_table_base_addr) & 0xffffffff);
	writel(word, (volatile void *)(core->hw_base_addr + HEVCPP_CTB_TABLE_BASE_ADDR));

	word = ((cmd->intermediate_buffer.slice_headers_base_addr) & 0xffffffff);
	writel(word, (volatile void *)(core->hw_base_addr + HEVCPP_SLICE_HEADERS_BASE_ADDR));

	word = ((cmd->intermediate_buffer.ctb_commands.base_addr) & 0xffffffff);
	writel(word, (volatile void *)(core->hw_base_addr + HEVCPP_CTB_COMMANDS_BASE_ADDR));

	word = ((cmd->intermediate_buffer.ctb_commands.length) & 0xffffffff);
	writel(word, (volatile void *)(core->hw_base_addr + HEVCPP_CTB_COMMANDS_LENGTH));

	word = ((cmd->intermediate_buffer.ctb_commands.start_offset) & 0xffffffff);
	writel(word, (volatile void *)(core->hw_base_addr + HEVCPP_CTB_COMMANDS_START_OFFSET));

#if defined(HEVC_HADES_CANNES25) || defined(HEVC_HADES_CANNESWIFI)
	word = ((cmd->intermediate_buffer.ctb_commands.end_offset - cmd->intermediate_buffer.ctb_commands.start_offset) &
	        0xffffffff);
#else
	word = ((cmd->intermediate_buffer.ctb_commands.end_offset) & 0xffffffff);
#endif
	writel(word, (volatile void *)(core->hw_base_addr + HEVCPP_CTB_COMMANDS_END_OFFSET));

	word = ((cmd->intermediate_buffer.ctb_residuals.base_addr) & 0xffffffff);
	writel(word, (volatile void *)(core->hw_base_addr + HEVCPP_CTB_RESIDUALS_BASE_ADDR));

	word = ((cmd->intermediate_buffer.ctb_residuals.length) & 0xffffffff);
	writel(word, (volatile void *)(core->hw_base_addr + HEVCPP_CTB_RESIDUALS_LENGTH));

	word = ((cmd->intermediate_buffer.ctb_residuals.start_offset) & 0xffffffff);
	writel(word, (volatile void *)(core->hw_base_addr + HEVCPP_CTB_RESIDUALS_START_OFFSET));

#if defined(HEVC_HADES_CANNES25) || defined(HEVC_HADES_CANNESWIFI)
	word = ((cmd->intermediate_buffer.ctb_residuals.end_offset -
	         cmd->intermediate_buffer.ctb_residuals.start_offset) & 0xffffffff);
#else
	word = ((cmd->intermediate_buffer.ctb_residuals.end_offset) & 0xffffffff);
#endif
	writel(word, (volatile void *)(core->hw_base_addr + HEVCPP_CTB_RESIDUALS_END_OFFSET));

	word = ((cmd->intermediate_buffer.slice_table_length) & 0xffffffff);
	writel(word, (volatile void *)(core->hw_base_addr + HEVCPP_SLICE_TABLE_LENGTH));

	word = ((cmd->intermediate_buffer.ctb_table_length) & 0xffffffff);
	writel(word, (volatile void *)(core->hw_base_addr + HEVCPP_CTB_TABLE_LENGTH));

	word = ((cmd->intermediate_buffer.slice_headers_length) & 0xffffffff);
	writel(word, (volatile void *)(core->hw_base_addr + HEVCPP_SLICE_HEADERS_LENGTH));

	// ERC settings : 0x600
	word = HEVCPP_CHKSYN_DIS_RBSP_TRAILING_DATA | HEVCPP_CHKSYN_DIS_RBSP_TRAILING_BITS;
	writel(word, (volatile void *)(core->hw_base_addr + HEVCPP_CHKSYN_DIS));

#if defined(HEVC_HADES_CANNES25) || defined(HEVC_HADES_CANNESWIFI)
	offset = 1 << ((cmd->log2_min_coding_block_size_minus3 + 3 + cmd->log2_diff_max_min_coding_block_size) << 1);
	max_renorm_bits = offset * (cmd->bit_depth_luma_minus8 + 8) + (offset >> 1) * (cmd->bit_depth_chroma_minus8 +
	                                                                               8); // RawCtuBits
	max_renorm_bits *= 5;
	max_renorm_bits /= 3;
	writel(max_renorm_bits, (volatile void *)(core->hw_base_addr + HEVCPP_MAX_RENORM_BITS));
#endif
}

/**
 * Actually launches task in HW
 */
static OSDEV_Status_t HadesppTriggerTask(struct HadesppTask *task)
{
	struct hadespp_ioctl_queue_t *in_param;
	struct HadesppCore *core = NULL;
	int ret = 0;
#if defined(HEVC_HADES_CANNES25) || defined(HEVC_HADES_CANNESWIFI)
	uint32_t disable_val = 0;
#endif
	BUG_ON(!hadespp);
	BUG_ON(!task->core);

	core = task->core;
	in_param = &task->InParam;
	core->current_task = task;

	/* Should power on the core (currently only used to reinit core register) */
	if (pm_runtime_get_sync(hadespp->dev) < 0) {
		pr_err("Error: %s pm_runtime_get_sync failed\n", __func__);
		return OSDEV_Error;
	}

	if (in_param->codec == CODEC_AVSP) {
		ret = HadesppClockOn_Avsp(core);
	} else {
		ret = HadesppClockOn_Hevc(core);
	}

	if (ret) {
		pr_err("Error: %s failed to enable clock\n", __func__);
		pm_runtime_mark_last_busy(hadespp->dev);
		pm_runtime_put(hadespp->dev);
		return OSDEV_Error;
	}

	/* Launch the pre-processor */
	task->decode_time_sw = OSDEV_GetTimeInMilliSeconds();
	//pr_info("%s Launch task %d\n", __func__, task->id);
	ret = OSDEV_NoError;

	/* Start write registers */
	mutex_lock(&core->hw_lock);
#if defined(HEVC_HADES_CANNES25) || defined(HEVC_HADES_CANNESWIFI)
	disable_val = readl((volatile void *)(core->hw_base_addr + HADESPP_DISABLE));
#endif

	switch (in_param->codec) {
	// Program the PP registers according to the pseudo-MME command
	case CODEC_HEVC:
#ifdef HEVC_HADES_CANNESWIFI
		if (disable_val & HADESPP_DISABLE_HEVC) {
			pr_err("Error: %s Not supported codec %d\n", __func__, in_param->codec);
			ret = OSDEV_Error;
			break;
		}
#endif
		HevcppTaskSet(core, &in_param->iCmd.hevc_iCmd);
		writel(HADESPP_START_START_AUTOCLEAR | HADESPP_START_HEVC_CODEC,
		       (volatile void *)(core->hw_base_addr + HADESPP_START));
		break;
#if defined(HEVC_HADES_CANNES25) || defined(HEVC_HADES_CANNESWIFI)
	case CODEC_AVSP:
		if (disable_val & HADESPP_DISABLE_AVS) {
			pr_err("Error: %s Not supported codec %d\n", __func__, in_param->codec);
			ret = OSDEV_Error;
			break;
		}
		AvsplusppTaskSet(core, &in_param->iCmd.avsp_iCmd);
		writel(HADESPP_START_START_AUTOCLEAR | HADESPP_START_AVSP_CODEC,
		       (volatile void *)(core->hw_base_addr + HADESPP_START));
		break;
#endif // HEVC_HADES_CANNES25 || HEVC_HADES_CANNESWIFI
#ifdef HEVC_HADES_CANNESWIFI
	case CODEC_H264:
		if (disable_val & HADESPP_DISABLE_H264) {
			pr_err("Error: %s Not supported codec %d\n", __func__, in_param->codec);
			ret = OSDEV_Error;
			break;
		}
		H264ppTaskSet(core, &in_param->iCmd.h264_iCmd);
		writel(HADESPP_START_START_AUTOCLEAR | HADESPP_START_H264_CODEC,
		       (volatile void *)(core->hw_base_addr + HADESPP_START));
		break;
#endif // HEVC_HADES_CANNESWIFI
	default:
		pr_err("Error: %s Not supported codec %d\n", __func__, in_param->codec);
	}
	mutex_unlock(&core->hw_lock);

	return ret;
}

/**
 * Check AVSP hw status
 */
static hadespp_status_t HadesppAVSPGetHwStatus(uint32_t err_sts, uint32_t chksyn_sts)
{
	hadespp_status_t ret = hadespp_ok;
#ifdef HEVC_HADES_CANNESWIFI
	if (err_sts & (AVSP_PP_ERROR_STATUS_BUFFER1_OVERFLOW | AVSP_PP_ERROR_STATUS_BUFFER2_OVERFLOW /*|
	               AVSP_PP_ERROR_STATUS_MOVE_OUT_OF_DMA*/)) {
		ret = hadespp_unrecoverable_error;
		pr_err("Error: %s HW returns unrecoverable error %#08x\n", __func__, err_sts);
		if (err_sts & AVSP_PP_ERROR_STATUS_BUFFER1_OVERFLOW) {
			pr_err("Error: %s PP slice error & status buffer overflow\n", __func__);
		}
		if (err_sts & AVSP_PP_ERROR_STATUS_BUFFER2_OVERFLOW) {
			pr_err("Error: %s PP data buffer overflow\n", __func__);
		}
	} else if (err_sts & (AVSP_PP_ERROR_STATUS_ERROR_ON_HEADER_FLAG |
	                      AVSP_PP_ERROR_STATUS_ERROR_ON_DATA_FLAG |
	                      AVSP_PP_ERROR_STATUS_UNEXPECTED_SC_DETECTED)) {
		ret = hadespp_recoverable_error;
		if (err_sts & AVSP_PP_ERROR_STATUS_ERROR_ON_HEADER_FLAG) {
			pr_err("Error: %s HW returns recoverable error %#08x -> error on header\n", __func__, err_sts);
		}
		if (err_sts & AVSP_PP_ERROR_STATUS_ERROR_ON_DATA_FLAG) {
			pr_err("Error: %s HW returns recoverable error %#08x -> error on data\n", __func__, err_sts);
		}
		if (err_sts & AVSP_PP_ERROR_STATUS_UNEXPECTED_SC_DETECTED) {
			pr_err("Error: %s HW returns recoverable error %#08x -> error start code detected in the middle of a slice\n", __func__,
			       err_sts);
		}
	} else if ((err_sts & (AVSP_PP_ERROR_STATUS_BUFFER1_DONE | AVSP_PP_ERROR_STATUS_BUFFER2_DONE |
	                       AVSP_PP_ERROR_STATUS_END_OF_PROCESS | AVSP_PP_ERROR_STATUS_END_OF_PICTURE_DETECTED |
	                       AVSP_PP_ERROR_STATUS_END_OF_DMA)) !=
	           (AVSP_PP_ERROR_STATUS_BUFFER1_DONE | AVSP_PP_ERROR_STATUS_BUFFER2_DONE |
	            AVSP_PP_ERROR_STATUS_END_OF_PROCESS | AVSP_PP_ERROR_STATUS_END_OF_PICTURE_DETECTED |
	            AVSP_PP_ERROR_STATUS_END_OF_DMA)) {
		ret = hadespp_recoverable_error;
		pr_err("Error: %s HW returns recoverable error %#08x\n", __func__, err_sts);
	}

	/* Syntax checks */
	if (chksyn_sts & AVSP_PP_CHKSYN_STS_FORBIDDEN_BYTE_SEQUENCE) {
		pr_info("Warning: %s PP_CHKSYN_STS: %#08x -> Forbidden byte sequence\n",  __func__, chksyn_sts);
	}
	if (chksyn_sts & AVSP_PP_CHKSYN_STS_HEADER_RANGE) {
		pr_info("Warning: %s PP_CHKSYN_STS: %#08x -> Header range error\n",  __func__, chksyn_sts);
	}
	if (chksyn_sts & AVSP_PP_CHKSYN_STS_HEADER_COHERENCY) {
		pr_info("Warning: %s PP_CHKSYN_STS: %#08x -> Header coherency error\n",  __func__, chksyn_sts);
	}
	if (chksyn_sts & AVSP_PP_CHKSYN_STS_HEADER_ALIGNMENT) {
		pr_info("Warning: %s PP_CHKSYN_STS: %#08x -> Header alignement error\n",  __func__, chksyn_sts);
	}
	if (chksyn_sts & AVSP_PP_CHKSYN_STS_WEIGHTED_PREDICTION) {
		pr_info("Warning: %s PP_CHKSYN_STS: %#08x -> Weighted pred error\n",  __func__, chksyn_sts);
	}
	if (chksyn_sts & AVSP_PP_CHKSYN_STS_ENTRY_POINT_OFFSETS) {
		pr_info("Warning: %s PP_CHKSYN_STS: %#08x -> Entry point offset error\n",  __func__, chksyn_sts);
	}
	if (chksyn_sts & AVSP_PP_CHKSYN_STS_DATA_RANGE) {
		pr_info("Warning: %s PP_CHKSYN_STS: %#08x -> Data range error\n",  __func__, chksyn_sts);
	}
	if (chksyn_sts & AVSP_PP_CHKSYN_STS_DATA_RENORM) {
		pr_info("Warning: %s PP_CHKSYN_STS: %#08x -> Data renorm error\n",  __func__, chksyn_sts);
	}
	if (chksyn_sts & AVSP_PP_CHKSYN_STS_DATA_ALIGNMENT) {
		pr_info("Warning: %s PP_CHKSYN_STS: %#08x -> Data alignement error\n",  __func__, chksyn_sts);
	}
	if (chksyn_sts & AVSP_PP_CHKSYN_STS_RBSP_TRAILING_BITS) {
		pr_info("Warning: %s PP_CHKSYN_STS: %#08x -> Rbsp trailing bits error\n",  __func__, chksyn_sts);
	}
	if (chksyn_sts & AVSP_PP_CHKSYN_STS_RBSP_TRAILING_DATA) {
		pr_info("Warning: %s PP_CHKSYN_STS: %#08x -> Rbsp trailing data\n",  __func__, chksyn_sts);
	}
#endif
	return ret;
}

/**
 * Check HEVC hw status
 */
static hadespp_status_t HadesppHEVCGetHwStatus(struct HadesppTask *task, uint32_t err_sts, uint32_t chksyn_sts)
{
	hadespp_status_t ret = hadespp_ok;

	// As per ERC architecture doc:
	//-----------------------------------------------------------------------------------------------------
	// Bit                    Meaning                                  Expected   Action
	//-----------------------------------------------------------------------------------------------------
	// BUFFERx_DONE (x1..5) | Intermediate buffer written             | 1       | If 0, discard
	// END_OF_PROCESS       | Decoding is finished                    | 1       | If 0, discard
	// OVERFLOW (x1..5)     | Intermediate buffer overflow            | 0       | If 1, discard
	// END_OF_DMA           | All specified bit buffer has been read  | 1       | If 0, follow policy
	// PICTURE_COMPLETED    | Last CTB read and DMA end               | 1       | If 0, follow policy
	// UNEXPECTED_SC        | Start code in slice header or data      | 0       | If 1, follow policy
	// ERROR_ON_DATA_FLAG   | Error in slice data, will be emulated   | 0       | If 1, follow policy
	// ERROR_ON_HEADER_FLAG | Error in slice header, will be emulated | 0       | If 1, follow policy
	// MOVE_OUT_OF_DMA      | Still parsing at bit buffer end         | 0       | If 1, follow policy
#ifdef HEVC_HADES_CANNESWIFI
	if (((err_sts & (HEVCPP_ERC_NO_ERROR              | HEVCPP_ERROR_STATUS_BUFFER1_DONE |
	                 HEVCPP_ERROR_STATUS_BUFFER2_DONE | HEVCPP_ERROR_STATUS_BUFFER3_DONE |
	                 HEVCPP_ERROR_STATUS_BUFFER4_DONE | HEVCPP_ERROR_STATUS_BUFFER5_DONE))
	     != (HEVCPP_ERC_NO_ERROR              | HEVCPP_ERROR_STATUS_BUFFER1_DONE |
	         HEVCPP_ERROR_STATUS_BUFFER2_DONE | HEVCPP_ERROR_STATUS_BUFFER3_DONE |
	         HEVCPP_ERROR_STATUS_BUFFER4_DONE | HEVCPP_ERROR_STATUS_BUFFER5_DONE)) ||
	    (err_sts & (HEVCPP_ERROR_STATUS_SLICE_TABLE_OVERFLOW   | HEVCPP_ERROR_STATUS_ERROR_ON_HEADER_FLAG |
	                HEVCPP_ERROR_STATUS_SLICE_HEADER_OVERFLOW  | HEVCPP_ERROR_STATUS_CTB_TABLE_OVERFLOW |
	                HEVCPP_ERROR_STATUS_CTB_CMD_OVERFLOW       | HEVCPP_ERROR_STATUS_RES_CMD_OVERFLOW))) {
		pr_err("Error: %s HW returns unrecoverable error %#08x\n", __func__, err_sts);
		ret = hadespp_unrecoverable_error;
	}
#else
	if (((err_sts & (HEVCPP_ERC_NO_ERROR              | HEVCPP_ERROR_STATUS_BUFFER1_DONE |
	                 HEVCPP_ERROR_STATUS_BUFFER2_DONE | HEVCPP_ERROR_STATUS_BUFFER3_DONE |
	                 HEVCPP_ERROR_STATUS_BUFFER4_DONE | HEVCPP_ERROR_STATUS_BUFFER5_DONE))
	     != (HEVCPP_ERC_NO_ERROR              | HEVCPP_ERROR_STATUS_BUFFER1_DONE |
	         HEVCPP_ERROR_STATUS_BUFFER2_DONE | HEVCPP_ERROR_STATUS_BUFFER3_DONE |
	         HEVCPP_ERROR_STATUS_BUFFER4_DONE | HEVCPP_ERROR_STATUS_BUFFER5_DONE)) ||
	    (err_sts & (HEVCPP_ERROR_STATUS_SLICE_TABLE_OVERFLOW   | HEVCPP_ERROR_STATUS_ERROR_ON_HEADER_FLAG |
	                HEVCPP_ERROR_STATUS_SLICE_HEADER_OVERFLOW  | HEVCPP_ERROR_STATUS_CTB_TABLE_OVERFLOW |
	                HEVCPP_ERROR_STATUS_CTB_CMD_OVERFLOW       | HEVCPP_ERROR_STATUS_RES_CMD_OVERFLOW   |
	                HEVCPP_ERROR_STATUS_OVHD_VIOLATION | HEVCPP_ERROR_STATUS_HEVC_VIOLATION))) {
		if (err_sts & HEVCPP_ERROR_STATUS_OVHD_VIOLATION) {
			pr_err("Error: %s HW returns unrecoverable ovhd violation error %#08x\n", __func__, err_sts);
			ret = hevcpp_unrecoverable_ovhd_violation_error;
		} else if (err_sts & HEVCPP_ERROR_STATUS_HEVC_VIOLATION) {
			pr_err("Error: %s HW returns unrecoverable hevc violation error %#08x\n", __func__, err_sts);
			ret = hevcpp_unrecoverable_hevc_violation_error;
		} else {
			pr_err("Error: %s HW returns unrecoverable error %#08x\n", __func__, err_sts);
			if (err_sts & HEVCPP_ERROR_STATUS_RES_CMD_OVERFLOW) {
				pr_err("Error: %s HW returns RES_CMD_OVERFLOW (ctb_residuals_stop_offset: %d / length: %d)\n",
				       __func__, task->OutParam.iStatus.hevc_iStatus.ctb_residuals_stop_offset,
				       task->InParam.iCmd.hevc_iCmd.intermediate_buffer.ctb_residuals.length);
			}
			ret = hadespp_unrecoverable_error;
		}
	} else if (task->OutParam.iStatus.hevc_iStatus.slice_table_entries == 0) {
		pr_err("Error: %s HW returns unrecoverable error %#08x with no slices in picture\n", __func__, err_sts);
		ret = hadespp_unrecoverable_error;
	} else if (((err_sts & (HEVCPP_ERROR_STATUS_PICTURE_COMPLETED | HEVCPP_ERROR_STATUS_END_OF_DMA))
	            != (HEVCPP_ERROR_STATUS_PICTURE_COMPLETED | HEVCPP_ERROR_STATUS_END_OF_DMA))) {
		if (err_sts & HEVCPP_ERROR_STATUS_ERROR_ON_HEADER_FLAG) {
			pr_err("Error: %s HW returns recoverable ERROR_ON_HEADER_FLAG error %#08x\n", __func__, err_sts);
			ret = hadespp_recoverable_error;
		}
		if (err_sts & HEVCPP_ERROR_STATUS_UNEXPECTED_SC) {
			pr_err("Error: %s HW returns recoverable HEVCPP_ERROR_STATUS_UNEXPECTED_SC error %#08x\n", __func__, err_sts);
			ret = hadespp_recoverable_error;
		}
		// If this flag PICTURE_COMPLETED is not set, hades may hang in decode stage -> mandatory
		if ((err_sts & HEVCPP_ERROR_STATUS_PICTURE_COMPLETED) != HEVCPP_ERROR_STATUS_PICTURE_COMPLETED) {
			pr_err("Error: %s HW returns unrecoverable HEVCPP_ERROR_STATUS_PICTURE_COMPLETED error %#08x\n", __func__, err_sts);
			ret = hadespp_unrecoverable_error;
		}
	}
#endif
	return ret;
}

/**
 * Check H264 hw status
 */
static hadespp_status_t HadesppH264GetHwStatus(uint32_t err_sts, uint32_t chksyn_sts)
{
	hadespp_status_t ret = hadespp_ok;
#ifdef HEVC_HADES_CANNESWIFI
	if (err_sts & (H264PP_ERROR_STATUS_BUFFER1_OVERFLOW | H264PP_ERROR_STATUS_BUFFER2_OVERFLOW)) {
		ret = hadespp_unrecoverable_error;
		if (err_sts & H264PP_ERROR_STATUS_BUFFER1_OVERFLOW)
			pr_err("Error: %s HW returns unrecoverable error %#08x -> PP slice error & status buffer overflow\n",
			       __func__, err_sts);
		if (err_sts & H264PP_ERROR_STATUS_BUFFER2_OVERFLOW)
			pr_err("Error: %s HW returns unrecoverable error %#08x -> PP data buffer overflow\n",
			       __func__, err_sts);
	} else if (err_sts & (H264PP_ERROR_STATUS_ERROR_ON_HEADER |
	                      H264PP_ERROR_STATUS_ERROR_ON_DATA |
	                      H264PP_ERROR_STATUS_UNEXPECTED_SC)) {
		ret = hadespp_recoverable_error;
		if (err_sts & H264PP_ERROR_STATUS_ERROR_ON_HEADER) {
			pr_err("Error: %s HW returns recoverable error %#08x -> error on header\n", __func__, err_sts);
		}
		if (err_sts & H264PP_ERROR_STATUS_ERROR_ON_DATA) {
			pr_err("Error: %s HW returns recoverable error %#08x -> error on data\n", __func__, err_sts);
		}
		if (err_sts & H264PP_ERROR_STATUS_UNEXPECTED_SC) {
			pr_err("Error: %s HW returns recoverable error %#08x -> error start code detected in the middle of a slice\n", __func__,
			       err_sts);
		}
	} else if ((err_sts & (H264PP_ERROR_STATUS_BUFFER1_DONE | H264PP_ERROR_STATUS_BUFFER2_DONE |
	                       H264PP_ERROR_STATUS_END_OF_DMA)) != (H264PP_ERROR_STATUS_BUFFER1_DONE |
	                                                            H264PP_ERROR_STATUS_BUFFER2_DONE | H264PP_ERROR_STATUS_END_OF_DMA)) {
		ret = hadespp_recoverable_error;
		if ((err_sts & H264PP_ERROR_STATUS_BUFFER1_DONE) !=  H264PP_ERROR_STATUS_BUFFER1_DONE) {
			pr_err("Error: %s HW returns recoverable error %#08x -> slice error status not written\n", __func__,
			       err_sts);
		}
		if ((err_sts & H264PP_ERROR_STATUS_BUFFER2_DONE) !=  H264PP_ERROR_STATUS_BUFFER2_DONE) {
			pr_err("Error: %s HW returns recoverable error %#08x -> MB structure buffer not written\n", __func__,
			       err_sts);
		}
		if ((err_sts & H264PP_ERROR_STATUS_END_OF_DMA) !=  H264PP_ERROR_STATUS_END_OF_DMA) {
			pr_err_ratelimited("Error: %s HW returns recoverable error %#08x -> end_of_dma not reached\n", __func__,
			                   err_sts);
		}
	} else {
		ret = hadespp_ok;
	}
#else
	(void)err_sts; // warning removal
#endif
	return ret;
}

// ////////////////////////////////////////////////////////////////////////////////
/**
 * Ioctl GetPreprocessedBuffer -> waits for buffer to be pre-processed and
 * returns output parameters pointed by ParameterAddress
 */
static OSDEV_Status_t HadesppIoctlGetPreprocessedBuffer(struct HadesppOpenContext *instance,
                                                        struct hadespp_ioctl_dequeue_t *OutParam)
{
	OSDEV_Status_t Status = OSDEV_NoError;
	struct HadesppTask task;
	struct Hadespp *hadespp;
	uint32_t err_sts;
	uint32_t chksyn_sts;
	int ret = 0;

	hadespp = (struct Hadespp *)instance->data;
	BUG_ON(!hadespp);

	ret = HadesppPopTask(instance, &task);
	if (ret) {
		Status = OSDEV_Error;
	}

	if (task.OutParam.hwStatus != hadespp_ok) {
		pr_err("Error: %s - Task status NOK: %d\n", __func__, task.OutParam.hwStatus);
	}

	if (task.InParam.codec == CODEC_HEVC) {
		err_sts = task.OutParam.iStatus.hevc_iStatus.error;
		chksyn_sts = task.OutParam.iStatus.hevc_iStatus.chksyn_status;
		task.OutParam.hwStatus = HadesppHEVCGetHwStatus(&task, err_sts, chksyn_sts);
	} else if (task.InParam.codec == CODEC_H264) {
		err_sts = task.OutParam.iStatus.h264_iStatus.error;
		task.OutParam.hwStatus = HadesppH264GetHwStatus(err_sts, 0);
	} else if (task.InParam.codec == CODEC_AVSP) {
		err_sts = task.OutParam.iStatus.avsp_iStatus.error;
		chksyn_sts = task.OutParam.iStatus.avsp_iStatus.chksyn_status;
		task.OutParam.hwStatus = HadesppAVSPGetHwStatus(err_sts, chksyn_sts);
	}

	//pr_info("Hadespp: %s Returns task %d err_sts: 0x%x\n", __func__, task.id, err_sts);
	*OutParam = task.OutParam;

	return Status;
}

/**
 * Ioctl to send synchronously a task to PP
 * Blocking call until the task has been processed
 * Avoid the usage another thread, in order to limit the number of context switches
 */
static OSDEV_Status_t HadesppIoctlQueueAndWaitBuffer(struct HadesppOpenContext *instance,
                                                     struct hadespp_ioctl_synchronous_call_t *param)
{
	OSDEV_Status_t Status = OSDEV_NoError;
	struct HadesppTask *task;
	int ret = 0;

	/* In case instance has been closed meanwhile */
	task = HadesppPushTask(instance, &param->in);
	if (!task) {
		pr_err("Error: %s to push task : Fifo full\n", __func__);
		Status = OSDEV_Error;
		goto bail;
	}
	Status = HadesppTriggerTask(task);

	/* Block until one task has been completed by HW */
	ret = HadesppGetEndedTask(instance, &task);
	if (ret < 0 || !task) {
		pr_err("Error: %s HadesppGetEndedTask failed\n", __func__);
		Status = OSDEV_Error;
		goto bail;
	}

	HadesppHandleEndOfTask(instance, task);

	Status = HadesppIoctlGetPreprocessedBuffer(instance, &param->out);

bail:
	return Status;
}

/**
 * Ioctl to send a task to PP
 */
static OSDEV_Status_t HadesppIoctlQueueBuffer(struct HadesppOpenContext *instance,
                                              struct hadespp_ioctl_queue_t *inParam)
{
	OSDEV_Status_t Status = OSDEV_NoError;
	struct HadesppTask *task;

	/* In case instance has been closed meanwhile */
	task = HadesppPushTask(instance, inParam);
	if (!task) {
		pr_err("Error: %s to push task : Fifo full\n", __func__);
		Status = OSDEV_Error;
		goto bail;
	}
	Status = HadesppTriggerTask(task);

bail:
	return Status;
}

/**
 * Main ioctl function
 */
static OSDEV_IoctlEntrypoint(HadesppIoctl)
{
	OSDEV_Status_t Status = OSDEV_NoError;
	struct HadesppOpenContext *instance;
	struct hadespp_ioctl_queue_t InParam;
	struct hadespp_ioctl_dequeue_t OutParam;
	struct hadespp_ioctl_synchronous_call_t param;

	OSDEV_IoctlEntry();
	instance = (struct HadesppOpenContext *)OSDEV_PrivateData;
	BUG_ON(!instance);

	switch (OSDEV_IoctlCode) {
	case HADES_PP_IOCTL_PREPROCESS_BUFFER:
		OSDEV_CopyToDeviceSpace(&param, OSDEV_ParameterAddress,
		                        sizeof(struct hadespp_ioctl_synchronous_call_t));
		Status = HadesppIoctlQueueAndWaitBuffer(instance, &param);
		OSDEV_CopyToUserSpace(OSDEV_ParameterAddress,
		                      &param, sizeof(struct hadespp_ioctl_synchronous_call_t));
		break;

	case HADES_PP_IOCTL_QUEUE_BUFFER:
		OSDEV_CopyToDeviceSpace(&InParam, OSDEV_ParameterAddress,
		                        sizeof(struct hadespp_ioctl_queue_t));
		Status = HadesppIoctlQueueBuffer(instance, &InParam);
		break;

	case HADES_PP_IOCTL_GET_PREPROCESSED_BUFFER:
		Status = HadesppIoctlGetPreprocessedBuffer(instance, &OutParam);
		OSDEV_CopyToUserSpace(OSDEV_ParameterAddress,
		                      &OutParam, sizeof(struct hadespp_ioctl_dequeue_t));
		break;

	default:
		pr_err("Error: %s Invalid ioctl %08x\n", __func__, OSDEV_IoctlCode);
		Status = OSDEV_Error;
		break;
	}

	OSDEV_IoctlExit(Status);
}

static OSDEV_Descriptor_t HadesppDeviceDescriptor = {
	.Name = "Hades Pre-processor Module",
	.MajorNumber = MAJOR_NUMBER,
	.OpenFn  = HadesppOpen,
	.CloseFn = HadesppClose,
	.IoctlFn = HadesppIoctl,
	.MmapFn  = NULL
};

/**
 * Interrupt handler for task completion
 */
irqreturn_t HadesppInterruptHandler(int irq, void *data)
{
	struct HadesppCore *core = (struct HadesppCore *)data;
	struct HadesppTask *task = core->current_task;
	hevcpreproc_command_status_t *hevc_sts = &task->OutParam.iStatus.hevc_iStatus;
	avspluspreproc_command_status_t *avsp_sts = &task->OutParam.iStatus.avsp_iStatus;
#ifdef HEVC_HADES_CANNESWIFI
	h264preproc_command_status_t *h264_sts = &task->OutParam.iStatus.h264_iStatus;
#else
	unsigned int picture_size;
#endif

	(void)irq; // warning removal

	if (task->InParam.codec == CODEC_AVSP) {
		avsp_sts->error  = readl((volatile void *)(core->hw_base_addr + AVSP_PP_ERROR_STATUS));
		avsp_sts->chksyn_status = readl((volatile void *)(core->hw_base_addr + AVSP_PP_CHKSYN_STATUS));
		avsp_sts->slice_data_stop_offset = readl((volatile void *)(core->hw_base_addr + AVSP_PP_SLICE_DATA_STOP_OFFSET));
		avsp_sts->sesb_stop_offset = readl((volatile void *)(core->hw_base_addr + AVSP_PP_SESB_STOP_OFFSET));

		writel(0x02, (volatile void *)(core->hw_base_addr + AVSP_PP_CTRL_OBS));
		writel(0, (volatile void *)(core->hw_base_addr + AVSP_PP_CTRL_OBS));
	} else if (task->InParam.codec == CODEC_HEVC) {
		hevc_sts->error = readl((volatile void *)(core->hw_base_addr + HEVCPP_ERROR_STATUS));
#if defined (HEVC_HADES_CANNES25) || defined (HEVC_HADES_CANNESWIFI)
		hevc_sts->chksyn_status = readl((volatile void *)(core->hw_base_addr + HEVCPP_CHKSYN_STATUS));
#endif

#if !defined(HEVC_HADES_CANNESWIFI)
		// WA for bz56274 (HW bug RnDHV00060580)
		// After blowing the over_hd fuse, if a stream with 4K resolution played it will return
		// over hd voilation error as expected. However this error bit in status register is not
		// getting cleared after the error status register read. Now if a stream with resolution
		// HD or below played, status register read will still return over hd voilation bit as 1.
		// WA is to ignore this preprocessor error status bit for picture size HD or below so that
		// subsequent operation in SE are not effected.
		picture_size = readl((volatile void *)(core->hw_base_addr + HADESPP_PICTURE_SIZE));
		if ((picture_size & 0xFFFF) <= 1920) {
			// reset OVHD_VIOLATION bit in status read from PP error status register
			hevc_sts->error &= ~(HEVCPP_ERROR_STATUS_OVHD_VIOLATION);
		}
#endif

		hevc_sts->ctb_commands_stop_offset  = readl((volatile void *)(core->hw_base_addr + HEVCPP_CTB_COMMANDS_STOP_OFFSET));
		hevc_sts->ctb_residuals_stop_offset = readl((volatile void *)(core->hw_base_addr + HEVCPP_CTB_RESIDUALS_STOP_OFFSET));
		hevc_sts->slice_table_entries = readl((volatile void *)(core->hw_base_addr + HEVCPP_SLICE_TABLE_ENTRIES));
		hevc_sts->slice_table_entries = (hevc_sts->slice_table_entries >> 5) & 0x7ffffff;
		hevc_sts->slice_table_stop_offset = readl((volatile void *)(core->hw_base_addr + HEVCPP_SLICE_TABLE_STOP_OFFSET));
		hevc_sts->ctb_table_stop_offset = readl((volatile void *)(core->hw_base_addr + HEVCPP_CTB_TABLE_STOP_OFFSET));
		hevc_sts->slice_headers_stop_offset = readl((volatile void *)(core->hw_base_addr + HEVCPP_SLICE_HEADERS_STOP_OFFSET));

#ifdef CONFIG_STM_HADES_DEBUG_TOOLS
		hevc_sts->slice_table_crc   = readl((volatile void *)(core->hw_base_addr + HEVCPP_SLICE_TABLE_CRC));
		hevc_sts->ctb_table_crc     = readl((volatile void *)(core->hw_base_addr + HEVCPP_CTB_TABLE_CRC));
		hevc_sts->slice_headers_crc = readl((volatile void *)(core->hw_base_addr + HEVCPP_SLICE_HEADERS_CRC));
		hevc_sts->ctb_commands_crc  = readl((volatile void *)(core->hw_base_addr + HEVCPP_CTB_COMMANDS_CRC));
		hevc_sts->ctb_residuals_crc = readl((volatile void *)(core->hw_base_addr + HEVCPP_CTB_RESIDUALS_CRC));

		if (hadespp->crc_checker != NULL) {
			crc_set(& task->crcs, CRC_ID_RG_SLI_TAB, hevc_sts->slice_table_crc);
			crc_set(& task->crcs, CRC_ID_RG_CTB_TAB, hevc_sts->ctb_table_crc);
			crc_set(& task->crcs, CRC_ID_RG_SLI_HDR, hevc_sts->slice_headers_crc);
			crc_set(& task->crcs, CRC_ID_RG_CTB_CMD, hevc_sts->ctb_commands_crc);
			crc_set(& task->crcs, CRC_ID_RG_CTB_RES, hevc_sts->ctb_residuals_crc);
		}
#endif
		// Clear IRQ
		writel(0x2, (volatile void *)(core->hw_base_addr + HEVCPP_CTRL_OBS));
		writel(0, (volatile void *)(core->hw_base_addr + HEVCPP_CTRL_OBS));

#ifdef HEVC_HADES_CANNESWIFI
	} else if (task->InParam.codec == CODEC_H264) {
		h264_sts->error = readl((volatile void *)(core->hw_base_addr + H264_PP_ERROR_STATUS));
		h264_sts->sesb_stop_offset = readl((volatile void *)(core->hw_base_addr + H264_PP_SESB_STOP_OFFSET));
		h264_sts->residual_stop_offset = readl((volatile void *)(core->hw_base_addr + H264_PP_RESIDUALS_STOP_OFFSET));
		h264_sts->slice_data_stop_offset = readl((volatile void *)(core->hw_base_addr + H264_PP_SLICE_DATA_STOP_OFFSET));
		h264_sts->QueueIdentifier = task->InParam.iDecodeIndex;

		// Clear IRQ
		writel(0x2, (volatile void *)(core->hw_base_addr + H264_PP_CTRL_OBS));
		writel(0, (volatile void *)(core->hw_base_addr + H264_PP_CTRL_OBS));

		if ((readl((volatile void *)(core->hw_base_addr + H264_PP_CTRL_OBS)) & H264PP_CTRL_OBS_RDPLUG_PROC1_ACK) ==
		    H264PP_CTRL_OBS_RDPLUG_PROC1_ACK) {
			writel(HADESPP_RESET_RDPLUG_RESET_AUTOCLEAR | HADESPP_RESET_WRPLUG_RESET_AUTOCLEAR,
			       (volatile void *)(core->hw_base_addr + HADESPP_RESET));
		}
#endif
	}
#if defined (HEVC_HADES_CANNES25) || defined (HEVC_HADES_CANNESWIFI)
	task->OutParam.decode_time_hw = readl((volatile void *)(core->hw_base_addr + HADESPP_DECODE_TIME));
	if (core->clk_rate) {
		task->OutParam.decode_time_hw = (task->OutParam.decode_time_hw) / (core->clk_rate / 1000000);
	}
#endif
	/* New PP reset strategy: Plugs should also be reset on each picture (reset requires all HW client clocks to be enabled) */
	HadesppClockOn_Hevc(core);
	HadesppClockOn_Avsp(core);
#if defined (HEVC_HADES_CANNES25) || defined (HEVC_HADES_CANNESWIFI)
	writel((HADESPP_RESET_PP_RESET_AUTOCLEAR | HADESPP_RESET_RDPLUG_RESET_AUTOCLEAR | HADESPP_RESET_WRPLUG_RESET_AUTOCLEAR),
	       (volatile void *)(core->hw_base_addr + HADESPP_RESET));
#else
	writel(HADESPP_RESET_PP_RESET_AUTOCLEAR,
	       (volatile void *)(core->hw_base_addr + HADESPP_RESET));
#endif
	HadesppClockOff_Hevc(core);
	HadesppClockOff_Avsp(core);

	complete(&task->hw_ended);
	return IRQ_HANDLED;
}

/**
 * DT stuff
 */
static int hadespp_get_of_pdata(struct platform_device *pdev, struct Hadespp *hadespp)
{
#ifndef CONFIG_STM_VIRTUAL_PLATFORM
	struct HadesppCore *core;
	int ret;
	int i;

	for (i = 0; i < hadespp->pp_nb; i++) {
		core = &hadespp->core[i];
		core->clk_hevc_pp = devm_clk_get(&pdev->dev, "clk_hevc_pp");
		if (IS_ERR(core->clk_hevc_pp)) {
			pr_err("Error: %s Unable to get hevc pp clock\n", __func__);
			return PTR_ERR(core->clk_hevc_pp);
		}
#if defined (HEVC_HADES_CANNES25) || defined (HEVC_HADES_CANNESWIFI)
		core->clk_avsp_pp = devm_clk_get(&pdev->dev, "clk_avsp_pp");
		if (IS_ERR(core->clk_avsp_pp)) {
			pr_err("Error: %s Unable to get avsp pp clock\n", __func__);
			return PTR_ERR(core->clk_avsp_pp);
		}
#endif
	}
	ret = HadesppClockSetRate(pdev->dev.of_node);
	if (ret < 0) {
		pr_err("Error: %s Failed to set max frequency for hadespp clock\n", __func__);
		return -EINVAL;
	}
#endif  // CONFIG_STM_VIRTUAL_PLATFORM
	return 0;
}

/**
 *
 */
static int HadesppCheckChipId(struct Hadespp *hadespp)
{
	struct HadesppCore *core;
	unsigned int chip_id = 0;
	int ret = 0;

	if (!hadespp) {
		pr_info("Unable to get hw_version\n");
		return -EINVAL;
	}
	core = &hadespp->core[0];

	if (pm_runtime_get_sync(hadespp->dev) < 0) {
		pr_err("Error: %s pm_runtime_get_sync failed\n", __func__);
		return -EINVAL;
	}

	ret = HadesppClockOn_Hevc(core);
	if (ret) {
		pm_runtime_mark_last_busy(hadespp->dev);
		pm_runtime_put(hadespp->dev);
		pr_err("Error: %s HadesppClockOn failed\n", __func__);
		return -EINVAL;
	}

	//TODO for some reason HEVCPP_VERSION is unreachable on Cannes 2c2
#ifdef HEVC_HADES_CANNESWIFI
	chip_id = readl((volatile void *)(core->hw_base_addr + HADESPP_IP_VERSION));
#else
	chip_id = readl((volatile void *)(core->hw_hades_addr + HADESPP_PICTURE_SIZE));
#endif
	HadesppClockOff_Hevc(core);
	pm_runtime_mark_last_busy(hadespp->dev);
	pm_runtime_put(hadespp->dev);

	hadespp->hw_version = chip_id;
	pr_info("Hadespp version: PP 0x%x released on %d/%d/%d - patch version %d\n",
	        chip_id, ((chip_id >> 24) & 0xFF), ((chip_id >> 16) & 0xFF),
	        ((chip_id >> 8) & 0xFF), (chip_id & 0xFF));

	return ret;
}

/**
 *
 */
static int HadesppProbe(struct platform_device *pdev)
{
	OSDEV_Status_t Status = OSDEV_NoError;
	struct HadesppCore *core = NULL;
	struct resource *iomem;
	int ret = 0;
	int i = 0;

	BUG_ON(pdev == NULL);

	hadespp = devm_kzalloc(&pdev->dev, sizeof(*hadespp), GFP_KERNEL);
	if (!hadespp) {
		pr_err("Error: %s alloc failed\n", __func__);
		ret = -ENOMEM;
		goto hadespp_bail;
	}

	/**
	 * Saving pointer to main struct to be able to retrieve it in pm_runtime
	 * callbacks for example (it is *NOT* platform device data, just *driver* data)
	 */
	platform_set_drvdata(pdev, hadespp);
	hadespp->dev = &pdev->dev;

	/* Platform include IOMEM and IRQs */
	hadespp->pp_nb = pdev->num_resources / 2;
	BUG_ON(hadespp->pp_nb > HADES_PP_MAX_SUPPORTED_PRE_PROCESSORS);
	pr_info("%s hw core nb: %d\n", __func__, hadespp->pp_nb);
	hadespp->core = devm_kzalloc(&pdev->dev, hadespp->pp_nb * sizeof(*hadespp->core), GFP_KERNEL);
	if (!hadespp->core) {
		pr_err("Error: %s core alloc failed\n", __func__);
		ret = -ENOMEM;
		goto hadespp_bail;
	}

	sema_init(&hadespp->pp_unactive_lock, hadespp->pp_nb);
	spin_lock_init(&hadespp->core_lock);

	if (pdev->dev.of_node) {
		ret = hadespp_get_of_pdata(pdev, hadespp);
		if (ret) {
			pr_err("Error: %s Failed to retrieve driver data\n", __func__);
			goto hadespp_bail;
		}
	} else {
		pr_warn("warning: %s No DT entry\n", __func__);
	}

	for (i = 0; i < hadespp->pp_nb ; i++) {
		core = &hadespp->core[i];

		snprintf(core->name, sizeof(core->name), "%s_%d", HADESPP_DEVICE_NAME, i);
		core->name[sizeof(core->name) - 1] = '\0';

		mutex_init(&core->hw_lock);
		spin_lock_init(&core->clk_lock);
		core->inuse = false;
		core->need_soft_reset = false;

		iomem = platform_get_resource(pdev, IORESOURCE_MEM, i);
		if (!iomem) {
			pr_err("Error: %s Get ressource MEM[%d] failed\n", __func__, i);
			ret = -ENODEV;
			goto hadespp_bail;
		}
		core->regs_size = resource_size(iomem);
		pr_info("hadespp [%d]: base: %p, size:%d\n", i, (void *)iomem->start, core->regs_size);
		/* DT defines hades base addr as we need it to configure PP */
		core->hw_hades_addr =
		        devm_ioremap_nocache(&pdev->dev, iomem->start, core->regs_size);
		if (!core->hw_hades_addr) {
			pr_err("Error: %s ioremap registers region[%d] failed\n", __func__, i);
			ret = -ENOMEM;
			goto hadespp_bail;
		}

		/* hw_hades_addr is base addr IP (set by DT), hw_base_addr is the register bank start after plug registers */
#ifdef HEVC_HADES_CANNESWIFI
		core->hw_base_addr = core->hw_hades_addr + HADESPP_BASE_OFFSET;
#else
		core->hw_base_addr = core->hw_hades_addr + HEVCPP_REGISTER_OFFSET;
#endif
		core->irq_its = platform_get_irq(pdev, i);
		if (core->irq_its <= 0) {
			pr_err("Error: %s Get ressource IRQ[%d] failed\n", __func__, i);
			ret = -ENODEV;
			goto hadespp_bail;
		}

		ret = devm_request_irq(&pdev->dev, core->irq_its, HadesppInterruptHandler,
		                       0, core->name, (void *)core);
		if (ret) {
			pr_err("Error: %s Request IRQ 0x%x failed\n", __func__, core->irq_its);
			goto hadespp_bail;
		}
		disable_irq(core->irq_its);

#ifndef CONFIG_STM_VIRTUAL_PLATFORM
		ret = clk_prepare(core->clk_hevc_pp);
		if (ret) {
			pr_err("Error: %s Failed to prepare hevcpp clock[%d]\n", __func__, i);
			goto hadespp_bail;
		}
#if defined (HEVC_HADES_CANNES25) || defined (HEVC_HADES_CANNESWIFI)
		ret = clk_prepare(core->clk_avsp_pp);
		if (ret) {
			pr_err("Error: %s Failed to prepare avsp pp clock[%d]\n", __func__, i);
			goto hadespp_bail;
		}
#endif
#endif // CONFIG_STM_VIRTUAL_PLATFORM
		enable_irq(core->irq_its);
	}

	/* Reset control on sys_cfg registers should be done by kernel thru common api*/
	hadespp->reset_ctl = devm_reset_control_get(&pdev->dev, "sw_reset");
	if (IS_ERR(hadespp->reset_ctl)) {
		pr_debug("%s - No Reset control found\n", __func__);
		hadespp->reset_ctl = NULL;
	} else {
		ret = reset_control_deassert(hadespp->reset_ctl);
		if (ret) {
			pr_err("Error: %s Failed to bring out PP reset\n", __func__);
			goto hadespp_bail;
		}
	}

	Status = OSDEV_RegisterDevice(&HadesppDeviceDescriptor);
	if (Status != OSDEV_NoError) {
		pr_err("Error: %s Unable to get major %d\n", __func__, MAJOR_NUMBER);
		ret = -ENODEV;
		goto hadespp_bail;
	}

	Status = OSDEV_LinkDevice(HADESPP_DEVICE, MAJOR_NUMBER, 0);
	if (Status != OSDEV_NoError) {
		pr_err("Error: %s OSDEV_LinkDevice failed (major %d)\n", __func__, MAJOR_NUMBER);
		OSDEV_DeRegisterDevice(&HadesppDeviceDescriptor);
		ret = -ENODEV;
		goto hadespp_bail;
	}

	/* Clear the device's 'power.runtime_error' flag and set the device's runtime PM status to 'suspended' */
	pm_runtime_set_suspended(&pdev->dev);
	pm_suspend_ignore_children(&pdev->dev, 1);
	pm_runtime_enable(&pdev->dev);

	ret = ConfigureIBIFramework(hadespp);
	if (ret) {
		pr_err("Error: %s - ConfigureIBIFramework failed %d\n", __func__, ret);
	}

	ret = HadesppCheckChipId(hadespp);
	if (ret) {
		pr_err("Error: %s PP HW not supported (0x%x)\n", __func__, hadespp->hw_version);
		goto hadespp_bail;
	}

	OSDEV_Dump_MemCheckCounters(__func__);
	pr_info("Hadespp probe done ok\n");

	return 0;

hadespp_bail:
	if (core) {
		for (i = 0 ; i < hadespp->pp_nb ; i++) {
			if (hadespp->core[i].clk_hevc_pp) {
				clk_unprepare(core->clk_hevc_pp);
			}
#if defined (HEVC_HADES_CANNES25) || defined (HEVC_HADES_CANNESWIFI)
			if (hadespp->core[i].clk_avsp_pp) {
				clk_unprepare(core->clk_avsp_pp);
			}
#endif
		}
	}

	OSDEV_Dump_MemCheckCounters(__func__);
	pr_err("Error: %s Hadespp probe failed\n", __func__);

	return ret;
}

/**
 *
 */
static int HadesppRemove(struct platform_device *pdev)
{
	struct Hadespp     *hadespp;
	struct HadesppCore *core;
	int i = 0;

	BUG_ON(pdev == NULL);

	hadespp = dev_get_drvdata(&pdev->dev);
	BUG_ON(hadespp == NULL);

	for (i = 0 ; i < hadespp->pp_nb ; i++) {
		core = &hadespp->core[i];
		disable_irq(core->irq_its);
		clk_unprepare(core->clk_hevc_pp);
#if defined (HEVC_HADES_CANNES25) || defined (HEVC_HADES_CANNESWIFI)
		clk_unprepare(core->clk_avsp_pp);
#endif
	}

	if (hadespp->reset_ctl != NULL) {
		reset_control_assert(hadespp->reset_ctl);
	}
	pm_runtime_disable(&pdev->dev);

	OSDEV_UnLinkDevice(HADESPP_DEVICE);
	OSDEV_DeRegisterDevice(&HadesppDeviceDescriptor);
	dev_set_drvdata(&pdev->dev, NULL);

	OSDEV_Dump_MemCheckCounters(__func__);
	pr_info("Hadespp remove done\n");

	return 0;
}

// ////////////////////////////PM_RUNTIME///////////////////////////////////////////
/**
 * pm_runtime_resume callback for Active Standby
 * called if pm_runtime ref counter == 0 (and for Active Standby)
 */
static int HadesppPmRuntimeResume(struct device *dev)
{
	(void)dev; // warning removal
	return 0;
}
/**
 * pm_runtime_suspend callback for Active Standby
 * called if pm_runtime ref counter == 0 (and for Active Standby)
 */
static int HadesppPmRuntimeSuspend(struct device *dev)
{
	(void)dev; // warning removal
	return 0;
}

// ////////////////////////////////PM///////////////////////////////////////////
/**
 * PM suspend callback
 * For Gateway profile: called on Host Passive Standby entry (HPS entry)
 * For STB profile: called on Controller Passive Standby entry (CPS entry)
 */
static int HadesppPmSuspend(struct device *dev)
{
	int i;
	struct Hadespp *hadespp = dev_get_drvdata(dev);

	pr_info("%s\n", __func__);
	BUG_ON(!hadespp);
	/**
	 * Here we assume that there are no more jobs in progress.
	 * This is ensured at SE level, which has already entered "low power state",
	 * in PM notifier callback of player2 module (on PM_SUSPEND_PREPARE event)
	 */
	for (i = 0; i < hadespp->pp_nb; i++) {
		if (hadespp->core[i].inuse) {
			pr_info("%s : Core %d used (should not happen)!\n", __func__, i);
		}
	}
	return 0;
}

/**
 * PM resume callback
 * For Gateway profile: called on Host Passive Standby exit (HPS exit)
 * For STB profile: called on Controller Passive Standby exit (CPS exit)
 */
static int HadesppPmResume(struct device *dev)
{
	int ret;
	(void)dev; // warning removal
	pr_info("%s\n", __func__);
	/**
	 * Here we assume that no new jobs have been posted yet.
	 * This is ensured at SE level, which will exit "low power state" later on,
	 * in PM notifier callback of player2 module (on PM_POST_SUSPEND event).
	 */

	ret = ConfigureIBIFramework(hadespp);
	if (ret) {
		pr_err("Error: %s - ConfigureIBIFramework failed %d\n", __func__, ret);
	}

	return 0;
}

static struct dev_pm_ops HadesppPmOps = {
	.suspend = HadesppPmSuspend,
	.resume  = HadesppPmResume,
	.runtime_suspend  = HadesppPmRuntimeSuspend,
	.runtime_resume   = HadesppPmRuntimeResume,
};

static struct of_device_id stm_hadespp_match[] = {
	{
		.compatible = "st,se-hadespp",
	},
	{},
};
//MODULE_DEVICE_TABLE(of, stm_hadespp_match);

static struct platform_driver HadesppDriver = {
	.driver = {
		.name = HADESPP_DEVICE_NAME,
		.owner = THIS_MODULE,
		.of_match_table = of_match_ptr(stm_hadespp_match),
		.pm = &HadesppPmOps,
	},
	.probe = HadesppProbe,
	.remove = HadesppRemove,
};

module_platform_driver(HadesppDriver);

