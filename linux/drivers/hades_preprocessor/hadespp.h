/************************************************************************
  Copyright (C) 2003-2014 STMicroelectronics. All Rights Reserved.

  This file is part of the Streaming Engine.

  Streaming Engine is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License version 2 as
  published by the Free Software Foundation.

  Streaming Engine is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
  See the GNU General Public License for more details.

  You should have received a copy of the GNU General Public License along
  with Streaming Engine; see the file COPYING.  If not, write to the Free
  Software Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307,
  USA.

  The Streaming Engine Library may alternatively be licensed under a
  proprietary license from ST.
 ************************************************************************/

#ifndef __HADESPP_H__
#define __HADESPP_H__

#include "hevcpp.h"
#include "avspluspp.h"
#include "h264pp.h"

#define HADES_PP_MAX_SUPPORTED_PRE_PROCESSORS 2

/* 64 bytes alignement */
#define HADESPP_BUFFER_ALIGNMENT           0x3FU

#define HADESPP_DISABLE_OVHD                BIT(0)
#define HADESPP_DISABLE_HEVC                BIT(1)
#define HADESPP_DISABLE_AVS                 BIT(2)
#define HADESPP_DISABLE_H264                BIT(3)
#define HADESPP_DISABLE_VP9                 BIT(4)

#define HADESPP_DISABLE_OVHD_STATUS         BIT(16)
#define HADESPP_DISABLE_HEVC_STATUS         BIT(17)
#define HADESPP_DISABLE_AVS_STATUS          BIT(18)
#define HADESPP_DISABLE_H264_STATUS         BIT(19)
#define HADESPP_DISABLE_VP9_STATUS          BIT(20)


#ifdef HEVC_HADES_CANNESWIFI

/* Plug registers are in [0, 0x10000] */
#define HADESPP_BASE_OFFSET                (0x10000)

#define HADESPP_IP_VERSION                 (0x0)
#define HADESPP_RESET                      (0x4)
#define HADESPP_DISABLE                    (0x8)
#define HADESPP_START                      (0xC)
#define HADESPP_DECODE_TIME                (0x10)
#define HADESPP_PICTURE_SIZE               (0x14)
#define HADESPP_BIT_BUFFER_BASE_ADDR       (0x18)
#define HADESPP_BIT_BUFFER_START_OFFSET    (0x1C)
#define HADESPP_BIT_BUFFER_LENGTH          (0x20)
#define HADESPP_BIT_BUFFER_END_OFFSET      (0x24)
#define HADESPP_PP_PLUG_STAT_CFG           (0x28)

#define HADESPP_START_START_AUTOCLEAR     (1U<<0)
#define HADESPP_START_HEVC_CODEC          (0U<<1)
#define HADESPP_START_AVSP_CODEC          (1U<<1)
#define HADESPP_START_H264_CODEC          (1U<<2)

#define HADESPP_RESET_PP_RESET_AUTOCLEAR         (1U<<0)
#define HADESPP_RESET_RDPLUG_RESET_AUTOCLEAR     (1U<<1)
#define HADESPP_RESET_WRPLUG_RESET_AUTOCLEAR     (1U<<2)
#define HADESPP_RESET_RDPLUG_IVC_SRST_AUTOCLEAR  (1U<<14)
#define HADESPP_RESET_WRPLUG_IVC_SRST_AUTOCLEAR  (1U<<15)
#define HADESPP_RESET_RDPLUG_SRS_ACTIVE          (1U<<16)
#define HADESPP_RESET_RDPLUG_SRS_END             (1U<<17)
#define HADESPP_RESET_WRPLUG_SRS_ACTIVE          (1U<<18)
#define HADESPP_RESET_WRPLUG_SRS_END             (1U<<19)

#else /* !HEVC_HADES_CANNESWIFI */

#define HADESPP_PICTURE_SIZE               (0x0)

#define HADESPP_BIT_BUFFER_BASE_ADDR       (0x8C)
#define HADESPP_BIT_BUFFER_LENGTH          (0x90)
#define HADESPP_BIT_BUFFER_START_OFFSET    (0x94)
#ifdef HEVC_HADES_CANNES25
#define HADESPP_BIT_BUFFER_END_OFFSET      (0x98)
#else
#define HADESPP_BIT_BUFFER_STOP_OFFSET     (0x98)
#endif

#define HADESPP_START                      (0x100)
#define HADESPP_RESET                      (0x120)

#ifdef HEVC_HADES_CANNES25
#define HADESPP_IP_VERSION                 (0x138)
#endif

#define HADESPP_START_START_AUTOCLEAR     (1<<0)
#define HADESPP_START_HEVC_CODEC          (0<<1)
#define HADESPP_START_AVSP_CODEC          (1<<1)
#define HADESPP_START_H264_CODEC          (2<<1)
#define HADESPP_START_VP9_CODEC           (3<<1)

#define HADESPP_RESET_PP_RESET_AUTOCLEAR         (1U<<0)
#define HADESPP_RESET_RDPLUG_RESET_AUTOCLEAR     (1U<<1)
#define HADESPP_RESET_WRPLUG_RESET_AUTOCLEAR     (1U<<2)
#define HADESPP_RESET_RDPLUG_IVC_SRST_AUTOCLEAR  (1U<<14)
#define HADESPP_RESET_WRPLUG_IVC_SRST_AUTOCLEAR  (1U<<15)
#define HADESPP_RESET_RDPLUG_SRS_ACTIVE          (1U<<16)
#define HADESPP_RESET_RDPLUG_SRS_END             (1U<<17)
#define HADESPP_RESET_WRPLUG_SRS_ACTIVE          (1U<<18)
#define HADESPP_RESET_WRPLUG_SRS_END             (1U<<19)

#define HADESPP_BUFFER_CID                 (0xC)
#define HADESPP_BITSTREAM_CID              (0x3)
#define HADESPP_BUFFER_SID                 (0x35)
#define HADESPP_BITSTREAM_SID              (0x31)

#ifdef HEVC_HADES_CANNES25
#define HADESPP_PLUG_STAT_CFG             (0x13C)
#define HADESPP_PLUG_STAT0                (0x140)
#define HADESPP_PLUG_STAT1                (0x144)
#define HADESPP_PLUG_STAT2                (0x148)
#define HADESPP_PLUG_STAT3                (0x14C)
#define HADESPP_PLUG_STAT4                (0x150)
#define HADESPP_PLUG_STAT5                (0x154)
#define HADESPP_PLUG_STAT6                (0x158)
#define HADESPP_PLUG_STAT7                (0x15C)
#define HADESPP_PLUG_STAT8                (0x160)
#define HADESPP_DECODE_TIME               (0x168)
#define HADESPP_DISABLE                   (0x16C)
#endif

#endif  /* HEVC_HADES_CANNESWIFI */

#endif /* __HADESPP_H_ */

