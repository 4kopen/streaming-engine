/************************************************************************
  Copyright (C) 2003-2014 STMicroelectronics. All Rights Reserved.

  This file is part of the Streaming Engine.

  Streaming Engine is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License version 2 as
  published by the Free Software Foundation.

  Streaming Engine is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
  See the GNU General Public License for more details.

  You should have received a copy of the GNU General Public License along
  with Streaming Engine; see the file COPYING.  If not, write to the Free
  Software Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307,
  USA.

  The Streaming Engine Library may alternatively be licensed under a
  proprietary license from ST.
 ************************************************************************/

#ifndef HADESPPINLINE_H
#define HADESPPINLINE_H

// this file is meant to be included from player cpp code
// e.g. not to be included from linux native modules
#ifndef __cplusplus
#error "not included from player cpp code"
#endif

#include "osinline.h"
#include "osdev_user.h"

#include "hadesppio.h"

struct HadesppDevice_t {
	OSDEV_DeviceIdentifier_t iUnderlyingDevice;
};

static inline hadespp_status_t HadesppOpen(struct HadesppDevice_t **aDevice)
{
	OSDEV_Status_t status;

	*aDevice = (HadesppDevice_t *) OS_Malloc(sizeof(struct HadesppDevice_t));
	if (*aDevice == NULL) {
		return hadespp_error;
	}

	memset(*aDevice, 0, sizeof(struct HadesppDevice_t));

	status = OSDEV_Open(HADESPP_DEVICE, &((*aDevice)->iUnderlyingDevice));
	if (status != OSDEV_NoError) {
		SE_ERROR("Failed to open device\n");
		OS_Free(*aDevice);
		*aDevice = NULL;
		return hadespp_error;
	}

	return hadespp_ok;
}

static inline void HadesppClose(struct HadesppDevice_t *aDevice)
{
	if (aDevice != NULL) {
		OSDEV_Close(aDevice->iUnderlyingDevice);
		OS_Free(aDevice);
	}
}

static inline hadespp_status_t HadesppQueueBuffer(
        struct HadesppDevice_t *aDevice,
        struct hadespp_ioctl_queue_t *aQueParams)
{
	int r;

	if (aDevice == NULL) {
		SE_ERROR("Device not open\n");
		return hadespp_error;
	}

#ifdef HEVC_HADES_CANNES25
	if (aQueParams->codec == CODEC_H264) {
		SE_ERROR("Codec not supported\n");
		return hadespp_error;
	}
#else
#ifndef HEVC_HADES_CANNESWIFI
	if (aQueParams->codec != CODEC_HEVC) {
		SE_ERROR("Codec not supported\n");
		return hadespp_error;
	}
#endif
#endif

	r = OSDEV_Ioctl(aDevice->iUnderlyingDevice, HADES_PP_IOCTL_QUEUE_BUFFER,
	                aQueParams, sizeof(struct hadespp_ioctl_queue_t));
	if (r != OSDEV_NoError) {
		SE_ERROR("Failed to queue buffer\n");
		return hadespp_error;
	}

	return hadespp_ok;
}

static inline hadespp_status_t HadesppGetPreProcessedBuffer(
        struct HadesppDevice_t *aDevice,
        struct hadespp_ioctl_dequeue_t *aDequeParams)
{
	int r;

	if (aDevice == NULL) {
		SE_ERROR("Device not open\n");
		return hadespp_error;
	}

	r = OSDEV_Ioctl(aDevice->iUnderlyingDevice,
	                HADES_PP_IOCTL_GET_PREPROCESSED_BUFFER,
	                aDequeParams, sizeof(struct hadespp_ioctl_dequeue_t));
	if (r != OSDEV_NoError) {
		SE_ERROR("Failed to get buffer\n");
		return hadespp_error;
	}

	return hadespp_ok;
}

// Synchronous call
static inline hadespp_status_t HadesppPreProcessBuffer(
        struct HadesppDevice_t *aDevice,
        hadespp_ioctl_synchronous_call_t *param)
{
	int r;

	if (aDevice == NULL) {
		SE_ERROR("Device not open\n");
		return hadespp_error;
	}

	r = OSDEV_Ioctl(aDevice->iUnderlyingDevice,
	                HADES_PP_IOCTL_PREPROCESS_BUFFER,
	                param, sizeof(struct hadespp_ioctl_synchronous_call_t));
	if (r != OSDEV_NoError) {
		SE_ERROR("Failed to preprocess buffer\n");
		return hadespp_error;
	}

	return hadespp_ok;
}

#endif // HADESPPINLINE_H
