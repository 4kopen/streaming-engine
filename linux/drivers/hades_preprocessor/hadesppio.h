/************************************************************************
Copyright (C) 2003-2014 STMicroelectronics. All Rights Reserved.

This file is part of the Streaming Engine.

Streaming Engine is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License version 2 as
published by the Free Software Foundation.

Streaming Engine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with Streaming Engine; see the file COPYING.  If not, write to the Free
Software Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307,
USA.

The Streaming Engine Library may alternatively be licensed under a
proprietary license from ST.
************************************************************************/

#ifndef _HADESPPIO_H_
#define _HADESPPIO_H_

//hevc_video_transformer_types.h still needed for enums and circular_buffer structs
// to be removed
#include <hevc_video_transformer_types.h>
#include "hadespp.h"

#define HADESPP_DEVICE_NAME   "hadespp"

#define HADESPP_DEVICE        "/dev/"HADESPP_DEVICE_NAME

#define HADES_PP_IOCTL_QUEUE_BUFFER              1
#define HADES_PP_IOCTL_GET_PREPROCESSED_BUFFER   2
#define HADES_PP_IOCTL_PREPROCESS_BUFFER         3

typedef enum {
	CODEC_HEVC = 0,
	CODEC_AVSP,
	CODEC_H264
} codec_t;

typedef enum {
	hadespp_ok = 0,
	hadespp_error,
	hadespp_timeout,
	hadespp_unrecoverable_error,
	hadespp_recoverable_error,
	hevcpp_unrecoverable_ovhd_violation_error,
	hevcpp_unrecoverable_hevc_violation_error
} hadespp_status_t;

///////////////// HEVC ////////////////////
typedef struct {
	uint32_t error;
	uint32_t chksyn_status;

	uint32_t slice_table_stop_offset;
	uint32_t ctb_table_stop_offset;
	uint32_t slice_headers_stop_offset;
	uint32_t ctb_commands_stop_offset;
	uint32_t ctb_residuals_stop_offset;

	uint32_t bitstream_crc;
	uint32_t slice_table_crc;
	uint32_t ctb_table_crc;
	uint32_t slice_headers_crc;
	uint32_t ctb_commands_crc;
	uint32_t ctb_residuals_crc;

	uint32_t slice_table_entries;
	uint32_t total_nb_of_bins;
} hevcpreproc_command_status_t;

typedef struct {
	//! Control flags of the checks to be performed by the preprocessor
	uint32_t chksyn_dis;
	uint32_t disable_hevc;
	uint32_t disable_ovhd;

	uint32_t check;

	//! The bit buffer is a circular buffer (word size = 1 byte).
	circular_buffer_t bit_buffer;
	uint32_t rdplug_dma_1_cid;
	//! Preprocessor stops reading the bit buffer at this offset
	uint32_t  bit_buffer_stop_offset;
	//! Maximum number of slices (depends on the profile/level). Shall be >= 1
	uint32_t  max_slice;
	//! Intermediate buffer output by the preprocessor
	intermediate_buffer_t intermediate_buffer;

	uint32_t wrplug_dma_1_cid;
	uint32_t wrplug_dma_2_cid;
	uint32_t wrplug_dma_3_cid;
	uint32_t wrplug_dma_4_cid;
	uint32_t wrplug_dma_5_cid;

	// ---------------
	// SPS parameters
	uint32_t  chroma_format_idc;
	uint32_t  separate_colour_plane_flag;
	uint32_t  pic_width_in_luma_samples;
	uint32_t  pic_height_in_luma_samples;
	uint32_t bit_depth_luma_minus8;
	uint32_t bit_depth_chroma_minus8;

	uint32_t  pcm_enabled_flag;
	uint32_t  pcm_bit_depth_luma_minus1;
	uint32_t  pcm_bit_depth_chroma_minus1;
	uint32_t  log2_max_pic_order_cnt_lsb_minus4;
	uint32_t  lists_modification_present_flag;
	uint32_t  log2_min_coding_block_size_minus3;
	uint32_t  log2_diff_max_min_coding_block_size;
	uint32_t  log2_min_transform_block_size_minus2;
	uint32_t  log2_diff_max_min_transform_block_size;
	uint32_t  log2_min_pcm_coding_block_size_minus3;
	uint32_t  log2_diff_max_min_pcm_coding_block_size;
	uint32_t  max_transform_hierarchy_depth_inter;
	uint32_t  max_transform_hierarchy_depth_intra;
	uint32_t  transform_skip_enabled_flag;
	uint32_t  pps_loop_filter_across_slices_enabled_flag;
	uint32_t  amp_enabled_flag;
	uint32_t  sample_adaptive_offset_enabled_flag;
	uint32_t  num_short_term_ref_pic_sets;
	uint32_t  long_term_ref_pics_present_flag;
	uint32_t  num_long_term_ref_pics_sps;
	uint32_t  sps_temporal_mvp_enabled_flag;
	// Only the num_short_term_ref_pic_sets first elements are filled.
	// Unused values shall be set to 0
	uint32_t  num_delta_pocs[HEVC_MAX_SHORT_TERM_RPS];

	// ---------------
	// PPS parameters
	uint32_t  dependent_slice_segments_enabled_flag;
	uint32_t  sign_data_hiding_flag;
	uint32_t  cabac_init_present_flag;
	uint32_t  num_ref_idx_l0_default_active_minus1;
	uint32_t  num_ref_idx_l1_default_active_minus1;
	int32_t   init_qp_minus26;
	uint32_t  cu_qp_delta_enabled_flag;
	uint32_t  diff_cu_qp_delta_depth;
	uint32_t  weighted_pred_flag;
	uint32_t  weighted_bipred_flag;
	uint32_t  output_flag_present_flag;
	uint32_t  deblocking_filter_control_present_flag;
	uint32_t  deblocking_filter_override_enabled_flag;
	uint32_t  pps_deblocking_filter_disable_flag;
	int32_t   pps_beta_offset_div2;
	int32_t   pps_tc_offset_div2;
	uint32_t  transquant_bypass_enable_flag;
	uint32_t  tiles_enabled_flag;
	uint32_t  entropy_coding_sync_enabled_flag;
	//! Number of tiles in width inside the picture. Shall be >= 1
	uint32_t  num_tile_columns;
	//! Number of tiles in height inside the picture. Shall be >= 1
	uint32_t  num_tile_rows;
	//! Width of each tile column, in number of ctb. Unused values shall be set to 0
	uint32_t  tile_column_width[HEVC_MAX_TILE_COLUMNS];
	//! Height of each tile row, in number of ctb. Unused values shall be set to 0
	uint32_t  tile_row_height[HEVC_MAX_TILE_ROWS];
	uint32_t  slice_segment_header_extension_flag;
	uint32_t  num_extra_slice_header_bits;
	uint32_t  pps_slice_chroma_qp_offsets_present_flag;

	// ---------------
	// Slice header parameters
	//! This parameter is computed using syntax elements found in the first slice header of the picture
	//! (short_term_ref_pic_set(), short_term_ref_pic_set_idx, num_long_term_pics...)
	uint32_t  num_poc_total_curr;
} hevcpreproc_transform_param_t;

///////////////// AVS+ ////////////////////
typedef struct {
	void            *InputBufferPhysicalAddress;
	unsigned int     InputBufferSize;

	uint32_t sesb_start;
	uint32_t sesb_length;
	uint32_t slice_data_start;
	uint32_t slice_data_length;

	// AVSP-PP :: for AVS_PP_CFG1 register #########
	uint32_t avs_pp_cfg1;

	// AVSP-PP :: for AVS_PP_CFG2 register #########
	uint32_t avsp_mb_horizontal_size;
	uint32_t avsp_mb_vertical_size;
	uint32_t avsp_aec_enable;
	uint32_t avsp_skip_mode_flag;
	uint32_t avsp_chroma_format;
	uint32_t avsp_picture_structure;
	uint32_t avsp_picture_type;
	uint32_t avsp_picture_ref_flag;
	uint32_t avsp_prevPictureType;
	uint32_t avsp_outEndianness;

	// AVSP-PP :: for AVS_PP_CFG3 register #########
	uint32_t avsp_fixed_pic_qp;
	uint32_t avsp_wq_flag;
	uint32_t avsp_mb_adapt_wq_disable;
	uint32_t avsp_wq_model;
	uint32_t avsp_wq_param_idx;
	uint32_t avsp_picture_qp;
	uint32_t avsp_chroma_quant_param_disable;
	uint32_t avsp_chroma_quant_param_delta_cb;
	uint32_t avsp_chroma_quant_param_delta_cr;

	// AVSP-PP :: for AVS_PP_CFG4-5-6 register #########
	uint32_t avsp_weighting_quant_param_delta1[6];
	uint32_t avsp_weighting_quant_param_delta2[6];

	uint32_t avsp_cfg1;
	uint32_t avsp_cfg2;
	uint32_t avsp_cfg3;
	uint32_t avsp_cfg4;
	uint32_t avsp_cfg5;
	uint32_t avsp_cfg6;

	uint32_t avsp_chksyn_dis;
} avspluspreproc_transform_param_t;

typedef struct {
	unsigned int error;
	unsigned int chksyn_status;
	unsigned int slice_data_stop_offset;
	unsigned int sesb_stop_offset;
	unsigned int slice_table_entries;
	unsigned int PPSize;
} avspluspreproc_command_status_t;

typedef struct {
	circular_buffer_t bit_buf;
	uint32_t sesb_start;
	uint32_t sesb_length;
	uint32_t slice_data_start;
	uint32_t slice_data_length;
	/* circular_buffer_t res_buf; NOT USED */

	uint32_t pic_width_in_mbs_minus1;
	uint32_t pic_height_in_map_units_minus1;
	uint32_t mb_adaptive_frame_field_flag;
	uint32_t entropy_coding_mode_flag;
	uint32_t frame_mbs_only_flag;
	uint32_t pic_order_cnt_type;
	uint32_t pic_order_present_flag;
	uint32_t delta_pic_order_always_zero_flag;
	uint32_t redundant_pic_cnt_present_flag;
	uint32_t weighted_pred_flag;
	int32_t  weighted_bipred_idc;
	uint32_t deblocking_filter_control_present_flag;
	uint32_t num_ref_idx_l0_active_minus1;
	uint32_t num_ref_idx_l1_active_minus1;
	int32_t  pic_init_qp_minus26;
	uint32_t transform_8x8_mode_flag;
	uint32_t direct_8x8_inference_flag;
	uint32_t chroma_format_idc;
	uint32_t field_pic_flag;
	uint32_t log2_max_frame_num_minus4;
	uint32_t log2_max_pic_order_cnt_lsb_minus4;
	uint32_t slice_group_change_cycle_size;
	uint32_t num_slice_group_minus1;
	uint32_t slice_group_map_type;
	uint32_t fmo_aso_en;
} h264preproc_transform_param_t;

typedef struct  {
	uint32_t QueueIdentifier; //TODO rm with ppRing
	uint32_t sesb_stop_offset;
	uint32_t residual_stop_offset;
	uint32_t slice_data_stop_offset;
	uint32_t error;
} h264preproc_command_status_t;

struct hadespp_ioctl_queue_t {
	codec_t codec;
	union {
		hevcpreproc_transform_param_t hevc_iCmd;
		avspluspreproc_transform_param_t avsp_iCmd;
		h264preproc_transform_param_t    h264_iCmd;
	} iCmd;
	// Decode index of picture used to reference intermediate buffer CRCs
	unsigned int iDecodeIndex;
};

struct hadespp_ioctl_dequeue_t {
	union {
		// Task status
		hevcpreproc_command_status_t    hevc_iStatus;
		avspluspreproc_command_status_t avsp_iStatus;
		h264preproc_command_status_t    h264_iStatus;
	} iStatus;
	hadespp_status_t hwStatus;        //HW status
	uint32_t         decode_time_hw;  //HW decoding time in us
};

struct hadespp_ioctl_synchronous_call_t {
	struct hadespp_ioctl_queue_t in;
	struct hadespp_ioctl_dequeue_t out;
};
#endif
