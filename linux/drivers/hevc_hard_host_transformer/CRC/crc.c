/*!
 * \file crc.c
 *
 * \brief CRC computation
 */
#include <linux/module.h>

#include "crc.h"

char *CRCNames[CRC_ID_TOTAL] = {
	"decode index",
	"IDR",
	"POC",

	"RG_SLI_TAB"
	"RG_CTB_TAB",
	"RG_SLI_HDR",
	"RG_CTB_CMD",
	"RG_CTB_RES",
	"MM_SLI_TAB",
	"MM_CTB_TAB",
	"MM_SLI_HDR",
	"MM_CTB_CMD",
	"MM_CTB_RES",
	"MME_STATUS",

	"HWPE_HWC_CTB_RX_0",
	"HWPE_HWC_SLI_RX_0",
	"HWPE_HWC_MVP_TX_0",
	"HWPE_HWC_RED_TX_0",
	"HWPE_MVP_PPB_RX_0",
	"HWPE_MVP_PPB_TX_0",
	"HWPE_MVP_CMD_TX_0",
	"HWPE_MAC_CMD_TX_0",
	"HWPE_MAC_DAT_TX_0",
	"HWPE_XOP_CMD_TX_0",
	"HWPE_XOP_DAT_TX_0",
	"HWPE_RED_DAT_RX_0",
	"HWPE_RED_CMD_TX_0",
	"HWPE_RED_DAT_TX_0",
	"HWPE_PIP_CMD_TX_0",
	"HWPE_PIP_DAT_TX_0",
	"HWPE_IPR_CMD_TX_0",
	"HWPE_IPR_DAT_TX_0",
	"HWPE_DBK_CMD_TX_0",
	"HWPE_DBK_DAT_TX_0",
	"HWPE_SAO_CMD_TX_0",
	"HWPE_RSZ_CMD_TX_0",
	"HWPE_OS_REF_TX_0",
	"HWPE_OS_R2B_TX_0",
	"HWPE_OS_RSZ_TX_0",
	"HWPE_DMA_REF_TX_0",
	"HWPE_DMA_R2B_TX_0",
	"HWPE_DMA_RSZ_TX_0",

	"HWPE_HWC_CTB_RX_1",
	"HWPE_HWC_SLI_RX_1",
	"HWPE_HWC_MVP_TX_1",
	"HWPE_HWC_RED_TX_1",
	"HWPE_MVP_PPB_RX_1",
	"HWPE_MVP_PPB_TX_1",
	"HWPE_MVP_CMD_TX_1",
	"HWPE_MAC_CMD_TX_1",
	"HWPE_MAC_DAT_TX_1",
	"HWPE_XOP_CMD_TX_1",
	"HWPE_XOP_DAT_TX_1",
	"HWPE_RED_DAT_RX_1",
	"HWPE_RED_CMD_TX_1",
	"HWPE_RED_DAT_TX_1",
	"HWPE_PIP_CMD_TX_1",
	"HWPE_PIP_DAT_TX_1",
	"HWPE_IPR_CMD_TX_1",
	"HWPE_IPR_DAT_TX_1",
	"HWPE_DBK_CMD_TX_1",
	"HWPE_DBK_DAT_TX_1",
	"HWPE_SAO_CMD_TX_1",
	"HWPE_RSZ_CMD_TX_1",
	"HWPE_OS_REF_TX_1",
	"HWPE_OS_R2B_TX_1",
	"HWPE_OS_RSZ_TX_1",
	"HWPE_DMA_REF_TX_1",
	"HWPE_DMA_R2B_TX_1",
	"HWPE_DMA_RSZ_TX_1",

	"OMEGA_L",
	"OMEGA_C",
	"RASTER_L",
	"RASTER_C",
	"RESIZE_L",
	"RESIZE_C",
	"PPB"
};

void crc_clear(FrameCRC_t *crc)
{
	CRCId_t id;

	for (id = 0; id < CRC_ID_TOTAL; id++) {
		crc_reset(crc, id);
	}
}

void crc_reset(FrameCRC_t *crc, CRCId_t id)
{
	crc->present[id] = 0;
}

void crc_set(FrameCRC_t *crc, CRCId_t id, uint32_t value)
{
	crc->values[id] = value;
	crc->present[id] = 1;
}


#define CRC_INIT 0xffffffffu
#define CRC_POLYNOMIAL 0x04c11db7u //!< Polynomial for CRC computation

uint32_t
crc_update
(uint32_t word,
 uint8_t  bits,
 uint32_t result)
{
	uint8_t i;
	uint32_t mask;

	word <<= 32 - bits;
	for (i = 0; i < bits; i++) {
		mask = (result ^ word) >> 31;
		result <<= 1;
		word <<= 1;
		if (mask) {
			result ^= CRC_POLYNOMIAL;
		}
	}
	return result;
}

uint32_t
crc_rot
(uint8_t *base,
 uint32_t start,
 uint32_t stop,
 uint32_t end)
{
	uint32_t result = CRC_INIT;

	if (stop < start) {
		for (; start < end; start++) {
			result = crc_update(base[start], 8, result);
		}
		start = 0;
	}
	for (; start < stop; start++) {
		result = crc_update(base[start], 8, result);
	}
	return result;
}

uint32_t
crc_rot_swap
(uint8_t *base,
 uint32_t start,
 uint32_t stop,
 uint32_t end)
{
	uint32_t result = CRC_INIT;

	// Assumption: start/stop/end shall be a multiple of 8 bytes

	if (stop < start) {
		for (; start < end; start += 8) {
			result = crc_update(base[start + 4], 8, result);
			result = crc_update(base[start + 5], 8, result);
			result = crc_update(base[start + 6], 8, result);
			result = crc_update(base[start + 7], 8, result);
			result = crc_update(base[start + 0], 8, result);
			result = crc_update(base[start + 1], 8, result);
			result = crc_update(base[start + 2], 8, result);
			result = crc_update(base[start + 3], 8, result);
		}
		start = 0;
	}
	for (; start < stop; start += 8) {
		result = crc_update(base[start + 4], 8, result);
		result = crc_update(base[start + 5], 8, result);
		result = crc_update(base[start + 6], 8, result);
		result = crc_update(base[start + 7], 8, result);
		result = crc_update(base[start + 0], 8, result);
		result = crc_update(base[start + 1], 8, result);
		result = crc_update(base[start + 2], 8, result);
		result = crc_update(base[start + 3], 8, result);
	}
	return result;
}

uint32_t
crc_1d
(uint8_t *buffer,
 uint32_t size)
{
	uint32_t i, result = CRC_INIT;
	uint8_t bits = 8 * sizeof(uint8_t);

	for (i = 0; i < size; i++) {
		result = crc_update(buffer[i], bits, result);
	}

	return result;
}

uint32_t
crc_2d
(uint8_t *buffer,
 uint16_t stride,
 uint16_t h_size,
 uint16_t v_size)
{
	uint16_t i, j;
	uint32_t result = CRC_INIT;
	uint8_t bits = 8 * sizeof(uint8_t);

	for (j = 0; j < v_size; j++) {
		for (i = 0; i < h_size; i++) {
			result = crc_update(buffer[i], bits, result);
		}
		buffer += stride;
	}
	return result;
}

uint32_t
crc_3d
(uint8_t *buffer1,
 uint8_t *buffer2,
 uint16_t stride,
 uint16_t h_size,
 uint16_t v_size)
{
	uint16_t i, j;
	uint32_t result = CRC_INIT;
	uint8_t bits = 8 * sizeof(uint8_t);

	for (j = 0; j < v_size; j++) {
		for (i = 0; i < h_size; i++) {
			result = crc_update(buffer1[i], bits, result);
			result = crc_update(buffer2[i], bits, result);
		}
		buffer1 += stride;
		buffer2 += stride;
	}
	return result;
}
