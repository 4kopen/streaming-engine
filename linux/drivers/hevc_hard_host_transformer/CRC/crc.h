/************************************************************************
Copyright (C) 2013 STMicroelectronics. All Rights Reserved.

This file is part of the Streaming Engine Library.

Streaming Engine is free software; you can redistribute it and/or modify it under
the terms of the GNU General Public License version 2 as published by the
Free Software Foundation.

Streaming Engine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with Streaming Engine; see the file COPYING.  If not, write to the Free Software
Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

The Streaming Engine Library may alternatively be licensed under a proprietary
license from ST.
************************************************************************/

//
// List of CRCs
//

#ifndef H_CRC_DEF
#define H_CRC_DEF

#include <linux/types.h>

typedef enum {
	CRC_ID_HWPE_HWC_CTB_RX = 0,
	CRC_ID_HWPE_HWC_SLI_RX,
	CRC_ID_HWPE_HWC_MVP_TX,
	CRC_ID_HWPE_HWC_RED_TX,
	CRC_ID_HWPE_MVP_PPB_RX,
	CRC_ID_HWPE_MVP_PPB_TX,
	CRC_ID_HWPE_MVP_CMD_TX,
	CRC_ID_HWPE_MAC_CMD_TX,
	CRC_ID_HWPE_MAC_DAT_TX,
	CRC_ID_HWPE_XOP_CMD_TX,
	CRC_ID_HWPE_XOP_DAT_TX,
	CRC_ID_HWPE_RED_DAT_RX,
	CRC_ID_HWPE_RED_CMD_TX,
	CRC_ID_HWPE_RED_DAT_TX,
	CRC_ID_HWPE_PIP_CMD_TX,
	CRC_ID_HWPE_PIP_DAT_TX,
	CRC_ID_HWPE_IPR_CMD_TX,
	CRC_ID_HWPE_IPR_DAT_TX,
	CRC_ID_HWPE_DBK_CMD_TX,
	CRC_ID_HWPE_DBK_DAT_TX,
	CRC_ID_HWPE_SAO_CMD_TX,
	CRC_ID_HWPE_RSZ_CMD_TX,
	CRC_ID_HWPE_OS_REF_TX,
	CRC_ID_HWPE_OS_R2B_TX,
	CRC_ID_HWPE_OS_RSZ_TX,
	CRC_ID_HWPE_DMA_REF_TX,
	CRC_ID_HWPE_DMA_R2B_TX,
	CRC_ID_HWPE_DMA_RSZ_TX,
	CRC_ID_HWPE_TOTAL
} CRCHwpeId_t;

typedef enum {
	CRC_ID_PICTURE_INFO = 0,
	CRC_ID_DECODE = CRC_ID_PICTURE_INFO,
	CRC_ID_IDR,
	CRC_ID_POC,

	CRC_ID_PREPROCESSOR,
	CRC_ID_RG_SLI_TAB = CRC_ID_PREPROCESSOR,
	CRC_ID_RG_CTB_TAB,
	CRC_ID_RG_SLI_HDR,
	CRC_ID_RG_CTB_CMD,
	CRC_ID_RG_CTB_RES,
	CRC_ID_MM_SLI_TAB,
	CRC_ID_MM_CTB_TAB,
	CRC_ID_MM_SLI_HDR,
	CRC_ID_MM_CTB_CMD,
	CRC_ID_MM_CTB_RES,
	CRC_ID_MME_STATUS,

	CRC_ID_DEBUG_0,
	CRC_ID_DEBUG_1 = CRC_ID_DEBUG_0 + CRC_ID_HWPE_TOTAL,

	CRC_ID_DPB = CRC_ID_DEBUG_1 + CRC_ID_HWPE_TOTAL,
	CRC_ID_OMEGA_L = CRC_ID_DPB,
	CRC_ID_OMEGA_C,
	CRC_ID_RASTER_L,
	CRC_ID_RASTER_C,
	CRC_ID_RESIZE_L,
	CRC_ID_RESIZE_C,
	CRC_ID_PPB,

	CRC_ID_TOTAL
} CRCId_t;

typedef struct {
	// Values is there or not
	int present[CRC_ID_TOTAL];

	// Values
	uint32_t values[CRC_ID_TOTAL]; // meaningful only if "present"
} FrameCRC_t;

extern char *CRCNames[CRC_ID_TOTAL];

void crc_clear(FrameCRC_t *crc);
void crc_reset(FrameCRC_t *crc, CRCId_t id);
void crc_set(FrameCRC_t *crc, CRCId_t id, uint32_t value);

uint32_t crc_update(uint32_t word, uint8_t bits, uint32_t result);
uint32_t crc_rot(uint8_t *base, uint32_t start, uint32_t stop, uint32_t end);
uint32_t crc_rot_swap(uint8_t *base, uint32_t start, uint32_t stop, uint32_t end);
uint32_t crc_1d(uint8_t *buffer, uint32_t size);
uint32_t crc_2d(uint8_t *buffer, uint16_t stride, uint16_t h_size, uint16_t v_size);
uint32_t crc_3d(uint8_t *buffer1, uint8_t *buffer2, uint16_t stride, uint16_t h_size, uint16_t v_size);

#endif // H_CRC_DEF
