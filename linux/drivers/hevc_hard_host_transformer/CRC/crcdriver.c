/************************************************************************
Copyright (C) 2013 STMicroelectronics. All Rights Reserved.

This file is part of the Streaming Engine Library.

Streaming Engine is free software; you can redistribute it and/or modify it under
the terms of the GNU General Public License version 2 as published by the
Free Software Foundation.

Streaming Engine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with Streaming Engine; see the file COPYING.  If not, write to the Free Software
Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

The Streaming Engine Library may alternatively be licensed under a proprietary
license from ST.
************************************************************************/

//
// CRC driver
//
// Manages comms with user space & decoder/preprocessor
//
#include <linux/platform_device.h>
#include <linux/poll.h>
#include <linux/interrupt.h>
#include <linux/ptrace.h>

#include <linux/types.h>
#include <linux/module.h>
#include <linux/device.h>
#include <linux/file.h>
#include <linux/cdev.h>

#include "osdev_mem.h"

#include "checker.h"
#include "crc.h"
#include "crcdriver.h"

#define MAX_CRC_LINE 2000 // should be enough to hold any line of a CRC file (~1000 for dual-HWPE)

#define MAX_STREAMS 20 // number of minor numbers for the CRC device

//
// API between User space <=> Linux Kernel
//

static int CRCOpen(struct inode *inode, struct file *filep);
static int CRCRelease(struct inode *inode, struct file *filep);
static ssize_t CRCReadComputed(struct file *filep, char *buff, size_t count, loff_t *offp);
static ssize_t CRCWriteRefs(struct file *filep, const char *buff, size_t count, loff_t *offp);

static struct file_operations FileOps = {
owner:          THIS_MODULE,
open:           CRCOpen,
release:        CRCRelease,
read:           CRCReadComputed,
write:          CRCWriteRefs,
};

typedef struct {
	CheckerHandle_t my_checker;
	char            CRCParseLine[MAX_CRC_LINE];
	int             parseSize;
	char            CRCOutputLine[MAX_CRC_LINE];
	int 		outputSize;
} CRCInstance;

//
// Helper functions to convert between CRC binary format and CRC text format
//

static int ParseCRCLine(char *line, int max, int *is_comment, FrameCRC_t *crc);
static int FormatCRCLine(char *line, int max,            const FrameCRC_t *crc);

//
// character device internals
//
static dev_t           device;
static struct cdev     chardev;
static CheckerHandle_t checker[MAX_STREAMS];

//
// /////////////////////////////////////////
//

int CRCDriver_Init(const char *device_name)
{
	int Result;
	int minor;

	// Allocate major/minor numbers
	Result  = alloc_chrdev_region(&device, 0, MAX_STREAMS, device_name);
	if (Result < 0) {
		pr_err("Error: CRCDriver: unable to allocate device numbers\n");
		return -ENODEV;
	}
	pr_info("CRCDriver: major %d, minor %d to %d\n", MAJOR(device), MINOR(device),
	        MINOR(device) + MAX_STREAMS - 1);

	// Create device
	cdev_init(&chardev, &FileOps);
	chardev.owner = THIS_MODULE;
	kobject_set_name(& chardev.kobj, "%s%d", device_name, 0);

	// Add device
	Result = cdev_add(&chardev, device, MAX_STREAMS);
	if (Result < 0) {
		pr_err("Error: CRCDriver: unable to add device\n");
		return -ENODEV;
	}

	// Initialize checkers array
	for (minor = 0; minor < MAX_STREAMS; minor ++) {
		checker[minor] = NULL;
	}

	return 0;
}

int CRCDriver_Term(void)
{
	cdev_del(&chardev);
	unregister_chrdev_region(device, MAX_STREAMS);
	return 0;
}

int CRCDriver_GetChecker(int minor, CheckerHandle_t *handle)
{
	if (minor < 0 || minor >= MAX_STREAMS) {
		return -EINVAL;
	}
	if (checker[minor] != NULL) {
		CHK_Take(checker[minor]);
	}
	*handle = checker[minor];
	return 0;
}

int CRCDriver_ReleaseChecker(int minor)
{
	CheckerStatus_t status;

	if (minor < 0 || minor >= MAX_STREAMS) {
		return -EINVAL;
	}
	if (checker[minor] == NULL) {
		return -EIO;
	}
	status = CHK_Release(checker[minor]);
	if (status == CHECKER_STOPPED) {
		checker[minor] = NULL;
	}
	return 0;
}

int CRCOpen(struct inode *inode, struct file *filep)
{
	CheckerStatus_t status;
	CRCInstance    *instance;
	int             minor = iminor(inode);

	if (filep->f_mode & FMODE_WRITE) {
		if (checker[minor] != NULL) {
			pr_err("Error: CRC: device %d: already checking\n", minor);
			return -EBUSY;
		}
		status = CHK_Alloc(&checker[minor]);
		if (status == CHECKER_OUT_OF_MEMORY) {
			return -ENOMEM;
		}
		if (status != CHECKER_NO_ERROR) {
			return -EIO;
		}
		pr_info("CRC: now checking stream %d..\n", minor);
	} else { // read mode
		if (checker[minor] == NULL) {
			pr_err("Error: CRC: device %d: no ongoing check\n", minor);
			return -ENODEV;
		}
		if (CHK_Take(checker[minor]) != CHECKER_NO_ERROR) {
			return -EIO;
		}
	}

	filep->private_data = kmalloc(sizeof(CRCInstance), GFP_KERNEL);
	instance = (CRCInstance *) filep->private_data;
	instance->my_checker = checker[minor];
	instance->parseSize = 0;
	instance->outputSize = 0;
	return 0;
}

int CRCRelease(struct inode *inode, struct file *filep)
{
	CheckerStatus_t status;
	CRCInstance    *instance;

	if (filep->private_data == NULL) {
		return -EIO;        // should not happen
	}
	instance = (CRCInstance *)(filep->private_data);

	if (filep->f_mode & FMODE_WRITE) {
		// pr_info("CRC: shutting down check for stream %d..\n", iminor(inode));
		status = CHK_ShutdownReferences(instance->my_checker); // no more refs to come
		if (status != CHECKER_NO_ERROR) {
			return -EIO;
		}
	}
	// pr_info("CRC: releasing CRC device %d..\n", iminor(inode));
	status = CHK_Release(instance->my_checker);
	if (status == CHECKER_STOPPED) {
		checker[iminor(inode)] = NULL;
	}
	kfree(filep->private_data);
	return 0;
}

ssize_t CRCWriteRefs(struct file *filep, const char *buff, size_t count, loff_t *offp)
{
	CRCInstance *instance = (CRCInstance *) filep->private_data;
	int total_written = 0;

	while (count > 0) {
		int size;
		int remaining;
		FrameCRC_t crc;
		int parsed;
		int is_comment;

		size = MAX_CRC_LINE - instance->parseSize;
		if (size > count) {
			size = count;
		}
		remaining = copy_from_user(instance->CRCParseLine + instance->parseSize, buff + total_written, size);
		if (remaining != 0) {
			return -EFAULT;
		}
		total_written += size;
		count -= size;
		instance->parseSize += size;

		parsed = ParseCRCLine(instance->CRCParseLine, instance->parseSize, &is_comment, &crc);
		if (parsed < 0) {
			return -EIO;
		}
		if (parsed != 0) {
			if (! is_comment) {
				CheckerStatus_t status = CHK_AddRefCRC(instance->my_checker, &crc);
				if (status == CHECKER_STOPPED) {
					return -ENOSPC;
				}
				if (status != CHECKER_NO_ERROR) {
					return -EIO;
				}
			}
			memmove(instance->CRCParseLine, instance->CRCParseLine + parsed, instance->parseSize - parsed);
			instance->parseSize -= parsed;
		}
	}
	(*offp) += total_written;
	return total_written;
}

ssize_t CRCReadComputed(struct file *filep, char *buff, size_t count, loff_t *offp)
{
	CRCInstance *instance = (CRCInstance *) filep->private_data;
	CheckerStatus_t status;
	int total_read = 0;

	while (count > 0) {
		int remaining;
		int size;
		FrameCRC_t crc;
		int formatted;

		// first try to write remaining stored output
		if (instance->outputSize != 0) {
			size = instance->outputSize;
			if (size > count) {
				size = count;
			}
			remaining = copy_to_user(buff + total_read, instance->CRCOutputLine, size);
			// pr_info("CRCDriver: read: sent %d bytes to user space, remaining %d\n", size, remaining);
			if (remaining != 0) {
				return -EFAULT;
			}
			count -= size;
			total_read += size;
			memmove(instance->CRCOutputLine, instance->CRCOutputLine + size, instance->outputSize - size);
			instance->outputSize -= size;
			break; // debug: for line by line output
			// if (instance->outputSize != 0) break; // waiting for an empty buffer before writing into it
		}

		// retrieve a CRC
		// pr_info("CRCDriver: read: waiting for computed CRC..\n");
		status = CHK_GetComputedCRC(instance->my_checker, &crc);
		if (status == CHECKER_STOPPED) {
			break;
		}
		if (status != CHECKER_NO_ERROR) {
			return -EIO;
		}

		// Format the CRC
		formatted = FormatCRCLine(instance->CRCOutputLine, MAX_CRC_LINE, &crc);
		if (formatted <= 0) {
			return -EIO;
		}
		instance->outputSize += formatted;
	}
	(*offp) += total_read;
	return total_read;
}

// Parse a CRC field from reference files
// (*parsed) advances to next field (or to end of string)
static bool ParseNumber(char *line, int radix, bool is_signed, int *parsed, long *number)
{
	char *index = line + *parsed; // scans the line
	char *field;                  // first digit
	char *end_of_field;           // after last digit
	char *next_field;             // after the comma, or at terminating null char
	int ret;

	// Find beginning of field
	while (*index != 0 && (*index == ' ' || *index == '\t')) {
		++ index;
	}
	field = index;

	if (*index == ',' || *index == 0) {
		if (*index == ',') {
			++ index;
		}
		*parsed = index - line;
		return false; // empty field
	}

	// Find end of field
	while (*index != 0 && *index != ' ' && *index != '\t' && *index != ',') {
		++ index;
	}
	end_of_field = index;

	// Find next field
	while (*index != 0 && *index != ',') {
		++ index;
	}
	if (*index == ',') {
		++ index;
	}
	next_field = index;

	// Parse number
	*end_of_field = 0; // kstrtoint expects a null terminated string
	if (is_signed) {
		ret = kstrtoint(field, radix, (int *)number);
	} else {
		ret = kstrtouint(field, radix, (int *)number);
	}

	if (ret != 0) {
		*parsed = -1;
		pr_err("Error: CRCDriver: cannot read base-%d %s number from %s\n", radix, is_signed ? "signed" : "unsigned", field);
		return false;
	}

	*parsed = next_field - line;
	return true;
}

// Parse a CRC line from references file
// returns: 0   = incomplete line (no \n)
//          <0  = error
//          >0  = number of parsed bytes (including final \n)
static int ParseCRCLine(char *line, int max, int *comment, FrameCRC_t *crc)
{
	char *newline;
	int parsed;
	int not_empty;
	CRCId_t id;

	for (newline = line; newline < line + max; newline++)
		if (*newline == '\n') {
			break;
		}
	if (newline >= line + max) {
		return 0;
	}

	if (line[0] == '#') {
		*comment = 1;
		return newline - line + 1;
	}

	*newline = 0;
	parsed = 0;
	*comment = 0;

	for (id = 0; id < CRC_ID_TOTAL; id++) {
		int radix = id < CRC_ID_PREPROCESSOR ? 10 : 16;
		int is_signed = (id == CRC_ID_POC);

		not_empty = ParseNumber(line, radix, is_signed, &parsed, (long *) & (crc->values[id]));
		if (parsed < 0) {
			pr_err("Error: CRCDriver: cannot read field %s in reference CRCs\n", CRCNames[id]);
			return -1;
		}
		crc->present[id] = not_empty;
	}

	return newline - line + 1;
}

// Format a CRC into a string (for output to computed CRC file)
// returns: <=0 => error
//          >0  => number of formatted bytes (including final \n)
int FormatCRCLine(char *line, int max, const FrameCRC_t *crc)
{
	char *index = line;
	CRCId_t id;

	for (id = 0; id < CRC_ID_TOTAL; id++) {
		char *format;
		int width;

		switch (id) {
		case CRC_ID_DECODE:
			format = "%8u,";
			width = 8;
			break;
		case CRC_ID_IDR:
			format = "%3u,";
			width = 3;
			break;
		case CRC_ID_POC:
			format = "%8d,";
			width = 8;
			break;
		default:
			format = "  %08x  ,";
			width = 12;
		}
		if (crc->present[id]) {
			index += snprintf(index, max - (index - line), format, crc->values[id]);
		} else {
			index += snprintf(index, max - (index - line), "%*s,", width, "");
		}
		if (*index != 0) { /* overflow */
			return -1;
		}
	}

	index --; // remove last comma

	*index = '\n';
	return index - line + 1;
}

