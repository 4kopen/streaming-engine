/************************************************************************
Copyright (C) 2012 STMicroelectronics. All Rights Reserved.

This file is part of the Streaming Engine Library.

Streaming Engine is free software; you can redistribute it and/or modify it under
the terms of the GNU General Public License version 2 as published by the
Free Software Foundation.

Streaming Engine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with Streaming Engine; see the file COPYING.  If not, write to the Free Software
Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

The Streaming Engine Library may alternatively be licensed under a proprietary
license from ST.

Source file name : hades_api.h

Description: Hades API implementation for Linux kernel

Date        Modification                                    Name
----        ------------                                    --------
30-Jan-13   Copied from Hades model                         Jerome

************************************************************************/

#include <linux/bpa2.h>

#include "osdev_mem.h"
#include "hades_api.h"
#include "driver.h"
#include "driverEmu.h"
#include "elfLoader.h"
#include "sthormAlloc.h"
#include "hades_rab_programming.h"
#include "hevc_hwpe_debug.h"

static pedf_binary_t *PedfRT_Descr_p;
static int            RabMasterEntries;
static uintptr_t      HadesBaseAddress;

typedef struct HadesContext_s {
	HostAddress
	instance;  // PEDF ABI struct sent to initialize a new instance (pedf_instance_t). In context to keep info on the current instance.
	HostAddress fw_addr;   // pointer to store the firmware address allocated during InitTransformer
} HadesContext_t;

//
// Use sthorm to allocate memory in Hades L3
//

HadesError_t HADES_Malloc(HostAddress *ha, int size)
{
	if (! sthorm_malloc(ha, size)) {
		return HADES_NO_MEMORY;
	}
	return HADES_NO_ERROR;
}

void HADES_Free(HostAddress *ha)
{
	sthorm_free(ha);
}

// Converts a host memory address to an Hades L3 address
uint32_t HADES_HostToFabricAddress(HostAddress *ha)
{
	return sthorm_remapHostToFabric((uintptr_t) ha->physical);
}

// Converts a physical address to an Hades L3 address
uint32_t HADES_PhysicalToFabricAddress(void *physical)
{
	return sthorm_remapHostToFabric((uintptr_t) physical);
}

void HADES_Flush(HostAddress *ha)
{
	sthorm_flush(ha);
}

void HADES_Invalidate(HostAddress *ha)
{
	sthorm_invalidate(ha);
}

// Send a message to the fabric and block until the response is received
static int RpcFabric(
        pedf_commands_e cmd,
        HostAddress *msgAddr,
        p12_msgHeader_t *MsgHeader_p)
{
#ifdef CONFIG_STM_VIRTUAL_PLATFORM
	MsgHeader_p->fabricHandler = 1; // VSOC -> DEFAULT_RUNTIME_HANDLER = 1 (mandatory...)
#else
	MsgHeader_p->fabricHandler = PedfRT_Descr_p->rtInfo_p->handler;
#endif
	MsgHeader_p->requestID = cmd;

	MsgHeader_p->replyID = (uint32_t)MsgHeader_p;
	//pr_info("%s: MsgHeader_p: %p (p:%p v:%p) cmd:0x%x fabricHandler:0x%x\n",
	//__func__, MsgHeader_p, msgAddr->physical, msgAddr->virtual,
	//cmd, MsgHeader_p->fabricHandler);

#ifdef CONFIG_STM_VIRTUAL_PLATFORM
	return sthorm_sendSyncMsg((uint32_t)msgAddr->physical, MsgHeader_p);
#else
	return sthorm_sendSyncMsg(HADES_HostToFabricAddress(msgAddr), MsgHeader_p);
#endif
}

int HADES_UnLoadFirmware(fw_handle_t fw_handle)
{
	pedf_binary_t *fw_binary_p = (pedf_binary_t *)fw_handle;
#ifndef CONFIG_STM_VIRTUAL_PLATFORM
	if (fw_binary_p != NULL) {
		HADES_Free(&fw_binary_p->binary);
		HADES_Free(&fw_binary_p->name);
		HADES_Free(&fw_binary_p->errorStr);
	}
#endif

	OSDEV_Free(fw_binary_p);
	fw_binary_p = NULL; // since possible reuse after

	return 0;
}

/*******************************************************************
  HADES API implementation
********************************************************************/
HadesError_t HADES_Init(HadesInitParams_t *params)
{
	int nbEntries = 0;
	int clusterMask;
	unsigned long hadesl3_base;
	unsigned long hadesl3_size;

	// Register IRQ, map registers
	// Note : skip preprocessor registers at beginning of range
	if (sthorm_init(params->HadesBaseAddress + HADES_FABRIC_OFFSET,
	                params->HadesSize - HADES_FABRIC_OFFSET, params->InterruptNumber,
	                params->partition) != 0) {
		return HADES_ERROR;
	}
	HadesBaseAddress = params->HadesBaseAddress;

	bpa2_memory(bpa2_find_part("hades-l3"), &hadesl3_base, &hadesl3_size);
	if (hadesl3_base == 0) {
		LIB_TRACE(0, "Unable to find hades-l3 partition settings\n");
		return HADES_ERROR;
	}

	// Configure RABs
	// int clusterMask = (1 << P2012_MAX_FABRIC_CLUSTER_COUNT) | ((1 << get_nb_cluster()) - 1);
	clusterMask = (1 << P2012_MAX_FABRIC_CLUSTER_COUNT) | ((1 << 1) - 1);
	hal2_configure_rab_slave_default((uint32_t)HadesBaseAddress, clusterMask, &nbEntries);
	pr_info("hadesl3_base: 0x%lx hadesl3_size: %ld\n", hadesl3_base, hadesl3_size);
#ifdef CONFIG_STM_VIRTUAL_PLATFORM
	hal2_configure_rab_master_default((uint32_t)HadesBaseAddress, &RabMasterEntries,
	                                  hadesl3_base, hadesl3_base, hadesl3_size);
#else
	hal2_configure_rab_master_default((uint32_t)HadesBaseAddress, &RabMasterEntries,
	                                  HADES_HOST_DDR_ADDRESS, hadesl3_base, hadesl3_size);
#endif

	return HADES_NO_ERROR;
}

HadesError_t HADES_Boot(fw_info_t *fw_info)
{
#ifndef CONFIG_STM_VIRTUAL_PLATFORM
	// map hades boot address of fabric firmware to loaded firmware in DDR
	hal2_configure_rab_master_firmware((uint32_t)HadesBaseAddress, &RabMasterEntries,
	                                   STHORM_BOOT_ADDR_L3, (uint32_t)fw_info->Addr.physical, fw_info->Size);
	/* map STM registers after firmware (512K)
	 * !!!!! FW + STM reg <= reserved mem in mem map (currently 2Mb) !!!!! */
	hal2_configure_rab_master_firmware((uint32_t)HadesBaseAddress, &RabMasterEntries,
	                                   0xF0000000, (uint32_t)fw_info->Addr.physical + fw_info->Size + 4096, 0x80000); //STM reg
	// JLX: map TB driver for the debug firmware
	/*
	#define STHORM_CONF_TB_DRIVER_BASE     0x9FDDE000
	#define STHORM_CONF_TB_DRIVER_SIZE     0x00001000
	hal2_configure_rab_master_firmware((uint32_t)HadesBaseAddress, &RabMasterEntries,
			STHORM_CONF_TB_DRIVER_BASE, (uint32_t)FabricFirmware.physical + FW_image_size + 0x1280000 + 8192, STHORM_CONF_TB_DRIVER_SIZE); // TB driver
	*/
#else
	pr_info("HADES_Boot: VSOC Model: no firmware loading\n");
#endif
	// Activate both Master RABs
	hal2_activate_rab_master((uint32_t)HadesBaseAddress);

	/* boot base runtime and wait until its initialization's done */
	if (sthorm_boot() != 0) {
		return HADES_ERROR;
	}

	return HADES_NO_ERROR;
}

HadesError_t HADES_Term(fw_info_t *fw_info)
{
	sthorm_term();
	HADES_UnLoadFirmware((fw_handle_t)PedfRT_Descr_p);
	sthorm_unmap(&fw_info->Addr);
	return HADES_NO_ERROR;
}


// Post a deploy command on the command queue and wait for its completion
HadesError_t HADES_InitTransformer(HostAddress **HadesHandle, fw_info_t *fw_info)
{
	HostAddress     *ContextAddr_p;
	HadesContext_t  *Context_p;
	pedf_instance_t *instance;
	uintptr_t        rtInfo_fabric_p;
	pedf_binary_t   *fw_binary_p;
	uint32_t         err;
	int              ret = 0;

	// keep a handle on all the descriptors in local structure.
	ContextAddr_p = (HostAddress *) OSDEV_Malloc(sizeof(*ContextAddr_p));
	if (ContextAddr_p == NULL) {
		LIB_TRACE(0, "Unable to allocate context\n");
		return -1;
	}
	// actually, only the instance needs to be shared
	if (HADES_Malloc(ContextAddr_p, sizeof(*Context_p)) != HADES_NO_ERROR) {
		LIB_TRACE(0, "Unable to allocate context hades\n");
		goto err1;
	}
	Context_p = (HadesContext_t *)(ContextAddr_p->virtual);
	memset(Context_p, 0, sizeof(*Context_p));

	// Alloc in TCDM
	ret = sthorm_allocMessage(&Context_p->instance, sizeof(pedf_instance_t));
	if (ret != 1) {
		LIB_TRACE(0, "Out of resources (pedf_instance_t)\n");
		goto err2;
	}
	instance = (pedf_instance_t *)Context_p->instance.virtual;
	instance->fabricAddr = HADES_HostToFabricAddress(&Context_p->instance);

	if ((fw_binary_p = OSDEV_Malloc(sizeof(pedf_binary_t))) == NULL) {
		LIB_TRACE(0, "Unable to alloc pedf_binary\n");
		goto err3;
	}
	memset(fw_binary_p, 0, sizeof(pedf_binary_t));

	PedfRT_Descr_p = (fw_handle_t)fw_binary_p;
	PedfRT_Descr_p->clusterId = STHORM_FC_CLUSTERID;
	PedfRT_Descr_p->binary.size = fw_info->Size;

	// Then build the runtime info structure to pass as parameter
	LIB_TRACE(0, "Configuring static binary\n");
	ret = sthorm_allocMessage(&Context_p->fw_addr, sizeof(pedf_runtime_info_t));
	if (ret != 1) {
		LIB_TRACE(0, "Out of resources (fw_p pedf_rt_info)\n");
		goto err4;
	}

	PedfRT_Descr_p->rtInfo_p = (pedf_runtime_info_t *) Context_p->fw_addr.virtual;
	LIB_TRACE(0, "%s: PedfRT_Descr_p(%p) ->binary.size: %d clusterId: 0x%x\n", __func__,
	          PedfRT_Descr_p, PedfRT_Descr_p->binary.size, (uint32_t)PedfRT_Descr_p->clusterId);

	rtInfo_fabric_p = HADES_PhysicalToFabricAddress(Context_p->fw_addr.physical);
	if (PedfRT_Descr_p->rtInfo_p == NULL || rtInfo_fabric_p == 0) {
		LIB_TRACE(0, "Out of resources\n");
		goto err5;
	}

	HADES_Flush(&Context_p->instance);
	LIB_TRACE(0, "%s -> pedf_rt (p:0x%x v:0x%x size:%d)\n", __func__,
	          (uint32_t)Context_p->fw_addr.physical, (uint32_t)Context_p->fw_addr.virtual, Context_p->fw_addr.size);
	// Finally call the main entry point of the pedf-runtime
	err = sthorm_callMain(PedfRT_Descr_p, (void *)rtInfo_fabric_p);
	if (err != 0) {
		LIB_TRACE(0, "%s: sthorm_callMain failed (%d)\n", __func__, err);
		goto err6;
	}
	HADES_Invalidate(&Context_p->fw_addr);

	// Send message to FC, and block until its response
	RpcFabric(PEDF_CMD_NEW_INSTANCE, &Context_p->instance, &(instance->hdr));
	HADES_Invalidate(ContextAddr_p);

	*HadesHandle = ContextAddr_p;
	if (instance->retval != 0) {
		LIB_TRACE(0, "Something wrong in new pedf instance creation: %d\n", instance->retval);
		goto err6;
	}
	return HADES_NO_ERROR;

err6:
	HADES_UnLoadFirmware((fw_handle_t)PedfRT_Descr_p);
err5:
	sthorm_freeMessage(&Context_p->fw_addr);
err4:
	OSDEV_Free(fw_binary_p);
err3:
	sthorm_freeMessage(&Context_p->instance);
err2:
	HADES_Free(ContextAddr_p);
err1:
	OSDEV_Free(ContextAddr_p);
	return -1;
}

// Post a undeploy command on the command queue and wait for its completion
HadesError_t HADES_TermTransformer(HostAddress *HadesHandle)
{
	HadesError_t error = HADES_NO_ERROR;
	HadesContext_t  *Context_p;
	pedf_instance_t *instance;

	if (HadesHandle == NULL) {
		pr_err("Error: %s hades handle is null\n", __func__);
		return -EINVAL;
	}
	Context_p = (HadesContext_t *)(HadesHandle->virtual);
	instance  = (pedf_instance_t *)(Context_p->instance.virtual);

	RpcFabric(PEDF_CMD_DESTROY, &Context_p->instance, &(instance->hdr));

	HADES_Invalidate(&Context_p->instance);
	HADES_Invalidate(HadesHandle);
	error = (instance->retval == 0) ? HADES_NO_ERROR : HADES_ERROR;

	sthorm_freeMessage(&Context_p->fw_addr);
	sthorm_freeMessage(&Context_p->instance);
	HADES_Free(HadesHandle);
	OSDEV_Free(HadesHandle);

	return error;
}


void HADES_GetHWPE_Debug_Status(void)
{
	uint32_t cl0_hwpe_addr;
	HadesDebugStatus_t debug;

#ifdef CONFIG_STM_VIRTUAL_PLATFORM
	return;
#endif

	cl0_hwpe_addr = HadesBaseAddress + HADES_CL0_HWPE_DEBUG_OFFSET;

	debug.status_red_0 = sthorm_read32(cl0_hwpe_addr + HEVC_HWPE_DEBUG_STATUS_RED_0_OFFSET);
	debug.status_red_1 = sthorm_read32(cl0_hwpe_addr + HEVC_HWPE_DEBUG_STATUS_RED_1_OFFSET);
	debug.status_pipe_0 = sthorm_read32(cl0_hwpe_addr + HEVC_HWPE_DEBUG_STATUS_PIPE_0_OFFSET);
	debug.status_pipe_1 = sthorm_read32(cl0_hwpe_addr + HEVC_HWPE_DEBUG_STATUS_PIPE_1_OFFSET);
	debug.status_ipred = sthorm_read32(cl0_hwpe_addr + HEVC_HWPE_DEBUG_STATUS_IPRED_OFFSET);
	debug.status_mvpred = sthorm_read32(cl0_hwpe_addr + HEVC_HWPE_DEBUG_STATUS_MVPRED_OFFSET);
	debug.status_hwcfg = sthorm_read32(cl0_hwpe_addr + HEVC_HWPE_DEBUG_STATUS_HWCFG_OFFSET);
	debug.status_xpredmac_cmd_0 = sthorm_read32(cl0_hwpe_addr + HEVC_HWPE_DEBUG_STATUS_XPREDMAC_CMD_0_OFFSET);
	debug.status_xpredmac_cmd_1 = sthorm_read32(cl0_hwpe_addr + HEVC_HWPE_DEBUG_STATUS_XPREDMAC_CMD_1_OFFSET);
	debug.status_xpredop_0 = sthorm_read32(cl0_hwpe_addr + HEVC_HWPE_DEBUG_STATUS_XPREDOP_0_OFFSET);
	debug.status_xpredop_1 = sthorm_read32(cl0_hwpe_addr + HEVC_HWPE_DEBUG_STATUS_XPREDOP_1_OFFSET);
	debug.status_dbk = sthorm_read32(cl0_hwpe_addr + HEVC_HWPE_DEBUG_STATUS_DBK_OFFSET);
	debug.status_resize = sthorm_read32(cl0_hwpe_addr + HEVC_HWPE_DEBUG_STATUS_RESIZE_OFFSET);
	debug.status_general_0_hwpe0 = sthorm_read32(cl0_hwpe_addr + HEVC_HWPE_DEBUG_STATUS_GENERAL_0_HWPE0_OFFSET);
	debug.status_general_1_hwpe0 = sthorm_read32(cl0_hwpe_addr + HEVC_HWPE_DEBUG_STATUS_GENERAL_1_HWPE0_OFFSET);
	debug.status_general_0_hwpe1 = sthorm_read32(cl0_hwpe_addr + HEVC_HWPE_DEBUG_STATUS_GENERAL_0_HWPE1_OFFSET);
	debug.mcc_hit = sthorm_read32(cl0_hwpe_addr + HEVC_HWPE_DEBUG_MCC_HIT_OFFSET);
	debug.mcc_out_ld16 = sthorm_read32(cl0_hwpe_addr + HEVC_HWPE_DEBUG_MCC_OUT_LD16_OFFSET);
	debug.mcc_out_ld32 = sthorm_read32(cl0_hwpe_addr + HEVC_HWPE_DEBUG_MCC_OUT_LD32_OFFSET);
	debug.mcc_out_ld64 = sthorm_read32(cl0_hwpe_addr + HEVC_HWPE_DEBUG_MCC_OUT_LD64_OFFSET);
	debug.mcc_in_ld16 = sthorm_read32(cl0_hwpe_addr + HEVC_HWPE_DEBUG_MCC_IN_LD16_OFFSET);
	debug.mcc_in_ld32 = sthorm_read32(cl0_hwpe_addr + HEVC_HWPE_DEBUG_MCC_IN_LD32_OFFSET);
	debug.mcc_in_ld64 = sthorm_read32(cl0_hwpe_addr + HEVC_HWPE_DEBUG_MCC_IN_LD64_OFFSET);

	pr_info("%-25s 0x%08x\n", "status_red_0", debug.status_red_0);
	pr_info("%-25s 0x%08x\n", "status_red_1", debug.status_red_1);
	pr_info("%-25s 0x%08x\n", "status_pipe_0", debug.status_pipe_0);
	pr_info("%-25s 0x%08x\n", "status_pipe_1", debug.status_pipe_1);
	pr_info("%-25s 0x%08x\n", "status_ipred", debug.status_ipred);
	pr_info("%-25s 0x%08x\n", "status_mvpred", debug.status_mvpred);
	pr_info("%-25s 0x%08x\n", "status_hwcfg", debug.status_hwcfg);
	pr_info("%-25s 0x%08x\n", "status_xpredmac_cmd_0", debug.status_xpredmac_cmd_0);
	pr_info("%-25s 0x%08x\n", "status_xpredmac_cmd_1", debug.status_xpredmac_cmd_1);
	pr_info("%-25s 0x%08x\n", "status_xpredop_0", debug.status_xpredop_0);
	pr_info("%-25s 0x%08x\n", "status_xpredop_1", debug.status_xpredop_1);
	pr_info("%-25s 0x%08x\n", "status_dbk", debug.status_dbk);
	pr_info("%-25s 0x%08x\n", "status_resize", debug.status_resize);
	pr_info("%-25s 0x%08x\n", "status_general_0_hwpe0", debug.status_general_0_hwpe0);
	pr_info("%-25s 0x%08x\n", "status_general_1_hwpe0", debug.status_general_1_hwpe0);
	pr_info("%-25s 0x%08x\n", "status_general_0_hwpe1", debug.status_general_0_hwpe1);
	pr_info("%-25s 0x%08x\n", "mcc_hit", debug.mcc_hit);
	pr_info("%-25s 0x%08x\n", "mcc_out_ld16", debug.mcc_out_ld16);
	pr_info("%-25s 0x%08x\n", "mcc_out_ld32", debug.mcc_out_ld32);
	pr_info("%-25s 0x%08x\n", "mcc_out_ld64", debug.mcc_out_ld64);
	pr_info("%-25s 0x%08x\n", "mcc_in_ld16", debug.mcc_in_ld16);
	pr_info("%-25s 0x%08x\n", "mcc_in_ld32", debug.mcc_in_ld32);
	pr_info("%-25s 0x%08x\n", "mcc_in_ld64", debug.mcc_in_ld64);
}

// Post an exec command on the command queue
HadesError_t HADES_ProcessCommand(HostAddress *HadesHandle, HostAddress *pedfRunAddr, uintptr_t FrameParams_fabric_p)
{
#ifndef CONFIG_STM_VIRTUAL_PLATFORM
	HadesContext_t  *Context_p = (HadesContext_t *)(HadesHandle->virtual);
	pedf_instance_t *instance = (pedf_instance_t *)(Context_p->instance.virtual);
#endif
	pedf_run_t *run = (pedf_run_t *)pedfRunAddr->virtual;
	int ret;

#ifdef CONFIG_STM_VIRTUAL_PLATFORM
	run->fabricAddr = (uint32_t)pedfRunAddr->physical;
	run->instanceFabric = 0;
#else
	run->fabricAddr = HADES_HostToFabricAddress(pedfRunAddr);
	run->instanceFabric = (uint32_t) instance->fabricAddr;
#endif

	// The PEDF-runtime gets the attributes address directly
	run->attributes = (uint32_t)FrameParams_fabric_p;
	run->fabricDone = 0;
	run->hostDone = 0; // unused
	HADES_Flush(pedfRunAddr);
	//pr_info("%s: run:0x%x run->fabricAddr: 0x%x run->attributes:0x%x\n",
	//__func__, (uint32_t)run, run->fabricAddr, run->attributes);
	ret = RpcFabric(PEDF_CMD_EXEC, pedfRunAddr, &run->hdr);
	if (ret == -1) {
		HADES_GetHWPE_Debug_Status();
		pr_err("%s: RpcFabric returned %d\n", __func__, ret);
	}

	HADES_Invalidate(pedfRunAddr);
	if (! run->fabricDone) {
		pr_err("Error: HADES_ProcessCommand: fabric state machine error\n");
	}

	return HADES_NO_ERROR;
}

