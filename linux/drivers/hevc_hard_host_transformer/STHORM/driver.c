/*
 * STHORM Host driver
 *
 * Copyright (C) 2012 STMicroelectronics
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Authors: Bruno Lavigueur (bruno.lavigueur@st.com)
 *          Germain Haugou (germain.haugou@st.com)
 *
 */

#include "driverEmuHce.h"
#include "driver.h"
#include "driverEmu.h"
#include "sthormAlloc.h"

#include "p2012_fc_periph_mailbox_hal.h"
#include "p2012_pr_control_ipgen_hal.h"

// ----------------------------------------------------------------------------
// Sending commands to the FC
// ----------------------------------------------------------------------------

/*
 * Send a message to the FC in order to execute one of the base-runtime methods
 */
static inline
int callBaseRuntime(p12_runtime_itf_e functionID, HostAddress *msgAddr, p12_msgHeader_t *msg)
{
	msg->fabricHandler = 0; // Use the default base runtime handler
	msg->requestID = functionID;
	msg->replyID = (uint32_t) msg; // FIXME : using the address of the message will only work on 32bit host
	msg->sender = P12_TARGET_HOST;
	msg->receiver = P12_TARGET_FC;
	//msg->replyCallback is set by sthorm_sendSyncMsg()

	// Synchronous remote call : wait for the answer
	sthorm_flush(msgAddr);

	return sthorm_sendSyncMsg(sthorm_remapHostToFabric((uintptr_t)msgAddr->physical), msg);
}

// ========================================================================
//   sthorm_callMain
// ========================================================================
int sthorm_callMain(pedf_binary_t *binDesc, void *arg)
{
	HostAddress msgAddr;
	p12_callMainMsg_t *msg;
	int32_t result;

	if (binDesc == NULL) {
		pr_err("Error: %s pedf_binary NULL (should not happen)\n", __func__);
		return -1;
	}
	// Use msg in FC-TCDM
	result = sthorm_allocMessage(&msgAddr, sizeof(*msg));
	if (result != 1) {
		pr_err("Error: %s Unable to allocate p12_callMainMsg_t\n", __func__);
		return -ENOMEM;
	}
	msg = (p12_callMainMsg_t *)(msgAddr.virtual);

	msg->binDesc = binDesc->xp70BinDesc;
	msg->arg = (uint32_t) arg;

	LIB_TRACE(2, "Calling main entry point of binary 0x%x with arg %p\n", binDesc->xp70BinDesc, arg);
	if (callBaseRuntime(P12_CALLMAIN, &msgAddr, &(msg->hdr))) {
		pr_err("Error: %s STHORM - Failed calling main entry point of binary 0x%x\n", __func__, binDesc->xp70BinDesc);
		result = -3;
		goto bail;
	}

	sthorm_invalidate(&msgAddr);
	result = msg->result;

bail:
	sthorm_freeMessage(&msgAddr);
	LIB_TRACE(3, "Done calling main entry point of binary 0x%x\n", binDesc->xp70BinDesc);
	return result;
}

// ----------------------------------------------------------------------------
// Remapping functions
// ----------------------------------------------------------------------------

typedef struct {
	uint32_t  fabricAddr;   // Physical address seen by the STxP70 in the fabric
	uint32_t  size;         // Size of the memory region in bytes
	uintptr_t hostAddr;     // Address used by the host, may be virtual
} rab_map_t;

// Remapping table to move between fabric and host addresses
static rab_map_t remap_table[STHORM_REMAP_MAX_ENTRIES];

// Number of entries in the remapping table
static uint32_t remap_entries = 0;

void sthorm_clearRemapTable()
{
	int i;
	remap_entries = 0;
	for (i = 0; i < STHORM_REMAP_MAX_ENTRIES; i++) {
		remap_table[i].fabricAddr = 0;
		remap_table[i].size = 0;
		remap_table[i].hostAddr = 0;
	}
}

int sthorm_addRemapEntry(uintptr_t hostAddr, uint32_t fabricAddr, uint32_t size)
{
	if (remap_entries >= STHORM_REMAP_MAX_ENTRIES) {
		return -1;
	}
	LIB_TRACE(2, "New remap entry #%d [Host:0%p Fab:%p Size:%p]\n",
	          remap_entries, (void *)hostAddr, (void *)fabricAddr, (void *)size);
	remap_table[remap_entries].fabricAddr = fabricAddr;
	remap_table[remap_entries].hostAddr = hostAddr;
	remap_table[remap_entries].size = size;

	// pr_info("---- STHORM: REMAP:%d: fabric %p, host %p, %d\n", remap_entries, (void*)fabricAddr, (void*)hostAddr, size);
	remap_entries++;
	return 0;
}

// TODO: Eventualy we could do a binary search if the remap_table is ordered.
//       For a small table with the most frequent ranges at the begining
//       a linear search is good enough.
uintptr_t sthorm_remapFabricToHost(uint32_t addr)
{
	uint32_t i;
	uint32_t remapped = addr;

	for (i = 0; i < remap_entries; i++) {
		if ((addr >= remap_table[i].fabricAddr) &&
		    (addr < remap_table[i].fabricAddr + remap_table[i].size)) {
			remapped = addr - remap_table[i].fabricAddr + remap_table[i].hostAddr;
			return remapped;
		}
	}
	pr_err("Error: %s - STHORM: unable to remap fabric address %p to physical\n", __func__, (void *)addr);
	return 0;
}

uint32_t sthorm_remapHostToFabric(uintptr_t addr)
{
	uint32_t i;
	uint32_t remapped = addr;
	for (i = 0; i < remap_entries; i++) {
		if ((addr >= remap_table[i].hostAddr) &&
		    (addr < remap_table[i].hostAddr + remap_table[i].size)) {
			remapped = addr - remap_table[i].hostAddr + remap_table[i].fabricAddr;
			return remapped;
		}
	}
	pr_err("Error: %s - STHORM: unable to remap physical address %p to fabric address\n", __func__, (void *)addr);
	return 0;
}
