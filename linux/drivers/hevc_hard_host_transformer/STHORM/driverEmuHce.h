/*
 * Host driver emulation layer API - HCE specific part
 *
 * Copyright (C) 2012 STMicroelectronics
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Authors: Bruno Lavigueur (bruno.lavigueur@st.com)
 *
 */

#ifndef _HOST_HCE_DRIVER_EMU_h_
#define _HOST_HCE_DRIVER_EMU_h_

#include "driverEmu.h"

// STHORM register access
void sthorm_write32(uint32_t physicalAddr, uint32_t value);
uint32_t sthorm_read32(uint32_t physicalAddr);
void sthorm_write8(uint32_t physicalAddr, uint8_t value);
void sthorm_write_block(uint32_t physicalAddr, uint8_t *value, unsigned int size);
void sthorm_set_block(uint32_t physicalAddr, uint8_t value, unsigned int size);

#define tlm_write32 sthorm_write32
#define tlm_read32  sthorm_read32
#define tlm_write8  sthorm_write8
#define p2012_write sthorm_write32
#define p2012_read  sthorm_read32


#endif /* _HOST_HCE_DRIVER_EMU_h_ */
