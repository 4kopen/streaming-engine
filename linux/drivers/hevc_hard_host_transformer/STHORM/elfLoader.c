/*
 * ELF loader API
 *
 * Copyright (C) 2012 STMicroelectronics
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Authors: Matthieu Leclercq (STMicroelectronics)
 *
 */

#include <linux/elf.h>
#include "elfLoader.h"
#include "driverEmu.h"
#include "rt_host_def.h"
#include "hades_memory_map.h"
#include "p2012_config_mem.h"

// ----------------------------------------------------------------------------
// Implementation of the ELFLoader interface
// ----------------------------------------------------------------------------

int host_validate(char *file)
{
	int i;
	Elf64_Ehdr *elfHeader = (Elf64_Ehdr *) file;
	Elf64_Phdr *elfProgHeaders;
	//pr_info("ELF", "Validate elf file\n");

	/*
	if (elfHeader->e_machine != EM_ST200) {
	  pr_err("Invalid target architecture\n");
	  return -1;
	}
	*/

	// TODO check core architecture and ABI

	// Checks that first loadable segment is at vaddr=0
	elfProgHeaders = (Elf64_Phdr *) &file[elfHeader->e_phoff];
	for (i = 0; i < elfHeader->e_phnum; i++) {
		Elf64_Phdr *elfProgHeader = &elfProgHeaders[i];
		if (elfProgHeader->p_type != PT_LOAD) {
			continue;
		}
		/*
		if (elfProgHeader->p_vaddr != 0) {
		  pr_err("First loadable segment is not at vaddr=0\n");
		  return -2;
		}
		*/
		break;
	}

	//pr_info("ELF", "Elf file validated\n");
	return 0;
}

void elfCopyText(uint32_t filesz, uint32_t memsz, HostAddress *block, uint32_t offset, void *source)
{
	uint32_t dest = (uint32_t)block->virtual + offset;
	//pr_info ("%s memcpy dest: 0x%x(0x%x + 0x%x), source: 0x%x size:0x%x", __func__,
	//dest, (uint32_t)block->virtual, (uint32_t)offset, (uint32_t)source, filesz);

	memcpy((void *)dest, source, filesz);

	// if necessary, add padding zero
	if (filesz < memsz) {
		memset(block->virtual + offset + filesz, 0, memsz - filesz);
	}
}

void elfCopyData(uint32_t filesz, uint32_t memsz, Elf64_Addr dest, void *source)
{
	//pr_info ("%s memcpy dest: 0x%x, source: 0x%x size:0x%x", __func__,
	//(uint32_t)dest, (uint32_t)source, filesz);

	sthorm_memcpy((void *)(uint32_t)dest, source, filesz);
	if (filesz < memsz) {
		sthorm_memset((void *)(uint32_t)(dest + filesz), 0, memsz - filesz);
	}
}

void host_loadExecSegments(char *file, fw_info_t *fw_info)
{
	int i;
	Elf64_Ehdr  *elfHeader = (Elf64_Ehdr *) file;
	Elf64_Phdr  *elfProgHeaders = (Elf64_Phdr *) &file[elfHeader->e_phoff];
	HostAddress *block = &fw_info->Addr;

	if (!sthorm_map(block, fw_info->DestAddr, fw_info->Size)) {
		pr_err("Error: STHORM: %s unable to remap physical %p to virtual (size: %d)\n",
		       __func__, fw_info->DestAddr, fw_info->Size);
		return;
	}

	// transfer the text and data segments
	for (i = 0; i < elfHeader->e_phnum; i++) {
		Elf64_Phdr *segment = &elfProgHeaders[i];
		// If segment stay in memory
		if (segment->p_type != PT_LOAD) {
			continue;
		}

		if (segment->p_filesz == 0) {
			continue;
		}

		//pr_info("%s segment->p_paddr: 0x%llx >=? 0x%x (sthorm_boot_addr_l3: 0x%x) memsz:%llu filesz: %llu\n",
		//__func__, (segment->p_paddr), HADES_L3_BASE, STHORM_BOOT_ADDR_L3, segment->p_memsz, segment->p_filesz);

		// copy data from file
		if (segment->p_paddr >= (unsigned long long)HADES_L3_BASE) {
			if ((uint32_t)block->virtual + ((uint32_t)segment->p_paddr -
			                                STHORM_BOOT_ADDR_L3) + (uint32_t)segment->p_filesz >
			    (uint32_t)block->virtual + fw_info->Size) {
				pr_err("%s Unable to copy fw text segment (potential buffer overflow 0x%x > 0x%x)\n", __func__,
				       (uint32_t)block->virtual + ((uint32_t)segment->p_paddr - STHORM_BOOT_ADDR_L3) + (uint32_t)segment->p_filesz,
				       (uint32_t)block->virtual + fw_info->Size);
				continue;
			}

			elfCopyText((uint32_t)segment->p_filesz, (uint32_t)segment->p_memsz, block,
			            (uint32_t)segment->p_paddr - STHORM_BOOT_ADDR_L3, &file[segment->p_offset]);
		} else {
			if ((uint32_t)segment->p_paddr + (uint32_t)segment->p_filesz > (uint32_t)block->virtual + fw_info->Size) {
				pr_err("%s Unable to copy fw data segment (potential buffer overflow: 0x%x > 0x%x)\n", __func__,
				       (uint32_t)segment->p_paddr + (uint32_t)segment->p_filesz,
				       (uint32_t)block->virtual + fw_info->Size);
				continue;
			}

			elfCopyData((uint32_t)segment->p_filesz, (uint32_t)segment->p_memsz,
			            segment->p_paddr, &file[segment->p_offset]);
		}
	}

	// Flush text block
	sthorm_flush(block);
}

void host_loadELF(unsigned char *file, fw_info_t *fw_info)
{
	HostAddress *block = &fw_info->Addr;
	// Reference in the mapped file
	Elf64_Ehdr *header;
	//Elf64_Shdr* sections;

	// Base load address

	// Verify the ELF type of the file
	if (file[EI_MAG0] != ELFMAG0 ||
	    file[EI_MAG1] != ELFMAG1 ||
	    file[EI_MAG2] != ELFMAG2 ||
	    file[EI_MAG3] != ELFMAG3 ||
	    file[EI_CLASS] != ELFCLASS64) {
		pr_err("Error: This is not a ELF 64bit file\n");
		return;
	}

	// TODO checks the endianness !

	header = (Elf64_Ehdr *)file;
	switch (header->e_type) {
	case ET_NONE :
		pr_info("STHORM: UNKNOWN Elf Type file\n");
		pr_err("Error: STHORM: Type not yet handled\n");
		return;
	case ET_REL :
		pr_info("STHORM: Relocatable Elf Type file\n");
		pr_err("Error: STHORM: Type not yet handled\n");
		return;
	case ET_EXEC :
		//pr_info("Executable Elf Type file\n");
		// Validate ELF file by lowLoader
		if (host_validate((char *)file)) {
			pr_err("Error: STHORM: firmware ELF file is not suitable\n");
			return;
		}
		//sections = (Elf64_Shdr*)&file[header->e_shoff];
		// Load segments in memory
		// pr_info("[loader.loadELF]Load segments in memory\n");
		host_loadExecSegments((char *)file, fw_info);
		break;
	case ET_DYN :
		pr_info("STHORM: Dynamic Elf Type file\n");
		pr_err("Error: STHORM: Type not yet handled\n");
		return;
	case ET_CORE :
		pr_info("STHORM: Core Elf Type file\n");
		pr_err("Error: STHORM: Type not yet handled\n");
		return;
	default:
		pr_err("Error: STHORM: UNKNOWN Elf Type file\n");
		pr_err("Error: STHORM: Type not yet handled\n");
		return;
	}

	if (block != NULL) {
		pr_info("STHORM: firmware program segments loaded at %p (%d bytes)\n", block->physical, block->size);
	}
}

