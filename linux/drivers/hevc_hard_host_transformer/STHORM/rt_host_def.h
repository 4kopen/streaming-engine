/*
 * Runtime constant definitions used by the host
 *
 * Copyright (C) 2012 STMicroelectronics
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Authors: Bruno Lavigueur (bruno.lavigueur@st.com)
 *
 */

#ifndef __RT_HOST_DEF_H__
#define __RT_HOST_DEF_H__

#define STHORM_BOOT_ADDR_L3 0x9DDDD000U
#define STHORM_BOOT_ADDR_L2 0x58000000U

#define STHORM_FC_PEID 0x1F
#define STHORM_FC_CLUSTERID 0x1F

#endif /* __RT_HOST_DEF_H__ */
