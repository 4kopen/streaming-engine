/************************************************************************
Copyright (C) 2003-2014 STMicroelectronics. All Rights Reserved.

This file is part of the Streaming Engine.

Streaming Engine is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License version 2 as
published by the Free Software Foundation.

Streaming Engine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with Streaming Engine; see the file COPYING.  If not, write to the Free
Software Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307,
USA.

The Streaming Engine Library may alternatively be licensed under a
proprietary license from ST.
************************************************************************/

#ifndef __HADES_H__
#define __HADES_H__

#include <hevc_video_transformer_types.h>
#include <mme.h>

#include "hades_api.h"
#include "sthormAlloc.h"
#include "hades_cmd_types.h"

#define CRC_DEVICE_NAME "hades_crc"

struct debugInfo;

struct HevcDeviceContext_t {
	HostAddress       *hadesHandle;     // to communicate with the HADES hardware
	struct device     *iDev;
	HadesInitParams_t  hades_params;
	fw_info_t          fw_info;          // Used in full static FW loading mode
	struct clk        *clk_hwpe;
	struct clk        *clk_fc;
	struct clk        *clk_tx_icn;
	uint32_t           clk_tx_rate;      // ICN clk rate in Hz
	struct mutex       hw_lock;          // mutex to lock command processing
	atomic_t           instance_nb;
};

struct HevcTransformerContext_t {
	unsigned int                    instanceId;
	HostAddress                     pedfRunAddr;     // pedf cmd send to hades
	HostAddress                     hadesAttrAddr;   // used for cache operations
	HostAddress                     hadesCmdAndStatusAddr;  // transform host address
	hades_cmd_t                    *hades_cmd;       // copy of the current command, virtual address
	hades_status_t                 *hades_status;    //  status returned by Hades, virtual address
	void                           *ioctl_param;     // Avoid multiple alloc for ioctl
	struct debugInfo               *dbg;             // Debug structs/functions
};

struct debugInfo {
	void *checkerHandle;   // CRC checker instance
	void *computedCrc;
	void (*init)(struct HevcTransformerContext_t *ctx);
	void (*term)(struct HevcTransformerContext_t *ctx);

	void (*doMemTest)(MME_Command_t *cmd, HostAddress *payload, HostAddress *commandAndStatusAddr);
	void (*getDebugInfo)(struct HevcDeviceContext_t *dev, struct HevcTransformerContext_t *ctx, MME_Command_t *cmd);
	void (*processDebugInfo)(struct HevcTransformerContext_t *ctx, MME_Command_t *cmd);
};

int get_module_param_softcrc(void);
int get_module_param_outputdecodingtime(void);
int get_module_param_memtest(void);

#endif // __HADES_H__
