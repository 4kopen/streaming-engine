/************************************************************************
Copyright (C) 2003-2014 STMicroelectronics. All Rights Reserved.

This file is part of the Streaming Engine.

Streaming Engine is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License version 2 as
published by the Free Software Foundation.

Streaming Engine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with Streaming Engine; see the file COPYING.  If not, write to the Free
Software Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307,
USA.

The Streaming Engine Library may alternatively be licensed under a
proprietary license from ST.
************************************************************************/

/************************************************************************
MME Host transformer mapping to Hades hardware
Hades is the HEVC decoder

Restrictions: This driver supports only one hades device
 ************************************************************************/

#include <linux/slab.h>
#include <linux/types.h>

#include "hevc_hard_host_transformer.h"
#include "hades.h"
#include "hades_api.h"
#include "st_relayfs_se.h"

#include "driverEmuHce.h"
#include "hades_memory_map.h"
#include "hevc_hwpe_debug.h"
#include "checker.h"
#include "crcdriver.h"
#include "picture_dumper.h"
#include "memtest.h"

extern unsigned int decode_time;

static void initDebug(struct HevcTransformerContext_t *ctx)
{
	if (CRCDriver_Init(CRC_DEVICE_NAME) != 0) {
		pr_err("Error: %s - CRCDriver_Init failed\n", __func__);
		// FIXME: keep going?
	}

	ctx->dbg->computedCrc = kmalloc(sizeof(FrameCRC_t), GFP_KERNEL);
	if (ctx->dbg->computedCrc == NULL) {
		pr_err("Error: %s computedCrc allocation failure\n", __func__);
		return;
	}
	// the checker must be open before the stream starts to play
	CRCDriver_GetChecker(ctx->instanceId, &ctx->dbg->checkerHandle);
	pr_info("%s: CRC check started with handle %p\n", __func__, (void *)(ctx->dbg->checkerHandle));
}

static void termDebug(struct HevcTransformerContext_t *ctx)
{
	if (ctx->dbg->checkerHandle != NULL) {
		CRCDriver_ReleaseChecker(ctx->instanceId);
		CHK_ShutdownComputed(ctx->dbg->checkerHandle);
	}
	kfree(ctx->dbg->computedCrc);
	ctx->dbg->computedCrc = NULL;
	CRCDriver_Term();
}

void HADES_Print_HWPE(FrameCRC_t *crc, CRCId_t base)
{
	pr_info("%-25s 0x%08x\n", "HWC_CTB_RX", crc->values[base + CRC_ID_HWPE_HWC_CTB_RX]);
	pr_info("%-25s 0x%08x\n", "HWC_SLI_RX", crc->values[base + CRC_ID_HWPE_HWC_SLI_RX]);
	pr_info("%-25s 0x%08x\n", "HWC_MVP_TX", crc->values[base + CRC_ID_HWPE_HWC_MVP_TX]);
	pr_info("%-25s 0x%08x\n", "HWC_RED_TX", crc->values[base + CRC_ID_HWPE_HWC_RED_TX]);
	pr_info("%-25s 0x%08x\n", "MVP_PPB_RX", crc->values[base + CRC_ID_HWPE_MVP_PPB_RX]);
	pr_info("%-25s 0x%08x\n", "MVP_PPB_TX", crc->values[base + CRC_ID_HWPE_MVP_PPB_TX]);
	pr_info("%-25s 0x%08x\n", "MVP_CMD_TX", crc->values[base + CRC_ID_HWPE_MVP_CMD_TX]);
	pr_info("%-25s 0x%08x\n", "MAC_CMD_TX", crc->values[base + CRC_ID_HWPE_MAC_CMD_TX]);
	pr_info("%-25s 0x%08x\n", "MAC_DAT_TX", crc->values[base + CRC_ID_HWPE_MAC_DAT_TX]);
	pr_info("%-25s 0x%08x\n", "XOP_CMD_TX", crc->values[base + CRC_ID_HWPE_XOP_CMD_TX]);
	pr_info("%-25s 0x%08x\n", "XOP_DAT_TX", crc->values[base + CRC_ID_HWPE_XOP_DAT_TX]);
	pr_info("%-25s 0x%08x\n", "RED_DAT_RX", crc->values[base + CRC_ID_HWPE_RED_DAT_RX]);
	pr_info("%-25s 0x%08x\n", "RED_CMD_TX", crc->values[base + CRC_ID_HWPE_RED_CMD_TX]);
	pr_info("%-25s 0x%08x\n", "RED_DAT_TX", crc->values[base + CRC_ID_HWPE_RED_DAT_TX]);
	pr_info("%-25s 0x%08x\n", "PIP_CMD_TX", crc->values[base + CRC_ID_HWPE_PIP_CMD_TX]);
	pr_info("%-25s 0x%08x\n", "PIP_DAT_TX", crc->values[base + CRC_ID_HWPE_PIP_DAT_TX]);
	pr_info("%-25s 0x%08x\n", "IPR_CMD_TX", crc->values[base + CRC_ID_HWPE_IPR_CMD_TX]);
	pr_info("%-25s 0x%08x\n", "IPR_DAT_TX", crc->values[base + CRC_ID_HWPE_IPR_DAT_TX]);
	pr_info("%-25s 0x%08x\n", "DBK_CMD_TX", crc->values[base + CRC_ID_HWPE_DBK_CMD_TX]);
	pr_info("%-25s 0x%08x\n", "DBK_DAT_TX", crc->values[base + CRC_ID_HWPE_DBK_DAT_TX]);
	pr_info("%-25s 0x%08x\n", "SAO_CMD_TX", crc->values[base + CRC_ID_HWPE_SAO_CMD_TX]);
	pr_info("%-25s 0x%08x\n", "RSZ_CMD_TX", crc->values[base + CRC_ID_HWPE_RSZ_CMD_TX]);
	pr_info("%-25s 0x%08x\n", "OS_REF_TX", crc->values[base + CRC_ID_HWPE_OS_REF_TX]);
	pr_info("%-25s 0x%08x\n", "OS_R2B_TX", crc->values[base + CRC_ID_HWPE_OS_R2B_TX]);
	pr_info("%-25s 0x%08x\n", "OS_RSZ_TX", crc->values[base + CRC_ID_HWPE_OS_RSZ_TX]);
}

static void HADES_Get_HWPE_crcs(struct HevcDeviceContext_t *dev, int hwpe, CRCId_t baseId, FrameCRC_t *computed_crc)
{
	uint32_t cl0_hwpe_addr = dev->hades_params.HadesBaseAddress +
	                         HADES_CL0_HWPE_DEBUG_OFFSET + hwpe * HADES_HWPE_CFG_SIZE;

#ifdef CONFIG_STM_VIRTUAL_PLATFORM
	// JLX: cluster address space not declared in cannes_ic_wrapper_MAP_cannes_core.TOP.ROUTER_tac.map
	//   => CRC registers not modeled
	return;
#endif

	// get HWPE regs
	crc_set(computed_crc, baseId + CRC_ID_HWPE_MAC_CMD_TX,
	        sthorm_read32(cl0_hwpe_addr + HEVC_HWPE_DEBUG_CRC_CTBCMD_OFFSET));
	crc_set(computed_crc, baseId + CRC_ID_HWPE_HWC_CTB_RX,
	        sthorm_read32(cl0_hwpe_addr + HEVC_HWPE_DEBUG_CRC_SLICMD_OFFSET));
	crc_set(computed_crc, baseId + CRC_ID_HWPE_HWC_MVP_TX,
	        sthorm_read32(cl0_hwpe_addr + HEVC_HWPE_DEBUG_CRC_HWCFG_MVPRED_OFFSET));
	crc_set(computed_crc, baseId + CRC_ID_HWPE_HWC_RED_TX,
	        sthorm_read32(cl0_hwpe_addr + HEVC_HWPE_DEBUG_CRC_HWCFG_RED_OFFSET));
	crc_set(computed_crc, baseId + CRC_ID_HWPE_MVP_PPB_RX,
	        sthorm_read32(cl0_hwpe_addr + HEVC_HWPE_DEBUG_CRC_PPBIN_OFFSET));
	crc_set(computed_crc, baseId + CRC_ID_HWPE_MVP_PPB_TX,
	        sthorm_read32(cl0_hwpe_addr + HEVC_HWPE_DEBUG_CRC_PPBOUT_OFFSET));
	crc_set(computed_crc, baseId + CRC_ID_HWPE_MVP_CMD_TX,
	        sthorm_read32(cl0_hwpe_addr + HEVC_HWPE_DEBUG_CRC_MVPRED_XPREDMAC_OFFSET));
	crc_set(computed_crc, baseId + CRC_ID_HWPE_MAC_CMD_TX,
	        sthorm_read32(cl0_hwpe_addr + HEVC_HWPE_DEBUG_CRC_XPREDMAC_XPREDOP_CMD_OFFSET));
	crc_set(computed_crc, baseId + CRC_ID_HWPE_MAC_DAT_TX,
	        sthorm_read32(cl0_hwpe_addr + HEVC_HWPE_DEBUG_CRC_XPREDMAC_XPREDOP_DATA_OFFSET));
	crc_set(computed_crc, baseId + CRC_ID_HWPE_XOP_CMD_TX,
	        sthorm_read32(cl0_hwpe_addr + HEVC_HWPE_DEBUG_CRC_XPREDOP_IPRED_CMD_OFFSET));
	crc_set(computed_crc, baseId + CRC_ID_HWPE_XOP_DAT_TX,
	        sthorm_read32(cl0_hwpe_addr + HEVC_HWPE_DEBUG_CRC_XPREDOP_IPRED_DATA_OFFSET));
	crc_set(computed_crc, baseId + CRC_ID_HWPE_RED_DAT_RX,
	        sthorm_read32(cl0_hwpe_addr + HEVC_HWPE_DEBUG_CRC_RESDATA_OFFSET));
	crc_set(computed_crc, baseId + CRC_ID_HWPE_RED_CMD_TX,
	        sthorm_read32(cl0_hwpe_addr + HEVC_HWPE_DEBUG_CRC_RED_PIPE_CMD_OFFSET));
	crc_set(computed_crc, baseId + CRC_ID_HWPE_RED_DAT_TX,
	        sthorm_read32(cl0_hwpe_addr + HEVC_HWPE_DEBUG_CRC_RED_PIPE_DATA_OFFSET));
	crc_set(computed_crc, baseId + CRC_ID_HWPE_PIP_CMD_TX,
	        sthorm_read32(cl0_hwpe_addr + HEVC_HWPE_DEBUG_CRC_PIPE_IPRED_CMD_OFFSET));
	crc_set(computed_crc, baseId + CRC_ID_HWPE_PIP_DAT_TX,
	        sthorm_read32(cl0_hwpe_addr + HEVC_HWPE_DEBUG_CRC_PIPE_IPRED_DATA_OFFSET));
	crc_set(computed_crc, baseId + CRC_ID_HWPE_IPR_CMD_TX,
	        sthorm_read32(cl0_hwpe_addr + HEVC_HWPE_DEBUG_CRC_IPRED_OUT_CMD_OFFSET));
	crc_set(computed_crc, baseId + CRC_ID_HWPE_IPR_DAT_TX,
	        sthorm_read32(cl0_hwpe_addr + HEVC_HWPE_DEBUG_CRC_IPRED_OUT_DATA_OFFSET));
	crc_set(computed_crc, baseId + CRC_ID_HWPE_DBK_CMD_TX,
	        sthorm_read32(cl0_hwpe_addr + HEVC_HWPE_DEBUG_CRC_DEB_SAO_CMD_OFFSET));
	crc_set(computed_crc, baseId + CRC_ID_HWPE_DBK_DAT_TX,
	        sthorm_read32(cl0_hwpe_addr + HEVC_HWPE_DEBUG_CRC_DEB_SAO_DATA_OFFSET));
	crc_set(computed_crc, baseId + CRC_ID_HWPE_SAO_CMD_TX,
	        sthorm_read32(cl0_hwpe_addr + HEVC_HWPE_DEBUG_CRC_SAO_OUT_OFFSET));
	crc_set(computed_crc, baseId + CRC_ID_HWPE_RSZ_CMD_TX,
	        sthorm_read32(cl0_hwpe_addr + HEVC_HWPE_DEBUG_CRC_RESIZE_OUT_OFFSET));
	crc_set(computed_crc, baseId + CRC_ID_HWPE_OS_REF_TX,
	        sthorm_read32(cl0_hwpe_addr + HEVC_HWPE_DEBUG_CRC_OUT_REF_OFFSET));
	crc_set(computed_crc, baseId + CRC_ID_HWPE_OS_R2B_TX,
	        sthorm_read32(cl0_hwpe_addr + HEVC_HWPE_DEBUG_CRC_OUT_DISP_OFFSET));
	crc_set(computed_crc, baseId + CRC_ID_HWPE_OS_RSZ_TX,
	        sthorm_read32(cl0_hwpe_addr + HEVC_HWPE_DEBUG_CRC_OUT_DECIM_OFFSET));
	if (0) {
		HADES_Print_HWPE(computed_crc, baseId);
	}
}

void HADES_Get_Crcs(struct HevcDeviceContext_t *dev, FrameCRC_t *computed_crc)
{
	HADES_Get_HWPE_crcs(dev, 0, CRC_ID_DEBUG_0, computed_crc);
#ifdef HEVC_HADES_CANNES25
	HADES_Get_HWPE_crcs(dev, 1, CRC_ID_DEBUG_1, computed_crc);
#endif
}

static void processCRC(MME_Command_t *cmd, FrameCRC_t *ComputedCrc, void *checkerHandle)
{
	HevcCodecExtraCRCInfo_t *info = (HevcCodecExtraCRCInfo_t *)(cmd->DataBuffers_p[0]->ScatterPages_p[0].Page_p);
	FrameCRC_t *computed_crc = ComputedCrc;

	crc_set(computed_crc, CRC_ID_DECODE, info->DecodeIndex);
	crc_set(computed_crc, CRC_ID_IDR,    info->IDR);
	crc_set(computed_crc, CRC_ID_POC,    info->poc);

	if (get_module_param_softcrc()) {
		if (info->omega_luma != NULL && info->omega_luma_size != 0) {
			crc_set(computed_crc, CRC_ID_OMEGA_L,
			        crc_1d(info->omega_luma, info->omega_luma_size));
			crc_set(computed_crc, CRC_ID_PPB, crc_1d(info->ppb, info->ppb_size));
		}
		if (info->omega_chroma != NULL && info->omega_chroma_size != 0) {
			crc_set(computed_crc, CRC_ID_OMEGA_C,
			        crc_1d(info->omega_chroma, info->omega_chroma_size));
		}
		crc_set(computed_crc, CRC_ID_RASTER_L, crc_2d(info->raster_luma,
		                                              info->raster_stride, info->raster_width,
		                                              info->raster_height));
		crc_set(computed_crc, CRC_ID_RASTER_C, crc_2d(info->raster_chroma,
		                                              info->raster_stride, info->raster_width,
		                                              info->raster_height / 2));
		if (info->raster_decimated_luma != NULL && info->raster_decimated_chroma != NULL) {
			crc_set(computed_crc, CRC_ID_RESIZE_L,
			        crc_2d(info->raster_decimated_luma, info->raster_decimated_stride,
			               info->raster_decimated_width, info->raster_decimated_height));
			crc_set(computed_crc, CRC_ID_RESIZE_C,
			        crc_2d(info->raster_decimated_chroma, info->raster_decimated_stride,
			               info->raster_decimated_width, info->raster_decimated_height / 2));
		}
	}

	CHK_CheckDecoderCRC((CheckerHandle_t)checkerHandle, computed_crc);
}

#define OUTPUT_DIR "/mnt/host/tmp/kern_log/hades_vsoc"
static void dumpPicture(MME_Command_t *cmd)
{
	HevcCodecExtraCRCInfo_t *info = (HevcCodecExtraCRCInfo_t *)(cmd->DataBuffers_p[0]->ScatterPages_p[0].Page_p);
	hevcdecpix_transform_param_t *transform_param = (hevcdecpix_transform_param_t *) cmd->Param_p;

	if (info->omega_luma != NULL && info->omega_chroma != NULL) {
		O4_dump(OUTPUT_DIR, info->DecodeIndex,
		        info->omega_luma, info->omega_chroma, info->omega_luma_size,
		        transform_param->pic_width_in_luma_samples,
		        transform_param->pic_height_in_luma_samples);
	} else {
		pr_err("Unable to dump picture (omega_luma == NULL)\n");
	}

	R2B_dump(OUTPUT_DIR, 0, info->DecodeIndex, info->bits_per_pixel,
	         info->raster_luma, info->raster_chroma, info->raster_width,
	         info->raster_height, info->raster_stride);

	if (info->raster_decimated_luma != NULL && info->raster_decimated_chroma != NULL) {
		R2B_dump(OUTPUT_DIR, 1, info->DecodeIndex, info->bits_per_pixel,
		         info->raster_decimated_luma, info->raster_decimated_chroma,
		         info->raster_decimated_width, info->raster_decimated_height,
		         info->raster_decimated_stride);
	}
}

static void doMemTest(MME_Command_t *cmd, HostAddress *payload, HostAddress *commandAndStatusAddr)
{
	static unsigned int skip = 0;

	hevcdecpix_transform_param_t *transform = (hevcdecpix_transform_param_t *) cmd->Param_p;
	HevcCodecExtraCRCInfo_t *info = (HevcCodecExtraCRCInfo_t *)(cmd->DataBuffers_p[0]->ScatterPages_p[0].Page_p);

	MT_AddressRange_t ranges[MT_MAX_RANGES];
	int range = 0;
	int ref;

	uint32_t ppb_size = info->ppb_size;
	uint32_t omega_luma_size   = info->omega_luma_size;
	uint32_t raster_luma_size  = info->raster_stride * info->raster_height;
	uint32_t decimated_luma_size  = info->raster_decimated_stride * info->raster_decimated_height;

	MT_OpenMonitoring("CTBE", info->DecodeIndex);

#define ADD(NAME, START, LENGTH) \
	{ \
		ranges[range].name = NAME; \
		ranges[range].start = START; \
		ranges[range].length = LENGTH; \
		++range; \
	}

	/////////////////////////////////////////////////
	//                  HWPE                       //
	/////////////////////////////////////////////////

	// Intermediate buffer
	range = 0;
	ADD("Slice Table", transform->intermediate_buffer.slice_table_base_addr,
	    transform->intermediate_buffer.slice_table_length);
	ADD("CTB table", transform->intermediate_buffer.ctb_table_base_addr,
	    transform->intermediate_buffer.ctb_table_length);
	ADD("Slice headers", transform->intermediate_buffer.slice_headers_base_addr,
	    transform->intermediate_buffer.slice_headers_length);
	ADD("CTB commands", transform->intermediate_buffer.ctb_commands.base_addr,
	    transform->intermediate_buffer.ctb_commands.length);
	ADD("CTB residuals", transform->intermediate_buffer.ctb_residuals.base_addr,
	    transform->intermediate_buffer.ctb_residuals.length);

	// Reconstruction + PPB out
	if (transform->curr_picture_buffer.enable_flag) {
		ADD("O4 luma",  transform->curr_picture_buffer.samples.luma_offset, omega_luma_size);
		ADD("O4 chroma", transform->curr_picture_buffer.samples.chroma_offset, omega_luma_size / 2);
		if (transform->curr_picture_buffer.ppb_offset != 0 && ppb_size != 0) {
			ADD("PPB out", transform->curr_picture_buffer.ppb_offset, ppb_size);
		}
	}

	if (transform->curr_display_buffer.enable_flag) {
		ADD("R2B luma", transform->curr_display_buffer.samples.luma_offset, raster_luma_size);
		ADD("R2B chroma", transform->curr_display_buffer.samples.chroma_offset, raster_luma_size / 2);
	}

	if (transform->curr_resize_buffer.enable_flag) {
		ADD("R2Bdec luma", transform->curr_resize_buffer.samples.luma_offset, decimated_luma_size);
		ADD("R2Bdec chroma", transform->curr_resize_buffer.samples.chroma_offset, decimated_luma_size / 2);
	}

	// MME command & status
	ADD("MME", (uint32_t)(commandAndStatusAddr->physical), commandAndStatusAddr->size);

	// PPB read
	for (ref = 0; ref < transform->num_reference_pictures; ref++) {
		ADD("PPB in", transform->ref_picture_buffer[ref].ppb_offset, ppb_size);
	}

	MT_MonitorBuffers(MT_SOURCE_DATA_HWPE, MT_SOURCE_MONO_MASK, range,
	                  ranges); // All HEVC buffers (read IB, read/write PPB, write RCN)

	/////////////////////////////////////////////////
	//                 FIRMWARE                    //
	/////////////////////////////////////////////////

	range = 0;
	ADD("Hades L3", 0x41000000, 0x0E600000);   // BPA2 partition
	ADD("base+reloc", 0x78000000, 0x04000000); // reloc zone + base RT
	MT_MonitorBuffers(MT_SOURCE_CODE_ID, MT_SOURCE_CODE_MASK, range, ranges); // Firmware code sections

	range = 0;
	ADD("RUN payload", (uint32_t)(payload->physical), payload->size);
	MT_MonitorBuffers(MT_SOURCE_DATA_FIRMWARE, MT_SOURCE_MONO_MASK, 1, ranges); // Firmware data

	/////////////////////////////////////////////////
	//               XPRED - MCC                   //
	/////////////////////////////////////////////////

	for (ref = 0; ref < transform->num_reference_pictures; ref++) {
		if (ref * 2 + 1 >= MT_MAX_RANGES) {
			pr_info("Hades: Memtest: skipping remaining refs\n");
			break;
		}
		ranges[ref * 2].name = "ref luma";
		ranges[ref * 2].start =  transform->ref_picture_buffer[ref].samples.luma_offset;
		ranges[ref * 2].length = omega_luma_size;
		ranges[ref * 2 + 1].name = "ref chroma";
		ranges[ref * 2 + 1].start =  transform->ref_picture_buffer[ref].samples.chroma_offset;
		ranges[ref * 2 + 1].length = omega_luma_size / 2;
	}
	MT_MonitorBuffers(MT_SOURCE_XPRED, MT_SOURCE_MONO_MASK, ref * 2, ranges);

	if (get_module_param_memtest() >= 0) {
		MT_StartMonitoring(get_module_param_memtest());
	} else {
		MT_StartMonitoring(skip++);
	}
}

static void stopMemTest(void)
{
	MT_StopMonitoring();
}

/**
 * Clocks MUST be enabled
 */
void GetDebugInfo(struct HevcDeviceContext_t *dev, struct HevcTransformerContext_t *ctx, MME_Command_t *cmd)
{
	FrameCRC_t *ComputedCrc = (FrameCRC_t *)ctx->dbg->computedCrc;
	crc_clear(ComputedCrc);
	HADES_Get_Crcs(dev, ComputedCrc); // hardware registers
#if 0
	{
		HADES_GetHWPE_Debug(ComputedCrc);
	}
#endif
	if (ctx->dbg->checkerHandle != NULL) {
		processCRC(cmd, ComputedCrc,  ctx->dbg->checkerHandle);
	}
}

/**
 * Clock can be disabled for processing this debugging info
 */
void ProcessDebugInfo(struct HevcTransformerContext_t *ctx, MME_Command_t *cmd)
{
	HevcCodecExtraCRCInfo_t *info = (HevcCodecExtraCRCInfo_t *)(cmd->DataBuffers_p[0]->ScatterPages_p[0].Page_p);

	if (info == NULL) {
		pr_err("Error: %s info == NULL\n", __func__);
		return;
	}
	if (get_module_param_outputdecodingtime()) {
		st_relayfs_print_se(ST_RELAY_TYPE_HEVC_HW_DECODING_TIME, ST_RELAY_SOURCE_SE,
		                    "decoder,%d,%d,%d,%d\n",
		                    0,
		                    info->DecodeIndex,
		                    decode_time,
		                    ((hevcdecpix_status_t *)(cmd->CmdStatus.AdditionalInfo_p))->error_code);
		pr_info("decoding_time,decoder,%d,%d,%d,%d\n",
		        0,
		        info->DecodeIndex,
		        decode_time,
		        ((hevcdecpix_status_t *)(cmd->CmdStatus.AdditionalInfo_p))->error_code);
	}
	if (get_module_param_memtest() >= -1) {
		stopMemTest();
	}

	// For debug; dump some or all pictures
	if (0) {
		if (info->DecodeIndex <= 10) {
			dumpPicture(cmd);
		}
	}
}


struct debugInfo dbgInfo = {
	.init = &initDebug,
	.term = &termDebug,
	.doMemTest = &doMemTest,
	.getDebugInfo = &GetDebugInfo,
	.processDebugInfo = &ProcessDebugInfo,
};

