/************************************************************************
Copyright (C) 2003-2014 STMicroelectronics. All Rights Reserved.

This file is part of the Streaming Engine.

Streaming Engine is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License version 2 as
published by the Free Software Foundation.

Streaming Engine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with Streaming Engine; see the file COPYING.  If not, write to the Free
Software Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307,
USA.

The Streaming Engine Library may alternatively be licensed under a
proprietary license from ST.
************************************************************************/

/************************************************************************
Linux driver for Hades hardware
Hades is the HEVC decoder

Restrictions: This driver supports only one hades device
************************************************************************/

#include <linux/module.h>
#include <linux/device.h>
#include <linux/fs.h>
#include <linux/platform_device.h>
#include <linux/init.h>    /* Initialisation support */
#include <linux/kernel.h>  /* Kernel support */
#include <linux/err.h>
#include <linux/suspend.h>
#include <linux/pm_runtime.h>
#include <linux/pm.h>
#include <linux/firmware.h> /*for request_firmware() */
#include <linux/of.h>
#include <linux/clk.h>

#include "osdev_mem.h"
#include "osdev_device.h"

#include "hades.h"
#include "hadesio.h"
// upstream interface
#include <hevc_video_transformer_types.h>
// downstream interface
#include "pedf_host_abi.h"

#include "driverEmuHce.h"
#include "hades_memory_map.h"
#include "hevc_hwpe_debug.h"
#include "elfLoader.h"

#define MAJOR_NUMBER 248
#define MODULE_NAME "Hades platform driver"

MODULE_DESCRIPTION("Hades HEVC decode hardware cell platform driver");
MODULE_AUTHOR("STMicroelectronics");
MODULE_LICENSE("GPL");

static char *partition  = "hades-l3"; // must match a BPA2 partition in bootargs in SoC memory map or tlm_run_* (VSOC)
module_param(partition, charp, S_IRUGO);
MODULE_PARM_DESC(partition, "BPA2 partition for shared memory with Hades device");

static unsigned int softcrc = 0;
module_param(softcrc, uint, S_IRUGO | S_IWUSR);
MODULE_PARM_DESC(softcrc, "Set to 1 to use software CRCs instead of hardware ones");

static unsigned int outputdecodingtime = 0;
module_param(outputdecodingtime, uint, S_IRUGO | S_IWUSR);
MODULE_PARM_DESC(outputdecodingtime, "Set to 1 to output decoding time for each frame via strelay");

// memtest parameter:
//    n >= 0 : skip first n watchpoints of each image
//    n == -1: rotate thru watchpoints
//    n <= -2: do not check the memory
static int memtest = -3;
module_param(memtest, int, S_IRUGO | S_IWUSR);
MODULE_PARM_DESC(memtest, "Set to 0 to activate memory checking");

int get_module_param_softcrc() { return softcrc; }
EXPORT_SYMBOL(get_module_param_softcrc);
int get_module_param_outputdecodingtime() { return outputdecodingtime; }
EXPORT_SYMBOL(get_module_param_outputdecodingtime);
int get_module_param_memtest() { return memtest; }
EXPORT_SYMBOL(get_module_param_memtest);

extern struct debugInfo dbgInfo;

static struct HevcTransformerContext_t *AllocateContext(void);
static void FreeContext(struct HevcTransformerContext_t *context);
static int HevcClockSetRate(struct device_node *of_node);

static HadesError_t copyTransform(hevcdecpix_transform_param_t *frameParamsOut,
                                  hevcdecpix_transform_param_t *frameParamsIn);

// Global variables
//static int   HevcInstanceNumber = 0;   // TODO counts the number of initialized transformers for Power management
static struct HevcDeviceContext_t *HevcDeviceContext;

extern unsigned int decode_time;

static int HevcClockSetRate(struct device_node *of_node)
{
	int ret = 0;
	unsigned int max_freq = 0;
	if (!of_property_read_u32_index(of_node, "clock-frequency", 0, &max_freq)) {
		if (max_freq) {
			ret = clk_set_rate(HevcDeviceContext->clk_hwpe, max_freq);
			if (ret) {
				pr_err("Error: Failed to set max frequency for clk_hwpe_hades clock (%d)\n", ret);
				return -EINVAL;
			}
		}
	}
	pr_info("For CLK_HWPE_HADES ret value : 0x%x max frequency value : %u\n", (unsigned int)ret, max_freq);

	max_freq = 0;
	if (!of_property_read_u32_index(of_node, "clock-frequency", 1, &max_freq)) {
		if (max_freq) {
			ret = clk_set_rate(HevcDeviceContext->clk_fc, max_freq);
			if (ret) {
				pr_err("Error: Failed to set max frequency for clk_fc clock (%d)\n", ret);
				return -EINVAL;
			}
		}
	}
	pr_info("For CLK_FC_HADES ret value : 0x%x max frequency value : %u\n", (unsigned int)ret, max_freq);
	return ret;
}

static int HadesPowerOn(void)
{
	int ret = 0;
#ifdef CONFIG_PM_RUNTIME
	if (pm_runtime_get_sync(HevcDeviceContext->iDev) < 0) {
		ret = -EINVAL;
	}
#endif
	return ret;
}

static void HadesPowerOff(void)
{
#ifdef CONFIG_PM_RUNTIME
	pm_runtime_mark_last_busy(HevcDeviceContext->iDev);
	pm_runtime_put(HevcDeviceContext->iDev);
#endif
}

static int HadesDynamicPmOn(bool bypassPowerEnable)
{
	int ret;
	if (!bypassPowerEnable) {
		ret = HadesPowerOn();
		if (ret) {
			pr_err("Error: %s - failed to power on\n", __func__);
			return ret;
		}
	}

#ifdef CONFIG_ARCH_STI
	ret = clk_enable(HevcDeviceContext->clk_fc);
	if (ret) {
		pr_err("Error: %s - failed to clk on clk_fc\n", __func__);
		if (!bypassPowerEnable) { HadesPowerOff(); }
		goto clk_fail;
	}

	ret = clk_enable(HevcDeviceContext->clk_hwpe);
	if (ret) {
		pr_err("Error: %s - failed to clk on clk_hwpe\n", __func__);
		clk_disable(HevcDeviceContext->clk_fc);
		if (!bypassPowerEnable) { HadesPowerOff(); }
	}

clk_fail:
#endif
	return ret;
}

static void HadesDynamicPmOff(bool bypassPowerDisable)
{
#ifdef CONFIG_ARCH_STI
	clk_disable(HevcDeviceContext->clk_hwpe);
	clk_disable(HevcDeviceContext->clk_fc);
#endif
	if (!bypassPowerDisable) { HadesPowerOff(); }
}

#define ALIGN_ADDR(addr, alignment) ((((addr)+(alignment)-1) / (alignment))*(alignment))

static struct HevcTransformerContext_t *AllocateContext(void)
{
	int ret = 0;
	struct HevcTransformerContext_t *context;

	context = kmalloc(sizeof(*context), GFP_KERNEL);
	if (context == NULL) {
		pr_err("Error: %s context allocation failure\n", __func__);
		return NULL;
	}
	memset(context, 0, sizeof(*context));

	if (HADES_Malloc(&context->hadesCmdAndStatusAddr,
	                 sizeof(*context->hades_cmd) + sizeof(*context->hades_status)) == HADES_NO_ERROR) {
		context->hades_cmd = (hades_cmd_t *) context->hadesCmdAndStatusAddr.virtual;
		context->hades_status = (hades_status_t *)(context->hadesCmdAndStatusAddr.virtual + sizeof(*context->hades_cmd));
	} else {
		pr_err("Error: %s Out of resources (hadesCmdAndStatusAddr)\n", __func__);
		goto err_alloc_cmd;
	}

	// Allocs in FC-TCDM
	ret = sthorm_allocMessage(&context->hadesAttrAddr, sizeof(HadesAttr_t));
	if (ret != 1) {
		pr_err("Error: %s Out of resources (hadesAttrAddr) returns %d\n", __func__, ret);
		goto err_alloc_attr;
	}
	ret = sthorm_allocMessage(&context->pedfRunAddr, sizeof(pedf_run_t));
	if (ret != 1) {
		pr_err("Error: %s Out of resources (pedfRunAddr) returns %d\n", __func__, ret);
		goto err_alloc_pedf;
	}

	return context;

err_alloc_pedf:
	sthorm_freeMessage(&context->hadesAttrAddr);
err_alloc_attr:
	HADES_Free(&context->hadesCmdAndStatusAddr);
err_alloc_cmd:
	FreeContext(context);
	return NULL;
}

static void FreeContext(struct HevcTransformerContext_t *context)
{
	if (context != NULL) {
		if (context->hades_cmd != NULL) {
			HADES_Free(&context->hadesCmdAndStatusAddr);
		}
		if (context->pedfRunAddr.virtual) {
			sthorm_freeMessage(&context->pedfRunAddr);
		}
		if (context->hadesAttrAddr.virtual) {
			sthorm_freeMessage(&context->hadesAttrAddr);
		}
		kfree(context);
	}
}

HadesError_t HADES_CheckFwTags(struct HevcTransformerContext_t *ctx)
{
	HadesError_t Status = HADES_NO_ERROR;
#ifdef BUILD_DEFINED_SOC_STIH419
	int ret = 0;
	ret = HadesDynamicPmOn(false);
	if (ret != 0) {
		pr_err("Error: %s - unable to dynamic power on\n", __func__);
		return HADES_ERROR;
	}
	ctx->hades_cmd->command_type = HADES_FW_INFO;
	HADES_Flush(&ctx->hadesCmdAndStatusAddr);

	mutex_lock(&HevcDeviceContext->hw_lock);
	Status = HADES_ProcessCommand(HevcDeviceContext->hadesHandle,
	                              &ctx->pedfRunAddr,
	                              HADES_HostToFabricAddress(&ctx->hadesAttrAddr)); // blocking call
	mutex_unlock(&HevcDeviceContext->hw_lock);
	if (Status != HADES_NO_ERROR) {
		pr_err("Error: %s HADES_ProcessCommand failed:%d\n", __func__, Status);
		HadesDynamicPmOff(false);
		return HADES_ERROR;
	}
	HADES_Invalidate(&ctx->hadesCmdAndStatusAddr);

	pr_info("##############\n");
	pr_info("FIRMWARE INFO \n");
	pr_info("##############\n");
	pr_info("%s\n", ctx->hades_status->status.fw_info.hades_model_tag);
	pr_info("%s\n", ctx->hades_status->status.fw_info.hades_model_sha1);
	pr_info("%s\n", ctx->hades_status->status.fw_info.hevc_dec_ipm_tag);
	pr_info("%s\n", ctx->hades_status->status.fw_info.hevc_dec_ipm_sha1);
	pr_info("%s\n", ctx->hades_status->status.fw_info.pedf_home);
	pr_info("%s\n", ctx->hades_status->status.fw_info.pedf_link_mode);
	pr_info("%s\n\n", ctx->hades_status->status.fw_info.fw_build_string);
	HadesDynamicPmOff(false);
#endif
	(void)ctx; // warning removal
	return Status;
}

void write_bitfield(uint32_t offset, uint32_t mask, uint32_t shift, uint32_t val)
{
	uint32_t word = sthorm_read32(offset);
	word &= ~(mask << shift); //reset
	sthorm_write32(offset, (word | ((val & mask) << shift)));
}

void resetHWPEs(void)
{
#ifdef HEVC_HADES_CANNES25
	write_bitfield(HevcDeviceContext->hades_params.HadesBaseAddress + HOST_HWPE0_CFG_OFFSET, 0x1U, 0, 0);
	write_bitfield(HevcDeviceContext->hades_params.HadesBaseAddress + HOST_HWPE1_CFG_OFFSET, 0x1U, 0, 0);
	write_bitfield(HevcDeviceContext->hades_params.HadesBaseAddress + HOST_HWPE0_CFG_OFFSET, 0x1U, 0, 1);
	write_bitfield(HevcDeviceContext->hades_params.HadesBaseAddress + HOST_HWPE1_CFG_OFFSET, 0x1U, 0, 1);
	write_bitfield(HevcDeviceContext->hades_params.HadesBaseAddress + HOST_HWPE0_CFG_OFFSET, 0x1U, 0, 0);
	write_bitfield(HevcDeviceContext->hades_params.HadesBaseAddress + HOST_HWPE1_CFG_OFFSET, 0x1U, 0, 0);
#endif
}

// Clock HWPE MUST be enabled
void enablBWLimiter(hevcdecpix_transform_param_t *cmd)
{
#if !defined HEVC_HADES_CANNES25
	uint32_t cl0_dchan_base = HevcDeviceContext->hades_params.HadesBaseAddress +
	                          HOST_CL0_DCHAN_OFFSET;
	uint32_t word = 0x800U;
	if (cmd->pic_width_in_luma_samples * cmd->pic_height_in_luma_samples > 1920 * 1088) {
		word = 0xB00U;
	}
	sthorm_write32(cl0_dchan_base + HOST_CL0_DCHAN_WR_IRR_OFFSET, word);
	sthorm_write32(cl0_dchan_base + HOST_CL0_DCHAN_WR_IRR_THRESHOLD_OFFSET, 0x2U);
#endif
#ifdef HEVC_HADES_CANNES25
	uint32_t plug_base;
	// Based on worst-case 10bits UHDp50 + HDp50 usecase
	uint32_t irr_hwout_bw = 1486;     // bandwidth in MB/s (20 percent margin)
	uint32_t irr_xpred_bw = 2592;     // bandwidth in MB/s (40 percent margin)
	uint32_t bus_if_width_bytes = 16; // Hades ICN bus size in bytes

	uint32_t bus_if_freq = HevcDeviceContext->clk_tx_rate / 1000000; // Hades ICN bus freq in MHz
	if (bus_if_freq == 0) { bus_if_freq = 333; } // Default value

	irr_hwout_bw = (63 * irr_hwout_bw) / (bus_if_width_bytes * bus_if_freq) + 1;
	irr_xpred_bw = (63 * irr_xpred_bw) / (bus_if_width_bytes * bus_if_freq) + 1;
	resetHWPEs();

#define HADES_PLUG_IVCN_1_OFFSET       0x120
#define HADES_PLUG_IVCN_2_OFFSET       0x124
// HWPE0
	plug_base = HevcDeviceContext->hades_params.HadesBaseAddress + HOST_HWPE0_HWOUT_WRPLUG_OFFSET;
	// base_rwb_rate
	write_bitfield(plug_base + HADES_PLUG_IVCN_1_OFFSET, 0x3FU, 8, irr_hwout_bw);
	// base_max_bwe -> IRR BandWidth Error: must be programmed to its minimum value: 0x1
	// (equal to 2KB, minimum in actual version of IP-Plug)
	write_bitfield(plug_base + HADES_PLUG_IVCN_2_OFFSET, 0xFU, 1, 1);

	plug_base = HevcDeviceContext->hades_params.HadesBaseAddress + HOST_HWPE0_XPRED_RDPLUG_OFFSET;
	write_bitfield(plug_base + HADES_PLUG_IVCN_1_OFFSET, 0x3FU, 8, irr_xpred_bw);
	write_bitfield(plug_base + HADES_PLUG_IVCN_2_OFFSET, 0xFU, 1, 1);
// HWPE1
	plug_base = HevcDeviceContext->hades_params.HadesBaseAddress + HOST_HWPE1_HWOUT_WRPLUG_OFFSET;
	write_bitfield(plug_base + HADES_PLUG_IVCN_1_OFFSET, 0x3FU, 8, irr_hwout_bw);
	write_bitfield(plug_base + HADES_PLUG_IVCN_2_OFFSET, 0xFU, 1, 1);

	plug_base = HevcDeviceContext->hades_params.HadesBaseAddress + HOST_HWPE1_XPRED_RDPLUG_OFFSET;
	write_bitfield(plug_base + HADES_PLUG_IVCN_1_OFFSET, 0x3FU, 8, irr_xpred_bw);
	write_bitfield(plug_base + HADES_PLUG_IVCN_2_OFFSET, 0xFU, 1, 1);
#endif
}

// Synchronous call
static OSDEV_Status_t HadesIoctlProcessBuffer(struct HevcTransformerContext_t *HevcContext,
                                              struct HevcDecodeParameters *cmd, hevcdecpix_status_t *status)
{
	HadesError_t ret = HADES_NO_ERROR;
	HadesAttr_t *HadesAttr;
	hevcdecpix_transform_param_t *param_in;
	MME_ERROR mme_status = MME_SUCCESS;

	param_in = &cmd->TransformParameters;
	HevcContext->hades_cmd->command_type = HEVC;
	ret = copyTransform(&HevcContext->hades_cmd->command.hevc_mme_cmd, param_in);
	if (ret != HADES_NO_ERROR) {
		pr_err("Error: %s - failed to prepare transform parameters\n", __func__);
		return MME_INTERNAL_ERROR;
	}
	HADES_Flush(&HevcContext->hadesCmdAndStatusAddr);

	HadesAttr = (HadesAttr_t *) HevcContext->hadesAttrAddr.virtual;
	// Need clocks on to access FC TCDM
	if (HadesDynamicPmOn(false)) {
		pr_err("Error: %s - unable to dynamic power on\n", __func__);
		return MME_INTERNAL_ERROR;
	}

#ifdef CONFIG_STM_VIRTUAL_PLATFORM
	HadesAttr->hades_cmd_struct_p = (uint64_t)(HADES_HostToFabricAddress(&HevcContext->hadesCmdAndStatusAddr) +
	                                           offsetof(hades_cmd_t, command));
	HadesAttr->hades_status_struct_p = (uint64_t)(HADES_HostToFabricAddress(&HevcContext->hadesCmdAndStatusAddr)) +
	                                   sizeof(*HevcContext->hades_cmd) + offsetof(hades_status_t, status);
#else
	HadesAttr->hades_cmd_struct_p = (uintptr_t) HADES_HostToFabricAddress(&HevcContext->hadesCmdAndStatusAddr);
	HadesAttr->hades_status_struct_p = HadesAttr->hades_cmd_struct_p + sizeof(*HevcContext->hades_cmd);
#endif

	// Reset decode return status
	memset(HevcContext->hades_status, 0, sizeof(*HevcContext->hades_status));
	HevcContext->hades_status->command_type = HEVC;
	HevcContext->hades_status->status.hevc_mme_status.error_code = HEVC_DECODER_ERROR_NOT_RECOVERED;
	HADES_Flush(&HevcContext->hadesAttrAddr);
	if (get_module_param_memtest() >= -1) {
		if (HevcContext->dbg != NULL) {
			HevcContext->dbg->doMemTest(NULL, &HevcContext->hadesAttrAddr, &HevcContext->hadesCmdAndStatusAddr); // TODO NULL
		}
	}

	// HADES_ProcessCommand/enablBWLimiter must be mutex-locked for multi decode use cases (see pedfRunAddr variable)
	mutex_lock(&HevcDeviceContext->hw_lock);
	enablBWLimiter(param_in);
#ifdef CONFIG_STM_VIRTUAL_PLATFORM
	ret = HADES_ProcessCommand(HevcDeviceContext->hadesHandle,
	                           &HevcContext->pedfRunAddr, (uintptr_t)HevcContext->hadesAttrAddr.physical); // blocking call
#else
	ret = HADES_ProcessCommand(HevcDeviceContext->hadesHandle,
	                           &HevcContext->pedfRunAddr, HADES_HostToFabricAddress(&HevcContext->hadesAttrAddr)); // blocking call

#ifndef HEVC_HADES_CANNES25 // TODO  -> TBC for h419
	if (sthorm_read32(HevcDeviceContext->hades_params.HadesBaseAddress + HADES_CL0_HWPE_DEBUG_OFFSET +
	                  HEVC_HWPE_DEBUG_CRC_OUT_DISP_OFFSET) == 0xFFFFFFFFU) {
		pr_err("Error: %s HEVC Firmware Crash. Resetting board only option now to decode HEVC properly.\n",
		       __func__);
		mme_status = MME_COMMAND_TIMEOUT;
		ret = HADES_ERROR;
	}
#endif
#endif
	mutex_unlock(&HevcDeviceContext->hw_lock);
	if (ret != HADES_NO_ERROR) {
		pr_err("Error: %s HADES_ProcessCommand failed:%d\n", __func__, ret);
		switch (ret) {
		case HADES_ERROR:
			if (mme_status != MME_COMMAND_TIMEOUT) {
				mme_status = MME_INTERNAL_ERROR;
			}
			break;
		case HADES_INVALID_HANDLE:
			mme_status = MME_INVALID_HANDLE;
			break;
		case HADES_NO_MEMORY:
			mme_status = MME_NOMEM;
			break;
		default:
			mme_status = MME_INTERNAL_ERROR;
			break;
		}
	}

	if (HevcContext->dbg != NULL) { HevcContext->dbg->getDebugInfo(HevcDeviceContext, HevcContext, NULL); /* TODO NULL */   }
	HadesDynamicPmOff(false);

	HADES_Invalidate(&HevcContext->hadesCmdAndStatusAddr);
	memcpy(status, &HevcContext->hades_status->status.hevc_mme_status, sizeof(*status));

	if (HevcContext->dbg != NULL) { HevcContext->dbg->processDebugInfo(HevcContext, NULL); /* TODO */ }
	if (status->error_code != 0) {
		pr_err("Error: %s processCommand: HEVC decode status is %d\n", __func__, status->error_code);
	}

	return OSDEV_NoError;
}

/**
 * Main ioctl function
 */
static OSDEV_IoctlEntrypoint(HadesIoctl)
{
	OSDEV_Status_t Status = OSDEV_NoError;
	struct HevcTransformerContext_t *HevcContext;
	struct hades_ioctl_process_frame *param = NULL;

	OSDEV_IoctlEntry();
	HevcContext = (struct HevcTransformerContext_t *)OSDEV_PrivateData;
	BUG_ON(!HevcContext);

	if (HevcContext->ioctl_param == NULL) {
		HevcContext->ioctl_param = kzalloc(sizeof(*param), GFP_KERNEL);
	}
	if (HevcContext->ioctl_param == NULL) {
		pr_err("Error: %s Unable to allocate param\n", __func__);
		OSDEV_IoctlExit(OSDEV_Error);
	}
	param = (struct hades_ioctl_process_frame *)HevcContext->ioctl_param;

	switch (OSDEV_IoctlCode) {
	case HADES_IOCTL_PROCESS_FRAME:
		OSDEV_CopyToDeviceSpace(param, OSDEV_ParameterAddress, sizeof(*param));
		Status = HadesIoctlProcessBuffer(HevcContext, &param->cmd, &param->status);
		OSDEV_CopyToUserSpace(OSDEV_ParameterAddress, param, sizeof(*param));
		break;

	default:
		pr_err("Error: %s Invalid ioctl %08x\n", __func__, OSDEV_IoctlCode);
		Status = OSDEV_Error;
		break;
	}

	OSDEV_IoctlExit(Status);
}

/**
 * Open function creates a new internal instance
 */
static OSDEV_OpenEntrypoint(HadesOpen)
{
	struct HevcTransformerContext_t *HevcContext;
	HadesError_t hades_status = HADES_NO_ERROR;

	OSDEV_OpenEntry();

	BUG_ON(!HevcDeviceContext);
	BUG_ON(!HevcDeviceContext->hadesHandle);

	// Allocate a new context
	HevcContext = AllocateContext();
	if (HevcContext == NULL) {
		pr_err("Error: %s - Unable to allocate Hevc Context in %s\n", __func__, MODULE_NAME);
		return MME_NOMEM;
	}

	HevcContext->instanceId = atomic_read(&HevcDeviceContext->instance_nb);
	atomic_inc(&HevcDeviceContext->instance_nb);

#ifdef CONFIG_STM_HADES_DEBUG_TOOLS
	HevcContext->dbg = &dbgInfo;
#endif
	if (HevcContext->dbg != NULL) { HevcContext->dbg->init(HevcContext); }

	// TODO TBC in multi decoding use cases
	//hades_status = HADES_CheckFwTags(HevcContext);
	if (hades_status != HADES_NO_ERROR) {
		pr_err("Error: %s Unable to get fw info %s (%d)\n", __func__,
		       MODULE_NAME, hades_status);
	}

	OSDEV_PrivateData = HevcContext;
	pr_info("%s: Hades instance created\n", __func__);

	OSDEV_OpenExit(OSDEV_NoError);
}

static OSDEV_CloseEntrypoint(HadesClose)
{
	struct HevcTransformerContext_t *HevcContext;

	OSDEV_CloseEntry();

	HevcContext = (struct HevcTransformerContext_t *)OSDEV_PrivateData;

	BUG_ON(!HevcContext);
	if (HevcContext->ioctl_param != NULL) {
		kfree(HevcContext->ioctl_param);
		HevcContext->ioctl_param = NULL;
	}
	if (HevcContext->dbg != NULL) { HevcContext->dbg->term(HevcContext); }

	FreeContext(HevcContext);
	atomic_dec(&HevcDeviceContext->instance_nb);

	pr_info("%s: >>> Hades instance terminated\n", __func__);
	OSDEV_CloseExit(OSDEV_NoError);
}

static HadesError_t copyTransform(hevcdecpix_transform_param_t *frameParamsOut,
                                  hevcdecpix_transform_param_t *frameParamsIn)
{
	uintptr_t fabric_addr_p;
#ifndef HEVC_HADES_CANNES25 //TODO
	int       referenceIndex;
	int       listIndex;
	bool      converted[HEVC_MAX_REFERENCE_PICTURES];
#endif

	memcpy(frameParamsOut, frameParamsIn, sizeof(*frameParamsOut));

#define TRANSLATE(ADDR) \
	if (frameParamsOut->ADDR != 0) { \
		fabric_addr_p = HADES_PhysicalToFabricAddress((void*)frameParamsOut->ADDR); \
		if (fabric_addr_p == 0) \
		{ \
			pr_err( "Error: %s - HADES API: unable to translate " \
			#ADDR " = %p into fabric address\n", __func__, (void*)(frameParamsOut->ADDR)); \
			return HADES_ERROR; \
		} \
		frameParamsOut->ADDR = (uint32_t)fabric_addr_p; \
	}

	// Adresses must be translated into the HADES memory space
	// Intermediate buffer
	TRANSLATE(intermediate_buffer.slice_table_base_addr)
	TRANSLATE(intermediate_buffer.ctb_table_base_addr)
	TRANSLATE(intermediate_buffer.slice_headers_base_addr)

#ifndef HEVC_HADES_CANNES25
	TRANSLATE(intermediate_buffer.ctb_commands.base_addr)
	TRANSLATE(intermediate_buffer.ctb_residuals.base_addr)

	// Reference pictures
	for (referenceIndex = 0; referenceIndex < HEVC_MAX_REFERENCE_PICTURES; referenceIndex++) {
		converted[referenceIndex] = false;
	}
	for (listIndex = 0; listIndex < frameParamsOut->num_reference_pictures; listIndex++) {
		referenceIndex = frameParamsOut->initial_ref_pic_list_l0[listIndex];
		if (! converted[referenceIndex]) {
			TRANSLATE(ref_picture_buffer[referenceIndex].ppb_offset)
			TRANSLATE(ref_picture_buffer[referenceIndex].samples.luma_offset)
			TRANSLATE(ref_picture_buffer[referenceIndex].samples.chroma_offset)
			converted[referenceIndex] = true;
		}
		referenceIndex = frameParamsOut->initial_ref_pic_list_l1[listIndex];
		if (! converted[referenceIndex]) {
			TRANSLATE(ref_picture_buffer[referenceIndex].ppb_offset)
			TRANSLATE(ref_picture_buffer[referenceIndex].samples.luma_offset)
			TRANSLATE(ref_picture_buffer[referenceIndex].samples.chroma_offset)
			converted[referenceIndex] = true;
		}
	}

	// TODO: rm (already allocated in codec class)
	// JLX: patch for PPB: the Hades model needs a valid non-null PPB pointer even when nothing is written inside
	if (frameParamsOut->curr_picture_buffer.ppb_offset == 0) { // non-ref picture
		frameParamsOut->curr_picture_buffer.ppb_offset = frameParamsOut->curr_display_buffer.samples.luma_offset;
	}

	// Current picture for future reference
	TRANSLATE(curr_picture_buffer.ppb_offset)
	TRANSLATE(curr_picture_buffer.samples.luma_offset)
	TRANSLATE(curr_picture_buffer.samples.chroma_offset)

	// Display pictures
	TRANSLATE(curr_display_buffer.samples.luma_offset)
	TRANSLATE(curr_display_buffer.samples.chroma_offset)
	TRANSLATE(curr_resize_buffer.samples.luma_offset)
	TRANSLATE(curr_resize_buffer.samples.chroma_offset)
#else
	// ERC buffers
	frameParamsOut->fw_address_translation = HevcDeviceContext->fw_info.fw_address_translation;
#endif

	return HADES_NO_ERROR;
}

static OSDEV_Descriptor_t HadesDeviceDescriptor = {
	.Name = "Hades Module",
	.MajorNumber = MAJOR_NUMBER,
	.OpenFn  = HadesOpen,
	.CloseFn = HadesClose,
	.IoctlFn = HadesIoctl,
	.MmapFn  = NULL
};

//
// Loads firmware from user space filesystem into kernel memory
//
int HadesLoadFirmware(struct HevcDeviceContext_t *hades_ctx)
{
	unsigned char         *fw_addr;
	const struct firmware *fw_entry;
	int                    ret = 0;

#ifdef CONFIG_STM_VIRTUAL_PLATFORM
	return 0;
#endif

	// fallback in case DT property failed (ensure backward compatibility)
	if (strnlen(hades_ctx->fw_info.Name, PATH_LENGTH) == 0) {
		//TODO : build flag must be removed asap (Cannes 2.5 and further
		// versions MUST use DT property for firmware
#ifdef HEVC_HADES_CANNES25 //using DT is mandatory from cannes2.5
		pr_err("Error: %s hades fw not found\n", __func__);
		return -EINVAL;
#else //Cannes 2 cut2
		strncpy(hades_ctx->fw_info.Name, "hades_fw.bin", PATH_LENGTH);
#endif
		hades_ctx->fw_info.Name[sizeof(hades_ctx->fw_info.Name) - 1] = '\0';
	}
	// Get the firmware
	if ((ret = request_firmware(&fw_entry, hades_ctx->fw_info.Name, hades_ctx->iDev)) != 0) {
		pr_err("Error: %s loading firmware into kernel space %s (%d)\n",
		       __func__, MODULE_NAME, ret);
		return -EINVAL;
	}

	//// in full static link mode, the elf file can be directly loaded
	hades_ctx->fw_info.FwOffset = 0;
	hades_ctx->fw_info.FwId = 0;
	hades_ctx->fw_info.Size = fw_entry->size;
	if (!hades_ctx->fw_info.DestAddr) {
		hades_ctx->fw_info.DestAddr = (void *)HOST_DEFAULT_BASE_FIRMWARE_ADDRESS;
	}

	pr_info("%s Hades fw name: %s @ %p (size: %d)\n", __func__,
	        hades_ctx->fw_info.Name, hades_ctx->fw_info.DestAddr, hades_ctx->fw_info.Size);

	fw_addr = (unsigned char *)fw_entry->data + hades_ctx->fw_info.FwOffset;
	// extract BaseRuntime (elf static binary) in correct address
	host_loadELF(fw_addr, &hades_ctx->fw_info);

	release_firmware(fw_entry);
	return 0;
}

static int LoadHadesDevice(bool bypassPowerMgmt)
{
	HadesError_t hades_status = HADES_NO_ERROR;
	int ret = 0;

	mutex_init(&HevcDeviceContext->hw_lock);
	ret = HadesDynamicPmOn(bypassPowerMgmt);
	if (ret) {
		pr_err("Error: %s - unable to dynamic power on\n", __func__);
		return ret;
	}

	hades_status = HADES_Init(&HevcDeviceContext->hades_params);
	if (hades_status != HADES_NO_ERROR) {
		pr_err("Error: %s initializing HADES API (%d)\n", __func__, hades_status);
		ret = -EINVAL;
		goto hades_pmoff;
	}

	pr_info("%s: Hades API initialized with registers base %p, IRQ base %d, BPA2 partition %s\n",
	        __func__, (void *)(HevcDeviceContext->hades_params.HadesBaseAddress),
	        HevcDeviceContext->hades_params.InterruptNumber, HevcDeviceContext->hades_params.partition);

	if ((ret = HadesLoadFirmware(HevcDeviceContext))) {
		pr_err("Error: %s hades failed to load firmware\n", __func__);
		goto hades_pmoff;
	}

	pr_notice("%s: HADES FW name: %s, size: %d\n", __func__,
	          HevcDeviceContext->fw_info.Name, HevcDeviceContext->fw_info.Size);
	hades_status = HADES_Boot(&HevcDeviceContext->fw_info);
	if (hades_status != HADES_NO_ERROR) {
		pr_err("Error: %s booting firmware %s (%d)\n", __func__, MODULE_NAME, hades_status);
		ret = -EINVAL;
		goto fail_loadhades;
	}

	hades_status = HADES_InitTransformer(&HevcDeviceContext->hadesHandle, &HevcDeviceContext->fw_info);
	if (hades_status != HADES_NO_ERROR) {
		pr_err("Error: %s initializing HADES transformer %s (%d)\n", __func__,
		       MODULE_NAME, hades_status);
		ret = -EINVAL;
		goto fail_loadhades;
	}

#ifdef HEVC_HADES_CANNES25
	HevcDeviceContext->fw_info.fw_address_translation = HADES_PhysicalToFabricAddress(
	                                                            HevcDeviceContext->fw_info.Addr.physical) - (uint32_t)(HevcDeviceContext->fw_info.Addr.physical);
#endif
	goto hades_pmoff;

fail_loadhades:
	HADES_Term(&HevcDeviceContext->fw_info);
hades_pmoff:
	HadesDynamicPmOff(bypassPowerMgmt);
	return ret;
}

static int UnloadHadesDevice(bool bypassPowerMgmt)
{
	int ret = 0;
	HadesError_t Status = HADES_NO_ERROR;

	ret = HadesDynamicPmOn(bypassPowerMgmt);
	if (ret) {
		pr_err("Error: %s - unable to dynamic power on\n", __func__);
		goto fail_hadesdynamicpoweron;
	}

	Status = HADES_TermTransformer(HevcDeviceContext->hadesHandle);
	if (Status != HADES_NO_ERROR) {
		pr_err("Error: %s - error %d while terminating Hades transformer\n", __func__, Status);
	}

	HadesDynamicPmOff(bypassPowerMgmt);

fail_hadesdynamicpoweron:
	return ret;
}

static void hades_unprep_clk(struct HevcDeviceContext_t *hades_ctx)
{
#ifdef CONFIG_ARCH_STI
	clk_unprepare(hades_ctx->clk_fc);
	clk_unprepare(hades_ctx->clk_hwpe);
#endif //CONFIG_ARCH_STI
}

static int hades_get_of_pdata(struct platform_device *pdev, struct HevcDeviceContext_t *hades_ctx)
{
	int ret = 0;
	const char *fw_name;
	uint32_t destAddr = HOST_DEFAULT_BASE_FIRMWARE_ADDRESS;

#ifdef CONFIG_ARCH_STI
	hades_ctx->clk_hwpe = devm_clk_get(&pdev->dev, "clk_hwpe_hades");
	if (IS_ERR(hades_ctx->clk_hwpe)) {
		pr_err("Error: %s failed to get hades HWPE clock\n", __func__);
		return -EINVAL;
	}

	ret = clk_prepare(hades_ctx->clk_hwpe);
	if (ret) {
		pr_err("Error: %s failed to prepare hades HWPE clock (%d)\n", __func__, ret);
		return ret;
	}

	hades_ctx->clk_fc = devm_clk_get(&pdev->dev, "clk_fc_hades");
	if (IS_ERR(hades_ctx->clk_fc)) {
		pr_err("Error: %s failed to get hades FC clock\n", __func__);
		ret = -EINVAL;
		goto fail_clk_hwpe;
	}

	ret = HevcClockSetRate(pdev->dev.of_node);
	if (ret) {
		pr_err("Error: failed to set max frequencies for HADES clock (%d)\n", ret);
		return -EINVAL;
	}

	ret = clk_prepare(hades_ctx->clk_fc);
	if (ret) {
		pr_err("Error: %s failed to prepare hades FC clock (%d)\n", __func__, ret);
		goto fail_clk_hwpe;
	}

	hades_ctx->clk_tx_icn = devm_clk_get(&pdev->dev, "clk_tx_icn_hades");
	if (IS_ERR(hades_ctx->clk_tx_icn)) {
		pr_err("Error: %s failed to get hades TX_ICN clock\n", __func__);
		hades_ctx->clk_tx_icn = NULL; // Non critical
	} else {
		hades_ctx->clk_tx_rate = clk_get_rate(hades_ctx->clk_tx_icn);
		pr_debug("clk_tx_icn rate: %d\n", hades_ctx->clk_tx_rate);
	}

	if (!of_property_read_string(pdev->dev.of_node, "firmware", &fw_name)) {
		strncpy(hades_ctx->fw_info.Name, fw_name, PATH_LENGTH);
		hades_ctx->fw_info.Name[sizeof(hades_ctx->fw_info.Name) - 1] = '\0';
	} else {
		pr_info("%s Unable to get DT firmware property\n", __func__);
	}
	if (of_property_read_u32(pdev->dev.of_node, "firmware-addr", &destAddr)) {
		pr_info("%s Unable to get firmware-addr DT property\n", __func__);
	}

	// Due to RAB constraints alignement on 4K pages
	hades_ctx->fw_info.DestAddr = (void *)(destAddr & ~(0xFFFU));
	if (hades_ctx->fw_info.DestAddr != (void *)destAddr) {
		pr_warn("%s Address to load hades fw MUST be aligned on 0xFFF\n", __func__);
	}

	return 0;

fail_clk_hwpe:
	clk_unprepare(hades_ctx->clk_hwpe);
#endif //CONFIG_ARCH_STI

	return ret;
}

//
// driver probe is called by kernel when the platform devices are added by the player2 "platform" module
//
static int HadesProbe(struct platform_device *pdev)
{
	struct resource   *pResource;
	int                ret = 0;
	OSDEV_Status_t Status = OSDEV_NoError;

	BUG_ON(!pdev);

	HevcDeviceContext = devm_kzalloc(&pdev->dev, sizeof(*HevcDeviceContext), GFP_KERNEL);
	if (!HevcDeviceContext) {
		pr_err("Error: %s alloc failed\n", __func__);
		ret = -ENOMEM;
		goto fail_hades;
	}
	/**
	 * Saving pointer to main struct to be able to retrieve it in pm_runtime
	 * callbacks for example (it is *NOT* platform device data, just *driver* data)
	 */
	platform_set_drvdata(pdev, HevcDeviceContext);
	HevcDeviceContext->iDev = &pdev->dev;

	if (pdev->dev.of_node) {
		pr_info("HadesProbe: Probing device with DT\n");
		ret = hades_get_of_pdata(pdev, HevcDeviceContext);
		if (ret) {
			pr_err("Error: %s hades failed to retrieve driver data\n", __func__);
			goto fail_hades;
		}
	} else {
		pr_info("Error: %s hades no DT entry\n", __func__);
	}

#ifdef CONFIG_PM_RUNTIME
	// Power management
	/* Clear the device's 'power.runtime_error' flag and set the device's runtime PM status to 'suspended' */
	pm_runtime_set_suspended(&pdev->dev);
	pm_suspend_ignore_children(&pdev->dev, 1);
	pm_runtime_enable(&pdev->dev);
#endif

	// Platform info: get register base address and IRQ number
	pResource = platform_get_resource_byname(pdev, IORESOURCE_MEM, "hades_registers");
	if (!pResource) {
		pr_err("Error: %s platform_get_resource_byname failed\n", __func__);
		ret = -ENODEV;
		goto fail_hadesprobe;
	}

	HevcDeviceContext->hades_params.HadesBaseAddress = pResource->start - HADES_FABRIC_OFFSET;
	HevcDeviceContext->hades_params.HadesSize = pResource->end - HevcDeviceContext->hades_params.HadesBaseAddress + 1;
	pr_debug("%s: registers base address is %p\n", __func__,
	         (void *)HevcDeviceContext->hades_params.HadesBaseAddress);

	ret = platform_get_irq_byname(pdev, "hades_gen_irq");
	if (ret <= 0) {
		pr_err("Error: %s - platform_get_irq_byname failed %d\n", __func__, ret);
		goto fail_hadesprobe;
	} else {
		pr_debug("%s - irq number is %d\n", __func__, ret);
	}

	HevcDeviceContext->hades_params.InterruptNumber = ret;
	pr_debug("%s - hades request IRQ %d\n", __func__, HevcDeviceContext->hades_params.InterruptNumber);

	// Initialize HADES API
	HevcDeviceContext->hades_params.partition = partition;

	ret = LoadHadesDevice(false);
	if (ret) {
		pr_err("Error: %s LoadHadesDevice failed %d\n", __func__, ret);
		goto fail_hadesdevice;
	}

	Status = OSDEV_RegisterDevice(&HadesDeviceDescriptor);
	if (Status != OSDEV_NoError) {
		pr_err("Error: %s Unable to get major %d\n", __func__, MAJOR_NUMBER);
		ret = -ENODEV;
		goto fail_hadesTransformer;
	}

	Status = OSDEV_LinkDevice(HADES_DEVICE, MAJOR_NUMBER, 0);
	if (Status != OSDEV_NoError) {
		pr_err("Error: %s OSDEV_LinkDevice failed (major %d)\n", __func__, MAJOR_NUMBER);
		OSDEV_DeRegisterDevice(&HadesDeviceDescriptor);
		ret = -ENODEV;
		goto fail_hadesTransformer;
	}

	OSDEV_Dump_MemCheckCounters(__func__);

	pr_info("%s probe done ok\n", MODULE_NAME);
	return 0;

fail_hadesTransformer:
	UnloadHadesDevice(false);
	HADES_Term(&HevcDeviceContext->fw_info);
fail_hadesdevice:
fail_hadesprobe:
	hades_unprep_clk(HevcDeviceContext);
#ifdef CONFIG_PM_RUNTIME
	pm_runtime_disable(&pdev->dev);
#endif
fail_hades:
	OSDEV_Dump_MemCheckCounters(__func__);
	pr_err("Error: %s probe failed\n", MODULE_NAME);

	return ret;
}

static int HadesRemove(struct platform_device *pdev)
{
	UnloadHadesDevice(false);

	HADES_Term(&HevcDeviceContext->fw_info);
	hades_unprep_clk(HevcDeviceContext);
#ifdef CONFIG_PM_RUNTIME
	// Prevent use of PM runtime
	pm_runtime_disable(&pdev->dev);
#endif

	OSDEV_Dump_MemCheckCounters(__func__);
	OSDEV_DeRegisterDevice(&HadesDeviceDescriptor);

	pr_info("%s remove done\n", MODULE_NAME);
	return 0;
}

// PM suspend callback
// For Gateway profile: called on Host Passive Standby entry (HPS entry)
// For STB profile: called on Controller Passive Standby entry (CPS entry)
static int HadesPmSuspend(struct device *dev)
{
	pr_info("%s\n", __func__);
	(void)dev; // warning removal
	UnloadHadesDevice(true);
	HADES_Term(&HevcDeviceContext->fw_info);
	return 0;
}

// PM resume callback
// For Gateway profile: called on Host Passive Standby exit (HPS exit)
// For STB profile: called on Controller Passive Standby exit (CPS exit)
static int HadesPmResume(struct device *dev)
{
	int ret = 0;
	pr_info("%s\n", __func__);
	(void)dev; // warning removal

	ret = LoadHadesDevice(true);
	if (ret) {
		pr_err("Error: %s - booting hades, status(%d)\n", __func__, ret);
		ret = -EINVAL;
	}
	return ret;
}

// Hades runtime_suspend callback: relates to active profile standby
static int HadesPmRuntimeSuspend(struct device *dev)
{
#ifdef CONFIG_PM_RUNTIME
	int instance_nb = atomic_read(&HevcDeviceContext->instance_nb);
#endif
	pr_info("%s\n", __func__);
#ifdef CONFIG_PM_RUNTIME
	//Check no existing encoder instance to allow callback execution
	if (HevcDeviceContext && (instance_nb != 0)) {
		pr_info("HadesPmRuntime: still %d instances running ...\n", instance_nb);
		// Signal PM core there is still a user for the device: otherwise, runtime_suspend will never be called any more (see bug 22377)
		pm_runtime_get(dev);
		// warning, error code should be 0, -EBUSY or -EAGAIN to enable future callback execution
		return -EBUSY;
	}
#endif
	return 0;
}

// Hades runtime_resume callback
static int HadesPmRuntimeResume(struct device *dev)
{
	pr_info("%s\n", __func__);
	(void)dev; // warning removal
	return 0;
}

static struct dev_pm_ops HadesPmOps = {
	.suspend = HadesPmSuspend,
	.resume  = HadesPmResume,
	.runtime_suspend  = HadesPmRuntimeSuspend,
	.runtime_resume   = HadesPmRuntimeResume,
};

#ifdef CONFIG_OF
static struct of_device_id stm_hades_match[] = {
	{
		.compatible = "st,se-hades",
	},
	{},
};
//MODULE_DEVICE_TABLE(of, stm_hades_match);
#endif
static struct platform_driver HadesDriver = {
	.driver = {
		.name = HADES_DEVICE_NAME,
		.owner = THIS_MODULE,
		.of_match_table = of_match_ptr(stm_hades_match),
		.pm = &HadesPmOps,
	},
	.probe = HadesProbe,
	.remove = HadesRemove,
};

module_platform_driver(HadesDriver);

