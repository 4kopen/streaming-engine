/************************************************************************
Copyright (C) 2003-2015 STMicroelectronics. All Rights Reserved.

This file is part of the Streaming Engine.

Streaming Engine is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License version 2 as
published by the Free Software Foundation.

Streaming Engine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with Streaming Engine; see the file COPYING.  If not, write to the Free
Software Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307,
USA.

The Streaming Engine Library may alternatively be licensed under a
proprietary license from ST.
************************************************************************/

/*
 * PRINCIPLES OF OPERATION.
 *
 * This file defines some kernel structures whose size must be known at
 * compile-time by player2 for defining matching opaque types.  This file is
 * compiled and a script extracts these sizes from the resulting object file and
 * store them in a generated header file which is included by player2 OSAL.
 *
 * Doing it this way may insulates player2 from kernel structures size
 * variations resulting from kernel upgrades or changes in kernel configuration.
 * The latter is especially true for struct mutex whose size varies a lot
 * depending on various configuration options (debugging, SMP, spinning mutex
 * optimization...).  An alternative would be to hardcode in player2 sizes
 * big-enough for the biggest configuration but this is fragile and leads to
 * bloated objects that waste RAM and cache lines.
 */

#include <linux/mutex.h>
#include <linux/rtmutex.h>
#include <linux/rwsem.h>
#include <linux/semaphore.h>
#include <linux/spinlock.h>
#include <linux/wait.h>

struct mutex dummy_mutex;
struct rt_mutex dummy_rt_mutex;
struct rw_semaphore dummy_rw_semaphore;
struct semaphore dummy_semaphore;
struct spinlock dummy_spinlock;
wait_queue_head_t dummy_wait_queue_head;
