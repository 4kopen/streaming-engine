/************************************************************************
Copyright (C) 2003-2014 STMicroelectronics. All Rights Reserved.

This file is part of the Streaming Engine.

Streaming Engine is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License version 2 as
published by the Free Software Foundation.

Streaming Engine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with Streaming Engine; see the file COPYING.  If not, write to the Free
Software Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307,
USA.

The Streaming Engine Library may alternatively be licensed under a
proprietary license from ST.
************************************************************************/

#include <linux/module.h>
#include <linux/init.h>
#include <linux/device.h>
#include <linux/mutex.h>
#include <linux/slab.h>

// for not wrapping MME_ calls for this file
#define _ACC_MME_WRAPPER_C_
#include "acc_mme.h"

#include "st_relayfs_se.h"

MODULE_DESCRIPTION("MME logging utility functions");
MODULE_AUTHOR("STMicroelectronics");
MODULE_LICENSE("GPL");

DEFINE_MUTEX(cmd_mutex);

/* to avoid opening and closing a relay (which would break it) we leave
 * it open at all times and use a 'fake' FOPEN() from within cmd_log_lock() */

#define FILE void

FILE *usb_fout;
#define FWRITE(p,s,n,f) st_relayfs_write_se(ST_RELAY_TYPE_MME_LOG, ST_RELAY_SOURCE_SE, (unsigned char *) p, s*n, false)
#define FOPEN(n,p)      usb_fout
#define FFOPEN(n,p)     usb_fout
#define FCLOSE(f)       if (f) { f = NULL; }
#define CMD_LOG_FILE "acc_mme_wrapper"

enum eBoolean {
	ACC_FALSE = false,
	ACC_TRUE = true,
};

static bool log_enable = ACC_FALSE;
static bool store_enable = ACC_FALSE;

enum eCmdType {
	CMD_GETCAPABILITY, CMD_INIT, CMD_SEND, CMD_ABORT, CMD_TERM, CMD_LAST
};

static unsigned char *cmd_str[CMD_LAST] = {
	"CMD_GETC", "CMD_INIT", "CMD_SEND", "CMD_ABORT", "CMD_TERM"
};

struct mem_area {
	void   *addr;
	size_t  size;
};

struct cmd_list_s {
	struct list_head             list;
	MME_TransformerHandle_t      trans_hdl;
	size_t                       cmd_size;
	void                        *cmd_data;
};

LIST_HEAD(cmd_list_head);
DEFINE_MUTEX(cmd_list_mutex);

#define NB_MAX_TRANSFORMERS 8
int handles[NB_MAX_TRANSFORMERS + 1];

enum eWarning {
	ACC_WARNING_NO_SCATTERPAGE_IN_BUFFER,

	// do not edit
	ACC_LAST_WARNING
};

char warning[ACC_LAST_WARNING][64] = {
#define E(x) #x
	E(ACC_WARNING_NO_SCATTERPAGE_IN_BUFFER),
#undef E
};

static void handles_clear(void)
{
	int i;

	for (i = 0; i < NB_MAX_TRANSFORMERS; i++) {
		handles[i] = 0;
	}
}

static int handles_search(int hdl)
{
	int i;

	for (i = 0; i < NB_MAX_TRANSFORMERS; i++) {
		if (handles[i] == hdl) { return (i); }
	}

	return i;

}
static int handles_new(void)
{
	int i;

	for (i = 0; i < NB_MAX_TRANSFORMERS; i++) {
		if (handles[i] == 0) { return (i); }
	}
	return i;
}

FILE *cmd_log_lock(void)
{
	FILE *fin;

	mutex_lock(&cmd_mutex);

	fin = FOPEN(CMD_LOG_FILE, "ab");

	return (fin);
}

void cmd_log_release(FILE *fin)
{
	FCLOSE(fin);

	mutex_unlock(&cmd_mutex);
}

void acc_mme_logreset(void)
{
	FILE *fcmd;

	fcmd = FFOPEN(CMD_LOG_FILE, "wb");
	FCLOSE(fcmd);
}

void acc_mme_logstart(void)
{
	pr_info("Init of MME log wrapper\n");
	usb_fout = FFOPEN(CMD_LOG_FILE, "ab");
}

void acc_mme_logstop(void)
{
	log_enable = ACC_FALSE;
}

/*
 * free_cmd_list_item()
 *
 * Free dynamically allocated structures and delete list entry
 */
void free_cmd_list_item(struct list_head *list_item)
{
	struct cmd_list_s *cmd_item;

	cmd_item = list_entry(list_item, struct cmd_list_s, list);

	pr_debug("%s Remove cmd for Transformer:%u\n", __func__, cmd_item->trans_hdl);

	kfree(cmd_item->cmd_data);
	list_del(list_item);
	kfree(cmd_item);
}

/*
 * free_cmd_list_underlock()
 *
 * Walks the list of stored commands and free all entries.
 * cmd_list_mutex MUST be locked
 */
void free_cmd_list_underlock(void)
{
	struct list_head *list_item;
	struct list_head *temp;

	BUG_ON(!mutex_is_locked(&cmd_list_mutex));
	list_for_each_safe(list_item, temp, &cmd_list_head) {
		free_cmd_list_item(list_item);
	}
}

/*
 * free_cmd_list()
 *
 * Walks the list of stored commands and free all entries
 */
void free_cmd_list(void)
{
	pr_debug("%s\n", __func__);

	mutex_lock(&cmd_list_mutex);
	free_cmd_list_underlock();
	mutex_unlock(&cmd_list_mutex);
}


/*
 * free_all_transformer_cmd()
 *
 * Walks the list of stored commands and
 * free all entries associated to input transformer handle
 */
void free_all_transformer_cmd(MME_TransformerHandle_t hdl)
{
	struct list_head  *list_item;
	struct list_head  *temp;
	struct cmd_list_s *item;

	pr_info("%s Transformer Handle:%u\n", __func__, hdl);
	mutex_lock(&cmd_list_mutex);
	list_for_each_safe(list_item, temp, &cmd_list_head) {
		item = list_entry(list_item, struct cmd_list_s, list);
		if (item->trans_hdl == hdl) {
			free_cmd_list_item(list_item);
		}
	}
	mutex_unlock(&cmd_list_mutex);
}

/*
 * replay_cmd_list()
 *
 * Walks the list of stored commands and write all command data through strelay
 */
void replay_cmd_list(void)
{
	struct list_head  *list_item;
	struct cmd_list_s *item;
	FILE *fcmd = NULL;

	pr_debug("%s begin\n", __func__);

	mutex_lock(&cmd_list_mutex);
	if (list_empty(&cmd_list_head)) {
		pr_debug("%s list is empty\n", __func__);
		mutex_unlock(&cmd_list_mutex);
		return;
	}

	fcmd = cmd_log_lock();
	list_for_each(list_item, &cmd_list_head) {
		item = list_entry(list_item, struct cmd_list_s, list);
		FWRITE(item->cmd_data, item->cmd_size, 1, fcmd);
		pr_debug("%s replay MME %s for Trans:%u\n", __func__,
		         (char *) item->cmd_data, item->trans_hdl);
	}
	cmd_log_release(fcmd);

	free_cmd_list_underlock();
	mutex_unlock(&cmd_list_mutex);

	pr_debug("%s end\n", __func__);
}

/*
 * store_command()
 *
 * Allocates a single continuous memory are to copy data provided
 * through dump_array argument. Then it stores this copy into the list of
 * stored commands.
 *
 * @dump_array  [in]  NULL-terminted array of memory regions to store
 * @p_list_item [out] if a non-null p_list_item argument is provided,
 *                    store_command() fills it with pointer to allocated struct cmd_list_s
 *
 * return -ENOMEM : in case of memory allocation failure
 *              0 : otherwise
 */
static int store_command(struct mem_area dump_array[], struct cmd_list_s **p_list_item)
{
	int                i             = 0;
	void              *cmd_data_base = NULL;
	void              *cmd_data_ptr  = NULL;
	size_t             cmd_size      = 0;
	struct cmd_list_s *list_item     = NULL;

	if (!store_enable) {
		return 0;
	}

	/* Compute total size to allocate to store the command */
	while (dump_array[i].addr != NULL) {
		cmd_size += dump_array[i++].size;
	}

	cmd_data_base = kmalloc(cmd_size, GFP_KERNEL);
	if (cmd_data_base == NULL) {
		pr_err("%s Failed to kmalloc cmd_data_base\n", __func__);
		return -ENOMEM;
	}

	i = 0;
	cmd_data_ptr = cmd_data_base;
	while (dump_array[i].addr != NULL) {
		memcpy(cmd_data_ptr, dump_array[i].addr, dump_array[i].size);
		cmd_data_ptr += dump_array[i++].size;
	}

	list_item = (struct cmd_list_s *) kmalloc(sizeof(struct cmd_list_s), GFP_KERNEL);

	if (list_item == NULL) {
		kfree(cmd_data_base);
		pr_err("%s Failed to kmalloc list_item\n", __func__);
		return -ENOMEM;
	}

	list_item->cmd_size = cmd_size;
	list_item->cmd_data = cmd_data_base;

	mutex_lock(&cmd_list_mutex);
	list_add_tail(&list_item->list, &cmd_list_head);
	mutex_unlock(&cmd_list_mutex);

	pr_info("%s New command stored (size:%d bytes)\n", __func__, cmd_size);
	if (p_list_item != NULL) {
		*p_list_item = list_item;
	}

	return 0;
}

MME_ERROR acc_MME_InitTransformer(const char *Name,
                                  MME_TransformerInitParams_t *Params_p,
                                  MME_TransformerHandle_t      *Handle_p)
{
	MME_ERROR  mme_status;
	struct mem_area dump_array[5] = {
		{ cmd_str[CMD_INIT],                 strnlen(cmd_str[CMD_INIT], 256) + 1},
		{ (char *)Name,                      strnlen(Name, 256) + 1             },
		{ Params_p,                          sizeof(MME_TransformerInitParams_t)},
		{ Params_p->TransformerInitParams_p, Params_p->TransformerInitParamsSize},
		{ NULL                             , 0                                  }
	};

	if (log_enable == ACC_TRUE) {
		int i = 0;
		FILE *fcmd = cmd_log_lock();

		while (dump_array[i].addr != NULL) {
			FWRITE(dump_array[i].addr, dump_array[i].size, 1, fcmd);
			i++;
		}

		cmd_log_release(fcmd);
	}
	mme_status = MME_InitTransformer(Name, Params_p, Handle_p);

	if (mme_status == MME_SUCCESS) {
		handles[handles_new()] = (int) * Handle_p;
	}

	if (!log_enable) {
		struct cmd_list_s *list_item = NULL;
		store_command(dump_array, &list_item);
		if (list_item != NULL) {
			list_item->trans_hdl = *Handle_p;
		}
	}

	return mme_status;
}

MME_ERROR acc_MME_GetTransformerCapability(const char *TransformerName, MME_TransformerCapability_t *TransformerInfo_p)
{
	if (log_enable == ACC_TRUE) {
		FILE *fcmd = cmd_log_lock();

		FWRITE(cmd_str[CMD_GETCAPABILITY], strnlen(cmd_str[CMD_GETCAPABILITY], 256) + 1, 1, fcmd);
		FWRITE((char *)TransformerName, strnlen((char *)TransformerName, 256) + 1, 1, fcmd);
		cmd_log_release(fcmd);
	}

	return MME_GetTransformerCapability(TransformerName, TransformerInfo_p);
}

MME_ERROR acc_MME_SendCommand(MME_TransformerHandle_t Handle, MME_Command_t *CmdInfo_p)
{
	MME_ERROR   mme_status;
	FILE       *fcmd = NULL;
	int         j, nbuf;
	int         hdl_idx = handles_search(Handle);

	struct mem_area dump_array[6] = {
		{ cmd_str[CMD_SEND],  strnlen(cmd_str[CMD_SEND], 256) + 1},
		{ &hdl_idx,           sizeof(int)                        },
		{ CmdInfo_p,          sizeof(MME_Command_t)              },
		{ CmdInfo_p->Param_p, CmdInfo_p->ParamSize               },
		{ NULL,               0                                  }, // reserved for &CmdInfo_p->CmdStatus.CmdId
		{ NULL,               0                                  }
	};

	if (log_enable == ACC_TRUE) {
		// log the command;
		int i = 0;

		replay_cmd_list();

		fcmd = cmd_log_lock();

		while (dump_array[i].addr != NULL) {
			FWRITE(dump_array[i].addr, dump_array[i].size, 1, fcmd);
			i++;
		}

		nbuf =  CmdInfo_p->NumberInputBuffers + CmdInfo_p->NumberOutputBuffers;
		for (i = 0; i < nbuf; i++) {
			MME_DataBuffer_t   *db = CmdInfo_p->DataBuffers_p[i];
			MME_ScatterPage_t *sc;
			FWRITE(db, sizeof(MME_DataBuffer_t), 1, fcmd);

			if (db->NumberOfScatterPages == 0) {
				// check whether a buffer is sent without any pages  !! should never happen.
			}

			for (j = 0; j < db->NumberOfScatterPages; j++) {
				sc = &db->ScatterPages_p[j];

				FWRITE(sc, sizeof(MME_ScatterPage_t), 1, fcmd);
				if (sc->Size != 0) {
					if (i < CmdInfo_p->NumberInputBuffers) {
						FWRITE(sc->Page_p, sizeof(unsigned char), sc->Size, fcmd);
					}
				}
			}
		}
	}
	// send the command to get back the ID generated by Multicom
	mme_status = MME_SendCommand(Handle, CmdInfo_p);

	if (log_enable == ACC_TRUE) {
		// replace the ID in the local copy
		FWRITE(&CmdInfo_p->CmdStatus.CmdId, sizeof(unsigned int), 1 , fcmd);

		cmd_log_release(fcmd);
	} else if (CmdInfo_p->CmdCode == MME_SET_PARAMS) { // Only store config comands
		dump_array[4].addr = &CmdInfo_p->CmdStatus.CmdId;
		dump_array[4].size = sizeof(unsigned int);

		store_command(dump_array, NULL);
	}

	return mme_status;
}

MME_ERROR acc_MME_AbortCommand(MME_TransformerHandle_t Handle, MME_CommandId_t CmdId)
{
	if (log_enable == ACC_TRUE) {
		FILE *fcmd  = cmd_log_lock();
		int hdl_idx = handles_search(Handle);

		FWRITE(cmd_str[CMD_ABORT], strnlen(cmd_str[CMD_ABORT], 256) + 1, 1, fcmd);
		FWRITE(&hdl_idx, sizeof(int), 1, fcmd);
		FWRITE(&CmdId, sizeof(int), 1, fcmd);

		cmd_log_release(fcmd);
	}

	return MME_AbortCommand(Handle, CmdId);
}

/* MME_TermTransformer()
 * Terminate a transformer instance
 */
MME_ERROR acc_MME_TermTransformer(MME_TransformerHandle_t handle)
{
	MME_ERROR mme_status;
	int hdl_idx = handles_search(handle);

	if (log_enable == ACC_TRUE) {
		FILE *fcmd = cmd_log_lock();

		FWRITE(cmd_str[CMD_TERM], strnlen(cmd_str[CMD_TERM], 256) + 1, 1, fcmd);
		FWRITE(&hdl_idx, sizeof(int), 1, fcmd);

		cmd_log_release(fcmd);
	}

	mme_status = MME_TermTransformer(handle);

	if (mme_status == MME_SUCCESS) {
		handles[hdl_idx] = 0;
	}

	if (!log_enable) {
		free_all_transformer_cmd(handle);
	}

	return mme_status;
}

static int __init acc_mme_loginit(void)
{
	acc_mme_logstart();

	handles_clear();

	pr_info("%s done ok\n", __func__);
	return 0;
}
module_init(acc_mme_loginit);

static void __exit acc_mme_logcleanup(void)
{
	free_cmd_list();

	acc_mme_logstop();
	pr_info("%s done\n", __func__);
}
module_exit(acc_mme_logcleanup);

module_param(log_enable, bool, S_IRUGO | S_IWUSR);
module_param(store_enable, bool, S_IRUGO | S_IWUSR);
MODULE_PARM_DESC(store_enable,
                 "If enabled and log_enable is disabled, mmelog will allocate memory "
                 "to store INIT/SET_GLOBAL commands to replay them as soon as log_enable "
                 "is enabled.");

#if defined(CONFIG_DEBUG_FS)
EXPORT_SYMBOL(acc_MME_InitTransformer);
EXPORT_SYMBOL(acc_MME_TermTransformer);
EXPORT_SYMBOL(acc_MME_AbortCommand);
EXPORT_SYMBOL(acc_MME_SendCommand);
EXPORT_SYMBOL(acc_MME_GetTransformerCapability);
#endif // defined(CONFIG_DEBUG_FS)

