/************************************************************************
Copyright (C) 2003-2014 STMicroelectronics. All Rights Reserved.

This file is part of the Streaming Engine.

Streaming Engine is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License version 2 as
published by the Free Software Foundation.

Streaming Engine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with Streaming Engine; see the file COPYING.  If not, write to the Free
Software Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307,
USA.

The Streaming Engine Library may alternatively be licensed under a
proprietary license from ST.
************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <os21.h>
#include <stdarg.h>
#include <assert.h>

#include "osinline_os21.h"
#include "report.h"
#include "fatal_error.h"

// --------------------------------------------------------------
// Memory functions

void *OS_Malloc(unsigned int Size)
{
	return malloc(Size);
}

void OS_Free(void *Address)
{
	return free(Address);
}

// --------------------------------------------------------------
// Semaphore functions

OS_Status_t OS_SemaphoreInitialize(OS_Semaphore_t  *Semaphore,
                                   unsigned int     InitialCount)
{
	semaphore_t *sem = semaphore_create_fifo(InitialCount);
	if (sem == NULL) {
		return OS_ERROR;
	} else {
		*Semaphore = (OS_Semaphore_t) sem;
		return OS_NO_ERROR;
	}
}

OS_Status_t OS_SemaphoreTerminate(OS_Semaphore_t  *Semaphore)
{
	int res = semaphore_delete((semaphore_t *) *Semaphore);
	if (res == OS21_SUCCESS) {
		return OS_NO_ERROR;
	} else {
		return OS_ERROR;
	}
}

void OS_SemaphoreWaitAuto(OS_Semaphore_t *Semaphore)
{
	semaphore_wait((semaphore_t *) *Semaphore);
}

void OS_SemaphoreSignal(OS_Semaphore_t *Semaphore)
{
	semaphore_signal((semaphore_t *) *Semaphore);
}

// --------------------------------------------------------------
// Mutex functions

OS_Status_t OS_InitializeMutex(OS_Mutex_t  *Mutex)
{
	mutex_t *os21_mutex = mutex_create_fifo();

	if (os21_mutex) {
		*Mutex = (OS_Mutex_t) os21_mutex;
		return OS_NO_ERROR;
	} else {
		return OS_ERROR;
	}
}

OS_Status_t OS_TerminateMutex(OS_Mutex_t  *Mutex)
{
	int res = mutex_delete((mutex_t *)Mutex);
	if (res == OS21_SUCCESS) {
		return OS_NO_ERROR;
	} else {
		return OS_ERROR;
	}
}

void OS_LockMutex(OS_Mutex_t    *Mutex)
{
	mutex_lock((mutex_t *)Mutex);
}

void OS_UnLockMutex(OS_Mutex_t  *Mutex)
{
	mutex_release((mutex_t *)Mutex);
}

int OS_MutexIsLocked(OS_Mutex_t *Mutex)
{
	int res = mutex_trylock((mutex_t *)Mutex);
	if (res == OS21_FAILURE) {
		return true;
	} else {
		mutex_release((mutex_t *) Mutex);
		return false;
	}
}

// -----------------------------------------------------------------------------------------------
// event functions

OS_Status_t OS_InitializeEvent(OS_Event_t  *Event)
{
	/* create an event group that will explicitely clear
	   the event upon call to event_clear() */
	event_group_t *os21_event = event_group_create(0);

	if (os21_event) {
		*Event = (OS_Event_t) os21_event;
		return OS_NO_ERROR;
	} else {
		return OS_ERROR;
	}
}

OS_Status_t OS_TerminateEvent(OS_Event_t *Event)
{
	int res = event_group_delete((event_group_t *)Event);
	if (res == OS21_SUCCESS) {
		return OS_NO_ERROR;
	} else {
		return OS_ERROR;
	}
}

OS_Status_t OS_WaitForEvent(OS_Event_t *Event, OS_Timeout_t Timeout)
{
	int mask_out;
	int res;
	res = event_wait_all((event_group_t *)Event, 1, &mask_out, (const osclock_t *) &Timeout);
	if (res == OS21_FAILURE) {
		return OS_TIMED_OUT;
	} else {
		return OS_NO_ERROR;
	}
}

void OS_SetEvent(OS_Event_t *Event)
{
	event_post((event_group_t *)Event, 1);
}

void OS_ResetEvent(OS_Event_t *Event)
{
	event_clear((event_group_t *)Event, 1);
}

// --------------------------------------------------------------
// Thread functions

OS_Status_t   OS_CreateThread(OS_Thread_t         *Thread,
                              OS_TaskEntry_t       TaskEntry,
                              OS_TaskParam_t       Parameter,
                              const OS_TaskDesc_t *TaskDesc)
{
	OS_Status_t      Status;
	task_t          *new_task;

	if (TaskDesc == NULL) {
		return OS_ERROR;
	}

	new_task = task_create((void (*)(void *)) TaskEntry, Parameter,
	                       32768,                 /* Default Stack Size */
	                       TaskDesc->priority,
	                       TaskDesc->name,
	                       0);                    /* Task flags */

	if (NULL == new_task) {
		return OS_ERROR;
	}

	*Thread = (OS_Thread_t) new_task;
	return OS_NO_ERROR;
}

void   OS_TerminateThread(void)
{
	/* nothing to do :
	 * no global table of created thread is being
	 * maintained in this abstraction layer
	 */
}

OS_Status_t OS_SetSchedAndAffinity(const OS_TaskDesc_t *TaskDesc)
{
	if (TaskDesc == NULL) {
		return OS_ERROR;
	}

	task_priority_set(NULL, TaskDesc->priority);
	return OS_NO_ERROR;
}

char   *OS_ThreadName(void)
{
	return task_name(NULL);
}

unsigned int OS_ThreadID(void)
{
	return (unsigned int)task_id();
}

// --------------------------------------------------------------
// time functions

unsigned long long OS_GetTimeInMicroSeconds(void)
{
	return time_now();
}

// --------------------------------------------------------------
// Memory functions

int OS_SafeRead8(const void *Addr, uint8_t *Result)
{
	*Result = *(uint8_t *)Addr;
	return 0;
}

int OS_SafeRead32(const void *Addr, uint32_t *Result)
{
	*Result = *(uint32 *)Addr;
	return 0;
}

// --------------------------------------------------------------
// report functions

static report_severity_t active_severity_level = severity_error;

int report_group_debug_level(int trace_group) { return active_severity_level; }

int report_is_severity_active(report_severity_t report_severity)
{
	if (report_severity > active_severity_level) { return 0; }
	else { return 1; }
}

int report_is_group_active(report_severity_t report_severity, int trace_group)
{
	return true;
}

void report_print(const char *format, ...)
{
	va_list ap;
	va_start(ap, format);
	vprintf(format, ap);
	va_end(ap);
}

void report_dump_hex(unsigned char *data, int length, int width, void *start) {}
void report_dump_current_stack_trace(const char *func) {}

void fatal_error(const char *fmt, ...)
{
	va_list ap;

	va_start(ap, fmt);
	vprintf(fmt, ap);
	va_end(ap);

	assert(0);
}

size_t OS_CaptureStackTrace(unsigned long *Frames, unsigned int MaxFrames, unsigned int Skip)
{
	return 0;
}
