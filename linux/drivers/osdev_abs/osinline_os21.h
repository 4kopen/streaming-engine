/************************************************************************
Copyright (C) 2003-2014 STMicroelectronics. All Rights Reserved.

This file is part of the Streaming Engine.

Streaming Engine is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License version 2 as
published by the Free Software Foundation.

Streaming Engine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with Streaming Engine; see the file COPYING.  If not, write to the Free
Software Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307,
USA.

The Streaming Engine Library may alternatively be licensed under a
proprietary license from ST.
************************************************************************/

#ifndef H_OSINLINE_OS21
#define H_OSINLINE_OS21
#include <os21.h>
#include <stdarg.h>

#include "st220.h"  // for __lzcntw and __swapbw

#define unlikely
typedef unsigned long      phys_addr_t;

// TODO(pht) replace followings with <stdint.h> ?
typedef unsigned long long uint64_t;
typedef long long          int64_t;
typedef unsigned long      uint32_t;
typedef long               int32_t;
typedef unsigned short     uint16_t;
typedef short              int16_t;
typedef unsigned char      uint8_t;
typedef char               int8_t;
typedef uint32_t          *uintptr_t;

#ifdef __cplusplus
#undef  true
#undef  false
#define true linux_stddef_true
#define false linux_stddef_false
typedef bool _Bool;
#define bool  linux_types_bool

#include <stddef.h>

extern "C" {
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
}

#undef  true
#undef  false
#undef  bool
#undef NULL
#define NULL 0
#undef  inline
#else
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#undef  true
#define true  1

#undef  false
#define false 0

#endif /* __cplusplus */

//
// Error code remapping (and inclusion of errno ABI)
#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

#include <errno.h>

#ifdef __cplusplus
}
#endif /* __cplusplus */

#include "report.h"

/* --- Define bool if we not in c++ --- */

#ifndef __cplusplus
#ifndef bool
#define bool    unsigned char
#endif /* bool */
/* true/false come from linux/stddef.h */
#endif /* __cplusplus */

/* --- Return codes -- values must be compatible with OSDEV_Status_t enums --- */

#define OS_NO_ERROR             OS21_SUCCESS
#define OS_ERROR                OS21_FAILURE
#define OS_TIMED_OUT            2
#define OS_INTERRUPTED          3

//

#define OS_NO_PRIORITY          0xffffffff
#define OS_HIGHEST_PRIORITY     99
#define OS_MID_PRIORITY         50
#define OS_LOWEST_PRIORITY      0
#define OS_INFINITE             ((OS_Timeout_t)0xffffffff)
#define OS_INVALID_THREAD       ((OS_Thread_t)0)

/* --- Elfic magic --- */

#define DATA_UNLIKELY_SECTION

/* --- Useful type declarations --- */

typedef osclock_t                        OS_Timeout_t;
typedef task_t                          *OS_Thread_t;
typedef void                            *OS_TaskParam_t;
typedef struct {
	const char *name;
	int policy;
	int priority;
	int affinity_mask;
} OS_TaskDesc_t;

typedef int                              OS_Status_t;
typedef semaphore_t                     *OS_Semaphore_t;
typedef mutex_t                         *OS_Mutex_t;
typedef event_group_t                   *OS_Event_t;
typedef phys_addr_t                      OS_PhysAddr_t;

typedef struct OS_SpinLock_s            *OS_SpinLock_t;
typedef struct OS_RwLock_s              *OS_RwLock_t;

typedef void  *(*OS_TaskEntry_t)(void *Parameter);

// -----------------------------------------------------------------------------------------------

#ifdef __cplusplus
extern "C" {
#endif

// --------------------------------------------------------------
//      Specialist instruction emulation functions.
//      left these as inlines, since they do not
//      interact with anything else, they are generally
//      required to be fast.

static inline int OS_CountLeadingZeros(unsigned int a) { return __lzcntw(a); }
static inline int OS_Fls(unsigned int a) { return 32 - OS_CountLeadingZeros(a); }

// --------------------------------------------------------------
//      Memory functions

void         *OS_Malloc(unsigned int             Size);
void          OS_Free(void                    *Address);

// --------------------------------------------------------------
//      Semaphore functions

OS_Status_t   OS_SemaphoreInitialize(OS_Semaphore_t          *Semaphore,
                                     unsigned int             InitialCount);
OS_Status_t   OS_SemaphoreTerminate(OS_Semaphore_t          *Semaphore);
void          OS_SemaphoreWaitAuto(OS_Semaphore_t          *Semaphore);
OS_Status_t   OS_SemaphoreWaitInterruptible(OS_Semaphore_t          *Semaphore);
OS_Status_t   OS_SemaphoreWaitTimeout(OS_Semaphore_t          *Semaphore,
                                      unsigned int             Timeout);
void          OS_SemaphoreSignal(OS_Semaphore_t          *Semaphore);

// --------------------------------------------------------------
//      Mutex functions

OS_Status_t   OS_InitializeMutex(OS_Mutex_t       *Mutex);
OS_Status_t   OS_TerminateMutex(OS_Mutex_t        *Mutex);
void          OS_LockMutex(OS_Mutex_t             *Mutex);
void          OS_UnLockMutex(OS_Mutex_t           *Mutex);
int           OS_MutexIsLocked(OS_Mutex_t         *Mutex);

#define OS_LockMutexNested(Mutex, NestingLevel) OS_LockMutex(Mutex)
#define OS_AssertMutexHeld(Mutex)
#define OS_AssertMutexNotHeld(Mutex)

// --------------------------------------------------------------
//      RtMutex functions

// --------------------------------------------------------------
//      SpinLock functions

// Lockdep requires the key to be statically allocated so must be a macro..
#define OS_InitializeSpinLock(SpinLock) ( { \
    static unsigned int __key; \
    __OS_InitializeSpinLock(SpinLock, &__key, #SpinLock); \
} )
OS_Status_t   __OS_InitializeSpinLock(OS_SpinLock_t  *SpinLock, unsigned int *Key, const char *Name);
OS_Status_t   OS_TerminateSpinLock(OS_SpinLock_t  *SpinLock);
void          OS_LockSpinLock(OS_SpinLock_t  *SpinLock);
void          OS_UnLockSpinLock(OS_SpinLock_t  *SpinLock);
void          OS_LockSpinLockIRQSave(OS_SpinLock_t  *SpinLock);
void          OS_UnLockSpinLockIRQRestore(OS_SpinLock_t  *SpinLock);
int           OS_SpinLockIsLocked(OS_SpinLock_t  *SpinLock);

// --------------------------------------------------------------
//      RwLock functions

void    __OS_InitializeRwLock(OS_RwLock_t  *Lock, unsigned int *Key, const char *Name);
void    OS_TerminateRwLock(OS_RwLock_t     *Lock);
void    OS_LockRead(OS_RwLock_t            *Lock);
void    OS_LockWrite(OS_RwLock_t           *Lock);
void    OS_UnLockRead(OS_RwLock_t          *Lock);
void    OS_UnLockWrite(OS_RwLock_t         *Lock);

// --------------------------------------------------------------
//      Memory Barriers functions

#define          OS_Smp_Mb(void) while(0)

// --------------------------------------------------------------
//      Event functions

OS_Status_t   OS_InitializeEvent(OS_Event_t             *Event);
OS_Status_t   OS_WaitForEvent(OS_Event_t                *Event,
                              OS_Timeout_t               Timeout);
OS_Status_t   OS_WaitForEventInterruptible(OS_Event_t   *Event,
                                           OS_Timeout_t  Timeout);
bool          OS_TestEventSet(OS_Event_t          *Event);
void          OS_SetEvent(OS_Event_t              *Event);
void          OS_SetEventInterruptible(OS_Event_t *Event);
void          OS_ResetEvent(OS_Event_t            *Event);
void          OS_ReInitializeEvent(OS_Event_t     *Event);
OS_Status_t   OS_TerminateEvent(OS_Event_t        *Event);

// --------------------------------------------------------------
//      Thread functions

OS_Status_t   OS_CreateThread(OS_Thread_t         *Thread,
                              OS_TaskEntry_t       TaskEntry,
                              OS_TaskParam_t       Parameter,
                              const OS_TaskDesc_t *TaskDesc);
void          OS_TerminateThread(void);
char         *OS_ThreadName(void);
unsigned int  OS_ThreadID(void);
OS_Status_t   OS_SetSchedAndAffinity(const OS_TaskDesc_t *TaskDesc);

// --------------------------------------------------------------
//      Signal Interactions

int           OS_SignalPending(void);

// --------------------------------------------------------------
//      Time functions

unsigned int  OS_GetTimeInSeconds(void);
unsigned int  OS_GetTimeInMilliSeconds(void);
unsigned long long  OS_GetTimeInMicroSeconds(void);
void          OS_SleepMilliSeconds(unsigned int           Value);

// --------------------------------------------------------------
//      Tunable functions

void          OS_RegisterTuneable(const char             *Name,
                                  unsigned int           *Address);
void	      OS_RegisterStringTuneable(const char *Name,
                                        char *Buffer,
                                        size_t MaxSize,
                                        void (*OnChange)(void *),
                                        void *OnChangeData);
void         *OS_RegisterTuneableDir(const char          *Name,
                                     void                *Parent);
void          OS_UnregisterTuneable(const char           *Name);

// ----------------------------------------------------------------------------------------
//      Memory

void OS_Dump_MemCheckCounters(const char *str);

int OS_SafeRead8(const void *Addr, uint8_t *Result);
int OS_SafeRead32(const void *Addr, uint32_t *Result);

char *OS_StrDup(const char *String);

void *__builtin_new(size_t size);
void *__builtin_vec_new(size_t size);
void __builtin_delete(void *ptr);
void __builtin_vec_delete(void *ptr);

size_t OS_CaptureStackTrace(unsigned long *Frames, unsigned int MaxFrames, unsigned int Skip);

#ifdef __cplusplus
}
#endif

#endif
