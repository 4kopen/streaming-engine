/************************************************************************
Copyright (C) 2003-2015 STMicroelectronics. All Rights Reserved.

This file is part of the Streaming Engine.

Streaming Engine is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License version 2 as
published by the Free Software Foundation.

Streaming Engine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with Streaming Engine; see the file COPYING.  If not, write to the Free
Software Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307,
USA.

The Streaming Engine Library may alternatively be licensed under a
proprietary license from ST.
************************************************************************/

#ifndef H_SE_BASE
#define H_SE_BASE

/* --- Useful macro's --- */

/*
 * Maximum and minimum of two values.
 *
 * Player2 has historically defined internally min() and max() as macros that
 * perform no type-checking whatsoever.  For example, the following call would
 * confusingly return the left-hand-side because of C integer promotion rules:
 *
 *	max((int)-1, (unsigned int)1) == (unsigned)-1
 *
 * In contrast, the Linux kernel uses some GNU magic to make these macros
 * type-safe.
 *
 * There is another issue with min() and max() preventing from including some SE
 * sources files.  SE parts that use placement new must include <new>.  Failing
 * this unit tests do not build.  <new> includes <bits/c++config.h> which
 * #undef's min() and max() because they collide with ISO C++ APIs.  The
 * solution is to mimic ISO C++ and define min() and max() as (type-safe)
 * template functions instead.
 *
 * In an ideal world, SE min() and max() would be made type-safe and turned into
 * template functions for C++ code but as they are widely used this is not a
 * trivial task. They are therefore left as-is for the time being but
 * deprecated.  New code should use Min() and Max() instead.
 *
 * Min() and Max() are defined as template functions for C++ and type-safe
 * macros for C.  Using language-specific implementations is not ideal but
 * defining Min() and Max() as macro for C++ clashes with Google Mock.
 */

#undef min
/// @deprecated: use Min()
#define min( a,b )                      (((a)<(b)) ? (a) : (b))

#undef max
/// @deprecated: use Max()
#define max( a,b )                      (((a)<(b)) ? (b) : (a))

#ifdef __cplusplus

template <class T>
const T& Min(const T& a, const T& b)
{
	return (a < b) ? a : b;
}

template <class T>
const T& Max(const T& a, const T& b)
{
	return (a < b) ? b : a;
}

#else /* __cplusplus */

#undef Min
#define Min(x, y) ({				\
	__typeof(x) _min1 = (x);		\
	__typeof(y) _min2 = (y);		\
	(void) (&_min1 == &_min2);		\
	_min1 < _min2 ? _min1 : _min2; })

#undef Max
#define Max(x, y) ({				\
	__typeof(x) _max1 = (x);		\
	__typeof(y) _max2 = (y);		\
	(void) (&_max1 == &_max2);		\
	_max1 > _max2 ? _max1 : _max2; })

#endif /* __cplusplus */

#undef inrange
#define inrange( val, lower, upper )    (((val)>=(lower)) && ((val)<=(upper)))

#undef abs32
#define abs32(x) ({        \
  int32_t __x = (x);           \
  (__x < 0) ? -__x : __x;  \
})

#undef abs64
#define abs64(x) ({        \
  int64_t __x = (x);           \
  (__x < 0) ? -__x : __x;  \
})

#undef samesign
#define samesign( a, b )    ((a)*(b) >= 0)

#undef Clamp
#define Clamp( v, l, u )                {if( v < l ) v = l; else if( v > u ) v = u;}

/*
 * Number of elements in array.
 * TODO(theryn): The kernel uses gcc magic to detect pointer arguments but
 * unfortunately this magic is inoperant in c++ land.
 */
#ifndef ARRAY_SIZE
#define ARRAY_SIZE(a)                   (sizeof(a) / sizeof((a)[0]))
#endif

// Raise an error at compile-time if specified predicate is false.  Generate no
// runtime code.
//
// Usage:
//
// void Foo()
// {
//	COMPILE_ASSERT(sizeof(Bar_t) >= 2);
//	...
// }
#undef COMPILE_ASSERT
#define COMPILE_ASSERT(expr) do { \
	extern char compile_assert[(expr) ? 1 : -1]; \
	(void)compile_assert; \
} while (0)

#ifdef __cplusplus

// A macro to disallow the copy constructor and operator= functions
// This should be used in the private: declarations for a class
#ifndef DISALLOW_COPY_AND_ASSIGN
#define DISALLOW_COPY_AND_ASSIGN(TypeName) \
  TypeName(const TypeName&);               \
  void operator=(const TypeName&)
#endif

#endif  // __cplusplus

#endif  // H_SE_BASE
