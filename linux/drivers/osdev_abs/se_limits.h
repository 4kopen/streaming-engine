/************************************************************************
Copyright (C) 2003-2014 STMicroelectronics. All Rights Reserved.

This file is part of the Streaming Engine.

Streaming Engine is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License version 2 as
published by the Free Software Foundation.

Streaming Engine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with Streaming Engine; see the file COPYING.  If not, write to the Free
Software Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307,
USA.

The Streaming Engine Library may alternatively be licensed under a
proprietary license from ST.
************************************************************************/

/*
 * Definitions of integer min and max constants for code not having access to
 * <limits.h> or <linux/kernel.h>.
 */

#ifndef H_SE_LIMITS
#define H_SE_LIMITS

#ifndef USHRT_MAX
#define USHRT_MAX   ((unsigned short)-1)
#endif

#ifndef SHRT_MAX
#define SHRT_MAX    ((short)(USHRT_MAX>>1))
#endif

#ifndef SHRT_MIN
#define SHRT_MIN    ((short)(-SHRT_MAX - 1))
#endif

#ifndef UINT_MAX
#define UINT_MAX    ((unsigned int)-1)
#endif

#ifndef INT_MAX
#define INT_MAX     ((int)(UINT_MAX>>1))
#endif

#ifndef INT_MIN
#define INT_MIN     (-INT_MAX - 1)
#endif

#ifndef ULONG_MAX
#define ULONG_MAX   ((unsigned long)-1)
#endif

#ifndef LONG_MAX
#define LONG_MAX    ((long)(ULONG_MAX>>1))
#endif

#ifndef LONG_MIN
#define LONG_MIN    (-LONG_MAX - 1)
#endif

#ifndef ULLONG_MAX
#define ULLONG_MAX  ((unsigned long long)-1)
#endif

#ifndef LLONG_MAX
#define LLONG_MAX   ((long long)(ULLONG_MAX>>1))
#endif

#ifndef LLONG_MIN
#define LLONG_MIN   (-LLONG_MAX - 1)
#endif

#ifndef SIZE_MAX
#define SIZE_MAX    ((size_t)-1)
#endif

#endif
