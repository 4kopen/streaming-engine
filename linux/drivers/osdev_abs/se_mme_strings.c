/************************************************************************
Copyright (C) 2003-2015 STMicroelectronics. All Rights Reserved.

This file is part of the Streaming Engine.

Streaming Engine is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License version 2 as
published by the Free Software Foundation.

Streaming Engine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with Streaming Engine; see the file COPYING.  If not, write to the Free
Software Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307,
USA.

The Streaming Engine Library may alternatively be licensed under a
proprietary license from ST.
************************************************************************/

#include <linux/module.h>

#include "se_mme_strings.h"

EXPORT_SYMBOL(StringifyMmeCommandState);
EXPORT_SYMBOL(StringifyMmeCommandEndType);
EXPORT_SYMBOL(StringifyMmeEvent);
EXPORT_SYMBOL(StringifyMmeError);
EXPORT_SYMBOL(StringifyMmeCmdCode);

const char *StringifyMmeCommandState(MME_CommandState_t cmd_state)
{
    switch (cmd_state)
    {
#define E(x) case x: return #x
        E(MME_COMMAND_IDLE);
        E(MME_COMMAND_PENDING);
        E(MME_COMMAND_EXECUTING);
        E(MME_COMMAND_COMPLETED);
        E(MME_COMMAND_FAILED);
#undef E
        default:
        return "unknown MME_CommandState_t";
    }
}

const char *StringifyMmeCommandEndType(MME_CommandEndType_t endType)
{
    switch (endType)
    {
#define E(x) case x: return #x
        E(MME_COMMAND_END_RETURN_NO_INFO);
        E(MME_COMMAND_END_RETURN_NOTIFY);
        E(MME_COMMAND_END_RETURN_WAKE);
#undef E
        default:
        return "unknown MME_CommandEndType_t";
    }
}

const char *StringifyMmeEvent(MME_Event_t event)
{
    switch (event)
    {
#define E(x) case x: return #x
        E(MME_COMMAND_COMPLETED_EVT);
        E(MME_DATA_UNDERFLOW_EVT);
        E(MME_NOT_ENOUGH_MEMORY_EVT);
        E(MME_NEW_COMMAND_EVT);
        E(MME_TRANSFORMER_TIMEOUT);
#undef E
        default:
        return "INVALID MME_Event_t";
    }
}

const char *StringifyMmeError(MME_ERROR aMmeError)
{
    switch (aMmeError)
    {
#define E(x) case x: return #x
        E(MME_SUCCESS);
        E(MME_DRIVER_NOT_INITIALIZED);
        E(MME_DRIVER_ALREADY_INITIALIZED);
        E(MME_NOMEM);
        E(MME_INVALID_TRANSPORT);
        E(MME_INVALID_HANDLE);
        E(MME_INVALID_ARGUMENT);
        E(MME_UNKNOWN_TRANSFORMER);
        E(MME_TRANSFORMER_NOT_RESPONDING);
        E(MME_HANDLES_STILL_OPEN);
        E(MME_COMMAND_STILL_EXECUTING);
        E(MME_COMMAND_ABORTED);
        E(MME_DATA_UNDERFLOW);
        E(MME_DATA_OVERFLOW);
        E(MME_TRANSFORM_DEFERRED);
        E(MME_SYSTEM_INTERRUPT);
        E(MME_ICS_ERROR);
        E(MME_INTERNAL_ERROR);
        E(MME_NOT_IMPLEMENTED);
        E(MME_COMMAND_TIMEOUT);
        E(MME_NOMIPS);
#undef E
        default:
        return "unknown MME_ERROR";
    }
}

const char *StringifyMmeCmdCode(MME_CommandCode_t aMmeCmdCode)
{
    switch (aMmeCmdCode)
    {
#define E(x) case x: return #x
        E(MME_SET_PARAMS);
        E(MME_TRANSFORM);
        E(MME_SEND_BUFFERS);
        E(MME_PING);
#undef E
        default:
        return "unknown MME_CommandCode_t";
    }
}
