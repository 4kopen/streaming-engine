/************************************************************************
Copyright (C) 2003-2014 STMicroelectronics. All Rights Reserved.

This file is part of the Streaming Engine.

Streaming Engine is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License version 2 as
published by the Free Software Foundation.

Streaming Engine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with Streaming Engine; see the file COPYING.  If not, write to the Free
Software Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307,
USA.

The Streaming Engine Library may alternatively be licensed under a
proprietary license from ST.
************************************************************************/
#include <linux/module.h>
#include <linux/platform_device.h>

#include "osinline.h"
#include "audio_codec_params.h"
#include "player_types.h"

// sysfs visibility is considered as debug info:
#if defined(CONFIG_DEBUG_FS) || defined(SDK2_ENABLE_ATTRIBUTES)
#define MODPARAMVISIBILITY S_IRUGO
#else
#define MODPARAMVISIBILITY 0
#endif

#define AUDIO_DECODER_DEFAULT_CORE_STRING_MAX_SIZE 8

#define AUDIO_DECODER_DEFAULT_CORE_DEFAULT "default"
#define AUDIO_DECODER_DEFAULT_CORE_AUDIO   "audio"
#define AUDIO_DECODER_DEFAULT_CORE_GP      "gp"
#define AUDIO_DECODER_DEFAULT_CORE_HOST    "host"
#define AUDIO_DECODER_DEFAULT_CORE_MS12    "ms12"

/*************************************************************************************************
 select default audio decode core
**************************************************************************************************/
static char audioDecoderDefaultCore[AUDIO_DECODER_DEFAULT_CORE_STRING_MAX_SIZE] = AUDIO_DECODER_DEFAULT_CORE_DEFAULT;
module_param_string(audioDecoderDefaultCore, audioDecoderDefaultCore, AUDIO_DECODER_DEFAULT_CORE_STRING_MAX_SIZE,
                    MODPARAMVISIBILITY);
MODULE_PARM_DESC(audioDecoderDefaultCore,
                 "Default strategy of resource allocation for audio decoders [\"default\" = alternate core,  \"audio\", \"gp\", \"host\"]");

/*
 get default mapping strategy policy
*/
typedef struct audioCodecMapping {
	char                *mapName;
	const unsigned int   policy;
} audioCodecMapping_t;

static audioCodecMapping_t audioCodecMap[] = {
	{ .policy = PolicyValueCpuSelectionDefault, .mapName = AUDIO_DECODER_DEFAULT_CORE_DEFAULT},
	{ .policy = PolicyValueCpuSelectionAudio,   .mapName = AUDIO_DECODER_DEFAULT_CORE_AUDIO  },
	{ .policy = PolicyValueCpuSelectionGP,      .mapName = AUDIO_DECODER_DEFAULT_CORE_GP     },
	{ .policy = PolicyValueCpuSelectionHost,    .mapName = AUDIO_DECODER_DEFAULT_CORE_HOST   },
	{ .policy = PolicyValueCpuSelectionHost,    .mapName = AUDIO_DECODER_DEFAULT_CORE_MS12   },
};

int ModuleParameter_PolicyValueCpuSelection(void)
{
	unsigned int m;
	unsigned int n = ARRAY_SIZE(audioCodecMap);

	for (m = 0; m < n; m++) {
		if (strncmp(audioCodecMap[m].mapName, audioDecoderDefaultCore, AUDIO_DECODER_DEFAULT_CORE_STRING_MAX_SIZE) == 0) {
			return audioCodecMap[m].policy;
		}
	}

	return PolicyValueCpuSelectionDefault;
}

