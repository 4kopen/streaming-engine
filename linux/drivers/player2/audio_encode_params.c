/************************************************************************
Copyright (C) 2003-2016 STMicroelectronics. All Rights Reserved.

This file is part of the Streaming Engine.

Streaming Engine is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License version 2 as
published by the Free Software Foundation.

Streaming Engine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with Streaming Engine; see the file COPYING.  If not, write to the Free
Software Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307,
USA.

The Streaming Engine Library may alternatively be licensed under a
proprietary license from ST.
************************************************************************/
#include <linux/module.h>
#include <linux/platform_device.h>

#include "player_types.h"
#include "audio_encode_params.h"

// sysfs visibility is considered as debug info:
#if defined(CONFIG_DEBUG_FS) || defined(SDK2_ENABLE_ATTRIBUTES)
#define MODPARAMVISIBILITY S_IRUGO
#else
#define MODPARAMVISIBILITY 0
#endif

#define AUDIO_ENCODER_DEFAULT_CORE_STRING_MAX_SIZE 8

#define AUDIO_ENCODER_DEFAULT_CORE_DEFAULT "default"
#define AUDIO_ENCODER_DEFAULT_CORE_AUDIO   "audio"
#define AUDIO_ENCODER_DEFAULT_CORE_GP      "gp"
#define AUDIO_ENCODER_DEFAULT_CORE_HOST    "host"

/*************************************************************************************************
 select default audio encode core
**************************************************************************************************/
static char audioEncoderDefaultCore[AUDIO_ENCODER_DEFAULT_CORE_STRING_MAX_SIZE] = AUDIO_ENCODER_DEFAULT_CORE_DEFAULT;

module_param_string(audioEncoderDefaultCore,
                    audioEncoderDefaultCore,
                    AUDIO_ENCODER_DEFAULT_CORE_STRING_MAX_SIZE,
                    MODPARAMVISIBILITY);

MODULE_PARM_DESC(audioEncoderDefaultCore,
                 "Default strategy of resource allocation for audio encoders [\"default\" = alternate core,  \"audio\", \"gp\", \"host\"]");

/*
 get default mapping strategy policy
*/
typedef struct audioEncodeMapping {
	const unsigned int  cpuSelection;
	const char          *mapName;
} audioEncodeMapping_t;

static audioEncodeMapping_t audioEncodeMap[] = {
	{ .cpuSelection = STM_SE_CTRL_VALUE_CPU_DEFAULT, .mapName = AUDIO_ENCODER_DEFAULT_CORE_DEFAULT},
	{ .cpuSelection = STM_SE_CTRL_VALUE_CPU_AUDIO,   .mapName = AUDIO_ENCODER_DEFAULT_CORE_AUDIO  },
	{ .cpuSelection = STM_SE_CTRL_VALUE_CPU_GP,      .mapName = AUDIO_ENCODER_DEFAULT_CORE_GP     },
	{ .cpuSelection = STM_SE_CTRL_VALUE_CPU_HOST,    .mapName = AUDIO_ENCODER_DEFAULT_CORE_HOST   },
};

int ModuleParameter_EncoderCpuSelection(void)
{
	unsigned int m;
	unsigned int n = ARRAY_SIZE(audioEncodeMap);

	for (m = 0; m < n; m++) {
		if (strncmp(audioEncodeMap[m].mapName, audioEncoderDefaultCore, AUDIO_ENCODER_DEFAULT_CORE_STRING_MAX_SIZE) == 0) {
			return audioEncodeMap[m].cpuSelection;
		}
	}
	return STM_SE_CTRL_VALUE_CPU_DEFAULT;
}

