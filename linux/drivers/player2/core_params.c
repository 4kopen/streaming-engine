/************************************************************************
Copyright (C) 2003-2015 STMicroelectronics. All Rights Reserved.

This file is part of the Streaming Engine.

Streaming Engine is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License version 2 as
published by the Free Software Foundation.

Streaming Engine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with Streaming Engine; see the file COPYING.  If not, write to the Free
Software Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307,
USA.

The Streaming Engine Library may alternatively be licensed under a
proprietary license from ST.
************************************************************************/

#include <linux/module.h>
#include <linux/platform_device.h>

#include "osinline.h"
#include "stm_se.h"
#include "report.h"
#include "player_threads.h"
#include "core_params.h"

// sysfs statistics are considered as debug info:
#if defined(CONFIG_DEBUG_FS) || defined(SDK2_ENABLE_ATTRIBUTES)
#define MODPARAMVISIBILITY S_IRUGO
#else
#define MODPARAMVISIBILITY 0
#endif

/*************************************************************************************************
 check if given device is available on SOC
**************************************************************************************************/
static int match_case(struct device *dev, void *data)
{
	char *name = data;
	if (strstr(dev_name(dev), name)) {
		return 1;
	} else {
		return 0;
	}
}
bool is_device_available(const char *hw_device_name)
{
	struct device *se_dev = NULL;
	bool ret = false;
	se_dev = bus_find_device(&platform_bus_type, NULL, (void *)hw_device_name, &match_case);
	if (se_dev) {
		if (se_dev->driver) {
			ret = true;
		}
	}
	if (!ret) {
		pr_debug("%s device is not available\n", hw_device_name);
	}
	return ret;
}
EXPORT_SYMBOL(is_device_available);

/*************************************************************************************************
 select trace groups init levels
    module init for trace groups levels
    either set a valid (positive) severity level per group,
    or set -1 to use default global group level
**************************************************************************************************/
static int trace_groups_levels_init[GROUP_LAST] = {
	severity_info,  //group_api
	-1,  //group_player
	-1,  //group_havana
	-1,  //group_buffer
	-1,  //group_decodebufferm
	-1,  //group_timestamps
	-1,  //group_misc
	-1,  //group_metadebug
	severity_info,  //group_event
	-1,  //group_collator_audio
	-1,  //group_collator_video
	-1,  //group_esprocessor
	-1,  //group_frameparser_audio
	-1,  //group_frameparser_video
	-1,  //group_decoder_audio
	-1,  //group_decoder_video
	-1,  //group_encoder_stream
	-1,  //group_encode_coordinator
	-1,  //group_encoder_audio_preproc
	-1,  //group_encoder_audio_coder
	-1,  //group_encoder_video_preproc
	-1,  //group_encoder_video_preproc_marker
	-1,  //group_encoder_video_coder
	-1,  //group_encoder_transporter
	-1,  //group_manifestor_audio_ksound
	-1,  //group_manifestor_video_stmfb
	-1,  //group_manifestor_audio_grab
	-1,  //group_manifestor_video_grab
	-1,  //group_manifestor_audio_encode
	-1,  //group_manifestor_video_encode
	-1,  //group_frc
	-1,  //group_output_timer
	-1,  //group_avsync
	-1,  //group_audio_reader
	-1,  //group_audio_player
	-1,  //group_audio_generator
	-1,  //group_mixer
	-1,  //group_se_pipeline
	severity_fatal,  //group_perf_audio_decoder
	severity_fatal,  //group_perf_audio_encoder
	severity_fatal,  //group_perf_audio_pp
	severity_fatal,  //group_perf_audio_mixer
	severity_fatal,  //group_perf_video_decoder
	-1,  //group_interpolation
	severity_fatal,  //group_dump_stack
};
module_param_array(trace_groups_levels_init, int, NULL, S_IRUGO); // no sysfs entry, since updated through debugfs
MODULE_PARM_DESC(trace_groups_levels_init, "Trace group levels to be activated at the init of the module");

/*************************************************************************************************
 select trace global level
**************************************************************************************************/
static int trace_global_level  = severity_info;
module_param(trace_global_level, int, S_IRUGO); // no sysfs entry, since updated through debugfs
MODULE_PARM_DESC(trace_global_level, "Trace global default level to be activated at the init of the module");

/*************************************************************************************************
 select trace backend
**************************************************************************************************/
static int trace_backend  = TRACE_BACKEND_DEFAULT;
module_param(trace_backend, int, S_IRUGO); // no sysfs entry, since updated through debugfs
MODULE_PARM_DESC(trace_backend,
                 "Trace backend to be activated at the init of the module [0:KERNEL 1:FTRACE 2:KPTRACE]");

/*************************************************************************************************
 init/terminate report trace framework
**************************************************************************************************/
void init_report_trace_levels(void)
{
	report_init(trace_groups_levels_init, trace_global_level, trace_backend);
}

void terminate_report(void)
{
	report_term();
}

/*************************************************************************************************
 select thread parameters: policy, priority, affinity bitmap
**************************************************************************************************/
// WARNING: keep order in sync with tasks_desc_e indexes
static int thpolprio_inits[][3] = {
	{ -1, -1, 0 }, // SE_TASK_AUDIO_CTOP
	{ -1, -1, 0 }, // SE_TASK_AUDIO_PTOD
	{ -1, -1, 0 }, // SE_TASK_AUDIO_DTOM
	{ -1, -1, 0 }, // SE_TASK_AUDIO_MPost

	{ -1, -1, 0 }, // SE_TASK_VIDEO_CTOP
	{ -1, -1, 0 }, // SE_TASK_VIDEO_PTOD
	{ -1, -1, 0 }, // SE_TASK_VIDEO_DTOM
	{ -1, -1, 0 }, // SE_TASK_VIDEO_MPost

	{ -1, -1, 0 }, // SE_TASK_VIDEO_H264INT
	{ -1, -1, 0 }, // SE_TASK_VIDEO_HEVCINT
	{ -1, -1, 0 }, // SE_TASK_VIDEO_AVSPINT

	{ -1, -1, 0 }, // SE_TASK_AUDIO_READER
	{ -1, -1, 0 }, // SE_TASK_AUDIO_STREAMT
	{ -1, -1, 0 }, // SE_TASK_AUDIO_MIXER
	{ -1, -1, 0 }, // SE_TASK_AUDIO_BCAST_MIXER
	{ -1, -1, 0 }, // SE_TASK_AUDIO_BYPASS_MIXER

	{ -1, -1, 0 }, // SE_TASK_MANIF_COORD
	{ -1, -1, 0 }, // SE_TASK_MANIF_BRSRCGRAB
	{ -1, -1, 0 }, // SE_TASK_MANIF_BRCAPTURE

	{ -1, -1, 0 }, // SE_TASK_PLAYBACK_DRAIN

	{ -1, -1, 0 }, // SE_TASK_ENCOD_AUDITOP
	{ -1, -1, 0 }, // SE_TASK_ENCOD_AUDPTOC
	{ -1, -1, 0 }, // SE_TASK_ENCOD_AUDCTOO

	{ -1, -1, 0 }, // SE_TASK_ENCOD_VIDITOP
	{ -1, -1, 0 }, // SE_TASK_ENCOD_VIDPTOC
	{ -1, -1, 0 }, // SE_TASK_ENCOD_VIDCTOO

	{ -1, -1, 0 }, // SE_TASK_ENCOD_COORD

	{ -1, -1, 0 }, // SE_TASK_OTHER
};

module_param_array_named(thread_SE_Aud_CtoP,       thpolprio_inits[SE_TASK_AUDIO_CTOP], int, NULL,
                         MODPARAMVISIBILITY);
module_param_array_named(thread_SE_Aud_PtoD,       thpolprio_inits[SE_TASK_AUDIO_PTOD], int, NULL,
                         MODPARAMVISIBILITY);
module_param_array_named(thread_SE_Aud_DtoM,       thpolprio_inits[SE_TASK_AUDIO_DTOM], int, NULL,
                         MODPARAMVISIBILITY);
module_param_array_named(thread_SE_Aud_MPost,      thpolprio_inits[SE_TASK_AUDIO_MPost], int, NULL,
                         MODPARAMVISIBILITY);
module_param_array_named(thread_SE_Vid_CtoP,       thpolprio_inits[SE_TASK_VIDEO_CTOP], int, NULL,
                         MODPARAMVISIBILITY);
module_param_array_named(thread_SE_Vid_PtoD,       thpolprio_inits[SE_TASK_VIDEO_PTOD], int, NULL,
                         MODPARAMVISIBILITY);
module_param_array_named(thread_SE_Vid_DtoM,       thpolprio_inits[SE_TASK_VIDEO_DTOM], int, NULL,
                         MODPARAMVISIBILITY);
module_param_array_named(thread_SE_Vid_MPost,      thpolprio_inits[SE_TASK_VIDEO_MPost], int, NULL,
                         MODPARAMVISIBILITY);
module_param_array_named(thread_SE_Vid_H264Int,    thpolprio_inits[SE_TASK_VIDEO_H264INT], int, NULL,
                         MODPARAMVISIBILITY);
module_param_array_named(thread_SE_Vid_HevcInt,    thpolprio_inits[SE_TASK_VIDEO_HEVCINT], int, NULL,
                         MODPARAMVISIBILITY);
module_param_array_named(thread_SE_Vid_AvspInt,    thpolprio_inits[SE_TASK_VIDEO_AVSPINT], int, NULL,
                         MODPARAMVISIBILITY);
module_param_array_named(thread_SE_Aud_Reader,     thpolprio_inits[SE_TASK_AUDIO_READER], int, NULL,
                         MODPARAMVISIBILITY);
module_param_array_named(thread_SE_Aud_StreamT,    thpolprio_inits[SE_TASK_AUDIO_STREAMT], int, NULL,
                         MODPARAMVISIBILITY);
module_param_array_named(thread_SE_Aud_Mixer,      thpolprio_inits[SE_TASK_AUDIO_MIXER], int, NULL,
                         MODPARAMVISIBILITY);
module_param_array_named(thread_SE_Aud_BcastMixer, thpolprio_inits[SE_TASK_AUDIO_BCAST_MIXER], int, NULL,
                         MODPARAMVISIBILITY);
module_param_array_named(thread_SE_Aud_Bypass,     thpolprio_inits[SE_TASK_AUDIO_BYPASS_MIXER], int, NULL,
                         MODPARAMVISIBILITY);
module_param_array_named(thread_SE_Man_Coord,      thpolprio_inits[SE_TASK_MANIF_COORD], int, NULL,
                         MODPARAMVISIBILITY);
module_param_array_named(thread_SE_Man_BrSrcGrab,  thpolprio_inits[SE_TASK_MANIF_BRSRCGRAB], int, NULL,
                         MODPARAMVISIBILITY);
module_param_array_named(thread_SE_Man_BrCapture,  thpolprio_inits[SE_TASK_MANIF_BRCAPTURE], int, NULL,
                         MODPARAMVISIBILITY);
module_param_array_named(thread_SE_Playbck_Drain,  thpolprio_inits[SE_TASK_PLAYBACK_DRAIN], int, NULL,
                         MODPARAMVISIBILITY);
module_param_array_named(thread_SE_EncAud_ItoP,    thpolprio_inits[SE_TASK_ENCOD_AUDITOP], int, NULL,
                         MODPARAMVISIBILITY);
module_param_array_named(thread_SE_EncAud_PtoC,    thpolprio_inits[SE_TASK_ENCOD_AUDPTOC], int, NULL,
                         MODPARAMVISIBILITY);
module_param_array_named(thread_SE_EncAud_CtoO,    thpolprio_inits[SE_TASK_ENCOD_AUDCTOO], int, NULL,
                         MODPARAMVISIBILITY);
module_param_array_named(thread_SE_EncVid_ItoP,    thpolprio_inits[SE_TASK_ENCOD_VIDITOP], int, NULL,
                         MODPARAMVISIBILITY);
module_param_array_named(thread_SE_EncVid_PtoC,    thpolprio_inits[SE_TASK_ENCOD_VIDPTOC], int, NULL,
                         MODPARAMVISIBILITY);
module_param_array_named(thread_SE_EncVid_CtoO,    thpolprio_inits[SE_TASK_ENCOD_VIDCTOO], int, NULL,
                         MODPARAMVISIBILITY);
module_param_array_named(thread_SE_Encod_Coord,    thpolprio_inits[SE_TASK_ENCOD_COORD], int, NULL,
                         MODPARAMVISIBILITY);
module_param_array_named(thread_SE_Other,          thpolprio_inits[SE_TASK_OTHER], int, NULL,
                         MODPARAMVISIBILITY);
MODULE_PARM_DESC(thread_SE_xyz,
                 "SE threads parameters: policy (if different from -1), priority (if different from -1), affinity_mask");

/*************************************************************************************************
 init thread parameters
**************************************************************************************************/
void init_thread_parameters(void)
{
	int i;

	/* update player_tasks_desc if module parameter provided,
	   else update thpolprio_inits (for debug print in sysfs) */
	for (i = 0; i <= SE_TASK_OTHER; i++) {
		if (thpolprio_inits[i][0] != -1) {
			player_tasks_desc[i].policy = thpolprio_inits[i][0];
		} else {
			thpolprio_inits[i][0] = player_tasks_desc[i].policy;
		}

		if (thpolprio_inits[i][1] != -1) {
			player_tasks_desc[i].priority = thpolprio_inits[i][1];
		} else {
			thpolprio_inits[i][1] = player_tasks_desc[i].priority;
		}

		if (thpolprio_inits[i][2] != 0) {
			player_tasks_desc[i].affinity_mask = thpolprio_inits[i][2];
		} else {
			thpolprio_inits[i][2] = player_tasks_desc[i].affinity_mask;
		}

		pr_info("%s pol:%d prio:%d aff:%d\n", player_tasks_desc[i].name,
		        player_tasks_desc[i].policy, player_tasks_desc[i].priority, player_tasks_desc[i].affinity_mask);
	}
}

