/************************************************************************
Copyright (C) 2003-2015 STMicroelectronics. All Rights Reserved.

This file is part of the Streaming Engine.

Streaming Engine is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License version 2 as
published by the Free Software Foundation.

Streaming Engine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with Streaming Engine; see the file COPYING.  If not, write to the Free
Software Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307,
USA.

The Streaming Engine Library may alternatively be licensed under a
proprietary license from ST.
************************************************************************/

#ifndef H_CORE_PARAMS
#define H_CORE_PARAMS

#ifdef __cplusplus
extern "C" {
#endif

#define DEVICE_NOT_FOUND false
bool is_device_available(const char *hw_device_name);

void init_report_trace_levels(void);
void terminate_report(void);

void init_thread_parameters(void);

#ifdef __cplusplus
}
#endif

#endif /* H_CORE_PARAMS */
