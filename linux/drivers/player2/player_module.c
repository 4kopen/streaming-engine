/************************************************************************
Copyright (C) 2003-2014 STMicroelectronics. All Rights Reserved.

This file is part of the Streaming Engine.

Streaming Engine is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License version 2 as
published by the Free Software Foundation.

Streaming Engine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with Streaming Engine; see the file COPYING.  If not, write to the Free
Software Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307,
USA.

The Streaming Engine Library may alternatively be licensed under a
proprietary license from ST.
************************************************************************/

#include <linux/sched.h>
#include <linux/syscalls.h>
#include <linux/module.h>
#include <linux/platform_device.h>

#include <linux/suspend.h>
#include <linux/pm.h>
#include <linux/pm_runtime.h>

#include "player_version.h"

#include "osinline.h"
#include "stm_se.h"
#include "audio_mixer_params.h"
#include "core_params.h"

MODULE_DESCRIPTION("Streaming Engine sw device driver");
MODULE_AUTHOR("STMicroelectronics");
MODULE_VERSION(PLAYER_VERSION);
MODULE_LICENSE("GPL");

extern int SysfsInit(void);
extern void SysfsDelete(void);

// SE device driver implementation
// This is a pure SW device (no physical H/W device is attached)
// Note: Used for SE power management, for HPS/CPS modes only.
// CONFIG_PM is the compilation flag used by Linux kernel to support power management
// CONFIG_PM_RUNTIME is the compilation flag used by Linux kernel to support active standby

#ifdef CONFIG_PM
// PM suspend callback
// For Gateway profile: called on Host Passive Standby entry (HPS entry)
// For STB profile: called on Controller Passive Standby entry (CPS entry)
static int stm_pm_se_suspend(struct device *dev)
{
	(void)dev; // warning removal
	pr_info("SE suspend\n");

	// Nothing to do here
	// All needed actions have already been taken in PM notifier callback, on PM_SUSPEND_PREPARE event
	return 0;
}

// PM resume callback
// For Gateway profile: called on Host Passive Standby exit (HPS exit)
// For STB profile: called on Controller Passive Standby exit (CPS exit)
static int stm_pm_se_resume(struct device *dev)
{
	(void)dev; // warning removal
	pr_info("SE resume\n");

	// Nothing to do here
	// All needed actions will be taken afterward in PM notifier callback, on PM_POST_SUSPEND event
	return 0;
}

// PM notifier callback
// This callback must be registered to do all needed actions at SE level for HPS/CPS
// On standby entry: it is called by PM framework before call to suspend callbacks of every module
// On standby exit:  it is called by PM framework after call to resume callbacks of every module
static int stm_pm_se_notifier_call(struct notifier_block *this, unsigned long event, void *ptr)
{
	int ErrorCode;

	(void)this; // warning removal
	(void)ptr; // warning removal

	switch (event) {
	case PM_SUSPEND_PREPARE:
		// HPS/CPS enter: this callback is called by the PM framework before calling the suspend callback of every module
		pr_info("SE notifier: PM_SUSPEND_PREPARE\n");

		// Ask SE to enter low power state
		ErrorCode = __stm_se_pm_low_power_enter();
		if (ErrorCode != 0) {
			// Error returned by SE, but this should not prevent the system entering low power
			pr_err("Error: %s SE low power enter failed\n", __func__);
		}
		break;

	case PM_POST_SUSPEND:
		// HPS/CPS exit: this callback is called by the PM framework after calling the resume callback of every module
		pr_info("SE notifier: PM_POST_SUSPEND\n");

		// Ask SE to exit low power state
		ErrorCode = __stm_se_pm_low_power_exit();
		if (ErrorCode != 0) {
			// Error returned by SE, but this should not prevent the system exiting low power
			pr_err("Error: %s SE low power exit failed\n", __func__);
		}
		break;

	default:
		break;
	}

	return NOTIFY_DONE;
}

// Power management callbacks
static struct notifier_block stm_pm_se_notifier = {
	.notifier_call = stm_pm_se_notifier_call,
};

static struct dev_pm_ops stm_pm_se_pm_ops = {
	.suspend = stm_pm_se_suspend,
	.resume  = stm_pm_se_resume,
// .runtime_suspend and .runtime_resume are not needed because Active Standby is not handled at SE level
};
#endif // CONFIG_PM

static void stm_se_release(struct device *dev)
{
	(void)dev; // warning removal
	// Nothing to do here, but needed to avoid WARNING at module unload;
}

// SE probe function (called on SE module load)
static int stm_se_probe(struct platform_device *pdev)
{
	int ret;

	/* init thread parameters from module parameters */
	init_thread_parameters();

	/* init report trace levels from module parameters */
	init_report_trace_levels();

	ret = SysfsInit();

	if (ret) {
		goto failsysfs;
	}

	// Set release function to avoid WARNING at module unload
	pdev->dev.release = stm_se_release;

	// Initialize sysfs entries for mixer transformer selection
	create_sysfs_mixer_selection(&pdev->dev);

	// Initialise the Streaming Engine through the STKPI
	ret = stm_se_init();
	if (ret) {
		goto failinit;
	}

#ifdef CONFIG_PM
	// Register with PM notifiers (for HPS/CPS support)
	register_pm_notifier(&stm_pm_se_notifier);
#endif

	OS_Dump_MemCheckCounters(__func__);
	pr_info("player2 probe done ok\n");

	return 0;

failinit:
	SysfsDelete();
failsysfs:
	terminate_report();
	pr_err("Error: %s player2 probe failed\n", __func__);
	return ret;
}

// SE remove function (called on SE module unload)
static int stm_se_remove(struct platform_device *pdev)
{
	OS_Dump_MemCheckCounters(__func__);

#ifdef CONFIG_PM
	// Unregister with PM notifiers
	unregister_pm_notifier(&stm_pm_se_notifier);
#endif

	// Finally terminate the SE
	stm_se_term();

	// Destroy sysfs entries for mixer transformer selection
	remove_sysfs_mixer_selection(&pdev->dev);

	SysfsDelete();

	terminate_report();

	OS_Dump_MemCheckCounters(__func__);
	pr_info("player2 remove done\n");

	return 0;
}

static struct platform_driver stm_se_driver = {
	.driver = {
		.name = "stm-se",
		.owner = THIS_MODULE,
#ifdef CONFIG_PM
		.pm = &stm_pm_se_pm_ops,
#endif
	},
	.probe = stm_se_probe,
	.remove = stm_se_remove,
};
static struct platform_device stm_se_device = {
	.name = "stm-se",
	.id = 0,
	.dev.platform_data = NULL,
};

static __init int player_load(void)
{
	platform_device_register(&stm_se_device);
	platform_driver_register(&stm_se_driver);
	return 0;
}

static void __exit player_unload(void)
{
	platform_driver_unregister(&stm_se_driver);
	platform_device_unregister(&stm_se_device);
}

module_init(player_load);
module_exit(player_unload);
//module_platform_driver(stm_se_driver);

// Exported SE APIs

//
EXPORT_SYMBOL(stm_se_set_control);
EXPORT_SYMBOL(stm_se_get_control);
EXPORT_SYMBOL(stm_se_get_compound_control);
EXPORT_SYMBOL(stm_se_set_error_handler);
//
EXPORT_SYMBOL(stm_se_playback_new);
EXPORT_SYMBOL(stm_se_playback_delete);
EXPORT_SYMBOL(stm_se_playback_set_control);
EXPORT_SYMBOL(stm_se_playback_get_control);
EXPORT_SYMBOL(stm_se_playback_set_speed);
EXPORT_SYMBOL(stm_se_playback_get_speed);
EXPORT_SYMBOL(stm_se_playback_set_native_time);
EXPORT_SYMBOL(stm_se_playback_set_clock_data_point);
EXPORT_SYMBOL(stm_se_playback_get_clock_data_point);

//
EXPORT_SYMBOL(stm_se_play_stream_new);
EXPORT_SYMBOL(stm_se_play_stream_delete);
EXPORT_SYMBOL(stm_se_play_stream_set_control);
EXPORT_SYMBOL(stm_se_play_stream_get_control);
EXPORT_SYMBOL(stm_se_play_stream_set_enable);
EXPORT_SYMBOL(stm_se_play_stream_get_enable);
EXPORT_SYMBOL(stm_se_play_stream_get_compound_control);
EXPORT_SYMBOL(stm_se_play_stream_set_compound_control);
EXPORT_SYMBOL(stm_se_play_stream_attach);
EXPORT_SYMBOL(stm_se_play_stream_attach_to_pad);
EXPORT_SYMBOL(stm_se_play_stream_detach);
EXPORT_SYMBOL(stm_se_play_stream_inject_data);
EXPORT_SYMBOL(stm_se_play_stream_inject_discontinuity);
EXPORT_SYMBOL(stm_se_play_stream_inject_discontinuity_mask);
EXPORT_SYMBOL(stm_se_play_stream_drain);
EXPORT_SYMBOL(stm_se_play_stream_step);
EXPORT_SYMBOL(stm_se_play_stream_switch);
EXPORT_SYMBOL(stm_se_play_stream_get_info);
EXPORT_SYMBOL(stm_se_play_stream_set_interval);
EXPORT_SYMBOL(stm_se_play_stream_poll_message);
EXPORT_SYMBOL(stm_se_play_stream_get_message);
EXPORT_SYMBOL(stm_se_play_stream_subscribe);
EXPORT_SYMBOL(stm_se_play_stream_unsubscribe);
EXPORT_SYMBOL(stm_se_play_stream_register_buffer_capture_callback);
EXPORT_SYMBOL(stm_se_play_stream_set_alarm);
EXPORT_SYMBOL(stm_se_play_stream_set_discard_trigger);
EXPORT_SYMBOL(stm_se_play_stream_reset_discard_triggers);
//
EXPORT_SYMBOL(stm_se_advanced_audio_mixer_new);
EXPORT_SYMBOL(stm_se_audio_mixer_new);
EXPORT_SYMBOL(stm_se_audio_mixer_delete);
EXPORT_SYMBOL(stm_se_audio_mixer_set_control);
EXPORT_SYMBOL(stm_se_audio_mixer_get_control);
EXPORT_SYMBOL(stm_se_audio_mixer_set_compound_control);
EXPORT_SYMBOL(stm_se_audio_mixer_get_compound_control);
EXPORT_SYMBOL(stm_se_audio_mixer_attach);
EXPORT_SYMBOL(stm_se_audio_mixer_detach);
//
EXPORT_SYMBOL(stm_se_component_set_module_parameters);
//
EXPORT_SYMBOL(stm_se_audio_generator_new);
EXPORT_SYMBOL(stm_se_audio_generator_delete);
EXPORT_SYMBOL(stm_se_audio_generator_attach);
EXPORT_SYMBOL(stm_se_audio_generator_detach);
EXPORT_SYMBOL(stm_se_audio_generator_get_compound_control);
EXPORT_SYMBOL(stm_se_audio_generator_set_compound_control);
EXPORT_SYMBOL(stm_se_audio_generator_get_control);
EXPORT_SYMBOL(stm_se_audio_generator_set_control);
EXPORT_SYMBOL(stm_se_audio_generator_get_info);
EXPORT_SYMBOL(stm_se_audio_generator_commit);
EXPORT_SYMBOL(stm_se_audio_generator_start);
EXPORT_SYMBOL(stm_se_audio_generator_stop);
//
EXPORT_SYMBOL(stm_se_audio_reader_new);
EXPORT_SYMBOL(stm_se_audio_reader_delete);
EXPORT_SYMBOL(stm_se_audio_reader_attach);
EXPORT_SYMBOL(stm_se_audio_reader_detach);
EXPORT_SYMBOL(stm_se_audio_reader_get_compound_control);
EXPORT_SYMBOL(stm_se_audio_reader_set_compound_control);
EXPORT_SYMBOL(stm_se_audio_reader_get_control);
EXPORT_SYMBOL(stm_se_audio_reader_set_control);
//
EXPORT_SYMBOL(stm_se_audio_player_new);
EXPORT_SYMBOL(stm_se_audio_player_delete);
EXPORT_SYMBOL(stm_se_audio_player_set_control);
EXPORT_SYMBOL(stm_se_audio_player_get_control);
EXPORT_SYMBOL(stm_se_audio_player_set_compound_control);
EXPORT_SYMBOL(stm_se_audio_player_get_compound_control);
//
EXPORT_SYMBOL(stm_se_encode_new);
EXPORT_SYMBOL(stm_se_encode_delete);
EXPORT_SYMBOL(stm_se_encode_get_control);
EXPORT_SYMBOL(stm_se_encode_set_control);
//
EXPORT_SYMBOL(stm_se_encode_stream_new);
EXPORT_SYMBOL(stm_se_encode_stream_delete);
EXPORT_SYMBOL(stm_se_encode_stream_attach);
EXPORT_SYMBOL(stm_se_encode_stream_detach);
EXPORT_SYMBOL(stm_se_encode_stream_flush);
EXPORT_SYMBOL(stm_se_encode_stream_get_control);
EXPORT_SYMBOL(stm_se_encode_stream_set_control);
EXPORT_SYMBOL(stm_se_encode_stream_get_compound_control);
EXPORT_SYMBOL(stm_se_encode_stream_set_compound_control);
EXPORT_SYMBOL(stm_se_encode_stream_drain);
EXPORT_SYMBOL(stm_se_encode_stream_inject_frame);
EXPORT_SYMBOL(stm_se_encode_stream_inject_uframe);
EXPORT_SYMBOL(stm_se_encode_stream_inject_discontinuity);
//
EXPORT_SYMBOL(__stm_se_play_stream_get_statistics);
EXPORT_SYMBOL(__stm_se_play_stream_reset_statistics);
EXPORT_SYMBOL(__stm_se_play_stream_get_attributes);
EXPORT_SYMBOL(__stm_se_play_stream_reset_attributes);
//
EXPORT_SYMBOL(__stm_se_pm_low_power_enter);
EXPORT_SYMBOL(__stm_se_pm_low_power_exit);
