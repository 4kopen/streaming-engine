/************************************************************************
Copyright (C) 2003-2014 STMicroelectronics. All Rights Reserved.

This file is part of the Streaming Engine.

Streaming Engine is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License version 2 as
published by the Free Software Foundation.

Streaming Engine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with Streaming Engine; see the file COPYING.  If not, write to the Free
Software Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307,
USA.

The Streaming Engine Library may alternatively be licensed under a
proprietary license from ST.
************************************************************************/

#ifndef H_PLAYER_SYSFS
#define H_PLAYER_SYSFS

#ifdef __cplusplus
extern "C" {
#endif

int SysfsInit(void);

int PlaybackCreateSysfs(const char *Name, stm_se_playback_h Playback);
int PlaybackTerminateSysfs(stm_se_playback_h                Playback);

int StreamCreateSysfs(const char          *Name,
                      stm_se_playback_h    Playback,
                      stm_se_media_t       Media,
                      stm_se_play_stream_h Stream);
int StreamTerminateSysfs(stm_se_playback_h     Playback,
                         stm_se_play_stream_h  Stream);

int WrapperGetPlaybackStatisticsForSysfs(stm_se_playback_h    Playback,
                                         stm_se_play_stream_h Stream);

int MixerCreateSysfs(const char *Name, stm_se_audio_mixer_h Mixer);
int MixerTerminateSysfs(stm_se_audio_mixer_h                Mixer);
int WrapperGetMixerStatisticsForSysfs(stm_se_playback_h  Mixer);

int EncodeCreateSysfs(const char *Name, stm_se_encode_h Encode);
int EncodeTerminateSysfs(stm_se_encode_h                Encode);

int EncodeStreamCreateSysfs(const char                   *Name,
                            stm_se_encode_h               Encode,
                            stm_se_encode_stream_media_t  Media,
                            stm_se_encode_stream_h        Stream);
int EncodeStreamTerminateSysfs(stm_se_encode_h            Encode,
                               stm_se_encode_stream_h     Stream);

int WrapperGetEncodeStatisticsForSysfs(stm_se_encode_h Encode,
                                       stm_se_encode_stream_h EncodeStream);

#ifdef __cplusplus
}
#endif

#endif
