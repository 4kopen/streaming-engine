/************************************************************************
Copyright (C) 2003-2014 STMicroelectronics. All Rights Reserved.

This file is part of the Streaming Engine.

Streaming Engine is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License version 2 as
published by the Free Software Foundation.

Streaming Engine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with Streaming Engine; see the file COPYING.  If not, write to the Free
Software Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307,
USA.

The Streaming Engine Library may alternatively be licensed under a
proprietary license from ST.
************************************************************************/

#if __KERNEL__
#include "osdev_sched.h"
#else
#define SCHED_RR 0
#endif

#include "player_threads.h"

// tasks descriptions table: name, policy, prio
// WARNING: keep order in sync with tasks_desc_e indexes
// WARNING: max length is 16 (with terminating '\0')
OS_TaskDesc_t player_tasks_desc[] = {
	{ "SE-Aud-CtoP"  , SCHED_RR, OS_MID_PRIORITY +  8, 0  },   // SE_TASK_AUDIO_CTOP
	{ "SE-Aud-PtoD"  , SCHED_RR, OS_MID_PRIORITY +  8, 0  },   // SE_TASK_AUDIO_PTOD
	{ "SE-Aud-DtoM"  , SCHED_RR, OS_MID_PRIORITY + 10, 0  },   // SE_TASK_AUDIO_DTOM
	{ "SE-Aud-MPost" , SCHED_RR, OS_MID_PRIORITY + 12, 0  },   // SE_TASK_AUDIO_MPost

	{ "SE-Vid-CtoP"  , SCHED_RR, OS_MID_PRIORITY +  8, 0  },   // SE_TASK_VIDEO_CTOP
	{ "SE-Vid-PtoD"  , SCHED_RR, OS_MID_PRIORITY +  8, 0  },   // SE_TASK_VIDEO_PTOD
	{ "SE-Vid-DtoM"  , SCHED_RR, OS_MID_PRIORITY + 10, 0  },   // SE_TASK_VIDEO_DTOM
	{ "SE-Vid-MPost" , SCHED_RR, OS_MID_PRIORITY + 12, 0  },   // SE_TASK_VIDEO_MPost

	{ "SE-Vid-H264Int"  , SCHED_RR, OS_MID_PRIORITY + 9, 0 }, // SE_TASK_VIDEO_H264INT
	{ "SE-Vid-HevcInt"  , SCHED_RR, OS_MID_PRIORITY + 9, 0 }, // SE_TASK_VIDEO_HEVCINT
	{ "SE-Vid-AvspInt"  , SCHED_RR, OS_MID_PRIORITY + 9, 0 }, // SE_TASK_VIDEO_AVSPINT

	{ "SE-Aud-Reader"   , SCHED_RR, OS_MID_PRIORITY + 14, 0 }, // SE_TASK_AUDIO_READER
	{ "SE-Aud-StreamT"  , SCHED_RR, OS_MID_PRIORITY +  9, 0 }, // SE_TASK_AUDIO_STREAMT
	{ "SE-Aud-Mixer"    , SCHED_RR, OS_MID_PRIORITY + 14, 0 }, // SE_TASK_AUDIO_MIXER
	{ "SE-Aud-BcstMix"  , SCHED_RR, OS_MID_PRIORITY + 14, 0 }, // SE_TASK_AUDIO_BCAST_MIXER
	{ "SE-Aud-Bypass"   , SCHED_RR, OS_MID_PRIORITY + 14, 0 }, // SE_TASK_AUDIO_BYPASS_MIXER

	{ "SE-Man-Coord"    , SCHED_RR, OS_MID_PRIORITY +  9, 0 }, // SE_TASK_MANIF_COORD
	{ "SE-Man-BSrcGrab" , SCHED_RR, OS_MID_PRIORITY +  0, 0 }, // SE_TASK_MANIF_BRSRCGRAB
	{ "SE-Man-BCapture" , SCHED_RR, OS_MID_PRIORITY +  0, 0 }, // SE_TASK_MANIF_BRCAPTURE

	{ "SE-Play-Drain"  , SCHED_RR, OS_MID_PRIORITY + 14, 0 },  // SE_TASK_PLAYBACK_DRAIN

	{ "SE-EncAud-ItoP", SCHED_RR, OS_MID_PRIORITY + 11, 0 },   // SE_TASK_ENCOD_AUDITOP
	{ "SE-EncAud-PtoC", SCHED_RR, OS_MID_PRIORITY +  8, 0 },   // SE_TASK_ENCOD_AUDPTOC
	{ "SE-EncAud-CtoO", SCHED_RR, OS_MID_PRIORITY +  9, 0 },   // SE_TASK_ENCOD_AUDCTOO

	{ "SE-EncVid-ItoP", SCHED_RR, OS_MID_PRIORITY + 11, 0 },   // SE_TASK_ENCOD_VIDITOP
	{ "SE-EncVid-PtoC", SCHED_RR, OS_MID_PRIORITY +  8, 0 },   // SE_TASK_ENCOD_VIDPTOC
	{ "SE-EncVid-CtoO", SCHED_RR, OS_MID_PRIORITY +  9, 0 },   // SE_TASK_ENCOD_VIDCTOO

	{ "SE-Encod-Coord", SCHED_RR, OS_MID_PRIORITY + 11, 0 },   // SE_TASK_ENCOD_COORD

	{ "SE-Other", SCHED_RR, OS_MID_PRIORITY + 0, 0 },          // SE_TASK_OTHER
};

