/************************************************************************
Copyright (C) 2003-2014 STMicroelectronics. All Rights Reserved.

This file is part of the Streaming Engine.

Streaming Engine is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License version 2 as
published by the Free Software Foundation.

Streaming Engine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with Streaming Engine; see the file COPYING.  If not, write to the Free
Software Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307,
USA.

The Streaming Engine Library may alternatively be licensed under a
proprietary license from ST.
************************************************************************/

#include <linux/module.h>
#include <linux/platform_device.h>

#include "osinline.h"
#include "stm_se.h"

// sysfs statistics are considered as debug info:
#if defined(CONFIG_DEBUG_FS) || defined(SDK2_ENABLE_ATTRIBUTES)
#define MODPARAMVISIBILITY S_IRUGO
#else
#define MODPARAMVISIBILITY 0
#endif

/*************************************************************************************************
 select coded buffer size (applicable only in auto memory profile mode)
**************************************************************************************************/
static int CodedBufferSizeKbyte = 0;
module_param(CodedBufferSizeKbyte, int, MODPARAMVISIBILITY);
MODULE_PARM_DESC(CodedBufferSizeKbyte, "input coded bit buffer size (in Kbytes) for video decoder");

/*
 get coded buffer size (applicable only in auto memory profile mode)
*/
int ModuleParameter_GetCodedBitBufferSize(void)
{
	//convert to kbyte to byte
	return CodedBufferSizeKbyte * 1024;
}

/*************************************************************************************************
 select display64Mbytes crossing support
**************************************************************************************************/
static char DisplaySupport64MbyteCrossing[2] = "N";
module_param_string(DisplaySupport64MbyteCrossing, DisplaySupport64MbyteCrossing, 2, MODPARAMVISIBILITY);
MODULE_PARM_DESC(DisplaySupport64MbyteCrossing, "display 64 Mbyte crossing support [\"default\" = N, \"Y\"]");

/*
 get display64Mbytes crossing support
*/
bool ModuleParameter_Support64MbyteCrossing(void)
{
	if (DisplaySupport64MbyteCrossing[0] == 'Y') { return true; }
	if (DisplaySupport64MbyteCrossing[0] == 'N') { return false; }
	pr_err("Error: Module parameter \"DisplaySupport64MbyteCrossing\" invalid value %s, support value [\"Y\",\"N\"]\n",
	       DisplaySupport64MbyteCrossing);
	return false;
}

/*************************************************************************************************
 select use of videocopybuffer: discarded in low memory platforms
**************************************************************************************************/
static char UseVideoCopyBuffer[2] = "Y";
module_param_string(UseVideoCopyBuffer, UseVideoCopyBuffer, 2, MODPARAMVISIBILITY);
MODULE_PARM_DESC(UseVideoCopyBuffer, "UseVideoCopyBuffer [\"default\" = Y, \"N\"]");

/*
 get use of videocopybuffer
*/
bool ModuleParameter_NoVideoCopyBuffer(void)
{
	if (UseVideoCopyBuffer[0] == 'Y') { return false; }
	if (UseVideoCopyBuffer[0] == 'N') { return true; }
	pr_err("Error: Module parameter \"UseVideoCopyBUffer\" invalid value %s, support value [\"Y\",\"N\"]\n",
	       UseVideoCopyBuffer);
	return false;
}

/*************************************************************************************************
 select blitter IP id
**************************************************************************************************/
static int BlitterId = 0;
module_param(BlitterId, int, MODPARAMVISIBILITY);
MODULE_PARM_DESC(BlitterId, "Blitter Id; default:0");

/*
 get blitter IP id
*/
bool ModuleParameter_BlitterId(void)
{
	return BlitterId;
}

/*************************************************************************************************
 select serialization mode:
    in order to lower bandwidth on memory bus,
    serialization of decoders running on different IP may be needed
**************************************************************************************************/
static int serialize_decoders = 0;
module_param(serialize_decoders, int, MODPARAMVISIBILITY);
MODULE_PARM_DESC(serialize_decoders, "Serialization of video decoders");

/*
 get serialization mode
*/
bool ModuleParameter_SerializeDecoders(void)
{
#ifdef SOC_CANNESWIFI
	return false;
#else
	return (serialize_decoders == 1);
#endif
}
