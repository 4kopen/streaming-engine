/*
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301,
 * USA.
 */
/************************************************************************
Copyright (C) 2003-2014 STMicroelectronics. All Rights Reserved.
************************************************************************/

#include <linux/clk.h>
#include <linux/pm_runtime.h>

#include "osdev_time.h"
#include "vp8_defs.h"
#include "dwl.h"
#include "vp8hard.h"

//#define DWL_DEBUG(fmt, args...) pr_info(__FILE__ ":%d: " fmt, __LINE__ , ## args)
#define DWL_DEBUG(fmt, args...)

int hx170_check_chip_id(struct hX170dwl_core *core)
{
	int ret = 0;

#ifdef CONFIG_PM_RUNTIME
	if (pm_runtime_get_sync(core->dev) < 0) {
		pr_err("Error: %s pm_runtime_get_sync failed\n", __func__);
		return VP8HWDEC_SYSTEM_ERROR;
	}
#endif
	if (hx170_clock_on(core)) {
		pr_err("Error: %s failed to enable clock\n", __func__);
#ifdef CONFIG_PM_RUNTIME
		pm_runtime_mark_last_busy(core->dev);
		pm_runtime_put(core->dev);
#endif
		return VP8HWDEC_SYSTEM_ERROR;
	}


	core->chip_id = *(core->regs + HX170_CHIP_ID_REG);

	hx170_clock_off(core);
#ifdef CONFIG_PM_RUNTIME
	pm_runtime_mark_last_busy(core->dev);
	pm_runtime_put(core->dev);
#endif

	switch (core->chip_id) {
	case HX170_IP_VERSION_V1:
	case HX170_IP_VERSION_V2:
	case HX170_IP_VERSION_V6:
		pr_info("%s: Chip ID: 0x%x\n", __func__, core->chip_id);
		break;

	default:
		pr_err("Error: %s Unknown chip ID: 0x%x\n", __func__, core->chip_id);
		break;
	}

	return ret;
}

int vp8_fuse_check(struct hX170dwl_core *core)
{
	int ret = 0;
	unsigned int fuseReg = 0;
	unsigned int vp8_support_fuse = 0;
#ifdef CONFIG_PM_RUNTIME
	if (pm_runtime_get_sync(core->dev) < 0) {
		pr_err("Error: %s pm_runtime_get_sync failed\n", __func__);
		return VP8HWDEC_SYSTEM_ERROR;
	}
#endif
	if (hx170_clock_on(core)) {
		pr_err("Error: %s failed to enable clock\n", __func__);
#ifdef CONFIG_PM_RUNTIME
		pm_runtime_mark_last_busy(core->dev);
		pm_runtime_put(core->dev);
#endif
		return VP8HWDEC_SYSTEM_ERROR;
	}

	fuseReg = *(core->regs + HX170DEC_FUSE_CFG);
	vp8_support_fuse = (fuseReg >> DWL_VP8_FUSE_E) & 0x01U;

	hx170_clock_off(core);
#ifdef CONFIG_PM_RUNTIME
	pm_runtime_mark_last_busy(core->dev);
	pm_runtime_put(core->dev);
#endif
	if (!vp8_support_fuse) {
		ret = -EINVAL;
		pr_err("Error: %s vp8 fuse disabled\n", __func__);
	}
	return ret;
}

int hx170_clock_on(struct hX170dwl_core *core)
{
	int ret;
	//pr_info("VP8: %s\n", __func__);
	atomic_inc(&core->task_nb);
	ret = clk_enable(core->clk);
	return ret;
}

void hx170_clock_off(struct hX170dwl_core *core)
{
	//pr_info("VP8: %s\n", __func__);
	clk_disable(core->clk);
	atomic_dec(&core->task_nb);
}

/*------------------------------------------------------------------------------
    Function name   : DWLReadAsicFuseStatus
    Description     : Read HW fuse configuration. Does not need a DWL instance to run

    Returns     : DWLHwFuseStatus_t * pHwFuseSts - structure with HW fuse configuration
------------------------------------------------------------------------------*/
void DWLReadAsicFuseStatus(struct hX170dwl_core *core, DWLHwFuseStatus_t *pHwFuseSts)
{
	unsigned int *AsicBaseAdress_Cp;
	unsigned int configReg;
	unsigned int fuseReg;
	unsigned int fuseRegPp;

	assert(core);
	memset(pHwFuseSts, 0, sizeof(*pHwFuseSts));

	AsicBaseAdress_Cp = core->regs;

	/* Decoder fuse configuration */
	fuseReg = AsicBaseAdress_Cp[HX170DEC_FUSE_CFG];

	pHwFuseSts->h264SupportFuse = (fuseReg >> DWL_H264_FUSE_E) & 0x01U;
	pHwFuseSts->mpeg4SupportFuse = (fuseReg >> DWL_MPEG4_FUSE_E) & 0x01U;
	pHwFuseSts->mpeg2SupportFuse = (fuseReg >> DWL_MPEG2_FUSE_E) & 0x01U;
	pHwFuseSts->sorensonSparkSupportFuse =
	        (fuseReg >> DWL_SORENSONSPARK_FUSE_E) & 0x01U;
	pHwFuseSts->jpegSupportFuse = (fuseReg >> DWL_JPEG_FUSE_E) & 0x01U;
	pHwFuseSts->vp6SupportFuse = (fuseReg >> DWL_VP6_FUSE_E) & 0x01U;
	pHwFuseSts->vc1SupportFuse = (fuseReg >> DWL_VC1_FUSE_E) & 0x01U;
	pHwFuseSts->jpegProgSupportFuse = (fuseReg >> DWL_PJPEG_FUSE_E) & 0x01U;
	pHwFuseSts->rvSupportFuse = (fuseReg >> DWL_RV_FUSE_E) & 0x01U;
	pHwFuseSts->avsSupportFuse = (fuseReg >> DWL_AVS_FUSE_E) & 0x01U;
	pHwFuseSts->vp7SupportFuse = (fuseReg >> DWL_VP7_FUSE_E) & 0x01U;
	pHwFuseSts->vp8SupportFuse = (fuseReg >> DWL_VP8_FUSE_E) & 0x01U;
	pHwFuseSts->customMpeg4SupportFuse = (fuseReg >> DWL_CUSTOM_MPEG4_FUSE_E) & 0x01U;
	pHwFuseSts->mvcSupportFuse = (fuseReg >> DWL_MVC_FUSE_E) & 0x01U;

	/* check max. decoder output width */
	/* Recommended by Soc Validation team inorder to be aligned with correct
	behavior on Soc - if one resolution fuse is blown, all higher resolution fuses
	must also be blown */
	pHwFuseSts->maxDecPicWidthFuse = 176;

	if (fuseReg & 0x1000U) {
		pHwFuseSts->maxDecPicWidthFuse = 352;
	}
	if (fuseReg & 0x2000U) {
		pHwFuseSts->maxDecPicWidthFuse = 720;
	}
	if (fuseReg & 0x4000U) {
		pHwFuseSts->maxDecPicWidthFuse = 1280;
	}
	if (fuseReg & 0x8000U) {
		pHwFuseSts->maxDecPicWidthFuse = 1920;
	}

	pHwFuseSts->refBufSupportFuse = (fuseReg >> DWL_REF_BUFF_FUSE_E) & 0x01U;

	/* Pp configuration */
	configReg = AsicBaseAdress_Cp[HX170PP_SYNTH_CFG];

	if ((configReg >> DWL_PP_E) & 0x01U) {
		/* Pp fuse configuration */
		fuseRegPp = AsicBaseAdress_Cp[HX170PP_FUSE_CFG];

		if ((fuseRegPp >> DWL_PP_FUSE_E) & 0x01U) {
			pHwFuseSts->ppSupportFuse = 1;

			/* check max. pp output width */
			if (fuseRegPp & 0x8000U) {
				pHwFuseSts->maxPpOutPicWidthFuse = 1920;
			} else if (fuseRegPp & 0x4000U) {
				pHwFuseSts->maxPpOutPicWidthFuse = 1280;
			} else if (fuseRegPp & 0x2000U) {
				pHwFuseSts->maxPpOutPicWidthFuse = 720;
			} else if (fuseRegPp & 0x1000U) {
				pHwFuseSts->maxPpOutPicWidthFuse = 352;
			}

			pHwFuseSts->ppConfigFuse = fuseRegPp;
		} else {
			pHwFuseSts->ppSupportFuse = 0;
			pHwFuseSts->maxPpOutPicWidthFuse = 0;
			pHwFuseSts->ppConfigFuse = 0;
		}
	}

}

/*--------------------------------------------------------------------------
    Function name   : DWLReadAsicConfig
    Description     : Read HW configuration.
----------------------------------------------------------------------------*/
void DWLReadAsicConfig(struct hX170dwl_core *core)
{
	unsigned int *AsicBaseAdress_Cp;
	unsigned int configReg;
	unsigned int asicID;
	struct hX170dwl_config  *pHwCfg;
	DWLHwFuseStatus_t hwFuseSts;

	assert(core);
	pHwCfg = &core->cfg;
	AsicBaseAdress_Cp = core->regs;

	memset(pHwCfg, 0, sizeof(*pHwCfg));

	/* check fuse status */
	DWLReadAsicFuseStatus(core, &hwFuseSts);

#ifdef CONFIG_PM_RUNTIME
	/* Should power on the vp8 core (currently only used to reinit core register) */
	if (pm_runtime_get_sync(core->dev) < 0) {
		pr_err("Error: %s pm_runtime_get_sync failed\n", __func__);
		return;
	}
#endif
	if (hx170_clock_on(core)) {
		pr_err("Error: %s failed to enable clock\n", __func__);
#ifdef CONFIG_PM_RUNTIME
		pm_runtime_mark_last_busy(core->dev);
		pm_runtime_put(core->dev);
#endif
	}


	/* Decoder configuration */
	configReg = AsicBaseAdress_Cp[HX170DEC_SYNTH_CFG];

	pHwCfg->h264Support = (configReg >> DWL_H264_E) & 0x3U;
	/* check jpeg */
	pHwCfg->jpegSupport = (configReg >> DWL_JPEG_E) & 0x01U;
	if (pHwCfg->jpegSupport && ((configReg >> DWL_PJPEG_E) & 0x01U)) {
		pHwCfg->jpegSupport = JPEG_PROGRESSIVE;
	}
	pHwCfg->mpeg4Support = (configReg >> DWL_MPEG4_E) & 0x3U;
	pHwCfg->vc1Support = (configReg >> DWL_VC1_E) & 0x3U;
	pHwCfg->mpeg2Support = (configReg >> DWL_MPEG2_E) & 0x01U;
	pHwCfg->sorensonSparkSupport = (configReg >> DWL_SORENSONSPARK_E) & 0x01U;
	pHwCfg->refBufSupport = (configReg >> DWL_REF_BUFF_E) & 0x01U;
	pHwCfg->vp6Support = (configReg >> DWL_VP6_E) & 0x01U;
#ifdef DEC_X170_APF_DISABLE
	if (DEC_X170_APF_DISABLE) {
		pHwCfg->tiledModeSupport = 0;
	}
#endif /* DEC_X170_APF_DISABLE */

	pHwCfg->maxDecPicWidth = configReg & 0x07FFU;

	/* 2nd Config register */
	configReg = AsicBaseAdress_Cp[HX170DEC_SYNTH_CFG_2];
	if (pHwCfg->refBufSupport) {
		if ((configReg >> DWL_REF_BUFF_ILACE_E) & 0x01U) {
			pHwCfg->refBufSupport |= 2;
		}
		if ((configReg >> DWL_REF_BUFF_DOUBLE_E) & 0x01U) {
			pHwCfg->refBufSupport |= 4;
		}
	}

	pHwCfg->customMpeg4Support = (configReg >> DWL_MPEG4_CUSTOM_E) & 0x01U;
	pHwCfg->vp7Support = (configReg >> DWL_VP7_E) & 0x01U;
	pHwCfg->vp8Support = (configReg >> DWL_VP8_E) & 0x01U;
	pHwCfg->avsSupport = (configReg >> DWL_AVS_E) & 0x01U;

	/* JPEG xtensions */
	asicID = core->chip_id;
	if (((asicID >> 16) >= 0x8190U) ||
	    ((asicID >> 16) == 0x6731U)) {
		pHwCfg->jpegESupport = (configReg >> DWL_JPEG_EXT_E) & 0x01U;
	} else {
		pHwCfg->jpegESupport = JPEG_EXT_NOT_SUPPORTED;
	}

	if (((asicID >> 16) >= 0x9170U) ||
	    ((asicID >> 16) == 0x6731U)) {
		pHwCfg->rvSupport = (configReg >> DWL_RV_E) & 0x03U;
	} else {
		pHwCfg->rvSupport = RV_NOT_SUPPORTED;
	}

	pHwCfg->mvcSupport = (configReg >> DWL_MVC_E) & 0x03U;

	pHwCfg->webpSupport = (configReg >> DWL_WEBP_E) & 0x01U;
	pHwCfg->tiledModeSupport = (configReg >> DWL_DEC_TILED_L) & 0x03U;

	if (pHwCfg->refBufSupport &&
	    (asicID >> 16) == 0x6731U) {
		pHwCfg->refBufSupport |= 8; /* enable HW support for offset */
	}

	/* Pp configuration */
	configReg = AsicBaseAdress_Cp[HX170PP_SYNTH_CFG];

	if ((configReg >> DWL_PP_E) & 0x01U) {
		pHwCfg->ppSupport = 1;
		pHwCfg->maxPpOutPicWidth = configReg & 0x07FFU;
		/*pHwCfg->ppConfig = (configReg >> DWL_CFG_E) & 0x0FU; */
		pHwCfg->ppConfig = configReg;
	} else {
		pHwCfg->ppSupport = 0;
		pHwCfg->maxPpOutPicWidth = 0;
		pHwCfg->ppConfig = 0;
	}

	/* check the HW version */
	if (((asicID >> 16) >= 0x8190U) ||
	    ((asicID >> 16) == 0x6731U)) {
		unsigned int deInterlace;
		unsigned int alphaBlend;
		unsigned int deInterlaceFuse;
		unsigned int alphaBlendFuse;

		/* Maximum output width of Post-Processor */
		if (pHwCfg->maxPpOutPicWidth > hwFuseSts.maxPpOutPicWidthFuse) {
			pHwCfg->maxPpOutPicWidth = hwFuseSts.maxPpOutPicWidthFuse;
		}
		/* h264 */
		if (!hwFuseSts.h264SupportFuse) {
			pHwCfg->h264Support = H264_NOT_SUPPORTED;
		}
		/* mpeg-4 */
		if (!hwFuseSts.mpeg4SupportFuse) {
			pHwCfg->mpeg4Support = MPEG4_NOT_SUPPORTED;
		}
		/* custom mpeg-4 */
		if (!hwFuseSts.customMpeg4SupportFuse) {
			pHwCfg->customMpeg4Support = MPEG4_CUSTOM_NOT_SUPPORTED;
		}
		/* jpeg (baseline && progressive) */
		if (!hwFuseSts.jpegSupportFuse) {
			pHwCfg->jpegSupport = JPEG_NOT_SUPPORTED;
		}
		if ((pHwCfg->jpegSupport == JPEG_PROGRESSIVE) &&
		    !hwFuseSts.jpegProgSupportFuse) {
			pHwCfg->jpegSupport = JPEG_BASELINE;
		}
		/* mpeg-2 */
		if (!hwFuseSts.mpeg2SupportFuse) {
			pHwCfg->mpeg2Support = MPEG2_NOT_SUPPORTED;
		}
		/* vc-1 */
		if (!hwFuseSts.vc1SupportFuse) {
			pHwCfg->vc1Support = VC1_NOT_SUPPORTED;
		}
		/* vp6 */
		if (!hwFuseSts.vp6SupportFuse) {
			pHwCfg->vp6Support = VP6_NOT_SUPPORTED;
		}
		/* vp7 */
		if (!hwFuseSts.vp7SupportFuse) {
			pHwCfg->vp7Support = VP7_NOT_SUPPORTED;
		}
		/* pp */
		if (!hwFuseSts.ppSupportFuse) {
			pHwCfg->ppSupport = PP_NOT_SUPPORTED;
		}
		/* check the pp config vs fuse status */
		if ((pHwCfg->ppConfig & 0xFC000000) &&
		    ((hwFuseSts.ppConfigFuse & 0xF0000000) >> 5)) {
			/* config */
			deInterlace = ((pHwCfg->ppConfig & PP_DEINTERLACING) >> 25);
			alphaBlend = ((pHwCfg->ppConfig & PP_ALPHA_BLENDING) >> 24);
			/* fuse */
			deInterlaceFuse =
			        (((hwFuseSts.ppConfigFuse >> 5) & PP_DEINTERLACING) >> 25);
			alphaBlendFuse =
			        (((hwFuseSts.ppConfigFuse >> 5) & PP_ALPHA_BLENDING) >> 24);

			/* check if */
			if (deInterlace && !deInterlaceFuse) {
				pHwCfg->ppConfig &= 0xFD000000;
			}
			if (alphaBlend && !alphaBlendFuse) {
				pHwCfg->ppConfig &= 0xFE000000;
			}
		}
		/* sorenson */
		if (!hwFuseSts.sorensonSparkSupportFuse) {
			pHwCfg->sorensonSparkSupport = SORENSON_SPARK_NOT_SUPPORTED;
		}
		/* ref. picture buffer */
		if (!hwFuseSts.refBufSupportFuse) {
			pHwCfg->refBufSupport = REF_BUF_NOT_SUPPORTED;
		}

		/* rv */
		if (!hwFuseSts.rvSupportFuse) {
			pHwCfg->rvSupport = RV_NOT_SUPPORTED;
		}
		/* avs */
		if (!hwFuseSts.avsSupportFuse) {
			pHwCfg->avsSupport = AVS_NOT_SUPPORTED;
		}
		/* mvc */
		if (!hwFuseSts.mvcSupportFuse) {
			pHwCfg->mvcSupport = MVC_NOT_SUPPORTED;
		}
	}

	/* As config is stored, it is possible to switch off vp8 core */
	hx170_clock_off(core);
#ifdef CONFIG_PM_RUNTIME
	/** pm_runtime_put -> decrement pm ref_counter and if == 0 calls H264ppPmRuntimeSuspend */
	pm_runtime_mark_last_busy(core->dev);
	pm_runtime_put(core->dev);
#endif

	if (!hwFuseSts.vp8SupportFuse) {
		pHwCfg->vp8Support = VP8_NOT_SUPPORTED;
	}

	/* Maximum decoding width supported by the HW */
	if (pHwCfg->maxDecPicWidth != hwFuseSts.maxDecPicWidthFuse) {
		pHwCfg->maxDecPicWidth = hwFuseSts.maxDecPicWidthFuse;
	}

	pr_info("AsicFuse : vp8Support = %d;\n maxDecPicWidth = %d\n",
	        pHwCfg->vp8Support, pHwCfg->maxDecPicWidth);
}

/* ---------------------------------------------------- */

unsigned int GetDecRegister(const unsigned int *regBase, unsigned int id)
{
	unsigned int tmp;

	assert(id < HWIF_LAST_REG);

	tmp = regBase[hwDecRegSpec[id][0]];
	tmp = tmp >> hwDecRegSpec[id][2];
	tmp &= regMask[hwDecRegSpec[id][1]];
	return (tmp);
}


/* ---------------------------------------------------- */

void SetDecRegister(unsigned int *regBase, unsigned int id, unsigned int value)
{
	unsigned int tmp;

	assert(id < HWIF_LAST_REG);

	tmp = regBase[hwDecRegSpec[id][0]];
	tmp &= ~(regMask[hwDecRegSpec[id][1]] << hwDecRegSpec[id][2]);
	tmp |= (value & regMask[hwDecRegSpec[id][1]]) << hwDecRegSpec[id][2];
	regBase[hwDecRegSpec[id][0]] = tmp;
}

/*------------------------------------------------------------------------------
    Function name   : DWLWriteReg
    Description     : Write a value to a hardware IO register

    Return type     : void

    Argument        : dec_dwl - DWL instance
    Argument        : unsigned int offset - byte offset of the register to be written
    Argument        : unsigned int value - value to be written out
------------------------------------------------------------------------------*/
void DWLWriteReg(hX170dwl_t *dec_dwl, unsigned int offset, unsigned int value)
{
	/**
	 * SE : I keep the original definition of this function, there is no need to change it
	 * Rq : The Register zone should be mapped before calling this function
	 */
	/* TODO rm all these assertions! */
	assert(dec_dwl != NULL);
	assert(dec_dwl->core);

	DWL_DEBUG("DWL: Write reg %d at offset 0x%02X --> %08X\n", offset / 4,
	          offset, value);

	assert((dec_dwl->clientType != DWL_CLIENT_TYPE_PP &&
	        offset < HX170PP_REG_START) ||
	       (dec_dwl->clientType == DWL_CLIENT_TYPE_PP &&
	        offset >= HX170PP_REG_START));

	assert(offset < dec_dwl->core->regs_size);

	offset = offset / 4;
	dec_dwl->core->regs[offset] = value;
}

/*------------------------------------------------------------------------------
    Function name   : DWLWaitHwReady
    Description     : Wait until hardware has stopped running.
                      Used for synchronizing software runs with the hardware.
                      The wait could succed, timeout, or fail with an error.

    Return type     : signed int - one of the values DWL_HW_WAIT_OK
                                              DWL_HW_WAIT_TIMEOUT
                                              DWL_HW_WAIT_ERROR

    Argument        : const void * instance - DWL instance
------------------------------------------------------------------------------*/
signed int DWLWaitHwReady(hX170dwl_t *dec_dwl, unsigned int timeout)
{
	signed int ret;

	assert(dec_dwl);

	switch (dec_dwl->clientType) {
	case DWL_CLIENT_TYPE_H264_DEC:
	case DWL_CLIENT_TYPE_MPEG4_DEC:
	case DWL_CLIENT_TYPE_JPEG_DEC:
	case DWL_CLIENT_TYPE_VC1_DEC:
	case DWL_CLIENT_TYPE_MPEG2_DEC:
	case DWL_CLIENT_TYPE_RV_DEC:
	case DWL_CLIENT_TYPE_VP6_DEC:
	case DWL_CLIENT_TYPE_VP8_DEC:
	case DWL_CLIENT_TYPE_AVS_DEC: {
		ret = DWLWaitDecHwReady(dec_dwl, timeout);
		break;
	}
	case DWL_CLIENT_TYPE_PP: {
		ret = DWLWaitPpHwReady(dec_dwl, timeout);
		break;
	}
	default: {
		assert(0);  /* should not happen */
		ret = DWL_HW_WAIT_ERROR;
	}
	}

	return ret;
}

signed int DWLWaitDecHwReady(hX170dwl_t *dec_dwl, unsigned int timeout)
{
	return DWLWaitPpHwIt(dec_dwl, timeout);
	//    return DWLWaitPpHwReady(dec_dwl, timeout);
}

signed int DWLWaitPpHwIt(hX170dwl_t *dec_dwl, unsigned int timeout)
{
	(void)timeout; // warning removal
	assert(dec_dwl != NULL);

	down(&dec_dwl->core->Dec_End_sem);
	return DWL_HW_WAIT_OK;
}

/*------------------------------------------------------------------------------
    Function name   : DWLWaitHwReady
    Description     : Wait until hardware has stopped running.
                      Used for synchronizing software runs with the hardware.
                      The wait can succeed or timeout.

    Return type     : signed int - one of the values DWL_HW_WAIT_OK
                                              DWL_HW_WAIT_TIMEOUT

    Argument        : dec_dwl - DWL instance
                      unsigned int timeout - timeout period for the wait specified in
                                milliseconds; 0 will perform a poll of the
                                hardware status and -1 means an infinit wait
------------------------------------------------------------------------------*/

signed int DWLWaitPpHwReady(hX170dwl_t *dec_dwl, unsigned int timeout)
{
	volatile unsigned int irq_stats;
	unsigned int FirstTime;
	unsigned int EndTime;

	unsigned int irqRegOffset;

	assert(dec_dwl != NULL);

	if (dec_dwl->clientType == DWL_CLIENT_TYPE_PP) {
		irqRegOffset = HX170PP_REG_START;        /* pp ctrl reg offset */
	} else {
		irqRegOffset = HX170DEC_REG_START;        /* decoder ctrl reg offset */
	}

	/* wait for decoder */
#ifdef _READ_DEBUG_REGS
	(void) DWLReadReg(dec_dwl, 40 * 4);
	(void) DWLReadReg(dec_dwl, 41 * 4);
#endif
	irq_stats = DWLReadReg(dec_dwl, irqRegOffset);
	irq_stats = (irq_stats >> 12) & 0xFF;

	if (irq_stats != 0) {
		return DWL_HW_WAIT_OK;
	} else if (timeout) {
		unsigned int sleep_time = 0;
		int forever = 0;
		int loop = 1;
		signed int ret = DWL_HW_WAIT_TIMEOUT;
		int polling_interval_milli_sec = 1;   /* 1ms polling interval */
		unsigned int Decode_Time_micros_sec;

		FirstTime = OSDEV_GetTimeInMilliSeconds();
		if (timeout == (unsigned int)(-1)) {
			forever = 1;    /* wait forever */
		}

		do {
			OSDEV_SleepMilliSeconds(polling_interval_milli_sec);


#ifdef _READ_DEBUG_REGS
			(void) DWLReadReg(dec_dwl, 40 * 4);
			(void) DWLReadReg(dec_dwl, 41 * 4);
			(void) DWLReadReg(dec_dwl, 42 * 4);
			(void) DWLReadReg(dec_dwl, 43 * 4);
#endif

			irq_stats = DWLReadReg(dec_dwl, irqRegOffset);
			irq_stats = (irq_stats >> 12) & 0xFF;

			if (irq_stats != 0) {
				ret = DWL_HW_WAIT_OK;
				loop = 0;   /* end the polling loop */
			}
			/* if not a forever wait */
			else if (!forever) {
				sleep_time += polling_interval_milli_sec;

				if (sleep_time >= timeout) {
					ret = DWL_HW_WAIT_TIMEOUT;
					loop = 0;
				}
			}
			DWL_DEBUG("Loop for HW timeout. Total sleep: %dms\n", sleep_time);
		} while (loop);

		EndTime = OSDEV_GetTimeInMilliSeconds();
		Decode_Time_micros_sec = EndTime - FirstTime;
		if (Decode_Time_micros_sec) {
			DWL_DEBUG("Decode Time in �s : %d �s\n", Decode_Time_micros_sec);
		}
		return ret;
	} else {
		return DWL_HW_WAIT_TIMEOUT;
		/*Application don't want to wait, just read the register and return OK or TIMEOUT*/
	}
}
/*------------------------------------------------------------------------------
    Function name   : DWLReadReg
    Description     : Read the value of a hardware IO register

    Return type     : unsigned int - the value stored in the register

    Argument        : dec_dwl - DWL instance
    Argument        : unsigned int offset - byte offset of the register to be read
------------------------------------------------------------------------------*/
unsigned int DWLReadReg(hX170dwl_t *dec_dwl, unsigned int offset)
{
	unsigned int val;

	assert(dec_dwl != NULL);
	assert(dec_dwl->core);

	assert((dec_dwl->clientType != DWL_CLIENT_TYPE_PP &&
	        offset < HX170PP_REG_START) ||
	       (dec_dwl->clientType == DWL_CLIENT_TYPE_PP &&
	        offset >= HX170PP_REG_START) || (offset == 0) ||
	       (offset == HX170PP_SYNTH_CFG));

	assert(offset < dec_dwl->core->regs_size);

	offset = offset / 4;
	val = dec_dwl->core->regs[offset];

	DWL_DEBUG("DWL: Read reg %d at offset 0x%02X --> %08X\n", offset,
	          offset * 4, val);

	return val;
}

