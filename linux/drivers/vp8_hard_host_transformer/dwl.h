/*
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301,
 * USA.
 */
/************************************************************************
Copyright (C) 2003-2014 STMicroelectronics. All Rights Reserved.
************************************************************************/

#ifndef H_DWL
#define H_DWL

#include <linux/semaphore.h>
#include "decapicommon.h"

#define assert(expr, args...) do {\
    if (!(expr)) {\
        pr_crit("%s Assert from VP8 decoder\n", __func__);\
    }\
} while (0)

#define DWL_OK                      0
#define DWL_ERROR                  -1

#define MAP_FAILED                  0
#define DWL_HW_WAIT_OK              DWL_OK
#define DWL_HW_WAIT_ERROR           DWL_ERROR
#define DWL_HW_WAIT_TIMEOUT         1

#define DWL_CLIENT_TYPE_H264_DEC         1U
#define DWL_CLIENT_TYPE_MPEG4_DEC        2U
#define DWL_CLIENT_TYPE_JPEG_DEC         3U
#define DWL_CLIENT_TYPE_PP               4U
#define DWL_CLIENT_TYPE_VC1_DEC          5U
#define DWL_CLIENT_TYPE_MPEG2_DEC        6U
#define DWL_CLIENT_TYPE_VP6_DEC          7U
#define DWL_CLIENT_TYPE_AVS_DEC          9U /* TODO: fix */
#define DWL_CLIENT_TYPE_RV_DEC           8U
#define DWL_CLIENT_TYPE_VP8_DEC          10U

#define HX170_CHIP_ID_REG        0x0
#define HX170PP_REG_START       0xF0
#define HX170DEC_REG_START      0x4

#define HX170PP_SYNTH_CFG       100
#define HX170DEC_SYNTH_CFG       50
#define HX170DEC_SYNTH_CFG_2     54

#define HX170PP_FUSE_CFG         99
#define HX170DEC_FUSE_CFG        57

#define X170_SEM_KEY 0x8070

struct hX170dwl_config {
	unsigned int maxDecPicWidth;  /* Maximum video decoding width supported  */
	unsigned int maxPpOutPicWidth;   /* Maximum output width of Post-Processor */
	unsigned int h264Support;     /* HW supports h.264 */
	unsigned int jpegSupport;     /* HW supports JPEG */
	unsigned int mpeg4Support;    /* HW supports MPEG-4 */
	unsigned int customMpeg4Support; /* HW supports custom MPEG-4 features */
	unsigned int vc1Support;      /* HW supports VC-1 Simple */
	unsigned int mpeg2Support;    /* HW supports MPEG-2 */
	unsigned int ppSupport;       /* HW supports post-processor */
	unsigned int ppConfig;        /* HW post-processor functions bitmask */
	unsigned int sorensonSparkSupport;   /* HW supports Sorenson Spark */
	unsigned int refBufSupport;   /* HW supports reference picture buffering */
	unsigned int tiledModeSupport; /* HW supports tiled reference pictuers */
	unsigned int vp6Support;      /* HW supports VP6 */
	unsigned int vp7Support;      /* HW supports VP7 */
	unsigned int vp8Support;      /* HW supports VP8 */
	unsigned int avsSupport;      /* HW supports AVS */
	unsigned int jpegESupport;    /* HW supports JPEG extensions */
	unsigned int rvSupport;       /* HW supports REAL */
	unsigned int mvcSupport;      /* HW supports H264 MVC extension */
	unsigned int webpSupport;     /* HW supports WebP (VP8 snapshot) */
};

/* Core device data
 * @dev: device pointer
 * @chip_id: hw chip version (register)
 * @irq_its: end of task irq
 * @dev: remapped base address of core
 * @regs_size: size of IO ressource
 * @clk: clock
 * @task_nb: current task number of HW
 * @cfg: HW configuration
 * @hx170_init_sem: semaphore for init done
 */
struct hX170dwl_core {
	struct device          *dev;
	unsigned int           chip_id;
	unsigned int           irq_its;
	unsigned int __iomem   *regs;
	int                    regs_size;
	struct clk             *clk;
	atomic_t               task_nb;
	struct hX170dwl_config cfg;
	struct semaphore       hx170_init_sem;
	struct semaphore       Dec_End_sem;
	unsigned int           max_freq;
};

/* DWL instance */
typedef struct hX170dwl {
	struct hX170dwl_core *core; /* Pointer to core device data */
	unsigned int         clientType;
	struct semaphore     semid;
} hX170dwl_t;


typedef struct DWLHwFuseStatus {
	unsigned int h264SupportFuse;     /* HW supports h.264 */
	unsigned int mpeg4SupportFuse;    /* HW supports MPEG-4 */
	unsigned int mpeg2SupportFuse;    /* HW supports MPEG-2 */
	unsigned int sorensonSparkSupportFuse;   /* HW supports Sorenson Spark */
	unsigned int jpegSupportFuse;     /* HW supports JPEG */
	unsigned int vp6SupportFuse;      /* HW supports VP6 */
	unsigned int vp7SupportFuse;      /* HW supports VP6 */
	unsigned int vp8SupportFuse;      /* HW supports VP6 */
	unsigned int vc1SupportFuse;      /* HW supports VC-1 Simple */
	unsigned int jpegProgSupportFuse; /* HW supports Progressive JPEG */
	unsigned int ppSupportFuse;       /* HW supports post-processor */
	unsigned int ppConfigFuse;        /* HW post-processor functions bitmask */
	unsigned int maxDecPicWidthFuse;  /* Maximum video decoding width supported  */
	unsigned int maxPpOutPicWidthFuse; /* Maximum output width of Post-Processor */
	unsigned int refBufSupportFuse;   /* HW supports reference picture buffering */
	unsigned int avsSupportFuse;      /* one of the AVS values defined above */
	unsigned int rvSupportFuse;       /* one of the REAL values defined above */
	unsigned int mvcSupportFuse;
	unsigned int customMpeg4SupportFuse; /* Fuse for custom MPEG-4 */

} DWLHwFuseStatus_t;


/*Function's Prototypes*/
void DWLReadAsicFuseStatus(struct hX170dwl_core *core, DWLHwFuseStatus_t *pHwFuseSts);
void DWLReadAsicConfig(struct hX170dwl_core *core);
int DWLRelease(hX170dwl_t *instance);

unsigned int GetDecRegister(const unsigned int *regBase, unsigned int id);

void SetDecRegister(unsigned int *regBase, unsigned int id, unsigned int value);
void DWLWriteReg(hX170dwl_t *dec_dwl, unsigned int offset, unsigned int value);
signed int DWLWaitHwReady(hX170dwl_t *dec_dwl, unsigned int timeout);

signed int DWLWaitDecHwReady(hX170dwl_t *dec_dwl, unsigned int timeout);

signed int DWLWaitPpHwReady(hX170dwl_t *dec_dwl, unsigned int timeout);
signed int DWLWaitPpHwIt(hX170dwl_t *dec_dwl, unsigned int timeout);

unsigned int DWLReadReg(hX170dwl_t *dec_dwle, unsigned int offset);

int hx170_clock_on(struct hX170dwl_core *core);
void hx170_clock_off(struct hX170dwl_core *core);
int  hx170_check_chip_id(struct hX170dwl_core *core);
int vp8_fuse_check(struct hX170dwl_core *core);

#endif /*H_DWL*/

