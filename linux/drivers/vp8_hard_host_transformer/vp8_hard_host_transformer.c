/************************************************************************
Copyright (C) 2003-2014 STMicroelectronics. All Rights Reserved.

This file is part of the Streaming Engine.

Streaming Engine is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License version 2 as
published by the Free Software Foundation.

Streaming Engine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with Streaming Engine; see the file COPYING.  If not, write to the Free
Software Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307,
USA.

The Streaming Engine Library may alternatively be licensed under a
proprietary license from ST.
************************************************************************/

#include <linux/module.h>
#include <linux/device.h>
#include <linux/fs.h>
#include <linux/platform_device.h>
#include <linux/init.h>    /* Initialisation support */
#include <linux/kernel.h>  /* Kernel support */
#include <linux/err.h>
#include <linux/suspend.h>
#include <linux/pm_runtime.h>
#include <linux/pm.h>
#include <linux/firmware.h> /*for request_firmware() */
#include <linux/of.h>
#include <linux/clk.h>

#include <mme.h>

#include "osdev_mem.h"

#include "VP8_hard_VideoTransformer.h"
#include "VP8_hard_VideoTransformerTypes.h"
#include "vp8hard.h"
#include "vp8hard_decode.h"

#define MODULE_NAME "VP8 hardware host transformer"

MODULE_DESCRIPTION("VP8 decode hardware cell platform driver");
MODULE_AUTHOR("STMicroelectronics");
MODULE_LICENSE("GPL");

//#define DEBUG_VIDEO 1

/**
 * Global vp8_core structure (containing info about device...)
 */
struct hX170dwl_core *vp8_core;

static int RegisterVp8Transformer(void);
static int DeRegisterVp8Transformer(void);
static int stm_pm_vp8_notifier_call(struct notifier_block *this, unsigned long event, void *ptr);

// PM notifier callback
// At entry of low power, multicom is terminated in .freeze callback. When system exit from low
// power, multicom is initialized through pm_notifier, so we have to register VP8 transformer again.
// After VP8 pm_notifier, SE module notifier would be called in which other multicom apis
// are called.
// This callback is used to register the VP8 transformer after low power exit
#define ENTRY(enum) case enum: return #enum
inline const char *StringifyPmEvent(int aPmEvent)
{
	switch (aPmEvent) {
		ENTRY(PM_HIBERNATION_PREPARE);
		ENTRY(PM_POST_HIBERNATION);
		ENTRY(PM_SUSPEND_PREPARE);
		ENTRY(PM_POST_SUSPEND);
		ENTRY(PM_RESTORE_PREPARE);
		ENTRY(PM_POST_RESTORE);
	default: return "<unknown pm event>";
	}
}

static int Vp8ClockSetRate(struct device *dev)
{
	int ret = 0;
	struct hX170dwl_core *core;

	core = dev_get_drvdata(dev);

	/* check incase 0 is set for max frequency property in DT */
	if (core && core->max_freq) {
		ret = clk_set_rate(core->clk, core->max_freq);
		if (ret) {
			pr_err("Error: setting max frequency failed (%d)\n", ret);
			return -EINVAL;
		}
		pr_info("Vp8 clock set to %u\n", core->max_freq);
	}
	return ret;
}

static int stm_pm_vp8_notifier_call(struct notifier_block *this, unsigned long event, void *ptr)
{
	int ErrorCode;

	(void)this; // warning removal
	(void)ptr; // warning removal

	switch (event) {
	case PM_POST_SUSPEND:
	// fallthrough
	case PM_POST_HIBERNATION:
		pr_info("%s notifier: %s\n", __func__, StringifyPmEvent(event));

		ErrorCode = RegisterVp8Transformer();
		if (ErrorCode != 0) {
			// Error returned by VP8, but this should not prevent the system exiting low power
			pr_err("Error: %s RegisterVp8Transformer failed (%d)\n", __func__, ErrorCode);
		}
		break;

	default:
		break;
	}

	return NOTIFY_DONE;
}

/**
 * Get platform data from Device Tree
 */
static int vp8_get_of_pdata(struct platform_device *pdev,
                            struct hX170dwl_core *core)
{
#ifdef CONFIG_OF
	int ret;

	core->max_freq = 0;
	if (of_property_read_u32(pdev->dev.of_node, "clock-frequency", &core->max_freq)) {
		pr_info("Maximum vp8 clock frequency not retrieved from DT\n");
	}
#ifdef CONFIG_ARCH_STI
	core->clk = devm_clk_get(&pdev->dev, "clk_vp8");
#else
	struct device_node *np = pdev->dev.of_node;
	const char *clkName;

	of_property_read_string_index(np, "st,dev_clk", 0, &clkName);

	core->clk = devm_clk_get(&pdev->dev, clkName);
#endif
	if (IS_ERR(core->clk)) {
		pr_err("Error: %s failed to get clock\n", __func__);
		//return PTR_ERR(core->clk);
	}
	ret = Vp8ClockSetRate(&(pdev->dev));
	if (ret) {
		pr_err("Error: setting maximum clock frequency failed (%d)\n", ret);
	}

	return ret;
#else //CONFIG_OF
	return 0;
#endif
}

// Power management callbacks
static struct notifier_block stm_pm_vp8_notifier = {
	.notifier_call = stm_pm_vp8_notifier_call,
};

/*
 * vp8_probe() - This routine loads the hx170 core driver
 *
 * @pdev: platform device.
 */
static int vp8_probe(struct platform_device *pdev)
{
	struct resource *iomem;
	unsigned char clk_alias[20];
	int ret = 0;

	BUG_ON(pdev == NULL);

	ret = RegisterVp8Transformer();
	if (ret) {
		pr_err("Error: %s RegisterVp8Transformer failed %d\n", __func__, ret);
		goto fail_out;
	}

	vp8_core = devm_kzalloc(&pdev->dev, sizeof(*vp8_core), GFP_KERNEL);
	if (!vp8_core) {
		pr_err("Error: %s core alloc failed\n", __func__);
		ret = -ENOMEM;
		goto fail_vp8probe;
	}

	platform_set_drvdata(pdev, vp8_core);
	vp8_core->dev = &pdev->dev;

	iomem = platform_get_resource_byname(pdev, IORESOURCE_MEM, "regs");
	if (!iomem) {
		pr_err("Error: %s Failed to get DT registers property\n", __func__);
		ret = -EINVAL;
		goto fail_vp8probe;
	}
	vp8_core->regs_size = resource_size(iomem);
	vp8_core->regs = devm_ioremap_nocache(vp8_core->dev, iomem->start,
	                                      vp8_core->regs_size);
	if (!vp8_core->regs) {
		pr_err("Error: %s Can't ioremap hx170 registers region\n", __func__);
		ret = -ENOMEM;
		goto fail_vp8probe;
	}

	vp8_core->irq_its = platform_get_irq(pdev, 0);
	ret = devm_request_irq(&pdev->dev, vp8_core->irq_its, VP8InterruptHandler,
	                       0, "VP8_irq", (void *)vp8_core);

	if (pdev->dev.of_node) {
		ret = vp8_get_of_pdata(pdev, vp8_core);
		if (ret) {
			pr_err("Error: %s Probing device with DT failed\n", __func__);
			goto fail_vp8probe;
		}
	} else {
		pr_warn("warning: %s without DT\n", __func__);
		snprintf(clk_alias, sizeof(clk_alias), "clk_vp8");
		clk_alias[sizeof(clk_alias) - 1] = '\0';

		clk_add_alias(clk_alias, NULL, "CLK_M_ICN_VP8", NULL);
		vp8_core->clk = devm_clk_get(&pdev->dev, clk_alias);
		if (IS_ERR(vp8_core->clk)) {
			pr_err("Error: %s Unable to get clock\n", __func__);
			ret = PTR_ERR(vp8_core->clk);
			goto fail_vp8probe;
		}
	}

	ret = clk_prepare(vp8_core->clk);
	if (ret) {
		pr_err("Error: %s Failed to prepare clock\n", __func__);
		goto fail_vp8probe;
	}

#ifdef CONFIG_PM_RUNTIME
	pm_runtime_set_suspended(&pdev->dev);
	pm_runtime_enable(&pdev->dev);
#endif

	ret = hx170_check_chip_id(vp8_core);
	if (ret) {
		pr_err("Error: %s Failed to check chip_id\n", __func__);
		goto vp8_fail_unprep;
	}

	ret = vp8_fuse_check(vp8_core);
	if (ret) {
		pr_err("Error: %s Fuse check failed\n", __func__);
		goto vp8_fail_unprep;
	}

	sema_init(&vp8_core->Dec_End_sem, 0);

	// Register with PM notifiers (for HPS/CPS support)
	register_pm_notifier(&stm_pm_vp8_notifier);

	OSDEV_Dump_MemCheckCounters(__func__);
	pr_info("%s probe done ok\n", MODULE_NAME);
	pr_debug("%s chip ID:0x%x size:%d\n", __func__, vp8_core->chip_id, vp8_core->regs_size);

	return 0;

vp8_fail_unprep:
#ifdef CONFIG_PM_RUNTIME
	pm_runtime_disable(&pdev->dev);
#endif
	clk_unprepare(vp8_core->clk);
fail_vp8probe:
	DeRegisterVp8Transformer();
fail_out:
	OSDEV_Dump_MemCheckCounters(__func__);
	pr_err("Error: %s probe failed\n", MODULE_NAME);
	return ret;
}

/*
 * vp8_remove() - Module exit function for the hx170 driver
 */
static int vp8_remove(struct platform_device *pdev)
{
	struct hX170dwl_core *core;

	BUG_ON(pdev == NULL);

#ifdef CONFIG_PM_RUNTIME
	pm_runtime_disable(&pdev->dev);
#endif
	unregister_pm_notifier(&stm_pm_vp8_notifier);

	core = dev_get_drvdata(&pdev->dev);
	BUG_ON(core == NULL);

	clk_unprepare(core->clk);

	dev_set_drvdata(&pdev->dev, NULL);
	DeRegisterVp8Transformer();

	OSDEV_Dump_MemCheckCounters(__func__);
	pr_info("%s remove done\n", MODULE_NAME);
	return 0;
}

// ////////////////////////////////PM///////////////////////////////////////////
#ifdef CONFIG_PM
#ifdef CONFIG_PM_RUNTIME
/**
 * pm_runtime_suspend callback for Active Standby
 * called if pm_runtime ref counter == 0 (and for Active Standby)
 */
static int vp8_pm_runtime_suspend(struct device *dev)
{
	(void)dev; // warning removal
	return 0;
}

/**
 * pm_runtime_resume callback for Active Standby
 * called if pm_runtime ref counter == 0 (and for Active Standby)
 */
static int vp8_pm_runtime_resume(struct device *dev)
{
	(void)dev; // warning removal
	return 0;
}
#endif // CONFIG_PM_RUNTIME

/**
 * PM suspend callback
 * For Gateway profile: called on Host Passive Standby entry (HPS entry)
 * For STB profile: called on Controller Passive Standby entry (CPS entry)
 */
static int vp8_pm_suspend(struct device *dev)
{
	struct hX170dwl_core *core;

	core = dev_get_drvdata(dev);
	BUG_ON(!core);

	pr_info("%s\n", __func__);
	/**
	* Here we assume that there are no more jobs in progress
	* This is ensured at SE level, which has already entered "low power state",
	* in PM notifier callback of player2 module (on PM_SUSPEND_PREPARE event)
	 */
	if (atomic_read(&core->task_nb)) {
		pr_info("%s : core used (should not happen)\n", __func__);
	}

	return 0;
}

/**
 * PM resume callback
 * For Gateway profile: called on Host Passive Standby exit (HPS exit)
 * For STB profile: called on Controller Passive Standby exit (CPS exit)
 */
static int vp8_pm_resume(struct device *dev)
{
	pr_info("%s\n", __func__);
	return 0;
}

static struct dev_pm_ops vp8_pm_ops = {
	.suspend = vp8_pm_suspend,
	.resume  = vp8_pm_resume,
#ifdef CONFIG_PM_RUNTIME
	.runtime_suspend  = vp8_pm_runtime_suspend,
	.runtime_resume   = vp8_pm_runtime_resume,
#endif
};
#endif // CONFIG_PM

#ifdef CONFIG_OF
static struct of_device_id stm_vp8_match[] = {
	{
		.compatible = "st,se-hx170",
	},
	{},
};
//MODULE_DEVICE_TABLE(of, stm_vp8_match);
#endif

static struct platform_driver platform_vp8_driver = {
	.driver = {
		.owner = THIS_MODULE,
		.name = "vp8",
		.of_match_table = of_match_ptr(stm_vp8_match),
#ifdef CONFIG_PM
		.pm = &vp8_pm_ops,
#endif
	},
	.probe = vp8_probe,
	.remove = vp8_remove,
};

module_platform_driver(platform_vp8_driver);

MME_ERROR VP8DEC_Abort(void *context, MME_CommandId_t cmdId)
{
	(void)context; // warning removal
	(void)cmdId; // warning removal
	return MME_SUCCESS;
}

MME_ERROR VP8DEC_GetTransformerCapability(MME_TransformerCapability_t *capability)
{
	VP8_CapabilityParams_t *cap;

	/*
	typedef struct MME_TransformerCapability_t {
	            MME_UINT             StructSize;
	            MME_UINT             Version;
	            MME_DataFormat_t     InputType;
	            MME_DataFormat_t     OutputType;
	            MME_UINT             TransformerInfoSize;
	            MME_GenericParams_t  TransformerInfo_p;
	} MME_TransformerCapability_t;
	*/

	cap = (VP8_CapabilityParams_t *)capability->TransformerInfo_p;

	capability->Version = 0xF000F000;

	/* TODO : add a check on the hardware version (to be sure that google IP exist on SOC) */
	cap->IsHantroG1Here = 1;
	capability->TransformerInfoSize = sizeof(VP8_CapabilityParams_t);

	return MME_SUCCESS;
}

MME_ERROR VP8DEC_InitTransformer(MME_UINT paramsSize, MME_GenericParams_t params, void **context)
{
	Vp8HardInitParams_t  *HardInitParams_p;
	VP8_InitTransformerParam_t *initparams = (VP8_InitTransformerParam_t *)params;
	Vp8HardStatus_t status;
	MME_ERROR ret = MME_SUCCESS;

	(void)paramsSize; // warning removal

	/* allocate the memory for the HardwareInitParams structure ... */
	HardInitParams_p = kzalloc(sizeof(Vp8HardInitParams_t), GFP_KERNEL);
	if (!HardInitParams_p) {
		pr_err("Error: %s alloc failed\n", __func__);
		return MME_NOMEM;
	}

	HardInitParams_p->decFormat = VP8DEC_VP8;
	HardInitParams_p->numFrameBuffers = 4;
	HardInitParams_p->referenceFrameFormat = DEC_REF_FRM_RASTER_SCAN;
	HardInitParams_p->useVideoFreezeConcealment = 0;
	HardInitParams_p->CodedWidth = initparams->CodedWidth;
	HardInitParams_p->CodedHeight = initparams->CodedHeight;

	status = Vp8HardInit(vp8_core, HardInitParams_p);

	if (status != VP8HARD_NO_ERROR) {
		pr_err("Error: %s Vp8HardInit failed\n", __func__);
		*context = NULL;
		ret = MME_INTERNAL_ERROR;
	} else {
		*context = (void *)HardInitParams_p;
		pr_debug("Succeed to initialize the VP8 Hardware Decoder");
	}

	return ret;
}

MME_ERROR VP8DEC_ProcessCommand(void *handle, MME_Command_t *CmdInfo_p)
{
	/*
	typedef struct MME_Command_t {
	            MME_UINT                  StructSize;
	            MME_CommandCode_t         CmdCode;
	            MME_CommandEndType_t      CmdEnd;
	            MME_Time_t                DueTime;
	            MME_UINT                  NumberInputBuffers;
	            MME_UINT                  NumberOutputBuffers;
	            MME_DataBuffer_t**        DataBuffers_p;
	            MME_CommandStatus_t       CmdStatus;
	            MME_UINT                  ParamSize;
	            MME_GenericParams_t       Param_p;
	} MME_Command_t;
	*/
	char *VP8_BitBufferStart; /* Pointer to the start of  Bit Buffer containing the compressed frames */
	// char *VP8_BitBufferEnd; /* Pointer to the end of  Bit Buffer containing the compressed frames */
	char *VP8_BitStreamPointer; /* Pointer to the Current frame's compressed data inside  Bit Buffer */

	unsigned int  VP8_BitBufferSize;        /* total length of the Bit Buffer */

	// VP8_ReturnParams_t *vp8_returnparams ;
	Vp8HardInitParams_t  *HardInitParams_p;
	VP8DecInput *pInput;

	VP8_TransformParam_t         *Transformer_Struct_p;
	VP8_ParamPicture_t           Picture_Data_Struct_p;

	MME_CommandStatus_t *CmdStatus = &CmdInfo_p->CmdStatus;

	// VP8DecRet DecResult;

	CmdStatus->State = MME_COMMAND_EXECUTING;

	HardInitParams_p = (Vp8HardInitParams_t *)handle;
	if (HardInitParams_p == NULL) {
		return MME_INTERNAL_ERROR;
	}
	pInput = &HardInitParams_p->decIn;

	switch (CmdInfo_p->CmdCode) {
	case MME_TRANSFORM:
		HardInitParams_p  = (Vp8HardInitParams_t *)handle;
		Transformer_Struct_p = (VP8_TransformParam_t *)CmdInfo_p->Param_p;
		Picture_Data_Struct_p = Transformer_Struct_p->PictureParameters;
		/* For setting the return params */
		/* vp8_returnparams = (VP8_ReturnParams_t *)CmdStatus->AdditionalInfo_p; */
		/* Pointer to the Bit Buffer */
		VP8_BitBufferStart = (char *) Transformer_Struct_p->CodedData_Cp;
		VP8_BitBufferSize  = Transformer_Struct_p->CodedDataSize ;
		/* VP8_BitBufferEnd  = (char *)((unsigned int)VP8_BitBufferStart + VP8_BitBufferSize - 1); */

		/* Pointer to the compressed frame */
		VP8_BitStreamPointer = (char *)VP8_BitBufferStart;
		pInput->dataLen = VP8_BitBufferSize ;
		pInput->streamBusAddress = (unsigned int) Transformer_Struct_p->CodedData;
		pInput->pStream = (char *)VP8_BitStreamPointer;

#if defined(DEBUG_VIDEO)

		/* Frame Buffer where to decode */
		pInput->DataBuffers.picBufferBusAddressY = (unsigned int)Transformer_Struct_p->CurrentFB_Luma_p;
		pInput->DataBuffers.pPicBufferY = (unsigned int *)ioremap_nocache((unsigned int)
		                                                                  pInput->DataBuffers.picBufferBusAddressY,
		                                                                  Transformer_Struct_p->FrameBuffer_Size);

		pInput->DataBuffers.pPicBufferC = (unsigned int *)(((unsigned int)pInput->DataBuffers.pPicBufferY) +
		                                                   (HardInitParams_p->CodedWidth * HardInitParams_p->CodedHeight));
		pInput->DataBuffers.picBufferBusAddressC = Transformer_Struct_p->CurrentFB_Chroma_p;

		/* previous reference frame */
		pInput->DataBuffers.LastpicBufferBusAddressY = (unsigned int)Transformer_Struct_p->LastRefFB_Luma_p;
		pInput->DataBuffers.pLastPicBufferY = (unsigned int *)ioremap_nocache((unsigned int)
		                                                                      pInput->DataBuffers.LastpicBufferBusAddressY, Transformer_Struct_p->FrameBuffer_Size);

		pInput->DataBuffers.pLastPicBufferC = (unsigned int *) 0;
		pInput->DataBuffers.LastpicBufferBusAddressC = Transformer_Struct_p->LastRefFB_Chroma_p;

		/* golden reference frame  */
		pInput->DataBuffers.GoldpicBufferBusAddressY = Transformer_Struct_p->GoldenFB_Luma_p;
		pInput->DataBuffers.pGoldPicBufferY = (unsigned int *)ioremap_nocache((unsigned int)
		                                                                      pInput->DataBuffers.GoldpicBufferBusAddressY, Transformer_Struct_p->FrameBuffer_Size);

		pInput->DataBuffers.pGoldPicBufferC = (unsigned int *) 0;
		pInput->DataBuffers.GoldpicBufferBusAddressC = Transformer_Struct_p->GoldenFB_Chroma_p;

		/* alternate reference frame */
		pInput->DataBuffers.AltpicBufferBusAddressY = Transformer_Struct_p->AlternateFB_Luma_p;
		pInput->DataBuffers.pAltPicBufferY = (unsigned int *)ioremap_nocache((unsigned int)
		                                                                     pInput->DataBuffers.AltpicBufferBusAddressY, Transformer_Struct_p->FrameBuffer_Size);

		pInput->DataBuffers.pAltPicBufferC = (unsigned int *) 0;
		pInput->DataBuffers.AltpicBufferBusAddressC = Transformer_Struct_p->AlternateFB_Chroma_p;

#else /*defined(DEBUG_VIDEO)*/

		/* Frame Buffer where to decode */
		pInput->DataBuffers.picBufferBusAddressY = (unsigned int)Transformer_Struct_p->CurrentFB_Luma_p;
		pInput->DataBuffers.pPicBufferY = (unsigned int *)
		                                  Transformer_Struct_p->CurrentFB_Luma_p; /*Put the physical @ : HW don't use the mapped @, so we will not map it*/

		pInput->DataBuffers.pPicBufferC = (unsigned int *)
		                                  Transformer_Struct_p->CurrentFB_Chroma_p; /*Put the physical @ : HW don't use the mapped @, so we will not map it*/
		pInput->DataBuffers.picBufferBusAddressC = Transformer_Struct_p->CurrentFB_Chroma_p;

		/* previous reference frame */
		pInput->DataBuffers.LastpicBufferBusAddressY = (unsigned int)Transformer_Struct_p->LastRefFB_Luma_p;
		pInput->DataBuffers.pLastPicBufferY = (unsigned int *)
		                                      Transformer_Struct_p->LastRefFB_Luma_p; /*Put the physical @ : HW don't use the mapped @, so we will not map it*/

		pInput->DataBuffers.pLastPicBufferC = (unsigned int *) 0;
		pInput->DataBuffers.LastpicBufferBusAddressC =
		        Transformer_Struct_p->LastRefFB_Chroma_p;/*will not be used by the HW : Only the Luma pointer is set in the swRegs*/

		/* golden reference frame  */
		pInput->DataBuffers.GoldpicBufferBusAddressY = Transformer_Struct_p->GoldenFB_Luma_p;
		pInput->DataBuffers.pGoldPicBufferY = (unsigned int *)
		                                      Transformer_Struct_p->GoldenFB_Luma_p; /*Put the physical @ : HW don't use the mapped @, so we will not map it*/

		pInput->DataBuffers.pGoldPicBufferC = (unsigned int *) 0;
		pInput->DataBuffers.GoldpicBufferBusAddressC =
		        Transformer_Struct_p->GoldenFB_Chroma_p;/*will not be used by the HW : Only the Luma pointer is set in the swRegs*/

		/* alternate reference frame */
		pInput->DataBuffers.AltpicBufferBusAddressY = Transformer_Struct_p->AlternateFB_Luma_p;
		pInput->DataBuffers.pAltPicBufferY = (unsigned int *)
		                                     Transformer_Struct_p->AlternateFB_Luma_p; /*Put the physical @ : HW don't use the mapped @, so we will not map it*/

		pInput->DataBuffers.pAltPicBufferC = (unsigned int *) 0;
		pInput->DataBuffers.AltpicBufferBusAddressC =
		        Transformer_Struct_p->AlternateFB_Chroma_p; /*will not be used by the HW : Only the Luma pointer is set in the swRegs*/
#endif /*defined(DEBUG_VIDEO)*/

		/* DecResult =*/ VP8DecDecode(&HardInitParams_p->decInstance, &Picture_Data_Struct_p, pInput);

#if defined(DEBUG_VIDEO)&&defined(DUMP_PICTURES)
		/*Dump pics after each decode*/
		unsigned int header[6];
		unsigned int width = HardInitParams_p->CodedWidth;
		unsigned int height = HardInitParams_p->CodedHeight;
		unsigned int format = 0xb0;
		unsigned int widthAligned;

		header[1] = (unsigned int)format & 0xffff;
		header[2] = width;
		header[3] = height;

		header[0] = (0x420F << 16) | 0x6;
		header[1] |= 0x07110000;
		widthAligned = ((width + 31) >> 5) << 5;
		header[2] |= (widthAligned << 16);

		header[4] = width * height;
		header[5] = width * height * 0.5;

		struct file *filp;
		filp = filp_open("decoded_picture.gam", O_CREAT | O_RDWR, 0644);
		if (IS_ERR(filp)) {
			pr_alert("Oops ! filp_open failed\n");
		} else {
			mm_segment_t old_fs = get_fs();
			set_fs(get_ds());

			filp->f_op->write(filp, (char *) header, 24, &filp->f_pos);
			filp->f_op->write(filp, pInput->DataBuffers.pPicBufferY, header[4], &filp->f_pos);
			filp->f_op->write(filp, pInput->DataBuffers.pPicBufferC, header[5], &filp->f_pos);

			set_fs(old_fs);
			filp_close(filp, NULL);
			pr_alert("File Operations END\n");
		}
#endif /*defined(DEBUG_VIDEO)*/

#if defined(DEBUG_VIDEO)
		/*Now Unmap used Buffers*/
		iounmap((void *)pInput->DataBuffers.pPicBufferY);
		iounmap((void *)pInput->DataBuffers.pLastPicBufferY);
		iounmap((void *)pInput->DataBuffers.pGoldPicBufferY);
		iounmap((void *)pInput->DataBuffers.pAltPicBufferY);
#endif
		break;

	case MME_SET_GLOBAL_TRANSFORM_PARAMS:
		pr_err("Error: %s Should not receive a global transform command for VP8 \n", __func__);

		break;

	case MME_SEND_BUFFERS:
		break;

	default:
		break;
	}

	return MME_SUCCESS;
}

MME_ERROR VP8DEC_TermTransformer(void *context)
{
	Vp8HardInitParams_t *param = (Vp8HardInitParams_t *)context;
	if (param == NULL) {
		return MME_INTERNAL_ERROR;
	}

	VP8HwdAsicFreeMem(&param->decInstance);

	kfree(context);
	context = NULL;

	return MME_SUCCESS;
}

static int RegisterVp8Transformer()
{
	int ret = 0;
	MME_ERROR status;

	status =  MME_RegisterTransformer(
	                  VP8DECHW_MME_TRANSFORMER_NAME,
	                  VP8DEC_Abort,
	                  VP8DEC_GetTransformerCapability,
	                  VP8DEC_InitTransformer,
	                  VP8DEC_ProcessCommand,
	                  VP8DEC_TermTransformer);

	if (status != MME_SUCCESS) {
		return -EINVAL;
	}

	status =  MME_RegisterTransformer(
	                  VP8DECHW_MME_TRANSFORMER_NAME1,
	                  VP8DEC_Abort,
	                  VP8DEC_GetTransformerCapability,
	                  VP8DEC_InitTransformer,
	                  VP8DEC_ProcessCommand,
	                  VP8DEC_TermTransformer);

	if (status != MME_SUCCESS) {
		ret = -EINVAL;
	}

	return ret;
}

static int DeRegisterVp8Transformer()
{
	MME_ERROR status;
	int ret = 0;

	status = MME_DeregisterTransformer(VP8DECHW_MME_TRANSFORMER_NAME);
	if (status != MME_SUCCESS) {
		ret = -(ENODEV);
		goto fail_dereg;
	}

	status = MME_DeregisterTransformer(VP8DECHW_MME_TRANSFORMER_NAME1);
	if (status != MME_SUCCESS) {
		ret = -(ENODEV);
	}

fail_dereg:
	return ret;
}

