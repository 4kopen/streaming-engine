/************************************************************************
Copyright (C) 2003-2014 STMicroelectronics. All Rights Reserved.

This file is part of the Streaming Engine.

Streaming Engine is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License version 2 as
published by the Free Software Foundation.

Streaming Engine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with Streaming Engine; see the file COPYING.  If not, write to the Free
Software Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307,
USA.

The Streaming Engine Library may alternatively be licensed under a
proprietary license from ST.
************************************************************************/

#include <linux/sched.h>
#include <linux/module.h>
#include <linux/err.h>
#include <linux/suspend.h>
#include <linux/pm_runtime.h>
#include <linux/pm.h>
#include <linux/of.h>
#include <linux/clk.h>
#include <linux/interrupt.h>

#include "osdev_time.h"
#include "osdev_mem.h"
#include "osdev_sched.h"
#include "osdev_device.h"
#include "vp9_hard_host_transformer.h"
#include "vp9inline.h"

#define MODULE_NAME     "VP9 hardware host transformer"

MODULE_DESCRIPTION("VP9 decode hardware cell platform driver");
MODULE_AUTHOR("STMicroelectronics");
MODULE_LICENSE("GPL");

struct vp9_hX170dwl_core *vp9_core;
struct semaphore dec_core_sem;
struct semaphore pp_core_sem;
spinlock_t owner_lock = SPIN_LOCK_UNLOCKED;
DECLARE_WAIT_QUEUE_HEAD(dec_wait_queue);
static int dec_irq;

static int vp9_hx170_clock_on(struct vp9_hX170dwl_core *core);
static void vp9_hx170_clock_off(struct vp9_hX170dwl_core *core);
static int stm_pm_vp9_notifier_call(struct notifier_block *this, unsigned long event, void *ptr);

// PM notifier callback
// At entry of low power, multicom is terminated in .freeze callback. When system exit from low
// power, multicom is initialized through pm_notifier, so we have to register VP9 transformer again.
// After VP9 pm_notifier, SE module notifier would be called in which other multicom apis
// are called.
// This callback is used to register the VP9 transformer after low power exit
#define ENTRY(enum) case enum: return #enum
inline const char *StringifyPmEvent(int aPmEvent)
{
	switch (aPmEvent) {
		ENTRY(PM_HIBERNATION_PREPARE);
		ENTRY(PM_POST_HIBERNATION);
		ENTRY(PM_SUSPEND_PREPARE);
		ENTRY(PM_POST_SUSPEND);
		ENTRY(PM_RESTORE_PREPARE);
		ENTRY(PM_POST_RESTORE);
	default: return "<unknown pm event>";
	}
}

#define DWL_VP9_FUSE_E 6            /* 1 bit */

static int vp9_hw_read_config(struct vp9_hX170dwl_core *core)
{
	int ret = 0;
	int vp9_support_fuse = 0;
#ifdef CONFIG_PM_RUNTIME
	if (pm_runtime_get_sync(core->dev) < 0) {
		pr_err("Error: %s pm_runtime_get_sync failed\n", __func__);
		return VP9HWDEC_SYSTEM_ERROR;
	}
#endif
	atomic_inc(&core->task_nb);

	if (vp9_hx170_clock_on(core)) {
		pr_err("Error: %s failed to enable clock\n", __func__);
#ifdef CONFIG_PM_RUNTIME
		pm_runtime_mark_last_busy(core->dev);
		pm_runtime_put(core->dev);
#endif
		atomic_dec(&core->task_nb);
		return VP9HWDEC_SYSTEM_ERROR;
	}

	/* ASIC id */
	core->asic_id = ioread32(core->regs + 4 * VP9_HW_ASIC_ID);
	if (core->asic_id == 0) {
		ret = -EINVAL;
		pr_err("Error: %s asic id not read\n", __func__);
		goto fail_end;
	}

	/* DEC configuration */
	core->dec_cfg = ioread32(core->regs + 4 * VP9_HW_DEC_SYNTH_CFG);
	core->dec_cfg2 = ioread32(core->regs + 4 * VP9_HW_DEC_SYNTH_CFG_2);
	core->dec_cfg3 = ioread32(core->regs + 4 * VP9_HW_DEC_SYNTH_CFG_3);
	core->dec_fuse = ioread32(core->regs + 4 * VP9_HW_DEC_FUSE_CFG);
	vp9_support_fuse = (core->dec_fuse >> DWL_VP9_FUSE_E) & 0x01U;

	/* PP configuration */
	core->pp_cfg = ioread32(core->regs + 4 * VP9_HW_PP_SYNTH_CFG);
	core->pp_fuse = ioread32(core->regs + 4 * VP9_HW_PP_FUSE_CFG);

	vp9_hx170_clock_off(core);
	atomic_dec(&core->task_nb);

fail_end:
#ifdef CONFIG_PM_RUNTIME
	pm_runtime_mark_last_busy(core->dev);
	pm_runtime_put(core->dev);
#endif
	if (!vp9_support_fuse) {
		ret = -EINVAL;
		pr_err("Error: %s vp9 fuse disabled\n", __func__);
	}

	return ret;
}

static int vp9_hx170_clock_on(struct vp9_hX170dwl_core *core)
{
	int ret = clk_enable(core->clk);
	return ret;
}

static void vp9_hx170_clock_off(struct vp9_hX170dwl_core *core)
{
	clk_disable(core->clk);
}


static irqreturn_t vp9_hw_interrupt(int irq, void *dev)
{
	struct vp9_hX170dwl_core *core = dev;
	unsigned int irq_dec;
	unsigned int irq_pp;

	(void)irq; // warning removal

	/* there is two possible irq source decoder and post-processor */
	irq_dec = ioread32(core->regs + VP9_HW_DEC_IRQ_OFFSET);
	irq_pp = ioread32(core->regs + VP9_HW_PP_IRQ_OFFSET);

	if (irq_dec & VP9_HW_DEC_IRQ_MASK) {
		/* clear decoder irq */
		iowrite32(irq_dec & (~VP9_HW_DEC_IRQ_MASK),
		          (core->regs + VP9_HW_DEC_IRQ_OFFSET));
		dec_irq |= (1 << 0);
		wake_up_interruptible_all(&dec_wait_queue);
		pr_debug("Received interrupt from decoder\n");
	}

	if (irq_pp & VP9_HW_PP_IRQ_MASK) {
		/* clear post-processor irq */
		iowrite32(irq_pp & (~VP9_HW_PP_IRQ_MASK),
		          (core->regs + VP9_HW_PP_IRQ_OFFSET));
		pr_debug("Received interrupt from post-processor\n");
	}

	return IRQ_HANDLED;
}

static int CheckDecIrq(void)
{
	unsigned long flags;
	int rdy = 0;

	const u32 irq_mask = (1 << 0);

	spin_lock_irqsave(&owner_lock, flags);

	if (dec_irq & irq_mask) {
		/* reset the wait condition(s) */
		dec_irq &= ~irq_mask;
		rdy = 1;
	}

	spin_unlock_irqrestore(&owner_lock, flags);

	return rdy;
}

int Vp9WaitDecReadyAndRefreshTags(unsigned int *registers, unsigned int *duration)
{
	struct vp9_hX170dwl_core *core = vp9_core;
	unsigned int *regs = registers;
	int i = 0;

	if (wait_event_interruptible(dec_wait_queue, CheckDecIrq())) {
		pr_err("DEC  wait_event_interruptible interrupted\n");
		return -ERESTARTSYS;
	}
	core->end_time = OSDEV_GetTimeInMicroSeconds();

	/* read all registers from hardware */
	for (i = 0; i < VP9_MAX_REGS; i++) {
		regs[i] = ioread32(core->regs + i * 4);
	}
	*duration = core->end_time - core->start_time;

	return 0;
}



static int Vp9ClockSetRate(struct device *dev)
{
	int ret = 0;
	struct vp9_hX170dwl_core *core;

	core = dev_get_drvdata(dev);

	/* check incase 0 is set for max frequency property in DT */
	if (core && core->max_freq) {
		ret = clk_set_rate(core->clk, core->max_freq);
		if (ret) {
			pr_err("Error: %s setting max frequency failed (%d)\n", __func__, ret);
			return -EINVAL;
		}
		pr_info("Vp9 clock set to %u\n", core->max_freq);
	}
	return ret;
}

static int stm_pm_vp9_notifier_call(struct notifier_block *this, unsigned long event, void *ptr)
{
	(void)this; // warning removal
	(void)ptr; // warning removal

	switch (event) {
	case PM_POST_SUSPEND:
	// fallthrough
	case PM_POST_HIBERNATION:
		pr_info("%s notifier: %s\n", __func__, StringifyPmEvent(event));
		break;

	default:
		break;
	}

	return NOTIFY_DONE;
}

/**
 * Get platform data from Device Tree
 */
static int vp9_get_of_pdata(struct platform_device *pdev,
                            struct vp9_hX170dwl_core *core)
{
#ifdef CONFIG_OF
	int ret = 0;

	core->max_freq = 0;
	if (of_property_read_u32(pdev->dev.of_node, "clock-frequency", &core->max_freq)) {
		pr_info("Maximum vp9 clock frequency not retrieved from DT\n");
	}
#ifdef CONFIG_ARCH_STI
	core->clk = devm_clk_get(&pdev->dev, "clk_vp9");
#else
	struct device_node *np = pdev->dev.of_node;
	const char *clkName;

	of_property_read_string_index(np, "st,dev_clk", 0, &clkName);

	core->clk = devm_clk_get(&pdev->dev, clkName);
#endif
	if (IS_ERR(core->clk)) {
		pr_err("Error: %s failed to get clock\n", __func__);
		ret = -EINVAL;
		goto fail_end;
	}
	ret = Vp9ClockSetRate(&(pdev->dev));
	if (ret) {
		pr_err("Error: %s setting maximum clock frequency failed (%d)\n", __func__, ret);
	}

fail_end:
	return ret;
#else //CONFIG_OF
	return 0;
#endif
}

void Vp9GetConfiguration(struct vp9_config_regs *config)
{
	config->dec_cfg = (unsigned int)vp9_core->dec_cfg;
	config->dec_cfg2 = (unsigned int)vp9_core->dec_cfg2;
	config->dec_cfg3 = (unsigned int)vp9_core->dec_cfg3;
	config->dec_fuse = (unsigned int)vp9_core->dec_fuse;
	config->pp_cfg = (unsigned int)vp9_core->pp_cfg;
	config->pp_fuse = (unsigned int)vp9_core->pp_fuse;
	config->asic_id = (unsigned int)vp9_core->asic_id;
	config->reg_size = max(vp9_core->regs_size, VP9_MAX_REGS);
}

// Power management callbacks
static struct notifier_block stm_pm_vp9_notifier = {
	.notifier_call = stm_pm_vp9_notifier_call,
};

int Vp9HwRun(unsigned int *registers)
{
	struct vp9_hX170dwl_core *core = vp9_core;
	unsigned int *regs = registers;
	int i;
	BUG_ON(core == NULL);

	/* record start time when HW is getting enabled */
	if (regs[VP9_HW_DEC_IRQ_OFFSET / 4]) {
		core->start_time = OSDEV_GetTimeInMicroSeconds();
	}

	/* write all regs but the status reg[1] to hardware */
	for (i = 2; i < VP9_MAX_REGS; i++) {
		iowrite32(regs[i], core->regs  + i * 4);
	}

	/* write the status register, which may start the decoder and the postproc */
	iowrite32(regs[VP9_HW_DEC_IRQ_OFFSET / 4], core->regs + VP9_HW_DEC_IRQ_OFFSET);

	return 0;
}

/*
 * vp9_probe() - This routine loads the hx170 core driver
 *
 * @pdev: platform device.
 */
static int vp9_probe(struct platform_device *pdev)
{
	struct resource *iomem;
	unsigned char clk_alias[20];
	int ret = 0;

	BUG_ON(pdev == NULL);
	vp9_core = devm_kzalloc(&pdev->dev, sizeof(*vp9_core), GFP_KERNEL);
	if (!vp9_core) {
		pr_err("Error: %s core alloc failed\n", __func__);
		ret = -EINVAL;
		goto fail_vp9probe;
	}

	platform_set_drvdata(pdev, vp9_core);
	vp9_core->dev = &pdev->dev;

	iomem = platform_get_resource_byname(pdev, IORESOURCE_MEM, "vp9_regs");
	if (!iomem) {
		pr_err("Error: %s Failed to get DT registers property\n", __func__);
		ret = -EINVAL;
		goto fail_vp9probe;
	}

	vp9_core->regs_size = resource_size(iomem);
	vp9_core->regs = devm_ioremap_nocache(vp9_core->dev, iomem->start,
	                                      vp9_core->regs_size);
	if (!vp9_core->regs) {
		pr_err("Error: %s Can't ioremap hx170 (vp9) registers region\n", __func__);
		ret = -(ENOMEM);
		goto fail_vp9probe;
	}

	vp9_core->irq_its = platform_get_irq(pdev, 0);
	ret = devm_request_irq(&pdev->dev, vp9_core->irq_its, vp9_hw_interrupt,
	                       IRQF_SHARED, "VP9_irq", (void *)vp9_core);

	disable_irq(vp9_core->irq_its);

	if (pdev->dev.of_node) {
		ret = vp9_get_of_pdata(pdev, vp9_core);
		if (ret) {
			pr_err("Error: %s Probing device with DT failed\n", __func__);
			goto fail_vp9probe;
		}
	} else {
		pr_warn("warning: %s without DT\n", __func__);
		snprintf(clk_alias, sizeof(clk_alias), "clk_vp9");
		clk_alias[sizeof(clk_alias) - 1] = '\0';

		clk_add_alias(clk_alias, NULL, "CLK_VP9", NULL);
		vp9_core->clk = devm_clk_get(&pdev->dev, clk_alias);
		if (IS_ERR(vp9_core->clk)) {
			pr_err("Error: %s Unable to get clock\n", __func__);
			ret = PTR_ERR(vp9_core->clk);
			goto fail_vp9probe;
		}
	}

	ret = clk_prepare(vp9_core->clk);
	if (ret) {
		pr_err("Error: %s Failed to prepare clock\n", __func__);
		goto fail_vp9probe;
	}

	/* initialisation of semaphores */
	sema_init(&dec_core_sem, 1);
	sema_init(&pp_core_sem, 1);


#ifdef CONFIG_PM_RUNTIME
	pm_runtime_set_suspended(&pdev->dev);
	pm_suspend_ignore_children(&pdev->dev, 1);
	pm_runtime_enable(&pdev->dev);
#endif

	ret = vp9_hw_read_config(vp9_core);
	if (ret) {
		pr_err("Error: %s Failed to check configuration\n", __func__);
		goto vp9_fail_unprep;
	}

	// Register with PM notifiers (for HPS/CPS support)
	register_pm_notifier(&stm_pm_vp9_notifier);

	OSDEV_Dump_MemCheckCounters(__func__);
	pr_info("%s probe done ok\n", MODULE_NAME);

	return 0;

vp9_fail_unprep:
#ifdef CONFIG_PM_RUNTIME
	pm_runtime_disable(&pdev->dev);
#endif
	disable_irq(vp9_core->irq_its);
	clk_unprepare(vp9_core->clk);
fail_vp9probe:
	OSDEV_Dump_MemCheckCounters(__func__);
	pr_err("Error: %s probe failed\n", MODULE_NAME);
	return ret;
}

/*
 * vp9_remove() - Module exit function for the hx170 driver
 */
static int vp9_remove(struct platform_device *pdev)
{
	struct vp9_hX170dwl_core *core;
	BUG_ON(pdev == NULL);
	core = dev_get_drvdata(&pdev->dev);
	BUG_ON(core == NULL);

#ifdef CONFIG_PM_RUNTIME
	pm_runtime_disable(&pdev->dev);
#endif
	unregister_pm_notifier(&stm_pm_vp9_notifier);

	disable_irq(core->irq_its);
	clk_unprepare(core->clk);

	dev_set_drvdata(&pdev->dev, NULL);

	OSDEV_Dump_MemCheckCounters(__func__);
	pr_info("%s remove done\n", MODULE_NAME);
	return 0;
}

// ////////////////////////////////PM///////////////////////////////////////////
#ifdef CONFIG_PM
#ifdef CONFIG_PM_RUNTIME
/**
 * pm_runtime_suspend callback for Active Standby
 * called if pm_runtime ref counter == 0 (and for Active Standby)
 */
static int vp9_pm_runtime_suspend(struct device *dev)
{
	(void)dev; // warning removal
	return 0;
}

/**
 * pm_runtime_resume callback for Active Standby
 * called if pm_runtime ref counter == 0 (and for Active Standby)
 */
static int vp9_pm_runtime_resume(struct device *dev)
{
	(void)dev; // warning removal
	return 0;
}
#endif // CONFIG_PM_RUNTIME

/**
 * PM suspend callback
 * For Gateway profile: called on Host Passive Standby entry (HPS entry)
 * For STB profile: called on Controller Passive Standby entry (CPS entry)
 */
static int vp9_pm_suspend(struct device *dev)
{
	struct vp9_hX170dwl_core *core;

	core = dev_get_drvdata(dev);
	BUG_ON(core == NULL);

	pr_info("%s\n", __func__);
	/**
	* Here we assume that there are no more jobs in progress
	* This is ensured at SE level, which has already entered "low power state",
	* in PM notifier callback of player2 module (on PM_SUSPEND_PREPARE event)
	 */
	if (atomic_read(&core->task_nb)) {
		pr_warn("warning: core used (should not happen) in %s\n", __func__);
	}

	return 0;
}

/**
 * PM resume callback
 * For Gateway profile: called on Host Passive Standby exit (HPS exit)
 * For STB profile: called on Controller Passive Standby exit (CPS exit)
 */
static int vp9_pm_resume(struct device *dev)
{
	(void)dev; // warning removal
	pr_info("%s\n", __func__);
	return 0;
}

static struct dev_pm_ops vp9_pm_ops = {
	.suspend = vp9_pm_suspend,
	.resume  = vp9_pm_resume,
#ifdef CONFIG_PM_RUNTIME
	.runtime_suspend  = vp9_pm_runtime_suspend,
	.runtime_resume   = vp9_pm_runtime_resume,
#endif
};
#endif // CONFIG_PM

#ifdef CONFIG_OF
static struct of_device_id stm_vp9_match[] = {
	{
		.compatible = "st,se-hx_170",
	},
	{},
};
#endif

static struct platform_driver platform_vp9_driver = {
	.driver = {
		.owner = THIS_MODULE,
		.name = "vp9",
		.of_match_table = of_match_ptr(stm_vp9_match),
#ifdef CONFIG_PM
		.pm = &vp9_pm_ops,
#endif
	},
	.probe = vp9_probe,
	.remove = vp9_remove,
};

int Vp9MallocLinearBuffer(unsigned int **tempVirtual, unsigned int *tempPhysAddr, unsigned long size)
{
	int ret = 0;
	BUG_ON(tempVirtual == NULL);
	*tempVirtual = (unsigned int *)OSDEV_AllignedMallocHwBuffer(ALIGNMENT, size, BPA_PARTITION_NAME,
	                                                            (unsigned long *)tempPhysAddr, UNCACHED_TYPE);
	if (*tempVirtual == NULL) {
		pr_err("Error: %s Unable to allocate memory\n", __func__);
		ret = -ENOMEM;
	}
	return ret;
}

void Vp9FreeLinearBuffer(unsigned int *virtualAddr)
{
	OSDEV_AllignedFreeHwBuffer((void *) virtualAddr, BPA_PARTITION_NAME);
}

int Vp9ReserveHW(int type)
{
	struct vp9_hX170dwl_core *core = vp9_core;
	BUG_ON(core == NULL);

	if (type == VP9_DEC_USED) {
		if (down_interruptible(&dec_core_sem)) {
			return -ERESTARTSYS;
		}
	} else { /* PP */
		if (down_interruptible(&pp_core_sem)) {
			return -ERESTARTSYS;
		}
	}

#ifdef CONFIG_PM_RUNTIME
	if (pm_runtime_get_sync(core->dev) < 0) {
		pr_err("Error: %s pm_runtime_get_sync failed\n", __func__);
		return VP9HWDEC_SYSTEM_ERROR;
	}
#endif
	atomic_inc(&core->task_nb);

	if (vp9_hx170_clock_on(core)) {
		pr_err("Error: %s failed to enable clock\n", __func__);
#ifdef CONFIG_PM_RUNTIME
		pm_runtime_mark_last_busy(core->dev);
		pm_runtime_put(core->dev);
#endif
		atomic_dec(&core->task_nb);
		return VP9HWDEC_SYSTEM_ERROR;
	}

	/* enable irq */
	enable_irq(vp9_core->irq_its);

	return 0;
}

void Vp9ReleaseHW(int type)
{
	struct vp9_hX170dwl_core *core = vp9_core;
	BUG_ON(core == NULL);

	/* disable irq */
	disable_irq(vp9_core->irq_its);

	vp9_hx170_clock_off(core);
	atomic_dec(&core->task_nb);

#ifdef CONFIG_PM_RUNTIME
	pm_runtime_mark_last_busy(core->dev);
	pm_runtime_put(core->dev);
#endif

	if (type == VP9_DEC_USED) {
		up(&dec_core_sem);
	} else { /* PP */
		up(&pp_core_sem);
	}
}

module_platform_driver(platform_vp9_driver);
EXPORT_SYMBOL(Vp9GetConfiguration);
EXPORT_SYMBOL(Vp9HwRun);
EXPORT_SYMBOL(Vp9MallocLinearBuffer);
EXPORT_SYMBOL(Vp9FreeLinearBuffer);
EXPORT_SYMBOL(Vp9ReserveHW);
EXPORT_SYMBOL(Vp9ReleaseHW);
EXPORT_SYMBOL(Vp9WaitDecReadyAndRefreshTags);
