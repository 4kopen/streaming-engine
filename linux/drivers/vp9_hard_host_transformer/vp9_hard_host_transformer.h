/************************************************************************
Copyright (C) 2003-2014 STMicroelectronics. All Rights Reserved.

This file is part of the Streaming Engine.

Streaming Engine is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License version 2 as
published by the Free Software Foundation.

Streaming Engine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with Streaming Engine; see the file COPYING.  If not, write to the Free
Software Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307,
USA.

The Streaming Engine Library may alternatively be licensed under a
proprietary license from ST.
************************************************************************/

#ifndef _VP9_HARD_HOST_H__
#define _VP9_HARD_HOST_H__

/* define irq registers offset and masks */
#define VP9_HW_DEC_IRQ_OFFSET      (1*4)
#define VP9_HW_PP_IRQ_OFFSET       (60*4)
#define VP9_HW_DEC_IRQ_MASK         0x100
#define VP9_HW_PP_IRQ_MASK          0x100

#define VP9_HW_ASIC_ID             0

#define VP9_HW_PP_SYNTH_CFG        60
#define VP9_HW_DEC_SYNTH_CFG       50
#define VP9_HW_DEC_SYNTH_CFG_2     54
#define VP9_HW_DEC_SYNTH_CFG_3     56

#define VP9_HW_PP_FUSE_CFG         99
#define VP9_HW_DEC_FUSE_CFG        57


/**
 * Global vp9_core structure (containing info about device...)
 */
struct vp9_hX170dwl_core {
	struct device          *dev;
	unsigned int           chip_id;
	unsigned int           irq_its;
	void __iomem *regs;
	int                    regs_size;
	struct clk             *clk;
	atomic_t               task_nb;
	unsigned int           max_freq;
	atomic_t           instance_nb;
	unsigned int       start_time;
	unsigned int       end_time;
	/* config */
	u32 asic_id;
	u32 dec_cfg;
	u32 dec_cfg2;
	u32 dec_cfg3;
	u32 dec_fuse;
	u32 pp_cfg;
	u32 pp_fuse;
};

/* register set is 184 for normal decoding + 5 for down scaler */
#define VP9_MAX_REGS                 189

#define VP9HWDEC_SYSTEM_ERROR 0x0200
#define ALIGNMENT 16
#define BPA_PARTITION_NAME "vid-macroblock-0"

#define SPIN_LOCK_UNLOCKED      __SPIN_LOCK_UNLOCKED(old_style_spin_init)

#endif /* _VP9_HARD_HOST_H__ */
