/************************************************************************
Copyright (C) 2003-2014 STMicroelectronics. All Rights Reserved.

This file is part of the Streaming Engine.

Streaming Engine is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License version 2 as
published by the Free Software Foundation.

Streaming Engine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with Streaming Engine; see the file COPYING.  If not, write to the Free
Software Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307,
USA.

The Streaming Engine Library may alternatively be licensed under a
proprietary license from ST.
************************************************************************/

#ifndef _H_VP9INLINE__
#define _H_VP9INLINE__

#include "osdev_user.h"

#define VP9_DEC_USED 0
#define VP9_PP_USED  1

struct vp9_config_regs {
	unsigned int asic_id;
	unsigned int dec_cfg;
	unsigned int dec_cfg2;
	unsigned int dec_cfg3;
	unsigned int dec_fuse;
	unsigned int pp_cfg;
	unsigned int pp_fuse;
	unsigned int reg_size;
};

void Vp9GetConfiguration(struct vp9_config_regs *config);
int  Vp9HwRun(unsigned int *);
int  Vp9MallocLinearBuffer(unsigned int **tempVirtualAddr, unsigned int *tempPhysAddr, unsigned long size);
void Vp9FreeLinearBuffer(unsigned int *virtualAddr);
void Vp9ReleaseHW(int type);
int  Vp9ReserveHW(int type);
int  Vp9WaitDecReadyAndRefreshTags(unsigned int *registers, unsigned int *duration);

#endif /* _H_VP9INLINE__ */