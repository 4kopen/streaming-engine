/************************************************************************
Copyright (C) 2003-2014 STMicroelectronics. All Rights Reserved.

This file is part of the Streaming Engine.

Streaming Engine is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License version 2 as
published by the Free Software Foundation.

Streaming Engine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with Streaming Engine; see the file COPYING.  If not, write to the Free
Software Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307,
USA.

The Streaming Engine Library may alternatively be licensed under a
proprietary license from ST.
************************************************************************/

#include <linux/module.h>

#include "stm_se.h"

MODULE_AUTHOR("STMicroelectronics");
MODULE_LICENSE("GPL");

/* stubs for streaming engine */
MODULE_DESCRIPTION("streaming engine stubs");

int             stm_se_audio_generator_new(const char *name,
                                           stm_se_audio_generator_h *audio_generator)
{
	return 0;
}
int             stm_se_audio_generator_delete(stm_se_audio_generator_h audio_generator)
{
	return 0;
}
int             stm_se_audio_generator_attach(stm_se_audio_generator_h audio_generator,
                                              stm_se_audio_mixer_h sink)
{
	return 0;
}
int             stm_se_audio_generator_detach(stm_se_audio_generator_h audio_generator,
                                              stm_se_audio_mixer_h sink)
{
	return 0;
}
int             stm_se_audio_generator_set_compound_control(stm_se_audio_generator_h audio_generator,
                                                            stm_se_ctrl_t ctrl, const void *value)
{
	return 0;
}
int             stm_se_audio_generator_get_compound_control(stm_se_audio_generator_h audio_generator,
                                                            stm_se_ctrl_t ctrl, void *value)
{
	return 0;
}
int             stm_se_audio_generator_set_control(stm_se_audio_generator_h audio_generator,
                                                   stm_se_ctrl_t ctrl, int32_t value)
{
	return 0;
}
int             stm_se_audio_generator_get_control(stm_se_audio_generator_h audio_generator,
                                                   stm_se_ctrl_t ctrl, int32_t *value)
{
	return 0;
}
int             stm_se_audio_generator_get_info(stm_se_audio_generator_h audio_generator,
                                                stm_se_audio_generator_info_t *info)
{
	return 0;
}
int             stm_se_audio_generator_commit(stm_se_audio_generator_h audio_generator,
                                              uint32_t nspl)
{
	return 0;
}
int             stm_se_audio_generator_start(stm_se_audio_generator_h audio_generator)
{
	return 0;
}
int             stm_se_audio_generator_stop(stm_se_audio_generator_h audio_generator)
{
	return 0;
}
int             stm_se_advanced_audio_mixer_new(const char                *name,
                                                const stm_se_mixer_spec_t  topology,
                                                stm_se_audio_mixer_h      *audio_mixer)
{
	return 0;
}
int             stm_se_audio_mixer_new(const char                     *name,
                                       stm_se_audio_mixer_h           *audio_mixer)
{
	return 0;
}
int             stm_se_audio_mixer_delete(stm_se_audio_mixer_h            audio_mixer)
{
	return 0;
}
int             stm_se_audio_mixer_set_control(stm_se_audio_mixer_h           audio_mixer,
                                               stm_se_ctrl_t                  ctrl,
                                               int32_t                        value)
{
	return 0;
}
int             stm_se_audio_mixer_get_control(stm_se_audio_mixer_h           audio_mixer,
                                               stm_se_ctrl_t                  ctrl,
                                               int32_t                        *value)
{
	return 0;
}
int             stm_se_audio_mixer_set_compound_control(stm_se_audio_mixer_h   audio_mixer,
                                                        stm_se_ctrl_t                   ctrl,
                                                        const void                     *value)
{
	return 0;
}
int             stm_se_audio_mixer_get_compound_control(stm_se_audio_mixer_h   audio_mixer,
                                                        stm_se_ctrl_t                   ctrl,
                                                        void                           *value)
{
	return 0;
}
int             stm_se_audio_mixer_attach(stm_se_audio_mixer_h               audio_mixer,
                                          stm_se_audio_player_h               audio_player)
{
	return 0;
}
int             stm_se_audio_mixer_detach(stm_se_audio_mixer_h               audio_mixer,
                                          stm_se_audio_player_h               audio_player)
{
	return 0;
}

int             stm_se_audio_player_new(const char                         *name,
                                        const char                          *hw_name,
                                        stm_se_audio_player_h               *audio_player)
{
	return 0;
}
int             stm_se_audio_player_delete(stm_se_audio_player_h              audio_player)
{
	return 0;
}
int             stm_se_audio_player_set_compound_control(stm_se_audio_player_h    audio_player,
                                                         stm_se_ctrl_t             ctrl,
                                                         const void                *value)
{
	return 0;
}

int             stm_se_audio_player_get_compound_control(stm_se_audio_player_h    audio_player,
                                                         stm_se_ctrl_t             ctrl,
                                                         void                      *value)
{
	return 0;
}

int             stm_se_audio_player_set_control(stm_se_audio_player_h    audio_player,
                                                stm_se_ctrl_t             ctrl,
                                                const int32_t             value)
{
	return 0;
}

int             stm_se_audio_player_get_control(stm_se_audio_player_h    audio_player,
                                                stm_se_ctrl_t             ctrl,
                                                int32_t                  *value)
{
	return 0;
}
int             stm_se_audio_reader_new(const char                         *name,
                                        const char                          *hw_name,
                                        stm_se_audio_reader_h               *audio_reader)
{
	return 0;
}
int             stm_se_audio_reader_delete(stm_se_audio_reader_h              audio_reader)
{
	return 0;
}
int             stm_se_audio_reader_attach(stm_se_audio_reader_h              audio_reader,
                                           stm_se_play_stream_h                play_stream)
{
	return 0;
}
int             stm_se_audio_reader_detach(stm_se_audio_reader_h              audio_reader,
                                           stm_se_play_stream_h                play_stream)
{
	return 0;
}
int             stm_se_audio_reader_set_compound_control(stm_se_audio_reader_h    audio_reader,
                                                         stm_se_ctrl_t             ctrl,
                                                         const void                *value)
{
	return 0;
}
int             stm_se_audio_reader_get_compound_control(stm_se_audio_reader_h    audio_reader,
                                                         stm_se_ctrl_t             ctrl,
                                                         void                      *value)
{
	return 0;
}
int             stm_se_audio_reader_set_control(stm_se_audio_reader_h     audio_reader,
                                                stm_se_ctrl_t             ctrl,
                                                const int32_t             value)
{
	return 0;
}
int             stm_se_audio_reader_get_control(stm_se_audio_reader_h     audio_reader,
                                                stm_se_ctrl_t             ctrl,
                                                int32_t                  *value)
{
	return 0;
}
int             stm_se_component_set_module_parameters(component_handle_t      Component,
                                                       void                    *Data,
                                                       unsigned int            Size)
{
	return 0;
}
stream_buffer_capture_callback  stm_se_play_stream_register_buffer_capture_callback(
        stm_se_play_stream_h            play_stream,
        stm_se_event_context_h          context,
        stream_buffer_capture_callback  callback)
{
	return 0;
}
int             stm_se_encode_new(const char                      *name,
                                  stm_se_encode_h                 *encode)
{
	return 0;
}
int             stm_se_encode_delete(stm_se_encode_h                  encode)
{
	return 0;
}
int             stm_se_encode_get_control(stm_se_encode_h                  encode,
                                          stm_se_ctrl_t                    ctrl,
                                          int32_t                         *value)
{
	return 0;
}
int             stm_se_encode_set_control(stm_se_encode_h                  encode,
                                          stm_se_ctrl_t                    ctrl,
                                          int32_t                          value)
{
	return 0;
}
int             stm_se_encode_stream_new(const char                      *name,
                                         stm_se_encode_h                  encode,
                                         stm_se_encode_stream_encoding_t  encoding,
                                         stm_se_encode_stream_h          *encode_stream)
{
	return 0;
}
int             stm_se_encode_stream_delete(stm_se_encode_stream_h           encode_stream)
{
	return 0;
}

int             stm_se_encode_stream_attach(stm_se_encode_stream_h           encode_stream,
                                            stm_object_h                     sink)
{
	return 0;
}
int             stm_se_encode_stream_detach(stm_se_encode_stream_h           encode_stream,
                                            stm_object_h                     sink)
{
	return 0;
}
int             stm_se_encode_stream_flush(stm_se_encode_stream_h           encode_stream)
{
	return 0;
}
int             stm_se_encode_stream_get_control(stm_se_encode_stream_h           encode_stream,
                                                 stm_se_ctrl_t                    ctrl,
                                                 int32_t                         *value)
{
	return 0;
}
int             stm_se_encode_stream_set_control(stm_se_encode_stream_h           encode_stream,
                                                 stm_se_ctrl_t                    ctrl,
                                                 int32_t                          value)
{
	return 0;
}
int             stm_se_encode_stream_get_compound_control(stm_se_encode_stream_h    encode_stream,
                                                          stm_se_ctrl_t                    ctrl,
                                                          void                            *value)
{
	return 0;
}
int             stm_se_encode_stream_set_compound_control(stm_se_encode_stream_h    encode_stream,
                                                          stm_se_ctrl_t                    ctrl,
                                                          const void                      *value)
{
	return 0;
}
int             stm_se_encode_stream_drain(stm_se_encode_stream_h           encode_stream,
                                           bool                             discard)
{
	return 0;
}
int             stm_se_encode_stream_inject_frame(stm_se_encode_stream_h           encode_stream,
                                                  const void                      *frame_virtual_address,
                                                  unsigned long                    frame_physical_address,
                                                  uint32_t                         frame_length,
                                                  const stm_se_uncompressed_frame_metadata_t metadata)
{
	return 0;
}
int             stm_se_encode_stream_inject_discontinuity(stm_se_encode_stream_h    encode_stream,
                                                          stm_se_discontinuity_t    discontinuity)
{
	return 0;
}
int             __stm_se_playback_reset_statistics(stm_se_playback_h    Playback)
{
	return 0;
}
int             __stm_se_playback_get_statistics(stm_se_playback_h    Playback,
                                                 struct __stm_se_playback_statistics_s     *Statistics)
{
	return 0;
}
int             __stm_se_play_stream_reset_statistics(stm_se_play_stream_h    Stream)
{
	return 0;
}
int             __stm_se_play_stream_get_statistics(stm_se_play_stream_h    Stream,
                                                    struct statistics_s     *Statistics)
{
	return 0;
}
int             __stm_se_play_stream_reset_attributes(stm_se_play_stream_h    Stream)
{
	return 0;
}
int             __stm_se_play_stream_get_attributes(stm_se_play_stream_h    Stream,
                                                    struct attributes_s     *Attributes)
{
	return 0;
}
int             __stm_se_encode_stream_reset_statistics(stm_se_encode_stream_h    EncodeStream)
{
	return 0;
}
int             __stm_se_encode_stream_get_statistics(stm_se_encode_stream_h encode_stream,
                                                      struct encode_stream_statistics_s *Statistics)
{
	return 0;
}
int             __stm_se_audio_mixer_update_transformer_id(unsigned int mixerId,
                                                           const char *transformerName)
{
	return 0;
}
int __stm_se_pm_low_power_enter(void)
{
	return 0;
}
int __stm_se_pm_low_power_exit(void)
{
	return 0;
}
int             stm_se_playback_new(const char             *name,
                                    stm_se_playback_h      *playback)
{
	return 0;
}
int             stm_se_playback_delete(stm_se_playback_h       playback)
{
	return 0;
}
int             stm_se_playback_set_speed(stm_se_playback_h       playback,
                                          int32_t                 speed)
{
	return 0;
}
int             stm_se_playback_get_speed(stm_se_playback_h       playback,
                                          int32_t                *speed)
{
	return 0;
}
int             stm_se_playback_set_control(stm_se_playback_h       playback,
                                            stm_se_ctrl_t           ctrl,
                                            int32_t                 value)
{
	return 0;
}
int             stm_se_playback_get_control(stm_se_playback_h       playback,
                                            stm_se_ctrl_t           ctrl,
                                            int32_t                *value)
{
	return 0;
}
int             stm_se_playback_set_native_time(stm_se_playback_h       playback,
                                                uint64_t                native_time,
                                                unsigned long long      system_time)
{
	return 0;
}
int             stm_se_playback_set_clock_data_point(stm_se_playback_h       playback,
                                                     stm_se_time_format_t    time_format,
                                                     bool                    new_sequence,
                                                     uint64_t                source_time,
                                                     uint64_t                system_time)
{
	return 0;
}
int             stm_se_playback_get_clock_data_point(stm_se_playback_h       playback,
                                                     uint64_t               *source_time,
                                                     uint64_t               *system_time)
{
	return 0;
}
int             stm_se_play_stream_new(const char             *name,
                                       stm_se_playback_h       playback,
                                       stm_se_stream_encoding_t encoding,
                                       stm_se_play_stream_h        *play_stream)
{
	return 0;
}
int             stm_se_play_stream_delete(stm_se_play_stream_h    play_stream)
{
	return 0;
}
int             stm_se_play_stream_attach(stm_se_play_stream_h    play_stream,
                                          stm_object_h            sink,
                                          stm_se_play_stream_output_port_t data_type)
{
	return 0;
}
int             stm_se_play_stream_attach_to_pad(stm_se_play_stream_h    play_stream,
                                                 stm_object_h            sink,
                                                 stm_se_play_stream_output_port_t data_type,
                                                 stm_se_sink_input_port_t         input_port)
{
	return 0;
}
int             stm_se_play_stream_detach(stm_se_play_stream_h    play_stream,
                                          stm_object_h            sink)
{
	return 0;
}
int             stm_se_play_stream_inject_data(stm_se_play_stream_h    play_stream,
                                               const void             *data,
                                               uint32_t                data_length)
{
	return 0;
}
int             stm_se_play_stream_inject_discontinuity(stm_se_play_stream_h play_stream,
                                                        bool                 smooth_reverse,
                                                        bool                 surplus_data,
                                                        bool                 end_of_stream)
{
	return 0;
}
int             stm_se_play_stream_inject_discontinuity_mask(stm_se_play_stream_h play_stream, int discontinuity)
{
	return 0;
}
int             stm_se_play_stream_get_enable(stm_se_play_stream_h    play_stream,
                                              bool                   *enable)
{
	return 0;
}
int             stm_se_play_stream_drain(stm_se_play_stream_h    play_stream,
                                         unsigned int            discard)
{
	return 0;
}
int             stm_se_play_stream_set_enable(stm_se_play_stream_h    play_stream,
                                              bool                    enable)
{
	return 0;
}
int             stm_se_play_stream_set_control(stm_se_play_stream_h    play_stream,
                                               stm_se_ctrl_t           ctrl,
                                               int32_t                 value)
{
	return 0;
}
int             stm_se_play_stream_get_control(stm_se_play_stream_h    play_stream,
                                               stm_se_ctrl_t           ctrl,
                                               int32_t                *value)
{
	return 0;
}
int             stm_se_play_stream_step(stm_se_play_stream_h    play_stream)
{
	return 0;
}
int             stm_se_play_stream_switch(stm_se_play_stream_h    play_stream,
                                          stm_se_stream_encoding_t encoding)
{
	return 0;
}
int             stm_se_play_stream_get_info(stm_se_play_stream_h    play_stream,
                                            stm_se_play_stream_info_t *info)
{
	return 0;
}
int             stm_se_play_stream_set_interval(stm_se_play_stream_h    play_stream,
                                                unsigned long long      start,
                                                unsigned long long      end)
{
	return 0;
}
int             stm_se_play_stream_poll_message(stm_se_play_stream_h       play_stream,
                                                stm_se_play_stream_msg_id_t id,
                                                stm_se_play_stream_msg_t   *message)
{
	return 0;
}
int             stm_se_play_stream_get_message(stm_se_play_stream_h              play_stream,
                                               stm_se_play_stream_subscription_h subscription,
                                               stm_se_play_stream_msg_t         *message)
{
	return 0;
}

int             stm_se_play_stream_subscribe(stm_se_play_stream_h               play_stream,
                                             uint32_t                           message_mask,
                                             uint32_t                           depth,
                                             stm_se_play_stream_subscription_h *subscription)
{
	return 0;
}
int             stm_se_play_stream_unsubscribe(stm_se_play_stream_h               play_stream,
                                               stm_se_play_stream_subscription_h  subscription)
{
	return 0;
}

int             stm_se_play_stream_get_compound_control(stm_se_play_stream_h      play_stream,
                                                        stm_se_ctrl_t               ctrl,
                                                        void                        *value)
{
	return 0;
}
int             stm_se_play_stream_set_compound_control(stm_se_play_stream_h      play_stream,
                                                        stm_se_ctrl_t               ctrl,
                                                        void                        *value)
{
	return 0;
}
int             stm_se_play_stream_set_alarm(stm_se_play_stream_h         play_stream,
                                             stm_se_play_stream_alarm_t   alarm,
                                             bool                         enable,
                                             void                        *value)
{
	return 0;
}
int             stm_se_play_stream_set_discard_trigger(stm_se_play_stream_h                  play_stream,
                                                       stm_se_play_stream_discard_trigger_t *trigger)
{
	return 0;
}
int             stm_se_play_stream_reset_discard_triggers(stm_se_play_stream_h play_stream)
{
	return 0;
}
int             stm_se_set_control(stm_se_ctrl_t           ctrl,
                                   int32_t                 value)
{
	return 0;
}
int             stm_se_get_control(stm_se_ctrl_t           ctrl,
                                   int32_t                *value)
{
	return 0;
}
int             stm_se_get_compound_control(stm_se_ctrl_t               ctrl,
                                            void                        *value)
{
	return 0;
}
int             stm_se_set_error_handler(void                   *ctx,
                                         stm_error_handler       handler)
{
	return 0;
}

EXPORT_SYMBOL(stm_se_set_control);
EXPORT_SYMBOL(stm_se_get_control);
EXPORT_SYMBOL(stm_se_get_compound_control);
EXPORT_SYMBOL(stm_se_set_error_handler);
EXPORT_SYMBOL(stm_se_playback_new);
EXPORT_SYMBOL(stm_se_playback_delete);
EXPORT_SYMBOL(stm_se_playback_set_control);
EXPORT_SYMBOL(stm_se_playback_get_control);
EXPORT_SYMBOL(stm_se_playback_set_speed);
EXPORT_SYMBOL(stm_se_playback_get_speed);
EXPORT_SYMBOL(stm_se_playback_set_native_time);
EXPORT_SYMBOL(stm_se_playback_set_clock_data_point);
EXPORT_SYMBOL(stm_se_playback_get_clock_data_point);
EXPORT_SYMBOL(stm_se_play_stream_new);
EXPORT_SYMBOL(stm_se_play_stream_delete);
EXPORT_SYMBOL(stm_se_play_stream_set_control);
EXPORT_SYMBOL(stm_se_play_stream_get_control);
EXPORT_SYMBOL(stm_se_play_stream_set_enable);
EXPORT_SYMBOL(stm_se_play_stream_get_enable);
EXPORT_SYMBOL(stm_se_play_stream_get_compound_control);
EXPORT_SYMBOL(stm_se_play_stream_set_compound_control);
EXPORT_SYMBOL(stm_se_play_stream_attach);
EXPORT_SYMBOL(stm_se_play_stream_attach_to_pad);
EXPORT_SYMBOL(stm_se_play_stream_detach);
EXPORT_SYMBOL(stm_se_play_stream_inject_data);
EXPORT_SYMBOL(stm_se_play_stream_inject_discontinuity);
EXPORT_SYMBOL(stm_se_play_stream_inject_discontinuity_mask);
EXPORT_SYMBOL(stm_se_play_stream_drain);
EXPORT_SYMBOL(stm_se_play_stream_step);
EXPORT_SYMBOL(stm_se_play_stream_switch);
EXPORT_SYMBOL(stm_se_play_stream_get_info);
EXPORT_SYMBOL(stm_se_play_stream_set_interval);
EXPORT_SYMBOL(stm_se_play_stream_poll_message);
EXPORT_SYMBOL(stm_se_play_stream_get_message);
EXPORT_SYMBOL(stm_se_play_stream_subscribe);
EXPORT_SYMBOL(stm_se_play_stream_unsubscribe);
EXPORT_SYMBOL(stm_se_play_stream_register_buffer_capture_callback);
EXPORT_SYMBOL(stm_se_play_stream_set_alarm);
EXPORT_SYMBOL(stm_se_play_stream_set_discard_trigger);
EXPORT_SYMBOL(stm_se_play_stream_reset_discard_triggers);
EXPORT_SYMBOL(stm_se_advanced_audio_mixer_new);
EXPORT_SYMBOL(stm_se_audio_mixer_new);
EXPORT_SYMBOL(stm_se_audio_mixer_delete);
EXPORT_SYMBOL(stm_se_audio_mixer_set_control);
EXPORT_SYMBOL(stm_se_audio_mixer_get_control);
EXPORT_SYMBOL(stm_se_audio_mixer_set_compound_control);
EXPORT_SYMBOL(stm_se_audio_mixer_get_compound_control);
EXPORT_SYMBOL(stm_se_audio_mixer_attach);
EXPORT_SYMBOL(stm_se_audio_mixer_detach);
EXPORT_SYMBOL(stm_se_component_set_module_parameters);
EXPORT_SYMBOL(stm_se_audio_generator_new);
EXPORT_SYMBOL(stm_se_audio_generator_delete);
EXPORT_SYMBOL(stm_se_audio_generator_attach);
EXPORT_SYMBOL(stm_se_audio_generator_detach);
EXPORT_SYMBOL(stm_se_audio_generator_get_compound_control);
EXPORT_SYMBOL(stm_se_audio_generator_set_compound_control);
EXPORT_SYMBOL(stm_se_audio_generator_get_control);
EXPORT_SYMBOL(stm_se_audio_generator_set_control);
EXPORT_SYMBOL(stm_se_audio_generator_get_info);
EXPORT_SYMBOL(stm_se_audio_generator_commit);
EXPORT_SYMBOL(stm_se_audio_generator_start);
EXPORT_SYMBOL(stm_se_audio_generator_stop);
EXPORT_SYMBOL(stm_se_audio_reader_new);
EXPORT_SYMBOL(stm_se_audio_reader_delete);
EXPORT_SYMBOL(stm_se_audio_reader_attach);
EXPORT_SYMBOL(stm_se_audio_reader_detach);
EXPORT_SYMBOL(stm_se_audio_reader_get_compound_control);
EXPORT_SYMBOL(stm_se_audio_reader_set_compound_control);
EXPORT_SYMBOL(stm_se_audio_reader_get_control);
EXPORT_SYMBOL(stm_se_audio_reader_set_control);
EXPORT_SYMBOL(stm_se_audio_player_new);
EXPORT_SYMBOL(stm_se_audio_player_delete);
EXPORT_SYMBOL(stm_se_audio_player_set_control);
EXPORT_SYMBOL(stm_se_audio_player_get_control);
EXPORT_SYMBOL(stm_se_audio_player_set_compound_control);
EXPORT_SYMBOL(stm_se_audio_player_get_compound_control);
EXPORT_SYMBOL(stm_se_encode_new);
EXPORT_SYMBOL(stm_se_encode_delete);
EXPORT_SYMBOL(stm_se_encode_get_control);
EXPORT_SYMBOL(stm_se_encode_set_control);
EXPORT_SYMBOL(stm_se_encode_stream_new);
EXPORT_SYMBOL(stm_se_encode_stream_delete);
EXPORT_SYMBOL(stm_se_encode_stream_attach);
EXPORT_SYMBOL(stm_se_encode_stream_detach);
EXPORT_SYMBOL(stm_se_encode_stream_get_control);
EXPORT_SYMBOL(stm_se_encode_stream_set_control);
EXPORT_SYMBOL(stm_se_encode_stream_get_compound_control);
EXPORT_SYMBOL(stm_se_encode_stream_set_compound_control);
EXPORT_SYMBOL(stm_se_encode_stream_drain);
EXPORT_SYMBOL(stm_se_encode_stream_inject_frame);
EXPORT_SYMBOL(stm_se_encode_stream_inject_discontinuity);
EXPORT_SYMBOL(__stm_se_play_stream_get_statistics);
EXPORT_SYMBOL(__stm_se_play_stream_reset_statistics);
EXPORT_SYMBOL(__stm_se_play_stream_get_attributes);
EXPORT_SYMBOL(__stm_se_play_stream_reset_attributes);
EXPORT_SYMBOL(__stm_se_pm_low_power_enter);
EXPORT_SYMBOL(__stm_se_pm_low_power_exit);
