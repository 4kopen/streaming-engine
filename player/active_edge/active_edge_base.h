/************************************************************************
Copyright (C) 2003-2014 STMicroelectronics. All Rights Reserved.

This file is part of the Streaming Engine.

Streaming Engine is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License version 2 as
published by the Free Software Foundation.

Streaming Engine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with Streaming Engine; see the file COPYING.  If not, write to the Free
Software Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307,
USA.

The Streaming Engine Library may alternatively be licensed under a
proprietary license from ST.
************************************************************************/

#ifndef H_ACTIVE_EDGE
#define H_ACTIVE_EDGE

#include "active_edge_interface.h"
#include "player_threads.h"

#include "ring_generic.h"

#define ACTIVE_EDGE_MAX_MESSAGES                 8

#undef TRACE_TAG
#define TRACE_TAG "ActiveEdge_Base_c"

class ActiveEdge_Base_c: public ActiveEdgeInterface_c
{
public:
    ActiveEdge_Base_c(int ThreadDescId, void *Stream, int InputRingSize)
        : mInputRing(NULL)
        , mInputRingSize(InputRingSize)
        , mBaseStream(Stream)
        , mThreadHandle(0)
        , mThreadDescId(ThreadDescId)
        , mAccumulatedBeforeControlMessagesCount(0)
        , mAccumulatedAfterControlMessagesCount(0)
        , mAccumulatedBeforeControlMessages()
        , mAccumulatedAfterControlMessages()
        , mMaximumActualSequenceNumberSeen(0)
        , mDiscardingUntilDrainMarkerFrame(false)
    {
    }

    virtual ~ActiveEdge_Base_c()
    {
        if (mInputRing != NULL)
        {
            if (mInputRing->NonEmpty())
            {
                Stop();
            }
            delete mInputRing;
        }
    }

    virtual OS_Status_t Start()
    {
        mInputRing = RingGeneric_c::New(mInputRingSize, player_tasks_desc[mThreadDescId].name);
        if (mInputRing == NULL) { return PlayerInsufficientMemory; }

        return OS_CreateThread(&mThreadHandle, StartTrampoline, this, &player_tasks_desc[mThreadDescId]);
    }

    virtual void Stop()
    {
        if (mInputRing)
        {
            while (mInputRing->NonEmpty())
            {
                Buffer_t Buffer = NULL;
                mInputRing->Extract((uintptr_t *)(&Buffer));
                if (Buffer != NULL)
                {
                    unsigned int OwnerIdentifier;
                    Buffer->GetOwnerList(1, &OwnerIdentifier);
                    Buffer->DecrementReferenceCount(OwnerIdentifier);
                }
            }
        }
        WakeUp();
    }

    virtual void WakeUp()
    {
        if (mInputRing) { mInputRing->Insert((uintptr_t)NULL); }
    }

    virtual void DiscardUntilDrainMarkerFrame();
    virtual bool IsDiscardingUntilDrainMarkerFrame() const;

    Port_c *GetInputPort() { return mInputRing; }

protected:
    Ring_t              mInputRing;
    int                 mInputRingSize;

    void            HandlePlayerControlStructure(Buffer_t Buffer, unsigned long long SequenceNumber, unsigned long long MaximumActualSequenceNumberSeen);
    void            ProcessAccumulatedBeforeControlMessages(unsigned long long SequenceNumber);
    void            ProcessAccumulatedAfterControlMessages(unsigned long long SequenceNumber);
    void            StopDiscardingUntilDrainMarkerFrame();

private:
    void                       *mBaseStream;
    OS_Thread_t                 mThreadHandle;
    int                         mThreadDescId;
    unsigned int                mAccumulatedBeforeControlMessagesCount;
    unsigned int                mAccumulatedAfterControlMessagesCount;
    PlayerBufferRecord_t        mAccumulatedBeforeControlMessages[ACTIVE_EDGE_MAX_MESSAGES];
    PlayerBufferRecord_t        mAccumulatedAfterControlMessages[ACTIVE_EDGE_MAX_MESSAGES];
    unsigned long long          mMaximumActualSequenceNumberSeen;

    // Atomic flag accessed without locking.  See accessors.
    bool                        mDiscardingUntilDrainMarkerFrame;

    static void *StartTrampoline(void *param)
    {
        ActiveEdge_Base_c *activeEdge = reinterpret_cast<ActiveEdge_Base_c *>(param);
        activeEdge->MainLoop();
        OS_TerminateThread();
        return NULL;
    }

    PlayerStatus_t   ProcessAccumulatedControlMessages(
        unsigned int *MessageCount, unsigned int MessageTableSize,
        PlayerBufferRecord_t *MessageTable, unsigned long long SequenceNumber);

    PlayerStatus_t   ProcessControlMessage(Buffer_t Buffer, PlayerControlStructure_t *Message);

    PlayerStatus_t   AccumulateControlMessage(Buffer_t            Buffer,
                                              PlayerControlStructure_t     *Message,
                                              unsigned int             *MessageCount,
                                              unsigned int              MessageTableSize,
                                              PlayerBufferRecord_t         *MessageTable);



    virtual void MainLoop() = 0;
    virtual PlayerStatus_t  PerformInSequenceCall(PlayerControlStructure_t *ControlStructure) = 0;

    DISALLOW_COPY_AND_ASSIGN(ActiveEdge_Base_c);
};

#endif // H_THREAD
