/************************************************************************
Copyright (C) 2003-2014 STMicroelectronics. All Rights Reserved.

This file is part of the Streaming Engine.

Streaming Engine is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License version 2 as
published by the Free Software Foundation.

Streaming Engine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with Streaming Engine; see the file COPYING.  If not, write to the Free
Software Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307,
USA.

The Streaming Engine Library may alternatively be licensed under a
proprietary license from ST.
************************************************************************/

#include "player_generic.h"
#include "player_playback.h"
#include "player_stream.h"
#include "collate_to_parse_edge.h"
#include "parse_to_decode_edge.h"

#undef TRACE_TAG
#define TRACE_TAG "CollateToParseEdge_c"

CollateToParseEdge_c::CollateToParseEdge_c(int ThreadDescId, PlayerStream_t Stream, int InputRingSize,
                                           FrameParser_t FrameParser)
    : ActiveEdge_Base_c(ThreadDescId, (void *)Stream, InputRingSize)
    , mStream(Stream)
    , mCollateToParseLock()
    , mNextBufferSequenceNumber(0)
    , mLastSequenceNumberSeen(INVALID_SEQUENCE_VALUE)
    , mMaximumActualSequenceNumberSeen(0)
    , mFrameParser(FrameParser)
    , mLimiterPool(NULL)
{
    OS_InitializeMutex(&mCollateToParseLock);
}

CollateToParseEdge_c::~CollateToParseEdge_c()
{
    if (mLimiterPool != NULL) { mStream->GetPlayer()->GetBufferManager()->DestroyPool(mLimiterPool); }
    OS_TerminateMutex(&mCollateToParseLock);
}

// Insert buffer in this edge.
// This function can be called from several threads.
RingStatus_t CollateToParseEdge_c::Insert(uintptr_t  Value)
{
    Buffer_t Buffer = reinterpret_cast<Buffer_t>(Value);
    BufferType_t BufferType;

    Buffer->GetType(&BufferType);

    if (BufferType == mStream->GetCodedFrameBufferType())
    {
        PlayerSequenceNumber_t *SequenceNumberStructure;
        Buffer->ObtainMetaDataReference(mStream->GetPlayer()->MetaDataSequenceNumberType, (void **)(&SequenceNumberStructure));
        SE_ASSERT(SequenceNumberStructure != NULL);

        OS_LockMutex(&mCollateToParseLock);
        SequenceNumberStructure->Value                        = mNextBufferSequenceNumber++;
        OS_UnLockMutex(&mCollateToParseLock);

        SequenceNumberStructure->mMarkerFrame.mSequenceNumber = SequenceNumberStructure->Value;
        SequenceNumberStructure->TimeEntryInPipeline          = OS_GetTimeInMicroSeconds();
        SequenceNumberStructure->StreamUniqueIdentifier       = (int)mStream;
        SequenceNumberStructure->StreamTypeIdentifier         = mStream->GetStreamType();

        if (SequenceNumberStructure->mIsMarkerFrame == true)
        {
            SE_DEBUG(group_se_pipeline, "Stream 0x%p - %d - Inserting Marker Frame #%llu type %s E=%llu\n",
                     mStream,
                     mStream->GetStreamType(),
                     SequenceNumberStructure->Value,
                     MarkerFrameTypeString(SequenceNumberStructure->mMarkerFrame.mMarkerType),
                     SequenceNumberStructure->TimeEntryInPipeline
                    );
        }
        else
        {
            SE_DEBUG(group_se_pipeline, "Stream 0x%p - %d - #%lld PTS=%lld %s E=%llu\n",
                     mStream,
                     mStream->GetStreamType(),
                     SequenceNumberStructure->Value,
                     SequenceNumberStructure->PTS,
                     (SequenceNumberStructure->PtsValid ? "" : "(INVALID: no PTS)"),
                     SequenceNumberStructure->TimeEntryInPipeline
                    );
        }
    }

    return mInputRing->Insert(Value);
}

void CollateToParseEdge_c::HandleMarkerFrame(Buffer_t Buffer, PlayerSequenceNumber_t *SequenceNumberStructure)
{
    SE_DEBUG(group_player, "Stream 0x%p Got Marker Frame #%llu (type %s)\n", mStream, SequenceNumberStructure->Value,
             MarkerFrameTypeString(SequenceNumberStructure->mMarkerFrame.mMarkerType));

    mLastSequenceNumberSeen             = SequenceNumberStructure->Value;
    mMaximumActualSequenceNumberSeen    = max(mLastSequenceNumberSeen, mMaximumActualSequenceNumberSeen);

    if (SequenceNumberStructure->mMarkerFrame.mMarkerType == DrainDiscardMarker)
    {
        StopDiscardingUntilDrainMarkerFrame();
    }

    ProcessAccumulatedBeforeControlMessages(mLastSequenceNumberSeen);

    mFrameParser->Input(Buffer);

    Buffer->DecrementReferenceCount(IdentifierProcessCollateToParse);

    ProcessAccumulatedAfterControlMessages(mLastSequenceNumberSeen);
}

void CollateToParseEdge_c::HandleCodedFrameBuffer(Buffer_t Buffer, PlayerSequenceNumber_t *SequenceNumberStructure)
{
    mLastSequenceNumberSeen             = SequenceNumberStructure->Value;
    mMaximumActualSequenceNumberSeen    = max(mLastSequenceNumberSeen, mMaximumActualSequenceNumberSeen);

    ProcessAccumulatedBeforeControlMessages(mLastSequenceNumberSeen);

    unsigned int              BufferLength;
    Buffer->ObtainDataReference(NULL, &BufferLength, NULL);
    bool DiscardBuffer = (BufferLength != 0) && (mStream->IsUnPlayable() || IsDiscardingUntilDrainMarkerFrame());

    if (!DiscardBuffer)
    {
        SE_VERBOSE(group_se_pipeline, "Stream 0x%p - %d - #%lld PTS=%lld %s CtP=%llu\n",
                   mStream,
                   mStream->GetStreamType(),
                   SequenceNumberStructure->Value,
                   SequenceNumberStructure->PTS,
                   (SequenceNumberStructure->PtsValid ? "" : "(INVALID: no PTS)"),
                   SequenceNumberStructure->TimeEntryInProcess0
                  );

        mFrameParser->Input(Buffer);
    }
    else
    {
        SE_DEBUG(group_se_pipeline, "Stream 0x%p - %d - #%lld PTS=%lld %s Discard=%llu\n",
                 mStream,
                 mStream->GetStreamType(),
                 SequenceNumberStructure->Value,
                 SequenceNumberStructure->PTS,
                 (SequenceNumberStructure->PtsValid ? "" : "(INVALID: no PTS)"),
                 SequenceNumberStructure->TimeEntryInProcess0
                );
    }

    Buffer->DecrementReferenceCount(IdentifierProcessCollateToParse);

    ProcessAccumulatedAfterControlMessages(mLastSequenceNumberSeen);
}

void CollateToParseEdge_c::HandleCodedFrameBufferType(Buffer_t Buffer)
{
    PlayerSequenceNumber_t *SequenceNumberStructure;
    Buffer->ObtainMetaDataReference(mStream->GetPlayer()->MetaDataSequenceNumberType, (void **)(&SequenceNumberStructure));
    SE_ASSERT(SequenceNumberStructure != NULL);

    //  To limit Memory foot-print, number of buffers posted to stages after collator must
    //  be less than PLAYER_MAX_BUFFER_IN_DECODE_PIPELINE
    //  For this purpose a fake pool is used.
    if (mLimiterPool == NULL)
    {
        BufferStatus_t BufStatus = mStream->GetPlayer()->GetBufferManager()->CreatePool(&mLimiterPool, mStream->GetPlayer()->BufferFakeType, PLAYER_MAX_BUFFER_IN_DECODE_PIPELINE);
        if (BufStatus != BufferNoError)
        {
            SE_ERROR("Failed to create the injection limiter pool (status: %d)\n", BufStatus);
            return;
        }
    }
    Buffer_t FakeBuffer;
    if (mLimiterPool->GetBuffer(&FakeBuffer) != BufferNoError)
    {
        SE_ERROR("Failed to get a mLimiter buffer - Implementation error\n");
        return;
    }

    Buffer->AttachBuffer(FakeBuffer);
    FakeBuffer->DecrementReferenceCount();

    SequenceNumberStructure->TimeEntryInProcess0    = OS_GetTimeInMicroSeconds();

    if (SequenceNumberStructure->mIsMarkerFrame)
    {
        HandleMarkerFrame(Buffer, SequenceNumberStructure);
    }
    else
    {
        HandleCodedFrameBuffer(Buffer, SequenceNumberStructure);
    }
}

void   CollateToParseEdge_c::MainLoop()
{
    RingStatus_t              RingStatus;
    Buffer_t                  Buffer;
    BufferType_t              BufferType;

    //
    // Signal we have started
    //
    OS_LockMutex(&mStream->StartStopLock);
    mStream->ProcessRunningCount++;
    OS_SetEvent(&mStream->StartStopEvent);
    OS_UnLockMutex(&mStream->StartStopLock);

    SE_DEBUG(group_player, "process starting Stream 0x%p\n", mStream);

    //
    // Main Loop is woken up when getting a buffer
    // null buffers are used to signal specific events like terminating
    //
    while (!mStream->IsTerminating())
    {
        RingStatus  = mInputRing->Extract((uintptr_t *)(&Buffer), OS_INFINITE);

        if ((RingStatus == RingNothingToGet) || (Buffer == NULL))
        {
            continue;
        }

        Buffer->GetType(&BufferType);
        Buffer->TransferOwnership(IdentifierProcessCollateToParse);

        //
        // If we were set to terminate while we were 'Extracting' we should
        // remove the buffer reference and exit.
        //

        if (mStream->IsTerminating())
        {
            Buffer->DecrementReferenceCount(IdentifierProcessCollateToParse);
            continue;
        }

        if (BufferType == mStream->GetCodedFrameBufferType())
        {
            HandleCodedFrameBufferType(Buffer);
        }
        else if (BufferType == mStream->GetPlayer()->BufferPlayerControlStructureType)
        {
            HandlePlayerControlStructure(Buffer, mLastSequenceNumberSeen, mMaximumActualSequenceNumberSeen);
        }
        else
        {
            SE_ERROR("Unknown buffer type received - Implementation error\n");
            Buffer->DecrementReferenceCount();
        }
    }

    SE_DEBUG(group_player, "process terminating Stream 0x%p\n", mStream);


    //
    // Signal we have terminated
    //
    OS_LockMutex(&mStream->StartStopLock);
    mStream->ProcessRunningCount--;
    OS_SetEvent(&mStream->StartStopEvent);
    OS_UnLockMutex(&mStream->StartStopLock);
}


PlayerStatus_t   CollateToParseEdge_c::CallInSequence(
    PlayerSequenceType_t      SequenceType,
    PlayerSequenceValue_t     SequenceValue,
    PlayerComponentFunction_t Fn,
    ...)
{
    Buffer_t                  ControlStructureBuffer;

    BufferStatus_t Status = mStream->GetPlayer()->GetControlStructurePool()->GetBuffer(&ControlStructureBuffer, IdentifierInSequenceCall);
    if (Status != PlayerNoError)
    {
        SE_ERROR("Failed to get a control structure buffer\n");
        return Status;
    }

    PlayerControlStructure_t *ControlStructure;
    ControlStructureBuffer->ObtainDataReference(NULL, NULL, (void **)(&ControlStructure));
    SE_ASSERT(ControlStructure != NULL);  // not supposed to be empty

    ControlStructure->Action            = ActionInSequenceCall;
    ControlStructure->SequenceType      = SequenceType;
    ControlStructure->SequenceValue     = SequenceValue;
    ControlStructure->InSequence.Fn     = Fn;

    switch (Fn)
    {
    case PlayerFnSwitchFrameParser:
        break;

    default:
        SE_ERROR("Unsupported function call\n");
        ControlStructureBuffer->DecrementReferenceCount(IdentifierInSequenceCall);
        return PlayerNotSupported;
    }

    RingStatus_t ringStatus = mInputRing->Insert((uintptr_t)ControlStructureBuffer);
    if (ringStatus != RingNoError) { return PlayerError; }

    return PlayerNoError;
}

PlayerStatus_t   CollateToParseEdge_c::PerformInSequenceCall(PlayerControlStructure_t *ControlStructure)
{
    PlayerStatus_t  Status = PlayerNoError;

    switch (ControlStructure->InSequence.Fn)
    {
    case PlayerFnSwitchFrameParser:
        SwitchFrameParser();
        break;

    default:
        SE_ERROR("Unsupported function call - Implementation error\n");
        Status  = PlayerNotSupported;
        break;
    }

    return Status;
}



// //////////////////////////////////////////////////////////////////////////////////////////////////
//
//      Switch stream component function for the FrameParser
//

void    CollateToParseEdge_c::SwitchFrameParser()
{
    SE_DEBUG(group_player, "Stream 0x%p\n", mStream);

    //
    // Halt the current FrameParser; will be destroyed during stream switch
    //
    mFrameParser->Halt();

    //
    // Wait for the last decode to come out the other end, of the codec
    //
    OS_Status_t WaitStatus = OS_WaitForEventAuto(&mStream->SwitchStreamLastOneOutOfTheCodec, PLAYER_MAX_MARKER_TIME_THROUGH_CODEC);
    if (WaitStatus == OS_TIMED_OUT)
    {
        SE_ERROR("Stream 0x%p Last decode did not complete in reasonable time\n", mStream);
        // not fatal
    }

    unsigned int LastDecodeFrameIndex;
    mFrameParser->GetNextDecodeFrameIndex(&LastDecodeFrameIndex);

    //
    // Switch over to the new FrameParser
    //
    SE_ASSERT((mStream->SwitchingToFrameParser != NULL) && (mStream->SwitchingToFrameParser != mFrameParser));
    mFrameParser = mStream->SwitchingToFrameParser;

    //
    // Initialize the FrameParser
    //
    mFrameParser->RegisterPlayer(mStream->GetPlayer(), mStream->GetPlayback(), mStream);

    PlayerStatus_t Status  = mFrameParser->Connect(mStream->ParseToDecodeEdge->GetInputPort());
    if (Status != PlayerNoError)
    {
        SE_ERROR("Stream 0x%p Failed to register stream parameters with FrameParser - will mark stream unplayable\n", mStream);
        // fatal, will mark stream as unplayable at end of stream switch
        mStream->mSwitchingErrorOccurred = true;
    }

    mFrameParser->SetNextFrameIndices(LastDecodeFrameIndex);

    OS_SemaphoreSignal(&mStream->mSemaphoreStreamSwitchFrameParser);

    SE_DEBUG(group_player, "Stream 0x%p completed\n", mStream);
}
