/************************************************************************
Copyright (C) 2003-2014 STMicroelectronics. All Rights Reserved.

This file is part of the Streaming Engine.

Streaming Engine is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License version 2 as
published by the Free Software Foundation.

Streaming Engine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with Streaming Engine; see the file COPYING.  If not, write to the Free
Software Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307,
USA.

The Streaming Engine Library may alternatively be licensed under a
proprietary license from ST.
************************************************************************/

#include "havana_user_data.h"
#include "player_generic.h"
#include "player_playback.h"
#include "player_stream.h"
#include "decode_to_manifest_edge.h"
#include "post_manifest_edge.h"

#define MAXIMUM_MANIFESTATION_WINDOW_SLEEP      300000 // 300ms
#define ERRONEOUS_MANIFESTATION_WINDOW_SLEEP    MAX_STREAM_OFFSET_US
#define MINIMUM_SLEEP_TIME_US                   10000  // 10ms
#define PTS_DISCONTINUITY_DETECTION_US          500000  // 0.5 second for gap detection
#define MAX_ERRONEOUS_SLEEP_REQUESTS            1  // One errorneous sleep request is enough

#define MAX_REORDERING_RING_EXTRACTION_TIMEOUT  500 // Waiting on extract on the reordering ring should occur at least every 500ms

#undef TRACE_TAG
#define TRACE_TAG "DecodeToManifestEdge_c"

void DecodeToManifestEdge_c::CheckPtsDiscontinuity(const TimeStamp_c &Pts)
{
    // Apply Discontinuity action for video stream only and in normal speed
    if ((mStream->GetStreamType() != StreamTypeVideo) || (mStream->GetPlayback()->mSpeed != 1)
        || mStream->GetPlayer()->PolicyValue(mStream->GetPlayback(), mStream, PolicyTrickModeDomain) == PolicyValueTrickModeDecodeKeyFrames
        || mStream->GetPlayer()->PolicyValue(mStream->GetPlayback(), mStream, PolicyTrickModeDomain) == PolicyValueTrickModeDiscardNonReferenceFrames)
    {
        return;
    }

    if (!mPreviousPts.IsValid())
    {
        // Nothing to do more this time
        mPreviousPts = Pts;
        return;
    }

    if (!inrange(TimeStamp_c::DeltaUsec(Pts, mPreviousPts), -PTS_DISCONTINUITY_DETECTION_US, PTS_DISCONTINUITY_DETECTION_US)
        && (mAccumulatedDecodeBufferTableOccupancy != 0))
    {
        SE_WARNING("Stream 0x%p PTS too high compared to last PTS, new PTS=%llu last PTS=%llu Diff=%lld us\n",
                   mStream, Pts.NativeValue(), mPreviousPts.NativeValue(), TimeStamp_c::DeltaUsec(Pts, mPreviousPts));
        // Reset the reordering table
        FlushAccumulatedBuffers(&Pts);
    }

    mPreviousPts = Pts ;
}

void DecodeToManifestEdge_c::FlushAccumulatedBuffers(const TimeStamp_c *pCurrentPts)
{
    if (mAccumulatedDecodeBufferTableOccupancy == 0) { return; }

    for (unsigned int i = 0; i < mStream->GetNumberOfDecodeBuffers(); i++)
    {
        if (mAccumulatedDecodeBufferTable[i].Buffer == NULL)
        { continue; }

        if ((pCurrentPts != NULL) &&
            mAccumulatedDecodeBufferTable[i].ParsedFrameParameters->PTS.IsValid() &&
            (inrange(TimeStamp_c::DeltaUsec(*pCurrentPts, mAccumulatedDecodeBufferTable[i].ParsedFrameParameters->PTS),
                     -PTS_DISCONTINUITY_DETECTION_US, PTS_DISCONTINUITY_DETECTION_US)))
        { continue; }

        SE_DEBUG(group_player, "Releasing buffer with PTS %llu\n", mAccumulatedDecodeBufferTable[i].ParsedFrameParameters->PTS.NativeValue());
        ReleaseDecodeBuffer(mAccumulatedDecodeBufferTable[i].Buffer);

        mAccumulatedDecodeBufferTable[i].Buffer        = NULL;
        mAccumulatedDecodeBufferTable[i].ParsedFrameParameters = NULL;
        mAccumulatedDecodeBufferTableOccupancy--;
    }
}

unsigned int DecodeToManifestEdge_c::GetMaxDecodesOutOfOrder()
{
    if (mStream->GetStreamType() == StreamTypeAudio) { return 0; }

    unsigned int    PossibleDecodeBuffers;

    mStream->GetDecodeBufferManager()->GetEstimatedBufferCount(&PossibleDecodeBuffers);
    // Set depth of reordering queue to #of available buffers minus absolute minimum number of working coded buffers free in a playback
    unsigned int MaxDecodesOutOfOrder   = PossibleDecodeBuffers - PLAYER_MINIMUM_NUMBER_OF_WORKING_DECODE_BUFFERS;

    if (mStream->GetPlayback()->mDirection == PlayForward)
    {
        MaxDecodesOutOfOrder  = min(PLAYER_LIMIT_ON_OUT_OF_ORDER_DECODES, MaxDecodesOutOfOrder);
    }
    else
    {
        MaxDecodesOutOfOrder  = min(((3 * PossibleDecodeBuffers) / 4), MaxDecodesOutOfOrder);
    }

    /* for VP9 there are not out of order decode, so we keep MaxDecodesOutOfOrder to minimum to avoid deadlock */
    /* In fact problem is due to the GetEstimatedBufferCount , due to specific VP9 mbuffer management ,
    all buffer are taken at init time , so it returns max count instead of the real number of decoded buffer , may be 2 or 3 */
    if (mStream->GetEncodingType() == STM_SE_STREAM_ENCODING_VIDEO_VP9)
    {
        MaxDecodesOutOfOrder = 2;
    }

    return MaxDecodesOutOfOrder;
}

Buffer_t DecodeToManifestEdge_c::SearchNextDisplayOrderBufferInAccumulatedBuffers()
{
    mMinimumSequenceNumberAccumulated = 0xffffffffffffffffULL;

    if (mAccumulatedDecodeBufferTableOccupancy == 0) { return NULL; }

    unsigned int LowestIndex                = INVALID_INDEX;
    unsigned int LowestDisplayFrameIndex    = INVALID_INDEX;

    for (unsigned int i = 0; i < mStream->GetNumberOfDecodeBuffers(); i++)
    {
        if (mAccumulatedDecodeBufferTable[i].Buffer != NULL)
        {
            mMinimumSequenceNumberAccumulated = min(mMinimumSequenceNumberAccumulated, GetSequenceNumberStructure(mAccumulatedDecodeBufferTable[i].Buffer)->Value);

            if ((mAccumulatedDecodeBufferTable[i].ParsedFrameParameters->DisplayFrameIndex != INVALID_INDEX)
                && ((LowestIndex == INVALID_INDEX) || (mAccumulatedDecodeBufferTable[i].ParsedFrameParameters->DisplayFrameIndex < LowestDisplayFrameIndex)))
            {
                LowestDisplayFrameIndex     = mAccumulatedDecodeBufferTable[i].ParsedFrameParameters->DisplayFrameIndex;
                LowestIndex                 = i;
                SE_VERBOSE(group_player, "Stream 0x%p StreamType=%d LowestDisplayFrameIndex=%u LowestIndex=%u %s\n",
                           mStream, mStream->GetStreamType(), LowestDisplayFrameIndex, LowestIndex,
                           mAccumulatedDecodeBufferTable[i].ParsedFrameParameters->KeyFrame ? "I" : mAccumulatedDecodeBufferTable[i].ParsedFrameParameters->ReferenceFrame ? "P" : "B");
            }
        }
    }

    if (LowestIndex != INVALID_INDEX)
    {
        if (LowestDisplayFrameIndex == mDesiredFrameIndex)
        {
            SE_VERBOSE(group_player, "Stream 0x%p: StreamType=%d EXPECTED %d FrameIndex extraction\n",
                       mStream, mStream->GetStreamType(), mDesiredFrameIndex);
            return ExtractFromAccumulatedBuffers(LowestIndex);
        }
        else if (LowestDisplayFrameIndex < mDesiredFrameIndex)
        {
            SE_WARNING("Stream 0x%p: StreamType=%d Frame re-ordering failure (Got %d Expected %d)\n",
                       mStream,  mStream->GetStreamType(), LowestDisplayFrameIndex, mDesiredFrameIndex);
            return ExtractFromAccumulatedBuffers(LowestIndex);
        }
        else if (mAccumulatedDecodeBufferTable[LowestIndex].ParsedFrameParameters->CollapseHolesInDisplayIndices)
        {
            SE_WARNING("Stream 0x%p: StreamType=%d CollapseHolesInDisplayIndices (Got %d Expected %d) Extract FrameIndex %d\n",
                       mStream,  mStream->GetStreamType(), LowestDisplayFrameIndex, mDesiredFrameIndex, LowestIndex);
            mDesiredFrameIndex = LowestDisplayFrameIndex ;
            return ExtractFromAccumulatedBuffers(LowestIndex);
        }
        else if (mAccumulatedDecodeBufferTableOccupancy >= GetMaxDecodesOutOfOrder())
        {
            SE_WARNING("Stream 0x%p: StreamType=%d Hole in display frame indices (Got %d Expected %d)\n",
                       mStream,  mStream->GetStreamType(), LowestDisplayFrameIndex, mDesiredFrameIndex);
            return ExtractFromAccumulatedBuffers(LowestIndex);
        }
        else
        {
            SE_VERBOSE(group_player, "Stream 0x%p: StreamType=%d no frame is extracted\n", mStream, mStream->GetStreamType());
        }
    }
    else
    {
        SE_VERBOSE(group_player, "Stream 0x%p: StreamType=%d Invalid Index, no frame is extracted\n", mStream, mStream->GetStreamType());
    }

    return NULL;
}

void DecodeToManifestEdge_c::InsertInAccumulatedBuffers(Buffer_t Buffer)
{
    ParsedFrameParameters_t *ParsedFrameParameters   = GetParsedFrameParameters(Buffer);

    SE_DEBUG(group_se_pipeline, "Stream 0x%p - %d - #%llu PTS=%llu IAB=%llu DisplayFrameIndex=%u %s %s\n"
             , mStream
             , mStream->GetStreamType()
             , GetSequenceNumberStructure(Buffer)->Value
             , GetSequenceNumberStructure(Buffer)->PTS
             , OS_GetTimeInMicroSeconds()
             , ParsedFrameParameters->DisplayFrameIndex
             , ParsedFrameParameters->KeyFrame ? "K" : "-"
             , ParsedFrameParameters->IndependentFrame ? "I" : ParsedFrameParameters->ReferenceFrame ? "P" : "B");

    for (unsigned int i = 0; i < mStream->GetNumberOfDecodeBuffers(); i++)
    {
        if (mAccumulatedDecodeBufferTable[i].Buffer == NULL)
        {
            mAccumulatedDecodeBufferTable[i].Buffer                 = Buffer;
            mAccumulatedDecodeBufferTable[i].ParsedFrameParameters  = ParsedFrameParameters;
            mAccumulatedDecodeBufferTableOccupancy++;
            return;
        }
    }

    SE_ASSERT(0);
}

Buffer_t DecodeToManifestEdge_c::ExtractFromAccumulatedBuffers(int i)
{
    Buffer_t Buffer                                           = mAccumulatedDecodeBufferTable[i].Buffer;
    mAccumulatedDecodeBufferTable[i].Buffer                   = NULL;
    mAccumulatedDecodeBufferTable[i].ParsedFrameParameters    = NULL;
    mAccumulatedDecodeBufferTableOccupancy--;

    return Buffer;
}

void DecodeToManifestEdge_c::HandleMarkerFrame(Buffer_t MarkerFrameBuffer)
{
    PlayerSequenceNumber_t *SequenceNumberStructure = GetSequenceNumberStructure(MarkerFrameBuffer);
    mSequenceNumber                     = SequenceNumberStructure->Value;
    mMaximumActualSequenceNumberSeen    = max(mSequenceNumber, mMaximumActualSequenceNumberSeen);

    SE_DEBUG(group_player, "Stream 0x%p Got Marker Frame #%llu (type %s)\n", mStream, mSequenceNumber, MarkerFrameTypeString(SequenceNumberStructure->mMarkerFrame.mMarkerType));

    FlushAccumulatedBuffers(NULL);

    // Pass on the marker to Manifestors
    ManifestorStatus_t Status = mManifestationCoordinator->HandleMarkerFrame(MarkerFrameBuffer);
    if (Status != ManifestationCoordinatorNoError)
    {
        SE_ERROR("Stream 0x%p Failed to Handle MarkerFrame on manifestation coordinator (Status=%d)\n", mStream, Status);
    }

    ProcessAccumulatedBeforeControlMessages(mSequenceNumber);
    ProcessAccumulatedAfterControlMessages(mSequenceNumber);

    switch (SequenceNumberStructure->mMarkerFrame.mMarkerType)
    {
    case EosMarker:
        SE_DEBUG(group_player, "Stream 0x%p received EOS Marker #%lld\n", mStream, SequenceNumberStructure->mMarkerFrame.mSequenceNumber);
        break;

    case DrainDiscardMarker:
        SE_DEBUG(group_player, "Stream 0x%p received drain Marker #%lld\n",
                 mStream, SequenceNumberStructure->mMarkerFrame.mSequenceNumber);
        StopDiscardingUntilDrainMarkerFrame();
        break;

    default:
        break;
    }
}

Buffer_t DecodeToManifestEdge_c::GetNextBufferInDisplayOrder()
{
    Buffer_t Buffer;

    while (!mStream->IsTerminating())
    {
        if (IsDiscardingUntilDrainMarkerFrame())
        {
            FlushAccumulatedBuffers(NULL);
        }

        Buffer = SearchNextDisplayOrderBufferInAccumulatedBuffers();
        if (Buffer) { return Buffer; }

        // Skip any frame indices that were unused
        while (CheckForNonDecodedFrame(mDesiredFrameIndex))
        {
            mDesiredFrameIndex++;
        }

        if (mMarkerFrameBuffer != NULL)
        {
            HandleMarkerFrame(mMarkerFrameBuffer);
            mMarkerFrameBuffer = NULL;
            continue;
        }

        RingStatus_t RingStatus  = mInputRing->Extract((uintptr_t *) &Buffer, MAX_REORDERING_RING_EXTRACTION_TIMEOUT);
        if (RingStatus == RingNothingToGet || Buffer == NULL)
        {
            continue;
        }

        Buffer->TransferOwnership(IdentifierProcessDecodeToManifest);
        BufferType_t BufferType;
        Buffer->GetType(&BufferType);

        if (BufferType != mStream->DecodeBufferType)
        {
            return Buffer;
        }

        // If we were set to terminate while we were 'Extracting' we should
        // remove the buffer reference and exit.
        if (mStream->IsTerminating())
        {
            Buffer->DecrementReferenceCount(IdentifierProcessDecodeToManifest);
            continue;
        }

        PlayerSequenceNumber_t  *SequenceNumberStructure = GetSequenceNumberStructure(Buffer);

        SequenceNumberStructure->TimeEntryInProcess2        = OS_GetTimeInMicroSeconds();

        if (SequenceNumberStructure->mIsMarkerFrame)
        {
            mMarkerFrameBuffer = Buffer;
            continue;
        }

        InsertInAccumulatedBuffers(Buffer);
    }

    return NULL;
}

bool DecodeToManifestEdge_c::TestDiscardBuffer(Buffer_t Buffer, ParsedFrameParameters_t *ParsedFrameParameters)
{
    if (mStream->IsUnPlayable() || IsDiscardingUntilDrainMarkerFrame())
    {
        SE_DEBUG2(group_player, group_se_pipeline, "Stream 0x%p Frame Dropped stream UnPlayable %d DiscardingUntilMarkerFrame %d\n",
                  mStream, mStream->IsUnPlayable(), IsDiscardingUntilDrainMarkerFrame());
        return true;
    }

    if (ParsedFrameParameters->Discard_picture == true)
    {
        SE_DEBUG(group_player, "Stream 0x%p Discard a picture with desired index : %d\n", mStream, mDesiredFrameIndex);
        return true;
    }

    PlayerStatus_t Status = mOutputTimer->TestForFrameDrop(Buffer, OutputTimerBeforeOutputTiming);
    if (Status != OutputTimerNoError)
    {
        SE_DEBUG2(group_player, group_se_pipeline, "Stream 0x%p Frame Dropped before output timing: Status 0x%x (%s) DecodeFrameIndex %d\n",
                  mStream, Status, OutputTimer_c::StringifyOutputTimerStatus(Status), ParsedFrameParameters->DecodeFrameIndex);
        return true;
    }

    mOutputTimer->GenerateFrameTiming(Buffer);

    Status  = mOutputTimer->TestForFrameDrop(Buffer, OutputTimerBeforeManifestation);
    if (Status != OutputTimerNoError)
    {
        SE_DEBUG2(group_player, group_se_pipeline, "Stream 0x%p Frame Dropped before manifestation: Status 0x%x (%s) DecodeFrameIndex %d\n",
                  mStream, Status, OutputTimer_c::StringifyOutputTimerStatus(Status), ParsedFrameParameters->DecodeFrameIndex);
        return true;
    }

    // since we have waited, we need to reassess discarding conditions
    if (mStream->IsUnPlayable() || IsDiscardingUntilDrainMarkerFrame() || mStream->IsTerminating())
    {
        SE_DEBUG2(group_player, group_se_pipeline, "Stream 0x%p Frame Dropped : stream UnPlayable %d DiscardingUntilMarkerFrame %d Terminating %d\n",
                  mStream, mStream->IsUnPlayable(), IsDiscardingUntilDrainMarkerFrame(), mStream->IsTerminating());
        return true;
    }

    return false;
}

ManifestationCoordinatorStatus_t DecodeToManifestEdge_c::DelayForManifestationThrottling(Buffer_t Buffer)
{
    if (mStream->GetPlayer()->PolicyValue(mStream->GetPlayback(), mStream, PolicyAVDSynchronization) == PolicyValueDisapply)
    {
        return ManifestationCoordinatorNoError;
    }
    if (mFirstFrame == true && mStream->GetPlayer()->PolicyValue(mStream->GetPlayback(), mStream, PolicyManifestFirstFrameEarly) == PolicyValueApply)
    {
        SE_VERBOSE(group_avsync, "Stream 0x%p No throttling for first frame\n", mStream);
        mFirstFrame = false;
        return ManifestationCoordinatorNoError;
    }

    OutputTiming_t  *OutputTiming;
    Buffer->ObtainMetaDataReference(mStream->GetPlayer()->MetaDataOutputTimingType, (void **)&OutputTiming);
    SE_ASSERT(OutputTiming != NULL);

    if (ValidTime(OutputTiming->BaseSystemPlaybackTime))
    {
        //
        // How long shall we sleep
        //

        long long Now = OS_GetTimeInMicroSeconds();
        long long DeltaTimeToPresentation = OutputTiming->BaseSystemPlaybackTime - Now;
        long long ManifestationDelay = DeltaTimeToPresentation - PLAYER_LIMITED_EARLY_MANIFESTATION_WINDOW;

        if (DeltaTimeToPresentation > PLAYER_LIMITED_EARLY_MANIFESTATION_WINDOW)
        {
            if (ManifestationDelay < MINIMUM_SLEEP_TIME_US)
            {
                mErroneousSleepValues = 0;
                SE_EXTRAVERB(group_avsync, "Stream 0x%p Sleep period too short (%lluus)\n", mStream, ManifestationDelay);
                return ManifestationCoordinatorNoError;
            }
            else if (ManifestationDelay > MAXIMUM_MANIFESTATION_WINDOW_SLEEP)
            {
                if (ManifestationDelay > ERRONEOUS_MANIFESTATION_WINDOW_SLEEP)
                {
                    OutputTiming->BaseSystemPlaybackTime = INVALID_TIME;
                    SE_WARNING("Stream 0x%p - Invalid sleep request, invalidating playback time\n",
                               mStream);
                    mErroneousSleepValues ++;
                }
                if (mErroneousSleepValues > MAX_ERRONEOUS_SLEEP_REQUESTS)
                {
                    ManifestationDelay = 0;
                    return ManifestationCoordinatorNoError;
                }
                else
                {
                    SE_DEBUG(group_avsync, "Stream 0x%p Sleep period too long (%lluus), clamping to %d us\n", mStream, ManifestationDelay, MAXIMUM_MANIFESTATION_WINDOW_SLEEP);
                    ManifestationDelay               = MAXIMUM_MANIFESTATION_WINDOW_SLEEP;
                }
            }
            else
            {
                mErroneousSleepValues = 0;
            }

            if (ManifestationDelay > 1000000)
            {
                PlayerSequenceNumber_t  *SequenceNumberStructure = GetSequenceNumberStructure(Buffer);
                SE_VERBOSE(group_avsync, "Stream 0x%p - About to sleep for %lld ms (presentation_time=%llu, index=%lld)\n"
                           , mStream
                           , ManifestationDelay / 1000
                           , OutputTiming->BaseSystemPlaybackTime
                           , SequenceNumberStructure->Value);
            }
            if (ManifestationDelay != 0)
            {
                OS_Status_t WaitStatus = OS_WaitForEventAuto(&mThrottlingEvent, ManifestationDelay / 1000);
                if (WaitStatus != OS_TIMED_OUT)
                {
                    SE_VERBOSE(group_avsync, "Stream 0x%p wait for throttling woken up\n", mStream);
                    OS_ResetEvent(&mThrottlingEvent);

                    return ManifestationCoordinatorThrottlingInterrupted;
                }
            }
        }
        SE_VERBOSE(group_avsync, "Stream 0x%p - Slept for %lld ms vs expected %lld ms (presentation_time=%llu)\n",
                   mStream, (OS_GetTimeInMicroSeconds() - Now) / 1000, ManifestationDelay / 1000, OutputTiming->BaseSystemPlaybackTime);
    }
    return ManifestationCoordinatorNoError;
}

void DecodeToManifestEdge_c::ReleaseQueuedDecodeBuffers(bool ReleaseAllBuffers)
{
    OS_SetEvent(&mThrottlingEvent);
    OS_SemaphoreWaitAuto(&mQueueBufferSemaphore);
    mManifestationCoordinator->ReleaseQueuedDecodeBuffers(ReleaseAllBuffers);
    OS_SemaphoreSignal(&mQueueBufferSemaphore);
}

void DecodeToManifestEdge_c::WaitForFrozenSurface()
{
    OS_SemaphoreWaitAuto(&mQueueBufferSemaphore);
    mManifestationCoordinator->WaitForFrozenSurface();
    OS_SemaphoreSignal(&mQueueBufferSemaphore);
}

void DecodeToManifestEdge_c::WaitInPause(const ParsedFrameParameters_t *ParsedFrameParameters)
{
    if (mStream->GetPlayback()->mSpeed != 0) { return; }

    SE_VERBOSE(group_player, "mStep=%d mDiscardStep=%d\n", mStep, mDiscardStep);

    if (mDiscardStep && mStream->GetOutputTimer()->DropTestDiscardPts(ParsedFrameParameters) == OutputTimerNoError)
    {
        SE_EXTRAVERB(group_player, "Not Dropped\n");
        mDiscardStep = false;
    }

    while ((mStream->GetPlayback()->mSpeed == 0)
           && !mDiscardStep
           && !mStep
           && !mStream->IsTerminating()
           && !IsDiscardingUntilDrainMarkerFrame()
           && !mStream->IsUnPlayable())
    {
        // Release our hold on the manifestor critical section while waiting
        // to be resumed to avoid a deadlock between:
        // - This edge.
        // - A user-space process, e.g. grab_all, waiting for exclusive
        // access to the manifestor critical section for adding or removing
        // a manifestor while holding some STLinuxTV mutex(es).
        // - A user-space process, typically dvbtest, trying to resume this
        // edge but blocked on the aforementioned STLinuxTV mutex(es).
        //
        // Doing so is safe because we are not in a middle of some
        // computation relying on or modifying manifestation state.
        mStream->ExitManifestorSharedSection();

        SE_EXTRAVERB(group_player, "About to wait for step event (mStep=%d mDiscardStep=%d Terminating=%d Discarding=%d Unplayable=%d)\n",
                     mStep, mDiscardStep, mStream->IsTerminating(), IsDiscardingUntilDrainMarkerFrame(), mStream->IsUnPlayable());

        OS_WaitForEventAuto(&mStepEvent, PLAYER_NEXT_FRAME_EVENT_WAIT);

        SE_EXTRAVERB(group_player, "Woken up (mStep=%d mDiscardStep=%d)\n", mStep, mDiscardStep);

        mStream->EnterManifestorSharedSection();

        OS_ResetEvent(&mStepEvent);

        if (mDiscardStep && mStream->GetOutputTimer()->DropTestDiscardPts(ParsedFrameParameters) == OutputTimerNoError)
        {
            SE_EXTRAVERB(group_player, "Not Dropped\n");
            mDiscardStep = false;
        }
    }

    if (mStep)
    {
        SE_VERBOSE(group_player, "Single Stepping and Time = %lld\n", ParsedFrameParameters->PTS.NativeValue());
        mStream->GetPlayback()->SetDiscardPts(ParsedFrameParameters->PTS);
    }
}

void DecodeToManifestEdge_c::HandleCodedFrameBuffer(Buffer_t Buffer)
{
    ManifestationCoordinatorStatus_t ProcessStatus;
    ParsedFrameParameters_t *ParsedFrameParameters   = GetParsedFrameParameters(Buffer);
    PlayerSequenceNumber_t  *SequenceNumberStructure = GetSequenceNumberStructure(Buffer);

    SE_DEBUG(group_se_pipeline, "Stream 0x%p - %d - #%llu PTS=%llu DisplayFrameIndex=%u %s %s\n"
             , mStream
             , mStream->GetStreamType()
             , GetSequenceNumberStructure(Buffer)->Value
             , GetSequenceNumberStructure(Buffer)->PTS
             , ParsedFrameParameters->DisplayFrameIndex
             , ParsedFrameParameters->KeyFrame ? "K" : "-"
             , ParsedFrameParameters->IndependentFrame ? "I" : ParsedFrameParameters->ReferenceFrame ? "P" : "B");


    // First calculate the sequence number that applies to this frame
    // this calculation may appear weird, the idea is this, assume you
    // have a video stream IPBB, sequence numbers 0 1 2 3, frame reordering
    // will yield sequence numbers 0 2 3 1 IE any command to be executed at
    // the end of the stream will appear 1 frame early, the calculations
    // below will re-wossname the sequence numbers to 0 1 1 3 causing the
    // signal to occur at the correct point.
    //
    mSequenceNumber                      = SequenceNumberStructure->Value;
    mMaximumActualSequenceNumberSeen     = max(mSequenceNumber, mMaximumActualSequenceNumberSeen);
    mSequenceNumber                      = min(mMaximumActualSequenceNumberSeen, mMinimumSequenceNumberAccumulated);

    // Check if Time discontinuity detected
    CheckPtsDiscontinuity(ParsedFrameParameters->PTS);

    // Process any outstanding control messages to be applied before this buffer
    ProcessAccumulatedBeforeControlMessages(mSequenceNumber);

    while (1)
    {
        WaitInPause(ParsedFrameParameters);

        OS_SemaphoreWaitAuto(&mQueueBufferSemaphore);

        ProcessStatus = ProcessBuffer(Buffer, ParsedFrameParameters, SequenceNumberStructure);

        OS_SemaphoreSignal(&mQueueBufferSemaphore);

        if (ProcessStatus == ManifestationCoordinatorRetimeBuffer)
        {
            continue;
        }

        break;
    }

    // Process any outstanding control messages to be applied after this buffer
    ProcessAccumulatedAfterControlMessages(mSequenceNumber);
}

void DecodeToManifestEdge_c::ReportFrameDecodedEvent()
{
    PlayerEventRecord_t DecodeEvent;

    DecodeEvent.Code           = EventNewFrameDecoded;
    DecodeEvent.Playback       = mStream->GetPlayback();
    DecodeEvent.Stream         = mStream;

    PlayerStatus_t StatusEvt = mStream->SignalEvent(&DecodeEvent);
    if (StatusEvt != PlayerNoError)
    {
        SE_ERROR("Failed to signal decode event\n");
    }
}

ManifestationCoordinatorStatus_t DecodeToManifestEdge_c::ProcessBuffer(Buffer_t Buffer, ParsedFrameParameters_t *ParsedFrameParameters, PlayerSequenceNumber_t  *SequenceNumberStructure)
{
    PlayerStatus_t Status;
    bool DiscardBuffer = TestDiscardBuffer(Buffer, ParsedFrameParameters);

    if (mStream->GetStreamType() == StreamTypeVideo)
    {
        CheckForVideoDisplayParametersChange(Buffer);
    }

    // calculate next desired frame index
    mDesiredFrameIndex   = ParsedFrameParameters->DisplayFrameIndex + 1;

    if (!DiscardBuffer)
    {
        OutputTiming_t *OutputTiming;
        Buffer->ObtainMetaDataReference(mStream->GetPlayer()->MetaDataOutputTimingType, (void **)&OutputTiming);
        SE_ASSERT(OutputTiming != NULL);

        // Warning : the SequenceNumberStructure may not be relevant in case of streambase decoders
        // because any Collated buffer may be associated to many decoded buffer and at the time of
        // the association we don't know when which Collated buffer will be used to produce a given
        // decoded buffer.
        SE_VERBOSE(group_se_pipeline, "Stream 0x%p - %d - #%lld PTS=%lld CollatedPTS=%lld DtM=%llu %s %s\n",
                   mStream,
                   mStream->GetStreamType(),
                   SequenceNumberStructure->Value,
                   ParsedFrameParameters->PTS.NativeValue(),
                   SequenceNumberStructure->PTS,
                   SequenceNumberStructure->TimeEntryInProcess2,
                   ParsedFrameParameters->KeyFrame ? "K" : "-",
                   ParsedFrameParameters->IndependentFrame ? "I" : ParsedFrameParameters->ReferenceFrame ? "P" : "B"
                  );

        SequenceNumberStructure->TimePassToManifestor   = OS_GetTimeInMicroSeconds();

        // Pass the buffer to the UserData sender to provide it to memory Sink
        // The Buffer is not queued, only user data are extracted
        mStream->UserDataSender->GetUserDataFromDecodeBuffer(Buffer);

        Status = DelayForManifestationThrottling(Buffer);

        if (Status == ManifestationCoordinatorThrottlingInterrupted)
        {
            return ManifestationCoordinatorRetimeBuffer;
        }

        mOutputTimer->FillOutManifestationTimings(Buffer);

        ReportFrameDecodedEvent();

        // Send the buffer to manifestation coordinator
        mStream->FramesToManifestorCount++;
        mStream->Statistics().FrameCountToManifestor++;

        SE_DEBUG(group_se_pipeline, "Stream 0x%p - %d - #%lld PTS=%lld QDB=%llu PresentationTime %llu  deltaNow %lld\n",
                 mStream, mStream->GetStreamType(), SequenceNumberStructure->Value, ParsedFrameParameters->PTS.NativeValue(),
                 OS_GetTimeInMicroSeconds(), OutputTiming->BaseSystemPlaybackTime, OutputTiming->BaseSystemPlaybackTime - OS_GetTimeInMicroSeconds());

        // The HavanaCapture::BufferCaptureThread() can be scheduled just after the decode buffer
        // is passed on to the manifestor ring in QueueDecodeBuffer() but before the function returns.
        // Now if stm_se_play_stream_step() is called in context of ::BufferCaptureThread(), then it sets
        // 'mStep' in 'DecodeToManifestEdge'. Now when 'DecodeToManifestEdge' is rescheduled and 'QueueDecodeBuffer()'
        // returns, clearing 'mStep' will result in the loss of the _step() call.
        // So clearing 'mStep' before calling QueueDecodeBuffer() to avoid this race condition.
        mStep = false;

        Status  = mManifestationCoordinator->QueueDecodeBuffer(Buffer);
        if (Status != ManifestationCoordinatorNoError)
        {
            SE_ERROR("Stream 0x%p Failed to queue buffer to manifestation coordinator! (Status=%d)\n", mStream, Status);
            ReleaseDecodeBuffer(Buffer);
        }
    }
    else
    {
        SE_DEBUG(group_se_pipeline, "Stream 0x%p - %d - #%lld PTS=%lld Discard=%llu %s %s\n",
                 mStream,
                 mStream->GetStreamType(),
                 SequenceNumberStructure->Value,
                 SequenceNumberStructure->PTS,
                 SequenceNumberStructure->TimeEntryInProcess2,
                 ParsedFrameParameters->KeyFrame ? "K" : "-",
                 ParsedFrameParameters->IndependentFrame ? "I" : ParsedFrameParameters->ReferenceFrame ? "P" : "B"
                );

        ReleaseDecodeBuffer(Buffer);
    }

    return ManifestationCoordinatorNoError;
}

// //////////////////////////////////////////////////////////////////////////////////////////////////
//
//      Main process code
//

void   DecodeToManifestEdge_c::MainLoop()
{
    Buffer_t                          Buffer = NULL;
    BufferType_t                      BufferType;

    //
    // Signal we have started
    //
    OS_LockMutex(&mStream->StartStopLock);
    mStream->ProcessRunningCount++;
    OS_SetEvent(&mStream->StartStopEvent);
    OS_UnLockMutex(&mStream->StartStopLock);

    SE_DEBUG(group_player, "process starting Stream 0x%p\n", mStream);

    //
    // Main Loop
    //
    mStream->EnterManifestorSharedSection();
    while (!mStream->IsTerminating())
    {
        mStream->ExitManifestorSharedSection();
        Buffer = GetNextBufferInDisplayOrder();
        mStream->EnterManifestorSharedSection();

        if (mStream->IsTerminating())
        {
            continue;
        }

        Buffer->GetType(&BufferType);

        if (BufferType == mStream->DecodeBufferType)
        {
            HandleCodedFrameBuffer(Buffer);
        }
        else if (BufferType == mStream->GetPlayer()->BufferPlayerControlStructureType)
        {
            HandlePlayerControlStructure(Buffer, mSequenceNumber, mMaximumActualSequenceNumberSeen);
        }
        else
        {
            SE_ERROR("Stream 0x%p Unknown buffer type received - Implementation error\n", mStream);
            Buffer->DecrementReferenceCount();
        }
    }
    mStream->ExitManifestorSharedSection();

    if (mAccumulatedDecodeBufferTableOccupancy != 0)
    {
        for (unsigned int i = 0; i < mStream->GetNumberOfDecodeBuffers(); i++)
        {
            if (mAccumulatedDecodeBufferTable[i].Buffer != NULL)
            {
                ReleaseDecodeBuffer(mAccumulatedDecodeBufferTable[i].Buffer);
            }
        }
    }

    FlushNonDecodedFrameList();
    SE_DEBUG(group_player, "process terminating Stream 0x%p\n", mStream);

    //
    // Signal we have terminated
    //
    OS_LockMutex(&mStream->StartStopLock);
    mStream->ProcessRunningCount--;
    OS_SetEvent(&mStream->StartStopEvent);
    OS_UnLockMutex(&mStream->StartStopLock);
}


// //////////////////////////////////////////////////////////////////////////////////////////////////
//
//      Function to check if a frame index is non-decoded
//

bool   DecodeToManifestEdge_c::CheckForNonDecodedFrame(unsigned int DisplayFrameIndex)
{
    unsigned int    i;
    bool            Return;
    //
    // Check for this index in the table (if the table is non-empty)
    //
    OS_LockMutex(&mStream->NonDecodedBuffersLock);

    if (mStream->InsertionsIntoNonDecodedBuffers == mStream->RemovalsFromNonDecodedBuffers)
    {
        OS_UnLockMutex(&mStream->NonDecodedBuffersLock);
        return false;
    }

    for (i = 0; i < PLAYER_MAX_DISCARDED_FRAMES; i++)
    {
        if (mStream->NonDecodedBuffers[i].Buffer != NULL)
        {
            //
            // Can we release the actual buffer
            //
            if (!mStream->NonDecodedBuffers[i].ReleasedBuffer)
            {
                if (mStream->NonDecodedBuffers[i].ParsedFrameParameters->DisplayFrameIndex != INVALID_INDEX)
                {
                    if (IsDiscardingUntilDrainMarkerFrame() ||
                        mStream->NonDecodedBuffers[i].ParsedFrameParameters->CollapseHolesInDisplayIndices)
                    {
                        mStream->DisplayIndicesCollapse = max(mStream->DisplayIndicesCollapse, mStream->NonDecodedBuffers[i].ParsedFrameParameters->DisplayFrameIndex);
                    }

                    // NOTE due to union, ParsedFrameParameters is invalid after this line, which is OK as we free the buffer in the next line anyway
                    mStream->NonDecodedBuffers[i].DisplayFrameIndex      = mStream->NonDecodedBuffers[i].ParsedFrameParameters->DisplayFrameIndex;
                    mStream->NonDecodedBuffers[i].Buffer->DecrementReferenceCount(IdentifierNonDecodedFrameList);
                    mStream->NonDecodedBuffers[i].ReleasedBuffer         = true;
                }
                else
                {
                    continue;
                }
            }

            //
            // Now check the index
            //

            if (mStream->NonDecodedBuffers[i].DisplayFrameIndex <= DisplayFrameIndex)
            {
                Return                                  = (mStream->NonDecodedBuffers[i].DisplayFrameIndex == DisplayFrameIndex);
                mStream->NonDecodedBuffers[i].Buffer     = NULL;
                mStream->RemovalsFromNonDecodedBuffers++;

                if (Return)
                {
                    OS_UnLockMutex(&mStream->NonDecodedBuffersLock);
                    return true;
                }
            }
        }
    }
//
    OS_UnLockMutex(&mStream->NonDecodedBuffersLock);
//
    return (DisplayFrameIndex < mStream->DisplayIndicesCollapse);
}


// //////////////////////////////////////////////////////////////////////////////////////////////////
//
//      Function to flush any recorded non decoded frames
//

void   DecodeToManifestEdge_c::FlushNonDecodedFrameList()
{
    unsigned int i;
//
    OS_LockMutex(&mStream->NonDecodedBuffersLock);

    if (mStream->InsertionsIntoNonDecodedBuffers == mStream->RemovalsFromNonDecodedBuffers)
    {
        OS_UnLockMutex(&mStream->NonDecodedBuffersLock);
        return;
    }

    for (i = 0; i < PLAYER_MAX_DISCARDED_FRAMES; i++)
        if ((mStream->NonDecodedBuffers[i].Buffer != NULL) && !mStream->NonDecodedBuffers[i].ReleasedBuffer)
        {
            mStream->NonDecodedBuffers[i].Buffer->DecrementReferenceCount(IdentifierNonDecodedFrameList);
            mStream->NonDecodedBuffers[i].Buffer                 = NULL;
            mStream->RemovalsFromNonDecodedBuffers++;
        }

    OS_UnLockMutex(&mStream->NonDecodedBuffersLock);
}

//{{{  CheckForVideoDisplayParametersChange
//{{{  doxynote
/// \brief              Allows notification of _FRAME_RATE_CHANGED and PARAMETER_CHANGED events
/// \brief              of a decoded frame buffer before manifestation
//}}}
void DecodeToManifestEdge_c::CheckForVideoDisplayParametersChange(Buffer_t         Buffer)
{
    ParsedVideoParameters_t           *ParsedVideoParameters;
    VideoDisplayParameters_t          *VideoParameters;
    PlayerEventRecord_t                DisplayEvent;
    //
    // Retrieve video parameter from the buffer
    //
    Buffer->ObtainMetaDataReference(mStream->GetPlayer()->MetaDataParsedVideoParametersType, (void **)&ParsedVideoParameters);
    SE_ASSERT(ParsedVideoParameters != NULL);

    VideoParameters = &ParsedVideoParameters->Content;

    // retrieve original coded frame buffer associated and fix frame rate if was updated after buffer push to P2D edge
    Buffer_t OriginalCodedFrameBuffer;
    Buffer->ObtainAttachedBufferReference(mStream->GetCodedFrameBufferType(), &OriginalCodedFrameBuffer);
    SE_ASSERT(OriginalCodedFrameBuffer != NULL);
    ParsedVideoParameters_t  *OcFParsedVideoParameters;
    OriginalCodedFrameBuffer->ObtainMetaDataReference(mStream->GetPlayer()->MetaDataParsedVideoParametersType, (void **)(&OcFParsedVideoParameters));
    SE_ASSERT(OcFParsedVideoParameters != NULL);
    if (VideoParameters->FrameRate != OcFParsedVideoParameters->Content.FrameRate)
    {
        SE_DEBUG2(group_player, group_se_pipeline, "Stream 0x%p fixing FrameRate %d.%02d <- %d.%02d",
                  mStream,
                  VideoParameters->FrameRate.IntegerPart(),
                  VideoParameters->FrameRate.RemainderDecimal(),
                  OcFParsedVideoParameters->Content.FrameRate.IntegerPart(),
                  OcFParsedVideoParameters->Content.FrameRate.RemainderDecimal());
        VideoParameters->FrameRate = OcFParsedVideoParameters->Content.FrameRate;
    }

    //
    // check if something has changed since previous decoded buffer
    //
    if ((memcmp(&mVideoDisplayParameters, VideoParameters, sizeof(mVideoDisplayParameters)) == 0))
    {
        return;
    }

    //
    // Check framerate change
    //
    if (VideoParameters->FrameRate != mVideoDisplayParameters.FrameRate)
    {
        DisplayEvent.Code         = EventSourceFrameRateChangeManifest;
        DisplayEvent.Playback     = mStream->GetPlayback();
        DisplayEvent.Stream       = mStream;
        DisplayEvent.Rational     = VideoParameters->FrameRate;
        //
        // Notify this event
        //
        mStream->SignalEvent(&DisplayEvent);
    }

    //
    // Check if monitored parameters have changed
    //
    if ((VideoParameters->Width             != mVideoDisplayParameters.Width)
        || (VideoParameters->Height            != mVideoDisplayParameters.Height)
        || (VideoParameters->DisplayWidth      != mVideoDisplayParameters.DisplayWidth)
        || (VideoParameters->DisplayHeight     != mVideoDisplayParameters.DisplayHeight)
        || (VideoParameters->PixelAspectRatio  != mVideoDisplayParameters.PixelAspectRatio)
        || (VideoParameters->Progressive       != mVideoDisplayParameters.Progressive)
        || (VideoParameters->Pulldown          != mVideoDisplayParameters.Pulldown)
        || (VideoParameters->Output3DVideoProperty.Stream3DFormat != mVideoDisplayParameters.Output3DVideoProperty.Stream3DFormat)
        || (VideoParameters->ColourMatrixCoefficients             != mVideoDisplayParameters.ColourMatrixCoefficients)
        || (VideoParameters->FrameRate                            != mVideoDisplayParameters.FrameRate)
        || (VideoParameters->BitsPerComponent                     != mVideoDisplayParameters.BitsPerComponent)
        || (VideoParameters->FramePackingFlags                    != mVideoDisplayParameters.FramePackingFlags)
        || (VideoParameters->FramePackingArrangementType          != mVideoDisplayParameters.FramePackingArrangementType)
        || (VideoParameters->FrameRateNum                         != mVideoDisplayParameters.FrameRateNum)
        || (VideoParameters->FrameRateDen                         != mVideoDisplayParameters.FrameRateDen)
        || (VideoParameters->FrameRateExp                         != mVideoDisplayParameters.FrameRateExp)
        || (VideoParameters->Profile                              != mVideoDisplayParameters.Profile)
        || (VideoParameters->ProfileLevel                         != mVideoDisplayParameters.ProfileLevel)
        || (memcmp(&VideoParameters->VideoHdrInfo, &mVideoDisplayParameters.VideoHdrInfo, sizeof(stm_hdr_format_t))))
    {
        DisplayEvent.Code                   = EventSourceVideoParametersChangeManifest;
        DisplayEvent.Playback               = mStream->GetPlayback();
        DisplayEvent.Stream                 = mStream;
        DisplayEvent.Value[WidthInfo].UnsignedInt   = VideoParameters->Width;
        DisplayEvent.Value[HeightInfo].UnsignedInt   = VideoParameters->Height;
        DisplayEvent.Value[ProgressiveInfo].UnsignedInt   = VideoParameters->Progressive;
        DisplayEvent.Rational               = VideoParameters->PixelAspectRatio;
        DisplayEvent.Value[Stream3DFormatInfo].UnsignedInt   = VideoParameters->Output3DVideoProperty.Stream3DFormat;
        DisplayEvent.Value[Frame0IsLeftInfo].Bool          = VideoParameters->Output3DVideoProperty.Frame0IsLeft;
        // Retrieve colorspace information from MatrixCoefficients

        switch (VideoParameters->ColourMatrixCoefficients)
        {
        case MatrixCoefficients_ITU_R_BT601:      DisplayEvent.Value[ColorSpaceInfo].UnsignedInt = (unsigned int)STM_SE_COLORSPACE_SMPTE170M;       break;

        case MatrixCoefficients_ITU_R_BT709:      DisplayEvent.Value[ColorSpaceInfo].UnsignedInt = (unsigned int)STM_SE_COLORSPACE_BT709;           break;

        case MatrixCoefficients_ITU_R_BT470_2_M:  DisplayEvent.Value[ColorSpaceInfo].UnsignedInt = (unsigned int)STM_SE_COLORSPACE_BT470_SYSTEM_M;  break;

        case MatrixCoefficients_ITU_R_BT470_2_BG: DisplayEvent.Value[ColorSpaceInfo].UnsignedInt = (unsigned int)STM_SE_COLORSPACE_BT470_SYSTEM_BG; break;

        case MatrixCoefficients_SMPTE_170M:       DisplayEvent.Value[ColorSpaceInfo].UnsignedInt = (unsigned int)STM_SE_COLORSPACE_SMPTE170M;       break;

        case MatrixCoefficients_SMPTE_240M:       DisplayEvent.Value[ColorSpaceInfo].UnsignedInt = (unsigned int)STM_SE_COLORSPACE_SMPTE240M;       break;

        case MatrixCoefficients_ITU_R_BT2020:     DisplayEvent.Value[ColorSpaceInfo].UnsignedInt = (unsigned int)STM_SE_COLORSPACE_BT2020;       break;

        default:                                  DisplayEvent.Value[ColorSpaceInfo].UnsignedInt = (unsigned int)STM_SE_COLORSPACE_UNSPECIFIED;
        }

        DisplayEvent.Value[FrameRateInfo].UnsignedInt   = RoundedIntegerPart(VideoParameters->FrameRate * STM_SE_PLAY_FRAME_RATE_MULTIPLIER);
        DisplayEvent.Value[DisplayWidthInfo].UnsignedInt   = VideoParameters->DisplayWidth;
        DisplayEvent.Value[DisplayHeightInfo].UnsignedInt   = VideoParameters->DisplayHeight;
        DisplayEvent.Value[ProfileInfo].UnsignedInt   = VideoParameters->Profile;
        DisplayEvent.Value[ProfileLevelInfo].UnsignedInt   = VideoParameters->ProfileLevel;
        DisplayEvent.Value[BitsPerComponentInfo].UnsignedInt   = VideoParameters->BitsPerComponent;
        DisplayEvent.Value[FramePackingFlagsInfo].UnsignedInt   = VideoParameters->FramePackingFlags;
        DisplayEvent.Value[FramePackingArrangementTypeInfo].UnsignedInt   = VideoParameters->FramePackingArrangementType;
        DisplayEvent.Value[FrameRateNumInfo].UnsignedInt   = VideoParameters->FrameRateNum;
        DisplayEvent.Value[FrameRateDenInfo].UnsignedInt   = VideoParameters->FrameRateDen;
        DisplayEvent.Value[FrameRateExpInfo].UnsignedInt   = VideoParameters->FrameRateExp;
        SE_INFO2(group_player, group_se_pipeline, "Stream 0x%p Incoming Source coded %dx%d Display area %dx%d\n",
                 mStream, VideoParameters->DecodeWidth, VideoParameters->DecodeHeight,
                 VideoParameters->DisplayWidth, VideoParameters->DisplayHeight);
        SE_INFO2(group_player, group_se_pipeline, "Stream 0x%p %s Content, FrameRate %d.%02d, PixelAspectRatio %d.%02d\n",
                 mStream, VideoParameters->Progressive ? "Progressive" : (VideoParameters->Pulldown ? "Pulldown" : "Interlaced"),
                 VideoParameters->FrameRate.IntegerPart(), VideoParameters->FrameRate.RemainderDecimal(),
                 VideoParameters->PixelAspectRatio.IntegerPart(), VideoParameters->PixelAspectRatio.RemainderDecimal());
        SE_INFO2(group_player, group_se_pipeline,
                 "Stream 0x%p Profile 0x%x Level 0x%x BitsPerComponent %d FramePackingFlags 0x%x FramePackingArrangementType 0x%x FrameRateNR  %d FrameRateDR %d FrameRateEXP %d\n",
                 mStream, VideoParameters->Profile, VideoParameters->ProfileLevel, VideoParameters->BitsPerComponent,
                 VideoParameters->FramePackingFlags, VideoParameters->FramePackingArrangementType, VideoParameters->FrameRateNum,
                 VideoParameters->FrameRateDen, VideoParameters->FrameRateExp);

        // Set the HDR Info
        DisplayEvent.Value[HdrEOTFTypeInfo].UnsignedInt                 = VideoParameters->VideoHdrInfo.eotf_type;
        DisplayEvent.Value[HdrST2086PresentFlagInfo].UnsignedInt        = VideoParameters->VideoHdrInfo.is_st2086_metadata_present;
        DisplayEvent.Value[HdrLightLevelPresentFlagInfo].UnsignedInt    = VideoParameters->VideoHdrInfo.is_hdr_metadata_present;
        if (VideoParameters->VideoHdrInfo.is_st2086_metadata_present)
        {
            DisplayEvent.Value[HdrDisplayPrimX0Info].UnsignedInt        = VideoParameters->VideoHdrInfo.st2086_metadata.display_primaries_x_0;
            DisplayEvent.Value[HdrDisplayPrimY0Info].UnsignedInt        = VideoParameters->VideoHdrInfo.st2086_metadata.display_primaries_y_0;
            DisplayEvent.Value[HdrDisplayPrimX1Info].UnsignedInt        = VideoParameters->VideoHdrInfo.st2086_metadata.display_primaries_x_1;
            DisplayEvent.Value[HdrDisplayPrimY1Info].UnsignedInt        = VideoParameters->VideoHdrInfo.st2086_metadata.display_primaries_y_1;
            DisplayEvent.Value[HdrDisplayPrimX2Info].UnsignedInt        = VideoParameters->VideoHdrInfo.st2086_metadata.display_primaries_x_2;
            DisplayEvent.Value[HdrDisplayPrimY2Info].UnsignedInt        = VideoParameters->VideoHdrInfo.st2086_metadata.display_primaries_y_2;
            DisplayEvent.Value[HdrWhitePointXInfo].UnsignedInt          = VideoParameters->VideoHdrInfo.st2086_metadata.white_point_x;
            DisplayEvent.Value[HdrWhitePointYInfo].UnsignedInt          = VideoParameters->VideoHdrInfo.st2086_metadata.white_point_y;
            DisplayEvent.Value[HdrMaxDisplayLumInfo].UnsignedInt        = VideoParameters->VideoHdrInfo.st2086_metadata.max_display_mastering_luminance;
            DisplayEvent.Value[HdrMinDisplayLumInfo].UnsignedInt        = VideoParameters->VideoHdrInfo.st2086_metadata.min_display_mastering_luminance;
        }
        if (VideoParameters->VideoHdrInfo.is_hdr_metadata_present)
        {
            DisplayEvent.Value[HdrMaxCLLInfo].UnsignedInt               = VideoParameters->VideoHdrInfo.hdr_metadata.maxCLL;
            DisplayEvent.Value[HdrMaxFALLInfo].UnsignedInt              = VideoParameters->VideoHdrInfo.hdr_metadata.maxFALL;
        }
        DisplayEvent.Value[HdrColourPrimaries].UnsignedInt              = VideoParameters->VideoHdrInfo.colour_primaries;
        DisplayEvent.Value[HdrXferCharacteristics].UnsignedInt          = VideoParameters->VideoHdrInfo.transfer_characteristics;
        DisplayEvent.Value[HdrMatrixCoeffs].UnsignedInt                 = VideoParameters->VideoHdrInfo.matrix_coefficients;
        DisplayEvent.Value[HdrPreferredXferCharacteristics].UnsignedInt = VideoParameters->VideoHdrInfo.preferred_transfer_characteristics;

        //
        // Notify this event
        //
        mStream->SignalEvent(&DisplayEvent);
    }

    //
    // Save current Video parameter for next queued buffer
    //
    mVideoDisplayParameters = *VideoParameters;
}
//}}}


PlayerStatus_t   DecodeToManifestEdge_c::CallInSequence(
    PlayerSequenceType_t      SequenceType,
    PlayerSequenceValue_t     SequenceValue,
    PlayerComponentFunction_t Fn,
    ...)
{
    va_list                   List;
    Buffer_t                  ControlStructureBuffer;
    PlayerControlStructure_t *ControlStructure;

    BufferStatus_t Status = mStream->GetPlayer()->GetControlStructurePool()->GetBuffer(&ControlStructureBuffer, IdentifierInSequenceCall);
    if (Status != BufferNoError)
    {
        SE_ERROR("Stream 0x%p Failed to get a control structure buffer\n", mStream);
        return PlayerError;
    }

    ControlStructureBuffer->ObtainDataReference(NULL, NULL, (void **)(&ControlStructure));
    SE_ASSERT(ControlStructure != NULL); // not expected to be empty
    ControlStructure->Action            = ActionInSequenceCall;
    ControlStructure->SequenceType      = SequenceType;
    ControlStructure->SequenceValue     = SequenceValue;
    ControlStructure->InSequence.Fn     = Fn;

    switch (Fn)
    {
    case OutputTimerFnResetTimeMapping:
        break;

    case PlayerFnSwitchOutputTimer:
        va_start(List, Fn);
        ControlStructure->InSequence.Pointer    = (void *)va_arg(List, PlayerStream_t);
        va_end(List);
        break;

    default:
        SE_ERROR("Unsupported function call\n");
        ControlStructureBuffer->DecrementReferenceCount(IdentifierInSequenceCall);
        return PlayerNotSupported;
    }

    RingStatus_t ringStatus = mInputRing->Insert((uintptr_t)ControlStructureBuffer);
    if (ringStatus != RingNoError) { return PlayerError; }

    return PlayerNoError;
}

PlayerStatus_t   DecodeToManifestEdge_c::PerformInSequenceCall(PlayerControlStructure_t *ControlStructure)
{
    PlayerStatus_t  Status = PlayerNoError;

    switch (ControlStructure->InSequence.Fn)
    {
    case OutputTimerFnResetTimeMapping:
        SE_DEBUG(group_avsync, "OutputTimerFnResetTimeMapping\n");
        mOutputTimer->ResetTimeMapping();
        break;

    case PlayerFnSwitchOutputTimer:
        SwitchOutputTimer();
        break;

    default:
        SE_ERROR("Unsupported function call - Implementation error\n");
        Status  = PlayerNotSupported;
        break;
    }

    return Status;
}


// //////////////////////////////////////////////////////////////////////////////////////////////////
//
//      Switch stream component function for the OutputTimer
//

void    DecodeToManifestEdge_c::SwitchOutputTimer()
{
    SE_DEBUG(group_player, "Stream 0x%p\n", mStream);

    // We must reset to zero the VideoParameter data structure because we just switched to a new stream.
    // Rational_t variables of this structure must not be set to zero to avoid division by 0
    //
    memset((void *)&mVideoDisplayParameters, 0, sizeof(mVideoDisplayParameters));
    mVideoDisplayParameters.PixelAspectRatio = 1; // rational
    mVideoDisplayParameters.FrameRate        = 1; // rational

    // Next frame will be the first frame of a new stream
    mFirstFrame = true;
    OS_ResetEvent(&mThrottlingEvent);

    mPreviousPts = TimeStamp_c();

    //
    // If we are ready to switch the output timer then the last decode must
    // have come through, so we signal that the codec can be swapped.
    //
    OS_SetEvent(&mStream->SwitchStreamLastOneOutOfTheCodec);

    mManifestationCoordinator->ResetOnStreamSwitch();

    OS_SemaphoreSignal(&mStream->mSemaphoreStreamSwitchOutputTimer);

    SE_DEBUG(group_player, "Stream 0x%p completed\n", mStream);
}

