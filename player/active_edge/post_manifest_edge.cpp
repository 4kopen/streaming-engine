/************************************************************************
Copyright (C) 2003-2014 STMicroelectronics. All Rights Reserved.

This file is part of the Streaming Engine.

Streaming Engine is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License version 2 as
published by the Free Software Foundation.

Streaming Engine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with Streaming Engine; see the file COPYING.  If not, write to the Free
Software Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307,
USA.

The Streaming Engine Library may alternatively be licensed under a
proprietary license from ST.
************************************************************************/

#include "player_generic.h"
#include "player_playback.h"
#include "player_stream.h"
#include "post_manifest_edge.h"
#include "collate_to_parse_edge.h"
#include "parse_to_decode_edge.h"
#include "decode_to_manifest_edge.h"

#undef TRACE_TAG
#define TRACE_TAG "PostManifestEdge_c"

void PostManifestEdge_c::HandleMarkerFrame(Buffer_t Buffer, PlayerSequenceNumber_t *SequenceNumberStructure)
{
    SE_DEBUG(group_player, "Stream 0x%p Got Marker Frame #%llu (type %s)\n", mStream, SequenceNumberStructure->Value,
             MarkerFrameTypeString(SequenceNumberStructure->mMarkerFrame.mMarkerType));

    mStream->Statistics().FrameCountFromManifestor++;
    mStream->FramesFromManifestorCount++;

    mSequenceNumber                  = SequenceNumberStructure->Value;
    mMaximumActualSequenceNumberSeen = max(mSequenceNumber, mMaximumActualSequenceNumberSeen);

    if (SequenceNumberStructure->mMarkerFrame.mMarkerType == DrainDiscardMarker)
    {
        SE_DEBUG2(group_player, group_se_pipeline, "Stream 0x%p received drain Marker #%lld\n",
                  mStream, SequenceNumberStructure->mMarkerFrame.mSequenceNumber);
        StopDiscardingUntilDrainMarkerFrame();
    }

    ProcessAccumulatedBeforeControlMessages(mSequenceNumber);

    if (SequenceNumberStructure->mMarkerFrame.mMarkerType == EosMarker)
    {
        SE_DEBUG(group_player, "Stream 0x%p received EOS Marker #%lld\n", mStream, SequenceNumberStructure->mMarkerFrame.mSequenceNumber);
        HandleEosMarker();
    }

    Buffer->DecrementReferenceCount(IdentifierProcessPostManifest);
    ProcessAccumulatedAfterControlMessages(mSequenceNumber);
}

PlayerStatus_t PostManifestEdge_c::HandleEosMarker()
{
    PlayerEventRecord_t Event;
    Event.Code          = EventEndOfStream;
    Event.Playback      = mStream->GetPlayback();
    Event.Stream        = mStream;
    Event.PlaybackTime  = TIME_NOT_APPLICABLE;
    PlayerStatus_t  Status = mStream->SignalEvent(&Event);
    if (Status != PlayerNoError)
    {
        SE_ERROR("Stream 0x%p Failed to signal EOS event\n", mStream);
    }

    return Status;
}

void PostManifestEdge_c::HandleCodedFrameBuffer(
    Buffer_t Buffer,
    unsigned long long Now,
    PlayerSequenceNumber_t *SequenceNumberStructure,
    ParsedFrameParameters_t  *ParsedFrameParameters)
{
    (void)Now; // warning removal

    mStream->Statistics().FrameCountFromManifestor++;
    mStream->FramesFromManifestorCount++;

    if (mLastOutputTimeValid == false)
    {
        // confirm to the middleware that display has been (re) started
        PlayerEventRecord_t Event;
        Event.Code           = EventFrameSupplied;
        Event.Playback       = mStream->GetPlayback();
        Event.Stream         = mStream;
        mStream->SignalEvent(&Event);
    }

    mLastOutputTimeValid = true; // re-enable starvation report as we get something displayed

    OutputTiming_t  *OutputTiming;
    Buffer->ObtainMetaDataReference(mStream->GetPlayer()->MetaDataOutputTimingType, (void **)&OutputTiming);
    SE_ASSERT(OutputTiming != NULL);

    //
    // Extract the sequence number, and write the timing statistics
    //
    SE_EXTRAVERB(group_player, "Stream 0x%p FrameCountManifested: %d\n", mStream, ParsedFrameParameters->DisplayFrameIndex + 1);

    SequenceNumberStructure->TimeEntryInProcess3    = OS_GetTimeInMicroSeconds();
    mSequenceNumber                                 = SequenceNumberStructure->Value;
    mMaximumActualSequenceNumberSeen                = max(mSequenceNumber, mMaximumActualSequenceNumberSeen);
    mTime                                           = ParsedFrameParameters->PTS.NativeValue();

    //
    // Process any outstanding control messages to be applied before this buffer
    //
    ProcessAccumulatedBeforeControlMessages(mSequenceNumber);

    long long int ActualTime = (&OutputTiming->ManifestationTimings[0])->ActualSystemPlaybackTime;

    if (ValidTime(ActualTime))
    {
        SE_DEBUG2(group_player, group_se_pipeline, "Stream 0x%p - %d - #%lld PTS=%lld PM=%llu\n",
                  mStream,
                  mStream->GetStreamType(),
                  SequenceNumberStructure->Value,
                  mTime,
                  ActualTime);
    }
    else
    {
        SE_DEBUG2(group_player, group_se_pipeline, "Stream 0x%p - %d - #%lld PTS=%lld PM=INVALID_TIME\n",
                  mStream,
                  mStream->GetStreamType(),
                  SequenceNumberStructure->Value,
                  mTime);
    }

    SE_DEBUG2(group_player, group_se_pipeline, "Stream 0x%p - %d - #%lld PTS=%lld EXP=%llu\n",
              mStream,
              mStream->GetStreamType(),
              SequenceNumberStructure->Value,
              SequenceNumberStructure->PTS,
              OutputTiming->BaseSystemPlaybackTime
             );

    // Pass buffer back into output timer if not discarding
    if (!mStream->IsUnPlayable() && !IsDiscardingUntilDrainMarkerFrame())
    {
        mStream->GetOutputTimer()->RecordActualFrameTiming(Buffer);
    }
    // and release the buffer
    mStream->GetCodec()->ReleaseDecodeBuffer(Buffer);

    //
    // Process any outstanding control messages to be applied after this buffer
    //
    ProcessAccumulatedAfterControlMessages(mSequenceNumber);
}

void PostManifestEdge_c::HandleCodedFrameBufferType(Buffer_t Buffer, unsigned long long Now)
{
    Buffer_t OriginalCodedFrameBuffer;
    Buffer->ObtainAttachedBufferReference(mStream->GetCodedFrameBufferType(), &OriginalCodedFrameBuffer);
    SE_ASSERT(OriginalCodedFrameBuffer != NULL);

    PlayerSequenceNumber_t   *SequenceNumberStructure;
    OriginalCodedFrameBuffer->ObtainMetaDataReference(mStream->GetPlayer()->MetaDataSequenceNumberType, (void **)(&SequenceNumberStructure));
    SE_ASSERT(SequenceNumberStructure != NULL);

    ParsedFrameParameters_t  *ParsedFrameParameters;
    Buffer->ObtainMetaDataReference(mStream->GetPlayer()->MetaDataParsedFrameParametersReferenceType, (void **)(&ParsedFrameParameters));
    SE_ASSERT(ParsedFrameParameters != NULL);

    if (SequenceNumberStructure->mIsMarkerFrame)
    {
        HandleMarkerFrame(Buffer, SequenceNumberStructure);
    }
    else
    {
        HandleCodedFrameBuffer(Buffer, Now, SequenceNumberStructure, ParsedFrameParameters);
    }
}

void   PostManifestEdge_c::MainLoop()
{
    RingStatus_t              RingStatus;
    Buffer_t                  Buffer;
    BufferType_t              BufferType;
    unsigned long long        Now;
    unsigned long long        LoopIdleDuration;

    //
    // Signal we have started
    //
    OS_LockMutex(&mStream->StartStopLock);
    mStream->ProcessRunningCount++;
    OS_SetEvent(&mStream->StartStopEvent);
    OS_UnLockMutex(&mStream->StartStopLock);

    SE_DEBUG(group_player, "process starting Stream 0x%p\n", mStream);

    //
    // Main Loop
    //
    mStream->EnterManifestorSharedSection();
    while (!mStream->IsTerminating())
    {
        mStream->ExitManifestorSharedSection();

        // wait for ever if starvation detection is off
        if (mLastOutputTimeValid) { LoopIdleDuration = PLAYER_MAX_EVENT_WAIT; }
        else { LoopIdleDuration = OS_INFINITE; }

        RingStatus  = mInputRing->Extract((uintptr_t *)(&Buffer) , LoopIdleDuration);
        mStream->EnterManifestorSharedSection();

        Now = OS_GetTimeInMicroSeconds();

        if ((RingStatus == RingNothingToGet) || (Buffer == NULL))
        {
            if (mStream->GetPlayback()->mSpeed != 0)
            {
                if (mLastOutputTimeValid)
                {
                    if ((mStream->FramesToManifestorCount > 0)
                        && !(mStream->CollateToParseEdge->GetInputPort()->NonEmpty())
                        && !(mStream->ParseToDecodeEdge->GetInputPort()->NonEmpty())
                        && !(mStream->DecodeToManifestEdge->GetInputPort()->NonEmpty()))
                    {
                        PlayerEventRecord_t Event;
                        Event.Code           = EventFrameStarvation;
                        Event.Playback       = mStream->GetPlayback();
                        Event.Stream         = mStream;
                        mStream->SignalEvent(&Event);
                        mLastOutputTimeValid = false; // starvation is reported once but might be re-enabled only if we displayed something new
                    }
                }
            }
            else
            {
                mLastOutputTimeValid = false; // pausing is not a manifestation starvation
            }

            continue;
        }

        mStream->BuffersComingOutOfManifestation = true;
        LoopIdleDuration = PLAYER_MAX_EVENT_WAIT;
        Buffer->GetType(&BufferType);
        Buffer->TransferOwnership(IdentifierProcessPostManifest);

        //
        // Deal with a coded frame buffer
        //

        if (BufferType == mStream->DecodeBufferType)
        {
            HandleCodedFrameBufferType(Buffer, Now);
        }
        //
        // Deal with a player control structure
        //
        else if (BufferType == mStream->GetPlayer()->BufferPlayerControlStructureType)
        {
            HandlePlayerControlStructure(Buffer, mSequenceNumber, mMaximumActualSequenceNumberSeen);
        }
        else
        {
            SE_ERROR("Unknown buffer type received - Implementation error\n");
            Buffer->DecrementReferenceCount();
        }
    }
    mStream->ExitManifestorSharedSection();

    SE_DEBUG(group_player, "process terminating Stream 0x%p\n", mStream);

    //
    // Signal we have terminated
    //
    OS_LockMutex(&mStream->StartStopLock);
    mStream->ProcessRunningCount--;
    OS_SetEvent(&mStream->StartStopEvent);
    OS_UnLockMutex(&mStream->StartStopLock);
}

PlayerStatus_t   PostManifestEdge_c::CallInSequence(
    PlayerSequenceType_t      SequenceType,
    PlayerSequenceValue_t     SequenceValue,
    PlayerComponentFunction_t Fn,
    ...)
{
    va_list                   List;
    Buffer_t                  ControlStructureBuffer;
    PlayerControlStructure_t *ControlStructure;

    BufferStatus_t BufferStatus = mStream->GetPlayer()->GetControlStructurePool()->GetBuffer(&ControlStructureBuffer, IdentifierInSequenceCall);
    if (BufferStatus != BufferNoError)
    {
        SE_ERROR("Failed to get a control structure buffer\n");
        return PlayerError;
    }

    ControlStructureBuffer->ObtainDataReference(NULL, NULL, (void **)(&ControlStructure));
    SE_ASSERT(ControlStructure != NULL); // not expected to be empty
    ControlStructure->Action            = ActionInSequenceCall;
    ControlStructure->SequenceType      = SequenceType;
    ControlStructure->SequenceValue     = SequenceValue;
    ControlStructure->InSequence.Fn     = Fn;

    switch (Fn)
    {
    case OSFnSetEventOnPostManifestation:
        va_start(List, Fn);
        ControlStructure->InSequence.Pointer    = (void *)va_arg(List, OS_Event_t *);
        va_end(List);
        break;

    case PlayerFnSwitchComplete:
        break;

    default:
        SE_ERROR("Unsupported function call\n");
        ControlStructureBuffer->DecrementReferenceCount(IdentifierInSequenceCall);
        return PlayerNotSupported;
    }

    RingStatus_t ringStatus = mInputRing->Insert((uintptr_t)ControlStructureBuffer);
    if (ringStatus != RingNoError) { return PlayerError; }

    return PlayerNoError;
}

PlayerStatus_t   PostManifestEdge_c::PerformInSequenceCall(PlayerControlStructure_t *ControlStructure)
{
    PlayerStatus_t  Status = PlayerNoError;

    switch (ControlStructure->InSequence.Fn)
    {
    case OSFnSetEventOnPostManifestation:
        OS_SetEvent((OS_Event_t *)ControlStructure->InSequence.Pointer);
        break;

    case PlayerFnSwitchComplete:
        SwitchComplete();
        break;

    default:
        SE_ERROR("Unsupported function call - Implementation error\n");
        Status  = PlayerNotSupported;
        break;
    }

    return Status;
}

// //////////////////////////////////////////////////////////////////////////////////////////////////
//
//      Switch stream component function to recognize the switch is complete
//

void    PostManifestEdge_c::SwitchComplete()
{
    SE_DEBUG(group_player, "Stream 0x%p\n", mStream);
    OS_SemaphoreSignal(&mStream->mSemaphoreStreamSwitchComplete);
    SE_DEBUG(group_player, "Stream 0x%p completed\n", mStream);
}
