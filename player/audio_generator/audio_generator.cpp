/************************************************************************
Copyright (C) 2003-2014 STMicroelectronics. All Rights Reserved.

This file is part of the Streaming Engine.

Streaming Engine is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License version 2 as
published by the Free Software Foundation.

Streaming Engine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with Streaming Engine; see the file COPYING.  If not, write to the Free
Software Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307,
USA.

The Streaming Engine Library may alternatively be licensed under a
proprietary license from ST.
************************************************************************/

#include "audio_generator.h"
#include "audio_conversions.h"

#undef TRACE_TAG
#define TRACE_TAG "Audio_Generator_c"

////////////////////////////////////////////////////////////////////////////
///
///
///
Audio_Generator_c::Audio_Generator_c(const char *name)
    : Name()
    , RegisteredWithMixer(false)
    , PcmContent()
    , Mixer(NULL)
    , Buffer(NULL)
    , BufferSize(0)
    , BufferWriteOffset(0)
    , BufferReadOffset(0)
    , BufferCommittedBytes(0)
    , BytesPerSample(0)
    , PcmFormat()
    , Emphasis()
    , Info()
    , ApplicationType()
    , mBufferLock()
    , mInfoLock()
{
    strncpy(Name, name, sizeof(Name));
    Name[sizeof(Name) - 1] = '\0';

    Info.state = STM_SE_AUDIO_GENERATOR_DISCONNECTED;

    OS_InitializeSpinLock(&mBufferLock);
    OS_InitializeSpinLock(&mInfoLock);
}

////////////////////////////////////////////////////////////////////////////
///
///
///
Audio_Generator_c::~Audio_Generator_c()
{
    OS_TerminateSpinLock(&mBufferLock);
    OS_TerminateSpinLock(&mInfoLock);
}

int32_t Audio_Generator_c::SetCompoundOption(stm_se_ctrl_t ctrl, const void *value)
{
    SE_DEBUG(group_audio_generator, ">%s (compound-ctrl= %d)\n", Name, ctrl);

    switch (GetState())
    {
    case STM_SE_AUDIO_GENERATOR_DISCONNECTED:
    case STM_SE_AUDIO_GENERATOR_STOPPED:
        break;
    case STM_SE_AUDIO_GENERATOR_STARTED:
        return -EBUSY;
    }

    switch (ctrl)
    {
    case STM_SE_CTRL_AUDIO_GENERATOR_BUFFER:
    {
        stm_se_audio_generator_buffer_t *buffer = (stm_se_audio_generator_buffer_t *)value;
        if (buffer == NULL)
        {
            SE_ERROR("%s STM_SE_CTRL_AUDIO_GENERATOR_BUFFER: NULL pointer\n", Name);
            return -EINVAL;
        }

        BytesPerSample = StmSeAudioGetNrBytesFromLpcmFormat(buffer->format);
        if (BytesPerSample == 0)
        {
            SE_ERROR("%s Unsupported STM_SE_CTRL_AUDIO_GENERATOR_BUFFER.Format %d\n", Name, (int) buffer->format);
            return -EINVAL;
        }

        PcmFormat            = buffer->format;
        Buffer               = (unsigned char *)buffer->audio_buffer;
        BufferSize           = buffer->audio_buffer_size;
        BytesPerSample      *= GetNumChannels();

        // These are protected by mBufferLock
        BufferCommittedBytes = 0;
        BufferWriteOffset    = 0;
        BufferReadOffset     = 0;

        UpdateConsummedInfo();

        SE_DEBUG(group_audio_generator, "STM_SE_CTRL_AUDIO_GENERATOR_BUFFER: BufferSize:%d BytesPerSample:%d NbSamples:%d\n",
                 BufferSize, BytesPerSample, (BytesPerSample != 0) ? BufferSize / BytesPerSample : 0);
        break;
    }

    case STM_SE_CTRL_AUDIO_INPUT_FORMAT:
        memcpy((void *) &PcmContent, value, sizeof(stm_se_audio_core_format_t));
        break;

    default:
    {
        SE_ERROR("%s Invalid compound-control %d\n", Name, ctrl);
        return -EINVAL;
    }
    }

    return 0;
}

int32_t Audio_Generator_c::GetCompoundOption(stm_se_ctrl_t ctrl, void *value)
{
    switch (ctrl)
    {
    case STM_SE_CTRL_AUDIO_GENERATOR_BUFFER:
    {
        stm_se_audio_generator_buffer_t *buffer = (stm_se_audio_generator_buffer_t *) value;
        buffer->audio_buffer       = Buffer;
        buffer->audio_buffer_size  = BufferSize;
        buffer->format             = PcmFormat;
        break;
    }

    case STM_SE_CTRL_AUDIO_INPUT_FORMAT:
    {
        memcpy(value, &PcmContent, sizeof(stm_se_audio_core_format_t));
        break;
    }

    default:
        SE_ERROR("%s Invalid control %d\n", Name, ctrl);
        return -EINVAL;
    }

    return 0;
}

int32_t Audio_Generator_c::SetOption(stm_se_ctrl_t ctrl, int32_t value)
{
    SE_DEBUG(group_audio_generator, ">%s (ctrl= %d, value= %d)\n", Name, ctrl, value);

    switch (ctrl)
    {
    case STM_SE_CTRL_AUDIO_APPLICATION_TYPE:
        if (value <= STM_SE_CTRL_VALUE_LAST_AUDIO_APPLICATION_TYPE)
        {
            ApplicationType = value;
        }
        else
        {
            SE_ERROR(">%s: APPLICATION_TYPE invalid value %d\n", Name, value);
        }

        break;

    case STM_SE_CTRL_AUDIO_INPUT_EMPHASIS:
        Emphasis = ((value >= STM_SE_NO_EMPHASIS) && (value <= STM_SE_EMPH_CCITT_J_17)) ? (stm_se_emphasis_type_t) value : STM_SE_NO_EMPHASIS;
        break;

    default:
        SE_ERROR("%s Invalid control %d :: %d\n", Name, ctrl, value);
        return -EINVAL;
    }

    return 0;
}

int32_t Audio_Generator_c::GetOption(stm_se_ctrl_t ctrl, int32_t *value)
{
    switch (ctrl)
    {
    case STM_SE_CTRL_AUDIO_APPLICATION_TYPE:
        *value = ApplicationType;
        SE_DEBUG(group_audio_generator, ">%s %d [AUDIO_APPLICATION_TYPE] : %d\n", Name, ctrl, *value);
        break;

    case STM_SE_CTRL_AUDIO_INPUT_EMPHASIS:
        *value = Emphasis;
        break;

    default:
        SE_ERROR("%s Invalid control %d\n", Name, ctrl);
        return -EINVAL;
    }

    return 0;
}

int32_t Audio_Generator_c::Start()
{
    if (RegisteredWithMixer)
    {
        //Change the Audio Generator State to Started.
        SetState(STM_SE_AUDIO_GENERATOR_STARTED);
        PlayerStatus_t status = Mixer->StartAudioGenerator(this);
        return (status == PlayerNoError) ? 0 : -EINVAL;
    }

    SE_ERROR("Failed to start %s as it is not attached to any sink\n", Name);

    return -ENODEV;
}

int32_t Audio_Generator_c::Stop()
{
    SE_DEBUG(group_audio_generator, ">%s\n", Name);

    if (RegisteredWithMixer)
    {
        //Change the Audio Generator State to Stopped.
        SetState(STM_SE_AUDIO_GENERATOR_STOPPED);
        PlayerStatus_t status = Mixer->StopAudioGenerator();
        return (status == PlayerNoError) ? 0 : -EINVAL;
    }

    SE_ERROR("Failed to Stop %s as it is not attached to any sink\n", Name);
    return -ENODEV;
}

static inline const char *LookupAudioGeneratorState(stm_se_audio_generator_state_t state)
{
    switch (state)
    {
#define E(x) case x: return #x
        E(STM_SE_AUDIO_GENERATOR_DISCONNECTED);
        E(STM_SE_AUDIO_GENERATOR_STOPPED);
        E(STM_SE_AUDIO_GENERATOR_STARTED);
    }

    return "UNKNONWN STATE";
}

int32_t Audio_Generator_c::SetState(stm_se_audio_generator_state_t state)
{
    SE_DEBUG(group_audio_generator, "%s -> %s\n",
             LookupAudioGeneratorState(Info.state),
             LookupAudioGeneratorState(state));
    Info.state = state;
    return 0;
}

void Audio_Generator_c::UpdateConsummedInfo()
{
    // It is caller responsibility to lock or not mBufferLock.
    // However if both lock are needed, should be taken in this
    // order only "First mBufferLock then mInfoLock"

    OS_LockSpinLock(&mInfoLock);

    uint32_t available_bytes = BufferSize - BufferCommittedBytes;

    Info.avail       = BytesPerSample ? available_bytes / BytesPerSample : 0;
    Info.head_offset = BufferWriteOffset;

    OS_UnLockSpinLock(&mInfoLock);
}

void Audio_Generator_c::GetInfo(stm_se_audio_generator_info_t *info) const
{
    OS_LockSpinLock(&mInfoLock);
    *info = Info;
    OS_UnLockSpinLock(&mInfoLock);
}

int32_t Audio_Generator_c::Commit(uint32_t number_of_samples)
{
    uint32_t number_of_bytes = number_of_samples * BytesPerSample;

    OS_LockSpinLock(&mBufferLock);
    uint32_t available_bytes = BufferSize - BufferCommittedBytes;

    uint32_t trace_bufwriteoffset;

    if (number_of_bytes > available_bytes)
    {
        trace_bufwriteoffset = BufferWriteOffset;
        OS_UnLockSpinLock(&mBufferLock);

        SE_DEBUG(group_audio_generator, "%p attempt to add %d samples but this would overflow [only %d bytes can be written | WritePtr = %d]\n",
                 this, number_of_samples, available_bytes, trace_bufwriteoffset);
        return -EINVAL;
    }

    BufferWriteOffset += number_of_bytes;

    if (BufferWriteOffset >= BufferSize)
    {
        BufferWriteOffset -= BufferSize;
    }

    BufferCommittedBytes += number_of_bytes;
    UpdateConsummedInfo();

    trace_bufwriteoffset = BufferWriteOffset;
    uint32_t trace_bufcommitedbytes = BufferCommittedBytes;

    OS_UnLockSpinLock(&mBufferLock);

    SE_DEBUG(group_audio_generator, "%p %d samples added [total %d bytes to render | WritePtr = %d ]\n",
             this, number_of_samples, trace_bufcommitedbytes, trace_bufwriteoffset);

    return 0;
}

int32_t  Audio_Generator_c::SignalEvent(stm_se_audio_generator_event_t Event)
{
    stm_event_t   GeneratorEvent;
    int32_t err = 0;
    GeneratorEvent.object = (stm_object_h)this;
    GeneratorEvent.event_id = (uint32_t)Event;
    // Signal Event via Event manager
    err = stm_event_signal(&GeneratorEvent);
    if (err != 0)
    {
        SE_ERROR("%s Failed to Generate signal Event (%d)\n", Name, err);
        return HavanaError;
    }

    return 0;
}

int32_t Audio_Generator_c::SamplesConsumed(uint32_t number_of_samples)
{
    uint32_t number_of_bytes = number_of_samples * BytesPerSample;

    OS_LockSpinLock(&mBufferLock);

    uint32_t trace_bufcommitedbytes = BufferCommittedBytes;

    if (number_of_bytes > BufferCommittedBytes)
    {
        OS_UnLockSpinLock(&mBufferLock);
        SE_ASSERT(number_of_bytes <= trace_bufcommitedbytes);
        OS_LockSpinLock(&mBufferLock);
    }

    BufferCommittedBytes -= number_of_bytes;
    BufferReadOffset += number_of_bytes;

    while (BufferReadOffset >= BufferSize)
    {
        BufferReadOffset -= BufferSize;
    }

    UpdateConsummedInfo();

    uint32_t trace_bufreadoffset = BufferReadOffset;
    trace_bufcommitedbytes = BufferCommittedBytes;

    OS_UnLockSpinLock(&mBufferLock);

    SE_DEBUG(group_audio_generator, "%p %d samples consummed [%d bytes to render | ReadPtr = %d ]\n",
             this, number_of_samples, trace_bufcommitedbytes, trace_bufreadoffset);

    if (number_of_samples != 0)
    {
        uint32_t Status  = this->SignalEvent(STM_SE_AUDIO_GENERATOR_EVENT_DATA_CONSUMED);

        if (Status != 0)
        {
            SE_ERROR("Failed to signal event\n");
            return -EINVAL;
        }
    }

    return 0;
}

uint32_t Audio_Generator_c::GetBytesPerSample()
{
    return BytesPerSample;
}

uint32_t Audio_Generator_c::GetBufferReadOffset(void)
{
    OS_LockSpinLock(&mBufferLock);
    uint32_t buffer_read_offset = BufferReadOffset;
    OS_UnLockSpinLock(&mBufferLock);
    return buffer_read_offset;
}

uint32_t Audio_Generator_c::GetBufferCommittedBytes(void)
{
    OS_LockSpinLock(&mBufferLock);
    uint32_t commit_bytes = BufferCommittedBytes;
    OS_UnLockSpinLock(&mBufferLock);
    return commit_bytes;
}

stm_se_audio_generator_state_t Audio_Generator_c::GetState()
{
    return Info.state;
}

uint32_t Audio_Generator_c::GetApplicationType()
{
    return ApplicationType;
}

bool     Audio_Generator_c::IsInteractiveAudio()
{
    return (ApplicationType == STM_SE_CTRL_VALUE_AUDIO_APPLICATION_DVD);
}

uint32_t Audio_Generator_c::GetNumChannels()
{
    return PcmContent.channel_placement.channel_count;
}

enum eAccAcMode Audio_Generator_c::GetAudioMode()
{
    eAccAcMode Mode = ACC_MODE_ID;
    StmSeAudioChannelPlacementAnalysis_t Analysis;
    stm_se_audio_channel_placement_t SortedPlacement;
    bool AudioModeIsPhysical;

    if (0 != StmSeAudioGetAcmodAndAnalysisFromChannelPlacement(&Mode, &AudioModeIsPhysical,
                                                               &SortedPlacement, &Analysis,
                                                               &PcmContent.channel_placement))
    {
        SE_ERROR("Failed to convert to Audio Mode. Using MODE_ID\n");
    }

    return Mode;
}

uint32_t Audio_Generator_c::GetSamplingFrequency()
{
    return PcmContent.sample_rate;
}

AudioGeneratorStatus_t Audio_Generator_c::AttachMixer(Mixer_Mme_c *mixer)
{
    PlayerStatus_t Status;
    SE_DEBUG(group_audio_generator, ">%s %s\n", Name, LookupAudioGeneratorState(GetState()));

    if (RegisteredWithMixer || GetState() != STM_SE_AUDIO_GENERATOR_DISCONNECTED)
    {
        if (mixer == Mixer)
        {
            SE_WARNING("%s is already attached to the same Mixer:%p\n", Name, Mixer);
        }
        else
        {
            SE_ERROR("%s is already attached to another Mixer:%p\n", Name, Mixer);
        }
        return PlayerError;
    }

    Mixer = mixer;
    Status = Mixer->SendSetupAudioGeneratorRequest(this);

    if (Status != PlayerNoError)
    {
        Mixer = (Mixer_Mme_c *)0;
        SE_ERROR("%s Cannot register with the mixer\n", Name);
        return Status;
    }

    SetState(STM_SE_AUDIO_GENERATOR_STOPPED);

    RegisteredWithMixer = true;
    SE_DEBUG(group_audio_generator, "<\n");
    return PlayerNoError;
}

AudioGeneratorStatus_t Audio_Generator_c::DetachMixer(Mixer_Mme_c *mixer)
{
    SE_DEBUG(group_audio_generator, ">%s State:%s\n", Name, LookupAudioGeneratorState(GetState()));

    if (RegisteredWithMixer && mixer == Mixer)
    {
        if (GetState() == STM_SE_AUDIO_GENERATOR_STARTED)
        {
            SE_DEBUG(group_audio_generator, ">%s Detach AudioGenerator while running -> Will Stop it first\n", Name);
            Stop();
        }

        if (Mixer->SendFreeAudioGeneratorRequest(this) != PlayerNoError)
        {
            SE_ERROR("%s Cannot unregister with the mixer\n", Name);
            // no recovery possible
        }

        this->Mixer = (Mixer_Mme_c *)0;
        RegisteredWithMixer = false;

        SetState(STM_SE_AUDIO_GENERATOR_DISCONNECTED);
    }

    SE_DEBUG(group_audio_generator, "<\n");
    return PlayerNoError;
}

void  Audio_Generator_c::AudioGeneratorEventUnderflow(uint32_t SamplesToDescatter, Rational_t RescaleFactor)
{
    uint32_t TimeToFillInSamples;
    uint32_t NumberOfSamplesAvailable;
    uint32_t Status;
    OS_LockSpinLock(&mBufferLock);
    NumberOfSamplesAvailable = BufferCommittedBytes / BytesPerSample;
    OS_UnLockSpinLock(&mBufferLock);
    TimeToFillInSamples = (RescaleFactor * (Rational_t)SamplesToDescatter).RoundedUpIntegerPart();

    if (TimeToFillInSamples > NumberOfSamplesAvailable)
    {
        Status  = this->SignalEvent(STM_SE_AUDIO_GENERATOR_EVENT_UNDERFLOW);

        if (Status != 0)
        {
            SE_ERROR("Failed to signal event\n");
        }
    }
}
