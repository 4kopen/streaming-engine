/************************************************************************
Copyright (C) 2003-2014 STMicroelectronics. All Rights Reserved.

This file is part of the Streaming Engine.

Streaming Engine is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License version 2 as
published by the Free Software Foundation.

Streaming Engine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with Streaming Engine; see the file COPYING.  If not, write to the Free
Software Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307,
USA.

The Streaming Engine Library may alternatively be licensed under a
proprietary license from ST.
************************************************************************/

#include "osinline.h"

#include "buffer_generic.h"

CreatePoolInfo_t::CreatePoolInfo_t()
    :  Type()
    ,  NumberOfBuffers(UNRESTRICTED_NUMBER_OF_BUFFERS)
    ,  Size(UNSPECIFIED_SIZE)
    ,  MemoryPool(NULL)
    ,  ArrayOfMemoryBlocks(NULL)
    ,  DeviceMemoryPartitionName(NULL)
    ,  AllowCross64MbBoundary(true)
    ,  MemoryAccessType(MEMORY_DEFAULT_ACCESS)
    ,  MemoryDevice(NULL)
{
}

#undef TRACE_TAG
#define TRACE_TAG "BufferManager_Generic_c"

BufferManager_Generic_c::BufferManager_Generic_c()
    : BufMgrLock()
    , TypeDescriptorCount(0)
    , TypeDescriptors()
    , ListOfBufferPools(NULL)
    , mEternalPoolsMarked(false)
    , mDumpPool()
    , mLogRefCountInPool()
{
    SE_VERBOSE(group_buffer, "0x%p\n", this);

    OS_InitializeMutex(&BufMgrLock);
}

BufferManager_Generic_c::~BufferManager_Generic_c()
{
    SE_VERBOSE(group_buffer, "0x%p\n", this);

    OS_LockMutex(&BufMgrLock);
    CheckForPoolLeaks();
    OS_UnLockMutex(&BufMgrLock);

    // Destroy eternal pools and leaked pools.
    BufferPool_Generic_c *Next = NULL;
    for (BufferPool_Generic_c *Pool = ListOfBufferPools; Pool != NULL; Pool = Next)
    {
        Next = Pool->Next;

        // An eternal pool managed by shared pointers could be destructed after
        // this function returns for example if one of its pointers is a global
        // variable.  Do nothing in this case to avoid a double-deletion.
        if (Pool->IsManagedWithSharedPtr() == false)
        {
            delete Pool;
        }
    }

    for (unsigned int i = 0; i < TypeDescriptorCount; i++)
    {
        OS_Free(const_cast<char *>(TypeDescriptors[i].TypeName));
    }

    OS_TerminateMutex(&BufMgrLock);
}

// //////////////////////////////////////////////////////////////////////////////////////////////////
//
//  Add to defined types
//

BufferStatus_t   BufferManager_Generic_c::CreateBufferDataType(
    BufferDataDescriptor_t   *Descriptor,
    BufferType_t         *Type)
{
    SE_ASSERT(Descriptor != NULL);
    SE_VERBOSE(group_buffer, "0x%p Type %d Typename %s\n",
               this, Descriptor->Type, (Descriptor->TypeName == NULL) ? "Unnamed" : Descriptor->TypeName);
    //
    // Initialize the return parameter
    //
    *Type   = 0xffffffff;

    if (Descriptor->Type == MetaDataTypeBase)
    {
        if (Descriptor->AllocationSource == AllocateIndividualSuppliedBlocks)
        {
            SE_ERROR("0x%p Cannot allocate meta data from supplied block list\n", this);
            return BufferUnsupportedAllocationSource;
        }

        if (Descriptor->AllocateOnPoolCreation)
        {
            SE_ERROR("0x%p Cannot allocate meta data on pool creation\n", this);
            return BufferUnsupportedAllocationSource;
        }
    }
    else if (Descriptor->Type != BufferDataTypeBase)
    {
        SE_ERROR("0x%p Invalid type value\n", this);
        return BufferInvalidDescriptor;
    }

    //

    if (Descriptor->HasFixedSize && (Descriptor->FixedSize == NOT_SPECIFIED))
    {
        SE_ERROR("0x%p Fixed size not specified\n", this);
        return BufferInvalidDescriptor;
    }

    //
    // From here on in we need to lock access to the data structure, since we are now allocating an entry
    //
    OS_LockMutex(&BufMgrLock);

    if (TypeDescriptorCount >= MAX_BUFFER_DATA_TYPES)
    {
        OS_UnLockMutex(&BufMgrLock);
        SE_ERROR("0x%p Insufficient space to hold new data type descriptor\n", this);
        return BufferTooManyDataTypes;
    }

    //
    memcpy(&TypeDescriptors[TypeDescriptorCount], Descriptor, sizeof(BufferDataDescriptor_t));

    if (Descriptor->TypeName != NULL)
    {
        TypeDescriptors[TypeDescriptorCount].TypeName   = static_cast<const char *>(OS_StrDup(Descriptor->TypeName));

        if (TypeDescriptors[TypeDescriptorCount].TypeName == NULL)
        {
            OS_UnLockMutex(&BufMgrLock);
            SE_ERROR("0x%p Insufficient memory to copy type name\n", this);
            return BufferInsufficientMemoryGeneral;
        }
    }

    TypeDescriptors[TypeDescriptorCount].Type   = Descriptor->Type | TypeDescriptorCount;
    *Type                   = TypeDescriptors[TypeDescriptorCount].Type;
    //
    TypeDescriptorCount++;
    OS_UnLockMutex(&BufMgrLock);
    return BufferNoError;
}

// //////////////////////////////////////////////////////////////////////////////////////////////////
//
//  Retrieve the descriptor for a defined type
//

BufferStatus_t   BufferManager_Generic_c::FindBufferDataType(
    const char       *TypeName,
    BufferType_t     *Type)
{
    unsigned int    i;

    //
    // Initialize the return parameter
    //

    if (TypeName == NULL)
    {
        SE_ERROR("0x%p Cannot find an unnamed type\n", this);
        return BufferDataTypeNotFound;
    }

    OS_LockMutex(&BufMgrLock);

    for (i = 0; i < TypeDescriptorCount; i++)
        if ((TypeDescriptors[i].TypeName != NULL) &&
            (strncmp(TypeDescriptors[i].TypeName, TypeName, BUFFER_DATADESC_MAX_NAME_LENGTH) == 0))
        {
            *Type   = TypeDescriptors[i].Type;
            OS_UnLockMutex(&BufMgrLock);
            return BufferNoError;
        }

    OS_UnLockMutex(&BufMgrLock);
    return BufferDataTypeNotFound;
}

// //////////////////////////////////////////////////////////////////////////////////////////////////
//
//      Get descriptor
//

BufferStatus_t   BufferManager_Generic_c::GetDescriptor(BufferType_t             Type,
                                                        BufferPredefinedType_t   RequiredKind,
                                                        BufferDataDescriptor_t **Descriptor)
{
    if ((Type & TYPE_TYPE_MASK) != (unsigned int)RequiredKind)
    {
        SE_ERROR("0x%p Wrong kind of data type (%08x)\n", this, RequiredKind);
        return BufferDataTypeNotFound;
    }

    OS_LockMutex(&BufMgrLock);

    if ((Type & TYPE_INDEX_MASK) > TypeDescriptorCount)
    {
        OS_UnLockMutex(&BufMgrLock);
        SE_ERROR("0x%p Attempt to create buffer with unregisted type (%08x)\n", this, Type);
        return BufferDataTypeNotFound;
    }

    *Descriptor = &TypeDescriptors[Type & TYPE_INDEX_MASK];
    OS_UnLockMutex(&BufMgrLock);

    if ((*Descriptor)->Type != Type)
    {
        SE_ERROR("0x%p Type descriptor does not match type\n", this);
        return BufferInvalidDescriptor;
    }

    return BufferNoError;
}


// //////////////////////////////////////////////////////////////////////////////////////////////////
//
//      Create a pool of buffers - common implementation
//

// Create a pool and return a raw pointer to it.  Callers must call
// DestroyPool() to delete the created pool.
// TODO(theryn): Make private when all pools managed with shared pointers.
BufferStatus_t   BufferManager_Generic_c::CreatePool(BufferPool_t *Pool, const CreatePoolInfo_t &Info)
{
    BufferDataDescriptor_t *Descriptor;
    BufferPool_Generic_t    NewPool;

    //
    // Initialize the return parameter
    //

    if (Pool == NULL)
    {
        SE_ERROR("0x%p Null supplied as place to return Pool pointer\n", this);
        return BufferError;
    }

    *Pool   = NULL;
    //
    // Get the descriptor
    //
    BufferStatus_t Status  = GetDescriptor(Info.Type, BufferDataTypeBase, &Descriptor);
    if (Status != BufferNoError)
    {
        return Status;
    }

    SE_DEBUG(group_buffer, "0x%p Type %d Typename %s NumberOfBuffers %d Size %d partition %s AllowCross64MbBoundary:%d\n",
             this, Descriptor->Type, (Descriptor->TypeName == NULL) ? "Unnamed" : Descriptor->TypeName,
             Info.NumberOfBuffers, Info.Size, (Info.DeviceMemoryPartitionName == NULL) ? "Unnamed" : Info.DeviceMemoryPartitionName,
             Info.AllowCross64MbBoundary);

    //
    // Perform simple parameter checks
    //

    if (Descriptor->AllocateOnPoolCreation &&
        (Info.NumberOfBuffers == NOT_SPECIFIED))
    {
        SE_ERROR("0x%p Unable to allocate on creation, NumberOfBuffers not specified\n", this);
        return BufferParametersIncompatibleWithAllocationSource;
    }

    //
    // Perform the creation
    //
    NewPool = BufferPool_Generic_c::New(this, Descriptor, Info);
    if (NewPool == NULL)
    {
        SE_ERROR("0x%p Failed to construct pool\n", this);
        return BufferInsufficientMemoryForPool;
    }

    //
    // Insert the pool into our list
    //
    OS_LockMutex(&BufMgrLock);
    NewPool->Next   = ListOfBufferPools;
    ListOfBufferPools   = NewPool;
    OS_UnLockMutex(&BufMgrLock);

    *Pool = NewPool;

    SE_DEBUG(group_buffer, "0x%p Created pool 0x%p\n", this, NewPool);
    return BufferNoError;
}

// Create a pool and return a shared pointer to it.
// The shared pointer should then be copied in all objects accessing the pool.
// When the last shared pointer is destroyed, the pool is destroyed too.
//
// Do not call DestroyPool() for deleting a pool created with this function.
// Relax and let shared pointers do the hard work.
BufferStatus_t   BufferManager_Generic_c::CreatePool(
    SharedPtr_c<BufferPool_c>     *SharedPtr,
    const CreatePoolInfo_t        &Info)
{
    BufferPool_c *RawPtr = NULL;
    BufferStatus_t Status = CreatePool(&RawPtr, Info);
    if (Status == BufferNoError)
    {
        if (SharedPtr->Reset(RawPtr) == OS_NO_ERROR)
        {
            // Assign self-pointer to newly created pool so it can create strong
            // pointers from buffers to itself later on.
            static_cast<BufferPool_Generic_c *>(RawPtr)->SetSelfWeakPtr(*SharedPtr);
        }
        else
        {
            Status = BufferInsufficientMemoryGeneral;
            delete RawPtr;
        }
    }
    return Status;
}

// Create a pool and return a raw pointer to it.  Callers must call
// DestroyPool() to delete the created pool.
// TODO(theryn): Remove when all pools managed with shared pointers.
BufferStatus_t   BufferManager_Generic_c::CreatePool(
    BufferPool_t     *Pool,
    BufferType_t      Type,
    unsigned int      NumberOfBuffers,
    unsigned int      Size,
    void             *MemoryPool[3],
    void             *ArrayOfMemoryBlocks[][3],
    char             *DeviceMemoryPartitionName,
    bool              AllowCross64MbBoundary,
    unsigned int      MemoryAccessType)
{
    CreatePoolInfo_t Info;
    Info.Type = Type;
    Info.NumberOfBuffers = NumberOfBuffers;
    Info.Size = Size;
    // MemoryPool value is the address of an array but as it is
    // typed as an array we need to cast it to copy it.
    Info.MemoryPool = reinterpret_cast<AddressSet_t *>(MemoryPool);
    Info.ArrayOfMemoryBlocks = ArrayOfMemoryBlocks;
    Info.DeviceMemoryPartitionName = DeviceMemoryPartitionName;
    Info.AllowCross64MbBoundary = AllowCross64MbBoundary;
    Info.MemoryAccessType = MemoryAccessType;

    return CreatePool(Pool, Info);
}


// //////////////////////////////////////////////////////////////////////////////////////////////////
//
//      Destroy a pool of buffers
//

// Destroy a pool created with the CreatePool() overload returning a raw
// pointer.
// TODO(theryn): Remove when all pools managed with shared pointers.
void BufferManager_Generic_c::DestroyPool(BufferPool_t    Pool)
{
    delete Pool;
}

// //////////////////////////////////////////////////////////////////////////////////////////////////
//
//      Detach a buffer from all pools
//

void BufferManager_Generic_c::DetachBuffer(Buffer_Generic_t Buffer)
{
    OS_LockMutex(&BufMgrLock);

    DetachBufferLocked(Buffer);

    OS_UnLockMutex(&BufMgrLock);
}

void BufferManager_Generic_c::DetachBufferLocked(Buffer_Generic_t Buffer)
{
    SE_VERBOSE(group_buffer, "0x%p Buffer 0x%p\n", this, Buffer);
    OS_AssertMutexHeld(&BufMgrLock);

    BufferPool_Generic_t PoolSearch;
    for (PoolSearch = ListOfBufferPools;
         (PoolSearch != NULL); PoolSearch = PoolSearch->Next)
    {
        PoolSearch->DetachBuffer(Buffer);
    }
}

// //////////////////////////////////////////////////////////////////////////////////////////////////
//
//      Status dump/reporting
//

static const char *AllocationSources[] =
{
    "NoAllocation",
    "AllocateFromOSMemory",
    "AllocateFromSuppliedBlock",
    "AllocateIndividualSuppliedBlocks",
    "AllocateFromNamedDeviceMemory"
};

void BufferManager_Generic_c::Dump(unsigned int Flags)
{
    unsigned int        i;
    BufferPool_Generic_t    Pool;
    OS_LockMutex(&BufMgrLock);

    for (i = 0; i < TypeDescriptorCount; i++)
        if ((((TypeDescriptors[i].Type & TYPE_TYPE_MASK) == BufferDataTypeBase) && (Flags & DumpBufferTypes) != 0) ||
            (((TypeDescriptors[i].Type & TYPE_TYPE_MASK) == MetaDataTypeBase) && (Flags & DumpMetaDataTypes) != 0))
        {
            SE_DEBUG(group_buffer, "0x%p Buffer Type %04x - '%s' AllocationSource = %s\n", this, TypeDescriptors[i].Type,
                     (TypeDescriptors[i].TypeName == NULL) ? "Unnamed" : TypeDescriptors[i].TypeName,
                     (TypeDescriptors[i].AllocationSource <= AllocateIndividualSuppliedBlocks) ?
                     AllocationSources[TypeDescriptors[i].AllocationSource] : "Invalid");
            SE_DEBUG(group_buffer, "0x%p RequiredAlignment %08x AllocationUnitSize %08x AllocateOnPoolCreation %d "
                     "HasFixedSize %d FixedSize %08x\n",
                     this, TypeDescriptors[i].RequiredAlignment, TypeDescriptors[i].AllocationUnitSize, TypeDescriptors[i].AllocateOnPoolCreation,
                     TypeDescriptors[i].HasFixedSize, TypeDescriptors[i].FixedSize);
        }

    if ((Flags & DumpListPools) != 0)
    {
        SE_DEBUG(group_buffer, "0x%p Dump of Buffer Pools\n", this);

        for (Pool  = ListOfBufferPools;
             Pool != NULL;
             Pool  = Pool->Next)
        {
            Pool->Dump(Flags);
        }
    }

    OS_UnLockMutex(&BufMgrLock);
}

// Dump pool specified by tuneable.
void BufferManager_Generic_c::DoDumpPool()
{
    OS_LockMutex(&BufMgrLock);

    for (BufferPool_Generic_c *Pool = ListOfBufferPools;
         Pool != NULL;
         Pool  = Pool->Next)
    {
        OS_LockMutex(&Pool->PoolLock);
        if (strncmp(Pool->mBufDataDescriptor->TypeName, mDumpPool, sizeof(mDumpPool)) == 0)
        {
            Pool->DumpLocked(DumpAll);
        }
        OS_UnLockMutex(&Pool->PoolLock);
    }

    OS_UnLockMutex(&BufMgrLock);
}

void BufferManager_Generic_c::DumpPool(void *This)
{
    reinterpret_cast<BufferManager_Generic_c *>(This)->DoDumpPool();
}

void BufferManager_Generic_c:: CreateIcsMap()
{
    OS_LockMutex(&BufMgrLock);
    BufferPool_Generic_t PoolSearch;
    for (PoolSearch = ListOfBufferPools;
         (PoolSearch != NULL); PoolSearch = PoolSearch->Next)
    {
        if (PoolSearch->BufferBlock && PoolSearch->BufferBlock->MemoryAllocatorDevice && PoolSearch->BufferBlock->MemoryAllocatorDevice->UnderlyingDevice)
        {
            AllocatorCreateMapEx(PoolSearch->BufferBlock->MemoryAllocatorDevice->UnderlyingDevice);
        }
    }
    OS_UnLockMutex(&BufMgrLock);
}

void BufferManager_Generic_c:: ResetIcsMap()
{
    OS_LockMutex(&BufMgrLock);
    BufferPool_Generic_t PoolSearch;
    for (PoolSearch = ListOfBufferPools;
         (PoolSearch != NULL); PoolSearch = PoolSearch->Next)
    {
        if (PoolSearch->BufferBlock && PoolSearch->BufferBlock->MemoryAllocatorDevice && PoolSearch->BufferBlock->MemoryAllocatorDevice->UnderlyingDevice)
        {
            AllocatorRemoveMapEx(PoolSearch->BufferBlock->MemoryAllocatorDevice->UnderlyingDevice);
        }
    }
    OS_UnLockMutex(&BufMgrLock);
}

void BufferManager_Generic_c::RegisterTuneables()
{
    // Writing name of buffer pool in this file will enable reference count
    // logging for all buffers in this pool.  Calling Buffer_c::Dump() or
    // BufferPool_c::Dump() will then dump to kernel log reference count
    // history.  Must be set before pool creation.
    OS_RegisterStringTuneable("log_ref_count_in_pool", mLogRefCountInPool,
                              sizeof(mLogRefCountInPool), NULL, NULL);

    // Writing name of buffer pool in this file will dump it to the kernel log.
    OS_RegisterStringTuneable("dump_pool", mDumpPool, sizeof(mDumpPool), DumpPool, this);
}

void BufferManager_Generic_c::UnregisterTuneables()
{
    OS_UnregisterTuneable("dump_pool");
    OS_UnregisterTuneable("log_ref_count_in_pool");
}

bool BufferManager_Generic_c::IsReferenceCountLoggingEnabled(const BufferDataDescriptor_t *Descriptor)

{
    return (Descriptor->TypeName != NULL) ? (strncmp(Descriptor->TypeName, mLogRefCountInPool, sizeof(mLogRefCountInPool)) == 0) : false;
}

// Memorize that pools that have been created up to now will be destroyed when
// this manager is destroyed and shall not be reported as leaked by
// CheckForPoolLeakIf().
//
// This function shall be called once only at end of SE initialization.
void BufferManager_Generic_c::MarkEternalPools()
{
    OS_LockMutex(&BufMgrLock);

    SE_ASSERT(mEternalPoolsMarked == false);
    mEternalPoolsMarked = true;

    for (BufferPool_Generic_c *Pool = ListOfBufferPools; Pool != NULL; Pool = Pool->Next)
    {
        Pool->mIsEternal = true;
    }

    OS_UnLockMutex(&BufMgrLock);
}

// Conditionally check for leaked pools.
//
// Log an error for each non-eternal pool still alive if and only if the
// provided callback returns true.
//
// Testing whether to check for leaks inside this function thanks to the
// callback rather than in the caller prevents a race between a thread
// destructing the last pool and another one creating a new pool.  Locking is
// used internally to prevent this.
//
// As the callback is invoked with internal locks held, it shall not call into
// this manager.
void BufferManager_Generic_c::CheckForPoolLeakIf(bool (*MustCheck)()) const
{
    OS_LockMutex(&BufMgrLock);

    if (MustCheck())
    {
        CheckForPoolLeaks();
    }

    OS_UnLockMutex(&BufMgrLock);
}

// Unconditionally check for leaked pools.
//
// This function should be called when all non-eternal pools have been
// destroyed.
//
void BufferManager_Generic_c::CheckForPoolLeaks() const
{
    OS_AssertMutexHeld(&BufMgrLock);

    int Leaked = 0;
    for (BufferPool_Generic_c *Pool = ListOfBufferPools; Pool != NULL; Pool = Pool->Next)
    {
        if (Pool->mIsEternal == false)
        {
            SE_ERROR("pool %s leaked\n", Pool->GetTypeName());
            Leaked++;
            Pool->Dump();
        }
    }

#ifdef SE_FATAL_POOL_LEAK
    // Do not panic by default because when some tests fail they perform
    // incomplete cleanup and leak some pools.  The resulting oops obscures
    // the primary failure cause.
    if (Leaked > 0)
    {
        SE_FATAL("%d pools leaked\n", Leaked);
    }
#endif
}

// Collect statistics for specified pool.
// Check that PotentialPool is a valid pool pointer while interlocking against
// pool destruction to support callers having no control over lifetime of
// specified pool.  This check will be confused by a pool being deleted then
// another pool being created at the same address but that's okay as we only
// collect statistics.
void BufferManager_Generic_c::GetPoolUsage(BufferPool_t   PotentialPool,
                                           unsigned int  *BuffersInPool,
                                           unsigned int  *BuffersWithNonZeroReferenceCount,
                                           unsigned int  *MemoryInPool,
                                           unsigned int  *MemoryAllocated,
                                           unsigned int  *MemoryInUse,
                                           unsigned int  *LargestFreeMemoryBlock,
                                           unsigned int  *MaxBuffersWithNonZeroReferenceCount,
                                           unsigned int  *MaxMemoryInUse)
{
    OS_LockMutex(&BufMgrLock);

    // TODO(theryn): Iterating the cache-adverse pool list is required to ensure
    // pool has not been destructed but potentially costly if statistics are
    // sampled at a high rate .  Potential optimizations are: reference-counted
    // pools (when available) or using a more performant data structure (e.g.
    // sorted cache-friendly vector + binary search).
    for (BufferPool_Generic_c *Pool = ListOfBufferPools; Pool != NULL; Pool = Pool->Next)
    {
        if (Pool == PotentialPool)
        {
            Pool->GetPoolUsage(BuffersInPool,
                               BuffersWithNonZeroReferenceCount,
                               MemoryInPool,
                               MemoryAllocated,
                               MemoryInUse,
                               LargestFreeMemoryBlock,
                               MaxBuffersWithNonZeroReferenceCount,
                               MaxMemoryInUse);
            OS_UnLockMutex(&BufMgrLock);
            return;
        }
    }

    OS_UnLockMutex(&BufMgrLock);

    if (BuffersInPool != NULL) { *BuffersInPool = 0; }
    if (BuffersWithNonZeroReferenceCount != NULL) { *BuffersWithNonZeroReferenceCount = 0; }
    if (MemoryInPool != NULL) { *MemoryInPool = 0; }
    if (MemoryAllocated != NULL) { *MemoryAllocated = 0; }
    if (MemoryInUse != NULL) { *MemoryInUse = 0; }
    if (LargestFreeMemoryBlock != NULL) { *LargestFreeMemoryBlock = 0; }
    if (MaxBuffersWithNonZeroReferenceCount != NULL) { *MaxBuffersWithNonZeroReferenceCount = 0; }
    if (MaxMemoryInUse != NULL) { *MaxMemoryInUse = 0; }
}

// Called by pool during destruction.
void BufferManager_Generic_c::RemovePool(BufferPool_Generic_t Pool)
{
    OS_AssertMutexHeld(&BufMgrLock);

    BufferPool_Generic_t     LocalPool = (BufferPool_Generic_t)Pool;
    BufferPool_Generic_t    *LocationOfPointer;

    SE_DEBUG(group_buffer, "0x%p Destroy pool 0x%p\n", this, Pool);

    //
    // First find and remove the pool from our list
    //

    for (LocationOfPointer    = &ListOfBufferPools;
         *LocationOfPointer  != NULL;
         LocationOfPointer    = &((*LocationOfPointer)->Next))
    {
        if (*LocationOfPointer == LocalPool)
        {
            break;
        }
    }

    if (*LocationOfPointer == NULL)
    {
        SE_ERROR("0x%p Pool 0x%p not found\n", this, LocalPool);
        return;
    }

    *LocationOfPointer  = LocalPool->Next;
}
