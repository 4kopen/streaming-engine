/************************************************************************
Copyright (C) 2003-2014 STMicroelectronics. All Rights Reserved.

This file is part of the Streaming Engine.

Streaming Engine is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License version 2 as
published by the Free Software Foundation.

Streaming Engine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with Streaming Engine; see the file COPYING.  If not, write to the Free
Software Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307,
USA.

The Streaming Engine Library may alternatively be licensed under a
proprietary license from ST.
************************************************************************/

#ifndef H_BUFFER_MANAGER_GENERIC
#define H_BUFFER_MANAGER_GENERIC

// Singleton that keeps track of all BufferPool_Generic_c objects in the system.
//
// The buffer manager is primarily a pool factory.  It also provides methods
// operating on all pools (e.g. dumping all pools, detaching a buffer from all
// pools...).
//
// Thread-safe.
class BufferManager_Generic_c : public BufferManager_c
{
public:
    BufferManager_Generic_c();
    ~BufferManager_Generic_c();

    //
    // Add to the defined types
    //

    BufferStatus_t  CreateBufferDataType(BufferDataDescriptor_t  *Descriptor,
                                         BufferType_t            *Type);

    BufferStatus_t  FindBufferDataType(const char          *TypeName,
                                       BufferType_t        *Type);

    BufferStatus_t  GetDescriptor(BufferType_t              Type,
                                  BufferPredefinedType_t    RequiredKind,
                                  BufferDataDescriptor_t  **Descriptor);

    //
    // Create/destroy a pool of buffers overloaded creation function
    //

    BufferStatus_t   CreatePool(BufferPool_t  *Pool,
                                BufferType_t   Type,
                                unsigned int   NumberOfBuffers           = UNRESTRICTED_NUMBER_OF_BUFFERS,
                                unsigned int   Size                      = UNSPECIFIED_SIZE,
                                void          *MemoryPool[3]             = NULL,
                                void          *ArrayOfMemoryBlocks[][3]  = NULL,
                                char          *DeviceMemoryPartitionName = NULL,
                                bool           AllowCross64MbBoundary    = true,
                                unsigned int   MemoryAccessType          = MEMORY_DEFAULT_ACCESS);

    BufferStatus_t   CreatePool(BufferPool_t *Pool, const CreatePoolInfo_t &Info);
    BufferStatus_t   CreatePool(SharedPtr_c<BufferPool_c>  *Pool, const CreatePoolInfo_t &Info);

    void             DestroyPool(BufferPool_t         Pool);

    void             DetachBuffer(Buffer_Generic_t Buffer);

    void             ResetIcsMap();

    void             CreateIcsMap();

    void             GetPoolUsage(BufferPool_t   Pool,
                                  unsigned int  *BuffersInPool,
                                  unsigned int  *BuffersWithNonZeroReferenceCount,
                                  unsigned int  *MemoryInPool,
                                  unsigned int  *MemoryAllocated,
                                  unsigned int  *MemoryInUse,
                                  unsigned int  *LargestFreeMemoryBlock,
                                  unsigned int  *MaxBuffersWithNonZeroReferenceCount,
                                  unsigned int  *MaxMemoryInUse);

    void             RegisterTuneables();
    void             UnregisterTuneables();

    //
    // Status dump/reporting
    //

    void            MarkEternalPools();
    void            Dump(unsigned int    Flags = DumpAll);
    void            CheckForPoolLeakIf(bool (*MustCheck)()) const;

    bool IsReferenceCountLoggingEnabled(const BufferDataDescriptor_t *Descriptor);

    friend class BufferPool_Generic_c;
    friend class Buffer_Generic_c;

private:
    // Protect all members of this class unless otherwise specified.
    // Also taken for preventing pool destruction.
    // Ordering: this lock < BufferPool_Generic_c::PoolLock.
    mutable OS_Mutex_t      BufMgrLock;

    unsigned int            TypeDescriptorCount;
    BufferDataDescriptor_t  TypeDescriptors[MAX_BUFFER_DATA_TYPES];

    BufferPool_Generic_t    ListOfBufferPools;

    bool mEternalPoolsMarked;

    // Tuneables.
    // TODO(theryn): There is a race between this class and debugfs when
    // accessing these members.  This is a minor race because users are expected
    // to write to debugfs before running use cases.
    char mDumpPool[64];
    char mLogRefCountInPool[64];

    static void DumpPool(void *);
    void DoDumpPool();
    void CheckForPoolLeaks() const;
    void RemovePool(BufferPool_Generic_t Pool);

    void DetachBufferLocked(Buffer_Generic_t Buffer);

    DISALLOW_COPY_AND_ASSIGN(BufferManager_Generic_c);
};

#endif
