/************************************************************************
Copyright (C) 2003-2014 STMicroelectronics. All Rights Reserved.

This file is part of the Streaming Engine.

Streaming Engine is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License version 2 as
published by the Free Software Foundation.

Streaming Engine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with Streaming Engine; see the file COPYING.  If not, write to the Free
Software Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307,
USA.

The Streaming Engine Library may alternatively be licensed under a
proprietary license from ST.
************************************************************************/

#ifndef H_BUFFER_POOL_GENERIC
#define H_BUFFER_POOL_GENERIC

#include "stack_trace_store.h"

struct BlockDescriptor_s
{
    BlockDescriptor_t         Next;
    BufferDataDescriptor_t   *Descriptor;
    bool                      AttachedToPool;

    unsigned int              Size;
    void                     *Address[3];  // 3 address types (cached uncached physical) - only cached for meta data
    // TODO(pht) move to 2: using only cached & physical

    allocator_device_t        MemoryAllocatorDevice;
    unsigned int              PoolAllocatedOffset;

    // ctors
    BlockDescriptor_s(BlockDescriptor_t next,
                      BufferDataDescriptor_t *descriptor,
                      bool attachedtopool,
                      unsigned int size,
                      allocator_device_t memoryallocatordevice)
        : Next(next)
        , Descriptor(descriptor)
        , AttachedToPool(attachedtopool)
        , Size(size)
        , Address()
        , MemoryAllocatorDevice(memoryallocatordevice)
        , PoolAllocatedOffset(0)
    {}
private:
    BlockDescriptor_s();
};

// A buffer pool manages a set of Buffer_Generic_c objects allocated from
// a common type of memory selected at pool construction time (BPA2 area
// allocated by BufferPool_Generic_c, externally provided BPA2 area, kernel
// heap...).
//
// A pool supports various pre-allocation strategies: buffer object and
// underlying memory payload allocated at GetBuffer() time, pre-allocated buffer
// object and payload allocated at GetBuffer() time, buffer payload both
// pre-allocated.
//
// A pool lifetime must exceeds that of its buffers and pools and buffers are
// shared between threads.  So pool should ideally be reference-counted to avoid
// life-time issues.  Some pools are reference-counted thanks to SharedPtr_c.
// Other pools use ad-hoc schemes (object(s) owning pool, buffers and threads
// coordinate together to ensure pool is destructed when all buffers are gone
// and threads do not access pool anymore).
//
// Thread-safe.
class BufferPool_Generic_c : public BufferPool_c
{
public:
    static BufferPool_Generic_c *New(BufferManager_Generic_t   Manager,
                                     BufferDataDescriptor_t   *Descriptor,
                                     const CreatePoolInfo_t   &Info);
    ~BufferPool_Generic_c();

    //
    // Global operations on all pool members
    //
    BufferStatus_t   AttachMetaData(MetaDataType_t  Type,
                                    unsigned int    Size                      = UNSPECIFIED_SIZE,
                                    void           *MemoryPool                = NULL,
                                    void           *ArrayOfMemoryBlocks[]     = NULL,
                                    char           *DeviceMemoryPartitionName = NULL);
    void             DetachMetaData(MetaDataType_t  Type);

    //
    // Get/Release a buffer - overloaded get
    //

    /**
     * @param Buffer
     * @param OwnerIdentifier
     * @param RequiredSize
     * @param NonBlocking
     * @param RequiredSizeIsLowerBound
     * @param TimeOut in ms, 0 means blocking for ever: use
     *                NonBlocking for non-blocking
     *
     * @return BufferStatus_t
     */
    BufferStatus_t   GetBuffer(Buffer_t      *Buffer,
                               unsigned int   OwnerIdentifier          = UNSPECIFIED_OWNER,
                               unsigned int   RequiredSize             = UNSPECIFIED_SIZE,
                               bool           NonBlocking              = false,
                               bool           RequiredSizeIsLowerBound = false,
                               uint32_t       TimeOut                  = 0,
                               bool           QuietTraceMode           = false);

    void             RejectGetBuffers();
    void             AcceptGetBuffers();

    void             DetachBuffer(Buffer_Generic_t Buffer);

    void             SetMaximumNumberOfUseBuffers(unsigned int MaxNumberOfUseBuffers)
    {
        mMaxNumberOfUseBuffers = MaxNumberOfUseBuffers;
    };

    //
    // Usage/query/debug methods
    //

    void            GetType(BufferType_t *Type);

    const char     *GetTypeName() const
    {
        return (mBufDataDescriptor->TypeName == NULL) ? "Unnamed" : mBufDataDescriptor->TypeName;
    }

    void            GetPoolUsage(unsigned int   *BuffersInPool,
                                 unsigned int   *BuffersWithNonZeroReferenceCount       = NULL,
                                 unsigned int   *MemoryInPool                           = NULL,
                                 unsigned int   *MemoryAllocated                        = NULL,
                                 unsigned int   *MemoryInUse                            = NULL,
                                 unsigned int   *LargestFreeMemoryBlock                 = NULL,
                                 unsigned int   *MaxBuffersWithNonZeroReferenceCount    = NULL,
                                 unsigned int   *MaxMemoryInUse                         = NULL);

    BufferStatus_t  GetAllUsedBuffers(unsigned int   ArraySize,
                                      Buffer_t      *ArrayOfBuffers,
                                      unsigned int   OwnerIdentifier);

    void            BufferDumpViaRelay(unsigned int id, unsigned int source, Buffer_Generic_t Buffer);

    //
    // Status dump/reporting
    //
    void         Dump(unsigned int    Flags = DumpAll);

    friend class BufferManager_Generic_c;
    friend class Buffer_Generic_c;

private:
    // AllocateMemoryBlock() behaviour on out-of-memory.
    enum OomPolicy_t
    {
        // Block until memory available.
        BlockOnOom,

        // Return an error.
        FailOnOom,

        // Retry allocation a few times before returning an error.
        // Should be used when transient memory shortage possible,
        // for example during zap where memory pools are destructed
        // then recreated and asynchronous destruction is possible
        // because pools are managed with shared pointers.
        RetryOnOom
    };

    BufferManager_Generic_t mManager;

    // Protected by BufferManager_Generic_c::BufMgrLock.
    BufferPool_Generic_t    Next;

    // Protected by BufferManager_Generic_c::BufMgrLock.
    bool                    mIsEternal;

    // Protect members from this class except otherwise specified and some
    // members of Buffer_Generic_c objects carved from this pool.  See
    // per-member comments for details.  This lock must not be held when waiting
    // for memory during out-of-memory or a deadlock occurs with threads
    // releasing buffers.  The lock ordering is this lock before
    // Buffer_Generic_c::BufferLock.
    OS_Mutex_t              PoolLock;

    BufferDataDescriptor_t *mBufDataDescriptor;
    unsigned int            mNumberOfBuffers;
    unsigned int            mMaxNumberOfUseBuffers;
    unsigned int            CountOfBuffers;       // In effect when NumberOfBuffers == NOT_SPECIFIED
    unsigned int            Size;

    // All buffers in this pool.  Buffers in this list may have ReferenceCount
    // == 0.  Protected by this->PoolLock.
    Buffer_Generic_t        ListOfBuffers;

    Ring_c                 *FreeBuffer;
    void                   *mMemoryPool[3];        // If memory pool is specified, its three addresses
    Allocator_c            *MemoryPoolAllocator;
    allocator_device_t      MemoryPoolAllocatorDevice;
    char                    MemoryPartitionName[ALLOCATOR_MAX_PARTITION_NAME_SIZE];
    unsigned int            mMemoryAccessType;

    BlockDescriptor_t       BufferBlock;
    BlockDescriptor_t       ListOfMetaData;

    bool                    mRejectGetBuffers;       // Flag to reject all subsequent GetBuffer
    OS_Event_t              BufferReleaseSignal;

    unsigned int            CountOfReferencedBuffers; // Statistics (held rather than re-calculated every request)
    unsigned int            MaxCountOfReferencedBuffers;
    unsigned int            TotalAllocatedMemory;
    unsigned int            TotalUsedMemory;
    unsigned int            MaxTotalUsedMemory;
    StackTraceStore_c       mStackTraceStore;

    bool                    mAllowCross64MbBoundary;

    // Self pointer used for creating strong shared pointers from buffers to
    // this pool.  Valid only when this pool is managed by shared pointers.
    WeakPtr_c<BufferPool_c> mSelfWeakPtr;

    void SetSelfWeakPtr(const SharedPtr_c<BufferPool_c> &Ptr) { mSelfWeakPtr = Ptr; }
    bool IsManagedWithSharedPtr() const { return mSelfWeakPtr.Expired() == false; }

    // Functions

    BufferPool_Generic_c(BufferManager_Generic_t   Manager,
                         BufferDataDescriptor_t   *Descriptor,
                         const CreatePoolInfo_t   &Info);
    BufferStatus_t FinalizeInit(void  *MemoryPool[3],
                                void  *ArrayOfMemoryBlocks[][3]);

    void            InternalDetachMetaData(MetaDataType_t  Type, bool CalledUnderPoolLock);

    BufferStatus_t  CheckMemoryParameters(BufferDataDescriptor_t   *Descriptor,
                                          bool                      ArrayAllocate,
                                          unsigned int              Size,
                                          void                     *MemoryPool,
                                          void                     *ArrayOfMemoryBlocks,
                                          char                     *MemoryPartitionName,
                                          const char               *Caller,
                                          unsigned int             *ItemSize = NULL);

    BufferStatus_t  AllocateMemoryBlock(BlockDescriptor_t  Block,
                                        bool               ArrayAllocate,
                                        Allocator_c       *PoolAllocator,
                                        void              *MemoryPool,
                                        char              *MemoryPartitionName,
                                        const char        *Caller,
                                        bool               RequiredSizeIsLowerBound = false,
                                        unsigned int       MemoryAccessType = MEMORY_DEFAULT_ACCESS,
                                        OomPolicy_t        OomPolicy = BlockOnOom,
                                        bool               QuietTraceMode = false,
                                        unsigned int       Alignment = MEMORY_DEFAULT_PAGEALIGN);

    void            DeAllocateMemoryBlock(BlockDescriptor_t   Block);

    BufferStatus_t  DoAllocateMemoryBlock(BlockDescriptor_t  Block,
                                          bool               ArrayAllocate,
                                          Allocator_c       *PoolAllocator,
                                          void              *MemoryPool,
                                          char              *MemoryPartitionName,
                                          const char        *Caller,
                                          bool               RequiredSizeIsLowerBound,
                                          unsigned int       MemoryAccessType,
                                          bool               NonBlocking,
                                          bool               QuietTraceMode,
                                          unsigned int       Alignment);

    BufferStatus_t  ShrinkMemoryBlock(BlockDescriptor_t   Block,
                                      unsigned int        NewSize);

    void DumpLocked(unsigned int Flags);

    void ReleaseBuffer(Buffer_t Buffer);
    BufferStatus_t GetBufferDynamicNumberLocked(Buffer_Generic_t *LocalBuffer);
    BufferStatus_t GetBufferFixedNumberLocked(bool NonBlocking, uint32_t TimeOut, bool QuietTraceMode, Buffer_Generic_t *LocalBuffer);

    DISALLOW_COPY_AND_ASSIGN(BufferPool_Generic_c);
};

#endif
