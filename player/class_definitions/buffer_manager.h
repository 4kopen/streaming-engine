/************************************************************************
Copyright (C) 2003-2014 STMicroelectronics. All Rights Reserved.

This file is part of the Streaming Engine.

Streaming Engine is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License version 2 as
published by the Free Software Foundation.

Streaming Engine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with Streaming Engine; see the file COPYING.  If not, write to the Free
Software Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307,
USA.

The Streaming Engine Library may alternatively be licensed under a
proprietary license from ST.
************************************************************************/

#ifndef H_BUFFER_MANAGER
#define H_BUFFER_MANAGER

#include "buffer.h"
#include "allocinline.h"
#include <shared_ptr.h>

// BufferManager_c::CreatePool() argument.
// Optional arguments are set to default values.
struct CreatePoolInfo_t
{
    BufferType_t      Type;
    unsigned int      NumberOfBuffers;
    unsigned int      Size;

    // Pointer to single tuple storing address of already-allocated memory
    // block from which buffers will be carved.  NULL if no such memory.
    AddressSet_t     *MemoryPool;

    // Pointer to array of tuples storing addresses of already-allocated
    // memory blocks, one per buffer.  NULL if no such memory.
    AddressSet_t     *ArrayOfMemoryBlocks;

    char             *DeviceMemoryPartitionName;
    bool              AllowCross64MbBoundary;
    unsigned int      MemoryAccessType;

    // If not NULL, transfer ownership of specified memory device to the pool
    // meaning that the pool will close the device and thus free the underlying
    // memory on destruction.
    allocator_device_t MemoryDevice;

    CreatePoolInfo_t();
};

typedef enum
{
    NoAllocation        = 0,
    AllocateFromOSMemory,
    AllocateFromSuppliedBlock,
    AllocateIndividualSuppliedBlocks,
    AllocateFromNamedDeviceMemory       // Allocator with memory partition name
} BufferAllocationSource_t;

// ---------------------------------------------------------------------
//
// Descriptor record, for pre-defined types, and user defined type
//

typedef enum
{
    BufferDataTypeBase      = 0x0000,
    MetaDataTypeBase        = 0x8000,
} BufferPredefinedType_t;

#define BUFFER_DATADESC_MAX_NAME_LENGTH 512

typedef struct BufferDataDescriptor_s
{
    const char          *TypeName;
    BufferType_t         Type;

    BufferAllocationSource_t AllocationSource;
    unsigned int             RequiredAlignment;
    unsigned int             AllocationUnitSize;

    bool             HasFixedSize;
    bool             AllocateOnPoolCreation;
    unsigned int     FixedSize;
} BufferDataDescriptor_t;

class BufferManager_c : public BaseComponentClass_c
{
public:
    virtual BufferStatus_t  CreateBufferDataType(
        BufferDataDescriptor_t *Descriptor,
        BufferType_t           *Type) = 0;

    virtual BufferStatus_t  FindBufferDataType(
        const char    *TypeName,
        BufferType_t  *Type) = 0;

    virtual BufferStatus_t  GetDescriptor(BufferType_t              Type,
                                          BufferPredefinedType_t    RequiredKind,
                                          BufferDataDescriptor_t  **Descriptor) = 0;

    /// @deprecated: Use CreatePoolInfo_t overloads.
    /// TODO(theryn): Convert all callers to new overloads and remove.
    virtual BufferStatus_t  CreatePool(BufferPool_t  *Pool,
                                       BufferType_t   Type,
                                       unsigned int   NumberOfBuffers           = UNRESTRICTED_NUMBER_OF_BUFFERS,
                                       unsigned int   Size                      = UNSPECIFIED_SIZE,
                                       void          *MemoryPool[3]             = NULL,
                                       void          *ArrayOfMemoryBlocks[][3]  = NULL,
                                       char          *DeviceMemoryPartitionName = NULL,
                                       bool           AllowCross64MbBoundary    = true,
                                       unsigned int   MemoryAccessType          = MEMORY_DEFAULT_ACCESS) = 0;

    virtual BufferStatus_t  CreatePool(BufferPool_t *Pool, const CreatePoolInfo_t &Info) = 0;
    virtual BufferStatus_t  CreatePool(SharedPtr_c<BufferPool_c> *Pool, const CreatePoolInfo_t &Info) = 0;

    virtual void            DestroyPool(BufferPool_t  Pool) = 0;

    virtual void            ResetIcsMap() = 0;

    virtual void            CreateIcsMap() = 0;

    virtual void            GetPoolUsage(BufferPool_t   Pool,
                                         unsigned int  *BuffersInPool,
                                         unsigned int  *BuffersWithNonZeroReferenceCount,
                                         unsigned int  *MemoryInPool,
                                         unsigned int  *MemoryAllocated,
                                         unsigned int  *MemoryInUse,
                                         unsigned int  *LargestFreeMemoryBlock,
                                         unsigned int  *MaxBuffersWithNonZeroReferenceCount,
                                         unsigned int  *MaxMemoryInUse) = 0;

    //
    // Tuneables (un)registration
    //

    virtual void            RegisterTuneables() = 0;

    virtual void            UnregisterTuneables() = 0;

    //
    // Status dump/reporting
    //

    virtual void            MarkEternalPools() = 0;
    virtual void            Dump(unsigned int    Flags = DumpAll) = 0;
    virtual void            CheckForPoolLeakIf(bool (*MustCheck)()) const = 0;
};

#endif

