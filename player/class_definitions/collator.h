/************************************************************************
Copyright (C) 2003-2014 STMicroelectronics. All Rights Reserved.

This file is part of the Streaming Engine.

Streaming Engine is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License version 2 as
published by the Free Software Foundation.

Streaming Engine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with Streaming Engine; see the file COPYING.  If not, write to the Free
Software Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307,
USA.

The Streaming Engine Library may alternatively be licensed under a
proprietary license from ST.
************************************************************************/

#ifndef H_COLLATOR
#define H_COLLATOR

#include "marker_frames.h"
#include "player.h"
#include "data_block.h"

// Strings that will be transmitted to user space by strelay tool
// dont modify without modifying test references!
#define ST_RELAY_CONTROL_DISCONTINUITY_TEXT "Found discontinuity \n"
#define ST_RELAY_CONTROL_DISCONTINUITY_TEXT_LENGTH  (strnlen(ST_RELAY_CONTROL_DISCONTINUITY_TEXT, 128)+1)
#define ST_RELAY_CONTROL_SPLICING_TEXT "Found splicing \n"
#define ST_RELAY_CONTROL_SPLICING_TEXT_LENGTH  (strnlen(ST_RELAY_CONTROL_SPLICING_TEXT, 128)+1)

#define TUNEABLE_NAME_COLLATOR_OFFLOAD  "force_collator_offload"

enum
{
    CollatorNoError         = PlayerNoError,
    CollatorError           = PlayerError,

    CollatorWouldBlock      = BASE_COLLATOR,
    CollatorBufferOverflow,
    CollatorBufferUnderflow, ///< Primarily for internal use
    CollatorUnwindStack, ///< Reported to cause execution to unwind (to avoid recursion)
    CollatorBadControlType, // Unrecognized control data type
    CollatorNotAControlData, // SC detected doesn't correspond to a control data
    CollatorInputNotConnected,
    CollatorInputAlreadyConnected
};

typedef enum
{
    CollatorAudioElementrySyncLostCount,
    CollatorAudioPesSyncLostCount
} CollatorStreamStatistics_t;

typedef PlayerStatus_t  CollatorStatus_t;

typedef enum
{
    CollatorNoOffload   = 0,
    CollatorOffloadHost = 1,
    CollatorOffloadGplx = 2,
} CollatorOffload_t;

class Collator_c
{
public:
    virtual ~Collator_c() {}

    virtual PlayerStatus_t     Halt() = 0;

    virtual PlayerStatus_t     RegisterPlayer(Player_t             Player,
                                              PlayerPlayback_t     Playback,
                                              PlayerStream_t       Stream,
                                              OutputCoordinator_t  mOutputCoordinator) = 0;

    virtual PlayerStatus_t     SpecifySignalledEvents(PlayerEventMask_t        EventMask,
                                                      void                    *EventUserData) = 0;

    virtual CollatorStatus_t   Connect(Port_c *Port) = 0;

    virtual CollatorStatus_t   InputJump(int discontinuity) = 0;

    virtual CollatorStatus_t   Input(PlayerInputDescriptor_t  *Input,
                                     const DataBlock_t        *BlockList,
                                     int                       BlockCount,
                                     int                      *NbBlocksConsumed) = 0;

    static void RegisterTuneable()
    {
        OS_RegisterTuneable(TUNEABLE_NAME_COLLATOR_OFFLOAD, (unsigned int *)&gCollatorOffloadTuneable);
    }

    static void UnregisterTuneable()
    {
        OS_UnregisterTuneable(TUNEABLE_NAME_COLLATOR_OFFLOAD);
    }

    static CollatorOffload_t GetCollatorOffloadTuneable() { return gCollatorOffloadTuneable; }

    virtual void WakeUp() = 0;

    virtual bool TestComponentState(EngineComponentState_t s) = 0;
    virtual void SetComponentState(EngineComponentState_t s) = 0;

    virtual CollatorStatus_t ConnectInput() = 0;
    virtual CollatorStatus_t DisconnectInput() = 0;

private:
    static CollatorOffload_t gCollatorOffloadTuneable;
};

class CollatorSinkInterface_c
{
public:
    virtual ~CollatorSinkInterface_c() {};

    // Return a new output buffer and its metadata
    virtual CollatorStatus_t GetNewBuffer(
        unsigned char **bufferBase, unsigned int *bufferSize,
        StartCodeList_t **startCodeList, unsigned int *startcodeBufferSize,
        unsigned char **headerBufferBase, unsigned int *headerBufferSize) = 0;

    // Push a collated frame and its meta-data to the next pipeline stage.
    virtual CollatorStatus_t InsertInOutputPort(
        unsigned int bufferSize, unsigned int headerSize,
        const CodedFrameParameters_t &CodedFrameParameters) = 0;

    virtual CollatorStatus_t InsertMarkerFrameToOutputPort(const MarkerFrame_t &MarkerFrame) = 0;

    // Get a Stream-Specific Policy
    virtual int GetStreamPolicy(PlayerPolicy_t Policy) = 0;

    // Get the Playback's Low-Power state
    virtual bool PlaybackIsInLowPowerState() = 0;

    virtual void AttachBuffer(const void *data) = 0;

    virtual void UpdateStreamStatistics(CollatorStreamStatistics_t statistic, int value) = 0;
};

class CollatorProcessorInterface_c
{
public:
    virtual ~CollatorProcessorInterface_c() {}

    virtual CollatorStatus_t SetSink(CollatorSinkInterface_c *sink, unsigned int maximumCodedFrameSize) = 0;

    virtual PlayerStatus_t     Halt() = 0;

    virtual PlayerStatus_t     SpecifySignalledEvents(PlayerEventMask_t        EventMask,
                                                      void                    *EventUserData) = 0;

    virtual CollatorStatus_t   InputJump(int discontinuity) = 0;

    virtual CollatorStatus_t   Input(const PlayerInputDescriptor_t  *Input,
                                     unsigned int                    DataLength,
                                     const void                     *Data) = 0;

    virtual void GetCapabilities(bool            isAboveHdStream,
                                 bool           *generatesStartCodeList,
                                 unsigned int   *maxStartCodes,
                                 bool           *requireFrameHeaders,
                                 unsigned int   *maxHeaderSize) = 0;
};

// ---------------------------------------------------------------------
//
// Documentation
//

/*! \class Collator_c
\brief Responsible for taking demultiplexed input data and creating frames suitable for parsing/decoding.

The collator class is responsible for taking as input blocks of demultiplexed data and creating frames of output suitable for parsing/decoding. This is a list of its entrypoints, and a partial list of the calls it makes, and the data structures it accesses, these are in addition to the standard component class entrypoints, and the complete list of support entrypoints in the Player class.

The partial list of entrypoints used by this class:

- Empty list.

The partial list of meta data types used by this class:

- There are no input buffers, but as part of the input parameter list, the following is attached:
  - <b>PlayerInputDescriptor</b> Describes the input block.

- Attached to output buffers :-
  - <b>CodedFrameParameters</b>, describes the coded frame output.
  - <b>StartCodeList</b>, optional output attachment for those collators generating a start code scan.
*/

/*! \fn CollatorStatus_t Collator_c::Connect(Port_c *Port)
\brief Connect to a Port_c on which collated frames are to be placed.

\param Port Pointer to a Port_c instance.

\return Collator status code, CollatorNoError indicates success.
*/

/*! \fn CollatorStatus_t Collator_c::InputJump(int discontinuity)
\brief Demark discontinuous input data.

For more information on discontinuous streams see <b>InputJump</b> on \ref input.

\param discontinuity  discontinuity mask

\return Collator status code, CollatorNoError indicates success.
*/

/*! \fn CollatorStatus_t Collator_c::Input(PlayerInputDescriptor_t *Input, unsigned int DataLength, void *Data, bool NonBlocking, unsigned int *DataLengthRemaining )
\brief Accept demultiplexed data for collation.

<b>TODO: This method is not adequately documented.</b>

\param Input A pointer to an input descriptor.
\param DataLength Length of the data block (in bytes).
\param Data A pointer to a block of memory containing the input data.
\param NonBlocking flag indicating the call should not block. This is not supported in the non-reversing collator.
\param DataLengthRemaining returned value indicating the amount of data unconsumed when the function returns CollatorWouldBlock.

\return Collator status code, CollatorNoError indicates success.
*/

/*! \fn CollatorStatus_t Collator_c::DiscardAccumulatedData()
\brief Discard accumulated data that has not been passed to the output ring.

This function indicates that the current input data that has been accumulated, but not passed onto the output ring, is to be discarded.

\return Collator status code, CollatorNoError indicates success.
*/
#endif
