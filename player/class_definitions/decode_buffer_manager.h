/************************************************************************
Copyright (C) 2003-2014 STMicroelectronics. All Rights Reserved.

This file is part of the Streaming Engine.

Streaming Engine is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License version 2 as
published by the Free Software Foundation.

Streaming Engine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with Streaming Engine; see the file COPYING.  If not, write to the Free
Software Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307,
USA.

The Streaming Engine Library may alternatively be licensed under a
proprietary license from ST.
************************************************************************/

#ifndef H_DECODE_BUFFER_MANAGER
#define H_DECODE_BUFFER_MANAGER

#include "player.h"
#include "allocinline.h"

enum
{
    DecodeBufferManagerNoError              = PlayerNoError,
    DecodeBufferManagerError                = PlayerError,

    DecodeBufferManagerFailedToAllocateComponents   = BASE_DECODE_BUFFER_MANAGER,
};

typedef PlayerStatus_t  DecodeBufferManagerStatus_t;

#if defined CONFIG_ARM
#define MAX_DECODE_BUFFERS      64
#else
#define MAX_DECODE_BUFFERS      32
#endif
#define MAX_VIDEO_DECODE_BUFFERS 32

#define MAX_BUFFER_DIMENSIONS   5
#define MAX_BUFFER_COMPONENTS   5

#define MAX_PREPROC_POOL    5

typedef enum
{
    PrimaryManifestationComponent   = 0,
    DecimatedManifestationComponent,
    VideoDecodeCopy,
    VideoMacroblockStructure,
    VideoPostProcessControl,
    AdditionalMemoryBlock,
    UndefinedComponent
} DecodeBufferComponentType_t;

#define NUMBER_OF_COMPONENTS    UndefinedComponent

enum DecodeBufferComponentElementMask_e
{
    PrimaryManifestationElement             = 1,
    DecimatedManifestationElement           = 2,
    VideoDecodeCopyElement                  = 4,
    VideoMacroblockStructureElement         = 8,
    VideoPostProcessControlElement          = 16,
    AdditionalMemoryBlockElement            = 32,
    VideoMacroblockStructureForNotReferenceElement = 64,
    VideoMacroblockPoolStructureElement     = 128
};

typedef unsigned int DecodeBufferComponentElementMask_t;

enum DecodeBufferUsage_e
{
    NotUsed          = 0,
    ForDecode        = 1,
    ForManifestation = 2,
    ForReference     = 4
};
typedef unsigned int DecodeBufferUsage_t;

typedef enum
{
    FormatUnknown                       = 0,
    FormatMarkerFrame,
    FormatLinear,
    FormatPerMacroBlockLinear,
    FormatAudio,
    FormatVideo420_HadesMacroBlock, // SE internal
    FormatVideo420_MacroBlock,
    FormatVideo420_PairedMacroBlock,
//    FormatVideo420_Raster_Y_CbCr,
//    FormatVideo420_Raster_Y_Cb_Cr,
    FormatVideo422_Raster,
    FormatVideo420_Planar,
    FormatVideo420_PlanarAligned,
    FormatVideo422_Planar,
    FormatVideo8888_ARGB,
    FormatVideo888_RGB,
    FormatVideo565_RGB,
    FormatVideo422_YUYV,
    FormatVideo420_Raster2B,
    FormatVideo422_Raster2B,
    FormatVideo444_Raster,
    // 10 bits formats
    FormatVideo420_Raster2B_10B
} BufferFormat_t;

// ---------------------------------------------------------------------
// Structure to define the components that make up a decode buffer
//

typedef struct DecodeBufferComponentDescriptor_s
{
    DecodeBufferComponentType_t  Type;
    DecodeBufferUsage_t          Usage;
    bool                     DirectPoolUsage;
    BufferFormat_t           DataType;
    AddressType_t            DefaultAddressMode;
    BufferDataDescriptor_t  *DataDescriptor;

    unsigned int             ComponentBorders[MAX_BUFFER_COMPONENTS];
} DecodeBufferComponentDescriptor_t;

// ---------------------------------------------------------------------
// Structure to define a buffer request, filled in, holds most of the
// useful info about a decode buffer.
//

typedef struct DecodeBufferRequest_s
{
    //
    // Fields filled in for the request
    //

    bool         mIsMarkerFrame;
    bool         NonBlockingInCaseOfFailure;

    unsigned int DimensionCount;
    unsigned int Dimension[MAX_BUFFER_DIMENSIONS];
    unsigned int DecimationFactors[MAX_BUFFER_DIMENSIONS];

    bool         ReferenceFrame;
    unsigned int MacroblockSize;
    unsigned int PerMacroblockMacroblockStructureSize;
    unsigned int PerMacroblockMacroblockStructureFifoSize;
    unsigned int AdditionalMemoryBlockSize;
    unsigned int ManifestationBufferCount;
    unsigned int ReferenceBufferCount;
    unsigned int PixelDepth;
    bool         AlignmentNeededforInterlaced;
} DecodeBufferRequest_t;

class DecodeBufferManager_c : public BaseComponentClass_c
{
public:
    //
    // Function to obtain the buffer pool, in order to attach meta data to the whole pool
    //
    virtual BufferPool_t GetDecodeBufferPool() = 0;

    //
    // Function that monitors the decode buffer pool usage and releases all the component pools when no more used.
    // This intends to be used on a play_stream switch.
    //
    virtual void                          WaitAndReleaseAllComponentPools() = 0;

    //
    // Function to support entry into stream switch mode.
    // It intends to be used when the decode buffer memory needs are changing
    // and the component pools cannot be freed on-the-fly (e.g. HdmiRx input format change).
    // This will imply a temporary double allocation of the component pools.
    //
    virtual DecodeBufferManagerStatus_t   EnterStreamSwitch() = 0;

    //
    // Functions to specify the component list, and to enable or
    // disable the inclusion of specific components in a decode buffer.
    //
    virtual DecodeBufferManagerStatus_t   FillOutDefaultList(BufferFormat_t                      PrimaryFormat,
                                                             DecodeBufferComponentElementMask_t  Elements,
                                                             DecodeBufferComponentDescriptor_t   List[]) = 0;

    virtual DecodeBufferManagerStatus_t   InitializeComponentList(DecodeBufferComponentDescriptor_t *List) = 0;
    virtual DecodeBufferManagerStatus_t   InitializeSimpleComponentList(BufferFormat_t  Type) = 0;

    virtual DecodeBufferManagerStatus_t   ModifyComponentFormat(DecodeBufferComponentType_t  Component,
                                                                BufferFormat_t               NewFormat) = 0;

    virtual DecodeBufferManagerStatus_t   ComponentEnable(DecodeBufferComponentType_t Component,
                                                          bool                        Enabled) = 0;

    virtual void DisplayComponents() = 0;
    virtual void ResetDecoderMap() = 0;
    virtual DecodeBufferManagerStatus_t   CreateDecoderMap() = 0;

    virtual void RegisterPreprocBufferPool(BufferPool_t p, unsigned int index = 0) = 0;
    virtual BufferPool_t GetRegisterPreprocPool(unsigned int index) = 0;

    //
    // Functions to get/release a decode buffer, or to free up some of the components when they are no longer needed
    //
    virtual void    AheadReleaseReferenceComponents(Buffer_t         Buffer) = 0;

    virtual DecodeBufferManagerStatus_t   GetDecodeBuffer(DecodeBufferRequest_t  *Request,
                                                          Buffer_t               *Buffer) = 0;
    virtual DecodeBufferManagerStatus_t   EnsureReferenceComponentsPresent(Buffer_t Buffer) = 0;
    virtual DecodeBufferManagerStatus_t   GetEstimatedBufferCount(unsigned int        *Count,
                                                                  DecodeBufferUsage_t  For = ForManifestation) = 0;
    virtual DecodeBufferManagerStatus_t   ReleaseBuffer(Buffer_t  Buffer,
                                                        bool      ForReference) = 0;
    virtual DecodeBufferManagerStatus_t   ReleaseBufferComponents(Buffer_t            Buffer,
                                                                  DecodeBufferUsage_t ForUse) = 0;

    virtual DecodeBufferManagerStatus_t   IncrementReferenceUseCount(Buffer_t Buffer) = 0;
    virtual DecodeBufferManagerStatus_t   DecrementReferenceUseCount(Buffer_t Buffer) = 0;

    //
    // Accessors - in order to access pointers etc
    //

    virtual BufferPool_t     ComponentPool(DecodeBufferComponentType_t  Component) = 0;
    virtual BufferFormat_t   ComponentDataType(DecodeBufferComponentType_t  Component) = 0;
    virtual unsigned int     ComponentBorder(DecodeBufferComponentType_t    Component,
                                             unsigned int            Index) = 0;
    virtual void        *ComponentAddress(DecodeBufferComponentType_t   Component,
                                          AddressType_t           AlternateAddressType) = 0;

    virtual bool         ComponentPresent(Buffer_t                    Buffer,
                                          DecodeBufferComponentType_t Component) = 0;

    virtual void         ComponentDumpViaRelay(Buffer_t                     Buffer,
                                               DecodeBufferComponentType_t  Component,
                                               unsigned int                 id,
                                               unsigned int                 type) = 0;

    virtual unsigned int     ComponentSize(Buffer_t                    Buffer,
                                           DecodeBufferComponentType_t Component) = 0;

    virtual void        *ComponentBaseAddress(Buffer_t                    Buffer,
                                              DecodeBufferComponentType_t Component) = 0;
    virtual void        *ComponentBaseAddress(Buffer_t                      Buffer,
                                              DecodeBufferComponentType_t   Component,
                                              AddressType_t                 AlternateAddressType) = 0;
    virtual unsigned int     ComponentDimension(Buffer_t                    Buffer,
                                                DecodeBufferComponentType_t Component,
                                                unsigned int                DimensionIndex) = 0;
    virtual unsigned int     ComponentStride(Buffer_t                    Buffer,
                                             DecodeBufferComponentType_t Component,
                                             unsigned int                DimensionIndex,
                                             unsigned int                ComponentIndex) = 0;

    virtual unsigned int     DecimationFactor(Buffer_t          Buffer,
                                              unsigned int      DecimationIndex) = 0;


    virtual unsigned int     LumaSize(Buffer_t                    Buffer,
                                      DecodeBufferComponentType_t Component) = 0;
    virtual unsigned char   *Luma(Buffer_t                    Buffer,
                                  DecodeBufferComponentType_t Component) = 0;
    virtual unsigned char   *Luma(Buffer_t                    Buffer,
                                  DecodeBufferComponentType_t Component,
                                  AddressType_t               AlternateAddressType) = 0;

    virtual unsigned int     ChromaSize(Buffer_t                    Buffer,
                                        DecodeBufferComponentType_t Component) = 0;
    virtual unsigned char   *Chroma(Buffer_t                    Buffer,
                                    DecodeBufferComponentType_t Component) = 0;
    virtual unsigned char   *Chroma(Buffer_t                    Buffer,
                                    DecodeBufferComponentType_t Component,
                                    AddressType_t               AlternateAddressType) = 0;

    virtual unsigned int     RasterSize(Buffer_t                    Buffer,
                                        DecodeBufferComponentType_t Component) = 0;
};

#endif
