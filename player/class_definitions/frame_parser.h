/************************************************************************
Copyright (C) 2003-2014 STMicroelectronics. All Rights Reserved.

This file is part of the Streaming Engine.

Streaming Engine is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License version 2 as
published by the Free Software Foundation.

Streaming Engine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with Streaming Engine; see the file COPYING.  If not, write to the Free
Software Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307,
USA.

The Streaming Engine Library may alternatively be licensed under a
proprietary license from ST.
************************************************************************/

#ifndef H_FRAME_PARSER
#define H_FRAME_PARSER

#include "player.h"

enum
{
    FrameParserNoError              = PlayerNoError,
    FrameParserError                = PlayerError,

    FrameParserNoStreamParameters   = BASE_FRAME_PARSER,
    FrameParserPartialFrameParameters,
    FrameParserUnhandledHeader,
    FrameParserHeaderSyntaxError,
    FrameParserHeaderUnplayable,
    FrameParserStreamSyntaxError,

    FrameParserFailedToCreateReversePlayStacks,

    FrameParserFailedToAllocateBuffer,

    FrameParserReferenceListConstructionDeferred,
    FrameParserInsufficientReferenceFrames,

    FrameParserStreamUnplayable
};

typedef PlayerStatus_t  FrameParserStatus_t;

enum
{
    FrameParserHeaderFlagPartitionPoint             = 0x0001,
    FrameParserHeaderFlagPossibleReversiblePoint    = 0x0002,
    FrameParserHeaderFlagConfirmReversiblePoint     = 0x0004,
};

typedef unsigned int    FrameParserHeaderFlag_t;

class FrameParser_c : public BaseComponentClass_c
{
public:
    virtual FrameParserStatus_t   Connect(Port_c *Port) = 0;

    virtual FrameParserStatus_t   Input(Buffer_t  CodedBuffer) = 0;

    virtual FrameParserStatus_t   GetNextDecodeFrameIndex(unsigned int   *Index) = 0;
    virtual FrameParserStatus_t   SetNextFrameIndices(unsigned int        Value) = 0;

    //
    // Additions to support H264 framedecode (rather than slice), and reverible collation
    //

    virtual FrameParserStatus_t ResetReverseFailureCounter() {return FrameParserNoError;}

    virtual FrameParserStatus_t GetMpeg2TimeCode(stm_se_ctrl_mpeg2_time_code_t *TimeCode)
    {
        (void)TimeCode; // warning removal
        return FrameParserNoError;
    }
};

// ---------------------------------------------------------------------
//
// Documentation
//

/*! \class FrameParser_c
\brief Responsible for parsing coded frames extract metadata from the frame.

The frame parser class is responsible for taking individual coded
frames parsing them to extract parameters controlling the decode and
manifestation of the frame, and passing the coded frame on to the
output ring for passing to the codec for decoding. This is a list of
its entrypoints, and a partial list of the calls it makes, and the data
structures it accesses, these are in addition to the standard component
class entrypoints, and the complete list of support entrypoints in the
Player class.

The partial list of entrypoints used by this class:

- Empty list.

The partial list of meta data types used by this class:

- Attached to input buffers:
  - <b>CodedFrameParameters</b>, Describes the coded frame output.
  - <b>StartCodeList</b>, Optional output attachment for those collators generating a start code scan.

- Added to output buffers:
  - <b>ParsedFrameParameters</b>, Contains the parsed frame/stream parameters pointers, and the assigned frame index.
  - <b>Parsed[Video|Audio|Data]Parameters</b>, Optional output attachment containing the descriptive parameters for a frame used in manifestation/timing.

*/

/*! \fn FrameParserStatus_t FrameParser_c::Connect(Port_c *Port)
\brief Connect to a Port_c on which parsed frame buffers are to be placed.

\param Port Pointer to a Port_c instance.

\return Frame parser status code, FrameParserNoError indicates success.
*/

/*! \fn FrameParserStatus_t FrameParser_c::Input(Buffer_t CodedBuffer)
\brief Accept input from a collator.

This function accepts a coded frame buffer, as output from a collator.

\param CodedBuffer A pointer to a Buffer_c instance.

\return Frame parser status code, FrameParserNoError indicates success.
*/


/*! \fn FrameParserStatus_t FrameParser_c::GetNextDecodeFrameIndex()
\brief Get the last decode frame index.

This function reads the last decode frame index, to pass onto the next
(or even this) frame parser durting a stream switch.

\return Frame parser status code, FrameParserNoError indicates success.
*/

/*! \fn FrameParserStatus_t FrameParser_c::SetNextFrameIndices()
\brief Set the next decode frame index, and the next display frame index.

This function sets the next decode, and display, frame index. It is used
to give continuity to the indices after a stream switch.

\return Frame parser status code, FrameParserNoError indicates success.
*/

#endif
