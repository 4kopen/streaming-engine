/************************************************************************
Copyright (C) 2003-2014 STMicroelectronics. All Rights Reserved.

This file is part of the Streaming Engine.

Streaming Engine is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License version 2 as
published by the Free Software Foundation.

Streaming Engine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with Streaming Engine; see the file COPYING.  If not, write to the Free
Software Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307,
USA.

The Streaming Engine Library may alternatively be licensed under a
proprietary license from ST.
************************************************************************/

#ifndef H_PLAYER
#define H_PLAYER

#include "osinline.h"
#include "stm_se.h"
#include "player_types.h"

#include "base_component_class.h"

#include "buffer.h"
#include "port.h"

#include "predefined_metadata_types.h"
#include "owner_identifiers.h"

#include "collator.h"
#include "frame_parser.h"
#include "codec.h"
#include "manifestor.h"
#include "output_coordinator.h"
#include "output_timer.h"
#include "decode_buffer_manager.h"
#include "manifestation_coordinator.h"

#define MAXIMUM_SPEED_SUPPORTED 64
#define FRACTIONAL_MINIMUM_SPEED_SUPPORTED 100000

#define PLAYER_STRINGIFY(x) #x

// A macro to disallow the copy constructor and operator= functions
// This should be used in the private: declarations for a class
#ifndef DISALLOW_COPY_AND_ASSIGN
#define DISALLOW_COPY_AND_ASSIGN(TypeName) \
  TypeName(const TypeName&);               \
  void operator=(const TypeName&)
#endif

class Player_c
{
public:
    //
    // Globally visible data items
    //

    PlayerStatus_t  InitializationStatus;

    BufferType_t    MetaDataInputDescriptorType;
    BufferType_t    MetaDataCodedFrameParametersType;
    BufferType_t    MetaDataStartCodeListType;
    BufferType_t    MetaDataParsedFrameParametersType;
    BufferType_t    MetaDataParsedFrameParametersReferenceType;
    BufferType_t    MetaDataParsedVideoParametersType;
    BufferType_t    MetaDataParsedAudioParametersType;
    BufferType_t    MetaDataOutputTimingType;

    BufferType_t    BufferFakeType;
    BufferType_t    BufferPlayerControlStructureType;
    BufferType_t    MetaDataSequenceNumberType;     //made available so WMA can make it's private CodedFrameBufferPool properly
    BufferType_t    MetaDataUserDataType;

    Player_c()
        : InitializationStatus(PlayerNoError)
        , MetaDataInputDescriptorType(0)
        , MetaDataCodedFrameParametersType(0)
        , MetaDataStartCodeListType(0)
        , MetaDataParsedFrameParametersType(0)
        , MetaDataParsedFrameParametersReferenceType(0)
        , MetaDataParsedVideoParametersType(0)
        , MetaDataParsedAudioParametersType(0)
        , MetaDataOutputTimingType(0)
        , BufferFakeType(0)
        , BufferPlayerControlStructureType(0)
        , MetaDataSequenceNumberType(0)
        , MetaDataUserDataType(0)
    {}

    virtual ~Player_c() {}

    // Buffer type getters
    virtual BufferType_t GetCodedFrameBufferType() const = 0;
    virtual BufferType_t GetStartCodeHeadersBufferType() const = 0;

    //
    // Mechanisms for registering global items
    //

    virtual PlayerStatus_t   RegisterBufferManager(BufferManager_t     BufferManager) = 0;

    virtual void             RegisterLateTuneables() = 0;

    //
    // Mechanisms relating to event retrieval
    //

    virtual PlayerStatus_t   SpecifySignalledEvents(PlayerPlayback_t   Playback,
                                                    PlayerStream_t     Stream,
                                                    PlayerEventMask_t  Events,
                                                    void              *UserData              = NULL) = 0;

    //
    // Mechanisms for policy management
    //

    virtual PlayerStatus_t   SetPolicy(PlayerPlayback_t          Playback,
                                       PlayerStream_t            Stream,
                                       PlayerPolicy_t            Policy,
                                       int                       PolicyValue           = PolicyValueApply) = 0;

    //
    // Mechanisms for managing playbacks
    //

    virtual PlayerStatus_t   CreatePlayback(OutputCoordinator_t       OutputCoordinator,
                                            PlayerPlayback_t         *Playback) = 0;

    virtual PlayerStatus_t   TerminatePlayback(PlayerPlayback_t       Playback) = 0;

    virtual int              GetPlaybackCount() const = 0;

    //
    // Mechanisms for inserting module specific parameters
    //

    virtual PlayerStatus_t   SetModuleParameters(PlayerPlayback_t  Playback,
                                                 PlayerStream_t    Stream,
                                                 unsigned int      ParameterBlockSize,
                                                 void             *ParameterBlock) = 0;
    //
    // Support functions for the child classes
    //

    virtual BufferManager_t  GetBufferManager() = 0;

    virtual BufferPool_t     GetControlStructurePool() = 0;

    virtual int              PolicyValue(PlayerPlayback_t          Playback,
                                         PlayerStream_t            Stream,
                                         PlayerPolicy_t            Policy) = 0;

    //
    // Low power functions
    //

    virtual PlayerStatus_t   LowPowerEnter() = 0;
    virtual PlayerStatus_t   LowPowerExit() = 0;
    virtual PlayerStatus_t   PlaybackLowPowerEnter(PlayerPlayback_t    Playback) = 0;
    virtual PlayerStatus_t   PlaybackLowPowerExit(PlayerPlayback_t     Playback) = 0;

    virtual PlayerStatus_t   SetControl(PlayerPlayback_t   Playback,
                                        PlayerStream_t     Stream,
                                        stm_se_ctrl_t      Ctrl,
                                        const void        *Value) = 0;

    virtual PlayerStatus_t   GetControl(PlayerPlayback_t   Playback,
                                        PlayerStream_t     Stream,
                                        stm_se_ctrl_t      Ctrl,
                                        void              *Value,
                                        bool               ForceReset = false) = 0;

    virtual void GetReadLockBWLimiter() = 0;
    virtual void ReleaseReadLockBWLimiter() = 0;
    virtual void GetWriteLockBWLimiter() = 0;
    virtual void ReleaseWriteLockBWLimiter() = 0;
};

// ---------------------------------------------------------------------
//
// Documentation
//

/*! \class Player_c
\brief Glue all the other components together.

This class is the glue that holds all the components. Its methods from
the primary means for the player wrapper to manipulate a player instance.

*/

/*! \var PlayerStatus_t Player_c::InitializationStatus;
\brief Status flag indicating how the initialization of the class went.

In order to fit well into the Linux kernel environment the player is
compiled with exceptions and RTTI disabled. In C++ constructors cannot
return an error code since all errors should be reported using
exceptions. With exceptions disabled we need to use this alternative
means to communicate that the constructed object instance is not valid.

*/

/*! \var BufferType_t Player_c::MetaDataInputDescriptorType
*/

/*! \var BufferType_t Player_c::MetaDataCodedFrameParametersType
*/

/*! \var BufferType_t Player_c::MetaDataStartCodeListType
*/

/*! \var BufferType_t Player_c::MetaDataParsedFrameParametersType
*/

/*! \var BufferType_t Player_c::MetaDataParsedFrameParametersReferenceType
*/

/*! \var BufferType_t Player_c::MetaDataParsedVideoParametersType
*/

/*! \var BufferType_t Player_c::MetaDataParsedAudioParametersType
*/

/*! \var BufferType_t Player_c::MetaDataOutputTimingType
*/

//
// Mechanisms for registering global items
//

/*! \fn PlayerStatus_t Player_c::RegisterBufferManager( BufferManager_t           BufferManager )
\brief Register a buffer manager.

Register a player-global buffer manager.

\param BufferManager A pointer to a BufferManager_c instance.

\return Player status code, PlayerNoError indicates success.
*/

//
// Mechanisms relating to event retrieval
//

/*! \fn PlayerStatus_t Player_c::SpecifySignalledEvents(        PlayerPlayback_t          Playback,
                            PlayerStream_t            Stream,
                            PlayerEventMask_t         Events,
                            void                     *UserData              = NULL )
\brief Enable/disable ongoing event signalling.

This function is specifically for enabling/disabling ongoing event signalling.

\param Playback Playback identifier.
\param Stream   Stream identifier.
\param Events   Mask indicating which occurrences should raise an event record.
\param UserData Optional user pointer to be associated with any event raised.

\return Player status code, PlayerNoError indicates success.
*/

//
// Mechanisms for policy management
//

/*! \fn PlayerStatus_t Player_c::SetPolicy(                     PlayerPlayback_t          Playback,
                            PlayerStream_t            Stream,
                            PlayerPolicy_t            Policy,
                            int                       PolicyValue           = PolicyValueApply )
\brief Set playback policy for the specified playback and stream.

This method allows the control of behaviours within Player 2, whether
for a specific playback stream, or for all streams.

\param Playback Playback identifier (or PlayerAllPlaybacks to set policy for all playbacks)
\param Stream   Stream identifier (or PlayerAllStreams to set policy for all streams)
\param Policy   Policy identifier
\param PolicyValue Value for the policy (defaults to PolicyValueApply)

\return Player status code, PlayerNoError indicates success.
*/

//
// Mechanisms for managing playbacks
//

/*! \fn PlayerStatus_t Player_c::CreatePlayback(                OutputCoordinator_t       OutputCoordinator,
                            PlayerPlayback_t         *Playback )
\brief Create a playback context.

This method is responsible for creation of a playback context.

\param OutputCoordinator Pointer to an OutputCoordinator_c instance.
\param Playback Pointer to a variable, to be filled with the playback identifier.

\return Player status code, PlayerNoError indicates success.
*/

/*! \fn PlayerStatus_t Player_c::TerminatePlayback(             PlayerPlayback_t          Playback)
\brief Destroy a playback context.

Whether or not the termination should be immediate, or involve the
playout out all component streams, will be subject to the current
applicable policies in effect.

\param Playback      Playback identifier to be terminated.

\return Player status code, PlayerNoError indicates success.
*/

//
// Mechanisms for inserting module specific parameters
//

/*! \fn PlayerStatus_t Player_c::SetModuleParameters( PlayerPlayback_t          Playback,
                            PlayerStream_t            Stream,
                            unsigned int              ParameterBlockSize,
                            void                     *ParameterBlock )
\brief Specify a modules parameters.

Note: The parameter block will be used directly for immediate settings,
but will be copied for non-immediate use, so its lifetime is the same as
that of the function call.

\param Playback           Playback identifier.
\param Stream             Stream identifier.
\param ParameterBlockSize Size of the parameter block (in bytes)
\param ParameterBlock     Pointer to the parameter block

\return Player status code, PlayerNoError indicates success.
*/

//
// Support functions for the child classes
//

/*! \fn  BufferManager_t Player_c::GetBufferManager()
\brief Obtain a pointer to the buffer manager.

<b>WARNING</b>: This is an internal function and should only be called by player components.

\return pointer to buffer manager
*/

/*! \fn PlayerStatus_t Player_c::GetCodedFrameBufferPool(       PlayerStream_t            Stream,
                            BufferPool_t             *Pool = NULL,
                            unsigned int             *MaximumCodedFrameSize = NULL )
\brief Obtain a pointer to the coded frame buffer pool used by the specified stream.

<b>WARNING</b>: This is an internal function and should only be called by player components.

\param Stream                Stream identifier.
\param Pool                  Pointer to a variable to store a pointer to the BufferPool_c instance, or NULL.
\param MaximumCodedFrameSize Pointer to a variable to store the maximum coded frame size, or NULL.
\return Player status code, PlayerNoError indicates success.
*/

/*! \fn PlayerStatus_t Player_c::CallInSequence(                PlayerStream_t            Stream,
                            PlayerSequenceType_t      SequenceType,
                            PlayerSequenceValue_t     SequenceValue,
                            PlayerComponentFunction_t Fn,
                            ... )
\brief Do something magic that makes sense only to that nice Mr. Haydock.

<b>TODO: This method is not adequately documented.</b>

<b>WARNING</b>: This is an internal function and should only be called by player components.

\param Stream           Stream identifier.
\param SequenceType     TODO
\param SequenceValue    TODO
\param Fn               TODO

\return Player status code, PlayerNoError indicates success.
*/

/*! \fn int Player_c::PolicyValue(            PlayerPlayback_t          Playback,
                            PlayerStream_t            Stream,
                            PlayerPolicy_t            Policy )
\brief Get playback policy for the specified playback and stream.
Takes as parameters a playback identifier, a stream identifier and a policy identifier.

<b>WARNING</b>: This is an internal function and should only be called by player components.

\param Playback Playback identifier
\param Stream   Stream identifier
\param Policy   Policy identifier

\return The policy value. Any policy that has not been set will return the value 0.
*/

#endif
