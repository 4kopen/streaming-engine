/************************************************************************
Copyright (C) 2003-2014 STMicroelectronics. All Rights Reserved.

This file is part of the Streaming Engine.

Streaming Engine is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License version 2 as
published by the Free Software Foundation.

Streaming Engine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with Streaming Engine; see the file COPYING.  If not, write to the Free
Software Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307,
USA.

The Streaming Engine Library may alternatively be licensed under a
proprietary license from ST.
************************************************************************/

////////////////////////////////////////////////////////////////////////////
/// \class Codec_MmeAudio_c
///
/// Audio specific code to assist operation of the AUDIO_DECODER transformer
/// provided by the audio firmware.
///

#include "codec_mme_audio.h"
#include "player_generic.h"
#include "player_playback.h"
#include "player_stream.h"
#include "manifestor_audio.h"
#include "manifestor_encode_audio.h"
#include "manifestor_audio_sourceGrab.h"
#include "havana_stream.h"
#include "audio_codec_params.h"

#include "st_relayfs_se.h"

#undef TRACE_TAG
#define TRACE_TAG "Codec_MmeAudio_c"

#define BUFFER_TRANSCODED_FRAME_BUFFER        "TranscodedFrameBuffer"
#define BUFFER_TRANSCODED_FRAME_BUFFER_TYPE   {BUFFER_TRANSCODED_FRAME_BUFFER, BufferDataTypeBase, AllocateFromSuppliedBlock, 4, 64, false, false, 0}

static BufferDataDescriptor_t            InitialTranscodedFrameBufferDescriptor = BUFFER_TRANSCODED_FRAME_BUFFER_TYPE;

#define BUFFER_COMPRESSED_FRAME_BUFFER        "CompressedFrameBuffer"
#define BUFFER_COMPRESSED_FRAME_BUFFER_TYPE   {BUFFER_COMPRESSED_FRAME_BUFFER, BufferDataTypeBase, AllocateFromSuppliedBlock, 4, 64, false, false, 0}

static BufferDataDescriptor_t            InitialCompressedFrameBufferDescriptor = BUFFER_COMPRESSED_FRAME_BUFFER_TYPE;

#define BUFFER_AUX_FRAME_BUFFER        "AuxFrameBuffer"
#define BUFFER_AUX_FRAME_BUFFER_TYPE   {BUFFER_AUX_FRAME_BUFFER, BufferDataTypeBase, AllocateFromSuppliedBlock, 4, 64, false, false, 0}

static BufferDataDescriptor_t          InitialAuxFrameBufferDescriptor = BUFFER_AUX_FRAME_BUFFER_TYPE;

#define BUFFER_METADATA_BUFFER        "MetadataBuffer"
static BufferDataDescriptor_t InitialMetadataBufferDescriptor =
{
    BUFFER_METADATA_BUFFER,      //.TypeName
    BufferDataTypeBase,          //.Type
    AllocateFromSuppliedBlock,   //.AllocationSource
    4,                           //.RequiredAllignment
    4,                           //.AllocationUnitSize
    true,                        //.HasFixedSize
    false,                       //.AllocateOnPoolCreation
    sizeof(tMetadata)            //.FixedSize
};

///
Codec_MmeAudio_c::Codec_MmeAudio_c()
    : DefaultAudioSurfaceDescriptor()
    , AudioOutputSurface(NULL)
    , ParsedAudioParameters(NULL)
    , AudioParametersEvents()
    , SelectedChannel(ChannelSelectStereo)
    , DRC()
    , OutmodeMain(ACC_MODE_ID) // output raw decoded data by default
    , OutmodeAux(ACC_MODE_ID)
    , StreamDrivenDownmix(false)
    , RawAudioSamplingFrequency(STM_SE_DEFAULT_AUDIO_SAMPLE_RATE_HZ)
    , RawAudioNbChannels(2) // Set default NbChannels = 2
    , RelayfsIndex(0)
    , CurrentTranscodeBufferIndex(0)
    , TranscodedBuffers()
    , CurrentTranscodeBuffer(NULL)
    , TranscodeEnable(false)
    , TranscodeNeeded(false)
    , CompressedFrameNeeded(false)
    , TranscodedFrameMemoryDevice(NULL)
    , TranscodedFramePool(NULL)
    , TranscodedFrameMemory()
    , TranscodedFrameBufferDescriptor(NULL)
    , TranscodedFrameBufferType()
    , CurrentAuxBufferIndex(0)
    , AuxBuffers()
    , CurrentAuxBuffer(NULL)
    , AuxOutputEnable(false)
    , AuxFrameMemoryDevice(NULL)
    , AuxFramePool(NULL)
    , AuxFrameMemory()
    , AuxFrameBufferDescriptor(NULL)
    , AuxFrameBufferType()
    , CurrentCompressedFrameBufferIndex()
    , CompressedFrameBuffers()
    , CurrentCompressedFrameBuffer()
    , NoOfCompressedFrameBuffers(0)
    , CompressedFrameEnable(false)
    , CompressedFrameMemoryDevice(NULL)
    , CompressedFramePool(NULL)
    , CompressedFrameMemory()
    , CompressedFrameBufferDescriptor(NULL)
    , CompressedFrameBufferType()
    , mShallResetAudioCodec(false)
    , ProtectTransformName(false)
    , TransformName()
    , AudioDecoderTransformCapability()
    , AudioDecoderTransformCapabilityMask()
    , AudioDecoderInitializationParameters()
    , AudioDecoderStatus()
    , mTempoRequestedRatio(0)
    , mTempoSetRatio(0)
    , mEvent()
    , mNewAudioParametersValues()
    , mDecodedAudioMode()
    , CurrentMDBufferIndex(0)
    , MDBuffers()
    , CurrentMDBuffer(NULL)
    , MDFrameMemoryDevice(0)
    , MDFramePool(NULL)
    , MDFrameMemory()
{
    SetGroupTrace(group_decoder_audio);

    RelayfsIndex = st_relayfs_getindex_fortype_se(ST_RELAY_TYPE_DECODED_AUDIO_AUX_BUFFER0);

    Configuration.CodecName = "unknown audio";

    strncpy(Configuration.TranscodedMemoryPartitionName, "aud-transcoded", sizeof(Configuration.TranscodedMemoryPartitionName));
    Configuration.TranscodedMemoryPartitionName[sizeof(Configuration.TranscodedMemoryPartitionName) - 1] = '\0';
    strncpy(Configuration.AncillaryMemoryPartitionName, "aud-codec-data", sizeof(Configuration.AncillaryMemoryPartitionName));
    Configuration.AncillaryMemoryPartitionName[sizeof(Configuration.AncillaryMemoryPartitionName) - 1] = '\0';

    Configuration.StreamParameterContextCount           = 0;
    Configuration.StreamParameterContextDescriptor      = 0;
    Configuration.DecodeContextCount                    = 0;
    Configuration.DecodeContextDescriptor               = 0;
    Configuration.MaxDecodeIndicesPerBuffer             = 2;
    Configuration.SliceDecodePermitted                  = false;
    Configuration.AvailableTransformers                 = AUDIO_CODEC_MAX_TRANSFORMERS;

    Configuration.TransformName[0]                      = AUDIO_DECODER_TRANSFORMER_NAME0;
    Configuration.TransformName[1]                      = AUDIO_DECODER_TRANSFORMER_NAME1;
    Configuration.TransformName[2]                      = AUDIO_DECODER_TRANSFORMER_NAME2;

    Configuration.SizeOfTransformCapabilityStructure    = sizeof(AudioDecoderTransformCapability);
    Configuration.TransformCapabilityStructurePointer   = (void *)(&AudioDecoderTransformCapability);
    Configuration.AddressingMode                        = CachedAddress;
    Configuration.ShrinkCodedDataBuffersAfterDecode     = false;

    // Fill in surface descriptor details with our assumed defaults (copied from ManifestorAudio_c)
    DefaultAudioSurfaceDescriptor.StreamType               = StreamTypeAudio;
    DefaultAudioSurfaceDescriptor.ClockPullingAvailable    = true;
    DefaultAudioSurfaceDescriptor.MasterCapable            = true;
    DefaultAudioSurfaceDescriptor.BitsPerSample            = 32;
    DefaultAudioSurfaceDescriptor.ChannelCount             = 8;
    DefaultAudioSurfaceDescriptor.SampleRateHz             = 0;
    DefaultAudioSurfaceDescriptor.FrameRate                = 1; // rational

    AudioParametersEvents.audio_coding_type  = STM_SE_STREAM_ENCODING_AUDIO_NONE;
}

Codec_MmeAudio_c::~Codec_MmeAudio_c()
{
    if (CurrentTranscodeBuffer)
    {
        CurrentTranscodeBuffer->DecrementReferenceCount();
    }

    //
    // Release the coded frame buffer pool
    //

    if (TranscodedFramePool != NULL)
    {
        BufferManager->DestroyPool(TranscodedFramePool);
    }

    AllocatorClose(&TranscodedFrameMemoryDevice);

    // Release the Aux buffers if they are not currectly attached to anything.

    if (CurrentAuxBuffer)
    {
        CurrentAuxBuffer->DecrementReferenceCount();
    }

    // Release the Aux frame buffer pool
    if (AuxFramePool != NULL)
    {
        BufferManager->DestroyPool(AuxFramePool);
    }

    AllocatorClose(&AuxFrameMemoryDevice);

    //
    // Release metadata resources
    //
    if (CurrentMDBuffer)
    {
        CurrentMDBuffer->DecrementReferenceCount();
    }

    if (MDFramePool != NULL)
    {
        BufferManager->DestroyPool(MDFramePool);
    }


    AllocatorClose(&MDFrameMemoryDevice);

    //
    // Release the compressed frame buffer if its not been attached to anything.
    //
    for (int32_t i = 0; i < AUDIO_DECODER_COMPRESSED_FRAME_BUFFER_SCATTER_PAGES; i++)
    {
        if (CurrentCompressedFrameBuffer[i])
        {
            CurrentCompressedFrameBuffer[i]->DecrementReferenceCount();
        }
    }

    //
    // Release the compressed frame buffer pool
    //

    if (CompressedFramePool != NULL)
    {
        BufferManager->DestroyPool(CompressedFramePool);
    }

    AllocatorClose(&CompressedFrameMemoryDevice);
}

CodecStatus_t   Codec_MmeAudio_c::Halt()
{
    AudioOutputSurface          = NULL;
    ParsedAudioParameters       = NULL;
    SelectedChannel             = ChannelSelectStereo;

    st_relayfs_freeindex_fortype_se(ST_RELAY_TYPE_DECODED_AUDIO_AUX_BUFFER0, RelayfsIndex);

    return Codec_MmeBase_c::Halt();
}

// /////////////////////////////////////////////////////////////////////////
//
//      SetModuleParameters function
//      Action  : Allows external user to set up important environmental parameters
//      Input   :
//      Output  :
//      Result  :
//

CodecStatus_t   Codec_MmeAudio_c::SetModuleParameters(
    unsigned int   ParameterBlockSize,
    void          *ParameterBlock)
{
    struct CodecParameterBlock_s *CodecParameterBlock = (struct CodecParameterBlock_s *)ParameterBlock;

    if (ParameterBlockSize != sizeof(struct CodecParameterBlock_s))
    {
        SE_ERROR("Invalid parameter block\n");
        return CodecError;
    }

    // CodecSpecifyDownmix: received from the mixer to specify StreamDrivenDownmix
    if (CodecParameterBlock->ParameterType == CodecSpecifyDownmix)
    {
        if (StreamDrivenDownmix != CodecParameterBlock->Downmix.StreamDrivenDownmix)
        {
            StreamDrivenDownmix  = CodecParameterBlock->Downmix.StreamDrivenDownmix;
            SE_DEBUG(group_decoder_audio, "Setting StreamDrivenDownmix to %d\n", StreamDrivenDownmix);
            ForceStreamParameterReload = true;
        }

        return CodecNoError;
    }

    return Codec_MmeBase_c::SetModuleParameters(ParameterBlockSize, ParameterBlock);
}

////////////////////////////////////////////////////////////////////////////
///
///  Set Default FrameBase style TRANSFORM command IOs
///  Populate DecodeContext with 1 Input and 1 output Buffer
///  Populate I/O MME_DataBuffers
//
//   This function must be mutex-locked by caller
//
void Codec_MmeAudio_c::PresetIOBuffers()
{
    CodecBufferState_t      *State;
    OS_AssertMutexHeld(&Lock);
    // plumbing
    DecodeContext->MMEBufferList[0] = &DecodeContext->MMEBuffers[0];
    DecodeContext->MMEBufferList[1] = &DecodeContext->MMEBuffers[1];

    for (int i = 0; i < 2; ++i)
    {
        DecodeContext->MMEBufferList[i] = &DecodeContext->MMEBuffers[i];
        memset(&DecodeContext->MMEBuffers[i], 0, sizeof(MME_DataBuffer_t));
        DecodeContext->MMEBuffers[i].StructSize           = sizeof(MME_DataBuffer_t);
        DecodeContext->MMEBuffers[i].NumberOfScatterPages = 1;
        DecodeContext->MMEBuffers[i].ScatterPages_p       = &DecodeContext->MMEPages[i];
        memset(&DecodeContext->MMEPages[i], 0, sizeof(MME_ScatterPage_t));
    }

    // input
    DecodeContext->MMEBuffers[0].TotalSize = CodedDataLength;
    DecodeContext->MMEPages[0].Page_p      = CodedData;
    DecodeContext->MMEPages[0].Size        = CodedDataLength;
    // output
    State   = &BufferState[CurrentDecodeBufferIndex];
    DecodeContext->MMEBuffers[1].TotalSize = Stream->GetDecodeBufferManager()->ComponentSize(State->Buffer, PrimaryManifestationComponent);
    DecodeContext->MMEPages[1].Page_p      = Stream->GetDecodeBufferManager()->ComponentBaseAddress(State->Buffer, PrimaryManifestationComponent);
    DecodeContext->MMEPages[1].Size        = Stream->GetDecodeBufferManager()->ComponentSize(State->Buffer, PrimaryManifestationComponent);
}

////////////////////////////////////////////////////////////////////////////
///
///  Set Default FrameBase style TRANSFORM command for AudioDecoder MT
///  with 1 Input Buffer and 1 Output Buffer.
//
//   This function must be mutex-locked by caller
//
void Codec_MmeAudio_c::SetCommandIO()
{
    OS_AssertMutexHeld(&Lock);
    PresetIOBuffers();
    // FrameBase Transformer :: 1 Input Buffer / 1 Output Buffer sent through same MME_TRANSFORM
    DecodeContext->MMECommand.NumberInputBuffers  = 1;
    DecodeContext->MMECommand.NumberOutputBuffers = 1;
    DecodeContext->MMECommand.DataBuffers_p = DecodeContext->MMEBufferList;
}


////////////////////////////////////////////////////////////////////////////
///
/// PresetMetadataOutBuffer:
//  - tries to get a metadata buffer and attach it to DecodeContextBuffer
//  - fills a MME_DataBuffer_t pointed by MDBuffer input argument with
//    an empty Buffer dedicated to recieve metadata from the decoder transformer
//  - attaches MMEPage argument as the Scatter page of MMEBuffer input argument.
//  - Uses MMEPage input argument to store metadata buffer
//  - increments NbOutBuffer input argument value in case of success
//
CodecStatus_t Codec_MmeAudio_c::PresetMetadataOutBuffer(MME_DataBuffer_t  *MMEBuffer,
                                                        MME_ScatterPage_t *MMEPage,
                                                        unsigned int      *NbOutBuffer)
{
    CodecStatus_t Status;
    if (MMEBuffer == NULL || MMEPage == NULL || NbOutBuffer == NULL)
    {
        SE_ERROR("NULL input pointer: MMEBuffer=%p MMEPage=%p NbOutBuffer=%p\n", MMEBuffer, MMEPage, NbOutBuffer);
        return CodecError;
    }

    if ((Status = GetMetadataBuffer()) != CodecNoError)
    {
        SE_ERROR("Cannot GetMetadataBuffer\n");
        return Status;
    }

    if (CurrentDecodeBuffer->AttachBuffer(CurrentMDBuffer) != BufferNoError)
    {
        SE_ERROR("Cannot attach Metadata Buffer\n");
        return CodecError;
    }

    // Empty metadata output buffer setup
    memset(MMEBuffer, 0, sizeof(MME_DataBuffer_t));
    memset(MMEPage, 0, sizeof(MME_ScatterPage_t));

    MMEBuffer->StructSize           = sizeof(MME_DataBuffer_t);
    MMEBuffer->NumberOfScatterPages = 1;
    MMEBuffer->ScatterPages_p       = MMEPage;
    MMEBuffer->Flags                = BUFFER_TYPE_META_DATA_IO;
    MMEBuffer->TotalSize            = MDBuffers[CurrentMDBufferIndex].BufferLength;

    MMEPage->Page_p                 = MDBuffers[CurrentMDBufferIndex].BufferPointer;
    MMEPage->Size                   = MDBuffers[CurrentMDBufferIndex].BufferLength;

    (*NbOutBuffer)++;

    return CodecNoError;
}

////////////////////////////////////////////////////////////////////////////
///
/// CheckMetadataValidity:
//
CodecStatus_t Codec_MmeAudio_c::CheckMetadataValidity(MME_DataBuffer_t *MMEBuffer)
{
    if (MMEBuffer == NULL)
    {
        SE_ERROR("NULL MMEBuffer pointer\n");
        return CodecError;
    }

    if (MMEBuffer->Flags != BUFFER_TYPE_META_DATA_IO)
    {
        SE_ERROR("Not a Metadata Buffer\n");
        return CodecError;
    }

    for (unsigned int pageIdx = 0; pageIdx < MMEBuffer->NumberOfScatterPages; pageIdx++)
    {
        MME_ScatterPage_t *page = &MMEBuffer->ScatterPages_p[pageIdx];
        if (page == NULL)
        {
            SE_ERROR("NULL Page pointer MMEBuffer:%p pageIdx:%d\n", MMEBuffer, pageIdx);
            return CoderError;
        }

        tMetadata *metadata = (tMetadata *) page->Page_p;
        if (metadata == NULL)
        {
            continue;
        }

        if (metadata->Version != AUDIO_METADATA_VERSION)
        {
            SE_ERROR("Metadata Version is not compatible: Ver:0x%X [0x:%X]\n",
                     metadata->Version, AUDIO_METADATA_VERSION);
            return CodecError;
        }
        if (SE_IS_VERBOSE_ON(group_decoder_audio))
        {
            SE_VERBOSE(group_decoder_audio, "ScatterPage[%d] BytesUsed:%d\n", pageIdx, page->BytesUsed);
            SE_VERBOSE(group_decoder_audio, "MD ptr    :%p\n", metadata);
            SE_VERBOSE(group_decoder_audio, "MD version:0x%X\n", metadata->Version);
            SE_VERBOSE(group_decoder_audio, "MD NbSets :%d\n", metadata->NbSets);
        }
        st_relayfs_write_se(ST_RELAY_TYPE_AUDIO_METADATA + RelayfsIndex, ST_RELAY_SOURCE_SE,
                            (unsigned char *) metadata, sizeof(tMetadata), false);
    }

    return CodecNoError;
}

// /////////////////////////////////////////////////////////////////////////
//
//      Connect output port function
//

CodecStatus_t   Codec_MmeAudio_c::Connect(Port_c *Port)
{
//OutputSurfaceDescriptor_t       **ListOfSurfaceParameterPointers;
    //
    // Define the decode buffer elements -
    // Different from video as handle capabilities is done in audio
    // before a decode buffer manager exists (during construction).
    //
    Stream->GetDecodeBufferManager()->FillOutDefaultList(FormatAudio,
                                                         PrimaryManifestationElement,
                                                         Configuration.ListOfDecodeBufferComponents);

    HandleCpuSelectionPolicy();

    if (SelectedTransformer == CPU_HOST_TRANSFORMER_IDX)
    {
        Configuration.ListOfDecodeBufferComponents[0].DefaultAddressMode = CachedAddress;
    }
    else
    {
        Configuration.ListOfDecodeBufferComponents[0].DefaultAddressMode = PhysicalAddress;
    }

    SE_VERBOSE(group_decoder_audio, "Use %s \n",
               (Configuration.ListOfDecodeBufferComponents[0].DefaultAddressMode == PhysicalAddress)
               ? "PhysicalAddress" : "CachedAddress");

    AudioOutputSurface  = &DefaultAudioSurfaceDescriptor;
    if (AudioOutputSurface == NULL)
    {
        SE_ERROR("(%s) - Failed to get output surface parameters\n", Configuration.CodecName);
        return CodecError;
    }

    //
    // Perform the standard operations
    //
    CodecStatus_t Status = Codec_MmeBase_c::Connect(Port);
    if (Status != CodecNoError)
    {
        return Status;
    }

    //
    // To support audio with switch stream, we force reload of stream parameters
    //
    ForceStreamParameterReload  = true;
    //
    return CodecNoError;
}


// /////////////////////////////////////////////////////////////////////////
//
// Set the Codec Parameters that have been updated since last frame
//

void   Codec_MmeAudio_c::UpdateConfig(unsigned int Update)
{
    // NEW_MUTE_CONFIG doesn't require params update
    // because it is checked upon every FillOutDecodeCommand
    if (Update == NEW_MUTE_CONFIG)
    {
        return;
    }

    ForceStreamParameterReload = true;

    // The following config update only require to trigger the Reload of Parameters
    // NEW_EMPHASIS_CONFIG
    // NEW_DUALMONO_CONFIG
    // NEW_STEREO_CONFIG
    // NEW_DRC_CONFIG
    //

    if (Update & NEW_STEREO_CONFIG)
    {
        PlayerStatus_t PlayerStatus = Player->GetControl(Playback, Stream,
                                                         STM_SE_CTRL_STREAM_DRIVEN_STEREO,
                                                         &StreamDrivenDownmix);
        if (PlayerStatus != PlayerNoError)
        {
            SE_ERROR("Failed to get STREAM_DRIVEN_STEREO control (%08x)\n", PlayerStatus);
        }

        SE_DEBUG(group_decoder_audio, "Applying STREAM_DRIVEN_STEREO config\n");
    }

    if (Update & NEW_SPEAKER_CONFIG)
    {
        stm_se_audio_channel_assignment_t channelAssignment;
        PlayerStatus_t PlayerStatus = Player->GetControl(Playback, Stream,
                                                         STM_SE_CTRL_SPEAKER_CONFIG,
                                                         &channelAssignment);
        if (PlayerStatus != PlayerNoError)
        {
            SE_ERROR("Failed to get SPEAKER_CONFIG control (%08x)\n", PlayerStatus);
        }
        else if (0 != channelAssignment.malleable)
        {
            // Progressively check disconnected pairs of outputs to deduce the audiomode to apply
            enum eAccAcMode outmodeMain_from_player = ACC_MODE_ID;
            if (channelAssignment.pair3 != STM_SE_AUDIO_CHANNEL_PAIR_NOT_CONNECTED)
            {
                outmodeMain_from_player = ACC_MODE_ALL8;    // ALL PAIRS connected
            }
            else if (channelAssignment.pair2 != STM_SE_AUDIO_CHANNEL_PAIR_NOT_CONNECTED)
            {
                outmodeMain_from_player = ACC_MODE_ALL6;    // PAIR 0, PAIR1 & PAIR2 connected
            }
            else if (channelAssignment.pair1 != STM_SE_AUDIO_CHANNEL_PAIR_NOT_CONNECTED)
            {
                outmodeMain_from_player = ACC_MODE_ALL4;    // PAIR 0 & PAIR1 connected
            }
            else if (channelAssignment.pair0 != STM_SE_AUDIO_CHANNEL_PAIR_NOT_CONNECTED)
            {
                outmodeMain_from_player = ACC_MODE_ALL2;    // only PAIR0 conneted
            }
            OutmodeMain = outmodeMain_from_player;
            SE_DEBUG(group_decoder_audio, "Setting codec OutmodeMain from player to %s\n", StmSeAudioAcModeGetName(OutmodeMain));
        }
        else
        {
            // if malleable = 0, use provided channel assignment to deduce the audiomode to apply
            enum eAccAcMode outmodeMain_from_player;
            if (0 == StmSeAudioGetAcModeFromChannelAssignment(&outmodeMain_from_player, &channelAssignment))
            {
                SE_DEBUG(group_decoder_audio, "Setting codec OutmodeMain from player to %s\n", StmSeAudioAcModeGetName(outmodeMain_from_player));
                OutmodeMain = outmodeMain_from_player;
                SE_DEBUG(group_decoder_audio, "Applying new SPEAKER_CONFIG %x\n", OutmodeMain);
            }
            else
            {
                SE_ERROR("Invalid Channel Assignment\n");
            }
        }
    }
}


// /////////////////////////////////////////////////////////////////////////
//
//      The get coded frame buffer pool fn
//

CodecStatus_t   Codec_MmeAudio_c::Input(Buffer_t          CodedBuffer)
{
    //
    // Are we allowed in here
    //
    AssertComponentState(ComponentRunning);

    PlayerSequenceNumber_t *SequenceNumberStructure;
    CodedBuffer->ObtainMetaDataReference(Player->MetaDataSequenceNumberType, (void **)(&SequenceNumberStructure));
    SE_ASSERT(SequenceNumberStructure != NULL);

    // Check if we received a Drain (PlayOut or Discard) marker frame
    // If yes, we shall restart the decoder before decoding the first frame of
    // any new compressed data sequence
    if ((SequenceNumberStructure->mIsMarkerFrame) && (mShallResetAudioCodec == false) &&
        ((SequenceNumberStructure->mMarkerFrame.mMarkerType == DrainPlayOutMarker) || (SequenceNumberStructure->mMarkerFrame.mMarkerType == DrainDiscardMarker)))
    {
        SE_DEBUG(group_decoder_audio, "a Drain Marker Frame has been received :: audio-codec will be reset before processing next frame\n");
        mShallResetAudioCodec = true;
    }

    //
    // First perform base operations
    //
    CodecStatus_t Status = Codec_MmeBase_c::Input(CodedBuffer);
    if (Status != CodecNoError)
    {
        return Status;
    }

    // Check if any transform will be generated and only if so evaluate the params change
    if (CodedDataLength != 0)
    {
        EvaluateImplicitDownmixPromotion();
        EvaluateTranscodeCompressedNeeded();
        EvaluateMixerDRCParameters();
    }

    SE_VERBOSE(group_decoder_audio,  "(%s) - Handling frame %d newparams=%d eof=%d restart=%d\n"
               , Configuration.CodecName
               , ParsedFrameParameters->DisplayFrameIndex
               , ParsedFrameParameters->NewFrameParameters
               , ParsedFrameParameters->EofMarkerFrame
               , mShallResetAudioCodec
              );

    //
    // Do we need to issue a new set of stream parameters
    //

    if (ParsedFrameParameters->NewStreamParameters)
    {
        memset(&StreamParameterContext->MMECommand, 0, sizeof(MME_Command_t));

        Status = FillOutSetStreamParametersCommand();
        if (Status == CodecNoError)
        {
            Status = SendMMEStreamParameters();
        }

        if (Status != CodecNoError)
        {
            SE_ERROR("(%s) - Failed to fill out, and send, a set stream parameters command\n", Configuration.CodecName);
            ForceStreamParameterReload   = true;
            StreamParameterContextBuffer->DecrementReferenceCount();
            StreamParameterContextBuffer = NULL;
            StreamParameterContext       = NULL;
            OS_LockMutex(&Lock);
            ReleaseDecodeContext(DecodeContext);
            OS_UnLockMutex(&Lock);
            return Status;
        }

        StreamParameterContextBuffer    = NULL;
        StreamParameterContext          = NULL;
    }

    //
    // If there is no frame to decode then exit now.
    //

    /* In StreamBase decoder EofMarker buffer need to be send to the FW.
       So that FW can return Eof Marker after consuming entire input */

    if ((!ParsedFrameParameters->NewFrameParameters) && (!ParsedFrameParameters->EofMarkerFrame))
    {
        SE_VERBOSE(group_decoder_audio, "(%s) - Nothing to do with this frame\n", Configuration.CodecName);
        return CodecNoError;
    }

    Status = FillOutDecodeContext();
    if (Status != CodecNoError)
    {
        return Status;
    }

    //
    // Load the parameters into MME command
    //
    //! set up default to MME_TRANSFORM - fine for most codecs
    DecodeContext->MMECommand.CmdCode                           = MME_TRANSFORM;
    //! will change to MME_SEND_BUFFERS in WMA/OGG over-ridden FillOutDecodeCommand(), thread to do MME_TRANSFORMs
    //
    Status = FillOutDecodeCommand();
    if (Status != CodecNoError)
    {
        SE_ERROR("(%s) - Failed to fill out a decode command\n", Configuration.CodecName);
        OS_LockMutex(&Lock);
        ReleaseDecodeContext(DecodeContext);
        OS_UnLockMutex(&Lock);
        return Status;
    }

    if (mShallResetAudioCodec == true)
    {
        AudioCodecDecodeContext_t *Context = (AudioCodecDecodeContext_t *)DecodeContext;
        SE_VERBOSE(group_decoder_audio, "(%s) - request a restart of the codec\n", Configuration.CodecName);
        Context->Audio.FrameParams.Restart = ACC_MME_TRUE;
        mShallResetAudioCodec              = false;
    }
    //
    // Ensure that the coded frame will be available throughout the
    // life of the decode by attaching the coded frame to the decode
    // context prior to launching the decode.
    //
    AttachCodedFrameBuffer();

    // Attach the Aux Buffer to the decode context
    AttachAuxFrameBuffer();

    Status = SendMMEDecodeCommand();
    if (Status != CodecNoError)
    {
        SE_ERROR("(%s) - Failed to send a decode command\n", Configuration.CodecName);
        OS_LockMutex(&Lock);
        ReleaseDecodeContext(DecodeContext);
        OS_UnLockMutex(&Lock);
        return Status;
    }

    //
    // We have finished decoding into this buffer
    //
    FinishedDecode();
    return CodecNoError;
}

////////////////////////////////////////////////////////////////////////////
///
/// Attach the coded frame buffer to the decode frame.
/// In case of transcoding is required, attach also attach
/// the transcoded buffer to the original coded buffer
///
void Codec_MmeAudio_c::AttachCodedFrameBuffer()
{
    DecodeContextBuffer->AttachBuffer(CodedFrameBuffer);
    //DecodeContextBuffer->Dump (DumpAll);

    if (TranscodeEnable && TranscodeNeeded)
    {
        Buffer_t CodedDataBuffer;
        CurrentDecodeBuffer->ObtainAttachedBufferReference(CodedFrameBufferType, &CodedDataBuffer);
        SE_ASSERT(CodedDataBuffer != NULL);

        CodedDataBuffer->AttachBuffer(CurrentTranscodeBuffer);
        CurrentTranscodeBuffer->DecrementReferenceCount();
        CurrentTranscodeBuffer = NULL;
        // the transcoded buffer is now only referenced by its attachment to the coded buffer
    }
}

////////////////////////////////////////////////////////////////////////////
///
/// Attach the Aux frame buffer to the decode frame.
///
void Codec_MmeAudio_c::AttachAuxFrameBuffer()
{
    if (AuxOutputEnable)
    {
        if (CurrentAuxBuffer != NULL)
        {
            DecodeContextBuffer->AttachBuffer(CurrentAuxBuffer);
            CurrentAuxBuffer->DecrementReferenceCount();
            CurrentAuxBuffer = NULL;
        }
    }
}

////////////////////////////////////////////////////////////////////////////
///
/// Populate the DecodeContext structure with parameters for MPEG audio.
/// This can be over-ridden by WMA to not have any output buffer, as it will
/// do an MME_SEND_BUFFERS instead of MME_TRANSFORM
///
CodecStatus_t Codec_MmeAudio_c::FillOutDecodeContext()
{
    OS_LockMutex(&Lock);

    //
    // Obtain a new buffer if needed
    //

    if (CurrentDecodeBufferIndex == INVALID_INDEX)
    {
        CodecStatus_t Status = GetDecodeBuffer();
        if (Status != CodecNoError)
        {
            SE_ERROR("(%s) - Failed to get decode buffer\n", Configuration.CodecName);
            ReleaseDecodeContext(DecodeContext);
            OS_UnLockMutex(&Lock);
            return Status;
        }
    }

    //
    // Record the buffer being used in the decode context
    //
    DecodeContext->BufferIndex  = CurrentDecodeBufferIndex;

    //
    // Provide default values for the input and output buffers (the sub-class can change this if it wants to).
    //
    // Happily the audio firmware is less mad than the video firmware on the subject of buffers.
    //
    memset(&DecodeContext->MMECommand, 0, sizeof(MME_Command_t));

    SetCommandIO();

    DecodeContext->DecodeInProgress     = true;
    BufferState[CurrentDecodeBufferIndex].DecodesInProgress++;
    BufferState[CurrentDecodeBufferIndex].OutputOnDecodesComplete       = true;

    OS_UnLockMutex(&Lock);

    return CodecNoError;
}

////////////////////////////////////////////////////////////////////////////
///
/// Clear up - to be over-ridden by WMA codec to do nothing, as actual decode done elsewhere
///
void Codec_MmeAudio_c::FinishedDecode()
{
    OS_LockMutex(&Lock);
    //
    // We have finished decoding into this buffer
    //
    CurrentDecodeBufferIndex    = INVALID_INDEX;
    CurrentDecodeIndex          = INVALID_INDEX;
    OS_UnLockMutex(&Lock);
}


////////////////////////////////////////////////////////////////////////////
///
///     The intercept to the initialize data types function, that
///     ensures the audio specific type is recorded in the configuration
///     record.
///

CodecStatus_t   Codec_MmeAudio_c::InitializeDataTypes()
{
    //
    // Add audio specific types, and the address of
    // parsed audio parameters to the configuration
    // record. Then pass on down to the base class
    //
    Configuration.AudioVideoDataParsedParametersType    = Player->MetaDataParsedAudioParametersType;
    Configuration.AudioVideoDataParsedParametersPointer = (void **)&ParsedAudioParameters;
    Configuration.SizeOfAudioVideoDataParsedParameters  = sizeof(ParsedAudioParameters_t);
    return Codec_MmeBase_c::InitializeDataTypes();
}

////////////////////////////////////////////////////////////////////////////
///
/// Examine the capability structure returned by the firmware.
///
/// In addition to some diagnostic output and some trivial error checking the
/// primary purpose of this function is to ensure that the firmware is capable
/// of performing the requested decode.
///
CodecStatus_t   Codec_MmeAudio_c::ParseCapabilities(unsigned int ActualTransformer)
{
    // Locally rename the VeryLongMemberNamesUsedInTheSuperClass
    MME_LxAudioDecoderInfo_t &Capability = AudioDecoderTransformCapability;
    MME_LxAudioDecoderInfo_t &Mask = AudioDecoderTransformCapabilityMask;

    // Check for version skew
    if (Capability.StructSize != sizeof(MME_LxAudioDecoderInfo_t))
    {
        SE_ERROR("%s reports different sizeof MME_LxAudioDecoderInfo_t (version skew?)\n",
                 Configuration.TransformName[ActualTransformer]);
        return CodecError;
    }

    // Dump the transformer capability structure
    SE_DEBUG(group_decoder_audio,  "Transformer Capability: %s\n", Configuration.TransformName[ActualTransformer]);
    SE_DEBUG(group_decoder_audio,  "  DecoderCapabilityFlags = %x (requires %x)\n",
             Capability.DecoderCapabilityFlags, Mask.DecoderCapabilityFlags);
    SE_DEBUG(group_decoder_audio,  "  DecoderCapabilityExtFlags[0..1] = %x %x\n",
             Capability.DecoderCapabilityExtFlags[0],
             Capability.DecoderCapabilityExtFlags[1]);
    SE_DEBUG(group_decoder_audio,  "  PcmProcessorCapabilityFlags[0..1] = %x %x\n",
             Capability.PcmProcessorCapabilityFlags[0],
             Capability.PcmProcessorCapabilityFlags[1]);

    // Confirm that the transformer is sufficiently capable to support the required decoder
    if ((Mask.DecoderCapabilityFlags         !=
         (Mask.DecoderCapabilityFlags         & Capability.DecoderCapabilityFlags)) ||
        (Mask.DecoderCapabilityExtFlags[0]   !=
         (Mask.DecoderCapabilityExtFlags[0]   & Capability.DecoderCapabilityExtFlags[0])) ||
        (Mask.DecoderCapabilityExtFlags[1]   !=
         (Mask.DecoderCapabilityExtFlags[1]   & Capability.DecoderCapabilityExtFlags[1])) ||
        (Mask.PcmProcessorCapabilityFlags[0] !=
         (Mask.PcmProcessorCapabilityFlags[0]   & Capability.PcmProcessorCapabilityFlags[0])) ||
        (Mask.PcmProcessorCapabilityFlags[1] !=
         (Mask.PcmProcessorCapabilityFlags[1]   & Capability.PcmProcessorCapabilityFlags[1])))
    {
        SE_WARNING("%s is not capable of decoding %s\n",
                   Configuration.TransformName[ActualTransformer], Configuration.CodecName);
        return CodecError;
    }

    return CodecNoError;
}

void Codec_MmeAudio_c::HandleCpuSelectionPolicy()
{
    int AudioCpuSelect = Player->PolicyValue(Playback, Stream, PolicyCpuSelectionId);

    if (AudioCpuSelect == PolicyValueCpuSelectionDefault)
    {
        if (Player->PolicyValue(Playback, Stream, PolicyAudioApplicationType) == PolicyValueAudioApplicationMS12)
        {
            // Select HOST CPU if user provided PolicyValueAudioApplicationMS12
            AudioCpuSelect = PolicyValueCpuSelectionHost;
        }
        else
        {
            // Check SE Global Strategy about which default coprocessor is expected to perform audio decoding.
            AudioCpuSelect = ModuleParameter_PolicyValueCpuSelection();
        }
    }

    if (AudioCpuSelect != PolicyValueCpuSelectionDefault)
    {
        int  newTransformerId;

        // In case user has provided PolicyCpuSelectionId, select CPU provided by user.
        // Also overwrite transformer if selected above for PolicyValueAudioApplicationMS12.
        switch (AudioCpuSelect)
        {
        case PolicyValueCpuSelectionAudio:
            newTransformerId = CPU_AUDIO_TRANSFORMER_IDX;
            break;
        case PolicyValueCpuSelectionGP:
            newTransformerId = CPU_GP_TRANSFORMER_IDX;
            break;
        case PolicyValueCpuSelectionHost:
            newTransformerId = CPU_HOST_TRANSFORMER_IDX;
            break;
        default:
            newTransformerId = SelectedTransformer;
            break;
        }

        if (newTransformerId != SelectedTransformer)
        {
            // Switch the transformer to the new selected CPU
            SelectTransformer(newTransformerId);
        }
    }
}

////////////////////////////////////////////////////////////////////////////
///
/// Populate the supplied structure with parameters for MPEG audio.
///
CodecStatus_t Codec_MmeAudio_c::FillOutTransformerGlobalParameters(MME_LxAudioDecoderGlobalParams_t *GlobalParams_p)
{
    MME_LxAudioDecoderGlobalParams_t &GlobalParams = *GlobalParams_p;
    MME_LxDecConfig_t &Config           = *((MME_LxDecConfig_t *) GlobalParams.DecConfig);
    unsigned char     *PcmParams_p      = ((unsigned char *) &Config) + Config.StructSize;
    unsigned char      Policy;
    int                channelSelection = Player->PolicyValue(Playback, Stream, PolicyAudioDualMonoChannelSelection);
    //
    MME_LxPcmProcessingGlobalParams_Subset_t &PcmParams = *((MME_LxPcmProcessingGlobalParams_Subset_t *) PcmParams_p);
    PcmParams.StructSize = sizeof(PcmParams);
    PcmParams.DigSplit   = ACC_SPLIT_AUTO;
    PcmParams.AuxSplit   = ACC_SPLIT_AUTO;
    MME_DeEmphGlobalParams_t &deemph = PcmParams.DeEmphasis;
    int                        deemph_apply = Player->PolicyValue(Playback, Stream, PolicyAudioDeEmphasis);
    deemph.Id         = PCMPROCESS_SET_ID(ACC_PCM_DeEMPH_ID, ACC_MIX_MAIN);
    deemph.StructSize = sizeof(MME_DeEmphGlobalParams_t);
    deemph.Apply      = (enum eAccProcessApply) deemph_apply;
    deemph.Mode       = ACC_DEEMPH_50_15_us;

    // DRC parameters
    //  1- first try to get DRC parameters at player level (stream or playback)
    //  2- if not available, the uses DRC settings from mixer (which can be modified through ALSA)
    stm_se_drc_t main_drc_params;
    if (Player->PolicyValue(Playback, Stream, PolicyAudioApplicationType) == PolicyValueAudioApplicationMS12)
    {
        // Since in MS12, DRC is applied after Mixer, we explicitly disable the application of DRC at Decoder
        main_drc_params.mode  = (stm_se_compression_mode_t)STM_SE_NO_COMPRESSION;
        main_drc_params.cut   = 0;
        main_drc_params.boost = 0;
    }
    else
    {
        if (PlayerNoError != Player->GetControl(Playback, Stream, STM_SE_CTRL_AUDIO_DYNAMIC_RANGE_COMPRESSION, &main_drc_params))
        {
            SE_DEBUG(group_decoder_audio, "No DRC-MAIN parameters available at player level, using mixer parameters\n");
            main_drc_params.mode  = (stm_se_compression_mode_t) DRC.DRC_Type;
            main_drc_params.cut   = DRC.DRC_HDR;
            main_drc_params.boost = DRC.DRC_LDR;
        }
    }

    MME_CMCGlobalParams_t &cmc = PcmParams.CMC;
    cmc.Id               = PCMPROCESS_SET_ID(ACC_PCM_CMC_ID, ACC_MIX_MAIN);
    cmc.StructSize       = sizeof(MME_CMCGlobalParams_t);
    cmc.OutMode          = OutmodeMain;
    cmc.PcmDownScaled    = ACC_MME_TRUE;
    cmc.CenterMixCoeff   = ACC_M3DB;
    cmc.SurroundMixCoeff = ACC_M3DB;
    cmc.GlobalGainCoeff  = ACC_UNITY;
    cmc.LfeMixCoeff      = ACC_UNITY;
    cmc.CutFactor        = main_drc_params.cut;
    cmc.BoostFactor      = main_drc_params.boost;
    cmc.ComprMode        = StmSeAudioGetFwDrcCode(main_drc_params.mode);
    cmc.TargetLevel      = 0;
    cmc.DisableDNorm     = (main_drc_params.mode == STM_SE_COMP_CUSTOM_A);
    switch (cmc.OutMode)
    {
    case ACC_MODE_ALL2:
    case ACC_MODE_ALL4:
    case ACC_MODE_ALL6:
    case ACC_MODE_ALL8:
    {
        stm_se_audio_channel_assignment_t channelAssignment;
        PlayerStatus_t playerResult = Player->GetControl(Playback, Stream, STM_SE_CTRL_SPEAKER_CONFIG, (void *) &channelAssignment);
        if (playerResult != PlayerNoError)
        {
            SE_ERROR("Failed to get SPEAKER_CONFIG control (%08x)\n", playerResult);
        }
        else
        {
            cmc.PcmConfig.ChannelPair0 = channelAssignment.pair0;
            cmc.PcmConfig.ChannelPair1 = channelAssignment.pair1;
            cmc.PcmConfig.ChannelPair2 = channelAssignment.pair2;
            cmc.PcmConfig.ChannelPair3 = channelAssignment.pair3;
            cmc.PcmConfig.ChannelPair4 = channelAssignment.pair4;
            SE_DEBUG(group_decoder_audio, "Update PcmConfig structure with provided SPEAKER_CONFIG for malleable case\n");
        }
        break;
    }

    default:
        break;
    }
    SE_DEBUG(group_decoder_audio, "DRC params: MAIN[mode=%d - cut=%d - boost=%d]\n", cmc.ComprMode, cmc.CutFactor, cmc.BoostFactor);

    Policy  = (Player->PolicyValue(Playback, Stream, PolicyAudioStreamDrivenDualMono) == PolicyValueStreamDrivenDualMono) ? ACC_DUAL_1p1_ONLY : 0;

    switch (channelSelection)
    {
    case PolicyValueDualMonoStereoOut:
        SelectedChannel = ChannelSelectStereo;
        Policy |= ACC_DUAL_LR;
        break;

    case PolicyValueDualMonoLeftOut:
        SelectedChannel = ChannelSelectLeft;
        Policy |= ACC_DUAL_LEFT_MONO;
        break;

    case PolicyValueDualMonoRightOut:
        SelectedChannel = ChannelSelectRight;
        Policy |= ACC_DUAL_RIGHT_MONO;
        break;

    case PolicyValueDualMonoMixLeftRightOut:
        SelectedChannel = ChannelSelectMono;
        Policy |= ACC_DUAL_MIX_LR_MONO;
        break;

    default:
        SE_ERROR("Invalid Channel Selection\n");
        break;
    }

    // StreamDrivenDownmix
    // 1- first try to get StreamDrivenDownmix parameter at player level (stream or playback)
    // 2- if not available, the uses StreamDrivenDownmix setting from mixer (which can be modified through ALSA)
    bool streamDrivenDownmix_to_apply;

    if (PlayerNoError != Player->GetControl(Playback, Stream, STM_SE_CTRL_STREAM_DRIVEN_STEREO, (void *) &streamDrivenDownmix_to_apply))
    {
        SE_DEBUG(group_decoder_audio, "No StreamDrivenDownmix parameter available at player level, using mixer parameter\n");
        streamDrivenDownmix_to_apply  = StreamDrivenDownmix;
    }

    if (streamDrivenDownmix_to_apply)
    {
        Policy |= ACC_DUAL_AUTO;
    }

    cmc.DualMode       = Policy;
    SE_DEBUG(group_decoder_audio,  "Set DualMode to %d: [Channel Select = %d] (1p1only=%d]\n", Policy, SelectedChannel,
             ((Policy & ACC_DUAL_1p1_ONLY) != 0));
    SE_DEBUG(group_decoder_audio,  "StreamDrivenDownmix value applied =%d\n", streamDrivenDownmix_to_apply);

    MME_DMixGlobalParams_t &dmix = PcmParams.DMix;
    dmix.Id = PCMPROCESS_SET_ID(ACC_PCM_DMIX_ID, ACC_MIX_MAIN);
    dmix.Apply = ACC_MME_AUTO;
    dmix.StructSize = sizeof(MME_DMixGlobalParams_t);
    dmix.Config[DMIX_USER_DEFINED] = ACC_MME_FALSE;
    dmix.Config[DMIX_STEREO_UPMIX] = ACC_MME_FALSE;
    dmix.Config[DMIX_MONO_UPMIX] = ACC_MME_FALSE;
    dmix.Config[DMIX_MEAN_SURROUND] = ACC_MME_FALSE;
    dmix.Config[DMIX_SECOND_STEREO] = ACC_MME_TRUE;
    dmix.Config[DMIX_NORMALIZE] = DMIX_NORMALIZE_ON;
    dmix.Config[DMIX_NORM_IDX] = 0;
    dmix.Config[DMIX_DIALOG_ENHANCE] = ACC_MME_FALSE;

    MME_Resamplex2GlobalParams_t &resamplex2 = PcmParams.Resamplex2;
    resamplex2.Id = PCMPROCESS_SET_ID(ACC_PCM_RESAMPLE_ID, ACC_MIX_MAIN);
    resamplex2.StructSize = sizeof(MME_Resamplex2GlobalParams_t);
    resamplex2.Apply = ACC_MME_DISABLED;

    // WARNING : Activation of TempoControl involves a delay line
    // When we turn on the TempoControl, the PTS will be modified
    // wrt the duration of the delay line.
    // Consequently, tempo Control should not be activated for
    // PAUSE otherwise the Output timer will observe undue
    // non-linearity of timeline

    MME_TempoGlobalParams_t &tempo = PcmParams.Tempo;
    Rational_t               Speed;
    PlayDirection_t          Direction;
    Playback->GetSpeed(&Speed, &Direction);
    tempo.Id         = PCMPROCESS_SET_ID(ACC_PCM_TEMPO_ID, ACC_MIX_MAIN);
    tempo.StructSize = sizeof(MME_TempoGlobalParams_t);

    if ((Speed > 0) && (Direction == PlayForward) && (Speed != 1))
    {
        tempo.Apply  = ACC_MME_ENABLED;
        // Speed =  80/100 ==> - 20% slower
        // Speed = 200/100 ==> +100% faster
        tempo.Ratio  = (Speed * 100).RoundedIntegerPart() - 100;
    }
    else
    {
        tempo.Apply  = ACC_MME_DISABLED;
        tempo.Ratio  = 0;
    }
    mTempoRequestedRatio = tempo.Ratio;

    return CodecNoError;
}

////////////////////////////////////////////////////////////////////////////
///
/// Populate the AUDIO_DECODER's initialization parameters for audio.
///
/// Zero the entire initialization structure and populate those portions that
/// are generic for all AUDIO_DECODER instantiations.
///
/// WARNING: This method does not do what you think it does.
///
/// More exactly, if you think that this method will populate the
/// global parameters structure which forms the bulk of the initialization
/// parameters structure then you're wrong! Basically the codec specific
/// initialization parameters are variable size so from this method we don't
/// know what byte offset to use.
///
/// The upshot of all this is that this method \b must be overridden in
/// our sub-classes.
///
CodecStatus_t Codec_MmeAudio_c::FillOutTransformerInitializationParameters()
{
    MME_LxAudioDecoderInitParams_t &Params = AudioDecoderInitializationParameters;
    memset(&Params, 0, sizeof(Params));

    Params.StructSize = sizeof(MME_LxAudioDecoderInitParams_t);
    Params.CacheFlush = ACC_MME_ENABLED;
    Params.BlockWise.u32 = 0;
    Params.Reserved = 0;
    Params.MemConfig.Crc.CRC_Main = Codec_c::EnableAudioCRC;
    Params.MemConfig.Crc.CRC_ALL  = Codec_c::EnableAudioCRC == 2;
    // run the audio decoder with a fixed number of main (surround) channels
    // The aux channel has been initialised to 2 the (max value for aux o/p)
    Params.NChans[ACC_MIX_MAIN] = AudioOutputSurface->ChannelCount;
    Params.NChans[ACC_MIX_AUX]  = 2;
    Params.ChanPos[ACC_MIX_MAIN] = 0;
    Params.ChanPos[ACC_MIX_AUX] = 0;

    return CodecNoError;
}

void Codec_MmeAudio_c::GlobalParamsCommandCompleted(CodecBaseStreamParameterContext_t *StreamParameterContext)
{
    SE_VERBOSE(group_decoder_audio, "\n");

    if (mTempoSetRatio != mTempoRequestedRatio)
    {
        SE_DEBUG(group_decoder_audio, "Tempo Speed Change:%d->%d\n", mTempoSetRatio, mTempoRequestedRatio);
        mTempoSetRatio = mTempoRequestedRatio;
    }

    return Codec_MmeBase_c::GlobalParamsCommandCompleted(StreamParameterContext);
}

////////////////////////////////////////////////////////////////////////////
///
/// Validate the PCM processing extended status structures and squawk loudly if problems are found.
///
CodecStatus_t Codec_MmeAudio_c::ValidatePcmProcessingExtendedStatus(
    CodecBaseDecodeContext_t *Context,
    MME_PcmProcessingFrameExtStatus_t *PcmStatus)
{
    ParsedAudioParameters_t *AudioParameters;
    //
    // provide default values for parameters that may be influenced by the extended status
    //
    OS_LockMutex(&Lock);
    AudioParameters = BufferState[Context->BufferIndex].ParsedParameters.Audio;

    if (AudioParameters == NULL)
    {
        SE_ERROR("(%s) - AudioParameters are NULL\n", Configuration.CodecName);
        OS_UnLockMutex(&Lock);
        return CodecError;
    }

    AudioParameters->MixingMetadata.IsMixingMetadataPresent = false;
    OS_UnLockMutex(&Lock);
    //
    // Get ready to process the status messages (and perform a little bounds checking
    //
    int mt, id;
    int DmixTableNo = 0;
    int BytesLeft = PcmStatus->BytesUsed;
    MME_PcmProcessingStatusTemplate_t *SpecificStatus = &(PcmStatus->PcmStatus);

    if (BytesLeft > (int)(sizeof(MME_PcmProcessingFrameExtStatus_Concrete_t) - sizeof(PcmStatus->BytesUsed) + sizeof(MME_PcmProcessingFrameExtCommonStatus_t)))
    {
        SE_ERROR("BytesLeft is too large - BytesLeft %d\n", BytesLeft);
        return CodecError;
    }

    //
    // Iterate through the status messages processing each one
    //

    while (BytesLeft > 0)
    {
        // check for sanity
        if (SpecificStatus->StructSize < 8 ||
            SpecificStatus->StructSize > (unsigned) BytesLeft)
        {
            SE_ERROR("PCM extended status is too %s - Id %x  StructSize %d\n",
                     (SpecificStatus->StructSize < 8 ? "small" : "large"),
                     SpecificStatus->Id, SpecificStatus->StructSize);
            return CodecError;
        }

        // handle the status report
        mt = ACC_PCMPROC_MT(SpecificStatus->Id);
        id = ACC_PCMPROC_ID(SpecificStatus->Id);

        switch (mt)
        {
        case ACC_PCMPROCESS_MAIN_MT :
            switch (id)
            {
            case ACC_PCM_CMC_ID:
                SE_DEBUG(group_decoder_audio, "Found [Output 0 (MAIN)]\n");
                // do nothing with it
                break;

            case ACC_PCM_DMIX_ID:
                SE_DEBUG(group_decoder_audio, "Found [DMIX report on (MAIN)]\n");
                HandleDownmixTable(Context, SpecificStatus, DmixTableNo);
                DmixTableNo++;
                break;

            default:
                SE_INFO(group_decoder_audio, "Ignoring unexpected PCM-MAIN [%d] extended status - Id %x  StructSize %d\n", id,
                        SpecificStatus->Id, SpecificStatus->StructSize);
                // just ignore it... don't propagate the error
            }

            break;

        case ACC_DECODER_MT:
            SE_DEBUG(group_decoder_audio, "Found Codec[%d] SpecificStatus , StructSize %d\n",
                     SpecificStatus->Id, SpecificStatus->StructSize);
            break;

        case ACC_PCMPROCESS_COMMON_MT:
            switch (SpecificStatus->Id)
            {
            case ACC_MIX_METADATA_ID:
                SE_DEBUG(group_decoder_audio, "Found ACC_MIX_METADATA_ID - Id %x  StructSize %d\n",
                         SpecificStatus->Id, SpecificStatus->StructSize);
                HandleMixingMetadata(Context, SpecificStatus);
                break;

            case ACC_SA_ID:
                SE_DEBUG(group_decoder_audio, "Found SupplementaryAudio status :: ACC_SA_ID - Id %x  StructSize %d\n",
                         SpecificStatus->Id, SpecificStatus->StructSize);
                //      HandleMixingMetadata(Context, SpecificStatus);
                break;

            case ACC_CRC_ID:
                SE_DEBUG(group_decoder_audio, "Found ACC_CRC_ID - Id %x  StructSize %d\n",
                         SpecificStatus->Id, SpecificStatus->StructSize);
                ReportDecodedCRC(SpecificStatus);
                break;

            default:
                SE_INFO(group_decoder_audio, "Ignoring unexpected PCM-COMMON [%d] extended status - Id %x  StructSize %d\n", id,
                        SpecificStatus->Id, SpecificStatus->StructSize);
                // just ignore it... don't propagate the error
            }

            break;

        case ACC_PARSER_MT:
            SE_DEBUG(group_decoder_audio, "Found Parser status :: - Id %x  StructSize %d\n",
                     SpecificStatus->Id, SpecificStatus->StructSize);
            break;

        default:
            SE_INFO(group_decoder_audio, "Ignoring unexpected MT[%d] extended status - Id %x  StructSize %d\n", mt,
                    SpecificStatus->Id, SpecificStatus->StructSize);
            // just ignore it... don't propagate the error
        }

        // move to the next status field. we checked for sanity above so we shouldn't need any bounds checks.
        BytesLeft -= SpecificStatus->StructSize;
        SpecificStatus = (MME_PcmProcessingStatusTemplate_t *)
                         (((char *) SpecificStatus) + SpecificStatus->StructSize);
    }

    return CodecNoError;
}




////////////////////////////////////////////////////////////////////////////
///
/// Error generating stub function. Should be unreachable.
///
/// This is method is deliberately not abstract since not all sub-classes are
/// required to override it.
///
void Codec_MmeAudio_c::HandleMixingMetadata(CodecBaseDecodeContext_t *Context,
                                            MME_PcmProcessingStatusTemplate_t *PcmStatus)
{
    SE_ERROR("Found mixing metadata but sub-class didn't provide a mixing metadata handler %p %p\n",
             Context, PcmStatus);
}

// This function reports Downmix table reported by FW
void Codec_MmeAudio_c::HandleDownmixTable(CodecBaseDecodeContext_t *Context,
                                          MME_PcmProcessingStatusTemplate_t *PcmStatus, int DmixTableNo)
{
    ParsedAudioParameters_t *AudioParameters;
    MME_DmixStatus_t *DmixTable = (MME_DmixStatus_t *) PcmStatus;
    int nch_out, nch_in, i, j;
    unsigned int dmix_table_size;
    nch_out = DmixTable->Features.NChOut;
    nch_in  = DmixTable->Features.NChIn;
    dmix_table_size = SIZEOF_DMIX_STATUS(nch_out);

    //
    // Validation
    //
    if (DmixTableNo >= MAX_SUPPORTED_DMIX_TABLE)
    {
        SE_ERROR("No space to store reported Dmix table. Space supported for %d but reported for %d\n", MAX_SUPPORTED_DMIX_TABLE, DmixTableNo + 1);
        return;
    }

    if (DmixTable->StructSize < dmix_table_size)
    {
        SE_ERROR("Structure size of Dmix table is too small. got = %d expected = %d\n", DmixTable->StructSize, dmix_table_size);
        return;
    }

    if ((nch_out > MAX_NB_CHANNEL_COEFF) || nch_in > MAX_NB_CHANNEL_COEFF)
    {
        SE_ERROR("Unexpected Nch IN / Nch OUT\n");
        return;
    }

    OS_LockMutex(&Lock);
    AudioParameters = BufferState[Context->BufferIndex].ParsedParameters.Audio;

    if (AudioParameters == NULL)
    {
        SE_ERROR("(%s) - AudioParameters are NULL\n", Configuration.CodecName);
        OS_UnLockMutex(&Lock);
        return;
    }

    if (!DmixTable->Features.TablePresent)
    {
        AudioParameters->DownMixTable[DmixTableNo].IsTablePresent = 0;
        SE_DEBUG(group_decoder_audio, "Downmix table reported but not present\n");
        OS_UnLockMutex(&Lock);
        return;
    }

    AudioParameters->DownMixTable[DmixTableNo].IsTablePresent = DmixTable->Features.TablePresent;
    AudioParameters->DownMixTable[DmixTableNo].NChIn        = DmixTable->Features.NChIn;
    AudioParameters->DownMixTable[DmixTableNo].NChOut       = DmixTable->Features.NChOut;
    AudioParameters->DownMixTable[DmixTableNo].InMode       = DmixTable->Features.InMode;
    AudioParameters->DownMixTable[DmixTableNo].OutMode      = DmixTable->Features.OutMode;

    for (i = 0; i < nch_out; i++)
    {
        for (j = 0; j < nch_in; j++)
        {
            AudioParameters->DownMixTable[DmixTableNo].DownMixTableCoeff[i][j] = DmixTable->DownMixTable[i][j];
        }
    }

    OS_UnLockMutex(&Lock);
}

// This function reports CRC of the decoded output by reading FW reported status
void Codec_MmeAudio_c::ReportDecodedCRC(MME_PcmProcessingStatusTemplate_t *PcmStatus)
{
    MME_CrcStatus_t *CRCStatus = (MME_CrcStatus_t *) PcmStatus;
    st_relayfs_write_se(ST_RELAY_TYPE_AUDIO_DEC_CRC, ST_RELAY_SOURCE_SE,
                        (unsigned char *) &CRCStatus->Crc[0], sizeof(CRCStatus->Crc), false);
}

// /////////////////////////////////////////////////////////////////////////
//
//      The generic audio function used to fill out a buffer structure
//      request.
//

CodecStatus_t   Codec_MmeAudio_c::FillOutDecodeBufferRequest(DecodeBufferRequest_t     *Request)
{
    memset(Request, 0, sizeof(DecodeBufferRequest_t));

    if (ParsedAudioParameters == NULL)
    {
        SE_ERROR("(%s) - ParsedAudioParameters are NULL\n", Configuration.CodecName);
        return CodecError;
    }

    ParsedAudioParameters->Source.BitsPerSample = 32;
    ParsedAudioParameters->Source.ChannelCount  = 8;

    Request->DimensionCount     = 3;
    Request->Dimension[0]       = ParsedAudioParameters->Source.BitsPerSample;
    Request->Dimension[1]       = ParsedAudioParameters->Source.ChannelCount;
    // Size the output buffer as per the number of Samples ceiled to the nearest multiple of 8 samples.
    Request->Dimension[2]       = ParsedAudioParameters->SampleCount ? (ParsedAudioParameters->SampleCount + 7) & 0xFFF8 : Configuration.MaximumSampleCount;

    if ((Request->Dimension[0] != 32) ||
        (Request->Dimension[1] != 8) ||
        (Request->Dimension[2] == 0))
        SE_INFO(group_decoder_audio, "Non-standard parameters (%2d %d %d)\n",
                Request->Dimension[0], Request->Dimension[1], Request->Dimension[2]);

    Request->ManifestationBufferCount = MAX_DECODE_BUFFERS;
    Request->ReferenceBufferCount     = 0;

    return CodecNoError;
}

CodecStatus_t Codec_MmeAudio_c::GetAttribute(const char *Attribute, PlayerAttributeDescriptor_t *Value)
{
    if (0 == strncmp(Attribute, "sample_frequency", 32))
    {
        enum eAccFsCode SamplingFreqCode = (enum eAccFsCode) AudioDecoderStatus.SamplingFreq;
        Value->Id                        = SYSFS_ATTRIBUTE_ID_INTEGER;

        if (SamplingFreqCode < ACC_FS_reserved)
        {
            Value->u.Int = (int) StmSeTranslateDiscreteSamplingFrequencyToInteger(SamplingFreqCode);
        }
        else
        {
            Value->u.Int = 0;
        }

        return CodecNoError;
    }
    else if (0 == strncmp(Attribute, "number_channels", 32))
    {
#define C(f,b) case ACC_MODE ## f ## b: Value->u.ConstCharPointer = #f "/" #b ".0"; break; case ACC_MODE ## f ## b ## _LFE: Value->u.ConstCharPointer = #f "/" #b ".1"; break
#define Cn(f,b) case ACC_MODE ## f ## b: Value->u.ConstCharPointer = #f "/" #b ".0"; break
#define Ct(f,b) case ACC_MODE ## f ## b ## t: Value->u.ConstCharPointer = #f "/" #b ".0"; break; case ACC_MODE ## f ## b ## t_LFE: Value->u.ConstCharPointer = #f "/" #b ".1"; break
        Value->Id = SYSFS_ATTRIBUTE_ID_CONSTCHARPOINTER;

        switch ((enum eAccAcMode)(AudioDecoderStatus.DecAudioMode))
        {
            Ct(2, 0);
            C(1, 0);
            C(2, 0);
            C(3, 0);
            C(2, 1);
            C(3, 1);
            C(2, 2);
            C(3, 2);
            C(2, 3);
            C(3, 3);
            C(2, 4);
            C(3, 4);
            Cn(4, 2);
            Cn(4, 4);
            Cn(5, 2);
            Cn(5, 3);

        default:
            Value->u.ConstCharPointer = "UNKNOWN";
        }
        return CodecNoError;
    }
    else
    {
        SE_ERROR("This attribute does not exist\n");
        return CodecError;
    }

    return CodecNoError;
}

/// /////////////////////////////////////////////////////////////////////////
//
//      Get the aux frame buffer pool fn
//

CodecStatus_t   Codec_MmeAudio_c::GetAuxFrameBufferPool(BufferPool_t *Afp)
{
    allocator_status_t      AStatus;

    //
    // Create the buffer pool if not done already
    //

    if (AuxFramePool == NULL)
    {
        //
        // Coded frame buffer type
        //
        CodecStatus_t Status = InitializeDataType(&InitialAuxFrameBufferDescriptor, &AuxFrameBufferType, &AuxFrameBufferDescriptor);
        if (Status != CodecNoError)
        {
            return Status;
        }

        //
        // Get the memory and Create the pool with it
        //

        BufferManager = Player->GetBufferManager();

        AStatus = PartitionAllocatorOpen(&AuxFrameMemoryDevice,
                                         Configuration.AncillaryMemoryPartitionName,
                                         AUDIO_DECODER_AUX_FRAME_BUFFER_COUNT * Configuration.MaximumSampleCount,
                                         MEMORY_AUDIO_ACCESS);
        if (AStatus != allocator_ok)
        {
            SE_ERROR("Failed to allocate memory for %s\n", Configuration.CodecName);
            return PlayerInsufficientMemory;
        }

        AuxFrameMemory[CachedAddress]         = AllocatorUserAddress(AuxFrameMemoryDevice);
        AuxFrameMemory[PhysicalAddress]       = AllocatorPhysicalAddress(AuxFrameMemoryDevice);

        BufferStatus_t BufferStatus = BufferManager->CreatePool(&AuxFramePool,
                                                                AuxFrameBufferType,
                                                                AUDIO_DECODER_AUX_FRAME_BUFFER_COUNT,
                                                                AUDIO_DECODER_AUX_FRAME_BUFFER_COUNT * Configuration.MaximumSampleCount,
                                                                AuxFrameMemory);
        if (BufferStatus != BufferNoError)
        {
            SE_ERROR("GetAuxFrameBufferPool(%s) - Failed to create the pool\n", Configuration.CodecName);
            return CodecError;
        }

        ((PlayerStream_c *)Stream)->AuxFrameBufferType = AuxFrameBufferType;
    }

    *Afp = AuxFramePool;

    return CodecNoError;
}

// /////////////////////////////////////////////////////////////////////////
//
//      Function to obtain a new  aux buffer
//

CodecStatus_t   Codec_MmeAudio_c::GetAuxBuffer()
{
    BufferPool_t  Afp;
    unsigned int  BytesPerSample, NumberOfSamples;

    BytesPerSample  = ParsedAudioParameters->Source.BitsPerSample >> 3;
    NumberOfSamples = ParsedAudioParameters->SampleCount ? ParsedAudioParameters->SampleCount : Configuration.MaximumSampleCount;

    //
    // Get a buffer
    //

    CodecStatus_t Status = GetAuxFrameBufferPool(&Afp);
    if (Status != CodecNoError)
    {
        SE_ERROR("GetAuxBuffer(%s) - Failed to obtain the Aux buffer pool instance\n", Configuration.CodecName);
        return Status;
    }

    if (CurrentAuxBuffer)
    {
        CurrentAuxBuffer->DecrementReferenceCount();
        CurrentAuxBuffer = NULL;
    }

    BufferStatus_t BufferStatus = Afp->GetBuffer(&CurrentAuxBuffer, IdentifierCodec, NumberOfSamples * BytesPerSample * AUDIO_DECODER_MAX_AUX_CHANNEL_COUNT, false);
    if (BufferStatus != BufferNoError)
    {
        SE_ERROR("GetAuxBuffer(%s) - Failed to obtain a Aux buffer from the Aux buffer pool\n", Configuration.CodecName);
        Afp->Dump(DumpPoolStates | DumpBufferStates);

        return CodecError;
    }

    //
    // Map it and initialize the mapped entry.
    //

    CurrentAuxBuffer->GetIndex(&CurrentAuxBufferIndex);

    if (CurrentAuxBufferIndex >= AUDIO_DECODER_AUX_FRAME_BUFFER_COUNT)
    {
        SE_ERROR("GetAuxBuffer(%s) - Aux buffer index >= AUDIO_DECODER_AUX_FRAME_BUFFER_COUNT - Implementation error\n", Configuration.CodecName);
        return CodecError;
    }

    memset(&AuxBuffers[CurrentAuxBufferIndex], 0, sizeof(AdditionalBufferState_t));

    AuxBuffers[CurrentAuxBufferIndex].Buffer = CurrentAuxBuffer;

    //
    // Obtain the interesting references to the buffer
    //

    CurrentAuxBuffer->ObtainDataReference(&AuxBuffers[CurrentAuxBufferIndex].BufferLength,
                                          NULL,
                                          (void **)(&AuxBuffers[CurrentAuxBufferIndex].BufferPointer),
                                          Configuration.AddressingMode);

    return CodecNoError;
}


// /////////////////////////////////////////////////////////////////////////
//
//      Function to obtain a new decode buffer.
//

CodecStatus_t   Codec_MmeAudio_c::GetTranscodeBuffer()
{
    BufferPool_t   Tfp;
    //Buffer        Structure_t        BufferStructure;
    //
    // Get a buffer
    //
    CodecStatus_t Status = GetTranscodedFrameBufferPool(&Tfp);
    if (Status != CodecNoError)
    {
        SE_ERROR("(%s) - Failed to obtain the transcoded buffer pool instance\n", Configuration.CodecName);
        return Status;
    }

    if (CurrentTranscodeBuffer)
    {
        CurrentTranscodeBuffer->DecrementReferenceCount();
        CurrentTranscodeBuffer = NULL;
    }

    BufferStatus_t BufferStatus = Tfp->GetBuffer(&CurrentTranscodeBuffer, IdentifierCodec,
                                                 Configuration.TranscodedFrameMaxSize, false);
    if (BufferStatus != BufferNoError)
    {
        SE_ERROR("(%s) - Failed to obtain a transcode buffer from the transcoded buffer pool\n", Configuration.CodecName);
        Tfp->Dump(DumpPoolStates | DumpBufferStates);
        return CodecError;
    }

    //
    // Map it and initialize the mapped entry.
    //
    CurrentTranscodeBuffer->GetIndex(&CurrentTranscodeBufferIndex);

    if (CurrentTranscodeBufferIndex >= AUDIO_DECODER_TRANSCODE_BUFFER_COUNT)
    {
        SE_ERROR("(%s) - Transcode buffer index >= DTSHD_TRANSCODE_BUFFER_COUNT - Implementation error\n", Configuration.CodecName);
        return CodecError;
    }

    memset(&TranscodedBuffers[CurrentTranscodeBufferIndex], 0, sizeof(AdditionalBufferState_t));
    TranscodedBuffers[CurrentTranscodeBufferIndex].Buffer = CurrentTranscodeBuffer;
    //
    // Obtain the interesting references to the buffer
    //
    CurrentTranscodeBuffer->ObtainDataReference(&TranscodedBuffers[CurrentTranscodeBufferIndex].BufferLength,
                                                NULL,
                                                (void **)(&TranscodedBuffers[CurrentTranscodeBufferIndex].BufferPointer),
                                                Configuration.AddressingMode);

    return CodecNoError;
}


// /////////////////////////////////////////////////////////////////////////
//
//      The get coded frame buffer pool fn
//

CodecStatus_t   Codec_MmeAudio_c::GetTranscodedFrameBufferPool(BufferPool_t *Tfp)
{
    allocator_status_t      AStatus;

    //
    // If we haven't already created the buffer pool, do it now.
    //

    if (TranscodedFramePool == NULL)
    {
        //
        // Coded frame buffer type
        //
        CodecStatus_t Status = InitializeDataType(&InitialTranscodedFrameBufferDescriptor, &TranscodedFrameBufferType, &TranscodedFrameBufferDescriptor);
        if (Status != CodecNoError)
        {
            return Status;
        }

        //
        // Get the memory and Create the pool with it
        //
        BufferManager = Player->GetBufferManager();
        AStatus = PartitionAllocatorOpen(&TranscodedFrameMemoryDevice,
                                         Configuration.TranscodedMemoryPartitionName,
                                         AUDIO_DECODER_TRANSCODE_BUFFER_COUNT *
                                         Configuration.TranscodedFrameMaxSize,
                                         MEMORY_AUDIO_ACCESS);

        if (AStatus != allocator_ok)
        {
            SE_ERROR("Failed to allocate memory for %s\n", Configuration.CodecName);
            return PlayerInsufficientMemory;
        }

        TranscodedFrameMemory[CachedAddress]         = AllocatorUserAddress(TranscodedFrameMemoryDevice);
        TranscodedFrameMemory[PhysicalAddress]       = AllocatorPhysicalAddress(TranscodedFrameMemoryDevice);

        //
        BufferStatus_t BufferStatus = BufferManager->CreatePool(&TranscodedFramePool,
                                                                TranscodedFrameBufferType,
                                                                AUDIO_DECODER_TRANSCODE_BUFFER_COUNT,
                                                                AUDIO_DECODER_TRANSCODE_BUFFER_COUNT *
                                                                Configuration.TranscodedFrameMaxSize,
                                                                TranscodedFrameMemory);
        if (BufferStatus != BufferNoError)
        {
            SE_ERROR("(%s) - Failed to create the pool\n", Configuration.CodecName);
            return CodecError;
        }

        ((PlayerStream_c *)Stream)->TranscodedFrameBufferType = TranscodedFrameBufferType;
    }

    *Tfp = TranscodedFramePool;
    return CodecNoError;
}


// /////////////////////////////////////////////////////////////////////////
//
//      Function to obtain a new compressed frame buffers for N Scatter Pages.
//

CodecStatus_t   Codec_MmeAudio_c::GetCompressedFrameBuffer(int32_t NoOfCompressedBufferToGet)
{
    BufferPool_t             Cfp; // Compressed Frame Pool
    NoOfCompressedFrameBuffers = 0;
    //Buffer        Structure_t        BufferStructure;

    //
    // Get a buffer
    //
    if (NoOfCompressedBufferToGet > AUDIO_DECODER_COMPRESSED_FRAME_BUFFER_SCATTER_PAGES)
    {
        SE_ERROR("(%s) - NoOfCompressedBufferToGet is more than Pages allocated for Compressed Frame\n", Configuration.CodecName);
        return CodecError;
    }

    CodecStatus_t Status = GetCompressedFrameBufferPool(&Cfp);
    if (Status != CodecNoError)
    {
        SE_ERROR("(%s) - Failed to obtain the CompressedFrame buffer pool instance\n", Configuration.CodecName);
        return Status;
    }

    for (int32_t i = 0; i < NoOfCompressedBufferToGet; i++)
    {
        if (CurrentCompressedFrameBuffer[i])
        {
            CurrentCompressedFrameBuffer[i]->DecrementReferenceCount();
            CurrentCompressedFrameBuffer[i] = NULL;
        }


        BufferStatus_t BufferStatus = Cfp->GetBuffer(&CurrentCompressedFrameBuffer[i], IdentifierCodec, Configuration.CompressedFrameMaxSize, false);
        if (BufferStatus != BufferNoError)
        {
            SE_ERROR("(%s) - Failed to obtain a CompressedFrame buffer from the CompressedFramed buffer pool\n", Configuration.CodecName);
            Cfp->Dump(DumpPoolStates | DumpBufferStates);
            return CodecError;
        }

        //
        // Map and initialize the 1st Compressed Frame buffer mapped entry.
        //
        CurrentCompressedFrameBuffer[i]->GetIndex((unsigned int *) &CurrentCompressedFrameBufferIndex[i]);

        if (CurrentCompressedFrameBufferIndex[i] >= AUDIO_DECODER_COMPRESSED_FRAME_BUFFER_COUNT)
        {
            SE_ERROR("(%s) - buffer index >= AUDIO_DECODER_COMPRESSED_FRAME_BUFFER_COUNT - Implementation error\n", Configuration.CodecName);
        }

        memset(&CompressedFrameBuffers[i][CurrentCompressedFrameBufferIndex[i]], 0, sizeof(AdditionalBufferState_t));
        CompressedFrameBuffers[i][CurrentCompressedFrameBufferIndex[i]].Buffer = CurrentCompressedFrameBuffer[i];
        //
        // Obtain the interesting references to the buffer
        //
        CurrentCompressedFrameBuffer[i]->ObtainDataReference(&CompressedFrameBuffers[i][CurrentCompressedFrameBufferIndex[i]].BufferLength,
                                                             NULL,
                                                             (void **)(&CompressedFrameBuffers[i][CurrentCompressedFrameBufferIndex[i]].BufferPointer),
                                                             Configuration.AddressingMode);
        NoOfCompressedFrameBuffers++;
    }

    return CodecNoError;
}


// /////////////////////////////////////////////////////////////////////////
//
//      The get compressed frame buffer pool fn
//

CodecStatus_t   Codec_MmeAudio_c::GetCompressedFrameBufferPool(BufferPool_t *Cfp)
{
    allocator_status_t      AStatus;

    //
    // If we haven't already created the buffer pool, do it now.
    //

    if (CompressedFramePool == NULL)
    {
        //
        // compressed frame buffer type
        //
        CodecStatus_t Status = InitializeDataType(&InitialCompressedFrameBufferDescriptor, &CompressedFrameBufferType, &CompressedFrameBufferDescriptor);
        if (Status != CodecNoError)
        {
            return Status;
        }

        //
        // Get the memory and Create the pool with it
        //
        BufferManager = Player->GetBufferManager();

        AStatus = PartitionAllocatorOpen(&CompressedFrameMemoryDevice,
                                         Configuration.TranscodedMemoryPartitionName,
                                         AUDIO_DECODER_COMPRESSED_FRAME_BUFFER_COUNT *
                                         Configuration.CompressedFrameMaxSize,
                                         MEMORY_AUDIO_ACCESS);

        if (AStatus != allocator_ok)
        {
            SE_ERROR("Failed to allocate memory for %s\n", Configuration.CodecName);
            return PlayerInsufficientMemory;
        }

        CompressedFrameMemory[CachedAddress]         = AllocatorUserAddress(CompressedFrameMemoryDevice);
        CompressedFrameMemory[PhysicalAddress]       = AllocatorPhysicalAddress(CompressedFrameMemoryDevice);

        BufferStatus_t BufferStatus = BufferManager->CreatePool(&CompressedFramePool,
                                                                CompressedFrameBufferType,
                                                                AUDIO_DECODER_COMPRESSED_FRAME_BUFFER_COUNT,
                                                                AUDIO_DECODER_COMPRESSED_FRAME_BUFFER_COUNT *
                                                                Configuration.CompressedFrameMaxSize,
                                                                CompressedFrameMemory);

        if (BufferStatus != BufferNoError)
        {
            SE_ERROR("(%s) - Failed to create the pool\n", Configuration.CodecName);
            return CodecError;
        }

        ((PlayerStream_c *)Stream)->CompressedFrameBufferType = CompressedFrameBufferType;
    }

    *Cfp = CompressedFramePool;
    return CodecNoError;
}

// /////////////////////////////////////////////////////////////////////////
//
//      Function to obtain a new Metadata buffer
//
CodecStatus_t   Codec_MmeAudio_c::GetMetadataBuffer()
{
    BufferPool_t             MDBufPool;

    // Get a buffer
    CodecStatus_t Status = GetMetadataBufferPool(&MDBufPool);

    if (Status != CodecNoError)
    {
        SE_ERROR("%s- Failed to obtain the Metadata Buffer Pool instance\n", Configuration.CodecName);
        return Status;
    }

    if (CurrentMDBuffer)
    {
        SE_EXTRAVERB(group_decoder_audio, "CurrentMDBuffer(%p) DecrementReferenceCount\n", CurrentMDBuffer);
        CurrentMDBuffer->DecrementReferenceCount();
        CurrentMDBuffer = NULL;
    }

    if (MDBufPool->GetBuffer(&CurrentMDBuffer, IdentifierCodec, sizeof(tMetadata), false) != BufferNoError)
    {
        SE_ERROR("%s- Failed to obtain a MD buffer from the MD buffer pool\n", Configuration.CodecName);
        MDBufPool->Dump(DumpPoolStates | DumpBufferStates);

        return CodecError;
    }

    //
    // Map it and initialize the mapped entry.
    //

    CurrentMDBuffer->GetIndex(&CurrentMDBufferIndex);

    if (CurrentMDBufferIndex >= AUDIO_DECODER_METADATA_BUFFER_COUNT)
    {
        SE_ERROR("%s-MD buffer index >= AUDIO_DECODER_METADATA_BUFFER_COUNT\n",
                 Configuration.CodecName);
        CurrentMDBuffer->DecrementReferenceCount();
        return CodecError;
    }

    memset(&MDBuffers[CurrentMDBufferIndex], 0x00, sizeof(AdditionalBufferState_t));

    MDBuffers[CurrentMDBufferIndex].Buffer = CurrentMDBuffer;

    //
    // Obtain the interesting references to the buffer
    //

    CurrentMDBuffer->ObtainDataReference(&MDBuffers[CurrentMDBufferIndex].BufferLength,
                                         NULL,
                                         (void **)(&MDBuffers[CurrentMDBufferIndex].BufferPointer),
                                         Configuration.AddressingMode);


    return CodecNoError;
}

// /////////////////////////////////////////////////////////////////////////
//
//      Get Metadata buffer pool function
//

CodecStatus_t   Codec_MmeAudio_c::GetMetadataBufferPool(BufferPool_t *MDBufPool)
{
    BufferType_t            MDBufferType;

    //
    // If we haven't already created the buffer pool, do it now.
    //

    if (MDFramePool == NULL)
    {
        BufferDataDescriptor_t  *MDFrameBufferDescriptor;

        // Initialize Metadata frame buffer type
        CodecStatus_t Status = InitializeDataType(&InitialMetadataBufferDescriptor,
                                                  &MDBufferType,
                                                  &MDFrameBufferDescriptor);

        if (Status != CodecNoError)
        {
            SE_ERROR("Failed to InitializeDataType\n");
            return Status;
        }

        //
        // Get the memory and Create the pool with it
        //
        BufferManager = Player->GetBufferManager();
        allocator_status_t AStatus = PartitionAllocatorOpen(&MDFrameMemoryDevice,
                                                            Configuration.AncillaryMemoryPartitionName,
                                                            AUDIO_DECODER_METADATA_BUFFER_COUNT * sizeof(tMetadata),
                                                            MEMORY_AUDIO_ACCESS);

        if (AStatus != allocator_ok)
        {
            SE_ERROR("Failed to allocate Metadata Buffer Pool memory for %s\n", Configuration.CodecName);
            return PlayerInsufficientMemory;
        }

        MDFrameMemory[CachedAddress]         = AllocatorUserAddress(MDFrameMemoryDevice);
        MDFrameMemory[PhysicalAddress]       = AllocatorPhysicalAddress(MDFrameMemoryDevice);

        BufferStatus_t bStatus = BufferManager->CreatePool(&MDFramePool,
                                                           MDBufferType,
                                                           AUDIO_DECODER_METADATA_BUFFER_COUNT,
                                                           AUDIO_DECODER_METADATA_BUFFER_COUNT * sizeof(tMetadata),
                                                           MDFrameMemory);

        if (bStatus != BufferNoError)
        {
            SE_ERROR("(%s) - Failed to create the Metadata Buffer pool\n", Configuration.CodecName);
            AllocatorClose(&MDFrameMemoryDevice);
            return CodecError;
        }

        // Record BufferType within Stream struct to allow mixer to retrieve MD Buffers
        SE_DEBUG(group_decoder_audio, "MDBufferType:0x%X\n", MDBufferType);
        ((PlayerStream_c *)Stream)->MDBufferType = MDBufferType;
    }

    *MDBufPool = MDFramePool;
    return CodecNoError;
}

void  Codec_MmeAudio_c::SetAudioCodecDecStatistics()
{
    // Report the exact AudioMode from the coded stream instead of the AudioMode after pcm-processings
    // but report the SamplingFrequency of the transform output so that it relates to the NbOutSamples,
    // since there is no DecNbOutSamples
    Stream->Statistics().CodecAudioNumOfOutputSamples = AudioDecoderStatus.NbOutSamples;
    Stream->Statistics().CodecAudioCodingMode = AudioDecoderStatus.DecAudioMode;
    Stream->Statistics().CodecAudioSamplingFrequency = StmSeTranslateDiscreteSamplingFrequencyToInteger(AudioDecoderStatus.SamplingFreq);

    // generic handle of status for all codecs
    // Incase of EOF we amy receive muted frame with error status in callback of transfrom command when no Input is available so don't report this error
    if ((AudioDecoderStatus.DecStatus) && (AudioDecoderStatus.PTSflag.Bits.FrameType != STREAMING_DEC_EOF))
    {
        Stream->Statistics().FrameDecodeError++;
    }
}
///////////////////////////////////////////////////////////////////////////
///
///
///
CodecStatus_t   Codec_MmeAudio_c::ConvertFreqToAccCode(int aFrequency, enum eAccFsCode &aAccFreqCode)
{
    // Local variables.
    int retValue = StmSeTranslateIsoSamplingFrequencyToDiscrete((uint32_t) aFrequency, aAccFreqCode);

    if (retValue == -EINVAL)
    {
        // Convert aFrequency to ACC id frequency.
        aAccFreqCode = ACC_FS44k; // Default value.
        SE_ERROR("Bad frequency value. There is no ACC frequency id that corresponds to %d\n", aFrequency);
        return CodecError;
    }

    return CodecNoError;
}

void  Codec_MmeAudio_c::SetAudioCodecDecAttributes()
{
    PlayerAttributeDescriptor_t *Value;
    /* set "sample_frequency" */
    Value = &Stream->Attributes().sample_frequency;
    enum eAccFsCode SamplingFreqCode = (enum eAccFsCode) AudioDecoderStatus.SamplingFreq;
    Value->Id        = SYSFS_ATTRIBUTE_ID_INTEGER;

    if (SamplingFreqCode < ACC_FS_reserved)
    {
        Value->u.Int = (int) StmSeTranslateDiscreteSamplingFrequencyToInteger(SamplingFreqCode);
    }
    else
    {
        Value->u.Int = 0;
    }

    /* set "number_channels" */
    Value = &Stream->Attributes().number_channels;
#define C(f,b) case ACC_MODE ## f ## b: Value->u.ConstCharPointer = #f "/" #b ".0"; break; case ACC_MODE ## f ## b ## _LFE: Value->u.ConstCharPointer = #f "/" #b ".1"; break
#define Cn(f,b) case ACC_MODE ## f ## b: Value->u.ConstCharPointer = #f "/" #b ".0"; break
#define Ct(f,b) case ACC_MODE ## f ## b ## t: Value->u.ConstCharPointer = #f "/" #b ".0"; break; case ACC_MODE ## f ## b ## t_LFE: Value->u.ConstCharPointer = #f "/" #b ".1"; break
    Value->Id = SYSFS_ATTRIBUTE_ID_CONSTCHARPOINTER;

    switch ((enum eAccAcMode)(AudioDecoderStatus.DecAudioMode))
    {
        Ct(2, 0);
        C(1, 0);
        C(2, 0);
        C(3, 0);
        C(2, 1);
        C(3, 1);
        C(2, 2);
        C(3, 2);
        C(2, 3);
        C(3, 3);
        C(2, 4);
        C(3, 4);
        Cn(4, 2);
        Cn(4, 4);
        Cn(5, 2);
        Cn(5, 3);

    default:
        Value->u.ConstCharPointer = "UNKNOWN";
    }
}

void Codec_MmeAudio_c::CheckAudioParameterEvent(MME_LxAudioDecoderFrameStatus_t *DecFrameStatus, MME_PcmProcessingFrameExtStatus_t *PcmExtStatus)
{
    PlayerStatus_t   myStatus;
    enum eAccFsCode  receivedDecSamplingFreq;
    enum eAccBoolean receivedEmphasis;
    PlayerEventRecord_t  *myEvent = &mEvent;
    stm_se_play_stream_audio_parameters_t *newAudioParametersValues = &mNewAudioParametersValues;

    mDecodedAudioMode.Update((enum eAccAcMode) DecFrameStatus->DecAudioMode);
    receivedDecSamplingFreq  = (enum eAccFsCode) DecFrameStatus->DecSamplingFreq;
    receivedEmphasis         = (enum eAccBoolean) DecFrameStatus->Emphasis;
    // Update newAudioParametersValues for common codec parameters (specific codec parameters have been already updated in the calling function)
    // audio_coding_type parameter initialized with codec name in each codec constructor
    // we just need to retrieve the previous value
    // special case for spdfin codec
    newAudioParametersValues->audio_coding_type = AudioParametersEvents.audio_coding_type;

    // get & convert sampling_freq parameter
    if (receivedDecSamplingFreq < ACC_FS_reserved)
    {
        newAudioParametersValues->sampling_freq = StmSeTranslateDiscreteSamplingFrequencyToInteger(receivedDecSamplingFreq);
    }
    else
    {
        newAudioParametersValues->sampling_freq = 0;
        SE_ERROR("Invalid DecSamplingFreq value received\n");
    }

    // get & convert num_channels parameter
    newAudioParametersValues->num_channels       = mDecodedAudioMode.GetNbChannels();
    mDecodedAudioMode.GetChannelAssignment(&newAudioParametersValues->channel_assignment);
    // get & convert dual_mono parameter
    newAudioParametersValues->dual_mono          = mDecodedAudioMode.IsDualMono();
    // get bitrate and copyright parameters
    GetAudioDecodedStreamInfo(PcmExtStatus, newAudioParametersValues);
    if (newAudioParametersValues->bitrate != AudioParametersEvents.bitrate)
    {
        SE_DEBUG(group_decoder_audio, "AudioParametersEvent not sending due to bitrate update from %d to %d\n", AudioParametersEvents.bitrate, newAudioParametersValues->bitrate);
        AudioParametersEvents.bitrate = newAudioParametersValues->bitrate;
    }
    // get emphasis parameter
    newAudioParametersValues->emphasis = (ACC_MME_TRUE == receivedEmphasis) ? 1 : 0;

    // check if there was an update
    if (0 != memcmp(&AudioParametersEvents, newAudioParametersValues, sizeof(stm_se_play_stream_audio_parameters_t)))
    {
        SE_DEBUG(group_decoder_audio, "AudioParametersEvents updated\n");
        // update AudioParametersEvents with new values
        AudioParametersEvents = *newAudioParametersValues;
        SE_DEBUG(group_decoder_audio,
                 "<New EventSourceAudioParameters>: AudioCodingType=%d SamplingFreq=%d NumChannels=%d DualMono=%d ChannelAssign[%d-%d-%d-%d-%d malleable(%d)] Bitrate=%d Copyright=%d Emphasis=%d\n",
                 AudioParametersEvents.audio_coding_type,
                 AudioParametersEvents.sampling_freq,
                 AudioParametersEvents.num_channels,
                 AudioParametersEvents.dual_mono,
                 AudioParametersEvents.channel_assignment.pair0,
                 AudioParametersEvents.channel_assignment.pair1,
                 AudioParametersEvents.channel_assignment.pair2,
                 AudioParametersEvents.channel_assignment.pair3,
                 AudioParametersEvents.channel_assignment.pair4,
                 AudioParametersEvents.channel_assignment.malleable,
                 AudioParametersEvents.bitrate,
                 AudioParametersEvents.copyright,
                 AudioParametersEvents.emphasis
                );
        // identify the event
        myEvent->Code      = EventSourceAudioParametersChange;
        myEvent->Playback  = Playback;
        myEvent->Stream    = Stream;
        // set event common codec parameters
        // audio_coding_type, sampling_freq, num_channels, dual_mono
        myEvent->Value[0].UnsignedInt = AudioParametersEvents.audio_coding_type;
        myEvent->Value[1].UnsignedInt = AudioParametersEvents.sampling_freq;
        myEvent->Value[2].UnsignedInt = AudioParametersEvents.num_channels;
        myEvent->Value[3].UnsignedInt = AudioParametersEvents.dual_mono;
        //channel_assignment
        myEvent->Value[4].UnsignedInt = AudioParametersEvents.channel_assignment.pair0;
        myEvent->Value[5].UnsignedInt = AudioParametersEvents.channel_assignment.pair1;
        myEvent->Value[6].UnsignedInt = AudioParametersEvents.channel_assignment.pair2;
        myEvent->Value[7].UnsignedInt = AudioParametersEvents.channel_assignment.pair3;
        myEvent->Value[8].UnsignedInt = AudioParametersEvents.channel_assignment.pair4;
        myEvent->Value[9].UnsignedInt = AudioParametersEvents.channel_assignment.malleable;
        // birate, copyright, emphasis
        myEvent->Value[10].UnsignedInt = AudioParametersEvents.bitrate;
        myEvent->Value[11].UnsignedInt = AudioParametersEvents.copyright;
        myEvent->Value[12].UnsignedInt = AudioParametersEvents.emphasis;
        // codec specific parameters have been already updated in the calling function : myEvent->ExtValue[]
        myStatus            = Stream->SignalEvent(myEvent);

        if (myStatus != PlayerNoError)
        {
            SE_ERROR("bug during SignalEvent treatment\n");
        }
    }
}

//////////////////////////////////////////////////////////////////////////////////////////////////
///
/// Decoding of the structure MME_PcmProcessingFrameExtStatus_t to get MME_LxAudioProcessInfo_t
/// Used to get bitrate & copyright info
/// (MME_LxAudioProcessInfo_t is provided for all codecs)
///
void Codec_MmeAudio_c::GetAudioDecodedStreamInfo(MME_PcmProcessingFrameExtStatus_t *PcmExtStatus, stm_se_play_stream_audio_parameters_t *audioparams)
{
    MME_PcmProcessingStatusTemplate_t *Pcmstatus;
    MME_LxAudioProcessInfo_t *AudDecStreamInfo_p;
    U32 SizeofPCMStatus = PcmExtStatus->BytesUsed;
    U32 ByteUsed;

    for (ByteUsed = 0; ByteUsed < SizeofPCMStatus;)
    {
        Pcmstatus = (MME_PcmProcessingStatusTemplate_t *)((U8 *) & (PcmExtStatus->PcmStatus) + ByteUsed);

        if ((Pcmstatus->Id >> 8)  == ACC_PARSER_MT)
        {
            if ((Pcmstatus->Id == 0) || (Pcmstatus->StructSize > (SizeofPCMStatus - ByteUsed)))
            {
                ByteUsed = SizeofPCMStatus;
            }
            else
            {
                ByteUsed += Pcmstatus->StructSize;
                AudDecStreamInfo_p = (MME_LxAudioProcessInfo_t *)Pcmstatus;
                audioparams->bitrate   = (int) AudDecStreamInfo_p->BitRate;
                audioparams->copyright = (bool) AudDecStreamInfo_p->IsCopyright;
            }
        }
        else
        {
            ByteUsed += Pcmstatus->StructSize;
        }
    }
}


///////////////////////////////////////////////////////////////////////////
///
/// Disable the transcoding in case of dual decode for MS11/MS12 mode.
/// The enabling will depend on the specfic codec type
///
void   Codec_MmeAudio_c::DisableTranscodingBasedOnProfile()
{
    unsigned int AudioServiceType = Player->PolicyValue(Playback, Stream, PolicyAudioServiceType);
    unsigned int AudioApplicationType = Player->PolicyValue(Playback, Stream, PolicyAudioApplicationType);
    PlayerStream_t SecondaryStream = Playback->GetSecondaryStream();

    if ((NULL != SecondaryStream) || ((NULL == SecondaryStream) && (PolicyValueAudioServiceMainAndAudioDescription == AudioServiceType)))
    {
        // We have a secondary stream and the current stream also doesn't do main and AD decoding
        // Or we have only a single stream but it does dual decode in case of policy set to
        //    PolicyValueAudioServiceMainAndAudioDescription
        // So we have a dual decode scenario
        // do we have an MS11/MS12 use case?
        switch (AudioApplicationType)
        {
        case PolicyValueAudioApplicationMS11:
        case PolicyValueAudioApplicationMS12:
            // We don't do transcoding in this case
            TranscodeEnable = false;
            break;

        default:
            break;
        }
    }

    // If the surrent stream is secondary, then disable transcoding anyway
    switch (AudioServiceType)
    {
    case STM_SE_CTRL_VALUE_AUDIO_SERVICE_SECONDARY:
    case STM_SE_CTRL_VALUE_AUDIO_SERVICE_AUDIO_DESCRIPTION:
        TranscodeEnable = false;
        break;

    default:
        break;
    }

    SE_DEBUG(group_decoder_audio, "Stream[%p]: transcode= %d, serv:%u,app:%u\n", Stream, TranscodeEnable, AudioServiceType, AudioApplicationType);
}

////////////////////////////////////////////////////////////////////////////
///
/// Enable the AuxBuffer in case of  MS11 single decode mode.
/// The enabling will depend on the specfic codec type
///
void   Codec_MmeAudio_c::EnableAuxBufferBasedOnProfile()
{
    unsigned int AudioServiceType = Player->PolicyValue(Playback, Stream, PolicyAudioServiceType);
    unsigned int AudioApplicationType = Player->PolicyValue(Playback, Stream, PolicyAudioApplicationType);
    PlayerStream_t SecondaryStream = Playback->GetSecondaryStream();
    // Reset the AuxOutputEnable flag for runtime handling of the AuxBuffer (eg: When playback mode changes from
    // Single Decode to Dual Decode)
    AuxOutputEnable = false;

    if ((NULL == SecondaryStream) && (PolicyValueAudioServiceMainAndAudioDescription != AudioServiceType))
    {
        switch (AudioApplicationType)
        {
        case PolicyValueAudioApplicationMS11:
            // Enable the Aux Output in this case
            AuxOutputEnable = true;
            break;
        default:
            break;
        }
    }

    SE_DEBUG(group_decoder_audio, "Stream[%p]: auxbufferenable:%d, serv:%u,app:%u\n", Stream, TranscodeEnable, AudioServiceType, AudioApplicationType);
}

///////////////////////////////////////////////////////////////////////////
///
/// Downmix promotion evaluation
///
void Codec_MmeAudio_c::EvaluateImplicitDownmixPromotion()
{
    stm_se_audio_channel_assignment_t channelAssignment;
    PlayerStatus_t playerResult = Player->GetControl(Playback, Stream, STM_SE_CTRL_SPEAKER_CONFIG, (void *) &channelAssignment);
    unsigned int AudioServiceType = Player->PolicyValue(Playback, Stream, PolicyAudioServiceType);
    unsigned int AudioApplicationType = Player->PolicyValue(Playback, Stream, PolicyAudioApplicationType);
    PlayerStream_t SecondaryStream = Playback->GetSecondaryStream();
    bool IsSingleDecode = (NULL == SecondaryStream) && (PolicyValueAudioServiceMainAndAudioDescription != AudioServiceType) ? true : false;

    if (PlayerMatchNotFound == playerResult)
    {
        // no STM_SE_CTRL_SPEAKER_CONFIG control applied to this playstream,
        // Query all connected Manifestors for their expected ChannelConfiguration
        // If all Manifestors ChannelConfiguration are identical,
        // we can perform the downmix/upmix operation at codec level
        enum eAccAcMode AcMode, FinalAcMode = ACC_MODE_ID, FinalAuxAcMode = ACC_MODE_ID;
        bool FirstManifestor = true;

        Stream->GetHavanaStream()->LockManifestors();

        Manifestor_t *manifestorArray = Stream->GetHavanaStream()->GetManifestors();

        for (int i = 0; i < MAX_MANIFESTORS; i++)
        {
            if (manifestorArray[i] != NULL)
            {
                unsigned int capabilities;
                manifestorArray[i]->GetCapabilities(&capabilities);

                if (capabilities & MANIFESTOR_CAPABILITY_ENCODE)
                {
                    class Manifestor_EncodeAudio_c *manifestorEncAudio = NULL;
                    manifestorEncAudio = (class Manifestor_EncodeAudio_c *) manifestorArray[i];
                    manifestorEncAudio->GetChannelConfiguration(&AcMode);
                }
                else if ((capabilities & MANIFESTOR_CAPABILITY_DISPLAY) || (capabilities & MANIFESTOR_CAPABILITY_GRAB))
                {
                    class Manifestor_Audio_c *manifestorAudio = NULL;
                    manifestorAudio = (class Manifestor_Audio_c *) manifestorArray[i];
                    manifestorAudio->GetChannelConfiguration(&AcMode);
                }
                else if (capabilities & MANIFESTOR_CAPABILITY_SOURCE)
                {
                    class Manifestor_AudioSrc_c *manifestorAudioSrc;
                    manifestorAudioSrc = (class Manifestor_AudioSrc_c *) manifestorArray[i];
                    manifestorAudioSrc->GetChannelConfiguration(&AcMode);
                }
                else
                {
                    // This is an unknown manifestor
                    // So lets use ACC_MODE_ID as AcMode for this manifest.
                    AcMode = ACC_MODE_ID;
                    SE_DEBUG(group_decoder_audio, "Unknown capability[%d] for manifestor[%d]. Setting AcMode:%s\n", capabilities, i, StmSeAudioAcModeGetName(AcMode));
                }

                SE_DEBUG(group_decoder_audio, "manifestor[%d] AccAcMode:%s\n", i, StmSeAudioAcModeGetName(AcMode));

                if (FirstManifestor)
                {
                    FirstManifestor = false;
                    FinalAcMode = AcMode;
                }
                else if (FinalAcMode != AcMode)
                {
                    // The following condition populates the OutmodeAux variable with the acmode of that manifestor which has stereo output
                    // channel configuration in case there are multiple manifestors with different audio coding modes
                    if (FinalAuxAcMode == ACC_MODE_ID)
                    {
                        if (FinalAcMode == ACC_MODE20 || FinalAcMode == ACC_MODE20t)
                        {
                            FinalAuxAcMode = FinalAcMode;
                        }
                        else if (AcMode == ACC_MODE20 || AcMode == ACC_MODE20t)
                        {
                            FinalAuxAcMode = AcMode;
                        }
                    }
                    FinalAcMode = ACC_MODE_ID;
                    if (FinalAuxAcMode != ACC_MODE_ID)
                    {
                        // Atleast one stereo manifestor found
                        if (AcMode == ACC_MODE20 || AcMode == ACC_MODE20t)
                        {
                            if (FinalAuxAcMode != AcMode)
                            {
                                // Atleast one manifestor has a different stereo mode than the first stereo manifestor
                                FinalAuxAcMode = ACC_MODE_ID;
                                break;
                            }
                        }
                    }
                }
            }
        }

        Stream->GetHavanaStream()->UnlockManifestors();

        FinalAuxAcMode = IsSingleDecode && (PolicyValueAudioApplicationMS11 == AudioApplicationType) ? FinalAuxAcMode : ACC_MODE_ID;

        if (FinalAcMode != OutmodeMain)
        {
            SE_DEBUG(group_decoder_audio, "Change Channel Config to:%s\n", StmSeAudioAcModeGetName(FinalAcMode));
            OutmodeMain = FinalAcMode;
            ForceStreamParameterReload = true;
        }

        if (FinalAuxAcMode != OutmodeAux)
        {
            SE_DEBUG(group_decoder_audio, "Change Aux Channel Config to:%s\n", StmSeAudioAcModeGetName(FinalAuxAcMode));
            OutmodeAux = FinalAuxAcMode;
            ForceStreamParameterReload = true;
        }
    }
    else
    {
        SE_DEBUG(group_decoder_audio,  "codec OutmodeMain already set by playstream\n");
    }
}

///////////////////////////////////////////////////////////////////////////
///
/// Evaluate the need to generate a Transcoded Buffer and Compressed Buffer
/// This method is responsible to update TranscodeNeeded and CompressedFrameNeeded members.
///
void Codec_MmeAudio_c::EvaluateTranscodeCompressedNeeded()
{
    bool IsTranscodeNeeded       = false;
    bool IsCompressedFrameNeeded = false;

    Stream->GetHavanaStream()->LockManifestors();

    Manifestor_t *manifestorArray = Stream->GetHavanaStream()->GetManifestors();
    for (int i = 0; i < MAX_MANIFESTORS; i++)
    {
        if (manifestorArray[i] != NULL)
        {
            unsigned int capabilities;
            manifestorArray[i]->GetCapabilities(&capabilities);

            if (capabilities & MANIFESTOR_CAPABILITY_DISPLAY)
            {
                class Manifestor_Audio_c *manifestorAudio = NULL;
                manifestorAudio = (class Manifestor_Audio_c *) manifestorArray[i];
                IsTranscodeNeeded |= manifestorAudio->IsTranscodeNeeded();
                IsCompressedFrameNeeded |=  manifestorAudio->IsCompressedFrameNeeded();
            }
        }
    }

    Stream->GetHavanaStream()->UnlockManifestors();

    if (TranscodeNeeded != IsTranscodeNeeded)
    {
        SE_DEBUG(group_decoder_audio, "Change to TranscodeNeeded=%s\n", IsTranscodeNeeded ? "true" : "false");
        TranscodeNeeded = IsTranscodeNeeded;
        ForceStreamParameterReload = true;
    }
    if (CompressedFrameNeeded != IsCompressedFrameNeeded)
    {
        SE_DEBUG(group_decoder_audio, "Change to CompressedFrameNeeded=%s\n", IsCompressedFrameNeeded ? "true" : "false");
        CompressedFrameNeeded = IsCompressedFrameNeeded;
        ForceStreamParameterReload = true;
    }
}

///////////////////////////////////////////////////////////////////////////
///
/// DRC parameters evaluation
/// Query all connected ksound_manifestors for their Mixer DRC parameters
/// in order to apply them at codec level
///
void Codec_MmeAudio_c::EvaluateMixerDRCParameters()
{
    // Query all connected Manifestors for their expected DRC parameters
    unsigned int capabilities;
    Stream->GetHavanaStream()->LockManifestors();

    Manifestor_t *manifestorArray = Stream->GetHavanaStream()->GetManifestors();

    for (int i = 0; i < MAX_MANIFESTORS; i++)
    {
        if (manifestorArray[i] != NULL)
        {
            manifestorArray[i]->GetCapabilities(&capabilities);

            if (capabilities & MANIFESTOR_CAPABILITY_DISPLAY)
            {
                DRCParams_t MixerDRCparams = DRC;
                class Manifestor_Audio_c *manifestorAudio = NULL;
                manifestorAudio = (class Manifestor_Audio_c *) manifestorArray[i];
                manifestorAudio->GetDRCParams(&MixerDRCparams);
                if (memcmp(&DRC, &MixerDRCparams, sizeof(DRC)))
                {
                    DRC = MixerDRCparams;
                    SE_DEBUG(group_decoder_audio, "Setting DRC to MAIN{Enable:%d Type:%d HDR:%d LDR:%d}\n",
                             DRC.DRC_Enable, DRC.DRC_Type, DRC.DRC_HDR, DRC.DRC_LDR);
                    ForceStreamParameterReload = true;
                }
            }
            else
            {
                SE_VERBOSE(group_decoder_audio, "Out of interests manifestor\n");
            }
        }
    }
    Stream->GetHavanaStream()->UnlockManifestors();
}

//
// low power functions
//

void Codec_MmeAudio_c::ClearInternalBufferMap()
{
    SE_DEBUG(group_decoder_audio, "\n");
    if (CompressedFrameMemoryDevice != NULL)
    {
        AllocatorRemoveMapEx(CompressedFrameMemoryDevice->UnderlyingDevice);
    }
    if (TranscodedFrameMemoryDevice != NULL)
    {
        AllocatorRemoveMapEx(TranscodedFrameMemoryDevice->UnderlyingDevice);
    }
    if (AuxFrameMemoryDevice != NULL)
    {
        AllocatorRemoveMapEx(AuxFrameMemoryDevice->UnderlyingDevice);
    }
    if (MDFrameMemoryDevice != NULL)
    {
        AllocatorRemoveMapEx((MDFrameMemoryDevice)->UnderlyingDevice);
    }
}

void Codec_MmeAudio_c::CreateInternalBufferMap()
{
    SE_DEBUG(group_decoder_audio, "\n");
    if (CompressedFrameMemoryDevice != NULL)
    {
        AllocatorCreateMapEx(CompressedFrameMemoryDevice->UnderlyingDevice);
    }
    if (TranscodedFrameMemoryDevice != NULL)
    {
        AllocatorCreateMapEx(TranscodedFrameMemoryDevice->UnderlyingDevice);
    }
    if (AuxFrameMemoryDevice != NULL)
    {
        AllocatorCreateMapEx(AuxFrameMemoryDevice->UnderlyingDevice);
    }
    if (MDFrameMemoryDevice != NULL)
    {
        AllocatorCreateMapEx((MDFrameMemoryDevice)->UnderlyingDevice);
    }
}

CodecStatus_t  Codec_MmeAudio_c::InitFrameParamsAndStatus(unsigned int DecStatusSize)
{
    // All initialization common to all audio codecs done here

    // retrieve mute status
    bool mute = false;
    PlayerStatus_t PlayerStatus = Player->GetControl(Playback, Stream, STM_SE_CTRL_PLAY_STREAM_MUTE, &mute);
    if (PlayerStatus != PlayerNoError && PlayerStatus != PlayerMatchNotFound)
    {
        SE_ERROR("Failed to get STREAM_MUTE control\n");
    }

    // Limit prl to valid range if it is outside the supported range
    unsigned int PRL = (unsigned int)Player->PolicyValue(Playback, Stream, PolicyAudioProgramReferenceLevel);
    PRL = (PRL > 31) ? 31 : PRL;

    //
    // Initialize the FrameParams at each frame
    //
    AudioCodecDecodeContext_t *Context = (AudioCodecDecodeContext_t *)DecodeContext;
    memset(&Context->Audio.FrameParams, 0, sizeof(Context->Audio.FrameParams));

    Context->Audio.FrameParams.Cmd = mute ? ACC_CMD_MUTE : ACC_CMD_PLAY;

    // Provide the PTS for frame based decoders to AudioDecoders for Delay Adjustment

    Buffer_t AttachedCodedDataBuffer;
    CurrentDecodeBuffer->ObtainAttachedBufferReference(CodedFrameBufferType, &AttachedCodedDataBuffer);
    SE_ASSERT(AttachedCodedDataBuffer != NULL);

    ParsedFrameParameters_t *ParsedFrameParams;
    AttachedCodedDataBuffer->ObtainMetaDataReference(Player->MetaDataParsedFrameParametersType,
                                                     (void **)(&ParsedFrameParams));
    SE_ASSERT(ParsedFrameParams != NULL);

    unsigned int PTSFlag = ACC_NO_PTS_DTS;
    unsigned long long PTS = 0;
    // assume FW manages PTS only in 90KHz Format for frame based; cf bz82600 for fix
    enum ePtsTimeFormat PtsTimeFormat = PtsTimeFormat90k;

    if (ParsedFrameParams->PTS.IsValid())
    {
        PTSFlag = ACC_PTS_PRESENT;
        PTS = ParsedFrameParams->PTS.PtsValue();

        SE_DEBUG(group_decoder_audio, "PTSFlag %d, PTS %lld\n", PTSFlag, PTS);
    }
    else
    {
        SE_DEBUG(group_decoder_audio, "(%s) PTS = INVALID_TIME\n", Configuration.CodecName);
    }

    Context->Audio.FrameParams.PtsFlags.Bits.PTS_DTS_FLAG  = PTSFlag;
    Context->Audio.FrameParams.PTS                         = PTS;
    Context->Audio.FrameParams.PtsFlags.Bits.PtsTimeFormat = PtsTimeFormat;
    Context->Audio.FrameParams.PtsFlags.Bits.DialogNorm    = PRL;

    //
    // Zero the reply structure
    //
    memset(&Context->DecodeStatus, 0, DecStatusSize);

    //
    // Fill out the FrameParams and FrameStatus in the current command
    //
    Context->Audio.Base.MMECommand.ParamSize                    = sizeof(Context->Audio.FrameParams);
    Context->Audio.Base.MMECommand.Param_p                      = (MME_GenericParams_t)(&Context->Audio.FrameParams);
    Context->Audio.Base.MMECommand.CmdStatus.AdditionalInfoSize = DecStatusSize;
    Context->Audio.Base.MMECommand.CmdStatus.AdditionalInfo_p   = (MME_GenericParams_t)(&Context->DecodeStatus);

    return CodecNoError;
}

CodecStatus_t Codec_MmeAudio_c::CommonStatusUpdate(CodecBaseDecodeContext_t *Context, int LfePlaybackLevel)
{
    SE_VERBOSE(group_decoder_audio, ">><<\n");
    AssertComponentState(ComponentRunning);

    if (Context == NULL)
    {
        SE_ERROR("(%s) - CodecContext is NULL\n", Configuration.CodecName);
        return CodecError;
    }

    if (AudioOutputSurface == NULL)
    {
        SE_ERROR("(%s) - AudioOutputSurface is NULL\n", Configuration.CodecName);
        return CodecError;
    }

    AudioCodecDecodeContext_t       *DecodeContext = (AudioCodecDecodeContext_t *) Context;
    MME_LxAudioDecoderFrameStatus_t *Status        = &DecodeContext->DecodeStatus.DecStatus;

    AudioDecoderStatus = *Status;

    if (Status->DecStatus & (1U << 31)) // If Error Bit is Set
    {
        SE_WARNING("Decode error (muted frame): 0x%x\n", Status->DecStatus);
        // don't report an error to the higher levels (because the frame is muted)
        // Todo :: Count the number of muted frames for statistics
    }

    // The number of decoded samples  may vary by +/-1 sample at every frame
    // Print in as debug info (should be correlated with the number evaluated
    // by the parser
    SE_DEBUG(group_decoder_audio, "Number of output samples (%d)\n", Status->NbOutSamples);

    // SYSFS
    SetAudioCodecDecStatistics();

    //
    // Attach any codec derived metadata to the output buffer (or verify the
    // frame analysis if the frame analyser already filled everything in for
    // us).
    //
    OS_LockMutex(&Lock);
    ParsedAudioParameters_t *AudioParameters = BufferState[DecodeContext->Audio.Base.BufferIndex].ParsedParameters.Audio;

    if (AudioParameters == NULL)
    {
        SE_ERROR("(%s) - AudioParameters are NULL\n", Configuration.CodecName);
        OS_UnLockMutex(&Lock);
        return CodecError;
    }

    AudioParameters->Source.BitsPerSample = 32; // Output of Audio Decoder from FW is always 32 bits

    AudioParameters->Source.ChannelCount  = AudioOutputSurface->ChannelCount;

    AudioParameters->DialogNorm           = Status->PTSflag.Bits.DialogNorm; // Accept FW value

    AudioParameters->Organisation         = Status->AudioMode; // Accept FW reported Speaker Organization

    int32_t ExpectedNspl = AudioParameters->SampleCount;         // Frame Parser provided estimate of samples
    int32_t ExpectedFreq = AudioParameters->Source.SampleRateHz; // Frame Parser provided estimate of Sample Rate

    int32_t SampleRate = StmSeTranslateDiscreteSamplingFrequencyToInteger(Status->SamplingFreq); // FW reported Sample Rate
    int32_t NbSamples  = Status->NbOutSamples;                                                   // FW reported No. of Samples

    // A lot of sanity checks follow
    if (ExpectedFreq <= 0)
    {
        // Frame Parser must never generate a frame with unknown Sampling Freq in a Frame Based decoder
        SE_WARNING("Incorrect sample rate ( %d ) reported by Parser\n", ExpectedFreq);
        ExpectedFreq = (SampleRate > 0) ? SampleRate : STM_SE_DEFAULT_AUDIO_SAMPLE_RATE_HZ; // If FW reported Sample Rate looks sane use that else fix to default
    }

    if (SampleRate <= 0)
    {
        SE_WARNING("Incorrect sample rate ( %d ) reported by FW\n", SampleRate);
        SampleRate = ExpectedFreq; // Use Parser reported Sample Rate
    }

    if (ExpectedNspl == 0)
    {
        // Frame Parser must never generate a frame with unknown Sampling Freq or NbSamples in a Frame Based decoder
        SE_WARNING("Incorrect number of samples ( %d ) reported by Parser\n", ExpectedNspl);
    }
    else if (NbSamples == 0)
    {
        SE_WARNING("Incorrect number of samples ( %d ) reported by FW, expected (%d) from Parser\n", NbSamples, ExpectedNspl);
    }

    if (mTempoSetRatio != 0 && mTempoSetRatio != -100)
    {
        // Trickmode: Number of samples depends on playback speed
        int expectedSamples = (ExpectedNspl * 100) / (100 + mTempoSetRatio);

        // We authorize the range [-2;+2] around the computed value as
        // the number of samples return by tempo Processing control can vary wihtin this range.
        if ((NbSamples < (expectedSamples - 2)) || (NbSamples > (expectedSamples + 2)))
        {
            SE_WARNING("Unexpected number of output samples:%d  input SampleCount:%d ExpectedSamples:%d Ratio:%d\n",
                       NbSamples, ExpectedNspl, expectedSamples, mTempoSetRatio);
        }
    }
    // It is possible that the FW upsamples the input as in DTS96k and DTSHDLL 192KHz/DD+ 96K/HEAAC
    // But we ensure the period of the frame remains the same as reported by frame parser
    // i.e. (expected_spl * 1000 / expected_freq) == (firmware_spl * 1000 / firmware_freq)
    else if (ExpectedNspl * SampleRate != NbSamples * ExpectedFreq) // Ratio checking modified to avoid division
    {
        SE_WARNING("Wrong ratio between expected and parsed frame properties: nb samples: %d (expected %d), freq %d (expected %d)\n",
                   NbSamples, ExpectedNspl, SampleRate, ExpectedFreq);
    }

    // Sanity on Decoder's Elapsed Time => Are we realtime ?
    int duration = (int)(NbSamples * 1000000LL / SampleRate);  //Expected Frame Duration (in us)
    if (Status->ElapsedTime / 2 > duration) //  divide by 2 to WA Ticket74328
    {
        SE_WARNING("MME command took a lot of time (%d us vs %d us)\n",
                   Status->ElapsedTime / 2, duration);
    }

    // Accept the Number of Samples and SampleRate reported by the FW, since only
    // the FW knows how much data has been written to the buffer, ignoring FW params
    // implies un-initialized buffers may be manifested.
    AudioParameters->Source.SampleRateHz = SampleRate;
    AudioParameters->SampleCount         = NbSamples;

    // Update the PTS based upon the Delay compensation reported by Audio Decoder MT
    if (ACC_isPTS_PRESENT(Status->PTSflag.Bits.PTS_DTS_FLAG))
    {
        ParsedFrameParameters_t *DecodedFrameParsedFrameParameters;
        Buffer_t TheCurrentDecodeBuffer = BufferState[DecodeContext->Audio.Base.BufferIndex].Buffer;

        TheCurrentDecodeBuffer->ObtainMetaDataReference(Player->MetaDataParsedFrameParametersReferenceType, (void **)(&DecodedFrameParsedFrameParameters));
        SE_ASSERT(DecodedFrameParsedFrameParameters != NULL);

        if (DecodedFrameParsedFrameParameters->PTS.IsValid())
        {
            stm_se_time_format_t OrigTimeFormat = DecodedFrameParsedFrameParameters->PTS.TimeFormat();
            stm_se_time_format_t FwTimeFormat   = StmSeConvertFwTimeFormatToPlayerTimeFormat((enum ePtsTimeFormat) Status->PTSflag.Bits.PtsTimeFormat);
            TimeStamp_c  fw_PTS       = TimeStamp_c(Status->PTS, FwTimeFormat);
            // Convert the FW provided value to the original PTS format
            TimeStamp_c  fw_PTS_in_OrigTimeFormat = TimeStamp_c(fw_PTS.Value(OrigTimeFormat), OrigTimeFormat);
            int64_t      diff_pts     = TimeStamp_c::DeltaUsec(fw_PTS_in_OrigTimeFormat, DecodedFrameParsedFrameParameters->PTS);

            if (diff_pts > MAXIMUM_CODEC_DELAY || diff_pts < -MAXIMUM_CODEC_DELAY)   // Sanity on Decoder Latency
            {
                // Decoder Latency seems out of bounds => Ignore FW return value
                SE_WARNING("FW reported Latency out of bounds %lld\n", diff_pts);
            }
            else
            {
                // update the new codec delay adjusted PTS
                DecodedFrameParsedFrameParameters->PTS = fw_PTS_in_OrigTimeFormat;
            }
        }
    }

    if (SE_IS_DEBUG_ON(group_decoder_audio))
    {
        SE_DEBUG(group_decoder_audio, "BitsPerSample                %d\n", AudioParameters->Source.BitsPerSample);
        SE_DEBUG(group_decoder_audio, "ChannelCount                 %d\n", AudioParameters->Source.ChannelCount);
        SE_DEBUG(group_decoder_audio, "Organisation                 %d\n", AudioParameters->Organisation);
        SE_DEBUG(group_decoder_audio, "SampleCount                  %d\n", AudioParameters->SampleCount);
        SE_DEBUG(group_decoder_audio, "Source.SampleRateHz          %d\n", AudioParameters->Source.SampleRateHz);
    }
    OS_UnLockMutex(&Lock);

    // Validate the extended status (without propagating errors)
    (void) ValidatePcmProcessingExtendedStatus(Context,
                                               (MME_PcmProcessingFrameExtStatus_t *) &DecodeContext->DecodeStatus.PcmStatus);

    //SYSFS
    SetAudioCodecDecAttributes();

    // Send a Parameter Updated Event if something changed
    SendParamsUpdatedEvent(DecodeContext);

    return CodecNoError;
}

void Codec_MmeAudio_c::SendParamsUpdatedEvent(AudioCodecDecodeContext_t *DecodeContext)
{
    if (NULL != DecodeContext)
    {
        // Evaluate a new set of Audio parameters and compare with previous to determine the requirement for an event
        memset(&mNewAudioParametersValues, 0, sizeof(mNewAudioParametersValues));

        CheckAudioParameterEvent(&DecodeContext->DecodeStatus.DecStatus, (MME_PcmProcessingFrameExtStatus_t *) &DecodeContext->DecodeStatus.PcmStatus);
    }
}
