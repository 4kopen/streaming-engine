/************************************************************************
Copyright (C) 2003-2014 STMicroelectronics. All Rights Reserved.

This file is part of the Streaming Engine.

Streaming Engine is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License version 2 as
published by the Free Software Foundation.

Streaming Engine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with Streaming Engine; see the file COPYING.  If not, write to the Free
Software Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307,
USA.

The Streaming Engine Library may alternatively be licensed under a
proprietary license from ST.
************************************************************************/

////////////////////////////////////////////////////////////////////////////
/// \class Codec_MmeAudioDra_c
///
/// The DRA audio codec proxy.
///

#include "codec_mme_audio_dra.h"
#include "codec_capabilities.h"
#include "dra_audio.h"

#undef TRACE_TAG
#define TRACE_TAG "Codec_MmeAudioDra_c"

typedef struct DraAudioCodecStreamParameterContext_s
{
    CodecBaseStreamParameterContext_t   BaseContext;

    MME_LxAudioDecoderGlobalParams_t    StreamParameters;
} DraAudioCodecStreamParameterContext_t;

#define BUFFER_DRA_AUDIO_CODEC_STREAM_PARAMETER_CONTEXT         "DraAudioCodecStreamParameterContext"
#define BUFFER_DRA_AUDIO_CODEC_STREAM_PARAMETER_CONTEXT_TYPE    {BUFFER_DRA_AUDIO_CODEC_STREAM_PARAMETER_CONTEXT, BufferDataTypeBase, AllocateFromOSMemory, 32, 0, true, true, sizeof(DraAudioCodecStreamParameterContext_t)}

static BufferDataDescriptor_t DraAudioCodecStreamParameterContextDescriptor   = BUFFER_DRA_AUDIO_CODEC_STREAM_PARAMETER_CONTEXT_TYPE;

typedef struct
{
    MME_LxAudioDecoderFrameStatus_t             DecStatus;
    MME_PcmProcessingFrameExtCommonStatus_t     PcmStatus;
} MME_LxAudioDecoderFrameExtendedDraStatus_t;

//
typedef struct DraAudioCodecDecodeContext_s
{
    AudioCodecBaseDecodeContext_t               Audio;
    MME_LxAudioDecoderFrameExtendedDraStatus_t  DecodeStatus;
} DraAudioCodecDecodeContext_t;

#define BUFFER_DRA_AUDIO_CODEC_DECODE_CONTEXT                   "DraAudioCodecDecodeContext"
#define BUFFER_DRA_AUDIO_CODEC_DECODE_CONTEXT_TYPE              {BUFFER_DRA_AUDIO_CODEC_DECODE_CONTEXT, BufferDataTypeBase, AllocateFromOSMemory, 32, 0, true, true, sizeof(DraAudioCodecDecodeContext_t)}

static BufferDataDescriptor_t DraAudioCodecDecodeContextDescriptor    = BUFFER_DRA_AUDIO_CODEC_DECODE_CONTEXT_TYPE;

///
/// Fill in the configuration parameters used by the super-class and reset everything.
///
Codec_MmeAudioDra_c::Codec_MmeAudioDra_c()
    : DecoderId(ACC_DRA_ID)
{
    Configuration.CodecName                             = "DRA audio";
    Configuration.StreamParameterContextCount           = 1;
    Configuration.StreamParameterContextDescriptor      = &DraAudioCodecStreamParameterContextDescriptor;
    Configuration.DecodeContextCount                    = 4;
    Configuration.DecodeContextDescriptor               = &DraAudioCodecDecodeContextDescriptor;
    Configuration.MaximumSampleCount                    = DRA_MAX_DECODED_SAMPLE_COUNT;

    AudioDecoderTransformCapabilityMask.DecoderCapabilityFlags = (1 << ACC_DRA);
}


////////////////////////////////////////////////////////////////////////////
///
///     Destructor function, ensures a full halt and reset
///     are executed for all levels of the class.
///
Codec_MmeAudioDra_c::~Codec_MmeAudioDra_c()
{
    Halt();
}

////////////////////////////////////////////////////////////////////////////
///
/// Populate the supplied structure with parameters for DRA audio.
///

CodecStatus_t Codec_MmeAudioDra_c::FillOutTransformerGlobalParameters(MME_LxAudioDecoderGlobalParams_t *GlobalParams_p)
{
    SE_INFO(group_decoder_audio, "Initializing DRA %s audio decoder\n",
            ((DecoderId == ACC_DRA_ID) ? "DRA decoder" :
             "unknown"));

    MME_LxAudioDecoderGlobalParams_t &GlobalParams = *GlobalParams_p;
    GlobalParams.StructSize = sizeof(MME_LxAudioDecoderGlobalParams_t);

    MME_LxDRAConfig_t &Config = *((MME_LxDRAConfig_t *) GlobalParams.DecConfig);
    Config.StructSize                      = sizeof(MME_LxDRAConfig_t);
    Config.DecoderId                       = DecoderId;
    Config.Config[DRA_LFE_ENABLE]          = (U8) ACC_MME_TRUE;

    return Codec_MmeAudio_c::FillOutTransformerGlobalParameters(GlobalParams_p);
}


////////////////////////////////////////////////////////////////////////////
///
/// Populate the AUDIO_DECODER's initialization parameters for DRA audio.
///
/// When this method completes Codec_MmeAudio_c::AudioDecoderInitializationParameters
/// will have been filled out with valid values sufficient to initialize a
/// DRA audio decoder (defaults to MPEG Layer II but can be updated by new
/// stream parameters).
///
CodecStatus_t   Codec_MmeAudioDra_c::FillOutTransformerInitializationParameters()
{
    MME_LxAudioDecoderInitParams_t &Params = AudioDecoderInitializationParameters;

    MMEInitializationParameters.TransformerInitParamsSize = sizeof(Params);
    MMEInitializationParameters.TransformerInitParams_p   = &Params;

    CodecStatus_t Status = Codec_MmeAudio_c::FillOutTransformerInitializationParameters();
    if (Status != CodecNoError)
    {
        return Status;
    }

    return FillOutTransformerGlobalParameters(&Params.GlobalParams);
}


////////////////////////////////////////////////////////////////////////////
///
/// Populate the AUDIO_DECODER's MME_SET_GLOBAL_TRANSFORMER_PARAMS parameters for DRA audio.
///
CodecStatus_t   Codec_MmeAudioDra_c::FillOutSetStreamParametersCommand()
{
    CodecStatus_t Status;
    DraAudioCodecStreamParameterContext_t  *Context = (DraAudioCodecStreamParameterContext_t *)StreamParameterContext;

    memset(&(Context->StreamParameters), 0, sizeof(Context->StreamParameters));
    Status = FillOutTransformerGlobalParameters(&(Context->StreamParameters));

    if (Status != CodecNoError)
    {
        return Status;
    }

    // Fill out the actual command
    Context->BaseContext.MMECommand.CmdStatus.AdditionalInfoSize        = 0;
    Context->BaseContext.MMECommand.CmdStatus.AdditionalInfo_p          = NULL;
    Context->BaseContext.MMECommand.ParamSize                           = sizeof(Context->StreamParameters);
    Context->BaseContext.MMECommand.Param_p                             = (MME_GenericParams_t)(&Context->StreamParameters);

    return CodecNoError;
}

////////////////////////////////////////////////////////////////////////////
///
/// Populate the AUDIO_DECODER's MME_TRANSFORM parameters for DRA.
///
CodecStatus_t   Codec_MmeAudioDra_c::FillOutDecodeCommand()
{
    return InitFrameParamsAndStatus(sizeof(MME_LxAudioDecoderFrameExtendedDraStatus_t));
}

////////////////////////////////////////////////////////////////////////////
///
/// Validate the ACC status structure and squawk loudly if problems are found.
///
/// Dispite the squawking this method unconditionally returns success. This is
/// because the firmware will already have concealed the decode problems by
/// performing a soft mute.
///
/// \return CodecSuccess
///
CodecStatus_t   Codec_MmeAudioDra_c::ValidateDecodeContext(CodecBaseDecodeContext_t *Context)
{
    return CommonStatusUpdate(Context);
}

////////////////////////////////////////////////////////////////////////////
///
///  Public static function to fill DRA codec capabilities
///  to expose it through STM_SE_CTRL_GET_CAPABILITY_AUDIO_DECODE control.
///
void Codec_MmeAudioDra_c::GetCapabilities(stm_se_audio_dec_capability_t::audio_dec_dra_capability_s *cap,
                                          const MME_LxAudioDecoderHDInfo_t decInfo)
{
    cap->common.capable = (decInfo.DecoderCapabilityFlags     & (1 << ACC_DRA)
                           & SE_AUDIO_DEC_CAPABILITIES        & (1 << ACC_DRA)
                          ) ? true : false;
}
