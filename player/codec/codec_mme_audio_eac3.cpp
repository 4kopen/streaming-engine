/************************************************************************
Copyright (C) 2003-2014 STMicroelectronics. All Rights Reserved.

This file is part of the Streaming Engine.

Streaming Engine is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License version 2 as
published by the Free Software Foundation.

Streaming Engine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with Streaming Engine; see the file COPYING.  If not, write to the Free
Software Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307,
USA.

The Streaming Engine Library may alternatively be licensed under a
proprietary license from ST.
************************************************************************/

#include "codec_mme_audio_eac3.h"
#include "codec_capabilities.h"
#include "eac3_audio.h"
#include "player_generic.h"
#include "player_playback.h"
#include "player_stream.h"

#include "st_relayfs_se.h"

#undef TRACE_TAG
#define TRACE_TAG "Codec_MmeAudioEAc3_c"

#define EAC3_DMIX_TABLE_TO_REPORT  2

typedef struct EAc3AudioCodecStreamParameterContext_s
{
    CodecBaseStreamParameterContext_t   BaseContext;

    MME_LxAudioDecoderGlobalParams_t StreamParameters;
} EAc3AudioCodecStreamParameterContext_t;

#define BUFFER_EAC3_AUDIO_CODEC_STREAM_PARAMETER_CONTEXT                "EAc3AudioCodecStreamParameterContext"
#define BUFFER_EAC3_AUDIO_CODEC_STREAM_PARAMETER_CONTEXT_TYPE   {BUFFER_EAC3_AUDIO_CODEC_STREAM_PARAMETER_CONTEXT, BufferDataTypeBase, AllocateFromOSMemory, 32, 0, true, true, sizeof(EAc3AudioCodecStreamParameterContext_t)}

static BufferDataDescriptor_t  EAc3AudioCodecStreamParameterContextDescriptor = BUFFER_EAC3_AUDIO_CODEC_STREAM_PARAMETER_CONTEXT_TYPE;

// --------

typedef struct
{
    MME_LxAudioDecoderFrameStatus_t              DecStatus;
    MME_PcmProcessingFrameExtCommonStatus_t      PcmStatus;
    MME_LxAudioDecoderMixingMetadata_t           MixingMetadata;
    MME_DmixStatus_t                             DownmixingData[EAC3_DMIX_TABLE_TO_REPORT];
} MME_LxAudioDecoderFrameExtendedEc3Status_t;

typedef struct EAc3AudioCodecDecodeContext_s
{
    AudioCodecBaseDecodeContext_t               Audio;
    MME_LxAudioDecoderFrameExtendedEc3Status_t  DecodeStatus;
    unsigned int                                TranscodeBufferIndex;
    unsigned int                                AuxBufferIndex;
} EAc3AudioCodecDecodeContext_t;

#define EAC3_MIN_MIXING_METADATA_SIZE       (sizeof(MME_LxAudioDecoderMixingMetadata_t) - sizeof(MME_MixingOutputConfiguration_t))
#define EAC3_MIN_MIXING_METADATA_FIXED_SIZE (EAC3_MIN_MIXING_METADATA_SIZE - (2 * sizeof(U32))) // same as above minus Id and StructSize

#define BUFFER_EAC3_AUDIO_CODEC_DECODE_CONTEXT  "EAc3AudioCodecDecodeContext"
#define BUFFER_EAC3_AUDIO_CODEC_DECODE_CONTEXT_TYPE     {BUFFER_EAC3_AUDIO_CODEC_DECODE_CONTEXT, BufferDataTypeBase, AllocateFromOSMemory, 32, 0, true, true, sizeof(EAc3AudioCodecDecodeContext_t)}

static BufferDataDescriptor_t EAc3AudioCodecDecodeContextDescriptor = BUFFER_EAC3_AUDIO_CODEC_DECODE_CONTEXT_TYPE;

///
Codec_MmeAudioEAc3_c::Codec_MmeAudioEAc3_c()
    : DecoderId(ACC_DDPLUS_ID)
    , isFwEac3Capable(false)
{
    Configuration.CodecName                             = "EAC3 audio";
    Configuration.StreamParameterContextCount           = 1;
    Configuration.StreamParameterContextDescriptor      = &EAc3AudioCodecStreamParameterContextDescriptor;
    Configuration.DecodeContextCount                    = 4;
    Configuration.DecodeContextDescriptor               = &EAc3AudioCodecDecodeContextDescriptor;
    Configuration.TranscodedFrameMaxSize                = EAC3_AC3_MAX_FRAME_SIZE;
    Configuration.MaximumSampleCount                    = EAC3_MAX_DECODED_SAMPLE_COUNT;

    AudioDecoderTransformCapabilityMask.DecoderCapabilityFlags = (1 << ACC_AC3);

    AudioParametersEvents.audio_coding_type  = STM_SE_STREAM_ENCODING_AUDIO_AC3;

    // TODO(pht) move FinalizeInit to a factory method
    InitializationStatus = FinalizeInit();
}

CodecStatus_t Codec_MmeAudioEAc3_c::FinalizeInit()
{
    CodecStatus_t Status = GloballyVerifyMMECapabilities();
    if (CodecNoError != Status)
    {
        SE_INFO(group_decoder_audio, "EAC3 not found\n");
        return CodecError;
    }

    isFwEac3Capable = (((AudioDecoderTransformCapability.DecoderCapabilityExtFlags[0]) >> (4 * ACC_AC3)) & (1 << ACC_DOLBY_DIGITAL_PLUS));
    SE_INFO(group_decoder_audio, "%s\n", isFwEac3Capable ?
            "Using extended AC3 decoder (DD+ streams will be correctly decoded)" :
            "Using standard AC3 decoder (DD+ streams will be unplayable)");

    return CodecNoError;
}

///
Codec_MmeAudioEAc3_c::~Codec_MmeAudioEAc3_c()
{
    Halt();
}

////////////////////////////////////////////////////////////////////////////
///
/// Populate the supplied structure with parameters for EAC3 audio.
///
CodecStatus_t Codec_MmeAudioEAc3_c::FillOutTransformerGlobalParameters(MME_LxAudioDecoderGlobalParams_t *GlobalParams_p)
{
    MME_LxAudioDecoderGlobalParams_t &GlobalParams = *GlobalParams_p;
    GlobalParams.StructSize = sizeof(MME_LxAudioDecoderGlobalParams_t);

    MME_LxDDPConfig_t &Config = *((MME_LxDDPConfig_t *) GlobalParams.DecConfig);
    memset(&Config, 0, sizeof(MME_LxDDPConfig_t));
    Config.DecoderId     = isFwEac3Capable ? ACC_DDPLUS_ID : ACC_AC3_ID;
    Config.StructSize    = sizeof(MME_LxDDPConfig_t);

    Config.DecConfig.CRC_ENABLE         = ACC_MME_TRUE;
    Config.DecConfig.LFE_ENABLE         = ACC_MME_TRUE;
    Config.DecConfig.HIGH_COST_VCR      = ACC_MME_FALSE; // give better quality 2 channel output but uses more CPU

    // DDplus specific parameters
    if (isFwEac3Capable)
    {
        Config.DecConfig.Upsample       = 2; // never upsample;
        Config.DecConfig.AC3Endieness   = 1; // LittleEndian
        Config.DecConfig.DitherAlgo     = 0; // set to 0 for correct transcoding
        Config.DecConfig.DisablePgrmScl = 1; //Disable by default
        Config.DecConfig.SelSubstreamID = Player->PolicyValue(Playback, Stream, PolicyAudioSubstreamId);
    }

    unsigned int AudioApplicationType = Player->PolicyValue(Playback, Stream, PolicyAudioApplicationType);
    switch (AudioApplicationType)
    {
    case PolicyValueAudioApplicationMS10:
    case PolicyValueAudioApplicationMS11:
    case PolicyValueAudioApplicationMS12:
        Config.DecConfig.MdctBandLimit = ACC_MME_TRUE;
        break;
    default:
        Config.DecConfig.MdctBandLimit = ACC_MME_FALSE;
        break;
    }

    Config.DecConfig.ReportMetaData  = 1;
    Config.DecConfig.ReportFrameInfo = 1;

    CodecStatus_t Status = Codec_MmeAudio_c::FillOutTransformerGlobalParameters(GlobalParams_p);
    if (Status != CodecNoError)
    {
        return Status;
    }

    unsigned char *PcmParams_p = ((unsigned char *) &Config) + Config.StructSize;
    MME_LxPcmProcessingGlobalParams_Subset_t &PcmParams =
        *((MME_LxPcmProcessingGlobalParams_Subset_t *) PcmParams_p);
    // downmix must be disabled for EAC3
    MME_DMixGlobalParams_t &DMix = PcmParams.DMix;
    DMix.Apply = ACC_MME_DISABLED;

    MME_CMCGlobalParams_t &cmc = PcmParams.CMC;
    // Dolby require that LFE downmix is disabled in RF mode (certification requirement)
    Config.DecConfig.DisableLfeDmix = (ACC_RF_MODE == cmc.ComprMode) ? true : false;

    return CodecNoError;
}


////////////////////////////////////////////////////////////////////////////
///
/// Populate the AUDIO_DECODER's initialization parameters for EAC3 audio.
///
/// When this method completes Codec_MmeAudio_c::AudioDecoderInitializationParameters
/// will have been filled out with valid values sufficient to initialize an
/// EAC3 audio decoder.
///
CodecStatus_t   Codec_MmeAudioEAc3_c::FillOutTransformerInitializationParameters()
{
    MME_LxAudioDecoderInitParams_t &Params = AudioDecoderInitializationParameters;

    MMEInitializationParameters.TransformerInitParamsSize = sizeof(Params);
    MMEInitializationParameters.TransformerInitParams_p = &Params;

    CodecStatus_t Status = Codec_MmeAudio_c::FillOutTransformerInitializationParameters();
    if (Status != CodecNoError)
    {
        return Status;
    }

    return FillOutTransformerGlobalParameters(&Params.GlobalParams);
}


////////////////////////////////////////////////////////////////////////////
///
/// Populate the AUDIO_DECODER's MME_SET_GLOBAL_TRANSFORMER_PARAMS parameters for EAC3 audio.
///
CodecStatus_t   Codec_MmeAudioEAc3_c::FillOutSetStreamParametersCommand()
{
//EAc3AudioStreamParameters_t *Parsed = (EAc3AudioStreamParameters_t *)ParsedFrameParameters->StreamParameterStructure;
    EAc3AudioCodecStreamParameterContext_t  *Context = (EAc3AudioCodecStreamParameterContext_t *)StreamParameterContext;

    if (ParsedAudioParameters == NULL)
    {
        SE_ERROR("(%s) - ParsedAudioParameters are NULL\n", Configuration.CodecName);
        return CodecError;
    }

    // if the stream is dd+, then the transcoding is required
    TranscodeEnable = (ParsedAudioParameters->OriginalEncoding == AudioOriginalEncodingDdplus);
    // The below function will disable the Transcoding based on the stream and system profile
    DisableTranscodingBasedOnProfile();
    // Enable the AuxBuffer based on profile
    EnableAuxBufferBasedOnProfile();
    SE_DEBUG(group_decoder_audio, "Transcode_Enable : %d TranscodeNeeded:%d\n", TranscodeEnable, TranscodeNeeded);

    //
    // Examine the parsed stream parameters and determine what type of codec to instanciate
    //
    DecoderId = ACC_DDPLUS_ID;
    //
    // Now fill out the actual structure
    //
    memset(&(Context->StreamParameters), 0, sizeof(Context->StreamParameters));
    CodecStatus_t Status = FillOutTransformerGlobalParameters(&(Context->StreamParameters));
    if (Status != CodecNoError)
    {
        return Status;
    }

    //
    // Fill out the actual command
    //
    Context->BaseContext.MMECommand.CmdStatus.AdditionalInfoSize        = 0;
    Context->BaseContext.MMECommand.CmdStatus.AdditionalInfo_p          = NULL;
    Context->BaseContext.MMECommand.ParamSize                           = sizeof(Context->StreamParameters);
    Context->BaseContext.MMECommand.Param_p                             = (MME_GenericParams_t)(&Context->StreamParameters);

    return CodecNoError;
}

////////////////////////////////////////////////////////////////////////////
///
/// Populate the AUDIO_DECODER's MME_TRANSFORM parameters for MPEG audio.
///
CodecStatus_t   Codec_MmeAudioEAc3_c::FillOutDecodeCommand()
{
    return InitFrameParamsAndStatus(sizeof(MME_LxAudioDecoderFrameExtendedEc3Status_t));
}

////////////////////////////////////////////////////////////////////////////
///
/// Validate the ACC status structure and squawk loudly if problems are found.
///
/// Dispite the squawking this method unconditionally returns success. This is
/// because the firmware will already have concealed the decode problems by
/// performing a soft mute.
///
/// \return CodecSuccess
///
CodecStatus_t   Codec_MmeAudioEAc3_c::ValidateDecodeContext(CodecBaseDecodeContext_t *Context)
{
    if (Context == NULL)
    {
        SE_ERROR("(%s) - CodecContext is NULL\n", Configuration.CodecName);
        return CodecError;
    }

    unsigned int outputBufferIdx = EAC3_TRANSCODE_BUFFER_INDEX;
    EAc3AudioCodecDecodeContext_t   *EAc3DecodeContext = (EAc3AudioCodecDecodeContext_t *) Context;
    MME_LxAudioDecoderFrameStatus_t *Status            = &(EAc3DecodeContext->DecodeStatus.DecStatus);

    bool TranscodeOutputBufAvailable = false;
    /* To determine if a transcoded buffer must be handled,
     * let's check if a transcode buffer has been attached to the CodedDataBuffer */
    Buffer_t codedDataBuffer;
    Context->DecodeContextBuffer->ObtainAttachedBufferReference(CodedFrameBufferType, &codedDataBuffer);
    if (codedDataBuffer != NULL)
    {
        Buffer_t TranscodeBuffer;
        codedDataBuffer->ObtainAttachedBufferReference(TranscodedFrameBufferType, &TranscodeBuffer);
        if (TranscodeBuffer != NULL)
        {
            SE_VERBOSE(group_decoder_audio, "Transcode output buffer is available\n");
            TranscodeOutputBufAvailable = true;
        }
    }

    int TranscodedBufferSize = 0;
    if (TranscodeOutputBufAvailable)
    {
        MME_Command_t *Cmd = &EAc3DecodeContext->Audio.Base.MMECommand;
        int PauseBufferSize = sizeof(short) * 2; //corresponds to the pause duration in the transcoded output when FW is unable to do transcoding
        TranscodedBufferSize = Cmd->DataBuffers_p[EAC3_TRANSCODE_BUFFER_INDEX]->ScatterPages_p[0].BytesUsed;

        if ((Status->NbOutSamples) && (TranscodedBufferSize == 0)) // Firmware is not able to transcode
        {
            short *data_p = (short *)Cmd->DataBuffers_p[EAC3_TRANSCODE_BUFFER_INDEX]->ScatterPages_p[0].Page_p;
            TranscodedBufferSize = PauseBufferSize; //update the size of transcoded bufffer with the pause burst size
            Cmd->DataBuffers_p[EAC3_TRANSCODE_BUFFER_INDEX]->ScatterPages_p[0].BytesUsed = TranscodedBufferSize; //update byte used
            data_p[0] = (Status->NbOutSamples); //update duration for spdif pause burst.
            data_p[1] = 0;
        }

        if ((TranscodedBufferSize == 0) || (TranscodedBufferSize > EAC3_AC3_MAX_FRAME_SIZE))
        {
            SE_ERROR("Erroneous transcoded buffer size: %d\n", TranscodedBufferSize);
        }
        else if (TranscodedBufferSize != PauseBufferSize)
        {
            st_relayfs_write_se(ST_RELAY_TYPE_AUDIO_TRANSCODE, ST_RELAY_SOURCE_SE,
                                (unsigned char *) Cmd->DataBuffers_p[EAC3_TRANSCODE_BUFFER_INDEX]->ScatterPages_p[0].Page_p,
                                TranscodedBufferSize, false);
        }
        outputBufferIdx++;

        TranscodedBuffers[EAc3DecodeContext->TranscodeBufferIndex].Buffer->SetUsedDataSize(TranscodedBufferSize);
    }

    if (AuxOutputEnable)
    {
        MME_Command_t *Cmd = &EAc3DecodeContext->Audio.Base.MMECommand;
        outputBufferIdx = (outputBufferIdx <= Cmd->NumberOutputBuffers) ? outputBufferIdx : outputBufferIdx - 1;
        int32_t   AuxBufferSize = Cmd->DataBuffers_p[outputBufferIdx]->ScatterPages_p[0].BytesUsed;

        st_relayfs_write_se(ST_RELAY_TYPE_DECODED_AUDIO_AUX_BUFFER0 + RelayfsIndex, ST_RELAY_SOURCE_SE,
                            (uint8_t *) Cmd->DataBuffers_p[outputBufferIdx]->ScatterPages_p[0].Page_p,
                            AuxBufferSize, false);
        outputBufferIdx++;
    }

    if (EAc3DecodeContext->Audio.Base.MMECommand.NumberOutputBuffers > 1)
    {
        CheckMetadataValidity(EAc3DecodeContext->Audio.Base.MMECommand.DataBuffers_p[outputBufferIdx]);
    }

    if (CodecNoError != CommonStatusUpdate(Context, LFE_PLAYBACK_LEVEL_PLUS10dB))
    {
        return CodecError;
    }

    return CodecNoError;
}


////////////////////////////////////////////////////////////////////////////
///
/// Extract mixing metadata and stuff it into the audio parameters.
///
/// \todo Can we make this code common between EAC3 and DTSHD handling.
///
void Codec_MmeAudioEAc3_c::HandleMixingMetadata(CodecBaseDecodeContext_t *Context,
                                                MME_PcmProcessingStatusTemplate_t *PcmStatus)
{
    ParsedAudioParameters_t *AudioParameters;
    MME_LxAudioDecoderMixingMetadata_t *MixingMetadata = (MME_LxAudioDecoderMixingMetadata_t *) PcmStatus;
    int NbMixConfigurations;

    if (Context == NULL)
    {
        SE_ERROR("(%s) - CodecContext is NULL\n", Configuration.CodecName);
        return;
    }

    OS_LockMutex(&Lock);
    AudioParameters = BufferState[Context->BufferIndex].ParsedParameters.Audio;

    if (AudioParameters == NULL)
    {
        SE_ERROR("(%s) - AudioParameters are NULL\n", Configuration.CodecName);
        OS_UnLockMutex(&Lock);
        return;
    }

    //
    // Validation
    //
    SE_ASSERT(MixingMetadata->MinStruct.Id == ACC_MIX_METADATA_ID);   // already checked by framework

    if (MixingMetadata->MinStruct.StructSize < sizeof(MixingMetadata->MinStruct))
    {
        SE_ERROR("Mixing metadata is too small (%d)\n", MixingMetadata->MinStruct.StructSize);
        OS_UnLockMutex(&Lock);
        return;
    }

    NbMixConfigurations = MixingMetadata->MinStruct.NbOutMixConfig;

    if (NbMixConfigurations > MAX_MIXING_OUTPUT_CONFIGURATION)
    {
        SE_INFO(group_decoder_audio, "Number of mix out configs is gt 3 (%d)!\n", NbMixConfigurations);
        NbMixConfigurations = MAX_MIXING_OUTPUT_CONFIGURATION;
    }

    //
    // Action
    //
    memset(&AudioParameters->MixingMetadata, 0, sizeof(AudioParameters->MixingMetadata));
    AudioParameters->MixingMetadata.IsMixingMetadataPresent = true;
    AudioParameters->MixingMetadata.PostMixGain = MixingMetadata->MinStruct.PostMixGain;
    AudioParameters->MixingMetadata.NbOutMixConfig = MixingMetadata->MinStruct.NbOutMixConfig;

    for (int i = 0; i < NbMixConfigurations; i++)
    {
        MME_MixingOutputConfiguration_t &In  = MixingMetadata->MixOutConfig[i];
        MixingOutputConfiguration_t &Out = AudioParameters->MixingMetadata.MixOutConfig[i];
        Out.AudioMode = In.AudioMode;

        for (int j = 0; j < MAX_NB_CHANNEL_COEFF; j++)
        {
            Out.PrimaryAudioGain[j] = In.PrimaryAudioGain[j];
            Out.SecondaryAudioPanCoeff[j] = In.SecondaryAudioPanCoeff[j];
        }
    }

    OS_UnLockMutex(&Lock);
}

////////////////////////////////////////////////////////////////////////////
///
///  Set Default FrameBase style TRANSFORM command IOs
///  Populate DecodeContext with 1 Input and 2 output Buffers
///  Populate I/O MME_DataBuffers
void Codec_MmeAudioEAc3_c::PresetIOBuffers()
{
    unsigned int outputBufferIdx = EAC3_TRANSCODE_BUFFER_INDEX;
    MME_DataBuffer_t *MMEBuf;
    MME_ScatterPage_t *MMEPage;

    Codec_MmeAudio_c::PresetIOBuffers();

    if (TranscodeEnable & TranscodeNeeded)
    {
        MMEBuf = &DecodeContext->MMEBuffers[outputBufferIdx];
        MMEPage = &DecodeContext->MMEPages[outputBufferIdx];

        DecodeContext->MMEBufferList[outputBufferIdx] = MMEBuf;

        memset(MMEBuf, 0, sizeof(MME_DataBuffer_t));
        memset(MMEPage, 0, sizeof(MME_ScatterPage_t));

        MMEBuf->StructSize           = sizeof(MME_DataBuffer_t);
        MMEBuf->NumberOfScatterPages = 1;
        MMEBuf->ScatterPages_p       = MMEPage;
        MMEBuf->Flags                = BUFFER_TYPE_COMPRESSED_DATA; // trigger transcoding in the firmware
        MMEBuf->TotalSize            = TranscodedBuffers[CurrentTranscodeBufferIndex].BufferLength;
        MMEPage->Page_p              = TranscodedBuffers[CurrentTranscodeBufferIndex].BufferPointer;
        MMEPage->Size                = TranscodedBuffers[CurrentTranscodeBufferIndex].BufferLength;

        outputBufferIdx++;
    }

    if (AuxOutputEnable)
    {
        MMEBuf = &DecodeContext->MMEBuffers[outputBufferIdx];
        MMEPage = &DecodeContext->MMEPages[outputBufferIdx];

        DecodeContext->MMEBufferList[outputBufferIdx] = MMEBuf;

        memset(MMEBuf, 0, sizeof(MME_DataBuffer_t));
        memset(MMEPage, 0, sizeof(MME_ScatterPage_t));

        MMEBuf->StructSize           = sizeof(MME_DataBuffer_t);
        MMEBuf->NumberOfScatterPages = 1;
        MMEBuf->ScatterPages_p       = MMEPage;
        MMEBuf->Flags                = BUFFER_TYPE_AUDIO_IO | 1; // For the FW to consider this as an Aux buffer
        MMEBuf->TotalSize            = AuxBuffers[CurrentAuxBufferIndex].BufferLength;
        MMEPage->Page_p              = AuxBuffers[CurrentAuxBufferIndex].BufferPointer;
        MMEPage->Size                = AuxBuffers[CurrentAuxBufferIndex].BufferLength;

        outputBufferIdx++;
    }
}

////////////////////////////////////////////////////////////////////////////
///
///  Set Default FrameBase style TRANSFORM command for AudioDecoder MT
///  with 1 Input Buffer and 2 Output Buffer.

void Codec_MmeAudioEAc3_c::SetCommandIO()
{
    if (TranscodeEnable && TranscodeNeeded)
    {
        CodecStatus_t Status = GetTranscodeBuffer();

        if (Status != CodecNoError)
        {
            SE_ERROR("failed to get Transcoded buffer: %d. Disabling transcoding\n", Status);
            TranscodeEnable = false;
        }

        ((EAc3AudioCodecDecodeContext_t *)DecodeContext)->TranscodeBufferIndex = CurrentTranscodeBufferIndex;
    }

    if (AuxOutputEnable)
    {
        CodecStatus_t Status = GetAuxBuffer();

        if (Status != CodecNoError)
        {
            SE_ERROR("failed to get Auxiliary buffer: %d. Disabling AuxBufferEnable flag\n", Status);
            AuxOutputEnable = false;
        }
        ((EAc3AudioCodecDecodeContext_t *)DecodeContext)->AuxBufferIndex = CurrentAuxBufferIndex;
    }

    PresetIOBuffers();
    Codec_MmeAudio_c::SetCommandIO();

    if (TranscodeEnable && TranscodeNeeded)
    {
        // FrameBase Transformer :: 1 Input Buffer / 2 Output Buffer sent through same MME_TRANSFORM
        DecodeContext->MMECommand.NumberOutputBuffers = 2;
    }
    if (AuxOutputEnable)
    {
        // FrameBase Transformer :: 1 Input Buffer / 2 Output Buffer sent through same MME_TRANSFORM
        DecodeContext->MMECommand.NumberOutputBuffers += 1;
    }

    // Prepare Metadata Output buffer as the last output buffer
    unsigned int metadataBufIndex = DecodeContext->MMECommand.NumberInputBuffers
                                    + DecodeContext->MMECommand.NumberOutputBuffers;

    // Record Metadata buffer within BufferList
    DecodeContext->MMEBufferList[metadataBufIndex] = &DecodeContext->MMEBuffers[metadataBufIndex];

    // Prepare Metadata buffer
    PresetMetadataOutBuffer(&DecodeContext->MMEBuffers[metadataBufIndex],
                            &DecodeContext->MMEPages[metadataBufIndex],
                            &DecodeContext->MMECommand.NumberOutputBuffers);
}

////////////////////////////////////////////////////////////////////////////
///
///  Public static function to fill AC3/EAC3 codec capabilities
///  to expose it through STM_SE_CTRL_GET_CAPABILITY_AUDIO_DECODE control.
///
void Codec_MmeAudioEAc3_c::GetCapabilities(stm_se_audio_dec_capability_t::audio_dec_ac3_capability_s *cap,
                                           const MME_LxAudioDecoderHDInfo_t decInfo)
{
    const int ExtFlags = Codec_Capabilities_c::ExtractAudioExtendedFlags(decInfo, ACC_AC3);
    cap->common.capable      = (decInfo.DecoderCapabilityFlags     & (1 << ACC_AC3)
                                & SE_AUDIO_DEC_CAPABILITIES        & (1 << ACC_AC3)
                               ) ? true : false;
    cap->DolyDigitalEx       = (ExtFlags & (1 << ACC_DOLBY_DIGITAL_Ex))      ? true : false;
    cap->DolyDigitalPlus     = (ExtFlags & (1 << ACC_DOLBY_DIGITAL_PLUS))    ? true : false;
    cap->DolyDigitalPlus_7_1 = (ExtFlags & (1 << ACC_DOLBY_DIGITAL_PLUS_71)) ? true : false;
}
