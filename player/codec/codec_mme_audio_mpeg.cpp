/************************************************************************
Copyright (C) 2003-2014 STMicroelectronics. All Rights Reserved.

This file is part of the Streaming Engine.

Streaming Engine is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License version 2 as
published by the Free Software Foundation.

Streaming Engine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with Streaming Engine; see the file COPYING.  If not, write to the Free
Software Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307,
USA.

The Streaming Engine Library may alternatively be licensed under a
proprietary license from ST.
************************************************************************/

////////////////////////////////////////////////////////////////////////////
/// \class Codec_MmeAudioMpeg_c
///
/// The MPEG audio codec proxy.
///

#include "codec_mme_audio_mpeg.h"
#include "codec_capabilities.h"
#include "mpeg_audio.h"

#undef TRACE_TAG
#define TRACE_TAG "Codec_MmeAudioMpeg_c"

typedef struct MpegAudioCodecStreamParameterContext_s
{
    CodecBaseStreamParameterContext_t   Base;
    MME_LxAudioDecoderGlobalParams_t    StreamParameters;
} MpegAudioCodecStreamParameterContext_t;

#define BUFFER_MPEG_AUDIO_CODEC_STREAM_PARAMETER_CONTEXT                "MpegAudioCodecStreamParameterContext"
#define BUFFER_MPEG_AUDIO_CODEC_STREAM_PARAMETER_CONTEXT_TYPE   {BUFFER_MPEG_AUDIO_CODEC_STREAM_PARAMETER_CONTEXT, BufferDataTypeBase, AllocateFromOSMemory, 32, 0, true, true, sizeof(MpegAudioCodecStreamParameterContext_t)}

static BufferDataDescriptor_t MpegAudioCodecStreamParameterContextDescriptor = BUFFER_MPEG_AUDIO_CODEC_STREAM_PARAMETER_CONTEXT_TYPE;

// --------

typedef struct
{
    MME_LxAudioDecoderFrameStatus_t             DecStatus;
    MME_PcmProcessingFrameExtCommonStatus_t     PcmStatus;
} MME_LxAudioDecoderFrameExtendedMpegStatus_t;

typedef struct MpegAudioCodecDecodeContext_s
{
    AudioCodecBaseDecodeContext_t               Audio;
    MME_LxAudioDecoderFrameExtendedMpegStatus_t DecodeStatus;
} MpegAudioCodecDecodeContext_t;

#define BUFFER_MPEG_AUDIO_CODEC_DECODE_CONTEXT  "MpegAudioCodecDecodeContext"
#define BUFFER_MPEG_AUDIO_CODEC_DECODE_CONTEXT_TYPE     {BUFFER_MPEG_AUDIO_CODEC_DECODE_CONTEXT, BufferDataTypeBase, AllocateFromOSMemory, 32, 0, true, true, sizeof(MpegAudioCodecDecodeContext_t)}

static BufferDataDescriptor_t MpegAudioCodecDecodeContextDescriptor = BUFFER_MPEG_AUDIO_CODEC_DECODE_CONTEXT_TYPE;



///
Codec_MmeAudioMpeg_c::Codec_MmeAudioMpeg_c()
    : DecoderId(ACC_MP2A_ID)
{
    Configuration.CodecName                             = "MPEG audio";
    Configuration.StreamParameterContextCount           = 1;
    Configuration.StreamParameterContextDescriptor      = &MpegAudioCodecStreamParameterContextDescriptor;
    Configuration.DecodeContextCount                    = 4;
    Configuration.DecodeContextDescriptor               = &MpegAudioCodecDecodeContextDescriptor;
    Configuration.MaximumSampleCount                    = MPEG_MAX_DECODED_SAMPLE_COUNT;

    AudioDecoderTransformCapabilityMask.DecoderCapabilityFlags = (1 << ACC_MP2a) /*| (1 << ACC_MP3)*/;

    AudioParametersEvents.audio_coding_type = STM_SE_STREAM_ENCODING_AUDIO_MPEG2;
}

///
Codec_MmeAudioMpeg_c::~Codec_MmeAudioMpeg_c()
{
    Halt();
}

////////////////////////////////////////////////////////////////////////////
///
/// Populate the supplied structure with parameters for MPEG audio.
///
///
CodecStatus_t Codec_MmeAudioMpeg_c::FillOutTransformerGlobalParameters(MME_LxAudioDecoderGlobalParams_t *GlobalParams_p)
{
    int application_type = Player->PolicyValue(Playback, Stream, PolicyAudioApplicationType);

    SE_INFO(group_decoder_audio, "Initializing MPEG layer %s audio decoder\n",
            (DecoderId == ACC_MP2A_ID ? "I/II" :
             DecoderId == ACC_MP3_ID  ? "III" :
             "unknown"));

    // check for firmware decoder existence in case of SET_GLOBAL only
    // (we don't know the frame type at init time)

    if (ParsedFrameParameters)
    {
        MpegAudioStreamParameters_t *Parsed = (MpegAudioStreamParameters_t *)ParsedFrameParameters->StreamParameterStructure;

        if (Parsed && Parsed->Layer)
        {
            int mask = (DecoderId == ACC_MP2A_ID) ? ACC_MP2a : ACC_MP3;

            if (!(AudioDecoderTransformCapability.DecoderCapabilityFlags & (1 << mask)))
            {
                CodecStatus_t CodecStatus;
                SE_ERROR("This type of MPEG Layer is not supported by the selected firmware\n");
                // We've been that far because we thought the codec was supported and maybe it is but
                // maybe it is only supported by one of the audio CPUs.
                // terminate the current transformer
                CodecStatus = TerminateMMETransformer();

                // no sucess... then abort!
                if (CodecStatus != CodecNoError)
                {
                    Stream->MarkUnPlayable();
                    return CodecStatus; // sub-routine puts errors to console
                }

                // and check the capability again wrt the precise layer (will automatically change the Selected CPU if required).
                AudioDecoderTransformCapabilityMask.DecoderCapabilityFlags = (1 << mask);
                CodecStatus = GloballyVerifyMMECapabilities();

                if (CodecStatus == CodecNoError)
                {
                    // if the codec is indeed supported , reinitialise the transformer based on the new Selected CPU.
                    CodecStatus = InitializeMMETransformer();
                }

                // no sucess... then abort!
                if (CodecStatus != CodecNoError)
                {
                    Stream->MarkUnPlayable();
                    return CodecStatus; // sub-routine puts errors to console
                }
            }

            // Update AudioParametersEvents
            AudioParametersEvents.audio_coding_type  = (DecoderId == ACC_MP2A_ID) ? STM_SE_STREAM_ENCODING_AUDIO_MPEG2 : STM_SE_STREAM_ENCODING_AUDIO_MP3;
            AudioParametersEvents.codec_specific.mpeg_params.layer = Parsed->Layer;
        }
    }

    MME_LxAudioDecoderGlobalParams_t &GlobalParams = *GlobalParams_p;
    GlobalParams.StructSize = sizeof(MME_LxAudioDecoderGlobalParams_t);

    MME_LxMp2aConfig_t &Config = *((MME_LxMp2aConfig_t *) GlobalParams.DecConfig);
    Config.DecoderId = DecoderId;
    Config.StructSize = sizeof(Config);
    Config.Config[MP2a_CRC_ENABLE] = ACC_MME_TRUE;
    Config.Config[MP2a_LFE_ENABLE] = ACC_MME_FALSE; // low frequency effects
    {
        uMp2aDRC dynamic_range;
        dynamic_range.u32               = 0;
        // MP2 streams encoded in DVD may carry DRC info ... nothing can be inferred for other application types.
        // because there is no way for the decoder to verify the decoded info is indeed a DRC info,
        // it is preferred to condition its application to DVD streams only.
        dynamic_range.bits.DRC_Enable   = ((DRC.DRC_Enable) && (application_type == STM_SE_CTRL_VALUE_AUDIO_APPLICATION_DVD)) ?
                                          ACC_MME_TRUE : ACC_MME_FALSE;
        // If set to MP2A_PROFILE_DVB (legacy) the mp2a decoder would report DialogNorm=-23dBFS
        // unless the host specifies a non 0 value in the frame params...
        // If set to MP2A_PROFILE_ISO, it will precisely report the value given in the frame params that is it will report 0 dB
        // if the frame params says 0dB.
        dynamic_range.bits.Profile      = (application_type == STM_SE_CTRL_VALUE_AUDIO_APPLICATION_DVB) ? MP2A_PROFILE_DVB : MP2A_PROFILE_ISO;
        Config.Config[MP2a_DRC_ENABLE]  = (unsigned char) dynamic_range.u32;
    }
    // enabling multi-channel decode can cause a firmware crash when decoding
    // MPEG-1 audio (without multi-channel).
    // The only known source of mpeg2 multichannel stream is DVD streams.
    // So enable MC decoding only for DVD streams and if the requested  output speaker configuration is larger than stereo.
    Config.Config[MP2a_MC_ENABLE] = ((OutmodeMain >= ACC_MODE20) && (application_type == STM_SE_CTRL_VALUE_AUDIO_APPLICATION_DVD)) ?
                                    ACC_MME_TRUE : ACC_MME_FALSE;
    Config.Config[MP2a_NIBBLE_ENABLE] = ACC_MME_FALSE; // mpeg frame parser doesn't lock on 4bits aligned frames.
    Config.Config[MP3_FREE_FORMAT]    = ACC_MME_TRUE;  // "free bit rate streams"
    {
        uMp2aActionOnError action_on_error;
        action_on_error.u32                     = 0;
        action_on_error.MuteOnError             = ACC_MME_TRUE;
        action_on_error.bits.LayerSwitchTrigger = 8;
        Config.Config[MP3_MUTE_ON_ERROR]        = (unsigned char) action_on_error.u32;
    }
    Config.Config[MP3_DECODE_LAST_FRAME] = ACC_MME_TRUE;

    return Codec_MmeAudio_c::FillOutTransformerGlobalParameters(GlobalParams_p);
}


////////////////////////////////////////////////////////////////////////////
///
/// Populate the AUDIO_DECODER's initialization parameters for MPEG audio.
///
/// When this method completes Codec_MmeAudio_c::AudioDecoderInitializationParameters
/// will have been filled out with valid values sufficient to initialize an
/// MPEG audio decoder (defaults to MPEG Layer II but can be updated by new
/// stream parameters).
///
CodecStatus_t   Codec_MmeAudioMpeg_c::FillOutTransformerInitializationParameters()
{
    MME_LxAudioDecoderInitParams_t &Params = AudioDecoderInitializationParameters;

    MMEInitializationParameters.TransformerInitParamsSize = sizeof(Params);
    MMEInitializationParameters.TransformerInitParams_p = &Params;

    CodecStatus_t Status = Codec_MmeAudio_c::FillOutTransformerInitializationParameters();
    if (Status != CodecNoError)
    {
        return Status;
    }

    return FillOutTransformerGlobalParameters(&Params.GlobalParams);
}


////////////////////////////////////////////////////////////////////////////
///
/// Populate the AUDIO_DECODER's MME_SET_GLOBAL_TRANSFORMER_PARAMS parameters for MPEG audio.
///
CodecStatus_t   Codec_MmeAudioMpeg_c::FillOutSetStreamParametersCommand()
{
    CodecStatus_t Status;
    MpegAudioStreamParameters_t *Parsed = (MpegAudioStreamParameters_t *)ParsedFrameParameters->StreamParameterStructure;
    MpegAudioCodecStreamParameterContext_t  *Context = (MpegAudioCodecStreamParameterContext_t *)StreamParameterContext;

// Parsed may be NULL if call to this function results from an ALSA parameter update.
    if (Parsed)
    {
        //
        // Examine the parsed stream parameters and determine what type of codec to instanciate
        //
        DecoderId = Parsed->Layer == 3 ? ACC_MP3_ID : ACC_MP2A_ID;
    }

    //
    // Now fill out the actual structure
    //
    memset(&(Context->StreamParameters), 0, sizeof(Context->StreamParameters));
    Status = FillOutTransformerGlobalParameters(&(Context->StreamParameters));

    if (Status != CodecNoError)
    {
        return Status;
    }

    //
    // Fill out the actual command
    //
    Context->Base.MMECommand.CmdStatus.AdditionalInfoSize        = 0;
    Context->Base.MMECommand.CmdStatus.AdditionalInfo_p          = NULL;
    Context->Base.MMECommand.ParamSize                           = sizeof(Context->StreamParameters);
    Context->Base.MMECommand.Param_p                             = (MME_GenericParams_t)(&Context->StreamParameters);

    return CodecNoError;
}

////////////////////////////////////////////////////////////////////////////
///
/// Populate the AUDIO_DECODER's MME_TRANSFORM parameters for MPEG audio.
///
CodecStatus_t   Codec_MmeAudioMpeg_c::FillOutDecodeCommand()
{
    return InitFrameParamsAndStatus(sizeof(MME_LxAudioDecoderFrameExtendedMpegStatus_t));
}

////////////////////////////////////////////////////////////////////////////
///
/// Validate the ACC status structure and squawk loudly if problems are found.
///
/// Dispite the squawking this method unconditionally returns success. This is
/// because the firmware will already have concealed the decode problems by
/// performing a soft mute.
///
/// \return CodecSuccess
///
CodecStatus_t   Codec_MmeAudioMpeg_c::ValidateDecodeContext(CodecBaseDecodeContext_t *Context)
{
    return CommonStatusUpdate(Context);
}

void Codec_MmeAudioMpeg_c::SendParamsUpdatedEvent(AudioCodecDecodeContext_t *DecodeContext)
{
    // Check if a new AudioParameter event occurs
    if (NULL != DecodeContext)
    {
        // Evaluate a new set of Audio parameters and compare with previous to determine the requirement for an event
        memset(&mNewAudioParametersValues, 0, sizeof(mNewAudioParametersValues));

        // fill specific codec parameters: myEvent (for event notification) and newAudioParametersValues (for comparison)
        mEvent.ExtraValue[0].UnsignedInt =  AudioParametersEvents.codec_specific.mpeg_params.layer;
        mNewAudioParametersValues.codec_specific.mpeg_params.layer = AudioParametersEvents.codec_specific.mpeg_params.layer;

        CheckAudioParameterEvent(&DecodeContext->DecodeStatus.DecStatus, (MME_PcmProcessingFrameExtStatus_t *) &DecodeContext->DecodeStatus.PcmStatus);
    }
}

////////////////////////////////////////////////////////////////////////////
///
///  Public static function to fill MPEG2A codec capabilities
///  to expose it through STM_SE_CTRL_GET_CAPABILITY_AUDIO_DECODE control.
///

void Codec_MmeAudioMpeg_c::GetCapabilities(stm_se_audio_dec_capability_t::audio_dec_mp2a_capability_s *cap,
                                           const MME_LxAudioDecoderHDInfo_t decInfo)
{
    const int ExtFlags = Codec_Capabilities_c::ExtractAudioExtendedFlags(decInfo, ACC_MP2a);
    cap->common.capable = (decInfo.DecoderCapabilityFlags     & (1 << ACC_MP2a)
                           & SE_AUDIO_DEC_CAPABILITIES        & (1 << ACC_MP2a)
                          ) ? true : false;
    cap->mp2_stereo_L1 = cap->common.capable             ? true : false;
    cap->mp2_stereo_L2 = cap->common.capable             ? true : false;
    cap->mp2_51_L1     = false;
    cap->mp2_51_L2     = (ExtFlags & (1 << ACC_MP2_MC_L2)) ? true : false;
    cap->mp2_71_L2     = (ExtFlags & (1 << ACC_MP2_71_L2)) ? true : false;
    cap->mp2_DAB       = (ExtFlags & (1 << ACC_MP2_DAB))   ? true : false;
}

////////////////////////////////////////////////////////////////////////////
///
///  Public static function to fill MP3 codec capabilities
///  to expose it through STM_SE_CTRL_GET_CAPABILITY_AUDIO_DECODE control.
///
void Codec_MmeAudioMpeg_c::GetCapabilities(stm_se_audio_dec_capability_t::audio_dec_mp3_capability_s *cap,
                                           const MME_LxAudioDecoderHDInfo_t decInfo)
{
    const int ExtFlags = Codec_Capabilities_c::ExtractAudioExtendedFlags(decInfo, ACC_MP3);
    cap->common.capable = (decInfo.DecoderCapabilityFlags     & (1 << ACC_MP3)
                           & SE_AUDIO_DEC_CAPABILITIES        & (1 << ACC_MP3)
                          ) ? true : false;
    cap->mp3_PRO      = (ExtFlags & (1 << ACC_MP3_PRO))      ? true : false;
    cap->mp3_SURROUND = (ExtFlags & (1 << ACC_MP3_SURROUND)) ? true : false;
    cap->mp3_BINAURAL = (ExtFlags & (1 << ACC_MP3_BINAURAL)) ? true : false;
    cap->mp3_HD       = (ExtFlags & (1 << ACC_MP3_HD))       ? true : false;
}
