/************************************************************************
Copyright (C) 2003-2014 STMicroelectronics. All Rights Reserved.

This file is part of the Streaming Engine.

Streaming Engine is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License version 2 as
published by the Free Software Foundation.

Streaming Engine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with Streaming Engine; see the file COPYING.  If not, write to the Free
Software Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307,
USA.

The Streaming Engine Library may alternatively be licensed under a
proprietary license from ST.
************************************************************************/

#ifndef H_CODEC_MME_AUDIO_PCM
#define H_CODEC_MME_AUDIO_PCM

#include <ACC_Transformers/Pcm_TranscoderTypes.h>

#include "codec_mme_audio.h"
#include "pcm_audio.h"

#undef TRACE_TAG
#define TRACE_TAG "Codec_MmeAudioPcm_c"

class Codec_MmeAudioPcm_c : public Codec_MmeAudio_c
{
public:
    // Constructor/Destructor methods
    Codec_MmeAudioPcm_c();
    ~Codec_MmeAudioPcm_c();

    // Extension to base functions
    CodecStatus_t   ParseCapabilities(unsigned int ActualTransformer);

protected:
    eAccBoolean     RestartTransformer;

    CodecStatus_t   FillOutTransformerGlobalParameters(MME_LxAudioDecoderGlobalParams_t *GlobalParams);
    CodecStatus_t   FillOutTransformerInitializationParameters();
    CodecStatus_t   FillOutSetStreamParametersCommand();
    CodecStatus_t   FillOutDecodeCommand();

    CodecStatus_t   ValidateDecodeContext(CodecBaseDecodeContext_t *Context);
};

#endif /* H_CODEC_MME_AUDIO_PCM */
