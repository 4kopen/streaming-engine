/************************************************************************
Copyright (C) 2003-2014 STMicroelectronics. All Rights Reserved.

This file is part of the Streaming Engine.

Streaming Engine is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License version 2 as
published by the Free Software Foundation.

Streaming Engine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with Streaming Engine; see the file COPYING.  If not, write to the Free
Software Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307,
USA.

The Streaming Engine Library may alternatively be licensed under a
proprietary license from ST.
************************************************************************/

////////////////////////////////////////////////////////////////////////////
/// \class Codec_MmeAudioRma_c
///
/// The RMA audio codec proxy.
///

#include "codec_mme_audio_rma.h"
#include "codec_capabilities.h"
#include "rma_audio.h"

#undef TRACE_TAG
#define TRACE_TAG "Codec_MmeAudioRma_c"

static inline unsigned int BE2LE(unsigned int Value)
{
    return (((Value & 0xff) << 24) | ((Value & 0xff00) << 8) | ((Value >> 8) & 0xff00) | ((Value >> 24) & 0xff));
}

typedef struct RmaAudioCodecStreamParameterContext_s
{
    CodecBaseStreamParameterContext_t   BaseContext;
    MME_LxAudioDecoderGlobalParams_t    StreamParameters;
} RmaAudioCodecStreamParameterContext_t;

#define BUFFER_RMA_AUDIO_CODEC_STREAM_PARAMETER_CONTEXT         "RmaAudioCodecStreamParameterContext"
#define BUFFER_RMA_AUDIO_CODEC_STREAM_PARAMETER_CONTEXT_TYPE    {BUFFER_RMA_AUDIO_CODEC_STREAM_PARAMETER_CONTEXT, BufferDataTypeBase, AllocateFromOSMemory, 32, 0, true, true, sizeof(RmaAudioCodecStreamParameterContext_t)}

static BufferDataDescriptor_t           RmaAudioCodecStreamParameterContextDescriptor   = BUFFER_RMA_AUDIO_CODEC_STREAM_PARAMETER_CONTEXT_TYPE;

typedef struct
{
    MME_LxAudioDecoderFrameStatus_t             DecStatus;
    MME_PcmProcessingFrameExtCommonStatus_t     PcmStatus;
} MME_LxAudioDecoderFrameExtendedRmaStatus_t;

typedef struct RmaAudioCodecDecodeContext_s
{
    AudioCodecBaseDecodeContext_t               Audio;
    MME_LxAudioDecoderFrameExtendedRmaStatus_t  DecodeStatus;
} RmaAudioCodecDecodeContext_t;

#define BUFFER_RMA_AUDIO_CODEC_DECODE_CONTEXT                   "RmaAudioCodecDecodeContext"
#define BUFFER_RMA_AUDIO_CODEC_DECODE_CONTEXT_TYPE              {BUFFER_RMA_AUDIO_CODEC_DECODE_CONTEXT, BufferDataTypeBase, AllocateFromOSMemory, 32, 0, true, true, sizeof(RmaAudioCodecDecodeContext_t)}

static BufferDataDescriptor_t           RmaAudioCodecDecodeContextDescriptor    = BUFFER_RMA_AUDIO_CODEC_DECODE_CONTEXT_TYPE;

Codec_MmeAudioRma_c::Codec_MmeAudioRma_c()
    : DecoderId(ACC_REALAUDIO_ID)
    , RestartTransformer(ACC_MME_FALSE)
{
    Configuration.CodecName                             = "Rma audio";
    Configuration.StreamParameterContextCount           = 1;
    Configuration.StreamParameterContextDescriptor      = &RmaAudioCodecStreamParameterContextDescriptor;
    Configuration.DecodeContextCount                    = 4;
    Configuration.DecodeContextDescriptor               = &RmaAudioCodecDecodeContextDescriptor;
    Configuration.MaximumSampleCount                    = RMA_MAX_DECODED_SAMPLE_COUNT;

    AudioDecoderTransformCapabilityMask.DecoderCapabilityFlags = (1 << ACC_REAL_AUDIO);
}

Codec_MmeAudioRma_c::~Codec_MmeAudioRma_c()
{
    Halt();
}

//{{{  FillOutTransformerGlobalParameters
////////////////////////////////////////////////////////////////////////////
///
/// Populate the supplied structure with parameters for RMA audio.
///
///
CodecStatus_t Codec_MmeAudioRma_c::FillOutTransformerGlobalParameters(MME_LxAudioDecoderGlobalParams_t *GlobalParams_p)
{
    MME_LxAudioDecoderGlobalParams_t   &GlobalParams    = *GlobalParams_p;
    SE_INFO(group_decoder_audio, "Initializing RMA audio decoder\n");
    GlobalParams.StructSize             = sizeof(MME_LxAudioDecoderGlobalParams_t);
    MME_LxRealAudioConfig_t &Config     = *((MME_LxRealAudioConfig_t *)GlobalParams.DecConfig);
    Config.DecoderId                    = DecoderId;
    Config.StructSize                   = sizeof(Config);

    if ((ParsedFrameParameters != NULL) && (ParsedFrameParameters->StreamParameterStructure != NULL))
    {
        RmaAudioStreamParameters_s     *StreamParams    = (RmaAudioStreamParameters_s *)ParsedFrameParameters->StreamParameterStructure;
        Config.CodecType                = BE2LE(StreamParams->CodecId);
        Config.usNumChannels            = StreamParams->ChannelCount;
        Config.usFlavorIndex            = StreamParams->CodecFlavour;
        Config.ulSampleRate             = StreamParams->SampleRate;
        Config.ulBitsPerFrame           = StreamParams->SubPacketSize << 3;
        // Config.ulGranularity            = StreamParams->FrameSize; TODO(?) check what to do from FrameSize vs SubPacketSize
        Config.ulGranularity            = StreamParams->SubPacketSize;
        Config.ulOpaqueDataSize         = StreamParams->CodecOpaqueDataLength;
    }

    Config.NbSample2Conceal             = 0;
    Config.Features                     = 0;
    RestartTransformer                  = ACC_MME_TRUE;

    if (SE_IS_DEBUG_ON(group_decoder_audio))
    {
        SE_DEBUG(group_decoder_audio, " DecoderId                  %d\n", Config.DecoderId);
        SE_DEBUG(group_decoder_audio, " StructSize                 %d\n", Config.StructSize);
        SE_DEBUG(group_decoder_audio, " CodecType                %.4s\n", (unsigned char *)&Config.CodecType);
        SE_DEBUG(group_decoder_audio, " usNumChannels              %d\n", Config.usNumChannels);
        SE_DEBUG(group_decoder_audio, " usFlavorIndex              %d\n", Config.usFlavorIndex);
        SE_DEBUG(group_decoder_audio, " NbSample2Conceal           %d\n", Config.NbSample2Conceal);
        SE_DEBUG(group_decoder_audio, " ulSampleRate               %d\n", Config.ulSampleRate);
        SE_DEBUG(group_decoder_audio, " ulBitsPerFrame             %d\n", Config.ulBitsPerFrame);
        SE_DEBUG(group_decoder_audio, " ulGranularity              %d\n", Config.ulGranularity);
        SE_DEBUG(group_decoder_audio, " ulOpaqueDataSize           %d\n", Config.ulOpaqueDataSize);
        SE_DEBUG(group_decoder_audio, " Features                   %d\n", Config.Features);
    }

    return Codec_MmeAudio_c::FillOutTransformerGlobalParameters(GlobalParams_p);
}
//}}}
//{{{  FillOutTransformerInitializationParameters
////////////////////////////////////////////////////////////////////////////
///
/// Populate the AUDIO_DECODER's initialization parameters for MPEG audio.
///
/// When this method completes Codec_MmeAudio_c::AudioDecoderInitializationParameters
/// will have been filled out with valid values sufficient to initialize an
/// MPEG audio decoder (defaults to MPEG Layer II but can be updated by new
/// stream parameters).
///
CodecStatus_t   Codec_MmeAudioRma_c::FillOutTransformerInitializationParameters()
{
    CodecStatus_t                       Status;
    MME_LxAudioDecoderInitParams_t     &Params                  = AudioDecoderInitializationParameters;
    MMEInitializationParameters.TransformerInitParamsSize       = sizeof(Params);
    MMEInitializationParameters.TransformerInitParams_p         = &Params;
    Status                                                      = Codec_MmeAudio_c::FillOutTransformerInitializationParameters();

    if (Status != CodecNoError)
    {
        return Status;
    }

    return FillOutTransformerGlobalParameters(&Params.GlobalParams);
}
//}}}
//{{{  FillOutSetStreamParametersCommand
////////////////////////////////////////////////////////////////////////////
///
/// Populate the AUDIO_DECODER's MME_SET_GLOBAL_TRANSFORMER_PARAMS parameters for RMA audio.
///
CodecStatus_t   Codec_MmeAudioRma_c::FillOutSetStreamParametersCommand()
{
    CodecStatus_t                               Status;
    RmaAudioCodecStreamParameterContext_t      *Context = (RmaAudioCodecStreamParameterContext_t *)StreamParameterContext;
    DecoderId           = ACC_REALAUDIO_ID;
    // Fill out the structure
    memset(&(Context->StreamParameters), 0, sizeof(Context->StreamParameters));
    Status              = FillOutTransformerGlobalParameters(&(Context->StreamParameters));

    if (Status != CodecNoError)
    {
        return Status;
    }

    // Fill out the actual command
    Context->BaseContext.MMECommand.CmdStatus.AdditionalInfoSize = 0;
    Context->BaseContext.MMECommand.CmdStatus.AdditionalInfo_p   = NULL;
    Context->BaseContext.MMECommand.ParamSize                    = sizeof(Context->StreamParameters);
    Context->BaseContext.MMECommand.Param_p                      = (MME_GenericParams_t)(&Context->StreamParameters);
    return CodecNoError;
}
//}}}
//{{{  FillOutDecodeCommand
////////////////////////////////////////////////////////////////////////////
///
/// Populate the AUDIO_DECODER's MME_TRANSFORM parameters for Real audio.
///
CodecStatus_t   Codec_MmeAudioRma_c::FillOutDecodeCommand()
{
    InitFrameParamsAndStatus(sizeof(MME_LxAudioDecoderFrameExtendedRmaStatus_t));

    RmaAudioCodecDecodeContext_t    *Context                            = (RmaAudioCodecDecodeContext_t *)DecodeContext;

    Context->Audio.FrameParams.Restart           = RestartTransformer;
    RestartTransformer                          = ACC_MME_FALSE;

    SE_DEBUG(group_decoder_audio, "Restart                    %d\n", Context->Audio.FrameParams.Restart);
    return CodecNoError;
}
//}}}
//{{{  ValidateDecodeContext
////////////////////////////////////////////////////////////////////////////
///
/// Validate the ACC status structure and squawk loudly if problems are found.
///
/// \return CodecSuccess
///
CodecStatus_t   Codec_MmeAudioRma_c::ValidateDecodeContext(CodecBaseDecodeContext_t *Context)
{
    return CommonStatusUpdate(Context);
}
//}}}

//{{{ GetCapabilities
////////////////////////////////////////////////////////////////////////////
///
///  Public static function to fill REALAUDIO codec capabilities
///  to expose it through STM_SE_CTRL_GET_CAPABILITY_AUDIO_DECODE control.
///
void Codec_MmeAudioRma_c::GetCapabilities(stm_se_audio_dec_capability_t::audio_dec_realaudio_capability_s *cap,
                                          const MME_LxAudioDecoderHDInfo_t decInfo)
{
    const int ExtFlags = Codec_Capabilities_c::ExtractAudioExtendedFlags(decInfo, ACC_REAL_AUDIO);
    cap->common.capable = (decInfo.DecoderCapabilityFlags     & (1 << ACC_REAL_AUDIO)
                           & SE_AUDIO_DEC_CAPABILITIES        & (1 << ACC_REAL_AUDIO)
                          ) ? true : false;
    cap->ra_COOK   = (ExtFlags & (1 << ACC_RA_COOK))   ? true : false;
    cap->ra_LSD    = (ExtFlags & (1 << ACC_RA_LSD))    ? true : false;
    cap->ra_AAC    = (ExtFlags & (1 << ACC_RA_AAC))    ? true : false;
    cap->ra_DEPACK = (ExtFlags & (1 << ACC_RA_DEPACK)) ? true : false;
}

//}}}
