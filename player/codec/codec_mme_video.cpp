/************************************************************************
Copyright (C) 2003-2014 STMicroelectronics. All Rights Reserved.

This file is part of the Streaming Engine.

Streaming Engine is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License version 2 as
published by the Free Software Foundation.

Streaming Engine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with Streaming Engine; see the file COPYING.  If not, write to the Free
Software Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307,
USA.

The Streaming Engine Library may alternatively be licensed under a
proprietary license from ST.
************************************************************************/

#include "codec_mme_video.h"

#undef TRACE_TAG
#define TRACE_TAG "Codec_MmeVideo_c"

#define DECODE_RATE_LOWER_LIMIT             Rational_t(3,4)     // Used to be 1, changed to trap failure to decode of divxhd
#define DECODE_RATE_UPPER_LIMIT             6

// /////////////////////////////////////////////////////////////////////////
//
//      Constructor function, fills in the codec specific parameter values
//

Codec_MmeVideo_c::Codec_MmeVideo_c()
    : ParsedVideoParameters(NULL)
    , KnownLastSliceInFieldFrame(false)
    , PictureDecodeTime(0)
    , IsFieldPicture(false)
    , RefBufferType(PolicyValueMBandRasterBuff)
    , mPreviousRequestReferenceBufferCount(0)
    , mPreviousRequestManifestationBufferCount(0)
    , DeltaTopTransformCapability()
{
    SetGroupTrace(group_decoder_video);

    Configuration.CodecName = "unknown video";

    strncpy(Configuration.TranscodedMemoryPartitionName, "vid-transcoded",
            sizeof(Configuration.TranscodedMemoryPartitionName));
    Configuration.TranscodedMemoryPartitionName[sizeof(Configuration.TranscodedMemoryPartitionName) - 1] = '\0';
    strncpy(Configuration.AncillaryMemoryPartitionName, "vid-codec-data",
            sizeof(Configuration.AncillaryMemoryPartitionName));
    Configuration.AncillaryMemoryPartitionName[sizeof(Configuration.AncillaryMemoryPartitionName) - 1] = '\0';
}

// /////////////////////////////////////////////////////////////////////////
//
//  The Halt function, give up access to any registered resources
//

CodecStatus_t   Codec_MmeVideo_c::Halt()
{
    ParsedVideoParameters   = NULL;
    KnownLastSliceInFieldFrame  = false;
    return Codec_MmeBase_c::Halt();
}

// /////////////////////////////////////////////////////////////////////////
//
//  The get coded frame buffer pool fn
//

CodecStatus_t   Codec_MmeVideo_c::Input(Buffer_t      CodedBuffer)
{
    //
    // Are we allowed in here
    //
    AssertComponentState(ComponentRunning);

    //
    // First perform base operations
    //
    CodecStatus_t Status = Codec_MmeBase_c::Input(CodedBuffer);
    if (Status != CodecNoError)
    {
        return Status;
    }

    //
    // Do we need to issue a new set of stream parameters
    //

    if (ParsedFrameParameters->NewStreamParameters)
    {
        Status = FillOutSetStreamParametersCommand();
        if (Status == CodecNoError)
        {
            bool AVDSyncOff = (Player->PolicyValue(Playback, Stream, PolicyAVDSynchronization) == PolicyValueDisapply);
            // jobs are ordered thanks to their due time , 0 means top prio, 1 background prio
            StreamParameterContext->MMECommand.DueTime = (AVDSyncOff) ? 1 : 0;
            Status    = SendMMEStreamParameters();
        }

        if (Status != CodecNoError)
        {
            SE_ERROR("(%s) - Failed to fill out, and send, a set stream parameters command\n", Configuration.CodecName);
            ForceStreamParameterReload      = true;
            StreamParameterContextBuffer->DecrementReferenceCount();
            StreamParameterContextBuffer    = NULL;
            StreamParameterContext          = NULL;
            OS_LockMutex(&Lock);
            ReleaseDecodeContext(DecodeContext);
            Codec_MmeBase_c::OutputPartialDecodeBuffersLocked();
            OS_UnLockMutex(&Lock);
            return Status;
        }

        StreamParameterContextBuffer = NULL;
        StreamParameterContext       = NULL;
    }

    //
    // If there is no frame to decode then exit now.
    //

    if (!ParsedFrameParameters->NewFrameParameters)
    {
        return CodecNoError;
    }

    //
    // Test if this frame indicates completion of any previous decodes (for slice decoding)
    //
    OS_LockMutex(&Lock);

    if (Configuration.SliceDecodePermitted &&
        ParsedVideoParameters->FirstSlice &&
        (CurrentDecodeBufferIndex != INVALID_INDEX) &&
        (BufferState[CurrentDecodeBufferIndex].ParsedParameters.Video->PictureStructure == StructureFrame))
    {
        SE_WARNING("(%s) - End of frame (slice decoding)\n", Configuration.CodecName);
        Codec_MmeBase_c::OutputPartialDecodeBuffersLocked();
    }

    //
    // Check for a half buffer
    //
    if ((CurrentDecodeBufferIndex != INVALID_INDEX) && ParsedFrameParameters->FirstParsedParametersForOutputFrame)
    {
        SE_WARNING("(%s) - New frame starts when we have one field in decode buffer\n", Configuration.CodecName);
        Codec_MmeBase_c::OutputPartialDecodeBuffersLocked();
    }

    //
    // Obtain a new buffer if needed
    //
    if (CurrentDecodeBufferIndex == INVALID_INDEX)
    {
        Status = GetDecodeBuffer();
        if (Status == DecodeBufferManagerFailedToAllocateComponents)
        {
            unsigned int t = 0;
            while (t < 2 * MAX_GET_BUFFER_WAITING_TIME_MS)
            {
                Status = GetDecodeBuffer();
                if (Status == CodecNoError) { break; }
                OS_SleepMilliSeconds(GET_BUFFER_WAITING_PERIOD_MS);
                t += GET_BUFFER_WAITING_PERIOD_MS;
                SE_DEBUG2(group_decoder_video, group_player, "Retry to get decode buffer\n");
            }
        }
        if (Status != CodecNoError)
        {
            SE_ERROR("(%s) - Failed to get decode buffer\n", Configuration.CodecName);
            ReleaseDecodeContext(DecodeContext);
            OS_UnLockMutex(&Lock);
            return Status;
        }

        //
        // reset the buffer content to contain no data
        //
        CodecBufferState_t  *State = &BufferState[CurrentDecodeBufferIndex];
        State->ParsedParameters.Video->PictureStructure  = StructureEmpty;
        State->FieldDecode = ParsedVideoParameters->PictureStructure != StructureFrame;
    }

    //
    // If we are re-using the buffer, and this is the first slice
    // (hence of a second field) we first check the decode components
    // for a reference frame (to support first field non-reference,
    // second field reference). Then we update the field counts in the
    // buffer record.
    //
    else if (ParsedVideoParameters->FirstSlice)
    {
        if (ParsedFrameParameters->ReferenceFrame)
        {
            Status  = Stream->GetDecodeBufferManager()->EnsureReferenceComponentsPresent(BufferState[CurrentDecodeBufferIndex].Buffer);

            if (Status != PlayerNoError)
            {
                SE_ERROR("(%s) - Failed to ensure reference components present\n", Configuration.CodecName);
                Codec_MmeVideo_c::OutputPartialDecodeBuffersLocked();
                OS_UnLockMutex(&Lock);
                return Status;
            }
        }

        Status  = MapBufferToDecodeIndex(ParsedFrameParameters->DecodeFrameIndex, CurrentDecodeBufferIndex);
        if (Status != CodecNoError)
        {
            SE_ERROR("(%s) - Failed to map second field index to decode buffer - Implementation error\n", Configuration.CodecName);
            Codec_MmeVideo_c::OutputPartialDecodeBuffersLocked();
            OS_UnLockMutex(&Lock);
            return PlayerImplementationError;
        }

        ParsedVideoParameters_t  *PreviousFieldParameters = BufferState[CurrentDecodeBufferIndex].ParsedParameters.Video;
        if (PreviousFieldParameters == NULL)
        {
            SE_ERROR("(%s) - PreviousFieldParameters (ParsedVideoParameters, idx %d) are NULL\n", Configuration.CodecName, CurrentDecodeBufferIndex);
            Codec_MmeBase_c::OutputPartialDecodeBuffersLocked();
            OS_UnLockMutex(&Lock);
            return CodecError;
        }

        if (PreviousFieldParameters->DisplayCount[1] != 0)
        {
            SE_ERROR("(%s) - DisplayCount for second field non-zero after decoding only first field - Implementation error\n", Configuration.CodecName);
            Codec_MmeVideo_c::OutputPartialDecodeBuffersLocked();
            OS_UnLockMutex(&Lock);
            return PlayerImplementationError;
        }

        PreviousFieldParameters->DisplayCount[1]    = ParsedVideoParameters->DisplayCount[0];

        if ((PreviousFieldParameters->PanScanCount + ParsedVideoParameters->PanScanCount) >
            MAX_PAN_SCAN_VALUES)
        {
            SE_ERROR("(%s) - Cumulative PanScanCount in two fields too great (%d + %d) - Implementation error\n", Configuration.CodecName,
                     PreviousFieldParameters->PanScanCount, ParsedVideoParameters->PanScanCount);
            Codec_MmeVideo_c::OutputPartialDecodeBuffersLocked();
            OS_UnLockMutex(&Lock);
            return PlayerImplementationError;
        }

        unsigned int i;
        for (i = 0; i < ParsedVideoParameters->PanScanCount; i++)
        {
            memcpy(&PreviousFieldParameters->PanScan[i + PreviousFieldParameters->PanScanCount], &ParsedVideoParameters->PanScan[i], sizeof(PanScan_t));
        }
        PreviousFieldParameters->PanScanCount += ParsedVideoParameters->PanScanCount;

        // The PTS of ParsedFrameParameters attached to decode buffer is used to calculate the Playback Time.
        // The ParsedFrameParameters(MetaDataParsedFrameParametersReferenceType) of only first field of interlaced frame is attached to decode buffer.
        // So in case the PTS of second field is < PTS of first field (which can happen in case second field is to be displayed before the first field),
        // update the PTS of ParsedFrameParameters for first field attached to Decode Buffer.

        ParsedFrameParameters_t *PreviousParsedFrameParameters;
        BufferState[CurrentDecodeBufferIndex].Buffer->ObtainMetaDataReference(Player->MetaDataParsedFrameParametersReferenceType, (void **)(&PreviousParsedFrameParameters));
        SE_ASSERT(PreviousParsedFrameParameters != NULL);

        bool DisplaySecondFieldFirst = ((ParsedVideoParameters->PictureStructure == StructureTopField) && ParsedVideoParameters->TopFieldFirst) ||
                                       ((ParsedVideoParameters->PictureStructure == StructureBottomField) && !ParsedVideoParameters->TopFieldFirst);

        if ((PreviousParsedFrameParameters->PTS.IsValid()) &&
            (ParsedFrameParameters->PTS.IsValid()) && DisplaySecondFieldFirst &&
            (PreviousParsedFrameParameters->PTS > ParsedFrameParameters->PTS))
        {
            PreviousParsedFrameParameters->PTS = ParsedFrameParameters->PTS;
            SE_VERBOSE(group_decoder_video, "Update PTS of first field for Frame %d\n", PreviousParsedFrameParameters->DecodeFrameIndex);
        }

        // Attach the Coded Buffer of Second Field to Decode Buffer, in case Second Field carries User Data
        if (ParsedFrameParameters->UserDataNumber > 0)
        {
            BufferState[CurrentDecodeBufferIndex].Buffer->AttachBuffer(CodedBuffer);
        }
    }

    SE_VERBOSE(group_se_pipeline, "Stream 0x%p - %d - PTS=%lld GDB=%llu DecodeIndex=%u\n",
               Stream,
               Stream->GetStreamType(),
               ParsedFrameParameters->PTS.NativeValue(),
               OS_GetTimeInMicroSeconds(),
               ParsedFrameParameters->DecodeFrameIndex
              );

    //
    // Record the buffer being used in the decode context
    //
    DecodeContext->BufferIndex  = CurrentDecodeBufferIndex;
    //
    // Translate reference lists, and Update the reference frame access counts
    //
    Status  = TranslateReferenceFrameLists(ParsedVideoParameters->FirstSlice);
    if (Status != CodecNoError)
    {
        SE_ERROR("(%s) - Failed to find all reference frames - skipping frame\n", Configuration.CodecName);
        ReleaseDecodeContext(DecodeContext);

        if (BufferState[CurrentDecodeBufferIndex].ParsedParameters.Video->PictureStructure != StructureEmpty)
        {
            Codec_MmeVideo_c::OutputPartialDecodeBuffersLocked();
            OS_UnLockMutex(&Lock);
        }
        else
        {
            ReleaseDecodeBufferLocked(BufferState[CurrentDecodeBufferIndex].Buffer);
            CurrentDecodeBufferIndex = INVALID_INDEX;
            OS_UnLockMutex(&Lock);
        }

        if (ParsedFrameParameters->ReferenceFrame)
        {
            Codec_MmeVideo_c::ReleaseReferenceFrame(ParsedFrameParameters->DecodeFrameIndex);
        }

        return Status;
    }

    //
    // Provide default arguments for the input and output buffers. Default to no buffers not because there
    // are no buffers but because the video firmware interface uses a backdoor to gain access to the buffers.
    // Yes, this does violate the spec. but does nevertheless work on the current crop of systems.
    //
    DecodeContext->MMECommand.NumberInputBuffers        = 0;
    DecodeContext->MMECommand.NumberOutputBuffers       = 0;
    DecodeContext->MMECommand.DataBuffers_p         = NULL;
    //
    // Load the parameters into MME command
    //
    OS_UnLockMutex(&Lock);   // FIXME
    Status  = FillOutDecodeCommand();
    OS_LockMutex(&Lock);   // FIXME

    if (Status != CodecNoError)
    {
        SE_ERROR("(%s) - Failed to fill out a decode command\n", Configuration.CodecName);
        ReleaseDecodeContext(DecodeContext);

        if (BufferState[CurrentDecodeBufferIndex].ParsedParameters.Video->PictureStructure != StructureEmpty)
        {
            Codec_MmeVideo_c::OutputPartialDecodeBuffersLocked();
            OS_UnLockMutex(&Lock);
        }
        else
        {
            ReleaseDecodeBufferLocked(BufferState[CurrentDecodeBufferIndex].Buffer);
            CurrentDecodeBufferIndex = INVALID_INDEX;
            OS_UnLockMutex(&Lock);
        }

        if (ParsedFrameParameters->ReferenceFrame)
        {
            Codec_MmeVideo_c::ReleaseReferenceFrame(ParsedFrameParameters->DecodeFrameIndex);
        }

        return Status;
    }

    //
    // Update ongoing decode count, and completion flags
    //
    BufferState[CurrentDecodeBufferIndex].ParsedParameters.Video->PictureStructure   |= ParsedVideoParameters->PictureStructure;
    bool LastSlice      = Configuration.SliceDecodePermitted ? KnownLastSliceInFieldFrame : true;
    bool SeenBothFields = BufferState[CurrentDecodeBufferIndex].ParsedParameters.Video->PictureStructure == StructureFrame;
    bool LastDecodeIntoThisBuffer = SeenBothFields && LastSlice;
    DecodeContext->DecodeInProgress = true;
    DecodeContext->SliceType    = ParsedVideoParameters->SliceType;
    BufferState[CurrentDecodeBufferIndex].DecodesInProgress++;
    BufferState[CurrentDecodeBufferIndex].OutputOnDecodesComplete   = LastDecodeIntoThisBuffer;

    OS_UnLockMutex(&Lock);
    //
    // Ensure that the coded frame will be available throughout the
    // life of the decode by attaching the coded frame to the decode
    // context prior to launching the decode.
    //
    DecodeContextBuffer->AttachBuffer(CodedFrameBuffer);
    SE_VERBOSE(group_decoder_video, "Send DecodeCommand: DecodeFrameIndex %d DecodeContextIndex %d\n", ParsedFrameParameters->DecodeFrameIndex, DecodeContext->BufferIndex);

    //! set up MME_TRANSFORM - SendMMEDecodeCommand no longer does this as we need to do
    //! MME_SEND_BUFFERS instead for certain codecs, WMA being one, OGG Vorbis another
    DecodeContext->MMECommand.CmdCode = MME_TRANSFORM;
    bool AVDSyncOff = (Player->PolicyValue(Playback, Stream, PolicyAVDSynchronization) == PolicyValueDisapply);
    // jobs are ordered thanks to their due time , 0 means top prio, 1 background prio
    DecodeContext->MMECommand.DueTime = (AVDSyncOff) ? 1 : 0;

    // Save DecodeFrameIndex in local variable, as for StandAloneCodec cases decoding is synchronous and SendDecodeCommand
    // will return only after decoding is completed.
    // 'ParsedFrameParameters->DecodeFrameIndex' might get updated while decoding is ongoing for these codecs.
    unsigned int DecodeIndex = ParsedFrameParameters->DecodeFrameIndex;

    if (isStandAloneCodec())
    {
        Status  = SendDecodeCommand();
    }
    else
    {
        Status  = SendMMEDecodeCommand();
    }

    if (Status != CodecNoError)
    {
        SE_ERROR("(%s) - Failed to send a decode command\n", Configuration.CodecName);
        // FIXME: ?? include SendMMEDecodeCommand() call in lock ??
        OS_LockMutex(&Lock);
        ReleaseDecodeContext(DecodeContext);
        OS_UnLockMutex(&Lock);
        return Status;
    }

    //
    // have we finished decoding into this buffer
    //

    if (LastDecodeIntoThisBuffer)
    {
        CurrentDecodeBufferIndex = INVALID_INDEX;
        CurrentDecodeIndex       = INVALID_INDEX;
    }
    else
    {
        CurrentDecodeIndex      = DecodeIndex;
    }

    return CodecNoError;
}

// /////////////////////////////////////////////////////////////////////////
//
//  The intercept to the initialise data types function, that
//  ensures the video specific type is recorded in the configuration
//  record.
//

CodecStatus_t   Codec_MmeVideo_c::InitializeDataTypes()
{
    //
    // Add video specific types, and the address of
    // parsed video parameters to the configuration
    // record. Then pass on down to the base class
    //
    Configuration.AudioVideoDataParsedParametersType    = Player->MetaDataParsedVideoParametersType;
    Configuration.AudioVideoDataParsedParametersPointer = (void **)&ParsedVideoParameters;
    Configuration.SizeOfAudioVideoDataParsedParameters  = sizeof(ParsedVideoParameters_t);
    return Codec_MmeBase_c::InitializeDataTypes();
}

// /////////////////////////////////////////////////////////////////////////
//
//  The  video function used to determine the number of buffer to be used
//
void   Codec_MmeVideo_c::FillOutBufferCountRequest(DecodeBufferRequest_t   *Request)
{
    unsigned int Size        = Request->Dimension[0] * Request->Dimension[1] * 3 / 2;

    // Marker frame only , do not need to compute remaining field
    if (Size == 0) { return; }

    unsigned int    dpb_size = (Size > BUFFER_HD_SIZE) ? H264_MAX_4K2K_DPB_SIZE : H264_MAX_HD_DPB_SIZE;

    // computing max number of manifestation buffers
    unsigned int buffercount = min(dpb_size / Size, MAX_DPB_BUFFER) + 1 + MAX_DISPLAY_BUFFER;
    Request->ManifestationBufferCount = max(buffercount, MIN_MANIFESTATION_BUFFER_COUNT);

    //  Buffer allocation is over allocated due to display limitation
    if (ModuleParameter_Support64MbyteCrossing()) { Request->ManifestationBufferCount--; }

    // computing max number of references buffers
    buffercount = min(dpb_size / Size, MAX_DPB_BUFFER) + 1;
    Request->ReferenceBufferCount     = max(buffercount, MIN_REFERENCE_BUFFER_COUNT);

    // For Debug purpose
    if ((Request->ReferenceBufferCount  != mPreviousRequestReferenceBufferCount)
        || (Request->ManifestationBufferCount != mPreviousRequestManifestationBufferCount))
    {
        mPreviousRequestReferenceBufferCount = Request->ReferenceBufferCount;
        mPreviousRequestManifestationBufferCount = Request->ManifestationBufferCount ;
        SE_DEBUG(group_decoder_video, "%p Set Manifestation buffer count:%d reference buffer count:%d frame size %d x %d\n",
                 this , Request->ManifestationBufferCount , Request->ReferenceBufferCount, Request->Dimension[0] , Request->Dimension[1]);
    }
}


// /////////////////////////////////////////////////////////////////////////
//
//  The generic video function used to fill out a buffer structure
//  request.
//

CodecStatus_t   Codec_MmeVideo_c::FillOutDecodeBufferRequest(DecodeBufferRequest_t   *Request)
{
    bool DecimatedComponentNeeded;
    int  Decimate;


    if (ParsedVideoParameters == NULL)
    {
        SE_ERROR("(%s) - ParsedVideoParameters NULL\n", Configuration.CodecName);
        goto bail_err;
    }

    Decimate = Player->PolicyValue(Playback, Stream, PolicyDecimateDecoderOutput);
    //Check if the decimation value is supported by the codec
    if (!IsDecimationValueSupported(Decimate))
    {
        SE_WARNING_ONCE("(%s) - Decimation value not supported, would be disabled\n", Configuration.CodecName);
        Decimate = PolicyValueDecimateDecoderOutputDisabled;
    }

    DecimatedComponentNeeded = Configuration.DecimatedDecodePermitted &&
                               (Decimate != PolicyValueDecimateDecoderOutputDisabled);
    Stream->GetDecodeBufferManager()->ComponentEnable(DecimatedManifestationComponent, DecimatedComponentNeeded);
    Request->DimensionCount     = 2;
    Request->Dimension[0]       = ParsedVideoParameters->Content.DecodeWidth;
    Request->Dimension[1]       = ParsedVideoParameters->Content.DecodeHeight;

    // By default, set pixelDepth to 8 for all codecs
    Request->PixelDepth         = 8;

    //Policy values arranged in switch case blocks
    switch (Decimate)
    {
    case PolicyValueDecimateDecoderOutputH1V1:
        Request->DecimationFactors[0] = 1; //H
        Request->DecimationFactors[1] = 1; //V
        break;
    case PolicyValueDecimateDecoderOutputHalf:
        Request->DecimationFactors[0] = 2; //H
        Request->DecimationFactors[1] = 2; //V
        break;
    case PolicyValueDecimateDecoderOutputQuarter:
        Request->DecimationFactors[0] = 4; //H
        Request->DecimationFactors[1] = 2; //V
        break;
    case PolicyValueDecimateDecoderOutputH2V4:
        Request->DecimationFactors[0] = 2; //H
        Request->DecimationFactors[1] = 4; //V
        break;
    case PolicyValueDecimateDecoderOutputH2V8:
        Request->DecimationFactors[0] = 2;
        Request->DecimationFactors[1] = 8;
        break;
    case PolicyValueDecimateDecoderOutputH4V4:
        Request->DecimationFactors[0] = 4;
        Request->DecimationFactors[1] = 4;
        break;
    case PolicyValueDecimateDecoderOutputH4V8:
        Request->DecimationFactors[0] = 4;
        Request->DecimationFactors[1] = 8;
        break;
    case PolicyValueDecimateDecoderOutputH8V2:
        Request->DecimationFactors[0] = 8;
        Request->DecimationFactors[1] = 2;
        break;
    case PolicyValueDecimateDecoderOutputH8V4:
        Request->DecimationFactors[0] = 8;
        Request->DecimationFactors[1] = 4;
        break;
    case PolicyValueDecimateDecoderOutputH8V8:
        Request->DecimationFactors[0] = 8;
        Request->DecimationFactors[1] = 8;
        break;
    default:
        SE_VERBOSE(group_decoder_video, "(%s) - Continue without decimation\n", Configuration.CodecName);
        break;
    }

    if (ParsedFrameParameters == NULL)
    {
        SE_ERROR("(%s) -  ParsedFrameParameters NULL\n", Configuration.CodecName);
        goto bail_err;
    }
    Request->ReferenceFrame = ParsedFrameParameters->ReferenceFrame;
    Request->AlignmentNeededforInterlaced = false;
    FillOutBufferCountRequest(Request);

    return CodecNoError;

bail_err:
    //Default values to avoid div by 0 later ...
    if ((Request->DecimationFactors[0] == 0) || (Request->DecimationFactors[1] == 0))
    {
        Request->DecimationFactors[0] = 1;
        Request->DecimationFactors[1] = 1;
    }

    return CodecError;
}

// /////////////////////////////////////////////////////////////////////////
//
// Verify that the transformer is capable of correct operation.
//
// This method is always called before a transformer is initialised.
// This method overrides the base class so that we may query the
// DeltaTop Capabilities
//
CodecStatus_t Codec_MmeVideo_c::VerifyMMECapabilities(unsigned int ActualTransformer)
{
    MME_ERROR                        MMEStatus;
    MME_TransformerCapability_t      Capability;
    //
    // Query the capabilities of the DeltaTop
    //
    Configuration.DeltaTopCapabilityStructurePointer = (void *)(&DeltaTopTransformCapability);
    memset(&Capability, 0, sizeof(MME_TransformerCapability_t));
    memset(Configuration.DeltaTopCapabilityStructurePointer, 0, sizeof(DeltaTop_TransformerCapability_t));
    Capability.StructSize           = sizeof(MME_TransformerCapability_t);
    Capability.TransformerInfoSize  = sizeof(DeltaTop_TransformerCapability_t);
    Capability.TransformerInfo_p    = Configuration.DeltaTopCapabilityStructurePointer;
    MMEStatus                       = MME_GetTransformerCapability(DELTATOP_MME_TRANSFORMER_NAME "0", &Capability);

    if (MMEStatus != MME_SUCCESS)
    {
        SE_ERROR("(%s:%s) - Unable to read capabilities (%08x)\n", Configuration.CodecName, DELTATOP_MME_TRANSFORMER_NAME "0", MMEStatus);
        //
        // Failure to identify DELTATOP capabilities is not a fatal error. We may be on an old firmware
        //
    }

#if defined (CONFIG_STM_VIRTUAL_PLATFORM) /* VSOC WORKAROUND : Current native video delta firmware
                                             get capability function is stubbed, update manually... */
    DeltaTopTransformCapability.DecoderCapabilityFlags  = FLV1DEC_CAPABILITY  | VP6DEC_CAPABILITY | MPEG4P2DEC_CAPABILITY | AVSDECSD_CAPABILITY;
    DeltaTopTransformCapability.DecoderCapabilityFlags |= MPEG2DEC_CAPABILITY | VC1DEC_CAPABILITY | H264DEC_CAPABILITY;
    DeltaTopTransformCapability.DisplayBufferFormat     = DELTA_OUTPUT_RASTER;
#endif

    return Codec_MmeBase_c::VerifyMMECapabilities(ActualTransformer);
}

// //////////////////////////////////////////////////////////////////////////////
//
//  FillDecodeTimeStatistics to fill Statistics with DecodeTime returned from firmware.
//
//
void Codec_MmeVideo_c::FillDecodeTimeStatistics(unsigned int DecodeFrameIndex, int64_t pts)
{
    if (IsFieldPicture)
    {
        PictureDecodeTime *= 2;
    }

    // Do not use ParsedFrameParameters here
    SE_DEBUG2(group_se_pipeline, group_perf_video_decoder,
              "Stream 0x%p (dec idx: %d, PTS: %llu) hw frame decoding time: %u us\n",
              Stream, DecodeFrameIndex, pts, PictureDecodeTime);

    //To map first decode time as min DecodeTime
    if (!Stream->Statistics().MinVideoHwDecodeTime)
    {
        Stream->Statistics().MinVideoHwDecodeTime = PictureDecodeTime;
    }

    Stream->Statistics().MaxVideoHwDecodeTime = max(PictureDecodeTime , Stream->Statistics().MaxVideoHwDecodeTime);
    Stream->Statistics().MinVideoHwDecodeTime = min(PictureDecodeTime, Stream->Statistics().MinVideoHwDecodeTime);

    if (Stream->Statistics().FrameCountDecoded)
    {
        Stream->Statistics().AvgVideoHwDecodeTime = (PictureDecodeTime +
                                                     ((Stream->Statistics().FrameCountDecoded - 1) * Stream->Statistics().AvgVideoHwDecodeTime))
                                                    / (Stream->Statistics().FrameCountDecoded);
    }
    else
    {
        Stream->Statistics().AvgVideoHwDecodeTime = 0;
    }
}

//  To determine if Video Copy Buffer have to be used or not
//
//
bool Codec_MmeVideo_c::NoVideoCopyBuffer(enum hw_video_decoder HwCodec)
{
    int DecodeBuffPolicy = Player->PolicyValue(Playback, Stream, PolicyDecodeBufferCopy);

    // if default mode, module parameter enforces the copy buffer choice
    if (DecodeBuffPolicy == PolicyValueDefaultBufferCopyMode)
    {
        return ModuleParameter_NoVideoCopyBuffer();
    }

    // if not default mode, policy should match HW supported reference format
    if (HwCodec == DELTA_CODEC) { return (DecodeBuffPolicy == PolicyValueMBBuffOnly); }
    if (HwCodec == HADES_CODEC) { return (DecodeBuffPolicy == PolicyValueRasterBuffOnly); }

    return false;
}

// //////////////////////////////////////////////////////////////////////////////
//
// This function populates Video_Parameters with CEH register values.
//
//
void  Codec_MmeVideo_c::FillCEHRegisters(CodecBaseDecodeContext_t *Context, unsigned int *CEHRegisters)
{
    if ((Context == NULL) || (CEHRegisters == NULL))
    {
        SE_DEBUG(GetGroupTrace(), "Context=%p CEHRegisters=%p. Cannot fill CEH registers.\n", Context, CEHRegisters);
        return;
    }

    Buffer_t    CodedDataBuffer;
    ParsedVideoParameters_t  *DB_Video_Parameters = NULL;
    ParsedVideoParameters_t  *CPB_Video_Parameters = NULL;

    ParsedFrameParameters_t *Frame_Parameters = NULL;

    Context->DecodeContextBuffer->ObtainAttachedBufferReference(CodedFrameBufferType, &CodedDataBuffer);
    if (CodedDataBuffer != NULL)
    {
        CodedDataBuffer->ObtainMetaDataReference(Player->MetaDataParsedFrameParametersType, (void **)(&Frame_Parameters));
        CodedDataBuffer->ObtainMetaDataReference(Player->MetaDataParsedVideoParametersType, (void **)(&CPB_Video_Parameters));
    }

    BufferState[Context->BufferIndex].Buffer->ObtainMetaDataReference(Configuration.AudioVideoDataParsedParametersType,
                                                                      (void **)(&DB_Video_Parameters));

    if ((DB_Video_Parameters == NULL) || (Frame_Parameters == NULL) || (CPB_Video_Parameters == NULL))
    {
        SE_DEBUG(GetGroupTrace(), "PVP=%p PFP=%p. Cannot fill CEH registers.\n", DB_Video_Parameters, Frame_Parameters);
        return;
    }

    bool Frame  = (CPB_Video_Parameters->PictureStructure == StructureFrame);
    bool BottomField = (CPB_Video_Parameters->PictureStructure == StructureBottomField);

    // CEH parameters are stored in an array of 2*STM_SE_NUMBER_OF_CEH_INTERVALS elements to accomodate Interlaced usecase as {TOP_CEH,BOTTOM_CEH}.
    // In the progressive case, ceh values are read into first STM_SE_NUMBER_OF_CEH_INTERVALS indexes of the array, the rest indexes have value 0 e.g. {FRAME_CEH,0}
    // In the Interlaced TOP Field, ceh values are read into first STM_SE_NUMBER_OF_CEH_INTERVALS indexes of the array. {TOP_CEH,0}
    // In the Interlaced BOTTOM Field, ceh values are read into next STM_SE_NUMBER_OF_CEH_INTERVALS indexes of the array. {0,BOTTOM_CEH}
    // In the Interlaced Frame, ceh values are split for TOP and BOTTOM fields and stored in the array. {TOP_CEH,BOTTOM_CEH}

    if (CPB_Video_Parameters->InterlacedFrame)
    {
        // For Interlaced
        if (Frame)
        {
            // In case of a InterlacedFrame, CEH values are split into two parts and stored separately for Top and Bottom Fields.
            for (int i = 0; i < STM_SE_NUMBER_OF_CEH_INTERVALS; i++)
            {
                unsigned int Floor_Value = CEHRegisters[i] / 2;
                unsigned int Ceiling_Value = (CEHRegisters[i] - Floor_Value);
                bool CEHFieldToggle = DB_Video_Parameters->CEHFieldToggle;
                unsigned int Top_Value, Bottom_Value;

                // To distribute the CEH values in Top and Bottom arrays avoiding disparity in case of odd values.

                if (Floor_Value != Ceiling_Value)
                {
                    Top_Value = CEHFieldToggle  ? Floor_Value : Ceiling_Value;
                    Bottom_Value = CEHFieldToggle ? Ceiling_Value : Floor_Value ;
                    // Toggle CEHFieldToggle.
                    DB_Video_Parameters->CEHFieldToggle = !DB_Video_Parameters->CEHFieldToggle;
                }
                else
                {
                    Top_Value = Floor_Value;
                    Bottom_Value = Ceiling_Value;
                }


                DB_Video_Parameters->CEHReg_TopDefault[i] = Top_Value;
                DB_Video_Parameters->CEHReg_Bottom[i]     = Bottom_Value;
            }
            SE_VERBOSE(GetGroupTrace(), "DecodeFrameIndex %d Fill CEH InterlacedFrame \n", Frame_Parameters->DecodeFrameIndex);
        }
        else
        {
            // Read ceh values into lower STM_SE_NUMBER_OF_CEH_INTERVALS indexes in case of top field
            if (BottomField)
            {
                // Store ceh values for botton Field
                memcpy(DB_Video_Parameters->CEHReg_Bottom, CEHRegisters, sizeof(unsigned int) *STM_SE_NUMBER_OF_CEH_INTERVALS);
                SE_VERBOSE(GetGroupTrace(), "DecodeFrameIndex %d Fill CEH BottomField \n", Frame_Parameters->DecodeFrameIndex);
            }
            else
            {
                // Store ceh values for Top Field
                memcpy(DB_Video_Parameters->CEHReg_TopDefault, CEHRegisters, sizeof(unsigned int) *STM_SE_NUMBER_OF_CEH_INTERVALS);
                SE_VERBOSE(GetGroupTrace(), "DecodeFrameIndex %d Fill CEH TopField \n", Frame_Parameters->DecodeFrameIndex);
            }
        }
    }
    else
    {
        // For Progressive Streams
        SE_VERBOSE(GetGroupTrace(), "DecodeFrameIndex %d Fill CEH Frame \n", Frame_Parameters->DecodeFrameIndex);
        memcpy(DB_Video_Parameters->CEHReg_TopDefault, CEHRegisters, sizeof(unsigned int) *STM_SE_NUMBER_OF_CEH_INTERVALS);
    }
}

