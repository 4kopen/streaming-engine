/************************************************************************
Copyright (C) 2003-2014 STMicroelectronics. All Rights Reserved.

This file is part of the Streaming Engine.

Streaming Engine is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License version 2 as
published by the Free Software Foundation.

Streaming Engine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with Streaming Engine; see the file COPYING.  If not, write to the Free
Software Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307,
USA.

The Streaming Engine Library may alternatively be licensed under a
proprietary license from ST.
************************************************************************/

#ifndef H_CODEC_MME_VIDEO
#define H_CODEC_MME_VIDEO

#include <VideoCompanion.h>
#include "osinline.h"
#include "codec_mme_base.h"
#include "video_decoder_params.h"

#undef TRACE_TAG
#define TRACE_TAG "Codec_MmeVideo_c"

//
// Define to compute number of manifestation buffer and
// number of references
//

#define  BUFFER_HD_SIZE             1920*1088*3/2
#define  BUFFER_UHD_SIZE            3840*2160*3/2
#define  BUFFER_4K2K_SIZE           4096*2400*3/2

// from H264 standard level 4.1 and 5.1
#define  MAX_DPB_BUFFER             16
#define  H264_MAX_HD_DPB_SIZE       4 * BUFFER_HD_SIZE
#define  H264_MAX_4K2K_DPB_SIZE     5 * BUFFER_4K2K_SIZE

// from HEVC standard level 4.1 and 5/5.1/5.2
#define  HEVC_MAX_HD_DPB_SIZE       6 * BUFFER_HD_SIZE
#define  HEVC_MAX_4K2K_DPB_SIZE     6 * BUFFER_4K2K_SIZE

#define  MAX_HD_WIDTH               1920

#define  MAX_DISPLAY_BUFFER         3

// Manifestation buffer count should not be less than 9 for performances
#define  MIN_MANIFESTATION_BUFFER_COUNT  9

// Reference buffer count always between 7 and 17
// 7 needed for TC_STREAMING_FUNC_CODEC_VID_HEVC_MT41_RAP
// 5 for H264
#define  MIN_REFERENCE_BUFFER_COUNT  7


class Codec_MmeVideo_c : public Codec_MmeBase_c
{
public:
    Codec_MmeVideo_c();
    virtual ~Codec_MmeVideo_c() {}

    CodecStatus_t   Halt();

    //
    // Codec class functions
    //

    CodecStatus_t   Input(Buffer_t          CodedBuffer);

    //
    // Extension to base functions
    //

    CodecStatus_t   InitializeDataTypes();

    //
    // Implementation of fill out function for generic video,
    // may be overridden if necessary.
    //

    virtual CodecStatus_t   FillOutDecodeBufferRequest(DecodeBufferRequest_t     *Request);

protected:
    ParsedVideoParameters_t              *ParsedVideoParameters;
    bool                                  KnownLastSliceInFieldFrame;
    unsigned int                          PictureDecodeTime;
    bool                                  IsFieldPicture;
    int                                   RefBufferType;
    unsigned int                          mPreviousRequestReferenceBufferCount;
    unsigned int                          mPreviousRequestManifestationBufferCount;
    DeltaTop_TransformerCapability_t      DeltaTopTransformCapability;

    virtual  void   FillOutBufferCountRequest(DecodeBufferRequest_t     *Request);
    CodecStatus_t   VerifyMMECapabilities(unsigned int ActualTransformer);
    CodecStatus_t   VerifyMMECapabilities() { return VerifyMMECapabilities(SelectedTransformer); }
    void            FillDecodeTimeStatistics(unsigned int DecodeFrameIndex = INVALID_INDEX, int64_t pts = 0);
    void            FillCEHRegisters(CodecBaseDecodeContext_t *Context, unsigned int *CEHRegisters);

    // Success and failure codes are located entirely in the generic MME structures
    // allowing the super-class to determine whether the decode was successful. This
    // means that we have no work to do here.
    virtual CodecStatus_t   ValidateDecodeContext(CodecBaseDecodeContext_t *Context)
    {
        (void)Context; // warning removal
        return CodecNoError;
    }

    enum hw_video_decoder { DELTA_CODEC , HADES_CODEC };
    bool NoVideoCopyBuffer(enum hw_video_decoder HwCodec);

private:
    DISALLOW_COPY_AND_ASSIGN(Codec_MmeVideo_c);
};
#endif
