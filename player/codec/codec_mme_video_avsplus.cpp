/************************************************************************
Copyright (C) 2003-2014 STMicroelectronics. All Rights Reserved.

This file is part of the Streaming Engine.

Streaming Engine is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License version 2 as
published by the Free Software Foundation.

Streaming Engine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with Streaming Engine; see the file COPYING.  If not, write to the Free
Software Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307,
USA.

The Streaming Engine Library may alternatively be licensed under a
proprietary license from ST.
************************************************************************/

#include "player_threads.h"

#include "player_generic.h"
#include "player_playback.h"
#include "player_stream.h"
#include "parse_to_decode_edge.h"

#include "codec_mme_video_avsplus.h"
#include "avs.h"
#include "core_params.h"

#undef TRACE_TAG
#define TRACE_TAG "Codec_MmeVideoAvsPlus_c"

typedef struct AvsPlusCodecStreamParameterContext_s
{
    CodecBaseStreamParameterContext_t   BaseContext;

    MME_AVSPSetGlobalParamSequence_t     StreamParameters;
} AvsPlusCodecStreamParameterContext_t;

#define BUFFER_AVSPLUS_CODEC_STREAM_PARAMETER_CONTEXT             "AvsPlusCodecStreamParameterContext"
#define BUFFER_AVSPLUS_CODEC_STREAM_PARAMETER_CONTEXT_TYPE        {BUFFER_AVSPLUS_CODEC_STREAM_PARAMETER_CONTEXT, BufferDataTypeBase, AllocateFromOSMemory, 32, 0, true, true, sizeof(AvsPlusCodecStreamParameterContext_t)}

static BufferDataDescriptor_t            AvsPlusCodecStreamParameterContextDescriptor = BUFFER_AVSPLUS_CODEC_STREAM_PARAMETER_CONTEXT_TYPE;


// --------

// AVSP Intermediate Buffers
// Allocated in heap-style memory pools
// Note: - alignment of 8 to be checked with Hades designer
//       - min allocation size of 1024 to be fine tuned to avoid fragmentation and not spoil too much space

// Number of frames that must be in PP pools
#define AVSP_NB_OF_FRAME_BUFFERED           4

#define AVSP_SLICE_DATA_BUFFER        "AvsplusSliceDataBuf"
#define AVSP_SLICE_DATA_BUFFER_TYPE   {AVSP_SLICE_DATA_BUFFER, BufferDataTypeBase, AllocateFromSuppliedBlock, (HADESPP_BUFFER_ALIGNMENT+1), 1024, false, false, 0}

#define AVSP_SES_BUFFER               "AvsplusSESBuf"
#define AVSP_SES_BUFFER_TYPE          {AVSP_SES_BUFFER, BufferDataTypeBase, AllocateFromSuppliedBlock, (HADESPP_BUFFER_ALIGNMENT+1), 1024, false, false, 0}

static BufferDataDescriptor_t AvsPlusPreprocessorBufferDescriptor[] =
{
    AVSP_SLICE_DATA_BUFFER_TYPE,
    AVSP_SES_BUFFER_TYPE
};


typedef struct AvsPlusCodecDecodeContext_s
{
    CodecBaseDecodeContext_t            BaseContext;

    MME_AVSPVideoDecodeParams_t          DecodeParameters;
    MME_AVSPVideoDecodeReturnParams_t    DecodeStatus;
} AvsPlusCodecDecodeContext_t;

#define BUFFER_AVSPLUS_CODEC_DECODE_CONTEXT       "AvsPlusCodecDecodeContext"
#define BUFFER_AVSPLUS_CODEC_DECODE_CONTEXT_TYPE  {BUFFER_AVSPLUS_CODEC_DECODE_CONTEXT, BufferDataTypeBase, AllocateFromOSMemory, 32, 0, true, true, sizeof(AvsPlusCodecDecodeContext_t)}

static BufferDataDescriptor_t  AvsPlusCodecDecodeContextDescriptor = BUFFER_AVSPLUS_CODEC_DECODE_CONTEXT_TYPE;

#define BUFFER_AVSPLUS_PRE_PROCESSOR_BUFFER        "AvsPlusPreProcessorBuffer"

Codec_MmeVideoAvsPlus_c::Codec_MmeVideoAvsPlus_c()
    : AvsPlusTransformCapability()
    , AvsPlusInitializationParameters()
    , mPreProcessorBufferMaxSize()
    , mAvsPlusPreProcessorBufferDescriptor()
    , PreProcessorBufferPool(NULL)
    , LocalReferenceFrameList()
    , RasterOutput(false)
    , mDecodeBufferManager(NULL)
    , mPpFramesRing()
    , mDiscardMode(false)
    , mPreProcessorBufferAllocator()
    , mPreProcessorBufferType()
    , mPreprocessorBufferSize()
    , mPreProcessorBufferPool()
    , mPreProcessorDevice(NULL)
    , mIntThreadStopped()
{
    OS_SemaphoreInitialize(&mIntThreadStopped, 0);

    Configuration.CodecName                             = "AvsPlus video";
    Configuration.StreamParameterContextCount           = 1;
    Configuration.StreamParameterContextDescriptor      = &AvsPlusCodecStreamParameterContextDescriptor;
    Configuration.DecodeContextCount                    = 4;
    Configuration.DecodeContextDescriptor               = &AvsPlusCodecDecodeContextDescriptor;
    Configuration.MaxDecodeIndicesPerBuffer             = 2;
    Configuration.SliceDecodePermitted                  = false;
    Configuration.DecimatedDecodePermitted              = true;
    Configuration.TransformName[0]                      = AVSPDECHD_MME_TRANSFORMER_NAME "0";
    Configuration.TransformName[1]                      = AVSPDECHD_MME_TRANSFORMER_NAME "1";
    Configuration.AvailableTransformers                 = 2;
    Configuration.SizeOfTransformCapabilityStructure    = sizeof(AvsPlusTransformCapability);
    Configuration.TransformCapabilityStructurePointer   = (void *)(&AvsPlusTransformCapability);

    // The video firmware violates the MME spec. and passes data buffer addresses
    // as parametric information. For this reason it requires physical addresses
    // to be used.
    Configuration.AddressingMode                        = PhysicalAddress;

    //
    // Since we pre-process the data, we shrink the coded data buffers
    // before entering the generic code. as a consequence we do
    // not require the generic code to shrink buffers on our behalf,
    // and we do not want the generic code to find the coded data buffer
    // for us.
    //
    Configuration.ShrinkCodedDataBuffersAfterDecode     = false;
    Configuration.IgnoreFindCodedDataBuffer             = true;

    InitializationStatus = FinalizeInit();
}

Codec_MmeVideoAvsPlus_c::~Codec_MmeVideoAvsPlus_c()
{
    int ret = mPpFramesRing->InsertFakeEntry(0, ppActionTermination);
    if (ret < 0)
    {
        SE_ERROR("Failed to send ppActionTermination\n");
    }

    SE_DEBUG(group_player, "Waiting for AVS+ intermediate process to stop\n");
    OS_SemaphoreWaitAuto(&mIntThreadStopped);
    SE_DEBUG(group_player, "AVS+ intermediate process stopped\n");

    //
    // Make sure the pre-processor device is closed (note this may be done in halt,
    // but we check again, since you can enter reset without halt during a failed
    // start up).
    //

    if (mPreProcessorDevice != NULL)
    {
        HadesppClose(mPreProcessorDevice);
    }

    Halt();

    // Delete the frames in pre-processor chain ring
    delete mPpFramesRing;

    OS_SemaphoreTerminate(&mIntThreadStopped);
}

// /////////////////////////////////////////////////////////////////////////

CodecStatus_t   Codec_MmeVideoAvsPlus_c::Halt(void)
{
    DeallocatePreProcBufferPool();

    return Codec_MmeVideo_c::Halt();
}

// /////////////////////////////////////////////////////////////////////////
//
//      FinalizeInit finalizes init work by doing operations that may fail
//      (and such not doable in ctor)
//
CodecStatus_t Codec_MmeVideoAvsPlus_c::FinalizeInit()
{
    // Create the ring used to communicate with intermediate process (frame or synchro data)
    mPpFramesRing = new ppFramesRing_c<AvspPpFrame_t>(MAX_FRAME_NB_IN_PP_RING, 6);
    if (mPpFramesRing == NULL)
    {
        SE_ERROR("Failed to create ring to hold frames in pre-processor chain\n");
        return CodecError;
    }
    if (mPpFramesRing->FinalizeInit() != 0)
    {
        SE_ERROR("Failed to init PpFramesRing\n");
        return CodecError;
    }

    // Create the intermediate process
    OS_Thread_t  Thread;
    if (OS_CreateThread(&Thread, Codec_MmeVideoAvsPlus_IntermediateProcess,
                        this, &player_tasks_desc[SE_TASK_VIDEO_AVSPINT]) != OS_NO_ERROR)
    {
        SE_ERROR("Failed to create intermediate process\n");
        return CodecError;
    }

    return CodecNoError;
}


// /////////////////////////////////////////////////////////////////////////
//
//      Connect output port, we take this opportunity to
//      create the buffer pools for use in the pre-processor, and the
//      macroblock structure buffers
//
CodecStatus_t   Codec_MmeVideoAvsPlus_c::Connect(Port_c *Port)
{
    hadespp_status_t     PPStatus;
    unsigned int        i;

    // Let the ancestor classes setup the framework
    CodecStatus_t Status = Codec_MmeVideo_c::Connect(Port);
    if (Status != CodecNoError)
    {
        SetComponentState(ComponentInError);
        return Status;
    }

    //
    // Create pre-processor intermediate buffer types if they don't already
    // exist
    //
    for (i = 0; i < AVSP_NUM_INTERMEDIATE_POOLS; i ++)
    {
        Status = BufferManager->FindBufferDataType(
                     AvsPlusPreprocessorBufferDescriptor[i].TypeName,
                     &mPreProcessorBufferType[i]);

        if (Status != BufferNoError)
        {
            Status = BufferManager->CreateBufferDataType(
                         &AvsPlusPreprocessorBufferDescriptor[i],
                         &mPreProcessorBufferType[i]);

            if (Status != BufferNoError)
            {
                SE_ERROR("Failed to create the %s buffer type (%08x)\n",
                         AvsPlusPreprocessorBufferDescriptor[i].TypeName, Status);
                SetComponentState(ComponentInError);
                return CodecError;
            }
        }
    }

    PPStatus = HadesppOpen(&mPreProcessorDevice);
    if (PPStatus != hadespp_ok)
    {
        SE_ERROR("Failed to open the pre-processor device\n");
        SetComponentState(ComponentInError);
        return CodecError;
    }

    return CodecNoError;
}

// Check output of the PP and shrink PP buffers consequently (mandatory before
// launching a new preprocessing, in order to optimise PP pool usage)
// In case of PP error (CodecError is returned), PP buffers must be detached by caller
CodecStatus_t Codec_MmeVideoAvsPlus_c::CheckPPStatus(ppFrame_t<AvspPpFrame_t> *ppFrame)
{
    struct hadespp_ioctl_dequeue_t *ppEntry = &ppFrame->data.param.out;
    BufferStatus_t BufStatus;

    if ((ppEntry->hwStatus == hadespp_unrecoverable_error) || (ppEntry->hwStatus == hadespp_timeout))
    {
        SE_WARNING("Discarding picture with pre-processing timeout/unrecoverable pre-processing error\n");
        goto hw_failed;
    }
    else if ((ppEntry->hwStatus == hadespp_recoverable_error))
    {
        if (PolicyValueApply != Player->PolicyValue(Playback, Stream, PolicyAllowBadHevcPreProcessedFrames))
        {
            SE_INFO(group_decoder_video, "Due to policy PolicyAllowBadHevcPreProcessedFrames, picture with recoverable pre-processing error is discarded\n");
            goto hw_failed;
        }
    }

    if (Stream != NULL)
    {
        SE_VERBOSE(group_decoder_video, "Preproc decoding time: %lu us\n", (unsigned long)ppEntry->decode_time_hw);
        if (Stream->Statistics().MaxVideoHwPreprocTime < ppEntry->decode_time_hw)
        {
            Stream->Statistics().MaxVideoHwPreprocTime = ppEntry->decode_time_hw;
        }
        if ((Stream->Statistics().MinVideoHwPreprocTime == 0) && (ppEntry->decode_time_hw != 0))
        {
            Stream->Statistics().MinVideoHwPreprocTime = ppEntry->decode_time_hw;
        }
        if (Stream->Statistics().MinVideoHwPreprocTime > ppEntry->decode_time_hw)
        {
            Stream->Statistics().MinVideoHwPreprocTime = ppEntry->decode_time_hw;
        }
        if (Stream->Statistics().FrameCountDecoded != 0)
        {
            Stream->Statistics().AvgVideoHwPreprocTime =
                (ppEntry->decode_time_hw + ((Stream->Statistics().FrameCountDecoded - 1) *
                                            Stream->Statistics().AvgVideoHwPreprocTime)) /
                (Stream->Statistics().FrameCountDecoded);
        }
    }

    ppFrame->data.CodedBuffer->SetUsedDataSize(0);
    BufStatus = ppFrame->data.CodedBuffer->ShrinkBuffer(0);
    if (BufStatus != BufferNoError)
    {
        SE_INFO(group_decoder_video, "Failed to shrink coded buffer\n");
    }

    // Shrink intermediate buffers
    // Caution: do not shrink to zero otherwise the next ObtainDataReference() will return NULL
#define SHRINK_IB(INDEX, LENGTH) \
    {                                                \
        int new_length = ppEntry->iStatus.avsp_iStatus.LENGTH;     \
        if (new_length == 0)                         \
        new_length = 4; /* keep the buffer allocated */                \
        if (ppFrame->data.PreProcessorBuffer[INDEX]) {                          \
            ppFrame->data.PreProcessorBuffer[INDEX]->SetUsedDataSize( new_length ); \
            ppFrame->data.PreProcessorBuffer[INDEX]->ShrinkBuffer( new_length );    \
        }                                                                  \
    }
    //TODO rm + AVSP_PP_MAX_SESB_SIZE (fw dependent)
    SHRINK_IB(AVSP_SLICE_DATA, slice_data_stop_offset + AVSP_PP_MAX_SESB_SIZE);
    SHRINK_IB(AVSP_SESB      , sesb_stop_offset);

    SE_DEBUG(group_decoder_video,
             "slice_data_stop_offset:%d sesb_stop_offset:%d "
             "slice_table_entries:%d error:0x%08x\n",
             ppEntry->iStatus.avsp_iStatus.slice_data_stop_offset,
             ppEntry->iStatus.avsp_iStatus.sesb_stop_offset,
             ppEntry->iStatus.avsp_iStatus.slice_table_entries,
             ppEntry->iStatus.avsp_iStatus.error);

    return CodecNoError;

hw_failed:
    if (ppFrame->data.ParsedFrameParameters->FirstParsedParametersForOutputFrame)
    {
        Stream->ParseToDecodeEdge->RecordNonDecodedFrame(ppFrame->data.CodedBuffer,
                                                         ppFrame->data.ParsedFrameParameters);
    }
    else
    {
        OutputPartialDecodeBuffers();
    }

    ppFrame->data.CodedBuffer->SetUsedDataSize(0);
    BufStatus = ppFrame->data.CodedBuffer->ShrinkBuffer(0);
    if (BufStatus != BufferNoError)
    {
        SE_INFO(group_decoder_video, "Failed to shrink coded buffer\n");
    }

    return CodecError;
}

// /////////////////////////////////////////////////////////////////////////
//
//      Input, must feed the data to the preprocessor chain.
//      This function needs to replicate a considerable amount of the
//      ancestor classes, because this function is operating
//      in a different process to the ancestors, and because the
//      ancestor data, and the data used here, will be in existence
//      at the same time.
//

CodecStatus_t Codec_MmeVideoAvsPlus_c::Input(Buffer_t                  CodedBuffer)
{
    unsigned int              i, j;
    void                     *IntermediateBufferPhysAddr[AVSP_NUM_INTERMEDIATE_POOLS];
    unsigned int              CodedDataLength;
    void                     *CodedDataPhysAddr;
    BufferStatus_t            BufferStatus;
    hadespp_status_t          PPStatus;
    ParsedFrameParameters_t  *ParsedFrameParameters;
    ParsedVideoParameters_t  *ParsedVideoParameters;

    // First extract the useful pointers from the buffer all held locally
    CodedBuffer->ObtainMetaDataReference(Player->MetaDataParsedFrameParametersType, (void **)(&ParsedFrameParameters));
    SE_ASSERT(ParsedFrameParameters != NULL);

    CodedBuffer->ObtainMetaDataReference(Player->MetaDataParsedVideoParametersType, (void **)(&ParsedVideoParameters));
    SE_ASSERT(ParsedVideoParameters != NULL);

    AvsFrameParameters_t *FrameParameters = (AvsFrameParameters_t *)ParsedFrameParameters->FrameParameterStructure;
    AvsStreamParameters_t *ParsedStreamParameters = (AvsStreamParameters_t *)ParsedFrameParameters->StreamParameterStructure;
    AvsVideoPictureHeader_t *PictureHeader = &FrameParameters->PictureHeader;

    // If this does not contain any frame data, then we simply wish
    // to slot it into place for the intermediate process.
    int ret = 0;
    ppFrame_t<AvspPpFrame_t> ppFrame;
    if (!ParsedFrameParameters->NewFrameParameters ||
        (ParsedFrameParameters->DecodeFrameIndex == INVALID_INDEX))
    {
        new(&ppFrame) ppFrame_t<AvspPpFrame_t>(ppActionPassOnFrame, ParsedFrameParameters->DecodeFrameIndex);
        ppFrame.data.CodedBuffer = CodedBuffer;
        ppFrame.data.ParsedFrameParameters = ParsedFrameParameters;

        CodedBuffer->IncrementReferenceCount(IdentifierAvsPlusIntermediate);
        ret = mPpFramesRing->Insert(&ppFrame);
        if (ret < 0)
        {
            SE_ERROR("Failed to insert frame for PP\n");
            CodedBuffer->DecrementReferenceCount(IdentifierAvsPlusIntermediate);
            return CodecError;
        }
        return CodecNoError;
    }

    // We believe we have a frame - check that this is marked as a first slice
    if (!ParsedVideoParameters->FirstSlice)
    {
        SE_ERROR("Non first slice, when one is expected\n");
        return CodecError;
    }

    // Allocate intermediate buffers and attach them to coded buffer
    bool allocatePoolNeeded = false;
    for (i = 0; i < AVSP_NUM_INTERMEDIATE_POOLS; i++)
    {
        if (mPreProcessorBufferPool[i] == NULL)
        {
            allocatePoolNeeded = true;
            break;
        }
    }
    ret = UpdatePreprocessorBufferSize(ParsedVideoParameters->Content.Width, ParsedVideoParameters->Content.Height);
    if (ret < 0)
    {
        SE_ERROR("Unable to get PPBuffer size\n");
        return CodecError;
    }
    if ((ret > 0) || (allocatePoolNeeded))
    {
        if (mPreProcessorBufferPool[AVSP_SLICE_DATA] != NULL)
        {
            SE_INFO(group_decoder_video, "PPBuffer size has changed -> updating the PP Pool size\n");

            //
            // Waiting for end of ALL PP pending tasks
            // Before destroying the pool ensure that no buffer in the pool is in use.
            // With a highly corrupted H264 stream it has been observed that the coded buffer
            // along with the attached PP buffer exits the PP ring but before it is sent
            // for decode, DeallocatePreProcBufferPool gets invoked due to level change
            // resulting in assertion SE_ASSERT(PreProcessorBuffer != NULL) in FillOutDecodeCommand
            // Since all PP buffer pools are allocated and deallocated together checking pool
            // usage of one pool is enough
            //
            unsigned int BuffersWithNonZeroReferenceCount;
            unsigned long long wait_start = OS_GetTimeInMicroSeconds();
            unsigned long long waiting_time_in_us;
            do
            {
                OS_SleepMilliSeconds(2); // jiffies granularity is around 10ms anyway
                mPreProcessorBufferPool[AVSP_SLICE_DATA]->GetPoolUsage(NULL,
                                                                       &BuffersWithNonZeroReferenceCount, NULL, NULL, NULL, NULL);
                waiting_time_in_us = (OS_GetTimeInMicroSeconds() - wait_start) / 1000;
                if (waiting_time_in_us > 700)
                {
                    SE_WARNING("Before reallocation, waiting for PreProcBufferPool usage to go to zero during %llu us\n", waiting_time_in_us);
                }
            }
            while (BuffersWithNonZeroReferenceCount != 0);

            SE_VERBOSE(group_decoder_video, "Ready to re-allocate preproc buffer pool after %llu ms\n", waiting_time_in_us);
        }
        if (CodecNoError != AllocatePreProcBufferPool())
        {
            SE_ERROR("Unable to allocate PP buffer pools\n");
            //free pool/allocator if any memory allocated
            DeallocatePreProcBufferPool();
            return CodecError;
        }
    }

    new(&ppFrame) ppFrame_t<AvspPpFrame_t>(ppActionPassOnFrame, ParsedFrameParameters->DecodeFrameIndex);
    for (i = 0; i < AVSP_NUM_INTERMEDIATE_POOLS; i++)
    {
        // SE_DEBUG(group_decoder_video, "PreprocessorBufferSize %d = %d\n", i, mPreprocessorBufferSize[i]);
        /* Request for a pp buffer with aligned size as start addr will be aligned */
        BufferStatus = mPreProcessorBufferPool[i]->GetBuffer(&ppFrame.data.PreProcessorBuffer[i],
                                                             UNSPECIFIED_OWNER,
                                                             BUF_ALIGN_UP(mPreprocessorBufferSize[i], HADESPP_BUFFER_ALIGNMENT));

        if ((BufferStatus != BufferNoError) || (ppFrame.data.PreProcessorBuffer[i] == NULL))
        {
            BufferStatus = BufferError;
            break;
        }

        CodedBuffer->AttachBuffer(ppFrame.data.PreProcessorBuffer[i]);
        // Now that buffer is attached to the coded buffer we can release our hold on it
        ppFrame.data.PreProcessorBuffer[i]->DecrementReferenceCount();
        // Need to store physical address for preprocessor
        ppFrame.data.PreProcessorBuffer[i]->ObtainDataReference(NULL,
                                                                NULL,
                                                                &IntermediateBufferPhysAddr[i],
                                                                PhysicalAddress);
        SE_ASSERT(IntermediateBufferPhysAddr[i] != NULL); // not expected to be empty ?
    }

    // Failed to allocate all buffers. Free already allocated buffers if any
    // and return error
    if (BufferStatus != BufferNoError)
    {
        for (j = 0; j < i; j++)
        {
            CodedBuffer->DetachBuffer(ppFrame.data.PreProcessorBuffer[j]);
        }

        SE_ERROR("Failed to get a pre-processor buffer of type %d (%08x)\n", j, BufferStatus);
        return CodecError;
    }

    // Fill in relevant fields
    ppFrame.data.CodedBuffer           = CodedBuffer;
    ppFrame.data.ParsedFrameParameters = ParsedFrameParameters;

    // Preprocessor driver requires coded buffer's physical
    CodedBuffer->ObtainDataReference(NULL, &CodedDataLength, &CodedDataPhysAddr, PhysicalAddress);
    SE_ASSERT(CodedDataPhysAddr != NULL);

    // Prepare data structure for preprocessor driver
    struct hadespp_ioctl_queue_t *param = &ppFrame.data.param.in;
    avspluspreproc_transform_param_t *ppCmd = &param->iCmd.avsp_iCmd;
    param->codec = CODEC_AVSP;

    FillOutPreprocessorCommand(ppCmd, ParsedStreamParameters, PictureHeader);

    param->iDecodeIndex = ParsedFrameParameters->DecodeFrameIndex;
    ppCmd->InputBufferPhysicalAddress  = CodedDataPhysAddr;
    ppCmd->InputBufferSize             = CodedDataLength;

    //TODO fw expects contiguous buffer -> split AVSP_SLICE_DATA in 2 (sesb_length hardcoded in fw to AVSP_PP_MAX_SESB_SIZE!!!)
    //ppCmd->sesb_start = (uint32_t)IntermediateBufferPhysAddr[AVSP_SESB];
    ppCmd->sesb_start = (uint32_t)IntermediateBufferPhysAddr[AVSP_SLICE_DATA];
    ppCmd->sesb_length = mPreprocessorBufferSize[AVSP_SESB];
    ppCmd->slice_data_start = (uint32_t)IntermediateBufferPhysAddr[AVSP_SLICE_DATA] + ppCmd->sesb_length;
    ppCmd->slice_data_length =  mPreprocessorBufferSize[AVSP_SLICE_DATA];

    CodedBuffer->IncrementReferenceCount(IdentifierAvsPlusIntermediate);

    PPStatus = HadesppPreProcessBuffer(mPreProcessorDevice, &ppFrame.data.param);
    if ((PPStatus == hadespp_error) || (PPStatus == hadespp_unrecoverable_error))
    {
        SE_ERROR("Failed to retrieve a buffer from the pre-processor status: %d\n", PPStatus);
    }

    if (CodecNoError != CheckPPStatus(&ppFrame))
    {
        SE_ERROR("PP returned unrecoverable error\n");
        goto pp_error;
    }

    /* Add buffer to ring buffer so Intermediate process can pick it up later
       (must be inline with HadesppQueueBuffer calls) */
    ret = mPpFramesRing->Insert(&ppFrame);
    if (ret < 0)
    {
        SE_ERROR("Failed to insert PP frame[%d]\n", ppFrame.DecodeFrameIndex);
        return CodecError;
    }

    return CodecNoError;

pp_error:
    for (i = 0; i < AVSP_NUM_INTERMEDIATE_POOLS; i++)
    {
        CodedBuffer->DetachBuffer(ppFrame.data.PreProcessorBuffer[i]);
    }

    CodedBuffer->DecrementReferenceCount(IdentifierAvsPlusIntermediate);
    return CodecError;
}

// /////////////////////////////////////////////////////////////////////////
//
//      UpdatePreprocessorBufferSize
//      computes the maximum size of intermediate buffers (Slice Data Buffer, SESB)
//      Returns < 0 if error, > 0 if PreprocessorBufferSize changed, 0 otherwise
//

int Codec_MmeVideoAvsPlus_c::UpdatePreprocessorBufferSize(uint32_t w, uint32_t h)
{
    int ret = 0;

    // TODO fix this as soon as hardcoded value in fw is removed
    mPreprocessorBufferSize[AVSP_SESB] = AVSP_PP_MAX_SESB_SIZE; //AVSP_PP_MAX_SLICES * 8;  // 137 slices x 8 bytes

    uint32_t mb_nb = ((w + 16) * (h + 16)) / 256;
    uint32_t slice_data_size = AVSP_PP_MAX_SLICES * 30 + mb_nb * 792;
    slice_data_size = BUF_ALIGN_UP(slice_data_size, HADESPP_BUFFER_ALIGNMENT);

    if (mPreprocessorBufferSize[AVSP_SLICE_DATA] < slice_data_size)
    {
        SE_INFO(group_decoder_video, "Slice_data AVS+ buffer changed\n");
        ret = 1;
    }

    mPreprocessorBufferSize[AVSP_SLICE_DATA] = slice_data_size;
    SE_DEBUG(group_decoder_video, "size of AVS+ IB: sesb: %d  slice_data: %d\n",
             mPreprocessorBufferSize[AVSP_SESB], slice_data_size);
    return ret;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////

CodecStatus_t Codec_MmeVideoAvsPlus_c::AllocatePreProcBufferPool()
{
    unsigned int PPPoolSize[AVSP_NUM_INTERMEDIATE_POOLS];
    unsigned int TotalPPSize = 0;
    int i = 0;

    //free pool/allocator if memory is already allocated
    DeallocatePreProcBufferPool();

    // Allocation in reverse order -> to allocate big buffers first
    for (i = 0; i < AVSP_NUM_INTERMEDIATE_POOLS ; i++)
    {
        PPPoolSize[i] = AVSP_NB_OF_FRAME_BUFFERED * mPreprocessorBufferSize[i];
        PPPoolSize[i] = BUF_ALIGN_UP(PPPoolSize[i], HADESPP_BUFFER_ALIGNMENT);
        TotalPPSize += PPPoolSize[i];
        SE_INFO(group_decoder_video, "[ALLOC] Requesting PP pool[%d] of size: %d bytes\n", i, PPPoolSize[i]);
    }

    allocator_status_t AllocatorStatus = PartitionAllocatorOpen(&mPreProcessorBufferAllocator,
                                                                Configuration.AncillaryMemoryPartitionName,
                                                                TotalPPSize, MEMORY_VIDEO_HWONLY_ACCESS);

    if (AllocatorStatus != allocator_ok)
    {
        SE_ERROR("Failed to allocate pre-processor buffer partition (%08x)\n",
                 AllocatorStatus);
        SetComponentState(ComponentInError);
        // No need to free existing allocators, dtor() will take care of it
        return CodecError;
    }

    void *PPBufferMemoryPool[3];
    BufferStatus_t Status;
    PPBufferMemoryPool[PhysicalAddress] = AllocatorPhysicalAddress(mPreProcessorBufferAllocator);
    mDecodeBufferManager = Stream->GetDecodeBufferManager();

    for (i = 0; i < AVSP_NUM_INTERMEDIATE_POOLS; i++)
    {
        Status = BufferManager->CreatePool(&mPreProcessorBufferPool[i],
                                           mPreProcessorBufferType[i], NOT_SPECIFIED, PPPoolSize[i],
                                           PPBufferMemoryPool, NULL, NULL, true, MEMORY_VIDEO_HWONLY_ACCESS);

        if (Status != BufferNoError)
        {
            SE_ERROR("Failed to create pre-processor buffer pool of type %s (status: %08x)\n",
                     AvsPlusPreprocessorBufferDescriptor[i].TypeName, Status);
            SetComponentState(ComponentInError);
            // No need to destroy existing pools, dtor() will take care of it
            return CodecError;
        }
        PPBufferMemoryPool[PhysicalAddress] = (void *)((unsigned int)PPBufferMemoryPool[PhysicalAddress] + PPPoolSize[i]);
        mDecodeBufferManager->RegisterPreprocBufferPool(mPreProcessorBufferPool[i], i);
    }

    SE_INFO(group_decoder_video, "[ALLOC] Total size of PP pools: %d bytes\n", TotalPPSize);
    return CodecNoError;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////

void Codec_MmeVideoAvsPlus_c::DeallocatePreProcBufferPool()
{
    // Free buffer pools
    for (int i = 0; i < AVSP_NUM_INTERMEDIATE_POOLS; i ++)
    {
        if (mPreProcessorBufferPool[i] != NULL)
        {
            BufferManager->DestroyPool(mPreProcessorBufferPool[i]);
            mPreProcessorBufferPool[i] = NULL;
            mDecodeBufferManager->RegisterPreprocBufferPool(NULL, i);
        }
    }
    AllocatorClose(&mPreProcessorBufferAllocator);
}


//{{{  HandleCapabilities
// /////////////////////////////////////////////////////////////////////////
//
//      Function to deal with the returned capabilities
//      structure for an avs mme transformer.
//

CodecStatus_t   Codec_MmeVideoAvsPlus_c::HandleCapabilities()
{
    // Default to using Omega2 unless Capabilities tell us otherwise
    BufferFormat_t  DisplayFormat = FormatVideo420_MacroBlock;
    // Default elements to produce in the buffers
    DecodeBufferComponentElementMask_t Elements = PrimaryManifestationElement | DecimatedManifestationElement | VideoMacroblockStructureElement;
    DeltaTop_TransformerCapability_t *DeltaTopCapabilities = (DeltaTop_TransformerCapability_t *) Configuration.DeltaTopCapabilityStructurePointer;
    SE_INFO(group_decoder_video, "MME Transformer '%s' capabilities are :-\n", AVSPDECHD_MME_TRANSFORMER_NAME);
    SE_DEBUG(group_decoder_video, "    Version                           = %x\n", AvsPlusTransformCapability.api_version);
    RasterOutput = false;

    if ((DeltaTopCapabilities != NULL) && (DeltaTopCapabilities->DisplayBufferFormat == DELTA_OUTPUT_RASTER))
    {
        RasterOutput = true;
    }

    if (NoVideoCopyBuffer(DELTA_CODEC))
    {
        SE_WARNING("Overwrite: default DisplayBufferFormat received from firmware %s by %s\n", PLAYER_STRINGIFY(DELTA_OUTPUT_RASTER), PLAYER_STRINGIFY(DELTA_OUTPUT_OMEGA2));
        //Currently set simply to OMEGA2; Once SOC gets RASTER2B capability for decode buffer, it shall be configured accordingly.
        DeltaTopTransformCapability.DisplayBufferFormat = DELTA_OUTPUT_OMEGA2;
        RefBufferType = PolicyValueMBBuffOnly;
        RasterOutput = false;
    }

    if (RasterOutput)
    {
        DisplayFormat = FormatVideo420_Raster2B;
        Elements |= VideoDecodeCopyElement;
    }
    // AVS do not support to Have Ondemand allocation , buffer must come from a Pool
    Elements |= VideoMacroblockPoolStructureElement;

    Stream->GetDecodeBufferManager()->FillOutDefaultList(DisplayFormat,
                                                         Elements,
                                                         Configuration.ListOfDecodeBufferComponents);
    return CodecNoError;
}

//{{{  FillOutTransformerInitializationParameters
// /////////////////////////////////////////////////////////////////////////
//
//      Function to deal with the returned capabilities
//      structure for an avs mme transformer.
//

CodecStatus_t   Codec_MmeVideoAvsPlus_c::FillOutTransformerInitializationParameters()
{
    SE_VERBOSE(group_decoder_video, "\n");

    // Fill out the command parameters
    AvsPlusInitializationParameters.CircularBufferBeginAddr_p = (U32 *)0x00000000;
    AvsPlusInitializationParameters.CircularBufferEndAddr_p   = (U32 *)0xffffffff;
    AvsPlusInitializationParameters.IntraMB_struct_ptr        = NULL;
#if (AVSP_HD_MME_VERSION >= 11)
    AvsPlusInitializationParameters.SdpMode = 0;
#endif
    // Fill out the actual command
    MMEInitializationParameters.TransformerInitParamsSize = sizeof(AvsPlusInitializationParameters);
    MMEInitializationParameters.TransformerInitParams_p   = (MME_GenericParams_t)(&AvsPlusInitializationParameters);

    return CodecNoError;
}
//}}}

//{{{  FillOutSetStreamParametersCommand
// /////////////////////////////////////////////////////////////////////////
//
//      Function to fill out the stream parameters
//      structure for an avs mme transformer.
//

CodecStatus_t   Codec_MmeVideoAvsPlus_c::FillOutSetStreamParametersCommand()
{
    AvsStreamParameters_t *Parsed  = (AvsStreamParameters_t *)ParsedFrameParameters->StreamParameterStructure;
    AvsPlusCodecStreamParameterContext_t *Context = (AvsPlusCodecStreamParameterContext_t *)StreamParameterContext;
    //
    // Fill out the command parameters
    //
    Context->StreamParameters.Width  = Parsed->SequenceHeader.horizontal_size;
    Context->StreamParameters.Height = Parsed->SequenceHeader.vertical_size;
    Context->StreamParameters.Progressive_sequence = (AVSP_SeqSyntax_t)Parsed->SequenceHeader.progressive_sequence;
    Context->StreamParameters.Profile_id           = Parsed->SequenceHeader.profile_id;
    //
    // Fill out the actual command
    //
    memset(&Context->BaseContext.MMECommand, 0, sizeof(MME_Command_t));
    Context->BaseContext.MMECommand.CmdStatus.AdditionalInfoSize = 0;
    Context->BaseContext.MMECommand.CmdStatus.AdditionalInfo_p   = NULL;
    Context->BaseContext.MMECommand.ParamSize                    = sizeof(Context->StreamParameters);
    Context->BaseContext.MMECommand.Param_p                      = (MME_GenericParams_t)(&Context->StreamParameters);
    return CodecNoError;
}


//}}}
//{{{  FillOutPreprocessorCommand
// /////////////////////////////////////////////////////////////////////////
//
//      Function to fill out the preprocessor parameters
//      structure for an avs mme transformer.
//
CodecStatus_t   Codec_MmeVideoAvsPlus_c::FillOutPreprocessorCommand(avspluspreproc_transform_param_t *ppCmd,
                                                                    AvsStreamParameters_t    *ParsedStreamParameters,
                                                                    AvsVideoPictureHeader_t  *PictureHeader)
{
    unsigned int j;

    // AVSP-PP :: for AVS_PP_CFG2 register #########
    ppCmd->avsp_mb_horizontal_size   = ParsedStreamParameters->SequenceHeader.horizontal_size;;
    ppCmd->avsp_mb_vertical_size     = ParsedStreamParameters->SequenceHeader.vertical_size; ;
    ppCmd->avsp_aec_enable           = PictureHeader->aec_enable;
    ppCmd->avsp_skip_mode_flag       = PictureHeader->skip_mode_flag;
    ppCmd->avsp_chroma_format        = ParsedStreamParameters->SequenceHeader.chroma_format;
    ppCmd->avsp_picture_structure    = PictureHeader->picture_structure;
    ppCmd->avsp_picture_type         = PictureHeader->picture_coding_type;
    ppCmd->avsp_picture_ref_flag     = PictureHeader->picture_reference_flag;
    ppCmd->avsp_prevPictureType      = 0;
    ppCmd->avsp_outEndianness        = 0;

    // AVSP-PP :: for AVS_PP_CFG3 register #########
    ppCmd->avsp_fixed_pic_qp                   = PictureHeader->fixed_picture_qp;
    ppCmd->avsp_wq_flag                        = PictureHeader->weighting_quant_flag;
    ppCmd->avsp_mb_adapt_wq_disable            = 0;
    ppCmd->avsp_wq_model                       = PictureHeader->weighting_quant_model;
    ppCmd->avsp_wq_param_idx                   = PictureHeader->weighting_quant_param_index;
    ppCmd->avsp_picture_qp                     = PictureHeader->picture_qp;
    ppCmd->avsp_chroma_quant_param_disable     = PictureHeader->chroma_quant_param_disable;
    ppCmd->avsp_chroma_quant_param_delta_cb    = PictureHeader->chroma_quant_param_delta_cb;
    ppCmd->avsp_chroma_quant_param_delta_cr    = PictureHeader->chroma_quant_param_delta_cr;

    // AVSP-PP :: for AVS_PP_CFG4-5-6 register #########
    for (j = 0; j < 6; j++)
    {
        ppCmd->avsp_weighting_quant_param_delta1[j] = PictureHeader->weighting_quant_param_delta1[j];
        ppCmd->avsp_weighting_quant_param_delta2[j] = PictureHeader->weighting_quant_param_delta2[j];
    }

    return CodecNoError;
}

//}}}
//{{{  FillOutDecodeCommand
// /////////////////////////////////////////////////////////////////////////
//
//      Function to fill out the decode parameters
//      structure for an avs mme transformer.
//

CodecStatus_t   Codec_MmeVideoAvsPlus_c::FillOutDecodeCommand()
{
    unsigned int                 i;
    Buffer_t                     PreProcessorBuffer;
    unsigned int                 InputBuffer[AVSP_NUM_INTERMEDIATE_POOLS];
    unsigned int                 InputBufferSize[AVSP_NUM_INTERMEDIATE_POOLS];
    AvsPlusCodecDecodeContext_t *Context = (AvsPlusCodecDecodeContext_t *)DecodeContext;
    AvsFrameParameters_t        *Parsed = (AvsFrameParameters_t *)ParsedFrameParameters->FrameParameterStructure;
    AvsVideoPictureHeader_t     *PictureHeader = &Parsed->PictureHeader;
    MME_AVSPVideoDecodeParams_t *Param;
    AVSP_DecodedBufferAddress_t *Decode;
    AVSP_RefPicListAddress_t    *RefList;
    AVSP_DisplayBufferAddress_t *Display;
    Buffer_t                     DecodeBuffer;
    Buffer_t                     ReferenceBuffer;
    unsigned int                 Entry;

    SE_VERBOSE(group_decoder_video, "Fill Out Decode Command\n");
    OS_LockMutex(&Lock);

    //
    // Detach the pre-processor buffers and re-attach them to the decode context
    // this will make its release automatic on the freeing of the decode context.
    //
    for (i = 0; i < AVSP_NUM_INTERMEDIATE_POOLS; i++)
    {
        CodedFrameBuffer->ObtainAttachedBufferReference(mPreProcessorBufferType[i], &PreProcessorBuffer);
        SE_ASSERT(PreProcessorBuffer != NULL);

        DecodeContextBuffer->AttachBuffer(PreProcessorBuffer);      // Must be ordered, otherwise the pre-processor
        CodedFrameBuffer->DetachBuffer(PreProcessorBuffer);         // buffer will get released in the middle
        // Store address and size of buffer
        PreProcessorBuffer->ObtainDataReference(NULL, InputBufferSize + i, (void **)&InputBuffer[i], PhysicalAddress);
        SE_ASSERT(InputBuffer[i] != NULL); // not expected to be empty ? .. TBC
    }

    //
    // Fill out the sub-components of the command data
    //
    memset(&Context->DecodeParameters, 0, sizeof(MME_AVSPVideoDecodeParams_t));
    memset(&Context->DecodeStatus, 0xa5, sizeof(MME_AVSPVideoDecodeReturnParams_t));

    // For avs we do not do slice decodes.
    KnownLastSliceInFieldFrame = true;
    DecodeBuffer = BufferState[CurrentDecodeBufferIndex].Buffer;
    Param   = &Context->DecodeParameters;
    Decode  = &Param->DecodedBufferAddr;
    RefList = &Param->RefPicListAddr;

    DecodeBufferComponentType_t DecodeComponent = RasterOutput ? VideoDecodeCopy : PrimaryManifestationComponent;
    Decode->Luma_p   = (AVSP_LumaAddress_t)Stream->GetDecodeBufferManager()->Luma(DecodeBuffer, DecodeComponent);
    Decode->Chroma_p = (AVSP_ChromaAddress_t)Stream->GetDecodeBufferManager()->Chroma(DecodeBuffer, DecodeComponent);
    Display          = &Param->DisplayBufferAddr;
    Display->StructSize = sizeof(AVSP_DisplayBufferAddress_t);

    if (RefBufferType == PolicyValueMBBuffOnly)
    {
        Display->DisplayLuma_p   = NULL;
        Display->DisplayChroma_p = NULL;
    }
    else
    {
        /* Raster2B Display Buffer */
        DecodeBufferComponentType_t DisplayComponent = PrimaryManifestationComponent;
        Display->DisplayLuma_p    = (AVSP_LumaAddress_t)Stream->GetDecodeBufferManager()->Luma(DecodeBuffer, DisplayComponent);
        Display->DisplayChroma_p  = (AVSP_ChromaAddress_t)Stream->GetDecodeBufferManager()->Chroma(DecodeBuffer, DisplayComponent);
    }

    DecodeBufferComponentType_t DecimatedComponent = DecimatedManifestationComponent;
    if (Stream->GetDecodeBufferManager()->ComponentPresent(DecodeBuffer, DecimatedComponent))
    {
        Display->DisplayDecimatedLuma_p   = (AVSP_LumaAddress_t)Stream->GetDecodeBufferManager()->Luma(DecodeBuffer, DecimatedComponent);
        Display->DisplayDecimatedChroma_p = (AVSP_ChromaAddress_t)Stream->GetDecodeBufferManager()->Chroma(DecodeBuffer, DecimatedComponent);
    }
    else
    {
        Display->DisplayDecimatedLuma_p   = NULL;
        Display->DisplayDecimatedChroma_p = NULL;
    }

    Decode->MBStruct_p = (unsigned int *)Stream->GetDecodeBufferManager()->ComponentAddress(VideoMacroblockStructure, PhysicalAddress);

    // TODO fix this correct buffer offsets!!!
    Param->PictureStartAddr_p = (AVSP_CompressedData_t)(InputBuffer[AVSP_SLICE_DATA]);
    Param->PictureEndAddr_p   = (AVSP_CompressedData_t)(InputBuffer[AVSP_SLICE_DATA] + InputBufferSize[AVSP_SLICE_DATA]);

    //{{{  Fill out the reference frame lists
    if (ParsedFrameParameters->NumberOfReferenceFrameLists != 0)
    {
        if (DecodeContext->ReferenceFrameList[0].EntryCount > 0)
        {
            Entry                        = DecodeContext->ReferenceFrameList[0].EntryIndicies[0];
            ReferenceBuffer              = BufferState[Entry].Buffer;
            RefList->BackwardRefLuma_p   = (AVSP_LumaAddress_t)Stream->GetDecodeBufferManager()->Luma(ReferenceBuffer, DecodeComponent);
            RefList->BackwardRefChroma_p = (AVSP_ChromaAddress_t)Stream->GetDecodeBufferManager()->Chroma(ReferenceBuffer, DecodeComponent);
        }

        if (DecodeContext->ReferenceFrameList[0].EntryCount > 1)
        {
            Entry                       = DecodeContext->ReferenceFrameList[0].EntryIndicies[1];
            ReferenceBuffer             = BufferState[Entry].Buffer;
            RefList->ForwardRefLuma_p   = (AVSP_LumaAddress_t)Stream->GetDecodeBufferManager()->Luma(ReferenceBuffer, DecodeComponent);
            RefList->ForwardRefChroma_p = (AVSP_ChromaAddress_t)Stream->GetDecodeBufferManager()->Chroma(ReferenceBuffer, DecodeComponent);
        }
    }

    //}}}
    //{{{  Fill in remaining fields
    Param->Progressive_frame            = (AVSP_FrameSyntax_t)PictureHeader->progressive_frame;
    Param->AebrFlag                     = 1;
    Param->Picture_structure            = (AVSP_PicStruct_t)PictureHeader->picture_structure;
    Param->Picture_structure_bwd        = (AVSP_PicStruct_t)PictureHeader->picture_structure;
    Param->Fixed_picture_qp             = (MME_UINT)PictureHeader->fixed_picture_qp;
    Param->Picture_qp                   = (MME_UINT)PictureHeader->picture_qp;
    Param->Skip_mode_flag               = (AVSP_SkipMode_t)PictureHeader->skip_mode_flag;
    Param->Loop_filter_disable          = (MME_UINT)PictureHeader->loop_filter_disable;
    Param->alpha_offset                 = (S32)PictureHeader->alpha_c_offset;
    Param->beta_offset                  = (S32)PictureHeader->beta_offset;
    Param->Picture_ref_flag             = (AVSP_PicRef_t)PictureHeader->picture_reference_flag;
    Param->tr                           = (S32)PictureHeader->tr;
    Param->imgtr_next_P                 = (S32)PictureHeader->imgtr_next_P;
    Param->imgtr_last_P                 = (S32)PictureHeader->imgtr_last_P;
    Param->imgtr_last_prev_P            = (S32)PictureHeader->imgtr_last_prev_P;
    // To do
    Param->field_flag                   = (AVSP_FieldSyntax_t)0;
    Param->topfield_pos                 = (U32)PictureHeader->top_field_offset;
    Param->botfield_pos                 = (U32)PictureHeader->bottom_field_offset;
    Param->DecodingMode                 = AVSP_NORMAL_DECODE;
    Param->AdditionalFlags              = AVSP_ADDITIONAL_FLAG_CEH;
    Param->FrameType                    = (AVSP_PictureType_t)PictureHeader->picture_coding_type;

    Param->weighting_quant_flag         = PictureHeader->weighting_quant_flag;
    Param->chroma_quant_param_disable   = PictureHeader->chroma_quant_param_disable;
    Param->pb_field_enhanced_flag       = PictureHeader->pb_field_enhanced_flag;
    Param->chroma_quant_param_delta_cb  = PictureHeader->chroma_quant_param_delta_cb;
    Param->chroma_quant_param_delta_cr  = PictureHeader->chroma_quant_param_delta_cr;

#if (AVSP_HD_MME_VERSION >= 11)
    Param->IsSecure                     = 0;
#endif

    if (!Stream->GetDecodeBufferManager()->ComponentPresent(DecodeBuffer, DecimatedComponent))
    {
        // Normal Case
        if (RasterOutput && ParsedFrameParameters->ReferenceFrame)
        {
            Param->MainAuxEnable = AVSP_REF_MAIN_DISP_MAIN_EN;
        }
        else
        {
            Param->MainAuxEnable = AVSP_DISP_MAIN_EN;
        }
        Param->HorizontalDecimationFactor = AVSP_HDEC_1;
        Param->VerticalDecimationFactor   = AVSP_VDEC_1;
    }
    else
    {
        if (RasterOutput && ParsedFrameParameters->ReferenceFrame)
        {
            Param->MainAuxEnable = AVSP_REF_MAIN_DISP_MAIN_AUX_EN;
        }
        else
        {
            Param->MainAuxEnable = AVSP_DISP_AUX_MAIN_EN;
        }
        Param->HorizontalDecimationFactor = (Stream->GetDecodeBufferManager()->DecimationFactor(DecodeBuffer, 0) == 2) ?
                                            AVSP_HDEC_ADVANCED_2 :
                                            AVSP_HDEC_ADVANCED_4;

        if (Param->Progressive_frame)
        {
            Param->VerticalDecimationFactor = AVSP_VDEC_ADVANCED_2_PROG;
        }
        else
        {
            Param->VerticalDecimationFactor  = AVSP_VDEC_ADVANCED_2_INT;
        }
    }

    if (RefBufferType == PolicyValueMBBuffOnly)
    {
        if (Stream->GetDecodeBufferManager()->ComponentPresent(DecodeBuffer, DecimatedComponent))
        {
            //Reference (MB format) and decimation (Raster2B) buffers generated
            Param->MainAuxEnable = AVSP_REF_MAIN_DISP_AUX_EN;
        }
        else
        {
            //Only reference buffers (MB format) are generated; No display buffers
            Param->MainAuxEnable = AVSP_REF_MAIN_EN;
        }
    }

    //{{{  Fill out slice list if HD decode
    AVSP_StartCodecsParam_t *StartCodes = &Param->StartCodecs;
    StartCodes->SliceCount              = Parsed->SliceHeaderList.no_slice_headers;

    for (unsigned int i = 0; i < StartCodes->SliceCount; i++)
    {
        StartCodes->SliceArray[i].SliceStartAddrCompressedBuffer_p = (AVSP_CompressedData_t)(CodedData + Parsed->SliceHeaderList.slice_array[i].slice_offset);
        StartCodes->SliceArray[i].SliceAddress                     = Parsed->SliceHeaderList.slice_array[i].slice_start_code;
    }
    OS_UnLockMutex(&Lock);
    //}}}
    // Fill out the actual command
    memset(&Context->BaseContext.MMECommand, 0, sizeof(MME_Command_t));
    Context->BaseContext.MMECommand.CmdStatus.AdditionalInfoSize = sizeof(Context->DecodeStatus);
    Context->BaseContext.MMECommand.CmdStatus.AdditionalInfo_p   = (MME_GenericParams_t)(&Context->DecodeStatus);
    Context->BaseContext.MMECommand.ParamSize                    = sizeof(Context->DecodeParameters);
    Context->BaseContext.MMECommand.Param_p                      = (MME_GenericParams_t)(&Context->DecodeParameters);
    return CodecNoError;
}
//}}}
//{{{  DumpSetStreamParameters
// /////////////////////////////////////////////////////////////////////////
//
//      Function to dump out the set stream
//      parameters from an mme command.
//

CodecStatus_t   Codec_MmeVideoAvsPlus_c::DumpSetStreamParameters(void    *Parameters)
{
    MME_AVSPSetGlobalParamSequence_t    *StreamParameters        = (MME_AVSPSetGlobalParamSequence_t *)Parameters;
    SE_VERBOSE(group_decoder_video, "Progressive  %6d\n", StreamParameters->Progressive_sequence);
    SE_VERBOSE(group_decoder_video, "Width  %6d\n", StreamParameters->Width);
    SE_VERBOSE(group_decoder_video, "Height  %6d\n", StreamParameters->Height);
    SE_VERBOSE(group_decoder_video, "Profile_id  %6d\n", StreamParameters->Profile_id);
    return CodecNoError;
}
//}}}
//{{{  DumpDecodeParameters
// /////////////////////////////////////////////////////////////////////////
//
//      Function to dump out the decode
//      parameters from an mme command.
//

CodecStatus_t   Codec_MmeVideoAvsPlus_c::DumpDecodeParameters(void    *Parameters)
{
    MME_AVSPVideoDecodeParams_t         *Param = (MME_AVSPVideoDecodeParams_t *)Parameters;
    AVSP_DecodedBufferAddress_t         *Decode = &Param->DecodedBufferAddr;
    AVSP_DisplayBufferAddress_t         *Display = &Param->DisplayBufferAddr;
    AVSP_RefPicListAddress_t            *RefList = &Param->RefPicListAddr;

    SE_VERBOSE(group_decoder_video, "  Param->PictureStartAddr_p                  %p\n", Param->PictureStartAddr_p);
    SE_VERBOSE(group_decoder_video, "  Param->PictureEndAddr_p                    %p\n", Param->PictureEndAddr_p);
    SE_VERBOSE(group_decoder_video, "  Decode->Luma_p                             %p\n", Decode->Luma_p);
    SE_VERBOSE(group_decoder_video, "  Decode->Chroma_p                           %p\n", Decode->Chroma_p);
    SE_VERBOSE(group_decoder_video, "  Decode->MBStruct__p                        %p\n", Decode->MBStruct_p);
    SE_VERBOSE(group_decoder_video, "  RefList->ForwardRefLuma_p                  %p\n", RefList->ForwardRefLuma_p);
    SE_VERBOSE(group_decoder_video, "  RefList->ForwardRefChroma_p                %p\n", RefList->ForwardRefChroma_p);
    SE_VERBOSE(group_decoder_video, "  RefList->BackwardRefLuma_p                 %p\n", RefList->BackwardRefLuma_p);
    SE_VERBOSE(group_decoder_video, "  RefList->BackwardRefChroma_p               %p\n", RefList->BackwardRefChroma_p);
    SE_VERBOSE(group_decoder_video, "  Param->Progressive_frame                   %x\n", Param->Progressive_frame);
    SE_VERBOSE(group_decoder_video, "  Param->MainAuxEnable                       %x\n", Param->MainAuxEnable);
    SE_VERBOSE(group_decoder_video, "  Param->HorizontalDecimationFactor          %x\n", Param->HorizontalDecimationFactor);
    SE_VERBOSE(group_decoder_video, "  Param->VerticalDecimationFactor            %x\n", Param->VerticalDecimationFactor);
    SE_VERBOSE(group_decoder_video, "  Param->AebrFlag                            %x\n", Param->AebrFlag);
    SE_VERBOSE(group_decoder_video, "  Param->Picture_structure                   %x\n", Param->Picture_structure);
    SE_VERBOSE(group_decoder_video, "  Param->Picture_structure_bwd               %x\n", Param->Picture_structure_bwd);
    SE_VERBOSE(group_decoder_video, "  Param->Fixed_picture_qp                    %x\n", Param->Fixed_picture_qp);
    SE_VERBOSE(group_decoder_video, "  Param->Picture_qp                          %x\n", Param->Picture_qp);
    SE_VERBOSE(group_decoder_video, "  Param->Skip_mode_flag                      %x\n", Param->Skip_mode_flag);
    SE_VERBOSE(group_decoder_video, "  Param->Loop_filter_disable                 %x\n", Param->Loop_filter_disable);
    SE_VERBOSE(group_decoder_video, "  Param->alpha_offset                        %x\n", Param->alpha_offset);
    SE_VERBOSE(group_decoder_video, "  Param->beta_offset                         %x\n", Param->beta_offset);
    SE_VERBOSE(group_decoder_video, "  Param->Picture_ref_flag                    %x\n", Param->Picture_ref_flag);
    SE_VERBOSE(group_decoder_video, "  Param->tr                                  %x\n", Param->tr);
    SE_VERBOSE(group_decoder_video, "  Param->imgtr_next_P                        %x\n", Param->imgtr_next_P);
    SE_VERBOSE(group_decoder_video, "  Param->imgtr_last_P                        %x\n", Param->imgtr_last_P);
    SE_VERBOSE(group_decoder_video, "  Param->imgtr_last_prev_P                   %x\n", Param->imgtr_last_prev_P);
    SE_VERBOSE(group_decoder_video, "  Param->field_flag                          %x\n", Param->field_flag);
    SE_VERBOSE(group_decoder_video, "  Param->DecodingMode                        %x\n", Param->DecodingMode);
    SE_VERBOSE(group_decoder_video, "  Param->AdditionalFlags                     %x\n", Param->AdditionalFlags);
    SE_VERBOSE(group_decoder_video, "  Param->FrameType                           %x\n", Param->FrameType);
    SE_VERBOSE(group_decoder_video, "  Param->weighting_quant_flag                %x\n", Param->weighting_quant_flag);
    SE_VERBOSE(group_decoder_video, "  Param->chroma_quant_param_disable          %x\n", Param->chroma_quant_param_disable);
    SE_VERBOSE(group_decoder_video, "  Param->pb_field_enhanced_flag              %x\n", Param->pb_field_enhanced_flag);
    SE_VERBOSE(group_decoder_video, "  Param->chroma_quant_param_delta_cb         %x\n", Param->chroma_quant_param_delta_cb);
    SE_VERBOSE(group_decoder_video, "  Param->chroma_quant_param_delta_cr         %x\n", Param->chroma_quant_param_delta_cr);

    SE_VERBOSE(group_decoder_video, "  Display->Luma_p                             %p\n", Display->DisplayLuma_p);
    SE_VERBOSE(group_decoder_video, "  Display->Chroma_p                           %p\n", Display->DisplayChroma_p);
    SE_VERBOSE(group_decoder_video, "  Display->LumaDecim_p                        %p\n", Display->DisplayDecimatedLuma_p);
    SE_VERBOSE(group_decoder_video, "  Display->ChromaDecim_p                      %p\n", Display->DisplayDecimatedChroma_p);

    return CodecNoError;
}
//}}}

//{{{  CheckCodecReturnParameters
// Convert the return code into human readable form.
static const char *LookupError(unsigned int Error)
{
#define E(e) case e: return #e

    switch (Error)
    {
        E(AVSP_DECODER_ERROR_MB_OVERFLOW);
        E(AVSP_DECODER_ERROR_RECOVERED);
        E(AVSP_DECODER_ERROR_NOT_RECOVERED);
        E(AVSP_DECODER_ERROR_TASK_TIMEOUT);

    default: return "AVSP_DECODER_UNKNOWN_ERROR";
    }

#undef E
}

CodecStatus_t   Codec_MmeVideoAvsPlus_c::CheckCodecReturnParameters(CodecBaseDecodeContext_t *Context)
{
    MME_Command_t                    *MMECommand       = (MME_Command_t *)(&Context->MMECommand);
    MME_CommandStatus_t              *CmdStatus        = (MME_CommandStatus_t *)(&MMECommand->CmdStatus);
    MME_AVSPVideoDecodeReturnParams_t *AdditionalInfo_p = (MME_AVSPVideoDecodeReturnParams_t *)CmdStatus->AdditionalInfo_p;
    AvsPlusCodecDecodeContext_t      *AVSPContext       = (AvsPlusCodecDecodeContext_t *)Context;

    if (AdditionalInfo_p != NULL)
    {
        FillCEHRegisters(Context, AVSPContext->DecodeStatus.CEHRegisters);

        //
        // Get the decode time from firmware for each frame (for field, decode time is doubled)
        //
        PictureDecodeTime = AdditionalInfo_p->DecodeTimeInMicros;
        IsFieldPicture = !AVSPContext->DecodeParameters.Progressive_frame;
        Codec_MmeVideo_c::FillDecodeTimeStatistics();

#if 0  /*Should be removed when fixed on firmware side, firmware returns unknown decode error for good decode
         on Cannes2.5Cut1 there is no ERC only TIMEOUT errors are significant*/
        if (AdditionalInfo_p->Errortype != AVSP_DECODER_NO_ERROR)
#else
        if (AdditionalInfo_p->Errortype == AVSP_DECODER_ERROR_TASK_TIMEOUT)
#endif
        {
            // Set the decode quality to worst because there is a decoding error and there is no emulation.
            // This is done to allow flexibity for not displaying the corrupt output, using player policy, after encountering the decoding error.
            Context->DecodeQuality = 0;
            SE_INFO(group_decoder_video, "%s  0x%x\n", LookupError(AdditionalInfo_p->Errortype), AdditionalInfo_p->Errortype);

            switch (AdditionalInfo_p->Errortype)
            {
            case AVSP_DECODER_ERROR_MB_OVERFLOW:
                Stream->Statistics().FrameDecodeMBOverflowError++;
                break;

            case AVSP_DECODER_ERROR_RECOVERED:
                Stream->Statistics().FrameDecodeRecoveredError++;
                break;

            case AVSP_DECODER_ERROR_NOT_RECOVERED:
                Stream->Statistics().FrameDecodeNotRecoveredError++;
                break;

            case AVSP_DECODER_ERROR_TASK_TIMEOUT:
                Stream->Statistics().FrameDecodeErrorTaskTimeOutError++;
                break;

            default:
                Stream->Statistics().FrameDecodeError++;
                break;
            }
        }
    }
    return CodecNoError;
}
//}}}

// /////////////////////////////////////////////////////////////////////////
//
//      The intermediate process, taking output from the pre-processor
//      and feeding it to the lower level of the codec.
//

void *Codec_MmeVideoAvsPlus_IntermediateProcess(void *Parameter)
{
    Codec_MmeVideoAvsPlus_c    *Codec = (Codec_MmeVideoAvsPlus_c *)Parameter;
    Codec->IntermediateProcess();
    OS_TerminateThread();
    return NULL;
}



void Codec_MmeVideoAvsPlus_c::IntermediateProcess()
{
    PlayerStatus_t   Status;
    unsigned int     i;
    Buffer_t         PreProcessorBuffer;
    bool             Terminating = false;
    ParsedFrameParameters_t *FrameParameters;

    SE_INFO(group_decoder_video, "Starting Intermediate Process Thread\n");
    ppFrame_t<AvspPpFrame_t> *ppFrame = new ppFrame_t<AvspPpFrame_t>();
    if (!ppFrame)
    {
        SE_ERROR("Failed to allocate ppFrame\n");
        return;
    }

    // Main loop
    while (!Terminating)
    {
        // Blocking call to get next ppFrame
        if (0 > mPpFramesRing->Extract(ppFrame))
        {
            continue;
        }

        // If SE is in low power state, process should be kept inactive,
        // as we are not allowed to issue any MME commands
        if (Stream && Stream->IsLowPowerState())
        {
            ppFrame->Action = ppActionNull;
        }

        // Process activity - note aborted activity differs in consequences for each action.
        switch (ppFrame->Action)
        {
        case ppActionTermination:
            Terminating = true;
            continue;

        case ppActionPassOnPreProcessedFrame:  //TODO rm
        case ppActionNull:
            break;

        case ppActionEnterDiscardMode:
            mDiscardMode = true;
            break;

        case ppActionCallOutputPartialDecodeBuffers:
            if (Stream)
            {
                Codec_MmeVideo_c::OutputPartialDecodeBuffers();
            }
            break;

        case ppActionCallDiscardQueuedDecodes:
            mDiscardMode = false;
            Codec_MmeVideo_c::DiscardQueuedDecodes();
            break;

        case ppActionCallReleaseReferenceFrame:
            Codec_MmeVideo_c::ReleaseReferenceFrame(ppFrame->DecodeFrameIndex);
            break;

        case ppActionPassOnFrame:
            // Clear the NewStreamParameters flag so base class does not send a Set Stream Parameter MME command
            FrameParameters = NULL;
            ppFrame->data.CodedBuffer->ObtainMetaDataReference(Player->MetaDataParsedFrameParametersType,
                                                               (void **)(&FrameParameters));
            SE_ASSERT(FrameParameters != NULL);

            FrameParameters->NewStreamParameters = false;
            SE_VERBOSE(group_decoder_video, ">DEC IB input decodeIdx: %d PTS: %lu, mDiscardMode: %d\n",
                       FrameParameters->DecodeFrameIndex,
                       (unsigned long) FrameParameters->PTS.PtsValue(), mDiscardMode);

            // By default release buffers
            Status = CodecNoError;
            if (!mDiscardMode || (ppFrame->DecodeFrameIndex == INVALID_INDEX))
            {
                // Now mimic the input procedure as done in the player process (send command to decode)
                Status = Codec_MmeVideo_c::Input(ppFrame->data.CodedBuffer);
            }
            if ((Status != CodecNoError) || mDiscardMode)
            {
                if (Stream && FrameParameters->FirstParsedParametersForOutputFrame)
                {
                    Stream->ParseToDecodeEdge->RecordNonDecodedFrame(ppFrame->data.CodedBuffer, FrameParameters);
                    Codec_MmeVideo_c::OutputPartialDecodeBuffers();
                }

                // If pre processor buffers are still attached to the coded buffer, make sure they get freed
                if (ppFrame->DecodeFrameIndex != INVALID_INDEX)
                {
                    for (i = 0; i < AVSP_NUM_INTERMEDIATE_POOLS; i++)
                    {
                        ppFrame->data.CodedBuffer->ObtainAttachedBufferReference(mPreProcessorBufferType[i], &PreProcessorBuffer);
                        if (PreProcessorBuffer != NULL)
                        {
                            ppFrame->data.CodedBuffer->DetachBuffer(PreProcessorBuffer);
                        }
                    }
                }

                if (Status == DecodeBufferManagerFailedToAllocateComponents)
                {
                    SE_ERROR("Stream 0x%p Codec input failed for dbm components allocation - marking stream unplayable\n", Stream);
                    // raise the event to signal stream unplayable
                    Stream->MarkUnPlayable(STM_SE_PLAY_STREAM_MSG_REASON_CODE_INSUFFICIENT_MEMORY, true);
                }
            }
            SE_VERBOSE(group_decoder_video, "<DEC %d\n", FrameParameters->DecodeFrameIndex);
            break;
        }

        if (ppFrame->data.CodedBuffer)
        {
            ppFrame->data.CodedBuffer->DecrementReferenceCount(IdentifierAvsPlusIntermediate);
        }
    }

    // Clean up the ring
    while (!mPpFramesRing->IsEmpty())
    {
        mPpFramesRing->Extract(ppFrame);

        if (ppFrame->data.CodedBuffer != NULL)
        {
            ppFrame->data.CodedBuffer->DecrementReferenceCount(IdentifierAvsPlusIntermediate);
        }
    }

    delete ppFrame;
    SE_INFO(group_decoder_video, "Exiting AVSPlus IntermediateProcess\n");

    // We are not accessing any more member variables so it is safe to destroy this thread now
    OS_SemaphoreSignal(&mIntThreadStopped);
}

// /////////////////////////////////////////////////////////////////////////
//
//      Discard queued decodes, this will include any between here
//      and the preprocessor.
//

CodecStatus_t   Codec_MmeVideoAvsPlus_c::DiscardQueuedDecodes()
{
    int ret = mPpFramesRing->InsertFakeEntryHead(0, ppActionEnterDiscardMode);
    if (ret < 0)
    {
        SE_ERROR("Failed to insert DiscardQueuedDecodes cmd\n");
        return CodecError;
    }
    ret = mPpFramesRing->InsertFakeEntry(0, ppActionCallDiscardQueuedDecodes);
    if (ret < 0)
    {
        SE_ERROR("Failed to DiscardQueuedDecodes\n");
        return CodecError;
    }
    return CodecNoError;
}

// /////////////////////////////////////////////////////////////////////////
//
//      Output partial decode buffers - not entirely sure what to do here,
//      it may well be necessary to fire off the pre-processing of any
//      accumulated slices.
//

CodecStatus_t   Codec_MmeVideoAvsPlus_c::OutputPartialDecodeBuffers()
{
    int ret = mPpFramesRing->InsertFakeEntry(0, ppActionCallOutputPartialDecodeBuffers);
    if (ret < 0)
    {
        SE_ERROR("Failed to OutputPartialDecodeBuffers\n");
        return CodecError;
    }
    return CodecNoError;
}

// /////////////////////////////////////////////////////////////////////////
//
//      Release reference frame, this must be capable of releasing a
//      reference frame that has not yet exited the pre-processor.
//

CodecStatus_t   Codec_MmeVideoAvsPlus_c::ReleaseReferenceFrame(unsigned int ReferenceFrameDecodeIndex)
{
    int ret = mPpFramesRing->InsertFakeEntry(ReferenceFrameDecodeIndex,
                                             ppActionCallReleaseReferenceFrame);
    if (ret < 0)
    {
        SE_ERROR("Failed to ReleaseReferenceFrame\n");
        return CodecError;
    }
    return CodecNoError;
}

// /////////////////////////////////////////////////////////////////////////
//
//      Check reference frame list needs to account for those currently
//      in the pre-processor chain.
//

CodecStatus_t   Codec_MmeVideoAvsPlus_c::CheckReferenceFrameList(
    unsigned int              NumberOfReferenceFrameLists,
    ReferenceFrameList_t      ReferenceFrameList[])
{
    // Check we can cope
    if (NumberOfReferenceFrameLists > 2)
    {
        SE_ERROR("AVSP has only %d reference frame lists, requested to check %d\n", 2, NumberOfReferenceFrameLists);
        return CodecUnknownFrame;
    }

    // Construct local lists consisting of those elements we do not know about
    bool refFound;
    for (int i = 0; i < NumberOfReferenceFrameLists; i++)
    {
        LocalReferenceFrameList[i].EntryCount   = 0;

        for (int j = 0; j < ReferenceFrameList[i].EntryCount; j++)
        {
            refFound = mPpFramesRing->CheckRefListDecodeIndex(ReferenceFrameList[i].EntryIndicies[j]);
            if (!refFound)
            {
                LocalReferenceFrameList[i].EntryIndicies[LocalReferenceFrameList[i].EntryCount++] = ReferenceFrameList[i].EntryIndicies[j];
            }
        }
    }

    return Codec_MmeVideo_c::CheckReferenceFrameList(NumberOfReferenceFrameLists, LocalReferenceFrameList);
}


