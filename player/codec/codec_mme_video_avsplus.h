/************************************************************************
Copyright (C) 2003-2014 STMicroelectronics. All Rights Reserved.

This file is part of the Streaming Engine.

Streaming Engine is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License version 2 as
published by the Free Software Foundation.

Streaming Engine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with Streaming Engine; see the file COPYING.  If not, write to the Free
Software Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307,
USA.

The Streaming Engine Library may alternatively be licensed under a
proprietary license from ST.
************************************************************************/

#ifndef H_CODEC_MME_VIDEO_AVSPLUS
#define H_CODEC_MME_VIDEO_AVSPLUS

/*#define AVSP*/
#include "codec_mme_video.h"
#include "avs.h"
#include "hadesppinline.h"
#include "AVSP_HD_VideoTransformerTypes.h"

#include "codec_mme_video_utils.h"

#undef TRACE_TAG
#define TRACE_TAG "Codec_MmeVideoAvsPlus_c"

extern "C" {
    void *Codec_MmeVideoAvsPlus_IntermediateProcess(void *Parameter);
}

/////////////////////////////////////////////////////////////////////////

typedef struct AvspPpFrame_s
{
    AvspPpFrame_s()
        : CodedBuffer()
        , PreProcessorBuffer()
        , ParsedFrameParameters(NULL)
        , param() {}
    AvspPpFrame_s(const AvspPpFrame_s &f)
        : CodedBuffer(f.CodedBuffer)
        , ParsedFrameParameters(f.ParsedFrameParameters)
        , param(f.param)
    {
        memcpy(PreProcessorBuffer, f.PreProcessorBuffer,
               AVSP_NUM_INTERMEDIATE_POOLS * sizeof(Buffer_t));

    }
    ~AvspPpFrame_s() {}
    AvspPpFrame_s &operator=(const AvspPpFrame_s &f)
    {
        if (this != &f)
        {
            CodedBuffer = f.CodedBuffer;
            ParsedFrameParameters = f.ParsedFrameParameters;
            memcpy(PreProcessorBuffer, f.PreProcessorBuffer,
                   AVSP_NUM_INTERMEDIATE_POOLS * sizeof(Buffer_t));
            param = f.param;
        }
        return *this;
    }

    Buffer_t                 CodedBuffer;
    Buffer_t                 PreProcessorBuffer[AVSP_NUM_INTERMEDIATE_POOLS];
    ParsedFrameParameters_t *ParsedFrameParameters;
    hadespp_ioctl_synchronous_call_t param;
} AvspPpFrame_t;

/////////////////////////////////////////////////////////////////////////

class Codec_MmeVideoAvsPlus_c : public Codec_MmeVideo_c
{
public:
    Codec_MmeVideoAvsPlus_c();
    ~Codec_MmeVideoAvsPlus_c();
    CodecStatus_t   Halt();
    CodecStatus_t   FinalizeInit();

    CodecStatus_t   Connect(Port_c *Port);
    CodecStatus_t   Input(Buffer_t CodedBuffer);

    CodecStatus_t   OutputPartialDecodeBuffers();
    CodecStatus_t   ReleaseReferenceFrame(unsigned int ReferenceFrameDecodeIndex);
    CodecStatus_t   DiscardQueuedDecodes();
    CodecStatus_t   CheckReferenceFrameList(unsigned int NumberOfReferenceFrameLists,
                                            ReferenceFrameList_t ReferenceFrameList[]);

    void            IntermediateProcess();

protected:
    MME_AVSPVideoDecodeCapabilityParams_t AvsPlusTransformCapability;
    MME_AVSPVideoDecodeInitParams_t       AvsPlusInitializationParameters;

    BufferType_t                          mPreProcessorBufferMaxSize;
    BufferDataDescriptor_t                mAvsPlusPreProcessorBufferDescriptor;
    BufferPool_t                          PreProcessorBufferPool;

    ReferenceFrameList_t                  LocalReferenceFrameList[AVSP_NUM_REF_FRAME_LISTS];

    bool                                  RasterOutput;
    DecodeBufferManager_t                 mDecodeBufferManager;

    CodecStatus_t   CreatePreProcBufferPool();

    CodecStatus_t   DumpSetStreamParameters(void    *Parameters);
    CodecStatus_t   DumpDecodeParameters(void    *Parameters);

    CodecStatus_t   CheckCodecReturnParameters(CodecBaseDecodeContext_t *Context);

private:
    ppFramesRing_c<AvspPpFrame_t> *mPpFramesRing;
    bool                           mDiscardMode;

    allocator_device_t       mPreProcessorBufferAllocator;
    BufferType_t             mPreProcessorBufferType[AVSP_NUM_INTERMEDIATE_POOLS];
    unsigned int             mPreprocessorBufferSize[AVSP_NUM_INTERMEDIATE_POOLS];
    BufferPool_t             mPreProcessorBufferPool[AVSP_NUM_INTERMEDIATE_POOLS];
    struct HadesppDevice_t  *mPreProcessorDevice;
    OS_Semaphore_t           mIntThreadStopped;

    int             UpdatePreprocessorBufferSize(uint32_t w, uint32_t h);
    CodecStatus_t   AllocatePreProcBufferPool();
    void            DeallocatePreProcBufferPool();
    CodecStatus_t   FillOutPreprocessorCommand(avspluspreproc_transform_param_t *ppCmd,
                                               AvsStreamParameters_t    *ParsedStreamParameters,
                                               AvsVideoPictureHeader_t  *PictureHeader);
    CodecStatus_t   CheckPPStatus(ppFrame_t<AvspPpFrame_t> *ppFrame);

    CodecStatus_t   HandleCapabilities();
    CodecStatus_t   FillOutTransformerInitializationParameters();
    CodecStatus_t   FillOutSetStreamParametersCommand();
    CodecStatus_t   FillOutDecodeCommand();

    DISALLOW_COPY_AND_ASSIGN(Codec_MmeVideoAvsPlus_c);
};

#endif /* H_CODEC_MME_VIDEO_AVSPLUS */
