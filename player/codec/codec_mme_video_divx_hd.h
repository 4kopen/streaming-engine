/************************************************************************
Copyright (C) 2003-2014 STMicroelectronics. All Rights Reserved.

This file is part of the Streaming Engine.

Streaming Engine is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License version 2 as
published by the Free Software Foundation.

Streaming Engine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with Streaming Engine; see the file COPYING.  If not, write to the Free
Software Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307,
USA.

The Streaming Engine Library may alternatively be licensed under a
proprietary license from ST.
************************************************************************/

#ifndef H_CODEC_MME_VIDEO_DIVX_HD
#define H_CODEC_MME_VIDEO_DIVX_HD

#undef TRACE_TAG
#define TRACE_TAG "Codec_MmeVideoDivxHd_c"

#include <mme.h>

#include "codec_mme_video.h"
#include "mpeg4.h"

// /////////////////////////////////////////////////////////////////////////
// MPEG4P2/DIVX codec was changed from DivXHD to DivX at Version 18

class Codec_MmeVideoDivxHd_c : public Codec_MmeVideo_c
{
public:
    Codec_MmeVideoDivxHd_c();
    ~Codec_MmeVideoDivxHd_c();

protected:
    Divx_TransformerCapability_t      DivxTransformCapability;
    MME_DivXVideoDecodeInitParams_t   DivxInitializationParameters;

    MME_DivXSetGlobalParamSequence_t  DivXGlobalParamSequence;

    MME_DivXVideoDecodeReturnParams_t ReturnParams;
    MME_DivXVideoDecodeParams_t       DecodeParams;

    unsigned int MaxWidth;
    unsigned int MaxHeight;

    bool IsDivX;
    bool RasterOutput;

    CodecStatus_t   HandleCapabilities();

    CodecStatus_t   Connect(Port_c *Port);
    CodecStatus_t   FillOutTransformerInitializationParameters();
    CodecStatus_t   FillOutSetStreamParametersCommand();
    CodecStatus_t   FillOutDecodeCommand();

    CodecStatus_t   CheckCodecReturnParameters(CodecBaseDecodeContext_t *Context);

    CodecStatus_t   DumpDecodeParameters(void *Parameters);

#if (MPEG4P2_MME_VERSION < 22)
    CodecStatus_t   InitializeMMETransformer();
#endif

    CodecStatus_t   FillOutDecodeBufferRequest(DecodeBufferRequest_t    *Request);
    bool            IsDecimationValueSupported(int DecimationPolicy);
};

#endif
