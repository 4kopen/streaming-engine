/************************************************************************
Copyright (C) 2003-2014 STMicroelectronics. All Rights Reserved.

This file is part of the Streaming Engine.

Streaming Engine is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License version 2 as
published by the Free Software Foundation.

Streaming Engine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with Streaming Engine; see the file COPYING.  If not, write to the Free
Software Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307,
USA.

The Streaming Engine Library may alternatively be licensed under a
proprietary license from ST.
************************************************************************/

#include "player_threads.h"

#include "player_generic.h"
#include "player_playback.h"
#include "player_stream.h"
#include "parse_to_decode_edge.h"

#include "codec_mme_video_h264.h"
#include "h264.h"
#include "new.h"

#undef TRACE_TAG
#define TRACE_TAG "Codec_MmeVideoH264_c"

#define H264_MIN_RES_FOR_PP_BUFFERS_DIMENSIONNING 480

typedef struct H264CodecStreamParameterContext_s
{
    CodecBaseStreamParameterContext_t   BaseContext;

    H264_SetGlobalParam_t               StreamParameters;
} H264CodecStreamParameterContext_t;

#define BUFFER_H264_CODEC_STREAM_PARAMETER_CONTEXT             "H264CodecStreamParameterContext"
#define BUFFER_H264_CODEC_STREAM_PARAMETER_CONTEXT_TYPE        {BUFFER_H264_CODEC_STREAM_PARAMETER_CONTEXT, BufferDataTypeBase, AllocateFromOSMemory, 32, 0, true, true, sizeof(H264CodecStreamParameterContext_t)}

static BufferDataDescriptor_t H264CodecStreamParameterContextDescriptor = BUFFER_H264_CODEC_STREAM_PARAMETER_CONTEXT_TYPE;

// --------

typedef struct H264CodecDecodeContext_s
{
    CodecBaseDecodeContext_t            BaseContext;

    H264_TransformParam_fmw_t           DecodeParameters;
    H264_CommandStatus_fmw_t            DecodeStatus;
} H264CodecDecodeContext_t;

#define BUFFER_H264_CODEC_DECODE_CONTEXT       "H264CodecDecodeContext"
#define BUFFER_H264_CODEC_DECODE_CONTEXT_TYPE  {BUFFER_H264_CODEC_DECODE_CONTEXT, BufferDataTypeBase, AllocateFromOSMemory, 32, 0, true, true, sizeof(H264CodecDecodeContext_t)}

static BufferDataDescriptor_t H264CodecDecodeContextDescriptor = BUFFER_H264_CODEC_DECODE_CONTEXT_TYPE;

// --------

#define BUFFER_H264_PRE_PROCESSOR_BUFFER        "H264PreProcessorBuffer"

// /////////////////////////////////////////////////////////////////////////
//
//      Constructor function, fills in the codec specific parameter values
//

Codec_MmeVideoH264_c::Codec_MmeVideoH264_c()
    : H264TransformCapability()
    , H264InitializationParameters()
    , SD_MaxMBStructureSize(0)
    , HD_MaxMBStructureSize(0)
    , mPpFramesRing()
    , mDiscardMode(false)
    , mPreProcessorBufferType()
    , mPreProcessorBufferMaxSize()
    , mH264PreProcessorBufferDescriptor()
    , mPreProcessorBufferPool(NULL)
    , mPreProcessorBufferAllocator()
    , mPreProcessorDevice(NULL)
    , ReferenceFrameSlotUsed()
    , RecordedHostData()
    , OutstandingSlotAllocationRequest(INVALID_INDEX)
    , NumberOfUsedDescriptors(0)
    , DescriptorIndices()
    , LocalReferenceFrameList()
    , RasterOutput(false)
    , mDecodeBufferManager(NULL)
    , mIsFirstIdrOrIofStream(true)
    , mPreviousWidthInMBsMinus1(0)
    , mPreviousHeightInMapUnitsMinus1(0)
    , mLastLevelIdc(0xdeadbeef)
    , mIntThreadStopped()
{
    OS_SemaphoreInitialize(&mIntThreadStopped, 0);

    Configuration.CodecName                             = "H264 video";
    Configuration.StreamParameterContextCount           = 4;                    // H264 tests use stream param change per frame
    Configuration.StreamParameterContextDescriptor      = &H264CodecStreamParameterContextDescriptor;
    Configuration.DecodeContextCount                    = 8;
    Configuration.DecodeContextDescriptor               = &H264CodecDecodeContextDescriptor;
    Configuration.MaxDecodeIndicesPerBuffer             = 2;
    Configuration.SliceDecodePermitted                  = false;
    Configuration.DecimatedDecodePermitted              = true;
    Configuration.TransformName[0]                      = H264_MME_TRANSFORMER_NAME "0";
    Configuration.TransformName[1]                      = H264_MME_TRANSFORMER_NAME "1";
    Configuration.AvailableTransformers                 = 2;
    Configuration.SizeOfTransformCapabilityStructure    = sizeof(H264_TransformerCapability_fmw_t);
    Configuration.TransformCapabilityStructurePointer   = (void *)(&H264TransformCapability);

    //
    // The video firmware violates the MME spec. and passes data buffer addresses
    // as parametric information. For this reason it requires physical addresses
    // to be used.
    //
    Configuration.AddressingMode                        = PhysicalAddress;
    //
    // Since we pre-process the data, we shrink the coded data buffers
    // before entering the generic code. as a consequence we do
    // not require the generic code to shrink buffers on our behalf,
    // and we do not want the generic code to find the coded data buffer
    // for us.
    //
    Configuration.ShrinkCodedDataBuffersAfterDecode = false;
    Configuration.IgnoreFindCodedDataBuffer         = true;

    // TODO(pht) move FinalizeInit to a factory method
    InitializationStatus = FinalizeInit();
}

// /////////////////////////////////////////////////////////////////////////
//
//      FinalizeInit finalizes init work by doing operations that may fail
//      (and such not doable in ctor)
//
CodecStatus_t Codec_MmeVideoH264_c::FinalizeInit()
{
    // Create the ring used to communicate with intermediate process (frame or synchro data)
    mPpFramesRing = new ppFramesRing_c<H264PpFrame_t>(MAX_FRAME_NB_IN_PP_RING, 7);
    if (mPpFramesRing == NULL)
    {
        SE_ERROR("Failed to create ring to hold frames in pre-processor chain\n");
        return CodecError;
    }
    if (mPpFramesRing->FinalizeInit() != 0)
    {
        SE_ERROR("Failed to init PpFramesRing\n");
        return CodecError;
    }

    // Create the intermediate process
    OS_Thread_t Thread;
    if (OS_CreateThread(&Thread, Codec_MmeVideoH264_IntermediateProcess, this, &player_tasks_desc[SE_TASK_VIDEO_H264INT]) != OS_NO_ERROR)
    {
        SE_ERROR("Failed to create intermediate process\n");
        return CodecError;
    }

    return CodecNoError;
}

// /////////////////////////////////////////////////////////////////////////
//
//      Destructor function, ensures a full halt and reset
//      are executed for all levels of the class.
//

Codec_MmeVideoH264_c::~Codec_MmeVideoH264_c()
{
    int ret = mPpFramesRing->InsertFakeEntry(0, ppActionTermination);
    if (ret < 0)
    {
        SE_ERROR("Failed to send ppActionTermination\n");
    }

    SE_DEBUG(group_player, "Waiting for H264 intermediate process to stop\n");
    OS_SemaphoreWaitAuto(&mIntThreadStopped);
    SE_DEBUG(group_player, "H264 intermediate process stopped\n");


    //
    // Make sure the pre-processor device is closed (note this may be done in halt,
    // but we check again, since you can enter reset without halt during a failed
    // start up).
    //

    if (mPreProcessorDevice != NULL)
    {
#ifdef HEVC_HADES_CANNESWIFI
        HadesppClose(mPreProcessorDevice);
#else
        H264ppClose(mPreProcessorDevice);
#endif
        mPreProcessorDevice = NULL;  // reset for intermediate process
    }

    Halt();

    delete mPpFramesRing;
    OS_SemaphoreTerminate(&mIntThreadStopped);
}

// /////////////////////////////////////////////////////////////////////////
//
//      Halt function
//

CodecStatus_t Codec_MmeVideoH264_c::Halt()
{
    DeallocatePreProcBufferPool();
    return Codec_MmeVideo_c::Halt();
}

// /////////////////////////////////////////////////////////////////////////
//
//      Connect output port, we take this opportunity to
//      create the buffer pools for use in the pre-processor, and the
//      macroblock structure buffers
//
CodecStatus_t Codec_MmeVideoH264_c::Connect(Port_c *Port)
{
    // Let the ancestor classes setup the framework
    CodecStatus_t Status = Codec_MmeVideo_c::Connect(Port);
    if (Status != CodecNoError)
    {
        SetComponentState(ComponentInError);
        return Status;
    }

    // Create pre-processor intermediate buffer types if they don't already exist
    mH264PreProcessorBufferDescriptor.TypeName = BUFFER_H264_PRE_PROCESSOR_BUFFER;
    mH264PreProcessorBufferDescriptor.Type = BufferDataTypeBase;
    mH264PreProcessorBufferDescriptor.AllocationSource = AllocateFromSuppliedBlock;
    mH264PreProcessorBufferDescriptor.RequiredAlignment = (HADESPP_BUFFER_ALIGNMENT + 1);
    mH264PreProcessorBufferDescriptor.AllocationUnitSize = 1024;
    mH264PreProcessorBufferDescriptor.HasFixedSize = false;
    mH264PreProcessorBufferDescriptor.AllocateOnPoolCreation = false;
    mH264PreProcessorBufferDescriptor.FixedSize = 0;

    BufferStatus_t BufferStatus = BufferManager->FindBufferDataType(mH264PreProcessorBufferDescriptor.TypeName, &mPreProcessorBufferType);
    if (BufferStatus != BufferNoError)
    {
        BufferStatus = BufferManager->CreateBufferDataType(&mH264PreProcessorBufferDescriptor, &mPreProcessorBufferType);
        if (BufferStatus != BufferNoError)
        {
            SE_ERROR("Failed to create the %s buffer type (%08x)\n",
                     BUFFER_H264_PRE_PROCESSOR_BUFFER, BufferStatus);
            SetComponentState(ComponentInError);
            return CodecError;
        }
    }

#ifdef HEVC_HADES_CANNESWIFI
    if (hadespp_ok != HadesppOpen(&mPreProcessorDevice))
#else
    if (h264pp_ok != H264ppOpen(&mPreProcessorDevice))
#endif
    {
        SE_ERROR("Failed to open the pre-processor device\n");
        SetComponentState(ComponentInError);
        return CodecError;
    }
    return CodecNoError;
}


// /////////////////////////////////////////////////////////////////////////
//
//      Output partial decode buffers (discard preprocessed buffers)
//
CodecStatus_t Codec_MmeVideoH264_c::OutputPartialDecodeBuffers()
{
    AssertComponentState(ComponentRunning);

    int ret = mPpFramesRing->InsertFakeEntry(0, ppActionCallOutputPartialDecodeBuffers);
    if (ret < 0)
    {
        SE_ERROR("Failed to insert OutputPartialDecodeBuffers cmd\n");
        return CodecError;
    }
    return CodecNoError;
}

// /////////////////////////////////////////////////////////////////////////
//
//      Discard queued decodes, this will include any between here
//      and the preprocessor.
//

CodecStatus_t   Codec_MmeVideoH264_c::DiscardQueuedDecodes()
{
    int ret = mPpFramesRing->InsertFakeEntryHead(0, ppActionEnterDiscardMode);
    if (ret < 0)
    {
        SE_ERROR("Failed to insert OutputPartialDecodeBuffers cmd\n");
        return CodecError;
    }

    ret = mPpFramesRing->InsertFakeEntry(0, ppActionCallDiscardQueuedDecodes);
    if (ret < 0)
    {
        SE_ERROR("Failed to insert DiscardQueuedDecodes cmd\n");
        return CodecError;
    }
    return CodecNoError;
}


// /////////////////////////////////////////////////////////////////////////
//
//      Release reference frame, this must be capable of releasing a
//      reference frame that has not yet exited the pre-processor.
//

CodecStatus_t Codec_MmeVideoH264_c::ReleaseReferenceFrame(unsigned int ReferenceFrameDecodeIndex)
{
    int ret = mPpFramesRing->InsertFakeEntry(ReferenceFrameDecodeIndex,
                                             ppActionCallReleaseReferenceFrame);
    if (ret < 0)
    {
        SE_ERROR("Failed to insert ReleaseReferenceFrame cmd\n");
        return CodecError;
    }
    return CodecNoError;
}


// /////////////////////////////////////////////////////////////////////////
//
//      Check reference frame list needs to account for those currently
//      in the pre-processor chain.
//

CodecStatus_t   Codec_MmeVideoH264_c::CheckReferenceFrameList(
    unsigned int              NumberOfReferenceFrameLists,
    ReferenceFrameList_t      ReferenceFrameList[])
{
    // Check we can cope
    if (NumberOfReferenceFrameLists > H264_NUM_REF_FRAME_LISTS)
    {
        SE_ERROR("H264 has only %d reference frame lists, requested to check %d\n",
                 H264_NUM_REF_FRAME_LISTS, NumberOfReferenceFrameLists);
        return CodecUnknownFrame;
    }

    bool refFound;
    // Construct local lists consisting of those elements we do not know about
    for (int i = 0; i < NumberOfReferenceFrameLists; i++)
    {
        LocalReferenceFrameList[i].EntryCount   = 0;

        for (int j = 0; j < ReferenceFrameList[i].EntryCount; j++)
        {
            refFound = mPpFramesRing->CheckRefListDecodeIndex(ReferenceFrameList[i].EntryIndicies[j]);
            if (!refFound)
            {
                LocalReferenceFrameList[i].EntryIndicies[LocalReferenceFrameList[i].EntryCount++] = ReferenceFrameList[i].EntryIndicies[j];
            }
        }
    }

    return Codec_MmeVideo_c::CheckReferenceFrameList(NumberOfReferenceFrameLists, LocalReferenceFrameList);
}

// /////////////////////////////////////////////////////////////////////////

void Codec_MmeVideoH264_c::DeallocatePreProcBufferPool()
{
    if (mPreProcessorBufferPool != NULL)
    {
        BufferManager->DestroyPool(mPreProcessorBufferPool);
        mPreProcessorBufferPool = NULL;
        mDecodeBufferManager->RegisterPreprocBufferPool(NULL, 0);
    }
    AllocatorClose(&mPreProcessorBufferAllocator);
}

// /////////////////////////////////////////////////////////////////////////
//
// Create the Preproc buffer pool according to current frame size
//

CodecStatus_t Codec_MmeVideoH264_c::AllocatePreProcBufferPool(unsigned int pic_width_in_mbs,
                                                              unsigned int pic_height_in_mbs)
{
    allocator_status_t AllocatorStatus;
    CodecStatus_t Status;
    unsigned int PPPoolSize = 0;
    void *PPBufferMemoryPool[3];

    // free pool/allocator if memory is already allocated
    DeallocatePreProcBufferPool();

    // H264_MIN_NB_OF_PP_BUFFERS_PER_POOL represents the minimal theoritical number of buffers in the pool
    // Poolsize = (1 + K x (MIN_NB_OF_PP_BUFFERS_PER_POOL-1)) x computed worst-case buffer size
    // K is empirically set
#define H264_MIN_NB_OF_PP_BUFFERS_PER_POOL 6
#define H264_PP_BUFFER_RESIZING_FACTOR     20
    PPPoolSize = (mPreProcessorBufferMaxSize +
                  (H264_PP_BUFFER_RESIZING_FACTOR * mPreProcessorBufferMaxSize *
                   (H264_MIN_NB_OF_PP_BUFFERS_PER_POOL - 1)) / 100);
    // For small resolution streams, pool must be oversized to avoid GetBuffer failure
    if (((pic_width_in_mbs * 16) < H264_MIN_RES_FOR_PP_BUFFERS_DIMENSIONNING) ||
        ((pic_height_in_mbs * 16) < H264_MIN_RES_FOR_PP_BUFFERS_DIMENSIONNING))
    {
        PPPoolSize *= 2;
    }
    PPPoolSize = BUF_ALIGN_UP(PPPoolSize, HADESPP_BUFFER_ALIGNMENT);

    // mDecodeBufferManager MUST be set before calling DeallocatePreProcBufferPool (in alloc_failed)
    mDecodeBufferManager = Stream->GetDecodeBufferManager();

    AllocatorStatus = PartitionAllocatorOpen(&mPreProcessorBufferAllocator,
                                             Configuration.AncillaryMemoryPartitionName,
                                             PPPoolSize, MEMORY_VIDEO_HWONLY_ACCESS);
    if (AllocatorStatus != allocator_ok)
    {
        SE_ERROR("Failed to allocate pre-processor partition (status: %08x)\n", AllocatorStatus);
        goto alloc_failed;
    }

    SE_INFO(group_decoder_video, "[ALLOC] H264 Preproc pool size is %d bytes mPreProcessorBufferMaxSize: %d bytes\n",
            PPPoolSize, mPreProcessorBufferMaxSize);

    PPBufferMemoryPool[PhysicalAddress] = AllocatorPhysicalAddress(mPreProcessorBufferAllocator);

    Status = BufferManager->CreatePool(&mPreProcessorBufferPool, mPreProcessorBufferType,
                                       NOT_SPECIFIED, PPPoolSize,
                                       PPBufferMemoryPool, NULL, NULL,
                                       true, MEMORY_HWONLY_ACCESS);
    if (Status != BufferNoError)
    {
        SE_ERROR("Failed to create pre-processor buffer pool (%08x)\n", Status);
        goto alloc_failed;
    }

    mDecodeBufferManager->RegisterPreprocBufferPool(mPreProcessorBufferPool);

    SE_INFO(group_decoder_video, "[ALLOC] Allocated PP pool of  %d bytes\n", PPPoolSize);

    return CodecNoError;

alloc_failed:
    DeallocatePreProcBufferPool();
    SetComponentState(ComponentInError);
    return CodecError;
}

// /////////////////////////////////////////////////////////////////////////
//
//      UpdatePreprocessorBufferSize
//      computes the maximum size of the intermediate buffer
//      depends on the level of the stream (found in SPS)
//      Returns < 0 if error, > 0 if PreprocessorBufferSize changed, 0 otherwise
//
int Codec_MmeVideoH264_c::UpdatePreprocessorBufferSize(H264FrameParameters_t *FrameParameters,
                                                       unsigned int pic_width_in_mbs, unsigned int pic_height_in_mbs)
{
    uint32_t w = max(pic_width_in_mbs * 16, H264_MIN_RES_FOR_PP_BUFFERS_DIMENSIONNING);
    uint32_t h = max(pic_height_in_mbs * 16, H264_MIN_RES_FOR_PP_BUFFERS_DIMENSIONNING);
    uint32_t max_compressed_pic_size;
    uint32_t raw_mb_bits;
    uint32_t max_compressed_pic_bin_size;
    int max_h264_level = ARRAY_SIZE(h264_levels);
    int ret = 1;

    H264SequenceParameterSetHeader_t *sps = FrameParameters->SliceHeader.SequenceParameterSet;
    uint32_t level = sps->level_idc;
    uint32_t bit_per_sample = max(sps->bit_depth_luma_minus8 + 8, sps->bit_depth_chroma_minus8 + 8);

    if (bit_per_sample > 10)
    {
        SE_WARNING("Bit_depth forced to 10 (SPS.bit_depth_luma_minus8:%d bit_depth_chroma_minus8:%d) \n",
                   sps->bit_depth_luma_minus8, sps->bit_depth_chroma_minus8);
        bit_per_sample = 10;
    }

    if (level == mLastLevelIdc)
    {
        SE_DEBUG(group_decoder_video, "Level (%d) unchanged\n", level);
        ret = 0;
        goto bail;
    }

    // Find level in array of levels
    int level_idx;
    for (level_idx = 0; level_idx < max_h264_level; level_idx ++)
    {
        if (h264_levels[level_idx].level_idc == level)
        {
            break;
        }
    }

    if (level_idx  == max_h264_level)
    {
        SE_WARNING("Unable to get stream level\n");
        ret = -1;
        goto bail;
    }
    mLastLevelIdc = level;

    // Oversizing frame resolution for PP buffer size computation (avoid CTB_RES buffer overflow on some streams)
#define RESOLUTION_EXTENSION_RESIDUAL_SIZE 64
#define H264_PP_SPECIFIC_STREAM_OFFSET   256000
    max_compressed_pic_size = ((w + RESOLUTION_EXTENSION_RESIDUAL_SIZE) *
                               (h + RESOLUTION_EXTENSION_RESIDUAL_SIZE) * 3 * bit_per_sample) /
                              (2 * h264_levels[level_idx].MinCR);
    // RawMbBits = 256 x BitDepthY + 2 x MbWidthC x MbHeightC x BitDepthC
    // (assuming bitdepth chroma == bitdepth luma)
    raw_mb_bits = 256 * bit_per_sample + 2 * 8 * 8 * bit_per_sample;
    // max_compressed_pic_bin_size = YUV420 ratio x max_compressed_pic_size + (raw_mb_bits x pic_size_in_mb) / 32
    max_compressed_pic_bin_size = (4 * max_compressed_pic_size) / 3 + ((raw_mb_bits * w * h) / 256) / 32;

    // Intermediate Buffer is split in 2 parts:   | SESB | slice_data |
    // mPreProcessorBufferMaxSize = (H264_PP_SESB_SIZE + slice_data_size)
    // PP_SESB = Slice Status Error Buffer (stored at the beginning of IB)
    mPreProcessorBufferMaxSize = H264_PP_SESB_SIZE;
    mPreProcessorBufferMaxSize = BUF_ALIGN_UP(mPreProcessorBufferMaxSize, HADESPP_BUFFER_ALIGNMENT);
    mPreProcessorBufferMaxSize += ((5 * 8 * h264_levels[level_idx].MaxFrameSizeInMbs + max_compressed_pic_bin_size) / 8);
    // PreProc buffer size formula is not accurate, some margin is added for specific stream,See bz96097
    mPreProcessorBufferMaxSize += H264_PP_SPECIFIC_STREAM_OFFSET;
    mPreProcessorBufferMaxSize = BUF_ALIGN_UP(mPreProcessorBufferMaxSize, HADESPP_BUFFER_ALIGNMENT);

bail:
    SE_DEBUG(group_decoder_video, "Size of IB (level: %d): %d\n", level, mPreProcessorBufferMaxSize);
    return ret;
}


// Check output of the PP and shrink PP buffers consequently (mandatory before
// launching a new preprocessing, in order to optimize PP pool usage)
// In case of PP error (CodecError is returned), PP buffers must be detached by caller
CodecStatus_t Codec_MmeVideoH264_c::CheckPPStatus(ppFrame_t<H264PpFrame_t> *ppFrame,
                                                  ParsedFrameParameters_t  *parsedFrameParameters,
                                                  unsigned int PPSize, unsigned int PPStatusMask)
{
    if (PPStatusMask & H264PP_ERROR_TIMEOUT)
    {
        Stream->Statistics().H264PreprocReadErrors++;
    }
#ifdef HEVC_HADES_CANNESWIFI
    if (PPStatusMask & H264PP_ERROR_STATUS_UNEXPECTED_SC)
    {
        Stream->Statistics().H264PreprocErrorSCDetected++;
    }
    if ((PPStatusMask & H264PP_ERROR_STATUS_BUFFER1_OVERFLOW) ||
        (PPStatusMask & H264PP_ERROR_STATUS_BUFFER2_OVERFLOW))
    {
        Stream->Statistics().H264PreprocIntBufferOverflow++;
    }
    if (PPStatusMask & H264PP_ERROR_STATUS_END_OF_PIC_NOT_DETECTED)
    {
        Stream->Statistics().H264PreprocReadErrors++;
    }
#else
    if (PPStatusMask & PP_ITM__ERROR_SC_DETECTED)
    {
        Stream->Statistics().H264PreprocErrorSCDetected++;
    }

    if (PPStatusMask & PP_ITM__ERROR_BIT_INSERTED)
    {
        Stream->Statistics().H264PreprocErrorBitInserted++;
    }

    if (PPStatusMask & PP_ITM__INT_BUFFER_OVERFLOW)
    {
        Stream->Statistics().H264PreprocIntBufferOverflow++;
    }

    if (PPStatusMask & PP_ITM__BIT_BUFFER_UNDERFLOW)
    {
        Stream->Statistics().H264PreprocBitBufferUnderflow++;
    }

    if (PPStatusMask & PP_ITM__BIT_BUFFER_OVERFLOW)
    {
        Stream->Statistics().H264PreprocBitBufferOverflow++;
    }

    if (PPStatusMask & PP_ITM__READ_ERROR)
    {
        Stream->Statistics().H264PreprocReadErrors++;
    }

    if (PPStatusMask & PP_ITM__WRITE_ERROR)
    {
        Stream->Statistics().H264PreprocWriteErrors++;
    }
#endif

    // Coded buffer is not useful anymore, all the data is in the intermediate buffer
    UnuseCodedBuffer(ppFrame->data.CodedBuffer, &ppFrame->data);
    // Shrinking buffer with PPSize = sesb_stop_offset + slice_data size
    //  Avoid value 0
    if (PPSize == 0) { PPSize = 4; }
    ppFrame->data.PreProcessorBuffer->SetUsedDataSize(PPSize);
    ppFrame->data.PreProcessorBuffer->ShrinkBuffer(PPSize);
    if (PPStatusMask != 0)
    {
        if (PolicyValueApply != Player->PolicyValue(Playback, Stream, PolicyAllowBadPreProcessedFrames))
        {
            SE_WARNING("Due to policy PolicyAllowBadHevcPreProcessedFrames, picture is discarded\n");
            goto hw_failed;
        }
        SE_WARNING("Due to disabled policy PolicyAllowBadHevcPreProcessedFrames, picture is  kept\n");
    }

    return CodecNoError;

hw_failed:
    if (parsedFrameParameters->FirstParsedParametersForOutputFrame)
    {
        Stream->ParseToDecodeEdge->RecordNonDecodedFrame(ppFrame->data.CodedBuffer, parsedFrameParameters);
    }
    else
    {
        OutputPartialDecodeBuffers();
    }

    return CodecError;
}

// /////////////////////////////////////////////////////////////////////////
//
//      Input, must feed the data to the preprocessor chain.
//      This function needs to replicate a considerable amount of the
//      ancestor classes, because this function is operating
//      in a different process to the ancestors, and because the
//      ancestor data, and the data used here, will be in existence
//      at the same time.
//

CodecStatus_t   Codec_MmeVideoH264_c::Input(Buffer_t CodedBuffer)
{
    unsigned int              CodedDataLength;
    ParsedFrameParameters_t  *ParsedFrameParameters;
    ParsedVideoParameters_t  *ParsedVideoParameters;
    Buffer_t                  PreProcessorBuffer;
    H264FrameParameters_t    *FrameParameters;

    BufferStatus_t Status;

    // First extract the useful pointers from the buffer all held locally
    CodedBuffer->ObtainMetaDataReference(Player->MetaDataParsedFrameParametersType, (void **)(&ParsedFrameParameters));
    SE_ASSERT(ParsedFrameParameters != NULL);

    CodedBuffer->ObtainMetaDataReference(Player->MetaDataParsedVideoParametersType, (void **)(&ParsedVideoParameters));
    SE_ASSERT(ParsedVideoParameters != NULL);

    //
    // If this does not contain any frame data, then we simply wish
    // to slot it into place for the intermediate process.
    //
    int ret = 0;
    ppFrame_t<H264PpFrame_t> ppFrame;
    if (!ParsedFrameParameters->NewFrameParameters ||
        (ParsedFrameParameters->DecodeFrameIndex == INVALID_INDEX))
    {
        new(&ppFrame) ppFrame_t<H264PpFrame_t>(ppActionPassOnFrame, ParsedFrameParameters->DecodeFrameIndex);
        ppFrame.data.CodedBuffer = CodedBuffer;
        ppFrame.data.ParsedFrameParameters = ParsedFrameParameters;

        CodedBuffer->IncrementReferenceCount(IdentifierH264Intermediate);
        ret = mPpFramesRing->Insert(&ppFrame);
        if (ret < 0)
        {
            SE_ERROR("Failed to insert frame for PP\n");
            CodedBuffer->DecrementReferenceCount(IdentifierH264Intermediate);
            return CodecError;
        }
        return CodecNoError;
    }

    // We believe we have a frame - check that this is marked as a first slice
    FrameParameters = (H264FrameParameters_t *)ParsedFrameParameters->FrameParameterStructure;

    if (!ParsedVideoParameters->FirstSlice)
    {
        SE_ERROR("Non first slice, when one is expected\n");
        return CodecError;
    }

    // Allocate intermediate buffers (+ pool) and attach them to coded buffer
    H264SequenceParameterSetHeader_t *sps = FrameParameters->SliceHeader.SequenceParameterSet;
    uint32_t pic_width_in_mbs  = (sps->pic_width_in_mbs_minus1 + 1);
    uint32_t pic_height_in_mbs = ((2 - sps->frame_mbs_only_flag) * (sps->pic_height_in_map_units_minus1 + 1)) /
                                 (1 + FrameParameters->SliceHeader.field_pic_flag);
    ret = UpdatePreprocessorBufferSize(FrameParameters, pic_width_in_mbs, pic_height_in_mbs);
    if (ret < 0)
    {
        SE_ERROR("Unable to get PPBuffer size\n");
        return CodecError;
    }
    if ((ret > 0) || (mPreProcessorBufferPool == NULL))
    {
        if (mPreProcessorBufferPool != NULL)
        {
            SE_INFO(group_decoder_video, "PPBuffer size has changed -> updating the PP Pool size\n");

            //
            // Waiting for end of ALL PP pending tasks
            // Before destroying the pool ensure that no buffer in the pool is in use.
            // With a highly corrupted H264 stream it has been observed that the coded buffer
            // along with the attached PP buffer exits the PP ring but before it is sent
            // for decode, DeallocatePreProcBufferPool gets invoked due to level change
            // resulting in assertion SE_ASSERT(PreProcessorBuffer != NULL) in FillOutDecodeCommand
            //
            unsigned int BuffersWithNonZeroReferenceCount;
            unsigned long long wait_start = OS_GetTimeInMicroSeconds();
            unsigned long long waiting_time_in_us;
            do
            {
                OS_SleepMilliSeconds(2); // jiffies granularity is around 10ms anyway
                mPreProcessorBufferPool->GetPoolUsage(NULL, &BuffersWithNonZeroReferenceCount,
                                                      NULL, NULL, NULL, NULL);
                waiting_time_in_us = (OS_GetTimeInMicroSeconds() - wait_start) / 1000;
                if (waiting_time_in_us > 700)
                {
                    SE_WARNING("Before reallocation, waiting for PreProcBufferPool usage to go to zero during %llu us\n", waiting_time_in_us);
                }
            }
            while (BuffersWithNonZeroReferenceCount != 0);

            SE_VERBOSE(group_decoder_video, "Ready to re-allocate preproc buffer pool after %llu ms\n", waiting_time_in_us);
        }

        Status = AllocatePreProcBufferPool(pic_width_in_mbs, pic_height_in_mbs);
        if (Status != CodecNoError)
        {
            SE_ERROR("%p Failed to allocate PP buffer pool (%08x) MaxSize:%d\n",
                     this, Status, mPreProcessorBufferMaxSize);
            return Status;
        }
    }
    //
    // Get a pre-processor buffer and attach it to the coded frame.
    // Note since it is attached to the coded frame, we can release our
    // personal hold on it.
    //
    Status = mPreProcessorBufferPool->GetBuffer(&PreProcessorBuffer, UNSPECIFIED_OWNER, mPreProcessorBufferMaxSize);
    if (Status != BufferNoError)
    {
        SE_ERROR("Failed to get a pre-processor buffer (%08x)\n", Status);
        return CodecError;
    }

    CodedBuffer->AttachBuffer(PreProcessorBuffer);
    PreProcessorBuffer->DecrementReferenceCount();

    void *CodedDataPhysAddr;
    CodedBuffer->ObtainDataReference(NULL, &CodedDataLength, (void **)(&CodedDataPhysAddr), PhysicalAddress);
    SE_ASSERT(CodedDataPhysAddr != NULL);

    // It could happen in corrupted stream
    if (CodedDataLength < ParsedFrameParameters->DataOffset + FrameParameters->SlicesLength)
    {
        SE_ERROR("error DataOffset=%u + SlicesLength %u > CodedDataLength=%u\n",
                 ParsedFrameParameters->DataOffset, FrameParameters->SlicesLength, CodedDataLength);
        CodedBuffer->DetachBuffer(PreProcessorBuffer);
        return CodecError;
    }

    // Ready to launch a new PP task -> fill it in
    new(&ppFrame) ppFrame_t<H264PpFrame_t>(ppActionPassOnFrame, ParsedFrameParameters->DecodeFrameIndex);
    ppFrame.data.CodedBuffer = CodedBuffer;
    ppFrame.data.ParsedFrameParameters = ParsedFrameParameters;
    ppFrame.data.PreProcessorBuffer = PreProcessorBuffer;

    ppFrame.data.InputBufferPhysicalAddress = (void *)((unsigned int)CodedDataPhysAddr + ParsedFrameParameters->DataOffset);

    PreProcessorBuffer->ObtainDataReference(NULL, NULL, (void **)(&ppFrame.data.OutputBufferPhysicalAddress), PhysicalAddress);
    SE_ASSERT(ppFrame.data.OutputBufferPhysicalAddress != NULL);

    CodedBuffer->IncrementReferenceCount(IdentifierH264Intermediate);

    // Process the buffer
    SE_VERBOSE(group_decoder_video, ">PP frame %d\n", ppFrame.DecodeFrameIndex);
    unsigned int PPSize = 0;
    unsigned int PPStatusMask = 0;

#ifdef HEVC_HADES_CANNESWIFI
    //TODO use ppFrameRing
    struct hadespp_ioctl_queue_t queInfo;

    FillOutPreprocCmd(&queInfo, ParsedFrameParameters, (uint32_t)CodedDataPhysAddr,
                      (uint32_t)ppFrame.data.OutputBufferPhysicalAddress);

    hadespp_status_t PPStatus = HadesppQueueBuffer(mPreProcessorDevice, &queInfo);
    if (PPStatus != hadespp_ok)
    {
        SE_ERROR("Failed to queue a buffer, junking frame\n");
        goto pp_error;
    }

    struct hadespp_ioctl_dequeue_t dequeParams;
    h264preproc_command_status_t *sts;
    sts = &dequeParams.iStatus.h264_iStatus;
    if (hadespp_ok != HadesppGetPreProcessedBuffer(mPreProcessorDevice, &dequeParams))
    {
        SE_ERROR("Failed to retrieve a buffer from the pre-processor\n");
        // Fall through to CheckPPStatus to get more info and exit after
    }

    if ((dequeParams.hwStatus == hadespp_unrecoverable_error) || (dequeParams.hwStatus == hadespp_timeout))
    {
        SE_WARNING("Discarding picture with pre-processing timeout/unrecoverable error\n");
        goto pp_error;
    }
    else if (dequeParams.hwStatus == hadespp_recoverable_error)
    {
        // TODO: Rename policy
        if (PolicyValueApply != Player->PolicyValue(Playback, Stream, PolicyAllowBadHevcPreProcessedFrames))
        {
            SE_WARNING("Due to policy enforcement discarding picture with recoverable pre-processing error\n");
            goto pp_error;
        }
    }

    // To shrink intermediate buffer
    // Intermediate Buffer is split in 2 parts:   | SESB | slice_data |
    // PPsize = (H264_PP_SESB_SIZE + slice_data size)
    PPSize = H264_PP_SESB_SIZE + sts->slice_data_stop_offset;
    PPStatusMask = sts->error;
#else
    unsigned int    PPEntry = 0;
    h264pp_status_t PPStatus = H264ppProcessBuffer(mPreProcessorDevice,
                                                   &FrameParameters->SliceHeader,
                                                   ppFrame.DecodeFrameIndex,
                                                   mPreProcessorBufferMaxSize,
                                                   FrameParameters->SlicesLength,
                                                   ppFrame.data.InputBufferPhysicalAddress,
                                                   ppFrame.data.OutputBufferPhysicalAddress);
    if (PPStatus != h264pp_ok)
    {
        SE_ERROR("Failed to queue a buffer, junking frame\n");
        goto pp_error;
    }

    if (h264pp_ok != H264ppGetPreProcessedBuffer(mPreProcessorDevice, &PPEntry, &PPSize, &PPStatusMask))
    {
        SE_ERROR("Failed to retrieve a buffer from the pre-processor\n");
        // Fall through to CheckPPStatus to get more info and exit after
    }
#endif

    if (CodecNoError != CheckPPStatus(&ppFrame, ParsedFrameParameters, PPSize, PPStatusMask))
    {
        SE_ERROR("PP returned unrecoverable error\n");
        goto pp_error;
    }

    SE_VERBOSE(group_decoder_video, "<PP frame %d\n", ppFrame.DecodeFrameIndex);

    // Add buffer to ring so that Intermediate process can pick it up later
    ret = mPpFramesRing->Insert(&ppFrame);
    if (ret < 0)
    {
        SE_ERROR("Failed to insert PP frame[%d]\n", ppFrame.DecodeFrameIndex);
        goto pp_error;
    }

    return CodecNoError;

pp_error:
    SE_ERROR("Failed to process a buffer, Junking frame\n");
    CodedBuffer->DetachBuffer(PreProcessorBuffer);
    CodedBuffer->DecrementReferenceCount(IdentifierH264Intermediate);
    return CodecError;
}

// /////////////////////////////////////////////////////////////////////////

void Codec_MmeVideoH264_c::ClearInternalBufferMap()
{
    SE_DEBUG(group_decoder_video, "\n");
    if (mPreProcessorBufferAllocator != NULL)
    {
        AllocatorRemoveMapEx(mPreProcessorBufferAllocator->UnderlyingDevice);
    }
}

void Codec_MmeVideoH264_c::CreateInternalBufferMap()
{
    SE_DEBUG(group_decoder_video, "\n");
    if (mPreProcessorBufferAllocator != NULL)
    {
        AllocatorCreateMapEx(mPreProcessorBufferAllocator->UnderlyingDevice);
    }
}

// /////////////////////////////////////////////////////////////////////////
//
//      Function to deal with the returned capabilities
//      structure for an H264 mme transformer.
//

CodecStatus_t   Codec_MmeVideoH264_c::HandleCapabilities()
{
// Default to using Omega2 unless Capabilities tell us otherwise
    BufferFormat_t  DisplayFormat = FormatVideo420_MacroBlock;
// Default elements to produce in the buffers
    DecodeBufferComponentElementMask_t Elements = PrimaryManifestationElement |
                                                  DecimatedManifestationElement |
                                                  VideoMacroblockStructureElement;

    RasterOutput = false;
    SE_INFO(group_decoder_video, "MME Transformer '%s' capabilities are :-\n", H264_MME_TRANSFORMER_NAME);
    SE_DEBUG(group_decoder_video, "  SD_MaxMBStructureSize             = %d bytes\n", H264TransformCapability.SD_MaxMBStructureSize);
    SE_DEBUG(group_decoder_video, "  HD_MaxMBStructureSize             = %d bytes\n", H264TransformCapability.HD_MaxMBStructureSize);
    SE_DEBUG(group_decoder_video, "  MaximumFieldDecodingLatency90KHz  = %d\n", H264TransformCapability.MaximumFieldDecodingLatency90KHz);
    //
    // Unless things have changed, the firmware always
    // reported zero for the sizes, so we use our own
    // known values.
    //
    // Accordingly to max partition size (cf. havana_stream.cpp)
    SD_MaxMBStructureSize       = 110;
    HD_MaxMBStructureSize       = 48;
    DeltaTop_TransformerCapability_t *DeltaTopCapabilities = (DeltaTop_TransformerCapability_t *) Configuration.DeltaTopCapabilityStructurePointer;

    if ((DeltaTopCapabilities != NULL) && (DeltaTopCapabilities->DisplayBufferFormat == DELTA_OUTPUT_RASTER))
    {
        RasterOutput = true;
    }

    if (NoVideoCopyBuffer(DELTA_CODEC))
    {
        SE_WARNING("Overwrite: default DisplayBufferFormat received from firmware %s by %s\n", PLAYER_STRINGIFY(DELTA_OUTPUT_RASTER), PLAYER_STRINGIFY(DELTA_OUTPUT_OMEGA2));
        //Currently set simply to OMEGA2; Once SOC gets RASTER2B capability for decode buffer, it shall be configured accordingly.
        DeltaTopTransformCapability.DisplayBufferFormat     = DELTA_OUTPUT_OMEGA2;
        RefBufferType = PolicyValueMBBuffOnly;
        RasterOutput = false;
    }

    if (RasterOutput)
    {
        DisplayFormat = FormatVideo420_Raster2B;
        Elements |= VideoDecodeCopyElement;
        SD_MaxMBStructureSize   = H264_DUALHD_STORED_MBSTRUCT_SIZE_IN_PPB_BELOW_LEVEL31;
        HD_MaxMBStructureSize   = H264_DUALHD_STORED_MBSTRUCT_SIZE_IN_PPB_LEVEL31_OR_ABOVE;
    }

    SE_INFO(group_decoder_video, "  Using default MB structure sizes   = %d, %d bytes\n",
            SD_MaxMBStructureSize, HD_MaxMBStructureSize);
    Stream->GetDecodeBufferManager()->FillOutDefaultList(DisplayFormat,
                                                         Elements,
                                                         Configuration.ListOfDecodeBufferComponents);

    return CodecNoError;
}


// /////////////////////////////////////////////////////////////////////////
//
//      Function to extend the buffer request to incorporate the macroblock sizing
//

CodecStatus_t   Codec_MmeVideoH264_c::FillOutDecodeBufferRequest(DecodeBufferRequest_t    *Request)
{
    H264SequenceParameterSetHeader_t        *SPS;
    CodecStatus_t ret = CodecNoError;

    // Fill out the standard fields first
    ret = Codec_MmeVideo_c::FillOutDecodeBufferRequest(Request);

    // and now the extended field
    if (ParsedFrameParameters && ParsedFrameParameters->FrameParameterStructure)
    {
        SPS = ((H264FrameParameters_t *)ParsedFrameParameters->FrameParameterStructure)->SliceHeader.SequenceParameterSet;
        Request->PerMacroblockMacroblockStructureSize = (SPS->level_idc <= 30) ? SD_MaxMBStructureSize : HD_MaxMBStructureSize;
    }
    else
    {
        SE_DEBUG(group_decoder_video, "Unable to retrieve sps header\n");
        Request->PerMacroblockMacroblockStructureSize  = HD_MaxMBStructureSize;
        ret = CodecError;
    }

    Request->MacroblockSize                             = 256;
    Request->PerMacroblockMacroblockStructureFifoSize   = 512; // Fifo length is 256 bytes for 7108 but 512 are requested for MPE42c1, let's use widest value it's easier
    FillOutBufferCountRequest(Request);


    return ret;
}


// /////////////////////////////////////////////////////////////////////////
//
//      Function to deal with the returned capabilities
//      structure for an H264 mme transformer.
//

CodecStatus_t   Codec_MmeVideoH264_c::FillOutTransformerInitializationParameters()
{
    //
    // Fill out the command parameters
    //
    H264InitializationParameters.CircularBinBufferBeginAddr_p   = (U32 *)0x00000000;
    H264InitializationParameters.CircularBinBufferEndAddr_p     = (U32 *)0xffffffff;
    //
    // Fill out the actual command
    //
    MMEInitializationParameters.TransformerInitParamsSize       = sizeof(H264_InitTransformerParam_fmw_t);
    MMEInitializationParameters.TransformerInitParams_p         = (MME_GenericParams_t)(&H264InitializationParameters);

    return CodecNoError;
}


// /////////////////////////////////////////////////////////////////////////
//
//      Function to prepare the stream parameters
//      structure for an H264 mme transformer,
//      from the current PPS and SPS
//

CodecStatus_t Codec_MmeVideoH264_c::PrepareStreamParametersCommand(void                    *GenericContext,
                                                                   H264SequenceParameterSetHeader_t   *SPS,
                                                                   H264PictureParameterSetHeader_t    *PPS)
{
    H264CodecStreamParameterContext_t *Context = (H264CodecStreamParameterContext_t *)GenericContext;

    memset(&Context->StreamParameters, 0, sizeof(H264_SetGlobalParam_t));
    Context->StreamParameters.H264SetGlobalParamSPS.DecoderProfileConformance          = H264_HIGH_PROFILE;
    // Context->StreamParameters.H264SetGlobalParamSPS.DecoderProfileConformance          = (H264_DecoderProfileComformance_t)SPS->profile_idc;
    Context->StreamParameters.H264SetGlobalParamSPS.level_idc                          = SPS->level_idc;
    Context->StreamParameters.H264SetGlobalParamSPS.log2_max_frame_num_minus4          = SPS->log2_max_frame_num_minus4;
    Context->StreamParameters.H264SetGlobalParamSPS.pic_order_cnt_type                 = SPS->pic_order_cnt_type;
    Context->StreamParameters.H264SetGlobalParamSPS.log2_max_pic_order_cnt_lsb_minus4  = SPS->log2_max_pic_order_cnt_lsb_minus4;
    Context->StreamParameters.H264SetGlobalParamSPS.delta_pic_order_always_zero_flag   = SPS->delta_pic_order_always_zero_flag;
    Context->StreamParameters.H264SetGlobalParamSPS.pic_width_in_mbs_minus1            = SPS->pic_width_in_mbs_minus1;
    Context->StreamParameters.H264SetGlobalParamSPS.pic_height_in_map_units_minus1     = SPS->pic_height_in_map_units_minus1;

    // As new SPS has different width and/or height so firmware should not use previous reference for emulation.
    if (!((mPreviousWidthInMBsMinus1 == SPS->pic_width_in_mbs_minus1) && (mPreviousHeightInMapUnitsMinus1 == SPS->pic_height_in_map_units_minus1)))
    {
        mIsFirstIdrOrIofStream = true;
    }
    mPreviousWidthInMBsMinus1        = SPS->pic_width_in_mbs_minus1;
    mPreviousHeightInMapUnitsMinus1  = SPS->pic_height_in_map_units_minus1;

    Context->StreamParameters.H264SetGlobalParamSPS.frame_mbs_only_flag                = SPS->frame_mbs_only_flag;
    Context->StreamParameters.H264SetGlobalParamSPS.mb_adaptive_frame_field_flag       = SPS->mb_adaptive_frame_field_flag;
#if defined(CONFIG_STM_VIRTUAL_PLATFORM) // VSOC only trick - used by SystemC model of Delta Decoders for DPB management. Not relevant for SVC firmware
    Context->StreamParameters.H264SetGlobalParamSPS.max_num_ref_frames                 = SPS->num_ref_frames;
#endif
    // In case of LEVEL >= 3.0, we force direct_8x8_inference_flag to support streams with an invalid number of MV's : (Bug 12245)
    Context->StreamParameters.H264SetGlobalParamSPS.direct_8x8_inference_flag          = SPS->level_idc >= 30 ? true : SPS->direct_8x8_inference_flag;
    Context->StreamParameters.H264SetGlobalParamSPS.chroma_format_idc                  = SPS->chroma_format_idc;
    Context->StreamParameters.H264SetGlobalParamPPS.entropy_coding_mode_flag           = PPS->entropy_coding_mode_flag;
    Context->StreamParameters.H264SetGlobalParamPPS.pic_order_present_flag             = PPS->pic_order_present_flag;
    Context->StreamParameters.H264SetGlobalParamPPS.num_ref_idx_l0_active_minus1       = PPS->num_ref_idx_l0_active_minus1;
    Context->StreamParameters.H264SetGlobalParamPPS.num_ref_idx_l1_active_minus1       = PPS->num_ref_idx_l1_active_minus1;
    Context->StreamParameters.H264SetGlobalParamPPS.weighted_pred_flag                 = PPS->weighted_pred_flag;
    Context->StreamParameters.H264SetGlobalParamPPS.weighted_bipred_idc                = PPS->weighted_bipred_idc;
    Context->StreamParameters.H264SetGlobalParamPPS.pic_init_qp_minus26                = PPS->pic_init_qp_minus26;
    Context->StreamParameters.H264SetGlobalParamPPS.chroma_qp_index_offset             = PPS->chroma_qp_index_offset;
    Context->StreamParameters.H264SetGlobalParamPPS.deblocking_filter_control_present_flag = PPS->deblocking_filter_control_present_flag;
    Context->StreamParameters.H264SetGlobalParamPPS.constrained_intra_pred_flag        = PPS->constrained_intra_pred_flag;
    Context->StreamParameters.H264SetGlobalParamPPS.transform_8x8_mode_flag            = PPS->transform_8x8_mode_flag;
    Context->StreamParameters.H264SetGlobalParamPPS.second_chroma_qp_index_offset      = PPS->second_chroma_qp_index_offset;
    Context->StreamParameters.H264SetGlobalParamPPS.ScalingList.ScalingListUpdated     = 1;

    if (PPS->pic_scaling_matrix_present_flag)
    {
        memcpy(Context->StreamParameters.H264SetGlobalParamPPS.ScalingList.FirstSixScalingList, PPS->ScalingList4x4, sizeof(PPS->ScalingList4x4));
        memcpy(Context->StreamParameters.H264SetGlobalParamPPS.ScalingList.NextTwoScalingList, PPS->ScalingList8x8, sizeof(PPS->ScalingList8x8));
    }
    else
    {
        memcpy(Context->StreamParameters.H264SetGlobalParamPPS.ScalingList.FirstSixScalingList, SPS->ScalingList4x4, sizeof(PPS->ScalingList4x4));
        memcpy(Context->StreamParameters.H264SetGlobalParamPPS.ScalingList.NextTwoScalingList, SPS->ScalingList8x8, sizeof(PPS->ScalingList8x8));
    }

    // Needed for SVCDecoder firmware
    Context->StreamParameters.SvcSpecificSetGlobalParamSPS.slice_header_restriction_flag        = 1;
    Context->StreamParameters.SvcSpecificSetGlobalParamSPS.chroma_phase_x_plus1_flag            = 1;
    Context->StreamParameters.SvcSpecificSetGlobalParamSPS.chroma_phase_y_plus1             = 1;
    Context->StreamParameters.SvcSpecificSetGlobalParamSPS.seq_ref_layer_chroma_phase_x_plus1_flag  = 1;
    Context->StreamParameters.SvcSpecificSetGlobalParamSPS.seq_ref_layer_chroma_phase_y_plus1       = 1;
    //
    // Fill out the actual command
    //
    memset(&Context->BaseContext.MMECommand, 0, sizeof(MME_Command_t));
    Context->BaseContext.MMECommand.CmdStatus.AdditionalInfoSize        = 0;
    Context->BaseContext.MMECommand.CmdStatus.AdditionalInfo_p          = NULL;
    Context->BaseContext.MMECommand.ParamSize                           = sizeof(H264_SetGlobalParam_t);
    Context->BaseContext.MMECommand.Param_p                             = (MME_GenericParams_t)(&Context->StreamParameters);

    return CodecNoError;
}

// /////////////////////////////////////////////////////////////////////////
//
//      Function to fill out the stream parameters
//      structure for an H264 mme transformer.
//

CodecStatus_t   Codec_MmeVideoH264_c::FillOutSetStreamParametersCommand()
{
    H264StreamParameters_t *Parsed = (H264StreamParameters_t *)ParsedFrameParameters->StreamParameterStructure;
    H264CodecStreamParameterContext_t *Context = (H264CodecStreamParameterContext_t *)StreamParameterContext;
    H264PictureParameterSetHeader_t  *PPS = Parsed->PictureParameterSet;
    H264SequenceParameterSetHeader_t *SPS = Parsed->SequenceParameterSet;

    // Fill out the command parameters
    return PrepareStreamParametersCommand((void *)Context, SPS, PPS);
}


// /////////////////////////////////////////////////////////////////////////
//
//      Function to fill out the preproc parameters
// TODO review this function: align H264PpFrame_t struct + rework h264ppinline.h
//
void Codec_MmeVideoH264_c::FillOutPreprocCmd(struct hadespp_ioctl_queue_t *queInfo, ParsedFrameParameters_t *parsedFp,
                                             uint32_t CodedDataPhysAddr, uint32_t OutputBufferPhysicalAddr)
{
#ifdef HEVC_HADES_CANNESWIFI
    H264FrameParameters_t *fp = (H264FrameParameters_t *)parsedFp->FrameParameterStructure;
    h264preproc_transform_param_t *ppCmd = &queInfo->iCmd.h264_iCmd;
    H264SliceHeader_t *slice = &fp->SliceHeader;
    H264SequenceParameterSetHeader_t *sps = slice->SequenceParameterSet;
    H264PictureParameterSetHeader_t *pps = slice->PictureParameterSet;

    memset(ppCmd, 0, sizeof(h264preproc_transform_param_t));
    queInfo->codec = CODEC_H264;

    ppCmd->bit_buf.base_addr = ((uint32_t)CodedDataPhysAddr) & 0xFFFFFFF8;
    ppCmd->bit_buf.length = fp->SlicesLength + 1; // ?
    ppCmd->bit_buf.start_offset = parsedFp->DataOffset + ((uint32_t) CodedDataPhysAddr & 0x7U);
    ppCmd->bit_buf.end_offset = (uint32_t)ppCmd->bit_buf.start_offset + fp->SlicesLength + 1;

    //Intermediate Buffer is split in 2 parts:   | SESB | slice_data |
    ppCmd->sesb_start = OutputBufferPhysicalAddr;
    ppCmd->sesb_length = H264_PP_SESB_SIZE;

    ppCmd->slice_data_start = ppCmd->sesb_start + ppCmd->sesb_length;
    ppCmd->slice_data_length = mPreProcessorBufferMaxSize - ppCmd->sesb_length - 1; //-1?

    ppCmd->pic_width_in_mbs_minus1 = sps->pic_width_in_mbs_minus1;
    ppCmd->pic_height_in_map_units_minus1 = sps->pic_height_in_map_units_minus1;
    ppCmd->mb_adaptive_frame_field_flag = sps->mb_adaptive_frame_field_flag;
    ppCmd->entropy_coding_mode_flag = pps->entropy_coding_mode_flag;
    ppCmd->frame_mbs_only_flag = sps->frame_mbs_only_flag;
    ppCmd->pic_order_cnt_type = sps->pic_order_cnt_type;
    ppCmd->pic_order_present_flag = pps->pic_order_present_flag;
    ppCmd->delta_pic_order_always_zero_flag = sps->delta_pic_order_always_zero_flag;
    ppCmd->redundant_pic_cnt_present_flag = pps->redundant_pic_cnt_present_flag;
    ppCmd->weighted_pred_flag = pps->weighted_pred_flag;
    ppCmd->weighted_bipred_idc = pps->weighted_bipred_idc;
    ppCmd->deblocking_filter_control_present_flag = pps->deblocking_filter_control_present_flag;
    ppCmd->num_ref_idx_l0_active_minus1 = pps->num_ref_idx_l0_active_minus1;
    ppCmd->num_ref_idx_l1_active_minus1 = pps->num_ref_idx_l1_active_minus1;
    ppCmd->pic_init_qp_minus26 = pps->pic_init_qp_minus26;
    ppCmd->transform_8x8_mode_flag = pps->transform_8x8_mode_flag;
    ppCmd->direct_8x8_inference_flag = sps->direct_8x8_inference_flag;
    ppCmd->chroma_format_idc = sps->chroma_format_idc;
    ppCmd->field_pic_flag = slice->field_pic_flag;
    ppCmd->log2_max_frame_num_minus4 = sps->log2_max_frame_num_minus4;
    ppCmd->log2_max_pic_order_cnt_lsb_minus4 = sps->log2_max_pic_order_cnt_lsb_minus4;
    ppCmd->slice_group_change_cycle_size = slice->nbbits_of_slice_group_change_cycle;
    ppCmd->num_slice_group_minus1 = pps->num_slice_groups_minus1;
    ppCmd->slice_group_map_type = pps->slice_group_map_type;
    ppCmd->fmo_aso_en = 0;
#else
    (void)queInfo; // warning removal
    (void)parsedFp; // warning removal
    (void)CodedDataPhysAddr; // warning removal
    (void)OutputBufferPhysicalAddr; // warning removal
#endif
}

// /////////////////////////////////////////////////////////////////////////
//
//      Function to fill out the decode parameters
//      structure for an H264 mme transformer.
//

CodecStatus_t   Codec_MmeVideoH264_c::FillOutDecodeCommand()
{
    H264FrameParameters_t  *Parsed = (H264FrameParameters_t *)ParsedFrameParameters->FrameParameterStructure;
    //
    // Detach the pre-processor buffer and re-attach it to the decode context
    // this will make its release automatic on the freeing of the decode context.
    //

    Buffer_t PreProcessorBuffer;
    CodedFrameBuffer->ObtainAttachedBufferReference(mPreProcessorBufferType, &PreProcessorBuffer);
    SE_ASSERT(PreProcessorBuffer != NULL);

    DecodeContextBuffer->AttachBuffer(PreProcessorBuffer);      // Must be ordered, otherwise the pre-processor
    CodedFrameBuffer->DetachBuffer(PreProcessorBuffer);         // buffer will get released in the middle
    return PrepareDecodeCommand(&Parsed->SliceHeader, PreProcessorBuffer, ParsedFrameParameters->ReferenceFrame);
}


CodecStatus_t   Codec_MmeVideoH264_c::PrepareDecodeCommand(H264SliceHeader_t   *SliceHeader,
                                                           Buffer_t        PreProcessorBuffer,
                                                           bool            ReferenceFrame)
{
    CodecStatus_t                     Status;
    H264CodecDecodeContext_t         *Context = (H264CodecDecodeContext_t *)DecodeContext;
    H264_TransformParam_fmw_t        *Param;
    H264SequenceParameterSetHeader_t *SPS;
    unsigned int                      InputBuffer;
    unsigned int                      PreProcessorBufferSize;
    bool                  DowngradeDecode;
    unsigned int              MaxAllocatableBuffers;
    Buffer_t              DecodeBuffer;
    DecodeBufferComponentType_t   DecimatedComponent;
    DecodeBufferComponentType_t   DecodeComponent;
    //
    // For H264 we do not do slice decodes.
    //
    KnownLastSliceInFieldFrame                  = true;
    OS_LockMutex(&Lock);
    DecodeBuffer            = BufferState[CurrentDecodeBufferIndex].Buffer;
    DecimatedComponent          = DecimatedManifestationComponent;
    DecodeComponent         = RasterOutput ? VideoDecodeCopy : PrimaryManifestationComponent;

    //
    // If this is a reference frame, and no macroblock structure has been attached
    // to this decode buffer, obtain a reference frame slot, and obtain a macroblock
    // structure buffer and attach it.
    //

    if (ReferenceFrame && (BufferState[CurrentDecodeBufferIndex].ReferenceFrameSlot == INVALID_INDEX))
    {
        //
        // Obtain a reference frame slot
        //
        Status = ReferenceFrameSlotAllocate(CurrentDecodeBufferIndex);
        if (Status != CodecNoError)
        {
            SE_ERROR("Failed to obtain a reference frame slot\n");
            OS_UnLockMutex(&Lock);
            return Status;
        }

        //
        // Obtain a macroblock structure buffer
        //    First calculate the size,
        //    Second verify that there are enough decode buffers for the reference frame count
        //    Third verify that the macroblock structure memory is large enough
        //    Fourth allocate the actual buffer.
        //
        SPS = SliceHeader->SequenceParameterSet;

        Stream->GetDecodeBufferManager()->GetEstimatedBufferCount(&MaxAllocatableBuffers, ForManifestation);
        // 3 buffers may be in display pipe
        if (SPS->num_ref_frames > (MaxAllocatableBuffers - 3))
        {
            SE_ERROR("Insufficient memory allocated to cope with manifested frames (num_ref_frames:%d > (%d-3) max) - marking stream unplayable\n",
                     SPS->num_ref_frames, MaxAllocatableBuffers);
            // raise the event to signal stream unplayable
            Stream->MarkUnPlayable(STM_SE_PLAY_STREAM_MSG_REASON_CODE_INSUFFICIENT_MEMORY, true);
            OS_UnLockMutex(&Lock);
            return CodecError;
        }

        Stream->GetDecodeBufferManager()->GetEstimatedBufferCount(&MaxAllocatableBuffers, ForReference);
        if (SPS->num_ref_frames > MaxAllocatableBuffers)
        {
            SE_ERROR("Insufficient memory allocated to cope with reference frames (num_ref_frames:%d > %d max) - marking stream unplayable\n",
                     SPS->num_ref_frames, MaxAllocatableBuffers);
            // raise the event to signal stream unplayable
            Stream->MarkUnPlayable(STM_SE_PLAY_STREAM_MSG_REASON_CODE_INSUFFICIENT_MEMORY, true);
            OS_UnLockMutex(&Lock);
            return CodecError;
        }
    }

    //
    // Fill out the sub-components of the command data
    //
    memset(&Context->DecodeParameters, 0, sizeof(H264_TransformParam_fmw_t));
    memset(&Context->DecodeStatus, 0xa5, sizeof(H264_CommandStatus_fmw_t));
    FillOutDecodeCommandHostData(SliceHeader);
    FillOutDecodeCommandRefPicList();
    //
    // Fill out the remaining fields in the transform parameters structure.
    //
    Param = &Context->DecodeParameters;
    PreProcessorBuffer->ObtainDataReference(NULL, &PreProcessorBufferSize, (void **)&InputBuffer, PhysicalAddress);
    if (InputBuffer == NULL)
    {
        SE_ERROR("Unable to get PreProcessorBufferSize\n");
        OS_UnLockMutex(&Lock);
        return CodecError;
    }
    Param->PictureStartAddrBinBuffer = (unsigned int *)InputBuffer;  // Start address in the bin buffer
    Param->PictureStopAddrBinBuffer  = (unsigned int *)(InputBuffer + PreProcessorBufferSize); // Stop address in the bin buffer
    Param->MaxSESBSize = H264_PP_SESB_SIZE;

    //
    // Here we only support decimation by 2 in the vertical,
    // if the decimated buffer is present, then we decimate 2 vertical

    if (!Stream->GetDecodeBufferManager()->ComponentPresent(DecodeBuffer, DecimatedComponent))
    {
        // Normal Case
        if (RasterOutput && ParsedFrameParameters->ReferenceFrame)
        {
            Param->MainAuxEnable = H264_REF_MAIN_DISP_MAIN_EN;
        }
        else
        {
            Param->MainAuxEnable = H264_DISP_MAIN_EN;
        }

        Param->HorizontalDecimationFactor           = HDEC_1;
        Param->VerticalDecimationFactor             = VDEC_1;
    }
    else
    {
        if (RasterOutput && ParsedFrameParameters->ReferenceFrame)
        {
            Param->MainAuxEnable = H264_REF_MAIN_DISP_MAIN_AUX_EN;
        }
        else
        {
            Param->MainAuxEnable = H264_DISP_AUX_MAIN_EN;
        }

        Param->HorizontalDecimationFactor = (Stream->GetDecodeBufferManager()->DecimationFactor(DecodeBuffer, 0) == 2) ?
                                            HDEC_ADVANCED_2 :
                                            HDEC_ADVANCED_4;
        Param->VerticalDecimationFactor   = VDEC_ADVANCED_2_INT;
    }

    if (RefBufferType == PolicyValueMBBuffOnly)
    {
        if (Stream->GetDecodeBufferManager()->ComponentPresent(DecodeBuffer, DecimatedComponent))
        {
            //Reference (MB format) and decimation (Raster2B) buffers generated
            Param->MainAuxEnable = H264_REF_MAIN_DISP_AUX_EN;
        }
        else
        {
            //Only reference buffers (MB format) are generated; No display buffers
            Param->MainAuxEnable = H264_REF_MAIN_EN;
        }
    }

    //
    DowngradeDecode             = ParsedFrameParameters->ApplySubstandardDecode &&
                                  SLICE_TYPE_IS(((H264FrameParameters_t *)ParsedFrameParameters->FrameParameterStructure)->SliceHeader.slice_type, H264_SLICE_TYPE_B);
    Param->DecodingMode                         = DowngradeDecode ? H264_DOWNGRADED_DECODE_LEVEL1 :
                                                  (Player->PolicyValue(Playback, Stream, PolicyErrorDecodingLevel) == PolicyValuePolicyErrorDecodingLevelMaximum ?
                                                   H264_ERC_MAX_DECODE : H264_NORMAL_DECODE);
    Param->AdditionalFlags                      = H264_ADDITIONAL_FLAG_CEH;
    //
    // Omega2 Reference Buffers are stored in Decoded Buffer
    //
    Param->DecodedBufferAddress.Luma_p          = (H264_LumaAddress_t)Stream->GetDecodeBufferManager()->Luma(DecodeBuffer, DecodeComponent);
    Param->DecodedBufferAddress.Chroma_p        = (H264_ChromaAddress_t)Stream->GetDecodeBufferManager()->Chroma(DecodeBuffer, DecodeComponent);
    Param->DecodedBufferAddress.MBStruct_p      = (H264_MBStructureAddress_t)Stream->GetDecodeBufferManager()->ComponentBaseAddress(DecodeBuffer, VideoMacroblockStructure);

    if (RefBufferType == PolicyValueMBBuffOnly)
    {
        Param->DisplayBufferAddress.DisplayLuma_p   = NULL;
        Param->DisplayBufferAddress.DisplayChroma_p = NULL;
    }
    else
    {
        /* Raster2B Display Buffer */
        DecodeBufferComponentType_t DisplayComponent = PrimaryManifestationComponent;
        Param->DisplayBufferAddress.DisplayLuma_p    = (H264_LumaAddress_t)Stream->GetDecodeBufferManager()->Luma(DecodeBuffer, DisplayComponent);
        Param->DisplayBufferAddress.DisplayChroma_p  = (H264_ChromaAddress_t)Stream->GetDecodeBufferManager()->Chroma(DecodeBuffer, DisplayComponent);
    }

    if (Stream->GetDecodeBufferManager()->ComponentPresent(DecodeBuffer, DecimatedComponent))
    {
        Param->DisplayBufferAddress.DisplayDecimatedLuma_p   = (H264_LumaAddress_t)Stream->GetDecodeBufferManager()->Luma(DecodeBuffer, DecimatedComponent);
        Param->DisplayBufferAddress.DisplayDecimatedChroma_p = (H264_ChromaAddress_t)Stream->GetDecodeBufferManager()->Chroma(DecodeBuffer, DecimatedComponent);
    }

    OS_UnLockMutex(&Lock);
    // Bug 22786 : base_view_flag must be set for AVC Streams.
    // For MVC streams, this flag must be set to 1 for the base view and 0 for others.
    Param->base_view_flag = 1;
    // Needed for SVCDecoder firmware
    Param->OutputLayerParams.no_inter_layer_pred_flag = 1;
    Param->CurrentLayerParams.no_inter_layer_pred_flag = 1;
    Param->CurrentLayerParams.TargetLayerFlag = 1;

    //
    // Fill out the actual command
    //
    memset(&Context->BaseContext.MMECommand, 0, sizeof(MME_Command_t));
    Context->BaseContext.MMECommand.CmdStatus.AdditionalInfoSize = sizeof(H264_CommandStatus_fmw_t);
    Context->BaseContext.MMECommand.CmdStatus.AdditionalInfo_p   = (MME_GenericParams_t)(&Context->DecodeStatus);
    Context->BaseContext.MMECommand.ParamSize                    = sizeof(H264_TransformParam_fmw_t);
    Context->BaseContext.MMECommand.Param_p                      = (MME_GenericParams_t)(&Context->DecodeParameters);

    return CodecNoError;
}


// /////////////////////////////////////////////////////////////////////////
//
//      Function to fill out the host data segment of the h264 decode
//      parameters structure for the H264 mme transformer.
//
//      This function must be mutex-locked by caller
//
CodecStatus_t   Codec_MmeVideoH264_c::FillOutDecodeCommandHostData(H264SliceHeader_t *SliceHeader)
{
    H264CodecDecodeContext_t                 *Context = (H264CodecDecodeContext_t *)DecodeContext;
    H264SequenceParameterSetHeader_t         *SPS;
    H264_HostData_t                          *HostData;

    OS_AssertMutexHeld(&Lock);
    SPS         = SliceHeader->SequenceParameterSet;
    HostData    = &Context->DecodeParameters.HostData;

    HostData->PictureNumber  = 0;
    HostData->PicOrderCntTop = SliceHeader->PicOrderCntTop;
    HostData->PicOrderCntBot = SliceHeader->PicOrderCntBot;

    if (SPS->frame_mbs_only_flag || !SliceHeader->field_pic_flag)
    {
        HostData->PicOrderCnt    = min(SliceHeader->PicOrderCntTop, SliceHeader->PicOrderCntBot);
        HostData->DescriptorType = SPS->mb_adaptive_frame_field_flag ? H264_PIC_DESCRIPTOR_AFRAME : H264_PIC_DESCRIPTOR_FRAME;
    }
    else
    {
        HostData->PicOrderCnt    = SliceHeader->bottom_field_flag ? SliceHeader->PicOrderCntBot : SliceHeader->PicOrderCntTop;
        HostData->DescriptorType = SliceHeader->bottom_field_flag ? H264_PIC_DESCRIPTOR_FIELD_BOTTOM : H264_PIC_DESCRIPTOR_FIELD_TOP;
    }

    HostData->ReferenceType      = (SliceHeader->nal_ref_idc != 0) ?                             // Its a reference frame
                                   (SliceHeader->dec_ref_pic_marking.long_term_reference_flag ?
                                    H264_LONG_TERM_REFERENCE :
                                    H264_SHORT_TERM_REFERENCE) :
                                   H264_UNUSED_FOR_REFERENCE;
    HostData->IdrFlag            = SliceHeader->nal_unit_type == NALU_TYPE_IDR;

#if (H264_MME_VERSION >= 67)
    // If the current picture is the first IDR or first I picture of the stream and mIsFirstIdrOrIofStream is true,
    // then set the parameter mIsFirstIdrOrIofStream to false
    // and set HostData->FirstIdrOrIofStream to one otherwise set it to zero.
    if (mIsFirstIdrOrIofStream &&
        ((SliceHeader->nal_unit_type == NALU_TYPE_IDR) || (SliceHeader->slice_type == H264_SLICE_TYPE_I) || (SliceHeader->slice_type == H264_SLICE_TYPE_I_ALL)))
    {
        HostData->FirstIdrOrIofStream = 1;
        mIsFirstIdrOrIofStream = false;
    }
    else
    {
        HostData->FirstIdrOrIofStream = 0;
    }
#endif

    HostData->NonExistingFlag = 0;

    BufferState[CurrentDecodeBufferIndex].DecodedAsFrames       = (SliceHeader->field_pic_flag == 0);
    BufferState[CurrentDecodeBufferIndex].DecodedWithMbaff      = (SPS->mb_adaptive_frame_field_flag != 0);

    //
    // Save the host data only if this is a reference frame
    //

    if (ParsedFrameParameters->ReferenceFrame)
    {
        memcpy(&RecordedHostData[CurrentDecodeBufferIndex], HostData, sizeof(H264_HostData_t));
    }

    return CodecNoError;
}


// /////////////////////////////////////////////////////////////////////////
//
//      Function to fill out the reference picture list segment of the
//      h264 decode parameters structure for the H264 mme transformer.
//
//      This function must be mutex-locked by caller
//
CodecStatus_t   Codec_MmeVideoH264_c::FillOutDecodeCommandRefPicList()
{
    unsigned int                i;
    H264CodecDecodeContext_t   *Context = (H264CodecDecodeContext_t *)DecodeContext;
    H264_RefPictListAddress_t  *RefPicLists;
    unsigned int                ReferenceId;
    unsigned int                Slot;
    unsigned int                DescriptorIndex;
    Buffer_t                  ReferenceBuffer;
    DecodeBufferComponentType_t DecodeComponent;
    OS_AssertMutexHeld(&Lock);
    DecodeComponent = RasterOutput ? VideoDecodeCopy : PrimaryManifestationComponent;

    RefPicLists = &Context->DecodeParameters.InitialRefPictList;

    //
    // Setup the frame address array
    //

    for (i = 0; i < DecodeBufferCount; i++)
        if ((BufferState[i].ReferenceFrameCount != 0) && (BufferState[i].ReferenceFrameSlot != INVALID_INDEX))
        {
            Slot        = BufferState[i].ReferenceFrameSlot;
            ReferenceBuffer = BufferState[i].Buffer;
            RefPicLists->ReferenceFrameAddress[Slot].DecodedBufferAddress.Luma_p     = (H264_LumaAddress_t)Stream->GetDecodeBufferManager()->Luma(ReferenceBuffer, DecodeComponent);;
            RefPicLists->ReferenceFrameAddress[Slot].DecodedBufferAddress.Chroma_p   = (H264_ChromaAddress_t)Stream->GetDecodeBufferManager()->Chroma(ReferenceBuffer, DecodeComponent);;
            RefPicLists->ReferenceFrameAddress[Slot].DecodedBufferAddress.MBStruct_p = (H264_MBStructureAddress_t)Stream->GetDecodeBufferManager()->ComponentBaseAddress(ReferenceBuffer,
                                                                                       VideoMacroblockStructure);
        }

    //
    // Create the reference picture lists, NOTE this is a fudge, the
    // delta-phi requires reference picture lists for B and P pictures
    // to be supplied for all pictures (thats I B or P), so we have to
    // make up reference picture lists for a frame that this isn't.
    //
    NumberOfUsedDescriptors     = 0;
    memset(DescriptorIndices, 0xff, sizeof(DescriptorIndices));
    RefPicLists->ReferenceDescriptorsBitsField  = 0;                            // Default to all being frame descriptors
    //
    // For each entry in the lists we check if the descriptor is already available
    // and point to it. If it is not available we create a new descriptor.
    //
    RefPicLists->InitialPList0.RefPiclistSize   = DecodeContext->ReferenceFrameList[P_REF_PIC_LIST].EntryCount;

    for (i = 0; i < RefPicLists->InitialPList0.RefPiclistSize; i++)
    {
        unsigned int    UsageCode                       = DecodeContext->ReferenceFrameList[P_REF_PIC_LIST].ReferenceDetails[i].UsageCode;
        unsigned int    BufferIndex                     = DecodeContext->ReferenceFrameList[P_REF_PIC_LIST].EntryIndicies[i];
        Slot                        = BufferState[BufferIndex].ReferenceFrameSlot;
        ReferenceId                                     = (3 * Slot) + UsageCode;
        DescriptorIndex                                 = DescriptorIndices[ReferenceId];

        if (DescriptorIndex == 0xff)
        {
            DescriptorIndex                             = FillOutNewDescriptor(ReferenceId, BufferIndex, &DecodeContext->ReferenceFrameList[P_REF_PIC_LIST].ReferenceDetails[i]);
        }

        RefPicLists->InitialPList0.RefPicList[i]        = DescriptorIndex;
    }

    RefPicLists->InitialBList0.RefPiclistSize   = DecodeContext->ReferenceFrameList[B_REF_PIC_LIST_0].EntryCount;

    for (i = 0; i < RefPicLists->InitialBList0.RefPiclistSize; i++)
    {
        unsigned int    UsageCode                       = DecodeContext->ReferenceFrameList[B_REF_PIC_LIST_0].ReferenceDetails[i].UsageCode;
        unsigned int    BufferIndex                     = DecodeContext->ReferenceFrameList[B_REF_PIC_LIST_0].EntryIndicies[i];
        Slot                        = BufferState[BufferIndex].ReferenceFrameSlot;
        ReferenceId                                     = (3 * Slot) + UsageCode;
        DescriptorIndex                                 = DescriptorIndices[ReferenceId];

        if (DescriptorIndex == 0xff)
        {
            DescriptorIndex                             = FillOutNewDescriptor(ReferenceId, BufferIndex, &DecodeContext->ReferenceFrameList[B_REF_PIC_LIST_0].ReferenceDetails[i]);
        }

        RefPicLists->InitialBList0.RefPicList[i]        = DescriptorIndex;
    }

    RefPicLists->InitialBList1.RefPiclistSize   = DecodeContext->ReferenceFrameList[B_REF_PIC_LIST_1].EntryCount;

    for (i = 0; i < RefPicLists->InitialBList1.RefPiclistSize; i++)
    {
        unsigned int    UsageCode                       = DecodeContext->ReferenceFrameList[B_REF_PIC_LIST_1].ReferenceDetails[i].UsageCode;
        unsigned int    BufferIndex                     = DecodeContext->ReferenceFrameList[B_REF_PIC_LIST_1].EntryIndicies[i];
        Slot                        = BufferState[BufferIndex].ReferenceFrameSlot;
        ReferenceId                                     = (3 * Slot) + UsageCode;
        DescriptorIndex                                 = DescriptorIndices[ReferenceId];

        if (DescriptorIndex == 0xff)
        {
            DescriptorIndex                             = FillOutNewDescriptor(ReferenceId, BufferIndex, &DecodeContext->ReferenceFrameList[B_REF_PIC_LIST_1].ReferenceDetails[i]);
        }

        RefPicLists->InitialBList1.RefPicList[i]        = DescriptorIndex;
    }

    return CodecNoError;
}


// /////////////////////////////////////////////////////////////////////////
//
//      Function to fill out a single descriptor for a h264 reference frame
//
//      This function must be mutex-locked by caller
//
unsigned int   Codec_MmeVideoH264_c::FillOutNewDescriptor(
    unsigned int             ReferenceId,
    unsigned int             BufferIndex,
    ReferenceDetails_t  *Details)
{
    H264CodecDecodeContext_t  *Context = (H264CodecDecodeContext_t *)DecodeContext;
    H264_RefPictListAddress_t *RefPicLists;
    unsigned int               DescriptorIndex;
    unsigned int               SlotIndex;
    H264_PictureDescriptor_t  *Descriptor;

    OS_AssertMutexHeld(&Lock);
    RefPicLists = &Context->DecodeParameters.InitialRefPictList;
    DescriptorIndex                     = NumberOfUsedDescriptors++;
    SlotIndex               = BufferState[BufferIndex].ReferenceFrameSlot;
    DescriptorIndices[ReferenceId]      = DescriptorIndex;
    Descriptor                          = &RefPicLists->PictureDescriptor[DescriptorIndex];
    Descriptor->FrameIndex              = SlotIndex;
    //
    // Setup the host data
    //
    memcpy(&Descriptor->HostData, &RecordedHostData[BufferIndex], sizeof(H264_HostData_t));
    Descriptor->HostData.PictureNumber  = Details->PictureNumber;
    Descriptor->HostData.PicOrderCnt    = Details->PicOrderCnt;
    Descriptor->HostData.PicOrderCntTop = Details->PicOrderCntTop;
    Descriptor->HostData.PicOrderCntBot = Details->PicOrderCntBot;
    Descriptor->HostData.ReferenceType  = Details->LongTermReference ? H264_LONG_TERM_REFERENCE : H264_SHORT_TERM_REFERENCE;

    //
    // The nature of the frame being referenced is in its original descriptor type
    //

    if ((Descriptor->HostData.DescriptorType != H264_PIC_DESCRIPTOR_FRAME) &&
        (Descriptor->HostData.DescriptorType != H264_PIC_DESCRIPTOR_AFRAME))
    {
        RefPicLists->ReferenceDescriptorsBitsField      |= 1 << SlotIndex;
    }

    //
    // We now modify the descriptor type to reflect the way in which we reference the frame
    //

    if (Details->UsageCode == REF_PIC_USE_FRAME)
    {
        if ((Descriptor->HostData.DescriptorType != H264_PIC_DESCRIPTOR_AFRAME) &&
            (Descriptor->HostData.DescriptorType != H264_PIC_DESCRIPTOR_FRAME))
        {
            // REMINDER: remember to mutex lock/unlock BufferState[] access if you are going to use that following line
            // Descriptor->HostData.DescriptorType = BufferState[BufferIndex].DecodedWithMbaff ? H264_PIC_DESCRIPTOR_AFRAME : H264_PIC_DESCRIPTOR_FRAME;
            Descriptor->HostData.DescriptorType = H264_PIC_DESCRIPTOR_FRAME;
        }
    }
    else if (Details->UsageCode == REF_PIC_USE_FIELD_TOP)
    {
        Descriptor->HostData.DescriptorType     = H264_PIC_DESCRIPTOR_FIELD_TOP;
    }
    else if (Details->UsageCode == REF_PIC_USE_FIELD_BOT)
    {
        Descriptor->HostData.DescriptorType     = H264_PIC_DESCRIPTOR_FIELD_BOTTOM;
    }

    //
    // And return the index
    //
    return DescriptorIndex;
}

////////////////////////////////////////////////////////////////////////////
///
/// Unconditionally return success.
///
/// Success and failure codes are located entirely in the generic MME structures
/// allowing the super-class to determine whether the decode was successful. This
/// means that we have no work to do here.
///
/// \return CodecNoError
///
CodecStatus_t   Codec_MmeVideoH264_c::ValidateDecodeContext(CodecBaseDecodeContext_t *Context)
{
    H264CodecDecodeContext_t  *H264Context = (H264CodecDecodeContext_t *)Context;
    SE_EXTRAVERB(group_decoder_video, "Decode took %6dus\n", H264Context->DecodeStatus.DecodeTimeInMicros);

    if (H264Context->DecodeStatus.ErrorCode != H264_DECODER_NO_ERROR)
    {
        SE_INFO(group_decoder_video, "H264 decode status:%08x\n", H264Context->DecodeStatus.ErrorCode);
#if 0
        DumpMMECommand(&Context->MMECommand);
#endif
    }

    return CodecNoError;
}

// /////////////////////////////////////////////////////////////////////////
//
//      Function to dump out the set stream
//      parameters from an mme command.
//

CodecStatus_t   Codec_MmeVideoH264_c::DumpSetStreamParameters(void    *Parameters)
{
    unsigned int             i;
    H264_SetGlobalParam_t   *StreamParams;
    StreamParams    = (H264_SetGlobalParam_t *)Parameters;
    SE_VERBOSE(group_decoder_video, "AZA - STREAM PARAMS %p\n", StreamParams);
    SE_VERBOSE(group_decoder_video, "AZA -    SPS\n");
    SE_VERBOSE(group_decoder_video, "AZA -       DecoderProfileConformance            = %d\n", StreamParams->H264SetGlobalParamSPS.DecoderProfileConformance);
    SE_VERBOSE(group_decoder_video, "AZA -       level_idc                            = %d\n", StreamParams->H264SetGlobalParamSPS.level_idc);
    SE_VERBOSE(group_decoder_video, "AZA -       log2_max_frame_num_minus4            = %d\n", StreamParams->H264SetGlobalParamSPS.log2_max_frame_num_minus4);
    SE_VERBOSE(group_decoder_video, "AZA -       pic_order_cnt_type                   = %d\n", StreamParams->H264SetGlobalParamSPS.pic_order_cnt_type);
    SE_VERBOSE(group_decoder_video, "AZA -       log2_max_pic_order_cnt_lsb_minus4    = %d\n", StreamParams->H264SetGlobalParamSPS.log2_max_pic_order_cnt_lsb_minus4);
    SE_VERBOSE(group_decoder_video, "AZA -       delta_pic_order_always_zero_flag     = %d\n", StreamParams->H264SetGlobalParamSPS.delta_pic_order_always_zero_flag);
    SE_VERBOSE(group_decoder_video, "AZA -       pic_width_in_mbs_minus1              = %d\n", StreamParams->H264SetGlobalParamSPS.pic_width_in_mbs_minus1);
    SE_VERBOSE(group_decoder_video, "AZA -       pic_height_in_map_units_minus1       = %d\n", StreamParams->H264SetGlobalParamSPS.pic_height_in_map_units_minus1);
    SE_VERBOSE(group_decoder_video, "AZA -       frame_mbs_only_flag                  = %d\n", StreamParams->H264SetGlobalParamSPS.frame_mbs_only_flag);
    SE_VERBOSE(group_decoder_video, "AZA -       mb_adaptive_frame_field_flag         = %d\n", StreamParams->H264SetGlobalParamSPS.mb_adaptive_frame_field_flag);
    SE_VERBOSE(group_decoder_video, "AZA -       direct_8x8_inference_flag            = %d\n", StreamParams->H264SetGlobalParamSPS.direct_8x8_inference_flag);
    SE_VERBOSE(group_decoder_video, "AZA -       chroma_format_idc                    = %d\n", StreamParams->H264SetGlobalParamSPS.chroma_format_idc);
    SE_VERBOSE(group_decoder_video, "AZA -    PPS\n");
    SE_VERBOSE(group_decoder_video, "AZA -       entropy_coding_mode_flag             = %d\n", StreamParams->H264SetGlobalParamPPS.entropy_coding_mode_flag);
    SE_VERBOSE(group_decoder_video, "AZA -       pic_order_present_flag               = %d\n", StreamParams->H264SetGlobalParamPPS.pic_order_present_flag);
    SE_VERBOSE(group_decoder_video, "AZA -       num_ref_idx_l0_active_minus1         = %d\n", StreamParams->H264SetGlobalParamPPS.num_ref_idx_l0_active_minus1);
    SE_VERBOSE(group_decoder_video, "AZA -       num_ref_idx_l1_active_minus1         = %d\n", StreamParams->H264SetGlobalParamPPS.num_ref_idx_l1_active_minus1);
    SE_VERBOSE(group_decoder_video, "AZA -       weighted_pred_flag                   = %d\n", StreamParams->H264SetGlobalParamPPS.weighted_pred_flag);
    SE_VERBOSE(group_decoder_video, "AZA -       weighted_bipred_idc                  = %d\n", StreamParams->H264SetGlobalParamPPS.weighted_bipred_idc);
    SE_VERBOSE(group_decoder_video, "AZA -       pic_init_qp_minus26                  = %d\n", StreamParams->H264SetGlobalParamPPS.pic_init_qp_minus26);
    SE_VERBOSE(group_decoder_video, "AZA -       chroma_qp_index_offset               = %d\n", StreamParams->H264SetGlobalParamPPS.chroma_qp_index_offset);
    SE_VERBOSE(group_decoder_video, "AZA -       deblocking_filter_control_present_flag = %d\n", StreamParams->H264SetGlobalParamPPS.deblocking_filter_control_present_flag);
    SE_VERBOSE(group_decoder_video, "AZA -       constrained_intra_pred_flag          = %d\n", StreamParams->H264SetGlobalParamPPS.constrained_intra_pred_flag);
    SE_VERBOSE(group_decoder_video, "AZA -       transform_8x8_mode_flag              = %d\n", StreamParams->H264SetGlobalParamPPS.transform_8x8_mode_flag);
    SE_VERBOSE(group_decoder_video, "AZA -       second_chroma_qp_index_offset        = %d\n", StreamParams->H264SetGlobalParamPPS.second_chroma_qp_index_offset);
    SE_VERBOSE(group_decoder_video, "AZA -       ScalingListUpdated                   = %d\n", StreamParams->H264SetGlobalParamPPS.ScalingList.ScalingListUpdated);

    for (i = 0; i < 6; i++)
        SE_VERBOSE(group_decoder_video, "AZA -       ScalingList4x4[%d]                   = %02x %02x %02x %02x %02x %02x %02x %02x\n", i,
                   StreamParams->H264SetGlobalParamPPS.ScalingList.FirstSixScalingList[i][0], StreamParams->H264SetGlobalParamPPS.ScalingList.FirstSixScalingList[i][1],
                   StreamParams->H264SetGlobalParamPPS.ScalingList.FirstSixScalingList[i][2], StreamParams->H264SetGlobalParamPPS.ScalingList.FirstSixScalingList[i][3],
                   StreamParams->H264SetGlobalParamPPS.ScalingList.FirstSixScalingList[i][4], StreamParams->H264SetGlobalParamPPS.ScalingList.FirstSixScalingList[i][5],
                   StreamParams->H264SetGlobalParamPPS.ScalingList.FirstSixScalingList[i][6], StreamParams->H264SetGlobalParamPPS.ScalingList.FirstSixScalingList[i][7]);

    for (i = 0; i < 2; i++)
        SE_VERBOSE(group_decoder_video, "AZA -       ScalingList8x8[%d]                   = %02x %02x %02x %02x %02x %02x %02x %02x\n", i,
                   StreamParams->H264SetGlobalParamPPS.ScalingList.NextTwoScalingList[i][0], StreamParams->H264SetGlobalParamPPS.ScalingList.NextTwoScalingList[i][1],
                   StreamParams->H264SetGlobalParamPPS.ScalingList.NextTwoScalingList[i][2], StreamParams->H264SetGlobalParamPPS.ScalingList.NextTwoScalingList[i][3],
                   StreamParams->H264SetGlobalParamPPS.ScalingList.NextTwoScalingList[i][4], StreamParams->H264SetGlobalParamPPS.ScalingList.NextTwoScalingList[i][5],
                   StreamParams->H264SetGlobalParamPPS.ScalingList.NextTwoScalingList[i][6], StreamParams->H264SetGlobalParamPPS.ScalingList.NextTwoScalingList[i][7]);

    return CodecNoError;
}

// /////////////////////////////////////////////////////////////////////////
//
//      Function to dump out the decode
//      parameters from an mme command.
//

CodecStatus_t   Codec_MmeVideoH264_c::DumpDecodeParameters(void    *Parameters)
{
    unsigned int                      i;
    unsigned int                      MaxDescriptor;
    H264_TransformParam_fmw_t        *FrameParams;
    H264_HostData_t                  *HostData;
    H264_RefPictListAddress_t        *RefPicLists;

    FrameParams = (H264_TransformParam_fmw_t *)Parameters;
    HostData    = &FrameParams->HostData;
    RefPicLists = &FrameParams->InitialRefPictList;
    SE_VERBOSE(group_decoder_video, "AZA - FRAME PARAMS %p\n", FrameParams);
    SE_VERBOSE(group_decoder_video, "AZA -       PictureStartAddrBinBuffer            = %p\n", FrameParams->PictureStartAddrBinBuffer);
    SE_VERBOSE(group_decoder_video, "AZA -       PictureStopAddrBinBuffer             = %p\n", FrameParams->PictureStopAddrBinBuffer);
    SE_VERBOSE(group_decoder_video, "AZA -       MaxSESBSize                          = %d bytes\n", FrameParams->MaxSESBSize);
    SE_VERBOSE(group_decoder_video, "AZA -       MainAuxEnable                        = %08x\n", FrameParams->MainAuxEnable);
    SE_VERBOSE(group_decoder_video, "AZA -       HorizontalDecimationFactor           = %08x\n", FrameParams->HorizontalDecimationFactor);
    SE_VERBOSE(group_decoder_video, "AZA -       VerticalDecimationFactor             = %08x\n", FrameParams->VerticalDecimationFactor);
    SE_VERBOSE(group_decoder_video, "AZA -       DecodingMode                         = %d\n", FrameParams->DecodingMode);
    SE_VERBOSE(group_decoder_video, "AZA -       AdditionalFlags                      = %08x\n", FrameParams->AdditionalFlags);
    SE_VERBOSE(group_decoder_video, "AZA -       Luma_p                               = %p\n", FrameParams->DecodedBufferAddress.Luma_p);
    SE_VERBOSE(group_decoder_video, "AZA -       Chroma_p                             = %p\n", FrameParams->DecodedBufferAddress.Chroma_p);
    SE_VERBOSE(group_decoder_video, "AZA -       MBStruct_p                           = %p\n", FrameParams->DecodedBufferAddress.MBStruct_p);
    SE_VERBOSE(group_decoder_video, "AZA -       HostData\n");
    SE_VERBOSE(group_decoder_video, "AZA -           PictureNumber                    = %d\n", HostData->PictureNumber);
    SE_VERBOSE(group_decoder_video, "AZA -           PicOrderCntTop                   = %d\n", HostData->PicOrderCntTop);
    SE_VERBOSE(group_decoder_video, "AZA -           PicOrderCntBot                   = %d\n", HostData->PicOrderCntBot);
    SE_VERBOSE(group_decoder_video, "AZA -           PicOrderCnt                      = %d\n", HostData->PicOrderCnt);
    SE_VERBOSE(group_decoder_video, "AZA -           DescriptorType                   = %d\n", HostData->DescriptorType);
    SE_VERBOSE(group_decoder_video, "AZA -           ReferenceType                    = %d\n", HostData->ReferenceType);
    SE_VERBOSE(group_decoder_video, "AZA -           IdrFlag                          = %d\n", HostData->IdrFlag);
    SE_VERBOSE(group_decoder_video, "AZA -           NonExistingFlag                  = %d\n", HostData->NonExistingFlag);
    SE_VERBOSE(group_decoder_video, "AZA -       InitialRefPictList\n");
    SE_VERBOSE(group_decoder_video, "AZA -           ReferenceFrameAddresses\n");
    SE_VERBOSE(group_decoder_video, "AZA -                 Luma    Chroma   MBStruct\n");

    for (i = 0; i < DecodeBufferCount; i++)
        SE_VERBOSE(group_decoder_video, "AZA -               %p %p %p\n",
                   RefPicLists->ReferenceFrameAddress[i].DecodedBufferAddress.Luma_p,
                   RefPicLists->ReferenceFrameAddress[i].DecodedBufferAddress.Chroma_p,
                   RefPicLists->ReferenceFrameAddress[i].DecodedBufferAddress.MBStruct_p);

    MaxDescriptor       = 0;
    SE_VERBOSE(group_decoder_video, "AZA -           InitialPList0 Descriptor Indices\n");
    SE_VERBOSE(group_decoder_video, "AZA -               ");

    for (i = 0; i < RefPicLists->InitialPList0.RefPiclistSize; i++)
    {
        SE_VERBOSE(group_decoder_video, "%02d  ", RefPicLists->InitialPList0.RefPicList[i]);
        MaxDescriptor   = max(MaxDescriptor, (RefPicLists->InitialPList0.RefPicList[i] + 1));
    }

    SE_VERBOSE(group_decoder_video, "\n");
    SE_VERBOSE(group_decoder_video, "AZA -           InitialBList0 Descriptor Indices\n");
    SE_VERBOSE(group_decoder_video, "AZA -               ");

    for (i = 0; i < RefPicLists->InitialBList0.RefPiclistSize; i++)
    {
        SE_VERBOSE(group_decoder_video, "%02d  ", RefPicLists->InitialBList0.RefPicList[i]);
        MaxDescriptor   = max(MaxDescriptor, (RefPicLists->InitialBList0.RefPicList[i] + 1));
    }

    SE_VERBOSE(group_decoder_video, "\n");
    SE_VERBOSE(group_decoder_video, "AZA -           InitialBList1 Descriptor Indices\n");
    SE_VERBOSE(group_decoder_video, "AZA -               ");

    for (i = 0; i < RefPicLists->InitialBList1.RefPiclistSize; i++)
    {
        SE_VERBOSE(group_decoder_video, "%02d  ", RefPicLists->InitialBList1.RefPicList[i]);
        MaxDescriptor   = max(MaxDescriptor, (RefPicLists->InitialBList1.RefPicList[i] + 1));
    }

    SE_VERBOSE(group_decoder_video, "\n");
    SE_VERBOSE(group_decoder_video, "AZA -           Picture descriptors (ReferenceDescriptorsBitsField = %08x)\n", RefPicLists->ReferenceDescriptorsBitsField);

    for (i = 0; i < MaxDescriptor; i++)
        SE_VERBOSE(group_decoder_video, "AZA -               Desc %d - Buff %2d, PN = %2d, Cnt T %2d B %2d X %2d, DT = %d, RT = %d, IDR = %d, NEF = %d\n",
                   i, RefPicLists->PictureDescriptor[i].FrameIndex,
                   RefPicLists->PictureDescriptor[i].HostData.PictureNumber,
                   RefPicLists->PictureDescriptor[i].HostData.PicOrderCntTop,
                   RefPicLists->PictureDescriptor[i].HostData.PicOrderCntBot,
                   RefPicLists->PictureDescriptor[i].HostData.PicOrderCnt,
                   RefPicLists->PictureDescriptor[i].HostData.DescriptorType,
                   RefPicLists->PictureDescriptor[i].HostData.ReferenceType,
                   RefPicLists->PictureDescriptor[i].HostData.IdrFlag,
                   RefPicLists->PictureDescriptor[i].HostData.NonExistingFlag);

    return CodecNoError;
}


// /////////////////////////////////////////////////////////////////////////
//
//      A local release reference frame, the external one inserts into our
//  processing list, and the parent class one is generic, this version
//  of the function releases the reference list slot allocation, then
//  passes control to the generic function.
//

CodecStatus_t   Codec_MmeVideoH264_c::H264ReleaseReferenceFrame(unsigned int     ReferenceFrameDecodeIndex)
{
    CodecStatus_t   Status;
    unsigned int    Index;
    OS_LockMutex(&Lock);

    //
    // Release the appropriate entries in the reference list slot array
    //

    if (ReferenceFrameDecodeIndex == CODEC_RELEASE_ALL)
    {
        memset(ReferenceFrameSlotUsed, 0, H264_MAX_REFERENCE_FRAMES * sizeof(bool));
        OutstandingSlotAllocationRequest    = INVALID_INDEX;
    }
    else
    {
        Status = TranslateDecodeIndex(ReferenceFrameDecodeIndex, &Index);
        if ((Status == CodecNoError) && (BufferState[Index].ReferenceFrameCount == 1) &&
            (BufferState[Index].ReferenceFrameSlot != INVALID_INDEX))
        {
            ReferenceFrameSlotUsed[BufferState[Index].ReferenceFrameSlot] = false;
        }
    }

    //
    // Can we satisfy an outstanding reference frame slot request
    //
    ReferenceFrameSlotAllocate(INVALID_INDEX);
    //
    // And pass on down the generic chain
    //
    OS_UnLockMutex(&Lock);
    return Codec_MmeVideo_c::ReleaseReferenceFrame(ReferenceFrameDecodeIndex);
}


// /////////////////////////////////////////////////////////////////////////
//
//      A function to handle allocation of reference frame slots.
//  Split out since it can be called on a decode where the
//  allocation may be deferred, and on release where a deferred
//  allocation may be enacted. Only one allocation may be deferred
//  at any one time.
//
//      This function must be mutex-locked by caller
//
CodecStatus_t   Codec_MmeVideoH264_c::ReferenceFrameSlotAllocate(unsigned int        BufferIndex)
{
    unsigned int    i;
    OS_AssertMutexHeld(&Lock);

    //
    // Is there any outstanding request (that is still outstanding).
    //

    if (OutstandingSlotAllocationRequest != INVALID_INDEX)
    {
        if (BufferState[OutstandingSlotAllocationRequest].ReferenceFrameCount != 0)
        {
            for (i = 0; (i < H264_MAX_REFERENCE_FRAMES) && ReferenceFrameSlotUsed[i]; i++);

            // Did we find one, if not just fall through
            if (i < H264_MAX_REFERENCE_FRAMES)
            {
                ReferenceFrameSlotUsed[i]                       = true;
                BufferState[OutstandingSlotAllocationRequest].ReferenceFrameSlot    = i;
                OutstandingSlotAllocationRequest                    = INVALID_INDEX;
            }
        }
        else
        {
            OutstandingSlotAllocationRequest    = INVALID_INDEX;
        }
    }

    //
    // Do we have a new request
    //

    if (BufferIndex != INVALID_INDEX)
    {
        //
        // If there is an outstanding request we have a problem
        //
        if (OutstandingSlotAllocationRequest != INVALID_INDEX)
        {
            SE_ERROR("Outstanding request when new request is received\n");
            return CodecError;
        }

        //
        // Attempt to allocate, should we fail then make the request outstanding
        //

        for (i = 0; (i < H264_MAX_REFERENCE_FRAMES) && ReferenceFrameSlotUsed[i]; i++);

        if (i < H264_MAX_REFERENCE_FRAMES)
        {
            ReferenceFrameSlotUsed[i]           = true;
            BufferState[BufferIndex].ReferenceFrameSlot = i;
        }
        else
        {
            BufferState[BufferIndex].ReferenceFrameSlot = INVALID_INDEX;
            OutstandingSlotAllocationRequest        = BufferIndex;
        }
    }

    return CodecNoError;
}

// /////////////////////////////////////////////////////////////////////////
//
//      The intermediate process, taking output from the pre-processor
//      and feeding it to the lower level of the codec.
//

void *Codec_MmeVideoH264_IntermediateProcess(void *Parameter)
{
    Codec_MmeVideoH264_c    *Codec = (Codec_MmeVideoH264_c *)Parameter;
    Codec->IntermediateProcess();
    OS_TerminateThread();
    return NULL;
}

CodecStatus_t   Codec_MmeVideoH264_c::ReleaseDecodeContext(CodecBaseDecodeContext_t *Context)
{
    Buffer_t            AttachedCodedDataBuffer;
    Buffer_t            PPSBuffer, SPSBuffer;
    Buffer_t            StreamBuffer, FrameBuffer;
    BufferStatus_t        BufStatus;
    BufferType_t        SequenceParameterSetType;
    BufferType_t        PictureParameterSetType;
    BufferType_t        StreamParametersType;
    BufferType_t        FrameParametersType;
    Context->DecodeContextBuffer->ObtainAttachedBufferReference(CodedFrameBufferType, &AttachedCodedDataBuffer);
    if (AttachedCodedDataBuffer != NULL)
    {
        BufStatus = BufferManager->FindBufferDataType("H264SequenceParameterSet", &SequenceParameterSetType);
        if (BufStatus == BufferNoError)
        {
            AttachedCodedDataBuffer->ObtainAttachedBufferReference(SequenceParameterSetType, &SPSBuffer);
            if (SPSBuffer != NULL)
            {
                AttachedCodedDataBuffer->DetachBuffer(SPSBuffer);
            }
        }
        BufStatus = BufferManager->FindBufferDataType("H264PictureParameterSet", &PictureParameterSetType);
        if (BufStatus == BufferNoError)
        {
            AttachedCodedDataBuffer->ObtainAttachedBufferReference(PictureParameterSetType, &PPSBuffer);
            if (PPSBuffer != NULL)
            {
                AttachedCodedDataBuffer->DetachBuffer(PPSBuffer);
            }
        }
        BufStatus = BufferManager->FindBufferDataType("H264StreamParameters", &StreamParametersType);
        if (BufStatus == BufferNoError)
        {
            AttachedCodedDataBuffer->ObtainAttachedBufferReference(StreamParametersType, &StreamBuffer);
            if (StreamBuffer != NULL)
            {
                AttachedCodedDataBuffer->DetachBuffer(StreamBuffer);
            }
        }
        BufStatus = BufferManager->FindBufferDataType("H264FrameParameters", &FrameParametersType);
        if (BufStatus == BufferNoError)
        {
            AttachedCodedDataBuffer->ObtainAttachedBufferReference(FrameParametersType, &FrameBuffer);
            if (FrameBuffer != NULL)
            {
                AttachedCodedDataBuffer->DetachBuffer(FrameBuffer);
            }
        }
    }
    return Codec_MmeVideo_c::ReleaseDecodeContext(Context);
}

// ----------------------

void Codec_MmeVideoH264_c::UnuseCodedBuffer(Buffer_t CodedBuffer, H264PpFrame_t *PPentry)
{
    (void)PPentry; // warning removal

    CodedBuffer->SetUsedDataSize(0);

    BufferStatus_t Status = CodedBuffer->ShrinkBuffer(0);
    if (Status != BufferNoError)
    {
        SE_INFO(group_decoder_video, "Failed to shrink buffer\n");
    }
}

void Codec_MmeVideoH264_c::IntermediateProcess()
{
    PlayerStatus_t Status;
    Buffer_t       PreProcessorBuffer;
    bool           Terminating = false;
    //TODO: Is this bool still relevant ?
    bool PromoteNextStreamParametersToNew = false;

    ppFrame_t<H264PpFrame_t> *ppFrame = new ppFrame_t<H264PpFrame_t>();
    if (!ppFrame)
    {
        SE_ERROR("Failed to allocate ppFrame\n");
        return;
    }

    SE_INFO(group_decoder_video, "Starting Intermediate Process Thread\n");

    // Main loop
    while (!Terminating)
    {
        // Blocking call to get next ppFrame
        if (0 > mPpFramesRing->Extract(ppFrame))
        {
            continue;
        }

        // If SE is in low power state, process should be kept inactive,
        // as we are not allowed to issue any MME commands
        if (Stream && Stream->IsLowPowerState())
        {
            ppFrame->Action = ppActionNull;
        }

        // Process activity - note aborted activity differs in consequences for each action.
        switch (ppFrame->Action)
        {
        case ppActionTermination:
            Terminating = true;
            continue;

        case ppActionPassOnPreProcessedFrame:  //TODO rm
        case ppActionNull:
            break;

        case ppActionEnterDiscardMode:
            mDiscardMode = true;
            break;

        case ppActionCallOutputPartialDecodeBuffers:
            if (Stream)
            {
                Codec_MmeVideo_c::OutputPartialDecodeBuffers();
            }
            break;

        case ppActionCallDiscardQueuedDecodes:
            mDiscardMode = false;
            Codec_MmeVideo_c::DiscardQueuedDecodes();
            break;

        case ppActionCallReleaseReferenceFrame:
            H264ReleaseReferenceFrame(ppFrame->DecodeFrameIndex);
            break;

        case ppActionPassOnFrame:
            if (PromoteNextStreamParametersToNew && (ppFrame->data.ParsedFrameParameters->StreamParameterStructure != NULL))
            {
                ppFrame->data.ParsedFrameParameters->NewStreamParameters = true;
                PromoteNextStreamParametersToNew                        = false;
            }

            // By default release buffers
            Status = CodecNoError;

            if (!mDiscardMode || (ppFrame->DecodeFrameIndex == INVALID_INDEX))
            {
                // Now mimic the input procedure as done in the player process (send command to decode)
                Status = Codec_MmeVideo_c::Input(ppFrame->data.CodedBuffer);
            }
            if ((Status != CodecNoError) || mDiscardMode)
            {
                if (ppFrame->data.ParsedFrameParameters->NewStreamParameters)
                {
                    PromoteNextStreamParametersToNew  = true;
                }

                if (Stream && ppFrame->data.ParsedFrameParameters->FirstParsedParametersForOutputFrame)
                {
                    Stream->ParseToDecodeEdge->RecordNonDecodedFrame(ppFrame->data.CodedBuffer, ppFrame->data.ParsedFrameParameters);
                    Codec_MmeVideo_c::OutputPartialDecodeBuffers();
                }

                // If the pre processor buffer is still attached top the coded buffer, make sure it gets free
                if (ppFrame->DecodeFrameIndex != INVALID_INDEX)
                {
                    ppFrame->data.CodedBuffer->ObtainAttachedBufferReference(mPreProcessorBufferType, &PreProcessorBuffer);
                    if (PreProcessorBuffer != NULL)
                    {
                        ppFrame->data.CodedBuffer->DetachBuffer(PreProcessorBuffer);
                    }
                }

                if (Status == DecodeBufferManagerFailedToAllocateComponents)
                {
                    SE_ERROR("Stream 0x%p Codec input failed for dbm components allocation - marking stream unplayable\n", Stream);
                    // raise the event to signal stream unplayable
                    Stream->MarkUnPlayable(STM_SE_PLAY_STREAM_MSG_REASON_CODE_INSUFFICIENT_MEMORY, true);
                }
            }

            break;
        }

        // Release this entry
        if (ppFrame->data.CodedBuffer != NULL)
        {
            ppFrame->data.CodedBuffer->DecrementReferenceCount(IdentifierH264Intermediate);
        }
    }

    // Clean up the ring
    while (!mPpFramesRing->IsEmpty())
    {
        mPpFramesRing->Extract(ppFrame);
        if (ppFrame->data.CodedBuffer != NULL)
        {
            ppFrame->data.CodedBuffer->DecrementReferenceCount(IdentifierH264Intermediate);
        }
    }

    delete ppFrame;
    SE_INFO(group_decoder_video, "Exiting Intermediate Process Thread\n");
    // We are not accessing any more member variables so it is safe to destroy this thread now.
    OS_SemaphoreSignal(&mIntThreadStopped);
}

//{{{  CheckCodecReturnParameters
// Convert the return code into human readable form.
static const char *LookupError(unsigned int Error)
{
#define E(e) case e: return #e

    switch (Error)
    {
        E(H264_DECODER_ERROR_MB_OVERFLOW);
        E(H264_DECODER_ERROR_RECOVERED);
        E(H264_DECODER_ERROR_NOT_RECOVERED);
        E(H264_DECODER_ERROR_TASK_TIMEOUT);

    default: return "H264_DECODER_UNKNOWN_ERROR";
    }

#undef E
}

CodecStatus_t   Codec_MmeVideoH264_c::CheckCodecReturnParameters(CodecBaseDecodeContext_t *Context)
{
    unsigned int                 i, j;
    unsigned int                 ErroneousMacroBlocks;
    unsigned int                 TotalMacroBlocks;
    Buffer_t                     DecodeBuffer;
    DecodeBufferComponentType_t  DecodeComponent;
    H264CodecDecodeContext_t    *H264Context      = (H264CodecDecodeContext_t *)Context;
    MME_Command_t               *MMECommand       = (MME_Command_t *)(&Context->MMECommand);
    MME_CommandStatus_t         *CmdStatus        = (MME_CommandStatus_t *)(&MMECommand->CmdStatus);
    H264_CommandStatus_fmw_t    *AdditionalInfo_p = (H264_CommandStatus_fmw_t *)CmdStatus->AdditionalInfo_p;

    if (AdditionalInfo_p != NULL)
    {
        FillCEHRegisters(Context, H264Context->DecodeStatus.CEHRegisters);

        if (AdditionalInfo_p->ErrorCode == H264_DECODER_NO_ERROR)
        {
            //
            // Get the decode time from firmware for each frame (for field, decode time is doubled)
            //
            PictureDecodeTime = AdditionalInfo_p->DecodeTimeInMicros;
            IsFieldPicture = (H264Context->DecodeParameters.HostData.DescriptorType != H264_PIC_DESCRIPTOR_FRAME) &&
                             (H264Context->DecodeParameters.HostData.DescriptorType != H264_PIC_DESCRIPTOR_AFRAME);
            Codec_MmeVideo_c::FillDecodeTimeStatistics();
        }
        else
        {
            SE_INFO(group_decoder_video, "%s  %x\n", LookupError(AdditionalInfo_p->ErrorCode), AdditionalInfo_p->ErrorCode);
            //
            // Calculate decode quality from error status fields
            //
            OS_LockMutex(&Lock);
            DecodeBuffer        = BufferState[Context->BufferIndex].Buffer;
            DecodeComponent     = RasterOutput ? VideoDecodeCopy : PrimaryManifestationComponent;
            TotalMacroBlocks        = (Stream->GetDecodeBufferManager()->ComponentDimension(DecodeBuffer, DecodeComponent, 0) *
                                       Stream->GetDecodeBufferManager()->ComponentDimension(DecodeBuffer, DecodeComponent, 1)) / 256;
            OS_UnLockMutex(&Lock);

            if ((H264Context->DecodeParameters.HostData.DescriptorType != H264_PIC_DESCRIPTOR_FRAME) &&
                (H264Context->DecodeParameters.HostData.DescriptorType != H264_PIC_DESCRIPTOR_AFRAME))
            {
                TotalMacroBlocks  /= 2;
            }

            ErroneousMacroBlocks    = 0;

            for (i = 0; i < H264_STATUS_PARTITION; i++)
                for (j = 0; j < H264_STATUS_PARTITION; j++)
                {
                    ErroneousMacroBlocks  += H264Context->DecodeStatus.Status[i][j];
                }

            Context->DecodeQuality  = (ErroneousMacroBlocks < TotalMacroBlocks) ?
                                      Rational_t((TotalMacroBlocks - ErroneousMacroBlocks), TotalMacroBlocks) : 0;
#if 0
            SE_VERBOSE(group_decoder_video, "AZAAZA - %4d %4d - %d.%09d\n", TotalMacroBlocks, ErroneousMacroBlocks,
                       Context->DecodeQuality.IntegerPart(),  Context->DecodeQuality.RemainderDecimal(9));

            for (unsigned int i = 0; i < H264_STATUS_PARTITION; i++)
                SE_VERBOSE(group_decoder_video, "  %02x %02x %02x %02x %02x %02x\n",
                           H264Context->DecodeStatus.Status[0][i], H264Context->DecodeStatus.Status[1][i],
                           H264Context->DecodeStatus.Status[2][i], H264Context->DecodeStatus.Status[3][i],
                           H264Context->DecodeStatus.Status[4][i], H264Context->DecodeStatus.Status[5][i]);

#endif

//

            switch (AdditionalInfo_p->ErrorCode)
            {
            case H264_DECODER_ERROR_MB_OVERFLOW:
                Stream->Statistics().FrameDecodeMBOverflowError++;
                break;

            case H264_DECODER_ERROR_RECOVERED:
                Stream->Statistics().FrameDecodeRecoveredError++;
                break;

            case H264_DECODER_ERROR_NOT_RECOVERED:
                Stream->Statistics().FrameDecodeNotRecoveredError++;
                break;

            case H264_DECODER_ERROR_TASK_TIMEOUT:
                Stream->Statistics().FrameDecodeErrorTaskTimeOutError++;
                break;

            default:
                Stream->Statistics().FrameDecodeError++;
                break;
            }
        }
    }

    return CodecNoError;
}


// /////////////////////////////////////////////////////////////////////////
//
//      Function to translate the reference frame indexes
//
//      This function must be mutex-locked by caller
//
CodecStatus_t Codec_MmeVideoH264_c::TranslateReferenceFrameLists(bool IncrementUseCountForReferenceFrame)
{
    CodecStatus_t Status = Codec_MmeBase_c::TranslateReferenceFrameLists(IncrementUseCountForReferenceFrame);

    // If the function TranslateSetOfReferenceFrameLists returns No error, we still need to check, whether
    // the required number of frames/fields for reference is sufficient as per slice header information or not
    // when PolicyAllowReferenceFrameSubstitution is disabled. If it is not sufficient we return an error
    // and drop the picture from decoding.
    if ((Status == CodecNoError) && (Player->PolicyValue(Playback, Stream, PolicyAllowReferenceFrameSubstitution) == PolicyValueDisapply))
    {
        H264FrameParameters_t  *Parsed = (H264FrameParameters_t *)ParsedFrameParameters->FrameParameterStructure;

        unsigned int num_ref_idx_l0_active_minus1            = Parsed->SliceHeader.PictureParameterSet->num_ref_idx_l0_active_minus1;
        unsigned int num_ref_idx_l1_active_minus1            = Parsed->SliceHeader.PictureParameterSet->num_ref_idx_l1_active_minus1;

        if (Parsed->SliceHeader.num_ref_idx_active_override_flag)
        {
            num_ref_idx_l0_active_minus1            = Parsed->SliceHeader.num_ref_idx_l0_active_minus1;
            num_ref_idx_l1_active_minus1            = Parsed->SliceHeader.num_ref_idx_l1_active_minus1;
        }

        if (SLICE_TYPE_IS(Parsed->SliceHeader.slice_type, H264_SLICE_TYPE_P) &&
            (num_ref_idx_l0_active_minus1 + 1) > DecodeContext->ReferenceFrameList[0].EntryCount)
        {
            SE_INFO(group_decoder_video, "Insufficient reference frame(s) for P picture. Actual (%d) Needed (%d)\n",
                    DecodeContext->ReferenceFrameList[0].EntryCount, (num_ref_idx_l0_active_minus1 + 1));
            if (Parsed->SliceHeader.ref_pic_list_reordering.ref_pic_list_reordering_flag_l0)
            {
                if (ParsedFrameParameters->RefPicReorderSizeList0 > (num_ref_idx_l0_active_minus1 + 1))
                {
                    SE_INFO(group_decoder_video, "Discarding P slices with reorder\n");
                    return CodecError;
                }
            }
            else
            {
                SE_INFO(group_decoder_video, "Discarding P slices no reorder\n");
                return CodecError;
            }
        }
        if (SLICE_TYPE_IS(Parsed->SliceHeader.slice_type, H264_SLICE_TYPE_B) &&
            (((num_ref_idx_l0_active_minus1 + 1) > DecodeContext->ReferenceFrameList[1].EntryCount)
             || ((num_ref_idx_l1_active_minus1 + 1) > DecodeContext->ReferenceFrameList[2].EntryCount)))
        {
            SE_INFO(group_decoder_video, "Insufficient reference frame(s) for B picture.L0: Actual (%d) Needed (%d), L1: Actual (%d) Needed (%d)\n",
                    DecodeContext->ReferenceFrameList[1].EntryCount, (num_ref_idx_l0_active_minus1 + 1),
                    DecodeContext->ReferenceFrameList[2].EntryCount, (num_ref_idx_l1_active_minus1 + 1));

            if ((Parsed->SliceHeader.ref_pic_list_reordering.ref_pic_list_reordering_flag_l0) ||
                (Parsed->SliceHeader.ref_pic_list_reordering.ref_pic_list_reordering_flag_l1))
            {
                if ((ParsedFrameParameters->RefPicReorderSizeList0 > (num_ref_idx_l0_active_minus1 + 1)) ||
                    (ParsedFrameParameters->RefPicReorderSizeList1 > (num_ref_idx_l1_active_minus1 + 1)))
                {
                    SE_INFO(group_decoder_video, "Discarding B slices with reorder\n");
                    return CodecError;
                }
            }
            else
            {
                SE_INFO(group_decoder_video, "Discarding B slices without reorder\n");
                return CodecError;
            }
        }
    }

    return Status;
}

// /////////////////////////////////////////////////////////////////////////
//
//  The  video function used to determine the number of buffer to be used
//
void   Codec_MmeVideoH264_c::FillOutBufferCountRequest(DecodeBufferRequest_t   *Request)
{
    unsigned int PictureSizeInMBS        = (Request->Dimension[0] * Request->Dimension[1]) / 256;
    int max_h264_level = ARRAY_SIZE(h264_levels);

    // Marker frame only , do not need to compute remaining field
    if (PictureSizeInMBS == 0) { return; }

    //computing actual dpb based on level and resolution specified in stream
    // Find level in array of levels
    int level_idx;
    for (level_idx = 0; level_idx < max_h264_level; level_idx ++)
    {
        if (h264_levels[level_idx].level_idc == mLastLevelIdc)
        {
            break;
        }
    }

    if (level_idx == max_h264_level)
    {
        //as level_idx is equal to max_h264_level, so asuming 5.2 level
        level_idx--;
    }

    int PictureCountInDPB = h264_levels[level_idx].MaxDpbSizeInMbs / PictureSizeInMBS;
    int ActualDPBCount = min(PictureCountInDPB, MAX_DPB_BUFFER);
    SE_DEBUG(group_decoder_video, "Actual DPB count : %d\n", ActualDPBCount);
    H264SequenceParameterSetHeader_t        *SPS = ((H264FrameParameters_t *)ParsedFrameParameters->FrameParameterStructure)->SliceHeader.SequenceParameterSet;
    if (SPS->num_ref_frames > ActualDPBCount)
    {
        SE_WARNING("num_ref_frames (%d) is out of range in SPS. Allowed range is 0 to %d\n", SPS->num_ref_frames, ActualDPBCount);
        //realign to max level to compute DPB size
        level_idx = max_h264_level - 1;
        PictureCountInDPB = h264_levels[level_idx].MaxDpbSizeInMbs / PictureSizeInMBS;
        ActualDPBCount = min(PictureCountInDPB, MAX_DPB_BUFFER);
    }

    // additional buffers added to take care for current decoding and requirement to handle de-interlacing
    int buffercount = ActualDPBCount + (1 + MAX_DISPLAY_BUFFER);
    Request->ManifestationBufferCount = max(buffercount, MIN_MANIFESTATION_BUFFER_COUNT);

    //  Buffer allocation is over allocated due to display limitation
    if (ModuleParameter_Support64MbyteCrossing()) { Request->ManifestationBufferCount--; }

    buffercount = ActualDPBCount + 1;

    // For small resolution streams, and due to some fragmentation in Copy Buffer pool
    // (should not happen...), one extra buffer allocation is needed to avoid getting
    // stucked on GetDecodeBuffer, while no reference buffer can be released
    // (e.g. more than 16 consecutive decoding commands sent by FP without any release reference frames)
    if ((Request->Dimension[0] * Request->Dimension[1]) < (1280 * 720))
    {
        buffercount += 1;
    }

    Request->ReferenceBufferCount     = max(buffercount, MIN_REFERENCE_BUFFER_COUNT);

    SE_DEBUG(group_decoder_video, "%p Set Manifestation buffer count:%d reference buffer count:%d frame size %d x %d\n",
             this , Request->ManifestationBufferCount , Request->ReferenceBufferCount, Request->Dimension[0] , Request->Dimension[1]);
}
//}}}
