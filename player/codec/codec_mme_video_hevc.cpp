/************************************************************************
Copyright (C) 2003-2014 STMicroelectronics. All Rights Reserved.

This file is part of the Streaming Engine.

Streaming Engine is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License version 2 as
published by the Free Software Foundation.

Streaming Engine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with Streaming Engine; see the file COPYING.  If not, write to the Free
Software Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307,
USA.

The Streaming Engine Library may alternatively be licensed under a
proprietary license from ST.
************************************************************************/

#include "st_relayfs_se.h"
#include "player_threads.h"

#include "havana_stream.h"
#include "hevc.h"
#include "hadesio.h"
#include "codec_mme_video_hevc.h"
#include "parse_to_decode_edge.h"
#include "new.h"

#undef TRACE_TAG
#define TRACE_TAG "Codec_MmeVideoHevc_c"

#define BUFFER_HEVC_CODEC_STREAM_PARAMETER_CONTEXT             "HevcCodecStreamParameterContext"
#define BUFFER_HEVC_CODEC_STREAM_PARAMETER_CONTEXT_TYPE        {BUFFER_HEVC_CODEC_STREAM_PARAMETER_CONTEXT, BufferDataTypeBase, AllocateFromOSMemory, 32, 0, false, false, NOT_SPECIFIED}
#define MAX_PENDING_PP_TASK     18

static BufferDataDescriptor_t HevcCodecStreamParameterContextDescriptor = BUFFER_HEVC_CODEC_STREAM_PARAMETER_CONTEXT_TYPE;

typedef struct HevcCodecDecodeContext_s
{
    CodecBaseDecodeContext_t    BaseContext;
    struct hades_ioctl_process_frame frame;
#ifdef CONFIG_STM_HADES_DEBUG_TOOLS
    HevcCodecExtraCRCInfo_t     ExtraCRCInfo;
#endif
    PictureStructure_t          DecodePictureStructure; /* Picture structure of Frame/Field Queued for Decode */
    uint32_t                    DecodeIndex;
    int64_t                     PTS;
} HevcCodecDecodeContext_t;

#define BUFFER_HEVC_CODEC_DECODE_CONTEXT       "HevcCodecDecodeContext"
#define BUFFER_HEVC_CODEC_DECODE_CONTEXT_TYPE  {BUFFER_HEVC_CODEC_DECODE_CONTEXT, BufferDataTypeBase, AllocateFromOSMemory, 32, 0, true, true, sizeof(HevcCodecDecodeContext_t)}

static BufferDataDescriptor_t HevcCodecDecodeContextDescriptor = BUFFER_HEVC_CODEC_DECODE_CONTEXT_TYPE;

// HEVC Intermediate Buffers
// Allocated in heap-style memory pools
// Note: - alignment of 8 to be checked with Hades designer
//       - min allocation size of 1024 to be fine tuned to avoid fragmentation and not spoil too much space
//       - POOL_SIZE values taken from architecture document "HADES_BW&FOOTPRINT.xlsx"
//       - POOL_SIZE values are doubled to support dual hevc decode.

#define ALLOCATION_GRANULARITY          (128)

#define HEVC_SLICE_TABLE_BUFFER             "HevcSliceTblBuf"
#define HEVC_SLICE_TABLE_BUFFER_TYPE        {HEVC_SLICE_TABLE_BUFFER, BufferDataTypeBase, AllocateFromSuppliedBlock, (HADESPP_BUFFER_ALIGNMENT+1), ALLOCATION_GRANULARITY, false, false, 0}

#define HEVC_CTB_TABLE_BUFFER               "HevcCtbTblBuf"
#define HEVC_CTB_TABLE_BUFFER_TYPE          {HEVC_CTB_TABLE_BUFFER, BufferDataTypeBase, AllocateFromSuppliedBlock, (HADESPP_BUFFER_ALIGNMENT+1), ALLOCATION_GRANULARITY, false, false, 0}

#define HEVC_SLICE_HEADERS_BUFFER           "HevcSliceHdrBuf"
#define HEVC_SLICE_HEADERS_BUFFER_TYPE      {HEVC_SLICE_HEADERS_BUFFER, BufferDataTypeBase, AllocateFromSuppliedBlock, (HADESPP_BUFFER_ALIGNMENT+1), ALLOCATION_GRANULARITY, false, false, 0}

#define HEVC_CTB_COMMANDS_BUFFER            "HevcCtbCmdBuf"
#define HEVC_CTB_COMMANDS_BUFFER_TYPE       {HEVC_CTB_COMMANDS_BUFFER, BufferDataTypeBase, AllocateFromSuppliedBlock, (HADESPP_BUFFER_ALIGNMENT+1), ALLOCATION_GRANULARITY, false, false, 0}

#define HEVC_CTB_RESIDUALS_BUFFER           "HevcCtbResBuf"
#define HEVC_CTB_RESIDUALS_BUFFER_TYPE      {HEVC_CTB_RESIDUALS_BUFFER, BufferDataTypeBase, AllocateFromSuppliedBlock, (HADESPP_BUFFER_ALIGNMENT+1), ALLOCATION_GRANULARITY, false, false, 0}
static BufferDataDescriptor_t HevcPreprocessorBufferDescriptor[] =
{
    HEVC_SLICE_TABLE_BUFFER_TYPE,
    HEVC_CTB_TABLE_BUFFER_TYPE,
    HEVC_SLICE_HEADERS_BUFFER_TYPE,
    HEVC_CTB_COMMANDS_BUFFER_TYPE,
    HEVC_CTB_RESIDUALS_BUFFER_TYPE
};

#define HEVC_FORMAT_PIC_BASE_ADDR                                   (0x0)

/*!
* \brief
* Register : SCALING_LIST_PRED
*/

#define HEVC_FORMAT_PIC_SCALING_LIST_PRED_SIZE                      (32)
#define HEVC_FORMAT_PIC_SCALING_LIST_PRED_OFFSET                    (HEVC_FORMAT_PIC_BASE_ADDR + 0x0)
#define HEVC_FORMAT_PIC_SCALING_LIST_PRED_RESET_VALUE               (0x00000000)
#define HEVC_FORMAT_PIC_SCALING_LIST_PRED_BITFIELD_MASK             (0x01FF003F)
#define HEVC_FORMAT_PIC_SCALING_LIST_PRED_RWMASK                    (0x01FF003F)
#define HEVC_FORMAT_PIC_SCALING_LIST_PRED_ROMASK                    (0x00000000)
#define HEVC_FORMAT_PIC_SCALING_LIST_PRED_WOMASK                    (0x00000000)
#define HEVC_FORMAT_PIC_SCALING_LIST_PRED_UNUSED_MASK               (0xFE00FFC0)

/*!
* \brief
* Bit-field : scaling_list_pred_mode_flag
*/

#define HEVC_FORMAT_PIC_SCALING_LIST_PRED_scaling_list_pred_mode_flag_OFFSET (0x00000000)
#define HEVC_FORMAT_PIC_SCALING_LIST_PRED_scaling_list_pred_mode_flag_WIDTH (1)
#define HEVC_FORMAT_PIC_SCALING_LIST_PRED_scaling_list_pred_mode_flag_MASK (0x00000001)

/*!
* \brief
* Bit-field : scaling_list_pred_matrix_id_delta
*/

#define HEVC_FORMAT_PIC_SCALING_LIST_PRED_scaling_list_pred_matrix_id_delta_OFFSET (0x00000001)
#define HEVC_FORMAT_PIC_SCALING_LIST_PRED_scaling_list_pred_matrix_id_delta_WIDTH (5)
#define HEVC_FORMAT_PIC_SCALING_LIST_PRED_scaling_list_pred_matrix_id_delta_MASK (0x0000003E)

/*!
* \brief
* Bit-field : scaling_list_dc_coeff_minus8
*/

#define HEVC_FORMAT_PIC_SCALING_LIST_PRED_scaling_list_dc_coeff_minus8_OFFSET (0x00000010)
#define HEVC_FORMAT_PIC_SCALING_LIST_PRED_scaling_list_dc_coeff_minus8_WIDTH (9)
#define HEVC_FORMAT_PIC_SCALING_LIST_PRED_scaling_list_dc_coeff_minus8_MASK (0x01FF0000)

/*!
* \brief
* Register : SCALING_LIST_DELTA_COEF
*/

#define HEVC_FORMAT_PIC_SCALING_LIST_DELTA_COEF_SIZE                (32)
#define HEVC_FORMAT_PIC_SCALING_LIST_DELTA_COEF_OFFSET              (HEVC_FORMAT_PIC_BASE_ADDR + 0x4)
#define HEVC_FORMAT_PIC_SCALING_LIST_DELTA_COEF_RESET_VALUE         (0x00000000)
#define HEVC_FORMAT_PIC_SCALING_LIST_DELTA_COEF_BITFIELD_MASK       (0xFFFFFFFF)
#define HEVC_FORMAT_PIC_SCALING_LIST_DELTA_COEF_RWMASK              (0xFFFFFFFF)
#define HEVC_FORMAT_PIC_SCALING_LIST_DELTA_COEF_ROMASK              (0x00000000)
#define HEVC_FORMAT_PIC_SCALING_LIST_DELTA_COEF_WOMASK              (0x00000000)
#define HEVC_FORMAT_PIC_SCALING_LIST_DELTA_COEF_UNUSED_MASK         (0x00000000)

/*!
* \brief
* Bit-field : scaling_list_delta_coef0
*/

#define HEVC_FORMAT_PIC_SCALING_LIST_DELTA_COEF_scaling_list_delta_coef0_OFFSET (0x00000000)
#define HEVC_FORMAT_PIC_SCALING_LIST_DELTA_COEF_scaling_list_delta_coef0_WIDTH (8)
#define HEVC_FORMAT_PIC_SCALING_LIST_DELTA_COEF_scaling_list_delta_coef0_MASK (0x000000FF)

/*!
* \brief
* Bit-field : scaling_list_delta_coef1
*/

#define HEVC_FORMAT_PIC_SCALING_LIST_DELTA_COEF_scaling_list_delta_coef1_OFFSET (0x00000008)
#define HEVC_FORMAT_PIC_SCALING_LIST_DELTA_COEF_scaling_list_delta_coef1_WIDTH (8)
#define HEVC_FORMAT_PIC_SCALING_LIST_DELTA_COEF_scaling_list_delta_coef1_MASK (0x0000FF00)

/*!
* \brief
* Bit-field : scaling_list_delta_coef2
*/

#define HEVC_FORMAT_PIC_SCALING_LIST_DELTA_COEF_scaling_list_delta_coef2_OFFSET (0x00000010)
#define HEVC_FORMAT_PIC_SCALING_LIST_DELTA_COEF_scaling_list_delta_coef2_WIDTH (8)
#define HEVC_FORMAT_PIC_SCALING_LIST_DELTA_COEF_scaling_list_delta_coef2_MASK (0x00FF0000)

/*!
* \brief
* Bit-field : scaling_list_delta_coef3
*/

#define HEVC_FORMAT_PIC_SCALING_LIST_DELTA_COEF_scaling_list_delta_coef3_OFFSET (0x00000018)
#define HEVC_FORMAT_PIC_SCALING_LIST_DELTA_COEF_scaling_list_delta_coef3_WIDTH (8)
#define HEVC_FORMAT_PIC_SCALING_LIST_DELTA_COEF_scaling_list_delta_coef3_MASK (0xFF000000)

//----------
// TODO: Taken from bit_fields.h.

#define  MASK(reg,fld)          reg##_##fld##_MASK
#define  OFFSET(reg,fld)        reg##_##fld##_OFFSET
#define  WIDTH(reg,fld)         reg##_##fld##_WIDTH

// Note: these functions are defined as static in a .h file because of constraints in the APEX/TLM platform

static void
inline field_value_set
(uint32_t *reg,
 uint32_t mask,
 uint8_t  offset,
 uint32_t value)
{
    value <<= offset;
    value &= mask;
    mask = ~mask;
    mask &= *reg;
    *reg = mask | value;
}

static uint32_t
inline unsigned_field_value_get
(uint32_t *reg,
 uint32_t mask,
 uint8_t  offset)
{
    mask &= *reg;
    return mask >> offset;
}

static int32_t
inline signed_field_value_get
(uint32_t *reg,
 uint32_t mask,
 uint8_t  offset,
 uint8_t  width)
{
    int32_t value;
    value = unsigned_field_value_get(reg, mask, offset);
    mask = 1 << (width - 1);

    if (value & mask)
    {
        value |= 0xffffffff << width;
    }

    return value;
}

#define  FIELD_SET(cmd,reg,fld,val)  field_value_set(&cmd,MASK(reg,fld),OFFSET(reg,fld),val)
#define  FIELD_INIT(cmd,reg,fld,val) do { cmd=0; FIELD_SET(cmd,reg,fld,val); } while(0)
#define  FIELD_UGET(cmd,reg,fld)     unsigned_field_value_get(&cmd,MASK(reg,fld),OFFSET(reg,fld))
#define  FIELD_SGET(cmd,reg,fld)     signed_field_value_get(&cmd,MASK(reg,fld),OFFSET(reg,fld),WIDTH(reg,fld))

#define  CMD_SET(cmd,reg,fld,val)    field_value_set(cmd+reg,MASK(reg,fld),OFFSET(reg,fld),val)
#define  CMD_INIT(cmd,reg,fld,val)   do { cmd[reg]=0; CMD_SET(cmd,reg,fld,val); } while(0)
#define  CMD_UGET(cmd,reg,fld)       unsigned_field_value_get(cmd+reg,MASK(reg,fld),OFFSET(reg,fld))
#define  CMD_SGET(cmd,reg,fld)       signed_field_value_get(cmd+reg,MASK(reg,fld),OFFSET(reg,fld),WIDTH(reg,fld))


// /////////////////////////////////////////////////////////////////////////
//
// Macros for dumping C structures to STrelay
//
// #define HEVC_CODEC_DUMP_MME

#ifdef HEVC_CODEC_DUMP_MME
#define st_relayfs_write_mme_text(rid, format, args...) st_relayfs_print_se(rid, ST_RELAY_SOURCE_SE, format, ##args)

static const char *indent_first[5] = {"", "\t", "\t,\t", "\t\t,\t", "\t\t\t,\t"};
static const char *indent_next[5] = {"", "\t", "\t\t", "\t\t\t", "\t\t\t\t"};

#define LOG_ELEM(rid,cmd,elem) st_relayfs_write_mme_text(rid,"\t%d (0x%x) /* %s */", cmd->elem, cmd->elem, #elem)
#define LOG_NEXT_ELEM(rid,indent,cmd,elem)  do { \
        st_relayfs_write_mme_text(rid,"\n%s,",indent_next[indent]); \
        LOG_ELEM(rid,cmd,elem); \
} while(0)
#define LOG_FIRST_ELEM(rid,indent,cmd,elem)  do { \
        st_relayfs_write_mme_text(rid,"\n%s{",indent_first[indent]); \
        LOG_ELEM(rid,cmd,elem); \
} while(0)
#define LOG_TABLE(rid,cmd,tab,size) do { \
        st_relayfs_write_mme_text(rid,"\t{%d",cmd->tab[0]); \
        unsigned int _j; \
        for(_j=1;_j<size;_j++) \
                st_relayfs_write_mme_text(rid,", %d",cmd->tab[_j]); \
        st_relayfs_write_mme_text(rid,"} /* %s */",#tab); \
} while(0)
#define LOG_NEXT_TABLE(rid,indent,cmd,tab,size) do { \
        st_relayfs_write_mme_text(rid,"\n%s,",indent_next[indent]); \
        LOG_TABLE(rid,cmd,tab,size); \
} while(0)

#define LOG_ROTBUF(rid,indent,cmd,name) do { \
        circular_buffer_t *cmd2 = &cmd->name; \
        LOG_ELEM(rid,cmd2,base_addr); \
        LOG_NEXT_ELEM(rid,indent,cmd2,length); \
        LOG_NEXT_ELEM(rid,indent,cmd2,start_offset); \
        LOG_NEXT_ELEM(rid,indent,cmd2,end_offset); \
} while(0)

#if !defined(HEVC_HADES_CANNES25) && !defined (HEVC_HADES_CANNESWIFI)
#define LOG_PICBUF(rid,indent,cmd,name) do { \
        hevcdecpix_decoded_buffer_t *cmd2 = &cmd->name; \
        LOG_ELEM(rid,cmd2,raster2b_not_omega4); \
        LOG_NEXT_ELEM(rid,indent,cmd2,luma_offset); \
        LOG_NEXT_ELEM(rid,indent,cmd2,chroma_offset); \
        LOG_NEXT_ELEM(rid,indent,cmd2,stride); \
} while(0)
#else
#define LOG_PICBUF(rid,indent,cmd,name) do { \
        hevcdecpix_decoded_buffer_t *cmd2 = &cmd->name; \
        LOG_NEXT_ELEM(rid,indent,cmd2,luma_offset); \
        LOG_NEXT_ELEM(rid,indent,cmd2,chroma_offset); \
} while(0)
#endif

#define LOG_REFBUF(rid,indent,cmd,name) do { \
        hevcdecpix_reference_buffer_t *cmd3 = &cmd->name; \
        LOG_ELEM(rid,cmd3,enable_flag); \
        LOG_NEXT_ELEM(rid,indent,cmd3,ppb_offset); \
        st_relayfs_write_mme_text(rid,"\n%s{",indent_first[indent+1]); \
        LOG_PICBUF(rid,indent+1,cmd3,samples); \
        st_relayfs_write_mme_text(rid,"\n%s} /* pixels */",indent_first[indent+1]); \
} while(0)

#define LOG_DISPBUF(rid,indent,cmd,name) do { \
        hevcdecpix_display_buffer_t *cmd3 = &cmd->name; \
        LOG_ELEM(rid,cmd3,enable_flag); \
        LOG_NEXT_ELEM(rid,indent,cmd3,resize_flag); \
        st_relayfs_write_mme_text(rid,"\n%s{",indent_first[indent+1]); \
        LOG_PICBUF(rid,indent+1,cmd3,samples); \
        st_relayfs_write_mme_text(rid,"\n%s} /* pixels */",indent_first[indent+1]); \
} while(0)

#define LOG_PICINFO(rid,indent,cmd,name) do { \
        hevcdecpix_pic_info_t *cmd2 = &cmd->name; \
        LOG_ELEM(rid,cmd2,poc); \
        LOG_NEXT_ELEM(rid,indent,cmd2,non_existing_flag); \
        LOG_NEXT_ELEM(rid,indent,cmd2,long_term_flag); \
} while(0)

#define LOG_NEXT_STRUCT(rid,indent,TYPE,cmd,name) do { \
        st_relayfs_write_mme_text(rid,"\n%s{",indent_first[indent+1]); \
        LOG_##TYPE(rid,indent+1,cmd,name); \
        st_relayfs_write_mme_text(rid,"\n%s} /* %s */",indent_next[indent+1],#name); \
} while(0)

#define LOG_INTBUF(rid,indent,cmd,name) do { \
        intermediate_buffer_t *cmd3 = &cmd->name; \
        LOG_ELEM(rid,cmd3,slice_table_base_addr); \
        LOG_NEXT_ELEM(rid,indent,cmd3,slice_table_length); \
        LOG_NEXT_ELEM(rid,indent,cmd3,ctb_table_base_addr); \
        LOG_NEXT_ELEM(rid,indent,cmd3,ctb_table_length); \
        LOG_NEXT_ELEM(rid,indent,cmd3,slice_headers_base_addr); \
        LOG_NEXT_ELEM(rid,indent,cmd3,slice_headers_length); \
        st_relayfs_write_mme_text(rid,"\n%s{",indent_first[indent+1]); \
        LOG_ROTBUF(rid,indent+1,cmd3,ctb_commands); \
        st_relayfs_write_mme_text(rid,"\n%s} /* %s */",indent_next[indent+1],"ctb_commands"); \
        st_relayfs_write_mme_text(rid,"\n%s{",indent_first[indent+1]); \
        LOG_ROTBUF(rid,indent+1,cmd3,ctb_residuals); \
        st_relayfs_write_mme_text(rid,"\n%s} /* %s */",indent_next[indent+1],"ctb_residuals"); \
} while(0)

#define LOG_FIRST_STRUCT(rid,indent,TYPE,cmd,name) do { \
        st_relayfs_write_mme_text(rid,"\n%s{\t{",indent_first[indent]); \
        LOG_##TYPE(rid,indent+1,cmd,name); \
        st_relayfs_write_mme_text(rid,"\n%s} /* %s */",indent_next[indent+1],#name); \
} while(0)

#define LOG_STRUCT_TABLE(rid,indent,TYPE,cmd,name,size) do { \
        st_relayfs_write_mme_text(rid,"\t{"); \
        LOG_##TYPE(rid,indent+1,cmd,name[0]); \
        st_relayfs_write_mme_text(rid,"\n%s} /* %s[0] */",indent_next[indent+1],#name); \
        unsigned int _i; \
        for(_i=1;_i<size;_i++) { \
                st_relayfs_write_mme_text(rid,"\n%s,\t{",indent_next[indent]); \
                LOG_##TYPE(rid,indent+1,cmd,name[_i]); \
                st_relayfs_write_mme_text(rid,"\n%s} /* %s[%d] */",indent_next[indent+1],#name,_i); \
        } \
        st_relayfs_write_mme_text(rid,"\n%s} /* %s */",indent_next[indent],#name); \
} while(0)

#define LOG_NEXT_STRUCT_TABLE(rid,indent,TYPE,cmd,name,size) do { \
        st_relayfs_write_mme_text(rid,"\n%s{",indent_first[indent+1]); \
        LOG_STRUCT_TABLE(rid,indent+1,TYPE,cmd,name,size); \
} while(0)

#define LOG_RSZCOEFF(rid,indent,cmd,name) do { \
        hevc_resize_coeff_t *cmd2 = &cmd->name; \
        LOG_ELEM(rid,cmd2,round); \
        LOG_NEXT_TABLE(rid,indent,cmd2,coeff,HEVC_RESIZE_TAPS); \
} while(0)

#define LOG_RESIZE(rid,indent,cmd,name) do { \
  hevcdecpix_resize_param_t *cmd3 = &cmd->name; \
    st_relayfs_write_mme_text(rid,"\n%s{",indent_next[indent+1]); \
    LOG_RSZCOEFF(rid,indent+1,cmd3,v_dec_coeff); \
    st_relayfs_write_mme_text(rid,"\n%s} /* %s */",indent_next[indent+1],"v_dec_coeff"); \
    st_relayfs_write_mme_text(rid,"\n%s{",indent_first[indent+1]); \
    LOG_RSZCOEFF(rid,indent+1,cmd3,h_flt_coeff); \
    st_relayfs_write_mme_text(rid,"\n%s} /* %s */",indent_next[indent+1],"h_flt_coeff"); \
} while(0)
#endif // HEVC_CODEC_DUMP_MME


// ////////////////////////////////////////////////////////////////////////////
//
//  Level information: copied from hevclevels.c in STHM'
//

//! Maximum number of words in 16x16 (minimum CTB size for level<5.0) CTB command area
#define MAX_CTB_16x16_COMMAND_SIZE      22  // = 1 CTB + 3 SAO + 1 TU split + 1 TU cbf +  4x4 CU/PU/MVD
//! Maximum number of words in 32x32 (minimum CTB size for level>=5.0) CTB command area
#define MAX_CTB_32x32_COMMAND_SIZE      73  // = 1 CTB + 3 SAO + 1 TU split + 4 TU cbf + 16x4 CU/PU/MVD
//! Maximum number of samples required to store the debinarized residuals of a 16x16 block
#define MAX_16x16_RESIDUAL_SIZE         400 // Assuming 400 samples for a 16x16 block is enough ( > 384 samples used for a decompressed 16x16 4:2:0 block)

// /////////////////////////////////////////////////////////////////////////
#define HEVC_MIN_RES_FOR_PP_BUFFERS_DIMENSIONNING 320

// /////////////////////////////////////////////////////////////////////////
//
//      Constructor function, fills in the codec specific parameter values
//

Codec_MmeVideoHevc_c::Codec_MmeVideoHevc_c()
    : InitTransformerParam()
    , mPpFramesRing()
    , mDiscardMode(false)
    , mPreProcessorBufferType()
    , mPreProcessorBufferAllocator()
    , mPreprocessorBufferSize()
    , mPreProcessorBufferPool()
    , mPreProcessorDevice(NULL)
    , mHadesDevice(NULL)
    , mLastLevelIdc(0xdeadbeef)  // flush cache of level parameters
    , LocalReferenceFrameList()
    , mDecodeBufferManager(NULL)
    , mSavedReferenceForSubstitution()
    , mSavedReferenceForSubstitutionCanBeUsed(false)
    , mIntThreadStopped()
{
    OS_SemaphoreInitialize(&mIntThreadStopped, 0);
    // Direct use of low-level driver API (no MME framework)
    setStandAloneCodec(true);

// TODO: Check if H264 settings still apply for HEVC.
    Configuration.CodecName                             = "HEVC video";
    // Not used
    Configuration.StreamParameterContextCount           = NOT_SPECIFIED;
    Configuration.StreamParameterContextDescriptor      = &HevcCodecStreamParameterContextDescriptor;
    Configuration.DecodeContextCount                    = 8;
    Configuration.DecodeContextDescriptor               = &HevcCodecDecodeContextDescriptor;
    Configuration.MaxDecodeIndicesPerBuffer             = 2;  // For interlaced video, 2 fields can refer to same decode buffer
    Configuration.SliceDecodePermitted                  = false;
    Configuration.DecimatedDecodePermitted              = true;

// TODO: Update this later
#ifdef HEVC_HADES_CANNESWIFI
#undef HEVC_MME_TRANSFORMER_NAME
    //#define HEVC_MME_TRANSFORMER_NAME "HEVC_TRANSFORMER"
#define HEVC_MME_TRANSFORMER_NAME "HEVC_HADES0"
#endif
    Configuration.TransformName[0]                      = HEVC_MME_TRANSFORMER_NAME "0";
    Configuration.TransformName[1]                      = HEVC_MME_TRANSFORMER_NAME "1";
    Configuration.AvailableTransformers                 = 2;
//
//    Configuration.SizeOfTransformCapabilityStructure    = sizeof(hevcdecpix_transformer_capability_t);
//    Configuration.TransformCapabilityStructurePointer   = (void *)(&H264TransformCapability);
//

    //
    // Since we pre-process the data, we shrink the coded data buffers
    // before entering the generic code. as a consequence we do
    // not require the generic code to shrink buffers on our behalf,
    // and we do not want the generic code to find the coded data buffer
    // for us.
    //
    Configuration.ShrinkCodedDataBuffersAfterDecode     = false;
    Configuration.IgnoreFindCodedDataBuffer     = true;

    // TODO(pht) move FinalizeInit to a factory method
    InitializationStatus = FinalizeInit();
}

// /////////////////////////////////////////////////////////////////////////
//
//      FinalizeInit finalizes init work by doing operations that may fail
//      (and such not doable in ctor)
//
CodecStatus_t Codec_MmeVideoHevc_c::FinalizeInit()
{
    // Create the ring used to communicate with intermediate process (frame or synchro data)
    mPpFramesRing = new ppFramesRing_c<HevcPpFrame_t>(MAX_FRAME_NB_IN_PP_RING, MAX_PENDING_PP_TASK);
    if (mPpFramesRing == NULL)
    {
        SE_ERROR("Failed to create ring to hold frames in pre-processor chain\n");
        return CodecError;
    }
    if (mPpFramesRing->FinalizeInit() != 0)
    {
        SE_ERROR("Failed to init PpFramesRing\n");
        return CodecError;
    }

    // Create the intermediate process
    OS_Thread_t  Thread;
    if (OS_CreateThread(&Thread, Codec_MmeVideoHevc_IntermediateProcess, this, &player_tasks_desc[SE_TASK_VIDEO_HEVCINT]) != OS_NO_ERROR)
    {
        SE_ERROR("Failed to create intermediate process\n");
        return CodecError;
    }

    return CodecNoError;
}

// /////////////////////////////////////////////////////////////////////////
//
//      Destructor function, ensures a full halt and reset
//      are executed for all levels of the class.
//

Codec_MmeVideoHevc_c::~Codec_MmeVideoHevc_c()
{
    int ret = mPpFramesRing->InsertFakeEntry(0, ppActionTermination);
    if (ret < 0)
    {
        SE_ERROR("Failed to send ppActionTermination\n");
    }

    SE_DEBUG(group_player, "Waiting for HEVC intermediate process to stop\n");
    OS_SemaphoreWaitAuto(&mIntThreadStopped);
    SE_DEBUG(group_player, "HEVC intermediate process stopped\n");

    //
    // Make sure the pre-processor device is closed (note this may be done in halt,
    // but we check again, since you can enter reset without halt during a failed
    // start up).
    //

    if (mPreProcessorDevice != NULL)
    {
        HadesppClose(mPreProcessorDevice);
    }

    if (mHadesDevice != NULL)
    {
        OSDEV_Close(*mHadesDevice);
        OS_Free(mHadesDevice);
    }

    Halt();

    // Delete the frames in pre-processor chain ring
    delete mPpFramesRing;

    OS_SemaphoreTerminate(&mIntThreadStopped);
}

// /////////////////////////////////////////////////////////////////////////
//
//      Halt function
//

CodecStatus_t   Codec_MmeVideoHevc_c::Halt()
{
    // Release PP buffer memory while ppFrameRing may still not be empty
    DeallocatePreProcBufferPool();

    return Codec_MmeVideo_c::Halt();
}

// /////////////////////////////////////////////////////////////////////////
//
//      Connect output port, we take this opportunity to
//      create the buffer pools for use in the pre-processor, and the
//      macroblock structure buffers
//

CodecStatus_t Codec_MmeVideoHevc_c::Connect(Port_c *Port)
{
    if (Stream && Stream->GetDecodeBufferManager())
    {
        // Unless we had a positive error we always call handle capabilities,
        // so that codecs can initialize their decode buffer structures.
        CodecStatus_t Status = HandleCapabilities();
        if (Status != CodecNoError)
        {
            SE_ERROR("HandleCapabilities failed\n");
            return Status;
        }
    }
    //
    // Let the ancestor classes create the ring for decoded frames
    //
    CodecStatus_t Status = Codec_MmeVideo_c::Connect(Port);
    if (Status != CodecNoError)
    {
        SetComponentState(ComponentInError);
        return Status;
    }

    // Create pre-processor intermediate buffer types if they don't already exist
    for (int i = 0; i < HEVC_NUM_INTERMEDIATE_POOLS; i ++)
    {
        Status = BufferManager->FindBufferDataType(
                     HevcPreprocessorBufferDescriptor[i].TypeName,
                     &mPreProcessorBufferType[i]);

        if (Status != BufferNoError)
        {
            Status = BufferManager->CreateBufferDataType(
                         &HevcPreprocessorBufferDescriptor[i],
                         &mPreProcessorBufferType[i]);

            if (Status != BufferNoError)
            {
                SE_ERROR("Failed to create the %s buffer type (%08x)\n",
                         HevcPreprocessorBufferDescriptor[i].TypeName, Status);
                SetComponentState(ComponentInError);
                return CodecError;
            }
        }
    }

    mHadesDevice = (OSDEV_DeviceIdentifier_t *) OS_Malloc(sizeof(OSDEV_DeviceIdentifier_t));
    if (mHadesDevice == NULL)
    {
        SE_ERROR("Failed to allocate hades device\n");
        return CodecError;
    }
    memset(mHadesDevice, 0, sizeof(OSDEV_DeviceIdentifier_t));

    OSDEV_Status_t status = OSDEV_Open(HADES_DEVICE, mHadesDevice);
    if (status != OSDEV_NoError)
    {
        SE_ERROR("Failed to open hades device\n");
        OS_Free(mHadesDevice);
        mHadesDevice = NULL;
        return CodecError;
    }
    //
    // Open a new instance of the preprocessor device driver
    //
    hadespp_status_t PPStatus = HadesppOpen(&mPreProcessorDevice);
    if (PPStatus != hadespp_ok)
    {
        SE_ERROR("Failed to open the pre-processor device (%d)\n", PPStatus);
        SetComponentState(ComponentInError);
        return CodecError;
    }

    return CodecNoError;
}

// /////////////////////////////////////////////////////////////////////////
//
//      Output partial decode buffers - not entirely sure what to do here,
//      it may well be necessary to fire off the pre-processing of any
//      accumulated slices.
//

CodecStatus_t Codec_MmeVideoHevc_c::OutputPartialDecodeBuffers()
{
    int ret = mPpFramesRing->InsertFakeEntry(0, ppActionCallOutputPartialDecodeBuffers);
    if (ret < 0)
    {
        SE_ERROR("Failed to OutputPartialDecodeBuffers\n");
        return CodecError;
    }
    return CodecNoError;
}

// /////////////////////////////////////////////////////////////////////////
//
//      Discard queued decodes, this will include any between here
//      and the preprocessor.
//

CodecStatus_t Codec_MmeVideoHevc_c::DiscardQueuedDecodes()
{
    int ret = mPpFramesRing->InsertFakeEntryHead(0, ppActionEnterDiscardMode);
    if (ret < 0)
    {
        SE_ERROR("Failed to insert DiscardQueuedDecodes cmd\n");
        return CodecError;
    }
    ret = mPpFramesRing->InsertFakeEntry(0, ppActionCallDiscardQueuedDecodes);
    if (ret < 0)
    {
        SE_ERROR("Failed to DiscardQueuedDecodes\n");
        return CodecError;
    }
    return CodecNoError;
}

// /////////////////////////////////////////////////////////////////////////
//
//      Check whether the decimation value provided is supported by the codec
//
//

bool   Codec_MmeVideoHevc_c::IsDecimationValueSupported(int DecimationPolicy)
{
    switch (DecimationPolicy)
    {
    //Below decimations supported for this codec
    case PolicyValueDecimateDecoderOutputHalf:
    case PolicyValueDecimateDecoderOutputQuarter:
    case PolicyValueDecimateDecoderOutputH2V4:
    case PolicyValueDecimateDecoderOutputH2V8:
    case PolicyValueDecimateDecoderOutputH4V4:
    case PolicyValueDecimateDecoderOutputH4V8:
    case PolicyValueDecimateDecoderOutputH8V2:
    case PolicyValueDecimateDecoderOutputH8V4:
    case PolicyValueDecimateDecoderOutputH8V8:
        return true;
    default:
        SE_WARNING_ONCE("Decimation value not supported\n");
        return false;
    }
}


// /////////////////////////////////////////////////////////////////////////
//
//      Release reference frame, this must be capable of releasing a
//      reference frame that has not yet exited the pre-processor.
//

CodecStatus_t   Codec_MmeVideoHevc_c::ReleaseReferenceFrame(unsigned int ReferenceFrameDecodeIndex)
{
    int ret = mPpFramesRing->InsertFakeEntry(ReferenceFrameDecodeIndex,
                                             ppActionCallReleaseReferenceFrame);
    if (ret < 0)
    {
        SE_ERROR("Failed to ReleaseReferenceFrame\n");
        return CodecError;
    }
    return CodecNoError;
}

// /////////////////////////////////////////////////////////////////////////
//
//      Check reference frame list needs to account for those currently
//      in the pre-processor chain.
//

CodecStatus_t   Codec_MmeVideoHevc_c::CheckReferenceFrameList(
    unsigned int              NumberOfReferenceFrameLists,
    ReferenceFrameList_t      ReferenceFrameList[])
{
    // Check we can cope
    if (NumberOfReferenceFrameLists > HEVC_NUM_REF_FRAME_LISTS)
    {
        SE_ERROR("HEVC has only %d reference frame lists, requested to check %d\n",
                 HEVC_NUM_REF_FRAME_LISTS, NumberOfReferenceFrameLists);
        return CodecUnknownFrame;
    }

    // Construct local lists consisting of those elements we do not know about
    bool refFound;
    for (int i = 0; i < NumberOfReferenceFrameLists; i++)
    {
        LocalReferenceFrameList[i].EntryCount   = 0;

        for (int j = 0; j < ReferenceFrameList[i].EntryCount; j++)
        {
            refFound = mPpFramesRing->CheckRefListDecodeIndex(ReferenceFrameList[i].EntryIndicies[j]);
            if (!refFound)
            {
                LocalReferenceFrameList[i].EntryIndicies[LocalReferenceFrameList[i].EntryCount++] = ReferenceFrameList[i].EntryIndicies[j];
            }
        }
    }

    return Codec_MmeVideo_c::CheckReferenceFrameList(NumberOfReferenceFrameLists, LocalReferenceFrameList);
}

// /////////////////////////////////////////////////////////////////////////
//
//      UpdatePreprocessorBufferSize
//      computes the maximum size of each of the 5 intermediate buffer
//      in function of the level of the stream (found in SPS)
//      Returns < 0 if error, > 0 if PreprocessorBufferSize changed, 0 otherwise
//

int Codec_MmeVideoHevc_c::UpdatePreprocessorBufferSize(HevcFrameParameters_t *FrameParameters)
{
    int ret = 1;
    int level_index;
    uint32_t level = 0;
    uint32_t bit_per_sample = 8;
    uint32_t pic_width_in_luma_samples, pic_height_in_luma_samples;

    if (FrameParameters == NULL)
    {
        SE_WARNING("FrameParameters NULL\n");
        ret = -1;
        goto bail;
    }

    level = FrameParameters->SliceHeader.SequenceParameterSet->level_idc;
    pic_width_in_luma_samples  = FrameParameters->SliceHeader.SequenceParameterSet->pic_width_in_luma_samples;
    pic_height_in_luma_samples = FrameParameters->SliceHeader.SequenceParameterSet->pic_height_in_luma_samples;
    bit_per_sample = max(FrameParameters->SliceHeader.SequenceParameterSet->bit_depth_luma_minus8 + 8,
                         FrameParameters->SliceHeader.SequenceParameterSet->bit_depth_chroma_minus8 + 8);
    if (bit_per_sample > 10)
    {
        SE_ERROR("Bit_depth forced to 10 (SPS.bit_depth_luma_minus8:%d bit_depth_chroma_minus8:%d)\n",
                 FrameParameters->SliceHeader.SequenceParameterSet->bit_depth_luma_minus8,
                 FrameParameters->SliceHeader.SequenceParameterSet->bit_depth_chroma_minus8);
        bit_per_sample = 10;
    }

    if (level == mLastLevelIdc)
    {
        SE_DEBUG(group_decoder_video, "Level (%d) unchanged\n", level);
        ret = 0;
        goto bail;
    }

    mLastLevelIdc = level;

    // Find level in array of levels
    for (level_index = 0; level_index < HEVC_NUM_LEVELS; level_index ++)
    {
        if (hevc_levels[level_index].level_idc == level)
        {
            break;
        }
    }

    if (level_index < HEVC_NUM_LEVELS)
    {
        if (PlayerNoError != GetIntermediateBufferSize(level_index,
                                                       pic_width_in_luma_samples, pic_height_in_luma_samples,
                                                       bit_per_sample, mPreprocessorBufferSize))
        {
            SE_ERROR("Unable to get intermediate buffer size\n");
            ret = -1;
            goto bail;
        }
    }

bail:
    SE_DEBUG(group_decoder_video, "size of IB: level_idc %d, %d %d %d %d %d\n",
             level,
             mPreprocessorBufferSize[HEVC_SLICE_TABLE],
             mPreprocessorBufferSize[HEVC_CTB_TABLE],
             mPreprocessorBufferSize[HEVC_SLICE_HEADERS],
             mPreprocessorBufferSize[HEVC_CTB_COMMANDS],
             mPreprocessorBufferSize[HEVC_CTB_RESIDUALS]);
    return ret;
}

CodecStatus_t Codec_MmeVideoHevc_c::GetIntermediateBufferSize(int level_index,
                                                              uint32_t pic_width_in_luma_samples,
                                                              uint32_t pic_height_in_luma_samples,
                                                              uint32_t bits_per_sample,
                                                              unsigned int *IBSize)
{
    if (pic_width_in_luma_samples > hevc_levels[level_index].MaxDim)
    {
        SE_ERROR("picture width %u in stream level_idc %u should be less than %u\n",
                 pic_width_in_luma_samples, hevc_levels[level_index].level_idc, hevc_levels[level_index].MaxDim);
        return PlayerError;  /* ERC : Not Recoverable */
    }

    if (pic_height_in_luma_samples > hevc_levels[level_index].MaxDim)
    {
        SE_ERROR("picture height %u in stream level_idc %u should be less than %u\n",
                 pic_width_in_luma_samples, hevc_levels[level_index].level_idc, hevc_levels[level_index].MaxDim);
        return PlayerError; /* ERC : Not Recoverable */
    }

    // For small resolutions, residuals and ctb_table exceeds theoritical computation
    // Define minimal resolutions for pp buffer size computations
    pic_width_in_luma_samples = max(pic_width_in_luma_samples, HEVC_MIN_RES_FOR_PP_BUFFERS_DIMENSIONNING);
    pic_height_in_luma_samples = max(pic_height_in_luma_samples, HEVC_MIN_RES_FOR_PP_BUFFERS_DIMENSIONNING);

    uint32_t log2_min_ctb_size = hevc_levels[level_index].Log2MinCtbSize;
    // min_cb_luma_size depends on the level (16 below level 5.0 and 32 above)
    uint32_t min_cb_luma_size = (1 << log2_min_ctb_size);
    uint32_t pic_width_in_min_cbs_luma = (pic_width_in_luma_samples + min_cb_luma_size - 1) >> log2_min_ctb_size;
    uint32_t pic_height_in_min_cbs_luma = (pic_height_in_luma_samples + min_cb_luma_size - 1) >> log2_min_ctb_size;
    uint32_t pic_size_in_min_cbs = pic_width_in_min_cbs_luma * pic_height_in_min_cbs_luma;
    uint32_t ctb_cmd_size = ((log2_min_ctb_size == 4) ? (2 * MAX_CTB_16x16_COMMAND_SIZE) : MAX_CTB_32x32_COMMAND_SIZE);
    ctb_cmd_size *= pic_size_in_min_cbs;

    uint32_t tile_columns = (pic_width_in_luma_samples + 255) >> 8;
    uint32_t ctb_table = pic_height_in_min_cbs_luma * tile_columns;

    // Oversize resolution for PP buffer size computation (avoid CTB_RES buffer overflow on some streams)
#define RESOLUTION_EXTENSION_FOR_CTB_RESIDUAL_SIZE 760
    uint32_t max_compressed_pic_size = ((pic_width_in_luma_samples + RESOLUTION_EXTENSION_FOR_CTB_RESIDUAL_SIZE) *
                                        (pic_height_in_luma_samples + RESOLUTION_EXTENSION_FOR_CTB_RESIDUAL_SIZE) * 2 * bits_per_sample) /
                                       hevc_levels[level_index].MinCR;
    // raw_min_cu_bits =  (min_cb_luma_size * min_cb_luma_size * (bit_depth_luma + bit_depth_chroma/2))
    // Assuming bit_depth_luma == bit_depth_chroma
    uint32_t raw_min_cu_bits = (min_cb_luma_size * min_cb_luma_size * 3 * bits_per_sample) / 2;
    // Max compressed picture bin size -> this is the size after arithmetic decompression:
    // 1 bit (in coded bitstream) can code for 1 or more bin. It is the size of bitstream
    // after CABAC/CAVLC decoding (i.e. PP stage).
    uint32_t max_compressed_pic_bin_size = (4 * max_compressed_pic_size) / 3 + (raw_min_cu_bits * pic_size_in_min_cbs) / 32;

    IBSize[HEVC_SLICE_TABLE]   = hevc_levels[level_index].MaxSlicesPerPicture * SLICE_TABLE_ENTRY_SIZE * sizeof(uint32_t);
    IBSize[HEVC_SLICE_HEADERS] = hevc_levels[level_index].MaxSlicesPerPicture * MAX_SLICE_HEADER_SIZE * sizeof(uint32_t);
    IBSize[HEVC_CTB_TABLE]     = 2 * 2 * ctb_table * CTB_TABLE_ENTRY_SIZE * sizeof(uint32_t);
    IBSize[HEVC_CTB_COMMANDS]  = ctb_cmd_size * sizeof(uint32_t);
    IBSize[HEVC_CTB_RESIDUALS] = max_compressed_pic_bin_size / 8;

    // PP writes an extra 0-word at end of all tables except CTB table
    IBSize[HEVC_SLICE_TABLE] += 4;
    IBSize[HEVC_SLICE_HEADERS] += 4;
    IBSize[HEVC_CTB_COMMANDS] += 4;
    IBSize[HEVC_CTB_RESIDUALS] += 4;

    for (int i = 0; i < HEVC_NUM_INTERMEDIATE_POOLS; i++)
    {
        IBSize[i] = BUF_ALIGN_UP(IBSize[i], HADESPP_BUFFER_ALIGNMENT);
    }

    SE_VERBOSE(group_decoder_video, "New size of IB: level_idc %d, %d %d %d %d %d\n",
               hevc_levels[level_index].level_idc, IBSize[HEVC_SLICE_TABLE],
               IBSize[HEVC_CTB_TABLE] , IBSize[HEVC_SLICE_HEADERS],
               IBSize[HEVC_CTB_COMMANDS], IBSize[HEVC_CTB_RESIDUALS]);
    return PlayerNoError;
}

void Codec_MmeVideoHevc_c::DeallocatePreProcBufferPool()
{
    // Free buffer pools
    for (int i = 0; i < HEVC_NUM_INTERMEDIATE_POOLS; i ++)
    {
        if (mPreProcessorBufferPool[i] != NULL)
        {
            BufferManager->DestroyPool(mPreProcessorBufferPool[i]);
            mPreProcessorBufferPool[i] = NULL;
            mDecodeBufferManager->RegisterPreprocBufferPool(NULL, i);
        }
    }
    AllocatorClose(&mPreProcessorBufferAllocator);
}
/////////////////////////////////////////////////////////////////////////////////////////////////////
// Create or re-allocate the pre-processor buffer pools
//
CodecStatus_t Codec_MmeVideoHevc_c::AllocatePreProcBufferPool(HevcFrameParameters_t *FrameParameters)
{
    uint32_t pic_width_in_luma_samples = FrameParameters->SliceHeader.SequenceParameterSet->pic_width_in_luma_samples;
    uint32_t pic_height_in_luma_samples = FrameParameters->SliceHeader.SequenceParameterSet->pic_height_in_luma_samples;
    unsigned int PPPoolSize[HEVC_NUM_INTERMEDIATE_POOLS];
    unsigned int TotalPPSize = 0;
    BufferStatus_t Status;
    int i = 0;
    // MIN_NB_OF_PP_BUFFERS_PER_POOL represents the minimal theoritical number of buffers in the pool
    // HEVC_MIN_NB_OF_PP_BUFFERS_PER_POOL_LOWMEM is the same for low mem platforms:
    //   allows to limit the nb of pp buffers per pool for flush performance purpose
    // for HEVC_SLICE_TABLE, HEVC_SLICE_HEADERS, HEVC_CTB_TABLE :
    // Poolsize = MIN_NB_OF_PP_BUFFERS_PER_POOL x computed buffer size
    // for HEVC_CTB_COMMANDS and HEVC_CTB_RESIDUALS :
    // Poolsize = (1 + K x (MIN_NB_OF_PP_BUFFERS_PER_POOL-1)) x computed worst-case buffer size
    // K is empirically set based: 0.2 for residuals, and 0.5 for ctb commands
#define HEVC_MIN_NB_OF_PP_BUFFERS_PER_POOL 9
#define HEVC_MIN_NB_OF_PP_BUFFERS_PER_POOL_LOWMEM 6
#define CTB_COMMANDS_RESIZING_FACTOR       60
#define CTB_RESIDUALS_RESIZING_FACTOR      20

#ifdef HEVC_HADES_CANNESWIFI
    int ppBufPerPool = HEVC_MIN_NB_OF_PP_BUFFERS_PER_POOL_LOWMEM + 1;
#else
    int ppBufPerPool = HEVC_MIN_NB_OF_PP_BUFFERS_PER_POOL;
#endif
    // Patch for low-mem platforms -> using SerializeDecoders module param
    // Limiting the nb of pp buffers per pool for flush performance purpose
    if (ModuleParameter_SerializeDecoders())
    {
        ppBufPerPool = HEVC_MIN_NB_OF_PP_BUFFERS_PER_POOL_LOWMEM;
    }

    // free pool/allocator if memory is already allocated
    DeallocatePreProcBufferPool();
    for (i = 0; i < HEVC_NUM_INTERMEDIATE_POOLS; i++)
    {
        if (i == HEVC_CTB_COMMANDS)
        {
            PPPoolSize[i] = (mPreprocessorBufferSize[i] + (CTB_COMMANDS_RESIZING_FACTOR * mPreprocessorBufferSize[i] * (ppBufPerPool - 1)) / 100);
        }
        else if (i == HEVC_CTB_RESIDUALS)
        {
            PPPoolSize[i] = (mPreprocessorBufferSize[i] + (CTB_RESIDUALS_RESIZING_FACTOR * mPreprocessorBufferSize[i] * (ppBufPerPool - 1)) / 100);
        }
        else // for HEVC_SLICE_TABLE, HEVC_SLICE_HEADERS, HEVC_CTB_TABLE
        {
            PPPoolSize[i] = ppBufPerPool * mPreprocessorBufferSize[i];
        }

        // For small resolution streams, pool must be oversized to avoid GetBuffer failure
        if ((pic_height_in_luma_samples < HEVC_MIN_RES_FOR_PP_BUFFERS_DIMENSIONNING) ||
            (pic_width_in_luma_samples < HEVC_MIN_RES_FOR_PP_BUFFERS_DIMENSIONNING))
        {
            PPPoolSize[i] *= 2;
        }
        PPPoolSize[i] = BUF_ALIGN_UP(PPPoolSize[i], HADESPP_BUFFER_ALIGNMENT);
        TotalPPSize += PPPoolSize[i];
        SE_INFO(group_decoder_video, "[ALLOC] Requesting PP pool[%d] of size: %d bytes\n", i, PPPoolSize[i]);
    }

    allocator_status_t AllocatorStatus = PartitionAllocatorOpen(&mPreProcessorBufferAllocator,
                                                                Configuration.AncillaryMemoryPartitionName,
                                                                TotalPPSize, MEMORY_VIDEO_HWONLY_ACCESS);

    if (AllocatorStatus != allocator_ok)
    {
        SE_ERROR("Failed to allocate pre-processor partition (status: %08x)\n", AllocatorStatus);
        SetComponentState(ComponentInError);
        // No need to free existing allocators, dtor() will take care of it
        return CodecError;
    }

    void *PPBufferMemoryPool[3];
    PPBufferMemoryPool[CachedAddress] = NULL;
    PPBufferMemoryPool[PhysicalAddress] = AllocatorPhysicalAddress(mPreProcessorBufferAllocator);
    mDecodeBufferManager = Stream->GetDecodeBufferManager();
    for (i = 0; i < HEVC_NUM_INTERMEDIATE_POOLS; i++)
    {
        // TODO change to MEMORY_HWONLY_ACCESS as PP buffers are not accessed by host (except for soft CRC)
        Status = BufferManager->CreatePool(
                     &mPreProcessorBufferPool[i],
                     mPreProcessorBufferType[i],
                     MAX_PENDING_PP_TASK ,
                     PPPoolSize[i],
                     PPBufferMemoryPool,
                     NULL, NULL, true,
                     MEMORY_VIDEO_HWONLY_ACCESS);

        if (Status != BufferNoError)
        {
            SE_ERROR("Failed to create pre-processor buffer pool of type %s (%08x)\n",
                     HevcPreprocessorBufferDescriptor[i].TypeName, Status);
            goto pool_creation_failed;
        }
        PPBufferMemoryPool[PhysicalAddress] = (void *)((unsigned int)PPBufferMemoryPool[PhysicalAddress] + PPPoolSize[i]);
        mDecodeBufferManager->RegisterPreprocBufferPool(mPreProcessorBufferPool[i], i);
        SE_VERBOSE(group_decoder_video, "[ALLOC] Requesting PP pool[%d]: physical: 0x%x virtual: 0x%x of size: %d bytes\n", i,
                   (uint32_t)PPBufferMemoryPool[PhysicalAddress], (uint32_t)PPBufferMemoryPool[CachedAddress], PPPoolSize[i]);
    }

    SE_INFO(group_decoder_video, "[ALLOC] Total size of PP pools: %d bytes\n", TotalPPSize);
    return CodecNoError;

pool_creation_failed:
    DeallocatePreProcBufferPool();
    SetComponentState(ComponentInError);
    return CodecError;
}

// //////////////////////////////////////////////////////////////////////////////
//      Remove superfluous zero bytes at end of nal unit
//      Might be the first 0 of a 4-byte start code, or anti-emulated zero bytes
//
void Codec_MmeVideoHevc_c::TrimCodedBuffer(uint8_t *buffer, unsigned int DataOffset, unsigned int *CodedSlicesSize)
{
    uint8_t *at_start = buffer + DataOffset;
    uint8_t *at_end = at_start + *CodedSlicesSize - 1;
    uint8_t *at_tmp;
    uint32_t *tmp;
    // Little endian data format
    uint32_t mask = 0xFFFFFF00U;
    uint32_t val = 0x300000U;

    while ((at_end > at_start) && (*at_end == 0))  { at_end--; }

    at_tmp = at_end;
    tmp = (uint32_t *)(at_tmp - 3);
    // Fast search : try 12 bytes before
    while ((tmp > (uint32_t *)at_start) && (((*tmp) & mask) == val))
    {
        /* Lowest common multiple is 12 (= 3x4 bytes)*/
        at_tmp = (uint8_t *)tmp;
        tmp -= 3;
    }
    // Accurate search of the last 3-bytes != 00 00 03
    if (tmp < (uint32_t *)at_end - 3)
    {
        at_end = at_tmp;
        tmp = (uint32_t *)(at_end - 3);
        while ((at_end > at_start) && (((*tmp) & mask) == val))
        {
            at_end -= 3;
            tmp = (uint32_t *)at_end;
        }
    }

    *CodedSlicesSize = at_end - at_start + 1;
}

// Check output of the PP and shrink PP buffers consequently (mandatory before
// launching a new preprocessing, in order to optimise PP pool usage)
// In case of PP error (CodecError is returned), PP buffers must be detached by caller
CodecStatus_t Codec_MmeVideoHevc_c::CheckPPStatus(ppFrame_t<HevcPpFrame_t> *ppFrame,
                                                  ParsedFrameParameters_t *parsedFrameParameters)
{
    struct hadespp_ioctl_dequeue_t *ppEntry = &ppFrame->data.param.out;
    BufferStatus_t BufStatus;

    DumpIntermediateBuffers(ppFrame, parsedFrameParameters);
    if ((ppEntry->hwStatus == hadespp_unrecoverable_error) || (ppEntry->hwStatus == hadespp_timeout))
    {
        SE_WARNING("Discarding picture (decode idx: %d/ display idx: %d) with pre-processing timeout/unrecoverable error\n",
                   parsedFrameParameters->DecodeFrameIndex, parsedFrameParameters->DisplayFrameIndex);
        // Discard frame with PP unrecoverable errors
        goto hw_failed;
    }
    else if (ppEntry->hwStatus == hevcpp_unrecoverable_ovhd_violation_error)
    {
        if (Stream != NULL)
        {
            SE_ERROR("Discarding picture with pre-processing unrecoverable ovhd violation error - marking stream unplayable\n");
            // raise the event to signal stream unplayable
            Stream->MarkUnPlayable(STM_SE_PLAY_STREAM_MSG_REASON_CODE_OVHD_VIOLATION, true);
            goto hw_failed;
        }
    }
    else if (ppEntry->hwStatus == hevcpp_unrecoverable_hevc_violation_error)
    {
        if (Stream != NULL)
        {
            SE_ERROR("Discarding picture with pre-processing unrecoverable hevc violation error - marking stream unplayable\n");
            // raise the event to signal stream unplayable
            Stream->MarkUnPlayable(STM_SE_PLAY_STREAM_MSG_REASON_CODE_HEVC_VIOLATION, true);
            goto hw_failed;
        }
    }
    else if ((ppEntry->hwStatus == hadespp_recoverable_error))
    {
        int AllowBadPPFrames = Player->PolicyValue(Playback, Stream, PolicyAllowBadHevcPreProcessedFrames);
        if (AllowBadPPFrames != PolicyValueApply)
        {
            SE_INFO(group_decoder_video, "Due to policy PolicyAllowBadHevcPreProcessedFrames, picture with recoverable pre-processing error is discarded\n");
            goto hw_failed;
        }
    }

    if (Stream != NULL)
    {
        PlayerSequenceNumber_t  *SequenceNumberStructure;
        ppFrame->data.CodedBuffer->ObtainMetaDataReference(Stream->GetPlayer()->MetaDataSequenceNumberType,
                                                           (void **)(&SequenceNumberStructure));
        SE_ASSERT(SequenceNumberStructure != NULL);

        SE_VERBOSE2(group_se_pipeline, group_perf_video_decoder,
                    "Stream 0x%p - %d - #%lld (dec idx: %d) PP decoding time: %lu us, ctb_cmd: %dkB, ctb_res: %dkB, PTS=%lu PP=%llu %s %s\n",
                    (void *)Stream, Stream->GetStreamType(),
                    SequenceNumberStructure->Value,
                    parsedFrameParameters->DecodeFrameIndex,
                    (unsigned long)ppEntry->decode_time_hw,
                    ppEntry->iStatus.hevc_iStatus.ctb_commands_stop_offset / 1000,
                    ppEntry->iStatus.hevc_iStatus.ctb_residuals_stop_offset / 1000,
                    (unsigned long)parsedFrameParameters->PTS.PtsValue(),
                    OS_GetTimeInMicroSeconds(),
                    parsedFrameParameters->KeyFrame ? "K" : "-",
                    parsedFrameParameters->IndependentFrame ? "I" : parsedFrameParameters->ReferenceFrame ? "P" : "B"
                   );

        if (Stream->Statistics().MaxVideoHwPreprocTime < ppEntry->decode_time_hw)
        {
            Stream->Statistics().MaxVideoHwPreprocTime = ppEntry->decode_time_hw;
        }
        if ((Stream->Statistics().MinVideoHwPreprocTime == 0) && (ppEntry->decode_time_hw != 0))
        {
            Stream->Statistics().MinVideoHwPreprocTime = ppEntry->decode_time_hw;
        }
        if (Stream->Statistics().MinVideoHwPreprocTime > ppEntry->decode_time_hw)
        {
            Stream->Statistics().MinVideoHwPreprocTime = ppEntry->decode_time_hw;
        }
        if (Stream->Statistics().FrameCountDecoded != 0)
        {
            Stream->Statistics().AvgVideoHwPreprocTime =
                (ppEntry->decode_time_hw + ((Stream->Statistics().FrameCountDecoded - 1) *
                                            Stream->Statistics().AvgVideoHwPreprocTime)) /
                (Stream->Statistics().FrameCountDecoded);
        }
    }

    ppFrame->data.CodedBuffer->SetUsedDataSize(0);
    BufStatus = ppFrame->data.CodedBuffer->ShrinkBuffer(0);
    if (BufStatus != BufferNoError)
    {
        SE_INFO(group_decoder_video, "Failed to shrink buffer\n");
    }

    // Shrink intermediate buffers
    // Caution: do not shrink to zero otherwise the next ObtainDataReference() will return NULL
#define SHRINK_IB(INDEX, LENGTH) \
    {                                                \
        int new_length = ppEntry->iStatus.hevc_iStatus.LENGTH;     \
        if (new_length == 0)                         \
          new_length = HADESPP_BUFFER_ALIGNMENT+1; /* keep the buffer allocated and alignement for next buffer allocations */                \
        if (ppFrame->data.PreProcessorBuffer[INDEX]) {                            \
            ppFrame->data.PreProcessorBuffer[INDEX]->SetUsedDataSize(new_length); \
            ppFrame->data.PreProcessorBuffer[INDEX]->ShrinkBuffer(new_length);    \
        } \
    }
    SHRINK_IB(HEVC_SLICE_TABLE,   slice_table_stop_offset);
    SHRINK_IB(HEVC_CTB_TABLE,     ctb_table_stop_offset);
    SHRINK_IB(HEVC_SLICE_HEADERS, slice_headers_stop_offset);
    SHRINK_IB(HEVC_CTB_COMMANDS,  ctb_commands_stop_offset);
    SHRINK_IB(HEVC_CTB_RESIDUALS, ctb_residuals_stop_offset);

    ((HevcFrameParameters_t *)(parsedFrameParameters->FrameParameterStructure))->SliceTableEntries =
        ppEntry->iStatus.hevc_iStatus.slice_table_entries;

    SE_VERBOSE(group_decoder_video,
               "slice_table_stop_offset:%d ctb_table_stop_offset:%d "
               "slice_headers_stop_offset:%d ctb_commands_stop_offset:%d "
               "ctb_residuals_stop_offset:%d slice_table_entries:%d error:0x%08x\n",
               ppEntry->iStatus.hevc_iStatus.slice_table_stop_offset,
               ppEntry->iStatus.hevc_iStatus.ctb_table_stop_offset,
               ppEntry->iStatus.hevc_iStatus.slice_headers_stop_offset,
               ppEntry->iStatus.hevc_iStatus.ctb_commands_stop_offset,
               ppEntry->iStatus.hevc_iStatus.ctb_residuals_stop_offset,
               ppEntry->iStatus.hevc_iStatus.slice_table_entries,
               ppEntry->iStatus.hevc_iStatus.error);

    return CodecNoError;

hw_failed:
    if (parsedFrameParameters->FirstParsedParametersForOutputFrame)
    {
        Stream->ParseToDecodeEdge->RecordNonDecodedFrame(ppFrame->data.CodedBuffer, parsedFrameParameters);
    }
    else
    {
        OutputPartialDecodeBuffers();
    }

    ppFrame->data.CodedBuffer->SetUsedDataSize(0);
    BufStatus = ppFrame->data.CodedBuffer->ShrinkBuffer(0);
    if (BufStatus != BufferNoError)
    {
        SE_WARNING("Failed to shrink buffer\n");
    }

    return CodecError;
}

void Codec_MmeVideoHevc_c::DumpIntermediateBuffers(ppFrame_t<HevcPpFrame_t> *ppFrame,
                                                   ParsedFrameParameters_t *parsedFrameParameters)
{
    uint32_t ctb_table, slice_table, slice_headers, ctb_commands, ctb_residuals;
    hevcpreproc_transform_param_t *cmd = &ppFrame->data.param.in.iCmd.hevc_iCmd;
    struct hadespp_ioctl_dequeue_t *ppEntry = &ppFrame->data.param.out;
    intermediate_buffer_t *ib = &cmd->intermediate_buffer;

    void *PPBufferMemoryPool[3];
    PPBufferMemoryPool[CachedAddress] = AllocatorUserAddress(mPreProcessorBufferAllocator);
    PPBufferMemoryPool[PhysicalAddress] = AllocatorPhysicalAddress(mPreProcessorBufferAllocator);

#define PP_VIRTUAL_ADDR(a) ((uint32_t)PPBufferMemoryPool[CachedAddress] + \
        (a - (uint32_t)PPBufferMemoryPool[PhysicalAddress]))

    slice_table = PP_VIRTUAL_ADDR(ib->slice_table_base_addr);
    ctb_table = PP_VIRTUAL_ADDR(ib->ctb_table_base_addr);
    slice_headers = PP_VIRTUAL_ADDR(ib->slice_headers_base_addr);
    ctb_commands = PP_VIRTUAL_ADDR(ib->ctb_commands.base_addr);
    ctb_residuals = PP_VIRTUAL_ADDR(ib->ctb_residuals.base_addr);

    SE_VERBOSE(group_decoder_video, "dec idx: %d slice_table: v 0x%x p 0x%x  ctb_table: v 0x%x p 0x%x  slice_headers: v 0x%x p 0x%x  ctb_commands: v 0x%x p 0x%x ctb_residuals: v 0x%x p 0x%x\n",
               parsedFrameParameters->DecodeFrameIndex,
               slice_table, ib->slice_table_base_addr,
               ctb_table, ib->ctb_table_base_addr,
               slice_headers, ib->slice_headers_base_addr,
               ctb_commands, ib->ctb_commands.base_addr,
               ctb_residuals, ib->ctb_residuals.base_addr);
    st_relayfs_write_se(ST_RELAY_TYPE_INTERMEDIATE_VIDEO_BUFFER, ST_RELAY_SOURCE_SE,
                        (unsigned char *) slice_table, ppEntry->iStatus.hevc_iStatus.slice_table_stop_offset, false);
    st_relayfs_write_se(ST_RELAY_TYPE_INTERMEDIATE_VIDEO_BUFFER, ST_RELAY_SOURCE_SE,
                        (unsigned char *) ctb_table, ppEntry->iStatus.hevc_iStatus.ctb_table_stop_offset, false);
    st_relayfs_write_se(ST_RELAY_TYPE_INTERMEDIATE_VIDEO_BUFFER, ST_RELAY_SOURCE_SE,
                        (unsigned char *) slice_headers, ppEntry->iStatus.hevc_iStatus.slice_headers_stop_offset, false);
    st_relayfs_write_se(ST_RELAY_TYPE_INTERMEDIATE_VIDEO_BUFFER, ST_RELAY_SOURCE_SE,
                        (unsigned char *) ctb_commands, ppEntry->iStatus.hevc_iStatus.ctb_commands_stop_offset, false);
    st_relayfs_write_se(ST_RELAY_TYPE_INTERMEDIATE_VIDEO_BUFFER, ST_RELAY_SOURCE_SE,
                        (unsigned char *) ctb_residuals, ppEntry->iStatus.hevc_iStatus.ctb_residuals_stop_offset, false);
}


// /////////////////////////////////////////////////////////////////////////
//
//      Input, must feed the data to the preprocessor chain.
//      This function needs to replicate a considerable amount of the
//      ancestor classes, because this function is operating
//      in a different process to the ancestors, and because the
//      ancestor data, and the data used here, will be in existence
//      at the same time.
//

CodecStatus_t Codec_MmeVideoHevc_c::Input(Buffer_t CodedBuffer)
{
    unsigned int             i, j;
    hadespp_status_t         PPStatus;
    BufferStatus_t           BufferStatus;
    ParsedFrameParameters_t *ParsedFrameParameters;
    ParsedVideoParameters_t *ParsedVideoParameters;
    HevcFrameParameters_t   *FrameParameters;

    //
    // First extract the useful pointers from the buffer all held locally
    //
    CodedBuffer->ObtainMetaDataReference(Player->MetaDataParsedFrameParametersType,
                                         (void **)(&ParsedFrameParameters));
    SE_ASSERT(ParsedFrameParameters != NULL);
    FrameParameters = (HevcFrameParameters_t *) ParsedFrameParameters->FrameParameterStructure;

    CodedBuffer->ObtainMetaDataReference(Player->MetaDataParsedVideoParametersType,
                                         (void **)(&ParsedVideoParameters));
    SE_ASSERT(ParsedVideoParameters != NULL);

    //
    // If this does not contain any frame data, then we simply wish
    // to slot it into place for the intermediate process.
    //
    int ret = 0;
    ppFrame_t<HevcPpFrame_t> ppFrame;
    if (!ParsedFrameParameters->NewFrameParameters ||
        (ParsedFrameParameters->DecodeFrameIndex == INVALID_INDEX))
    {
        new(&ppFrame) ppFrame_t<HevcPpFrame_t>(ppActionPassOnFrame, ParsedFrameParameters->DecodeFrameIndex);
        ppFrame.data.CodedBuffer = CodedBuffer;

        CodedBuffer->IncrementReferenceCount(IdentifierHevcIntermediate);
        ret = mPpFramesRing->Insert(&ppFrame);
        if (ret < 0)
        {
            SE_ERROR("Failed to insert frame for PP\n");
            CodedBuffer->DecrementReferenceCount(IdentifierHevcIntermediate);
            return CodecError;
        }
        return CodecNoError;
    }

    //
    // We believe we have a frame - check that this is marked as a first slice
    //
    if (!ParsedVideoParameters->FirstSlice)
    {
        SE_ERROR("Non first slice, when one is expected\n");
        return CodecError;
    }

    if (ParsedFrameParameters->ReferenceFrameList[REF_PIC_LIST_0].EntryCount !=
        ParsedFrameParameters->ReferenceFrameList[REF_PIC_LIST_1].EntryCount)
    {
        SE_ERROR("Ref pic lists do not have same number of elements\n");
        return CodecError;
    }

    // Allocate intermediate buffers and attach them to coded buffer
    bool allocatePoolNeeded = false;
    for (i = 0; i < HEVC_NUM_INTERMEDIATE_POOLS; i++)
    {
        if (mPreProcessorBufferPool[i] == NULL)
        {
            allocatePoolNeeded = true;
            break;
        }
    }
    ret = UpdatePreprocessorBufferSize(FrameParameters);
    if (ret < 0)
    {
        SE_ERROR("Unable to get PPBuffer size\n");
        return CodecError;
    }
    if ((ret > 0) || (allocatePoolNeeded))
    {
        if (mPreProcessorBufferPool[HEVC_CTB_RESIDUALS] != NULL)
        {
            SE_INFO(group_decoder_video, "PPBuffer size has changed -> updating the PP Pool size\n");
            //
            // Waiting for end of ALL PP pending tasks
            // Before destroying the pool ensure that no buffer in the pool is in use.
            // With a highly corrupted HEVC stream it has been observed that the coded buffer
            // along with the attached PP buffer exits the PP ring but before it is sent
            // for decode, DeallocatePreProcBufferPool gets invoked due to level change
            // resulting in assertion SE_ASSERT(PreProcessorBuffer != NULL) in FillOutDecodeCommand
            // Since all PP buffer pools are allocated and deallocated together checking pool
            // usage of one pool is enough
            //
            unsigned int BuffersWithNonZeroReferenceCount;
            unsigned long long wait_start = OS_GetTimeInMicroSeconds();
            unsigned long long waiting_time_in_us;
            do
            {
                OS_SleepMilliSeconds(2); // jiffies granularity is around 10ms anyway
                mPreProcessorBufferPool[HEVC_CTB_RESIDUALS]->GetPoolUsage(NULL, &BuffersWithNonZeroReferenceCount, NULL, NULL, NULL, NULL);
                waiting_time_in_us = (OS_GetTimeInMicroSeconds() - wait_start) / 1000;
                if (waiting_time_in_us > 700)
                {
                    SE_WARNING("Before reallocation, waiting for PreProcBufferPool usage to go to zero during %llu us\n", waiting_time_in_us);
                }
            }
            while (BuffersWithNonZeroReferenceCount != 0);

            SE_VERBOSE(group_decoder_video, "Ready to re-allocate preproc buffer pool after %llu us\n", waiting_time_in_us);
        }

        if (CodecNoError != AllocatePreProcBufferPool(FrameParameters))
        {
            SE_ERROR("Unable to allocate PP buffer pools\n");
            DeallocatePreProcBufferPool();
            return CodecError;
        }
    }

    new(&ppFrame) ppFrame_t<HevcPpFrame_t>(ppActionPassOnFrame, ParsedFrameParameters->DecodeFrameIndex);
    for (i = 0; i < HEVC_NUM_INTERMEDIATE_POOLS; i++)
    {
        // SE_DEBUG(group_decoder_video, "PreprocessorBufferSize %d = %d\n", i, mPreprocessorBufferSize[i]);
        BufferStatus = mPreProcessorBufferPool[i]->GetBuffer(&ppFrame.data.PreProcessorBuffer[i],
                                                             UNSPECIFIED_OWNER,
                                                             mPreprocessorBufferSize[i]);
        if ((BufferStatus != BufferNoError) || (ppFrame.data.PreProcessorBuffer[i] == NULL))
        {
            BufferStatus = BufferError;
            break;
        }

        CodedBuffer->AttachBuffer(ppFrame.data.PreProcessorBuffer[i]);
        // Now that buffer is attached to the coded buffer we can release our hold on it
        ppFrame.data.PreProcessorBuffer[i]->DecrementReferenceCount();
    }

    //
    // Failed to allocate all buffers. Free already allocated buffers if any
    // and return error
    //
    if (BufferStatus != BufferNoError)
    {
        for (j = 0; j < i; j++)
        {
            CodedBuffer->DetachBuffer(ppFrame.data.PreProcessorBuffer[j]);
        }

        SE_ERROR("Failed to get a pre-processor buffer of type %d (%08x)\n", j, BufferStatus);
        return CodecError;
    }

    // Fill in relevant fields
    ppFrame.data.CodedBuffer = CodedBuffer;
    FillOutPreprocessorCommand(&ppFrame, ParsedFrameParameters);

    CodedBuffer->IncrementReferenceCount(IdentifierHevcIntermediate);

    SE_VERBOSE(group_decoder_video, ">PP frame %d\n", ppFrame.DecodeFrameIndex);

    // Blocking call to preprocess ppFrame buffer
    PPStatus = HadesppPreProcessBuffer(mPreProcessorDevice, &ppFrame.data.param);
    if (PPStatus != hadespp_ok)
    {
        SE_ERROR("Failed to preprocess buffer, junking frame\n");
        goto pp_error;
    }
    if (CodecNoError != CheckPPStatus(&ppFrame, ParsedFrameParameters))
    {
        SE_ERROR("PP returned unrecoverable error\n");
        goto pp_error;
    }
    SE_VERBOSE(group_decoder_video, "<PP frame %d\n", ppFrame.DecodeFrameIndex);

    // Send PP output for decoding stage (release of buffer will be done in IntermediateProcess)
    ret = mPpFramesRing->Insert(&ppFrame);
    if (ret < 0)
    {
        SE_ERROR("Failed to insert PP frame[%d]\n", ppFrame.DecodeFrameIndex);
        goto pp_error;
    }

    return CodecNoError;

pp_error:
    for (i = 0; i < HEVC_NUM_INTERMEDIATE_POOLS; i++)
    {
        CodedBuffer->DetachBuffer(ppFrame.data.PreProcessorBuffer[i]);
    }

    CodedBuffer->DecrementReferenceCount(IdentifierHevcIntermediate);
    return CodecError;
}

// /////////////////////////////////////////////////////////////////////////

void Codec_MmeVideoHevc_c::ClearInternalBufferMap()
{
    SE_DEBUG(group_decoder_video, "\n");
    if (mPreProcessorBufferAllocator != NULL)
    {
        AllocatorRemoveMapEx(mPreProcessorBufferAllocator->UnderlyingDevice);
    }
}

void Codec_MmeVideoHevc_c::CreateInternalBufferMap()
{
    SE_DEBUG(group_decoder_video, "\n");
    if (mPreProcessorBufferAllocator != NULL)
    {
        AllocatorCreateMapEx(mPreProcessorBufferAllocator->UnderlyingDevice);
    }
}

// /////////////////////////////////////////////////////////////////////////
//
//      Function to deal with the returned capabilities
//      structure for an HEVC mme transformer.
//

CodecStatus_t   Codec_MmeVideoHevc_c::HandleCapabilities()
{
    // Default is to keep DisplayFormat to 8 bit format
    BufferFormat_t DisplayFormat = FormatVideo420_Raster2B;

    // Elements to produce in the buffers
    // HADES HEVC HW bug : it always requires to have a buffer to store the Macro block structure
    // (ppb buffer) , this is normaly needed only for Reference frames.
    DecodeBufferComponentElementMask_t Elements = PrimaryManifestationElement |
                                                  DecimatedManifestationElement |
                                                  VideoMacroblockStructureElement |
                                                  VideoMacroblockStructureForNotReferenceElement;

#ifdef HEVC_HADES_CANNESWIFI
    // Canne WIFI supports to have reference in Raster format, avoiding to use the CopyBuffer
    if (NoVideoCopyBuffer(HADES_CODEC))
    {
        SE_INFO(group_decoder_video, "Display & Reference buffer format are same: %s\n", PLAYER_STRINGIFY(FormatVideo420_Raster2B));
        RefBufferType = PolicyValueRasterBuffOnly;
    }
    else
#endif
    {
        int DecodeBuffPolicy = Player->PolicyValue(Playback, Stream, PolicyDecodeBufferCopy);

        // Default: Display - Raster2B & Reference - OMEGA8\OMEGA10
        if ((DecodeBuffPolicy != PolicyValueMBandRasterBuff) && (DecodeBuffPolicy != PolicyValueDefaultBufferCopyMode))
        {
            SE_WARNING("Wrong DecodeBuffPolicy selected. Enabled default policy to %s\n", PLAYER_STRINGIFY(PolicyValueMBandRasterBuff));
            Player->SetPolicy(Playback, Stream, PolicyDecodeBufferCopy, PolicyValueMBandRasterBuff);
        }
        // VideoDecodeCopyElement added because reference fmt is different from display fmt
        Elements |= VideoDecodeCopyElement;
    }

    Stream->GetDecodeBufferManager()->FillOutDefaultList(DisplayFormat,
                                                         Elements,
                                                         Configuration.ListOfDecodeBufferComponents);
    return CodecNoError;
}


// /////////////////////////////////////////////////////////////////////////
//
//  The  HEVC video function used to determine the number of buffer to be used
//

void   Codec_MmeVideoHevc_c::FillOutBufferCountRequest(DecodeBufferRequest_t   *Request)
{
    unsigned int Size        = Request->Dimension[0] * Request->Dimension[1] * 3 / 2;

    // Marker frame only , do not need to compute remaining field
    if (Size == 0) { return; }

    unsigned int    dpb_size = (Size > BUFFER_HD_SIZE) ? HEVC_MAX_4K2K_DPB_SIZE : HEVC_MAX_HD_DPB_SIZE;

    // computing max number of manifestation buffers
    unsigned int buffercount = min(dpb_size / Size, MAX_DPB_BUFFER) + 1 + MAX_DISPLAY_BUFFER;
    Request->ManifestationBufferCount = max(buffercount, MIN_MANIFESTATION_BUFFER_COUNT);

    //  Buffer allocation is over allocated due to display limitation
    if (ModuleParameter_Support64MbyteCrossing()) { Request->ManifestationBufferCount--; }

    // computing max number of references buffers
    buffercount = min(dpb_size / Size, MAX_DPB_BUFFER) + 1;
    Request->ReferenceBufferCount     = max(buffercount, MIN_REFERENCE_BUFFER_COUNT);

    SE_DEBUG(group_decoder_video, "%p Set Manifestation buffer count:%d reference buffer count:%d frame size %d x %d\n",
             this , Request->ManifestationBufferCount , Request->ReferenceBufferCount, Request->Dimension[0] , Request->Dimension[1]);
    if ((Request->ReferenceBufferCount  != mPreviousRequestReferenceBufferCount)
        || (Request->ManifestationBufferCount != mPreviousRequestManifestationBufferCount))
    {
        mPreviousRequestReferenceBufferCount = Request->ReferenceBufferCount;
        mPreviousRequestManifestationBufferCount = Request->ManifestationBufferCount ;
        SE_DEBUG(group_decoder_video, "%p Set Manifestation buffer count:%d reference buffer count:%d frame size %d x %d\n",
                 this , Request->ManifestationBufferCount , Request->ReferenceBufferCount, Request->Dimension[0] , Request->Dimension[1]);
    }
}


// /////////////////////////////////////////////////////////////////////////
//
//      Function to extend the buffer request to incorporate the macroblock sizing
//

CodecStatus_t   Codec_MmeVideoHevc_c::FillOutDecodeBufferRequest(DecodeBufferRequest_t *Request)
{
    // HevcSequenceParameterSet_t *sps;
    //unsigned int                            ppb_length;
    //
    // Fill out the standard fields first
    //
    Codec_MmeVideo_c::FillOutDecodeBufferRequest(Request);
    //
    // and now the extended field
    //
    // HEVC PPB: 4 words per 16x16 block, see ppb_t[PPB_CMD_SIZE] in STHM'
    Request->MacroblockSize                             = 16 * 16;
    Request->PerMacroblockMacroblockStructureSize       = 4 * sizeof(uint32_t);
    Request->PerMacroblockMacroblockStructureFifoSize   = 0;  // Hades doesn't overrun PPB buffer (or does it ? FIXME)

    if (!Request->mIsMarkerFrame)
    {
        HevcSliceHeader_t *sliceheader = &((HevcFrameParameters_t *)ParsedFrameParameters->FrameParameterStructure)->SliceHeader;
        unsigned int PixelDepth = max(sliceheader->SequenceParameterSet->bit_depth_luma_minus8, sliceheader->SequenceParameterSet->bit_depth_chroma_minus8) + 8;
        BufferFormat_t dispFormat = FormatVideo420_Raster2B;
        Request->PixelDepth = 8;

        if (PixelDepth != 8)
        {
            Request->PixelDepth = 10;
            dispFormat = FormatVideo420_Raster2B_10B;
        }
        Stream->GetDecodeBufferManager()->ModifyComponentFormat(PrimaryManifestationComponent, dispFormat);
#ifdef HEVC_HADES_CANNESWIFI
        // Decimated output must be in 8 bits for Cannes Wifi/L2A
        Stream->GetDecodeBufferManager()->ModifyComponentFormat(DecimatedManifestationComponent, FormatVideo420_Raster2B);
#else
        Stream->GetDecodeBufferManager()->ModifyComponentFormat(DecimatedManifestationComponent, dispFormat);
#endif
    }
    FillOutBufferCountRequest(Request);

    if (ParsedVideoParameters && ParsedVideoParameters->PictureStructure != StructureFrame)
    {
        /* For interlaced video, it is possible that the first field is not a reference, but the second field is.
         * So force reference components allocation(and not shared scratch buffer) for first field.
         * In case the second field is not a reference, reference components would be freed later in FillOutDecodeCommand */
        Request->ReferenceFrame = true;

        /* For interlaced video, each half of DecodeCopy/PPB buffer needs to be aligned */
        Request->AlignmentNeededforInterlaced = true;
    }

    return CodecNoError;
}

// /////////////////////////////////////////////////////////////////////////
//
//      Populate the transformers initialization parameters.
//
//      This method is required the fill out the TransformerInitParamsSize
//      and TransformerInitParams_p members of Codec_MmeBase_c::MMEInitializationParameters
//

CodecStatus_t   Codec_MmeVideoHevc_c::FillOutTransformerInitializationParameters()
{
    // Pass the instance number of the stream (used for CRC checking)
    InitTransformerParam.InstanceId = Stream->GetInstanceId();
    MMEInitializationParameters.TransformerInitParamsSize       = sizeof(InitTransformerParam);
    MMEInitializationParameters.TransformerInitParams_p         = &InitTransformerParam;
//
    return CodecNoError;
}

CodecStatus_t   Codec_MmeVideoHevc_c::FillOutSetStreamParametersCommand()
{
    // Should never get here
    SE_ERROR("Implementation error\n");
    return CodecError;
}

// TODO: rework this static class
uint32_t Codec_MmeVideoHevc_c::scaling_list_command_init(uint32_t *pt,
                                                         uint8_t use_default,
                                                         scaling_list_t *scaling_list)
{
    uint32_t idx = 0;
    uint32_t word;

    for (uint8_t size_id = 0; size_id < MAX_SCALING_SIZEID; size_id++)
    {
        uint8_t max_matrixID = (size_id == 3) ? 2 : MAX_SCALING_MATRIXID;

        for (uint8_t matrix_id = 0; matrix_id < max_matrixID; matrix_id++)
        {
            if (use_default)
            {
                FIELD_INIT(word, HEVC_FORMAT_PIC_SCALING_LIST_PRED, scaling_list_pred_mode_flag, 0);
                FIELD_SET(word, HEVC_FORMAT_PIC_SCALING_LIST_PRED, scaling_list_pred_matrix_id_delta, 0);
                pt[idx++] = word;
            }
            else
            {
                scaling_list_elem_t *scaling_list_elem = &(*scaling_list)[size_id][matrix_id];
                FIELD_INIT(word, HEVC_FORMAT_PIC_SCALING_LIST_PRED, scaling_list_pred_mode_flag, scaling_list_elem->scaling_list_pred_mode_flag);
                FIELD_SET(word, HEVC_FORMAT_PIC_SCALING_LIST_PRED, scaling_list_pred_matrix_id_delta, scaling_list_elem->scaling_list_pred_matrix_id_delta);

                if (scaling_list_elem->scaling_list_pred_mode_flag)
                {
                    if (size_id > 1) // 16x16 or 32x32 TU
                    {
                        FIELD_SET(word, HEVC_FORMAT_PIC_SCALING_LIST_PRED, scaling_list_dc_coeff_minus8, scaling_list_elem->scaling_list_dc_coef_minus8);
                    }

                    pt[idx++] = word;
                    word = 0;
                    uint8_t num_coef = (size_id == 0 ? 16 : 64);

                    for (uint8_t i = 0; i < num_coef; i += 4)
                    {
                        FIELD_INIT(word, HEVC_FORMAT_PIC_SCALING_LIST_DELTA_COEF, scaling_list_delta_coef0, scaling_list_elem->scaling_list_delta_coef[i + 0]);
                        FIELD_SET(word, HEVC_FORMAT_PIC_SCALING_LIST_DELTA_COEF, scaling_list_delta_coef1, scaling_list_elem->scaling_list_delta_coef[i + 1]);
                        FIELD_SET(word, HEVC_FORMAT_PIC_SCALING_LIST_DELTA_COEF, scaling_list_delta_coef2, scaling_list_elem->scaling_list_delta_coef[i + 2]);
                        FIELD_SET(word, HEVC_FORMAT_PIC_SCALING_LIST_DELTA_COEF, scaling_list_delta_coef3, scaling_list_elem->scaling_list_delta_coef[i + 3]);
                        pt[idx++] = word;
                    }
                }
                else
                {
                    pt[idx++] = word;
                }
            }
        }
    }

    return idx;
}

void Codec_MmeVideoHevc_c::SubstituteSavedPreviousReference(hevcdecpix_transform_param_t *cmd, unsigned int index)
{
    cmd->ref_picture_buffer[index].samples.luma_offset   = mSavedReferenceForSubstitution.samples.luma_offset;
    cmd->ref_picture_buffer[index].samples.chroma_offset = mSavedReferenceForSubstitution.samples.chroma_offset;
    cmd->ref_picture_buffer[index].ppb_offset = mSavedReferenceForSubstitution.ppb_offset;
#if !defined (HEVC_HADES_CANNES25) && !defined (HEVC_HADES_CANNESWIFI)
    cmd->ref_picture_buffer[index].samples.stride = mSavedReferenceForSubstitution.samples.stride;
    cmd->ref_picture_buffer[index].samples.raster2b_not_omega4 = false;
#endif
    if ((cmd->ref_picture_buffer[index].samples.luma_offset != 0) && (cmd->ref_picture_buffer[index].samples.chroma_offset != 0))
    {
        cmd->ref_picture_buffer[index].enable_flag = true;
        cmd->ref_picture_info[index].non_existing_flag = 0;
    }
    else
    {
        // In case frame is freed ask firmware not to use it.
        cmd->ref_picture_buffer[index].enable_flag = false;
        cmd->ref_picture_info[index].non_existing_flag = 1;
    }
    cmd->ref_picture_info[index].long_term_flag = DecodeContext->ReferenceFrameList[REF_PIC_LIST_0].ReferenceDetails[0].LongTermReference;
}


CodecStatus_t   Codec_MmeVideoHevc_c::FillOutDecodeCommand()
{
    HevcCodecDecodeContext_t *Context = (HevcCodecDecodeContext_t *)DecodeContext;
    Buffer_t                 PreProcessorBuffer;
    unsigned int             InputBuffer[HEVC_NUM_INTERMEDIATE_POOLS];
    unsigned int             InputBufferSize[HEVC_NUM_INTERMEDIATE_POOLS];
    unsigned int             i;
    unsigned int             j;
    unsigned int             NumRefPic;
    int32_t                  abs_diff_poc, min_abs_diff_poc;
    unsigned int             width_offset = 0;

    // Configurable DecodeComponent to handle configurable DecodeBuffer fmt
    DecodeBufferComponentType_t DecodeComponent = (RefBufferType == PolicyValueRasterBuffOnly) ? PrimaryManifestationComponent : VideoDecodeCopy;

    //
    // Detach the pre-processor buffers and re-attach them to the decode context
    // this will make its release automatic on the freeing of the decode context.
    //

    for (i = 0; i < HEVC_NUM_INTERMEDIATE_POOLS; i++)
    {
        CodedFrameBuffer->ObtainAttachedBufferReference(mPreProcessorBufferType[i], &PreProcessorBuffer);
        SE_ASSERT(PreProcessorBuffer != NULL);

        DecodeContextBuffer->AttachBuffer(PreProcessorBuffer);      // Must be ordered, otherwise the pre-processor
        CodedFrameBuffer->DetachBuffer(PreProcessorBuffer);         // buffer will get released in the middle
        // Store address and size of buffer
        PreProcessorBuffer->ObtainDataReference(NULL, InputBufferSize + i, (void **)&InputBuffer[i], PhysicalAddress);
        SE_ASSERT(InputBuffer[i] != NULL); // not expected to be empty ? .. TBC
    }

    //
    // Fill out the sub-components of the command data
    //
    memset(&Context->frame, 0, sizeof(Context->frame));

    HevcSliceHeader_t *sliceheader = &((HevcFrameParameters_t *)ParsedFrameParameters->FrameParameterStructure)->SliceHeader;
    HevcSequenceParameterSet_t *sps = sliceheader->SequenceParameterSet;
    HevcPictureParameterSet_t *pps = sliceheader->PictureParameterSet;
    hevcdecpix_transform_param_t *cmd = &Context->frame.cmd.TransformParameters;

    DecodeBufferManager_t dbm = Stream->GetDecodeBufferManager();
    if (!ParsedFrameParameters->FirstParsedParametersForOutputFrame && (BufferState[CurrentDecodeBufferIndex].ReferenceFrameCount == 0))
    {
        /* In case none of the Fields is a Reference, Free the Reference Buffers(DecodeCopy and PPB) */
        dbm->AheadReleaseReferenceComponents(BufferState[CurrentDecodeBufferIndex].Buffer);
    }

#if defined(HEVC_HADES_CANNES25) || defined(HEVC_HADES_CANNESWIFI)
    cmd->num_long_term_pics = sliceheader->num_long_term_pics; // WA hevc PP : pass the parsed num_long_term_pics to firmware
#endif

    // Scaling list
    if (sps->scaling_list_enable_flag)
    {
        if (pps->pps_scaling_list_data_present_flag)
        {
            cmd->scaling_list_command_size = scaling_list_command_init(cmd->scaling_list_command, false, &pps->scaling_list);
        }
        else
        {
            cmd->scaling_list_command_size = scaling_list_command_init(cmd->scaling_list_command, !sps->sps_scaling_list_data_present_flag, &sps->scaling_list);
        }
    }
    else
    {
        cmd->scaling_list_command_size = 0;
    }

    for (i = cmd->scaling_list_command_size; i < HEVC_MAX_SCALING_LIST_BUFFER; i++)
    {
        cmd->scaling_list_command[i] = 0;
    }

    // Set bases of DPB and PPBs to 0 so that offset == address
    cmd->dpb_base_addr = 0;
    cmd->ppb_base_addr = 0;

#if defined (HEVC_HADES_CANNES25) || defined (HEVC_HADES_CANNESWIFI)
    // Set reference DPB format
    // HEVC_OMEGAH_10 when 9 or 10 bits, HEVC_RASTER2B_8/10 on future chips (raster prediction)
    // HEVC_OMEGAH_8 when 8 bits, HEVC_RASTER2B_8/10 on future chips (raster prediction)
    if (RefBufferType == PolicyValueRasterBuffOnly)
    {
        cmd->ref_format_idc = ((sps->bit_depth_luma_minus8 != 0 || sps->bit_depth_chroma_minus8 != 0) ? HEVC_RASTER2B_10 : HEVC_RASTER2B_8);
        cmd->ref_stride = BUF_ALIGN_UP(sps->pic_width_in_luma_samples, 0xFU); // Keeping 16 alignment as of now
        width_offset = cmd->ref_stride;
        if (ParsedVideoParameters->PictureStructure != StructureFrame)
        {
            cmd->ref_stride *= 2; // Double the stride for interlaced video
        }
    }
    else
    {
        cmd->ref_format_idc = ((sps->bit_depth_luma_minus8 != 0 || sps->bit_depth_chroma_minus8 != 0) ? HEVC_OMEGAH_10 : HEVC_OMEGAH_8);
        cmd->ref_stride = BUF_ALIGN_UP(sps->pic_width_in_luma_samples, 0xFU);
    }
#endif

    // Reference frame lists

    // before filling up the ref_picture_info[] array, set all pictures to 'non-existing'
    for (i = 0; i < HEVC_MAX_REFERENCE_PICTURES; i++)
    {
        cmd->ref_picture_info[i].non_existing_flag = 1;
        cmd->ref_picture_buffer[i].enable_flag = false;
        cmd->ref_picture_buffer[i].samples.luma_offset = (uint32_t) dbm->Luma(BufferState[CurrentDecodeBufferIndex].Buffer, DecodeComponent);
        cmd->ref_picture_buffer[i].samples.chroma_offset = (uint32_t) dbm->Chroma(BufferState[CurrentDecodeBufferIndex].Buffer, DecodeComponent);
        cmd->ref_picture_buffer[i].ppb_offset = (uint32_t) dbm->ComponentBaseAddress(BufferState[CurrentDecodeBufferIndex].Buffer, VideoMacroblockStructure);
#if !defined (HEVC_HADES_CANNES25) && !defined (HEVC_HADES_CANNESWIFI)
        cmd->ref_picture_buffer[i].samples.raster2b_not_omega4 = false;
        cmd->ref_picture_buffer[i].samples.stride = dbm->ComponentStride(BufferState[CurrentDecodeBufferIndex].Buffer, DecodeComponent, 0, 0);
#endif
    }

    NumRefPic = 0;

    for (i = 0; i < DecodeContext->ReferenceFrameList[REF_PIC_LIST_0].EntryCount; i++)
    {
        // Is reference picture already in array ?
        for (j = 0; j < NumRefPic; j++)
            if (cmd->ref_picture_info[j].poc == DecodeContext->ReferenceFrameList[REF_PIC_LIST_0].ReferenceDetails[i].PicOrderCnt)
            {
                break;
            }

        cmd->initial_ref_pic_list_l0[i] = j;

        // Store info if this is a new reference picture
        if (j == NumRefPic)
        {
            unsigned int BufferIndex = DecodeContext->ReferenceFrameList[REF_PIC_LIST_0].EntryIndicies[i];
            unsigned int luma_size   = dbm->LumaSize(BufferState[BufferIndex].Buffer, DecodeComponent);
            unsigned int chroma_size = dbm->ChromaSize(BufferState[BufferIndex].Buffer, DecodeComponent);
            unsigned int ppb_size    = dbm->ComponentSize(BufferState[BufferIndex].Buffer, VideoMacroblockStructure);
            /* For interlaced streams, the Reference Buffer Structure is:
             *    PPB buffer structure:         PPBTop  PPBBottom
             * Cannes 2
             *    DecodeCopy buffer structure: LumaTop  LumaBottom  ChromaTop ChromaBottom
             * Cannes 2.5
             *    DecodeCopy buffer structure: LumaTop  ChromaTop   LumaBottom ChromaBottom */
            cmd->ref_picture_buffer[j].samples.luma_offset   = (uint32_t) dbm->Luma(BufferState[BufferIndex].Buffer, DecodeComponent);
            cmd->ref_picture_buffer[j].samples.chroma_offset = (uint32_t) dbm->Chroma(BufferState[BufferIndex].Buffer, DecodeComponent);
            cmd->ref_picture_buffer[j].ppb_offset = (uint32_t) dbm->ComponentBaseAddress(BufferState[BufferIndex].Buffer, VideoMacroblockStructure);

#if !defined (HEVC_HADES_CANNES25) && !defined (HEVC_HADES_CANNESWIFI)
            if (DecodeContext->ReferenceFrameList[REF_PIC_LIST_0].ReferenceDetails[i].IsBottomField)
            {
                /* Offset for Bottom Field */
                cmd->ref_picture_buffer[j].samples.luma_offset   += (luma_size / 2);
                cmd->ref_picture_buffer[j].samples.chroma_offset += (chroma_size / 2);
                cmd->ref_picture_buffer[j].ppb_offset            += (ppb_size / 2);
            }
            cmd->ref_picture_buffer[j].samples.stride = dbm->ComponentStride(BufferState[BufferIndex].Buffer, DecodeComponent, 0, 0);
            cmd->ref_picture_buffer[j].samples.raster2b_not_omega4 = false;
#else
            if (DecodeContext->ReferenceFrameList[REF_PIC_LIST_0].ReferenceDetails[i].IsBottomField)
            {
                /* Offset for Bottom Field */
                // Ref/Display/Decode buffer format is raster NV12 so next field will start at buffer stride apart from
                // start of first field for both luma and chroma.
                if (RefBufferType == PolicyValueRasterBuffOnly)
                {
                    cmd->ref_picture_buffer[j].samples.luma_offset   += width_offset;
                    cmd->ref_picture_buffer[j].samples.chroma_offset += width_offset;
                }
                else
                {
                    cmd->ref_picture_buffer[j].samples.luma_offset   += ((luma_size + chroma_size) / 2);
                }
                cmd->ref_picture_buffer[j].ppb_offset            += (ppb_size / 2);
            }
#endif
            if ((cmd->ref_picture_buffer[j].samples.luma_offset != 0) && (cmd->ref_picture_buffer[j].samples.chroma_offset != 0))
            {
                cmd->ref_picture_buffer[j].enable_flag = true;
                cmd->ref_picture_info[j].non_existing_flag = 0;
            }
            else
            {
                SE_WARNING("New reference buffer has either or both luma and chroma pointers NULL. Marking ref[%d] as unavailable\n", j);
                cmd->ref_picture_buffer[j].enable_flag = false;
                cmd->ref_picture_info[j].non_existing_flag = 1;
            }
            cmd->ref_picture_info[j].long_term_flag = DecodeContext->ReferenceFrameList[REF_PIC_LIST_0].ReferenceDetails[i].LongTermReference;
            cmd->ref_picture_info[j].poc = DecodeContext->ReferenceFrameList[REF_PIC_LIST_0].ReferenceDetails[i].PicOrderCnt;

            SE_VERBOSE(group_decoder_video, "DEC %d: refpic %d L    %08x -> %08x\n",
                       ParsedFrameParameters->DecodeFrameIndex, j,
                       cmd->ref_picture_buffer[j].samples.luma_offset,
                       cmd->ref_picture_buffer[j].samples.luma_offset +
                       (uint32_t) dbm->LumaSize(BufferState[BufferIndex].Buffer, DecodeComponent));
            SE_VERBOSE(group_decoder_video, "DEC %d: refpic %d C    %08x -> %08x\n",
                       ParsedFrameParameters->DecodeFrameIndex, j,
                       cmd->ref_picture_buffer[j].samples.chroma_offset,
                       cmd->ref_picture_buffer[j].samples.chroma_offset +
                       (uint32_t) dbm->ChromaSize(BufferState[BufferIndex].Buffer, DecodeComponent));
            SE_VERBOSE(group_decoder_video, "DEC %d: refpic %d PPB  %08x -> %08x\n",
                       ParsedFrameParameters->DecodeFrameIndex, j,
                       cmd->ref_picture_buffer[j].ppb_offset,
                       cmd->ref_picture_buffer[j].ppb_offset +
                       (uint32_t) dbm->ComponentSize(BufferState[BufferIndex].Buffer, VideoMacroblockStructure));
            NumRefPic++;
        }
    }

    // initial_ref_pic_list_l0 is filled up by repeating the reference indexes (see standard section 8.3.4)
    if (DecodeContext->ReferenceFrameList[REF_PIC_LIST_0].EntryCount != 0)
        for (i = DecodeContext->ReferenceFrameList[REF_PIC_LIST_0].EntryCount; i < HEVC_MAX_REFERENCE_INDEX; i++)
        {
            cmd->initial_ref_pic_list_l0[i] = cmd->initial_ref_pic_list_l0[i % DecodeContext->ReferenceFrameList[REF_PIC_LIST_0].EntryCount];
        }

    for (i = 0; i < DecodeContext->ReferenceFrameList[REF_PIC_LIST_1].EntryCount; i++)
    {
        // Is reference picture already in array ?
        for (j = 0; j < NumRefPic; j++)
            if (cmd->ref_picture_info[j].poc == DecodeContext->ReferenceFrameList[REF_PIC_LIST_1].ReferenceDetails[i].PicOrderCnt)
            {
                break;
            }

        cmd->initial_ref_pic_list_l1[i] = j;

        // Store info if this is a new reference picture
        if (j == NumRefPic)
        {
            unsigned int BufferIndex = DecodeContext->ReferenceFrameList[REF_PIC_LIST_1].EntryIndicies[i];
            unsigned int luma_size   = dbm->LumaSize(BufferState[BufferIndex].Buffer, DecodeComponent);
            unsigned int chroma_size = dbm->ChromaSize(BufferState[BufferIndex].Buffer, DecodeComponent);
            unsigned int ppb_size    = dbm->ComponentSize(BufferState[BufferIndex].Buffer, VideoMacroblockStructure);

            cmd->ref_picture_buffer[j].samples.luma_offset = (uint32_t) dbm->Luma(BufferState[BufferIndex].Buffer, DecodeComponent);
            cmd->ref_picture_buffer[j].samples.chroma_offset = (uint32_t) dbm->Chroma(BufferState[BufferIndex].Buffer, DecodeComponent);
            cmd->ref_picture_buffer[j].ppb_offset = (uint32_t) dbm->ComponentBaseAddress(BufferState[BufferIndex].Buffer, VideoMacroblockStructure);

#if !defined (HEVC_HADES_CANNES25) && !defined (HEVC_HADES_CANNESWIFI)
            if (DecodeContext->ReferenceFrameList[REF_PIC_LIST_1].ReferenceDetails[i].IsBottomField)
            {
                /* Offset for Bottom Field */
                cmd->ref_picture_buffer[j].samples.luma_offset   += (luma_size / 2);
                cmd->ref_picture_buffer[j].samples.chroma_offset += (chroma_size / 2);
                cmd->ref_picture_buffer[j].ppb_offset            += (ppb_size / 2);
            }
            cmd->ref_picture_buffer[j].samples.stride = dbm->ComponentStride(BufferState[BufferIndex].Buffer, DecodeComponent, 0, 0);
            cmd->ref_picture_buffer[j].samples.raster2b_not_omega4 = false;
#else
            if (DecodeContext->ReferenceFrameList[REF_PIC_LIST_1].ReferenceDetails[i].IsBottomField)
            {
                /* Offset for Bottom Field */
                // Ref/Display/Decode buffer format is raster NV12 so next field will start at buffer stride apart from
                // start of first field for both luma and chroma.
                if (RefBufferType == PolicyValueRasterBuffOnly)
                {
                    cmd->ref_picture_buffer[j].samples.luma_offset += width_offset;
                    cmd->ref_picture_buffer[j].samples.chroma_offset += width_offset;
                }
                else
                {
                    cmd->ref_picture_buffer[j].samples.luma_offset   += ((luma_size + chroma_size) / 2);
                }
                cmd->ref_picture_buffer[j].ppb_offset            += (ppb_size / 2);
            }
#endif
            if ((cmd->ref_picture_buffer[j].samples.luma_offset != 0) && (cmd->ref_picture_buffer[j].samples.chroma_offset != 0))
            {
                cmd->ref_picture_buffer[j].enable_flag = true;
                cmd->ref_picture_info[j].non_existing_flag = 0;
            }
            else
            {
                SE_WARNING("New reference buffer has either or both luma and chroma pointers NULL. Marking ref[%d] as unavailable\n", j);
                cmd->ref_picture_buffer[j].enable_flag = false;
                cmd->ref_picture_info[j].non_existing_flag = 1;
            }

            cmd->ref_picture_info[j].long_term_flag = DecodeContext->ReferenceFrameList[REF_PIC_LIST_1].ReferenceDetails[i].LongTermReference;
            cmd->ref_picture_info[j].poc = DecodeContext->ReferenceFrameList[REF_PIC_LIST_1].ReferenceDetails[i].PicOrderCnt;
            SE_VERBOSE(group_decoder_video, "DEC %d: refpic %d L     %08x -> %08x\n",
                       ParsedFrameParameters->DecodeFrameIndex, j, cmd->ref_picture_buffer[j].samples.luma_offset,
                       cmd->ref_picture_buffer[j].samples.luma_offset + (uint32_t) dbm->LumaSize(BufferState[BufferIndex].Buffer, DecodeComponent));
            SE_VERBOSE(group_decoder_video, "DEC %d: refpic %d C     %08x -> %08x\n",
                       ParsedFrameParameters->DecodeFrameIndex, j, cmd->ref_picture_buffer[j].samples.chroma_offset,
                       cmd->ref_picture_buffer[j].samples.chroma_offset + (uint32_t) dbm->ChromaSize(BufferState[BufferIndex].Buffer, DecodeComponent));
            SE_VERBOSE(group_decoder_video, "DEC %d: refpic %d PPB   %08x -> %08x\n",
                       ParsedFrameParameters->DecodeFrameIndex, j, cmd->ref_picture_buffer[j].ppb_offset,
                       cmd->ref_picture_buffer[j].ppb_offset + (uint32_t) dbm->ComponentSize(BufferState[BufferIndex].Buffer, VideoMacroblockStructure));
            NumRefPic++;
        }
    }

    // initial_ref_pic_list_l1 is filled up by repeating the reference indexes (see standard section 8.3.4)
    if (DecodeContext->ReferenceFrameList[REF_PIC_LIST_1].EntryCount != 0)
        for (i = DecodeContext->ReferenceFrameList[REF_PIC_LIST_1].EntryCount; i < HEVC_MAX_REFERENCE_INDEX; i++)
        {
            cmd->initial_ref_pic_list_l1[i] = cmd->initial_ref_pic_list_l1[i % DecodeContext->ReferenceFrameList[REF_PIC_LIST_1].EntryCount];
        }

    // The preprocessor is generating ctb commands for decode using
    // ReferenceFrameList[REF_PIC_LIST_0].EntryCount number of
    // references so same number of references must be sent to hades
    // to avoid firmware crash
    if (DecodeContext->ReferenceFrameList[REF_PIC_LIST_0].EntryCount != 0)
    {
        cmd->num_reference_pictures = DecodeContext->ReferenceFrameList[REF_PIC_LIST_0].EntryCount;
    }
    else
    {
        // We don't know how many references are actually needed to
        // decode the frame so we put default value to avoid firmware hang
        if (ParsedFrameParameters->PictureHasMissingRef)
        {
            cmd->num_reference_pictures = HEVC_DEFAULT_REFERENCE_PICTURES;
        }
        else
        {
            // There are no valid references and there are no missing
            // references as well. This normally does not happen in P
            // and B pictures so set default value for them to avoid
            // firmware hang. Set value 0 for I (no references).
            if (ParsedVideoParameters->SliceType != SliceTypeI)
            {
                cmd->num_reference_pictures = HEVC_DEFAULT_REFERENCE_PICTURES;
            }
            else
            {
                cmd->num_reference_pictures = 0;
            }
        }
    }

    // Find best reference for ERC emulation
    for (i = 0; i < NumRefPic; i++)
    {
        abs_diff_poc = (cmd->ref_picture_info[i].poc >= sliceheader->PicOrderCnt) ?
                       cmd->ref_picture_info[i].poc - sliceheader->PicOrderCnt :
                       sliceheader->PicOrderCnt - cmd->ref_picture_info[i].poc;

        if (i == 0)
        {
            min_abs_diff_poc = abs_diff_poc;
        }
        else if (abs_diff_poc < min_abs_diff_poc)
        {
            min_abs_diff_poc = abs_diff_poc;
            cmd->erc_ref_pic_idx = i;
        }
    }

    // Current picture
    if (RefBufferType == PolicyValueRasterBuffOnly)
    {
        // always enable RCN
        cmd->curr_picture_buffer.enable_flag = true;
    }
    else
    {
        // enable Omega RCN only if the picture is a reference
        cmd->curr_picture_buffer.enable_flag = sliceheader->reference_flag ||
                                               ((sps->highest_tid > 0) ? ((unsigned int) sliceheader->temporal_id < (unsigned int) sps->highest_tid) : false);
    }

    /* Frame or Top Field */
    cmd->curr_picture_buffer.samples.luma_offset = (uint32_t) Stream->GetDecodeBufferManager()->Luma(BufferState[CurrentDecodeBufferIndex].Buffer, DecodeComponent);
    cmd->curr_picture_buffer.samples.chroma_offset = (uint32_t) Stream->GetDecodeBufferManager()->Chroma(BufferState[CurrentDecodeBufferIndex].Buffer, DecodeComponent);
    cmd->curr_picture_buffer.ppb_offset = (uint32_t) Stream->GetDecodeBufferManager()->ComponentBaseAddress(BufferState[CurrentDecodeBufferIndex].Buffer, VideoMacroblockStructure);

    unsigned int luma_size   = Stream->GetDecodeBufferManager()->LumaSize(BufferState[CurrentDecodeBufferIndex].Buffer, DecodeComponent);
    unsigned int chroma_size = Stream->GetDecodeBufferManager()->ChromaSize(BufferState[CurrentDecodeBufferIndex].Buffer, DecodeComponent);
    unsigned int ppb_size    = Stream->GetDecodeBufferManager()->ComponentSize(BufferState[CurrentDecodeBufferIndex].Buffer, VideoMacroblockStructure);
#if !defined (HEVC_HADES_CANNES25) && !defined (HEVC_HADES_CANNESWIFI)
    if (ParsedVideoParameters->PictureStructure == StructureBottomField)
    {
        /* Offset for Bottom Field */
        cmd->curr_picture_buffer.samples.luma_offset += (luma_size / 2);
        cmd->curr_picture_buffer.samples.chroma_offset += (chroma_size / 2);
        cmd->curr_picture_buffer.ppb_offset += (ppb_size / 2);
    }
    cmd->curr_picture_buffer.samples.raster2b_not_omega4 = false;
    cmd->curr_picture_buffer.samples.stride = Stream->GetDecodeBufferManager()->ComponentStride(BufferState[CurrentDecodeBufferIndex].Buffer, DecodeComponent, 0, 0);
#else
    if (ParsedVideoParameters->PictureStructure == StructureBottomField)
    {
        /* Offset for Bottom Field */
        // Ref/Display/Decode buffer format is raster NV12 so next field will start at buffer stride apart from
        // start of first field for both luma and chroma.
        if (RefBufferType == PolicyValueRasterBuffOnly)
        {
            cmd->curr_picture_buffer.samples.luma_offset += width_offset;
            cmd->curr_picture_buffer.samples.chroma_offset +=  width_offset;
        }
        else
        {
            cmd->curr_picture_buffer.samples.luma_offset += ((luma_size + chroma_size) / 2);
        }
        cmd->curr_picture_buffer.ppb_offset          += (ppb_size / 2);
    }
#endif

    cmd->curr_picture_info.non_existing_flag = 0;
    cmd->curr_picture_info.long_term_flag = 0;
    cmd->curr_picture_info.poc = sliceheader->PicOrderCnt;
    SE_VERBOSE(group_decoder_video, "DEC %d: picbuf L      %08x -> %08x\n", ParsedFrameParameters->DecodeFrameIndex, cmd->curr_picture_buffer.samples.luma_offset,
               cmd->curr_picture_buffer.samples.luma_offset + (uint32_t) Stream->GetDecodeBufferManager()->LumaSize(BufferState[CurrentDecodeBufferIndex].Buffer, DecodeComponent));
    SE_VERBOSE(group_decoder_video, "DEC %d: picbuf C      %08x -> %08x\n", ParsedFrameParameters->DecodeFrameIndex, cmd->curr_picture_buffer.samples.chroma_offset,
               cmd->curr_picture_buffer.samples.chroma_offset + (uint32_t) Stream->GetDecodeBufferManager()->ChromaSize(BufferState[CurrentDecodeBufferIndex].Buffer, DecodeComponent));
    SE_VERBOSE(group_decoder_video, "DEC %d: picbuf PPB    %08x -> %08x\n", ParsedFrameParameters->DecodeFrameIndex, cmd->curr_picture_buffer.ppb_offset,
               cmd->curr_picture_buffer.ppb_offset + (uint32_t) Stream->GetDecodeBufferManager()->ComponentSize(BufferState[CurrentDecodeBufferIndex].Buffer, VideoMacroblockStructure));

    //=================== Missing Reference Frame Substitution =====================
    unsigned int erc_poc_idx = cmd->erc_ref_pic_idx;

    // I/IDR Emulation: If the frame is an I/IDR frame and it is not the first frame then
    // last reference picture must be sent to firmware. This picture will be used for
    // emulation in case I/IDR frame is erroneous.
    if (ParsedVideoParameters->SliceType == SliceTypeI)
    {
        if (mSavedReferenceForSubstitutionCanBeUsed)
        {
            // Use the last saved reference for concealment
            SubstituteSavedPreviousReference(cmd, erc_poc_idx);
            cmd->num_reference_pictures = 1;
        }
    }

    // The current picture has only one reference and it is not I frame and has decode index > 0
    if ((cmd->num_reference_pictures == 1) && (ParsedVideoParameters->SliceType != SliceTypeI) && (CurrentDecodeBufferIndex > 0))
    {
        // ref_pic_buf.ppb_offset == curr_pic_buf.pic ppb_offset implies that this is case of missing reference
        if ((cmd->ref_picture_buffer[erc_poc_idx].ppb_offset == cmd->curr_picture_buffer.ppb_offset) && mSavedReferenceForSubstitutionCanBeUsed)
        {
            // Use the last saved reference for concealment
            SubstituteSavedPreviousReference(cmd, erc_poc_idx);
        }
    }

    // The current picture has more than one reference and it is not I frame
    if ((cmd->num_reference_pictures > 1) && (ParsedVideoParameters->SliceType != SliceTypeI))
    {
        for (i = 0; i < cmd->num_reference_pictures; i++)
        {
            // ref_pic_buf.ppb_offset == curr_pic_buf.pic ppb_offset implies that this is case of missing reference
            if (cmd->ref_picture_buffer[i].ppb_offset == cmd->curr_picture_buffer.ppb_offset)
            {
                if (i == 0)
                {
                    if (mSavedReferenceForSubstitutionCanBeUsed)
                    {
                        // Use the last saved reference for concealment
                        SubstituteSavedPreviousReference(cmd, i);
                    }
                }
                else
                {
                    // Use the immediate last one existed
                    cmd->ref_picture_buffer[i].samples.luma_offset   = cmd->ref_picture_buffer[i - 1].samples.luma_offset;
                    cmd->ref_picture_buffer[i].samples.chroma_offset = cmd->ref_picture_buffer[i - 1].samples.chroma_offset;
                    cmd->ref_picture_buffer[i].ppb_offset = cmd->ref_picture_buffer[i - 1].ppb_offset;
#if !defined (HEVC_HADES_CANNES25) && !defined (HEVC_HADES_CANNESWIFI)
                    cmd->ref_picture_buffer[i].samples.stride = cmd->ref_picture_buffer[i - 1].samples.stride;
                    cmd->ref_picture_buffer[i].samples.raster2b_not_omega4 = false;
#endif
                    if ((cmd->ref_picture_buffer[i].samples.luma_offset != 0) && (cmd->ref_picture_buffer[i].samples.chroma_offset != 0))
                    {
                        cmd->ref_picture_buffer[i].enable_flag = true;
                        cmd->ref_picture_info[i].non_existing_flag = 0;
                    }
                    else
                    {
                        // In case frame is freed ask firmware not to use it.
                        cmd->ref_picture_buffer[i].enable_flag = false;
                        cmd->ref_picture_info[i].non_existing_flag = 1;
                    }
                    cmd->ref_picture_info[i].long_term_flag = cmd->ref_picture_info[i - 1].long_term_flag;
                }
            }
        }
    }

    // Save latest reference frame for concealment
    if (ParsedFrameParameters->ReferenceFrame == 1)
    {
        mSavedReferenceForSubstitutionCanBeUsed = true;
        mSavedReferenceForSubstitution.samples.luma_offset = cmd->curr_picture_buffer.samples.luma_offset;
        mSavedReferenceForSubstitution.samples.chroma_offset = cmd->curr_picture_buffer.samples.chroma_offset;
        mSavedReferenceForSubstitution.ppb_offset = cmd->curr_picture_buffer.ppb_offset;
#if !defined (HEVC_HADES_CANNES25) && !defined (HEVC_HADES_CANNESWIFI)
        mSavedReferenceForSubstitution.samples.stride = cmd->curr_picture_buffer.samples.stride;
#endif
    }

    // Display buffer
    // For SoCs with raster only output (like Cwifi, L2A and L2B) on both decode and display components,
    // programming of curr_picture_buffer is sufficient. Hence marking the curr_display_buffer.enable_flag false for such SoCs.
    cmd->curr_display_buffer.enable_flag = sliceheader->output_flag && (RefBufferType != PolicyValueRasterBuffOnly);
    cmd->curr_display_buffer.resize_flag = 0;
#if defined (HEVC_HADES_CANNES25) || defined (HEVC_HADES_CANNESWIFI)
    cmd->curr_display_buffer.convert_10to8_flag = 0; // 10 bit input -> 10 bit output on main
    cmd->curr_display_buffer.stride = Stream->GetDecodeBufferManager()->ComponentStride(
                                          BufferState[CurrentDecodeBufferIndex].Buffer,
                                          PrimaryManifestationComponent,
                                          0, 0);
    width_offset = cmd->curr_display_buffer.stride;
    if (ParsedVideoParameters->PictureStructure != StructureFrame)
    {
        cmd->curr_display_buffer.stride *= 2; // Double the stride for interlaced video
    }
#else
    cmd->curr_display_buffer.samples.raster2b_not_omega4 = true;
    cmd->curr_display_buffer.samples.stride = Stream->GetDecodeBufferManager()->ComponentStride(
                                                  BufferState[CurrentDecodeBufferIndex].Buffer,
                                                  PrimaryManifestationComponent,
                                                  0, 0);
    width_offset = cmd->curr_display_buffer.samples.stride;
    if (ParsedVideoParameters->PictureStructure != StructureFrame)
    {
        cmd->curr_display_buffer.samples.stride *= 2; // Double the stride for interlaced video
    }
#endif
    /* Frame or Top Field */
    cmd->curr_display_buffer.samples.luma_offset = (uint32_t) Stream->GetDecodeBufferManager()->Luma(BufferState[CurrentDecodeBufferIndex].Buffer, PrimaryManifestationComponent);
    cmd->curr_display_buffer.samples.chroma_offset = (uint32_t) Stream->GetDecodeBufferManager()->Chroma(BufferState[CurrentDecodeBufferIndex].Buffer, PrimaryManifestationComponent);
    if (ParsedVideoParameters->PictureStructure == StructureBottomField)
    {
        /* Offset for Bottom Field */
        cmd->curr_display_buffer.samples.luma_offset +=  width_offset;
        cmd->curr_display_buffer.samples.chroma_offset += width_offset;
    }
    SE_VERBOSE(group_decoder_video, "DEC %d: dispbuf L     %08x -> %08x\n", ParsedFrameParameters->DecodeFrameIndex, cmd->curr_display_buffer.samples.luma_offset,
               cmd->curr_display_buffer.samples.luma_offset + (uint32_t) Stream->GetDecodeBufferManager()->LumaSize(BufferState[CurrentDecodeBufferIndex].Buffer, PrimaryManifestationComponent));
    SE_VERBOSE(group_decoder_video, "DEC %d: dispbuf C     %08x -> %08x\n", ParsedFrameParameters->DecodeFrameIndex, cmd->curr_display_buffer.samples.chroma_offset,
               cmd->curr_display_buffer.samples.chroma_offset + (uint32_t) Stream->GetDecodeBufferManager()->ChromaSize(BufferState[CurrentDecodeBufferIndex].Buffer, PrimaryManifestationComponent));

    Context->DecodePictureStructure = ParsedVideoParameters->PictureStructure;

    // Resize buffer
    if (Stream->GetDecodeBufferManager()->ComponentPresent(BufferState[CurrentDecodeBufferIndex].Buffer, DecimatedManifestationComponent))
    {
        unsigned int hdec = Stream->GetDecodeBufferManager()->DecimationFactor(BufferState[CurrentDecodeBufferIndex].Buffer, 0);
        unsigned int vdec = Stream->GetDecodeBufferManager()->DecimationFactor(BufferState[CurrentDecodeBufferIndex].Buffer, 1);

#if defined (HEVC_HADES_CANNES25) || defined (HEVC_HADES_CANNESWIFI)
        cmd->curr_resize_buffer.enable_flag = 1;
        cmd->curr_resize_buffer.resize_flag = 1;
#ifdef HEVC_HADES_CANNESWIFI
        // Decimated output must be in 8 bits for Cannes Wifi/L2A (until display support 10 bit decimated output)
        cmd->curr_resize_buffer.convert_10to8_flag = 1;
#else
        cmd->curr_resize_buffer.convert_10to8_flag = 0;
#endif
        cmd->curr_resize_buffer.stride = Stream->GetDecodeBufferManager()->ComponentStride(
                                             BufferState[CurrentDecodeBufferIndex].Buffer,
                                             DecimatedManifestationComponent,
                                             0, 0);
        width_offset = cmd->curr_resize_buffer.stride;
        if (ParsedVideoParameters->PictureStructure != StructureFrame)
        {
            cmd->curr_resize_buffer.stride *= 2;
        }
#else
        // forced enable_flag: not using cmd->curr_display_buffer.enable_flag;
        cmd->curr_resize_buffer.enable_flag = true; // JLX DEBUG SIRON: VSOC 2.1.9 model hangs when decimation is enabled
        cmd->curr_resize_buffer.resize_flag = true;
        cmd->curr_resize_buffer.samples.raster2b_not_omega4 = true;
        cmd->curr_resize_buffer.samples.stride = Stream->GetDecodeBufferManager()->ComponentStride(
                                                     BufferState[CurrentDecodeBufferIndex].Buffer,
                                                     DecimatedManifestationComponent,
                                                     0, 0);
        width_offset = cmd->curr_resize_buffer.samples.stride;
        if (ParsedVideoParameters->PictureStructure != StructureFrame)
        {
            cmd->curr_resize_buffer.samples.stride *= 2;
        }
#endif
        /* Frame or Top Field */
        cmd->curr_resize_buffer.samples.luma_offset = (uint32_t) Stream->GetDecodeBufferManager()->Luma(BufferState[CurrentDecodeBufferIndex].Buffer, DecimatedManifestationComponent);
        cmd->curr_resize_buffer.samples.chroma_offset = (uint32_t) Stream->GetDecodeBufferManager()->Chroma(BufferState[CurrentDecodeBufferIndex].Buffer, DecimatedManifestationComponent);
        if ((ParsedVideoParameters->PictureStructure == StructureBottomField))
        {
            /* Bottom Field */
            cmd->curr_resize_buffer.samples.luma_offset   +=  width_offset;
            cmd->curr_resize_buffer.samples.chroma_offset +=  width_offset;
        }

        SE_VERBOSE(group_decoder_video, "DEC %d: rszbuf L      %08x -> %08x\n",
                   ParsedFrameParameters->DecodeFrameIndex,
                   cmd->curr_resize_buffer.samples.luma_offset,
                   cmd->curr_resize_buffer.samples.luma_offset +
                   (uint32_t) Stream->GetDecodeBufferManager()->LumaSize(BufferState[CurrentDecodeBufferIndex].Buffer, DecimatedManifestationComponent));
        SE_VERBOSE(group_decoder_video, "DEC %d: rszbuf C      %08x -> %08x\n",
                   ParsedFrameParameters->DecodeFrameIndex,
                   cmd->curr_resize_buffer.samples.chroma_offset,
                   cmd->curr_resize_buffer.samples.chroma_offset +
                   (uint32_t) Stream->GetDecodeBufferManager()->ChromaSize(BufferState[CurrentDecodeBufferIndex].Buffer, DecimatedManifestationComponent));

        // Not all these decimations factors are supported by the Streaming Engine
        if (vdec == 2)
        {
            cmd->v_dec_factor = 1;
        }
        else if (vdec == 4)
        {
            cmd->v_dec_factor = 2;
        }
        else if (vdec == 8)
        {
            cmd->v_dec_factor = 3;
        }
        else
        {
            cmd->v_dec_factor = 0;
        }

        if (hdec == 2)
        {
            cmd->h_dec_factor = 1;
        }
        else if (hdec == 4)
        {
            cmd->h_dec_factor = 2;
        }
        else if (hdec == 8)
        {
            cmd->h_dec_factor = 3;
        }
        else
        {
            cmd->h_dec_factor = 0;
        }

        // Default resize coefficients
        cmd->luma_resize_param.v_dec_coeff.round = 128;
        cmd->luma_resize_param.v_dec_coeff.coeff[0] = 128;
        cmd->luma_resize_param.v_dec_coeff.coeff[1] = 128;
        cmd->luma_resize_param.v_dec_coeff.coeff[2] = 0;
        cmd->luma_resize_param.v_dec_coeff.coeff[3] = 0;
        cmd->luma_resize_param.v_dec_coeff.coeff[4] = 0;
        cmd->luma_resize_param.v_dec_coeff.coeff[5] = 0;
        cmd->luma_resize_param.v_dec_coeff.coeff[6] = 0;
        cmd->luma_resize_param.v_dec_coeff.coeff[7] = 0;
        cmd->luma_resize_param.h_flt_coeff.round = 128;
        cmd->luma_resize_param.h_flt_coeff.coeff[0] = 8;
        cmd->luma_resize_param.h_flt_coeff.coeff[1] = -14;
        cmd->luma_resize_param.h_flt_coeff.coeff[2] = 18;
        cmd->luma_resize_param.h_flt_coeff.coeff[3] = 236;
        cmd->luma_resize_param.h_flt_coeff.coeff[4] = 18;
        cmd->luma_resize_param.h_flt_coeff.coeff[5] = -14;
        cmd->luma_resize_param.h_flt_coeff.coeff[6] = 8;
        cmd->luma_resize_param.h_flt_coeff.coeff[7] = -4;
        cmd->chroma_resize_param.v_dec_coeff.round = 128;
        cmd->chroma_resize_param.v_dec_coeff.coeff[0] = 128;
        cmd->chroma_resize_param.v_dec_coeff.coeff[1] = 128;
        cmd->chroma_resize_param.v_dec_coeff.coeff[2] = 0;
        cmd->chroma_resize_param.v_dec_coeff.coeff[3] = 0;
        cmd->chroma_resize_param.v_dec_coeff.coeff[4] = 0;
        cmd->chroma_resize_param.v_dec_coeff.coeff[5] = 0;
        cmd->chroma_resize_param.v_dec_coeff.coeff[6] = 0;
        cmd->chroma_resize_param.v_dec_coeff.coeff[7] = 0;
        cmd->chroma_resize_param.h_flt_coeff.round = 128;
        cmd->chroma_resize_param.h_flt_coeff.coeff[0] = 8;
        cmd->chroma_resize_param.h_flt_coeff.coeff[1] = -14;
        cmd->chroma_resize_param.h_flt_coeff.coeff[2] = 18;
        cmd->chroma_resize_param.h_flt_coeff.coeff[3] = 236;
        cmd->chroma_resize_param.h_flt_coeff.coeff[4] = 18;
        cmd->chroma_resize_param.h_flt_coeff.coeff[5] = -14;
        cmd->chroma_resize_param.h_flt_coeff.coeff[6] = 8;
        cmd->chroma_resize_param.h_flt_coeff.coeff[7] = -4;
    }
    else
    {
#if defined(HEVC_HADES_CANNES25) || defined (HEVC_HADES_CANNESWIFI)
        cmd->curr_resize_buffer.convert_10to8_flag = 0;
#else
        cmd->curr_resize_buffer.samples.raster2b_not_omega4 = 0;
        cmd->curr_resize_buffer.samples.stride = 0;
#endif
        cmd->curr_resize_buffer.samples.luma_offset = 0;
        cmd->curr_resize_buffer.samples.chroma_offset = 0;
        cmd->v_dec_factor = 0;
        cmd->h_dec_factor = 0;
        cmd->curr_resize_buffer.enable_flag = 0;
        cmd->curr_resize_buffer.resize_flag = 0;
    }
    // TODO prog task timeout (unit: 4096 ticks on CLK_FC_HADES at 332.9Mhz ~= 12us)
    cmd->time_out_in_tick = 4052; /* 50 ms FC Timeout */

    intermediate_buffer_t *ib = &cmd->intermediate_buffer;
    // Intermediate buffers
    ib->slice_table_base_addr = (uint32_t) InputBuffer[HEVC_SLICE_TABLE];
    ib->slice_table_length = (uint32_t) InputBufferSize[HEVC_SLICE_TABLE];
    ib->ctb_table_base_addr = (uint32_t) InputBuffer[HEVC_CTB_TABLE];
    ib->ctb_table_length = (uint32_t) InputBufferSize[HEVC_CTB_TABLE];
    ib->slice_headers_base_addr = (uint32_t) InputBuffer[HEVC_SLICE_HEADERS];
    ib->slice_headers_length = (uint32_t) InputBufferSize[HEVC_SLICE_HEADERS];
    ib->ctb_commands.base_addr = (uint32_t) InputBuffer[HEVC_CTB_COMMANDS];
    ib->ctb_commands.length = (uint32_t) InputBufferSize[HEVC_CTB_COMMANDS];
    ib->ctb_commands.end_offset = (uint32_t) InputBufferSize[HEVC_CTB_COMMANDS];
    ib->ctb_commands.start_offset = 0;
    ib->ctb_residuals.base_addr = (uint32_t) InputBuffer[HEVC_CTB_RESIDUALS];
    ib->ctb_residuals.length = (uint32_t) InputBufferSize[HEVC_CTB_RESIDUALS];
    ib->ctb_residuals.end_offset = (uint32_t) InputBufferSize[HEVC_CTB_RESIDUALS];
    ib->ctb_residuals.start_offset = 0;

    cmd->slice_table_entries = ((HevcFrameParameters_t *)ParsedFrameParameters->FrameParameterStructure)->SliceTableEntries;
    SE_VERBOSE(group_decoder_video, "DEC %d: slice_table   %08x -> %08x\n",
               ParsedFrameParameters->DecodeFrameIndex, ib->slice_table_base_addr,
               ib->slice_table_base_addr +   ib->slice_table_length);
    SE_VERBOSE(group_decoder_video, "DEC %d: ctb_table     %08x -> %08x\n",
               ParsedFrameParameters->DecodeFrameIndex, ib->ctb_table_base_addr,
               ib->ctb_table_base_addr +  ib->ctb_table_length);
    SE_VERBOSE(group_decoder_video, "DEC %d: slice_headers %08x -> %08x\n",
               ParsedFrameParameters->DecodeFrameIndex, ib->slice_headers_base_addr,
               ib->slice_headers_base_addr + ib->slice_headers_length);
    SE_VERBOSE(group_decoder_video, "DEC %d: ctb_commands  %08x -> %08x\n",
               ParsedFrameParameters->DecodeFrameIndex, ib->ctb_commands.base_addr,
               ib->ctb_commands.base_addr +  ib->ctb_commands.length);
    SE_VERBOSE(group_decoder_video, "DEC %d: ctb_residuals %08x -> %08x\n",
               ParsedFrameParameters->DecodeFrameIndex, ib->ctb_residuals.base_addr,
               ib->ctb_residuals.base_addr + ib->ctb_residuals.length);

    // MCC programming
    cmd->mcc_mode   = HEVC_MCC_MODE_CACHE;
    cmd->mcc_opcode = HEVC_MCC_OPCODE_32;

    // Secure mode
#if defined (HEVC_HADES_CANNES25)
    cmd->sop_flag = 0;
#endif

    cmd->decoding_mode = HEVC_ERC_MAX_DECODE;

    // PPS & SPS elements
    cmd->first_picture_in_sequence_flag = !sps->is_active;
    sps->is_active = true;
    cmd->level_idc = (sps->level_idc != 0) ? sps->level_idc : HADES_MAX_LEVEL_IDC;
    cmd->loop_filter_across_tiles_enabled_flag = pps->loop_filter_across_tiles_enabled_flag;
    cmd->additional_flags = HEVC_ADDITIONAL_FLAG_NONE;
    cmd->pcm_loop_filter_disable_flag = sps->pcm_loop_filter_disable_flag;
    cmd->sps_temporal_mvp_enabled_flag = sps->sps_temporal_mvp_enabled_flag;
    cmd->log2_parallel_merge_level_minus2 = pps->log2_parallel_merge_level_minus2;
    cmd->pic_width_in_luma_samples = sps->pic_width_in_luma_samples;
    cmd->pic_height_in_luma_samples = sps->pic_height_in_luma_samples;
#if defined (HEVC_HADES_CANNES25) || defined (HEVC_HADES_CANNESWIFI)
    cmd->bit_depth_luma_minus8 = sps->bit_depth_luma_minus8;
    cmd->bit_depth_chroma_minus8 = sps->bit_depth_chroma_minus8;
#endif

    if (sps->pcm_enabled_flag)
    {
        cmd->pcm_bit_depth_luma_minus1 = sps->pcm_bit_depth_luma_minus1;
        cmd->pcm_bit_depth_chroma_minus1 = sps->pcm_bit_depth_chroma_minus1;
    }
    else
    {
        cmd->pcm_bit_depth_luma_minus1 = 0;
        cmd->pcm_bit_depth_chroma_minus1 = 0;
    }

    cmd->sps_max_dec_pic_buffering_minus1 = sps->sps_max_dec_pic_buffering[sps->highest_tid] - 1;
    cmd->log2_max_coding_block_size_minus3 = sps->log2_max_coding_block_size - 3;
    cmd->scaling_list_enable = sps->scaling_list_enable_flag;
    cmd->transform_skip_enabled_flag = pps->transform_skip_enabled_flag;
    cmd->lists_modification_present_flag = pps->lists_modification_present_flag;
    cmd->num_tile_columns = pps->num_tile_columns;
    cmd->num_tile_rows = pps->num_tile_rows;

    for (i = 0; i < cmd->num_tile_columns; i++)
    {
        cmd->tile_column_width[i] = pps->tile_width[i];
    }

    for (; i < HEVC_MAX_TILE_COLUMNS; i++)
    {
        cmd->tile_column_width[i] = 0;
    }

    for (i = 0; i < cmd->num_tile_rows; i++)
    {
        cmd->tile_row_height[i] = pps->tile_height[i];
    }

    for (; i < HEVC_MAX_TILE_ROWS; i++)
    {
        cmd->tile_row_height[i] = 0;
    }

    //when sign_data_hiding_flag is true, sign_data_hiding_threshold is inferred to 4
    cmd->sign_data_hiding_flag = pps->sign_data_hiding_flag;
    cmd->weighted_pred_flag = pps->weighted_pred_flag[P_SLICE];
    cmd->weighted_bipred_flag = pps->weighted_pred_flag[B_SLICE];
    cmd->constrained_intra_pred_flag = pps->constrained_intra_pred_flag;
    cmd->strong_intra_smoothing_enable_flag = sps->sps_strong_intra_smoothing_enable_flag;
    cmd->pps_cb_qp_offset = pps->pps_cb_qp_offset;
    cmd->pps_cr_qp_offset = pps->pps_cr_qp_offset;
    DumpDecodeParameters(cmd, ParsedFrameParameters->DecodeFrameIndex);

    // Fill out the actual command
    memset(&Context->BaseContext.MMECommand, 0, sizeof(MME_Command_t));
    Context->BaseContext.MMECommand.CmdStatus.AdditionalInfoSize = sizeof(hevcdecpix_status_t);

    Context->BaseContext.MMECommand.CmdStatus.AdditionalInfo_p   = (MME_GenericParams_t)(&Context->frame.status);
    Context->BaseContext.MMECommand.ParamSize                    = sizeof(struct HevcDecodeParameters);
    Context->BaseContext.MMECommand.Param_p                      = (MME_GenericParams_t)(&Context->frame.cmd);

    // Send decode index, IDR flag, poc and cached addresses thru a MME buffer
    // Used by CRC & soft CRC system in the Hades driver
    Context->BaseContext.MMECommand.NumberInputBuffers = 1;
    Context->BaseContext.MMECommand.NumberOutputBuffers = 0;
    Context->BaseContext.MMECommand.DataBuffers_p = Context->BaseContext.MMEBufferList;
    Context->BaseContext.MMEBufferList[0] = &(Context->BaseContext.MMEBuffers[0]);
    Context->BaseContext.MMEBuffers[0].StructSize = sizeof(MME_DataBuffer_t);
    Context->BaseContext.MMEBuffers[0].UserData_p = NULL;
    Context->BaseContext.MMEBuffers[0].Flags = 0;
    Context->BaseContext.MMEBuffers[0].StreamNumber = 0;
    Context->BaseContext.MMEBuffers[0].NumberOfScatterPages = 1;
    Context->BaseContext.MMEBuffers[0].ScatterPages_p = &(Context->BaseContext.MMEPages[0]);
    Context->BaseContext.MMEBuffers[0].StartOffset = 0;
    Context->BaseContext.MMEBuffers[0].TotalSize = sizeof(HevcCodecExtraCRCInfo_t);
    Context->PTS = (int64_t)ParsedFrameParameters->PTS.PtsValue();
    Context->DecodeIndex = ParsedFrameParameters->DecodeFrameIndex;

    FillOutDebugInfo((void *)Context, sliceheader);

    return CodecNoError;
}

void Codec_MmeVideoHevc_c::FillOutDebugInfo(void *context, HevcSliceHeader_t *sliceheader)
{
#ifdef CONFIG_STM_HADES_DEBUG_TOOLS
    HevcCodecDecodeContext_t *ctx = (HevcCodecDecodeContext_t *)context;
    hevcdecpix_transform_param_t *cmd = &ctx->frame.TransformParameters;
    DecodeBufferComponentType_t DecodeComponent = (RefBufferType == PolicyValueRasterBuffOnly) ?
                                                  PrimaryManifestationComponent : VideoDecodeCopy;
    HevcCodecExtraCRCInfo_t *dbg_info = &ctx->ExtraCRCInfo;

    ctx->BaseContext.MMEPages[0].Page_p = dbg_info;
    ctx->BaseContext.MMEPages[0].Size = sizeof(*dbg_info);
    ctx->BaseContext.MMEPages[0].BytesUsed = sizeof(*dbg_info);
    ctx->BaseContext.MMEPages[0].FlagsIn = MME_DATA_PHYSICAL;
    ctx->BaseContext.MMEPages[0].FlagsOut = MME_DATA_PHYSICAL;
    dbg_info->DecodeIndex = ParsedFrameParameters->DecodeFrameIndex;
    dbg_info->IDR = sliceheader->idr_flag;
    dbg_info->poc = sliceheader->PicOrderCnt;
#if defined (HEVC_HADES_CANNES25) || defined (HEVC_HADES_CANNESWIFI)
    dbg_info->bits_per_pixel = ((cmd->ref_format_idc == HEVC_OMEGAH_8) || (cmd->ref_format_idc == HEVC_RASTER2B_8)) ? 8 : 10;
#else
    dbg_info->bits_per_pixel = 8;
#endif
    DecodeBufferManager_t dbm = Stream->GetDecodeBufferManager();
    if (sliceheader->reference_flag)
    {
        dbg_info->omega_luma   = dbm->Luma(BufferState[CurrentDecodeBufferIndex].Buffer, DecodeComponent, CachedAddress);
        dbg_info->omega_chroma = dbm->Chroma(BufferState[CurrentDecodeBufferIndex].Buffer, DecodeComponent, CachedAddress);
        dbg_info->ppb = (uint8_t *)dbm->ComponentBaseAddress(BufferState[CurrentDecodeBufferIndex].Buffer, VideoMacroblockStructure, CachedAddress);
    }
    else
    {
        dbg_info->omega_luma = dbg_info->omega_chroma = dbg_info->ppb = NULL;
    }

    dbg_info->omega_luma_size = ParsedVideoParameters->Content.DecodeWidth * ParsedVideoParameters->Content.DecodeHeight;
    // HEVC_OMEGAH_8
    dbg_info->omega_luma_size += dbg_info->omega_luma_size / 2;
#if defined (HEVC_HADES_CANNES25) || defined (HEVC_HADES_CANNESWIFI)
    if ((cmd->ref_format_idc == HEVC_OMEGAH_10) || (cmd->ref_format_idc == HEVC_RASTER2B_10))
    {
        dbg_info->omega_luma_size *= 2;
    }
#endif
    dbg_info->omega_chroma_size = 0;

    // JLX: PPB size as computed in STHM'
    HevcSequenceParameterSet_t *sps = sliceheader->SequenceParameterSet;
    dbg_info->ppb_size = sps->pic_width_in_ctb * sps->pic_height_in_ctb;
    dbg_info->ppb_size <<= ((sps->log2_max_coding_block_size - 4) << 1);
    dbg_info->ppb_size *= 16; // sizeof (ppb_t)
    dbg_info->raster_luma   = dbm->Luma(BufferState[CurrentDecodeBufferIndex].Buffer, PrimaryManifestationComponent, CachedAddress);
    dbg_info->raster_chroma = dbm->Chroma(BufferState[CurrentDecodeBufferIndex].Buffer, PrimaryManifestationComponent, CachedAddress);
#if defined (HEVC_HADES_CANNES25) || defined (HEVC_HADES_CANNESWIFI)
    dbg_info->raster_stride = cmd->curr_display_buffer.stride;
#else
    dbg_info->raster_stride = dbm->ComponentStride(BufferState[CurrentDecodeBufferIndex].Buffer, PrimaryManifestationComponent, 0, 0);
#endif
    dbg_info->raster_width  = sps->pic_width_in_luma_samples; // ParsedVideoParameters->Content.DisplayWidth;
    dbg_info->raster_height = sps->pic_height_in_luma_samples;// ParsedVideoParameters->Content.DisplayHeight;
#if defined (HEVC_HADES_CANNES25) || defined (HEVC_HADES_CANNESWIFI)
    if (((cmd->ref_format_idc == HEVC_OMEGAH_10) || (cmd->ref_format_idc == HEVC_RASTER2B_10)) &&
        !cmd->curr_display_buffer.convert_10to8_flag)
    {
        dbg_info->raster_width += dbg_info->raster_width / 4;
    }
#endif
    if (dbm->ComponentPresent(BufferState[CurrentDecodeBufferIndex].Buffer, DecimatedManifestationComponent))
    {
        dbg_info->raster_decimated_luma =   dbm->Luma(BufferState[CurrentDecodeBufferIndex].Buffer, DecimatedManifestationComponent, CachedAddress);
        dbg_info->raster_decimated_chroma = dbm->Chroma(BufferState[CurrentDecodeBufferIndex].Buffer, DecimatedManifestationComponent, CachedAddress);
        dbg_info->raster_decimated_stride = dbm->ComponentStride(BufferState[CurrentDecodeBufferIndex].Buffer, DecimatedManifestationComponent, 0, 0);
        dbg_info->raster_decimated_width =  sps->pic_width_in_luma_samples  / dbm->DecimationFactor(BufferState[CurrentDecodeBufferIndex].Buffer, 0);
        dbg_info->raster_decimated_height = sps->pic_height_in_luma_samples / dbm->DecimationFactor(BufferState[CurrentDecodeBufferIndex].Buffer, 1);
#if defined (HEVC_HADES_CANNES25) || defined (HEVC_HADES_CANNESWIFI)
        if (((cmd->ref_format_idc == HEVC_OMEGAH_10) || (cmd->ref_format_idc == HEVC_RASTER2B_10)) &&
            !cmd->curr_display_buffer.convert_10to8_flag)
        {
            dbg_info->raster_decimated_width += dbg_info->raster_decimated_width / 4;
        }
#endif
    }
    else
    {
        dbg_info->raster_decimated_luma =   NULL;
        dbg_info->raster_decimated_chroma = NULL;
    }
#endif // CONFIG_STM_HADES_DEBUG_TOOLS
}

// Synchronous call
CodecStatus_t Codec_MmeVideoHevc_c::SendDecodeCommand(void)
{
    HevcCodecDecodeContext_t *HevcContext = (HevcCodecDecodeContext_t *)DecodeContext;

    // Perform the generic portion of the setup
    DecodeContext->MMECommand.StructSize = sizeof(MME_Command_t);
    DecodeContext->MMECommand.CmdEnd     = MME_COMMAND_END_RETURN_NOTIFY;

    // Check that we have not commenced shutdown.
    mMMECounters.IncPrepared();
    if (TestComponentState(ComponentHalted))
    {
        mMMECounters.IncAborted();
        return CodecNoError;
    }

    DumpMMECommand(&DecodeContext->MMECommand);

    /* In StreamBase decoder we are now sending the Marker buffer for Eof handling.
       This marker buffer has no input (zero size input buffer) so noting to do with the FlushCache */
    DecodeContextBuffer                 = NULL;
    DecodeContext->DecodeCommenceTime   = OS_GetTimeInMicroSeconds();

    if (ModuleParameter_SerializeDecoders())
    {
        Player->GetWriteLockBWLimiter();
    }

    OSDEV_Status_t r = OSDEV_Ioctl(*mHadesDevice, HADES_IOCTL_PROCESS_FRAME,
                                   (void *)&HevcContext->frame, sizeof(HevcContext->frame));
    if (r != OSDEV_NoError)
    {
        SE_ERROR("Failed to process buffer\n");
        return CodecError;
    }

    Stream->Statistics().FrameCountLaunchedDecode++;

    if (ParsedFrameParameters->KeyFrame)
    {
        Stream->Statistics().VidFrameCountLaunchedDecodeI++;
    }

    if (ParsedFrameParameters->ReferenceFrame && !ParsedFrameParameters->KeyFrame)
    {
        Stream->Statistics().VidFrameCountLaunchedDecodeP++;
    }

    if (!ParsedFrameParameters->ReferenceFrame && !ParsedFrameParameters->KeyFrame)
    {
        Stream->Statistics().VidFrameCountLaunchedDecodeB++;
    }

    //FTTB call callback for MME...
    DecodeContext->MMECommand.CmdCode = MME_TRANSFORM;
    Codec_MmeBase_c::CallbackFromMME(MME_COMMAND_COMPLETED_EVT, &DecodeContext->MMECommand);

    return CodecNoError;
}

// /////////////////////////////////////////////////////////////////////////
//
//      The intermediate process, taking output from the pre-processor
//      and feeding it to the lower level of the codec.
//

void *Codec_MmeVideoHevc_IntermediateProcess(void *Parameter)
{
    Codec_MmeVideoHevc_c *Codec = (Codec_MmeVideoHevc_c *)Parameter;
    Codec->IntermediateProcess();
    OS_TerminateThread();
    return NULL;
}

// ----------------------

void Codec_MmeVideoHevc_c::IntermediateProcess()
{
    CodecStatus_t                   Status;
    Buffer_t                        PreProcessorBuffer;
    ParsedFrameParameters_t        *FrameParameters;
    unsigned int                    i;
    bool                            Terminating = false;

    ppFrame_t<HevcPpFrame_t> *ppFrame = new ppFrame_t<HevcPpFrame_t>();
    if (!ppFrame)
    {
        SE_ERROR("Failed to allocate ppFrame\n");
        return;
    }
    // Main loop
    while (!Terminating)
    {
        // Blocking call to get next ppFrame
        if (0 > mPpFramesRing->Extract(ppFrame))
        {
            continue;
        }

        //
        // If SE is in low power state, process should keep inactive, as we are not allowed to issue any MME commands
        //
        if (Stream && Stream->IsLowPowerState())
        {
            ppFrame->Action = ppActionNull;
        }

        //
        // Process activity - note aborted activity differs in consequences for each action.
        //
        switch (ppFrame->Action)
        {
        case ppActionTermination:
            Terminating = true;
            continue;

        case ppActionPassOnPreProcessedFrame: //TODO rm
        case ppActionNull:
            break;

        case ppActionEnterDiscardMode:
            mDiscardMode = true;
            break;

        case ppActionCallOutputPartialDecodeBuffers:
            if (Stream)
            {
                Codec_MmeVideo_c::OutputPartialDecodeBuffers();
            }
            break;

        case ppActionCallDiscardQueuedDecodes:
            mDiscardMode = false;
            Codec_MmeVideo_c::DiscardQueuedDecodes();
            break;

        case ppActionCallReleaseReferenceFrame:
            Codec_MmeVideo_c::ReleaseReferenceFrame(ppFrame->DecodeFrameIndex);
            break;

        case ppActionPassOnFrame:
            // Clear the NewStreamParameters flag so base class does not send a Set Stream Parameter MME command
            FrameParameters = NULL;
            ppFrame->data.CodedBuffer->ObtainMetaDataReference(Player->MetaDataParsedFrameParametersType,
                                                               (void **)(&FrameParameters));
            SE_ASSERT(FrameParameters != NULL);

            FrameParameters->NewStreamParameters = false;
            SE_VERBOSE(group_decoder_video, ">DEC IB input decodeIdx: %d PTS: %lu, mDiscardMode: %d\n",
                       FrameParameters->DecodeFrameIndex,
                       (unsigned long) FrameParameters->PTS.PtsValue(), mDiscardMode);

            // By default release buffers
            Status = CodecNoError;
            if (!mDiscardMode || (ppFrame->DecodeFrameIndex == INVALID_INDEX))
            {
                // Now mimic the input procedure as done in the player process (send command to decode)
                Status = Codec_MmeVideo_c::Input(ppFrame->data.CodedBuffer);
            }

            if ((Status != CodecNoError) || mDiscardMode)
            {
                if (Stream && FrameParameters->FirstParsedParametersForOutputFrame)
                {
                    Stream->ParseToDecodeEdge->RecordNonDecodedFrame(ppFrame->data.CodedBuffer, FrameParameters);
                    Codec_MmeVideo_c::OutputPartialDecodeBuffers();
                }
                // If pre processor buffers are still attached to the coded buffer, make sure they get freed
                if (ppFrame->DecodeFrameIndex != INVALID_INDEX)
                {
                    for (i = 0; i < HEVC_NUM_INTERMEDIATE_POOLS; i++)
                    {
                        ppFrame->data.CodedBuffer->ObtainAttachedBufferReference(mPreProcessorBufferType[i], &PreProcessorBuffer);
                        if (PreProcessorBuffer != NULL)
                        {
                            ppFrame->data.CodedBuffer->DetachBuffer(PreProcessorBuffer);
                        }
                    }
                }

                if (Status == DecodeBufferManagerFailedToAllocateComponents)
                {
                    SE_ERROR("Stream 0x%p Codec input failed for dbm components allocation - marking stream unplayable\n", Stream);
                    // raise the event to signal stream unplayable
                    Stream->MarkUnPlayable(STM_SE_PLAY_STREAM_MSG_REASON_CODE_INSUFFICIENT_MEMORY, true);
                }
            }

            SE_VERBOSE(group_decoder_video, "<DEC %d\n", FrameParameters->DecodeFrameIndex);
            break;
        }

        if (ppFrame->data.CodedBuffer)
        {
            ppFrame->data.CodedBuffer->DecrementReferenceCount(IdentifierHevcIntermediate);
        }
    }

    // Clean up the ring
    while (!mPpFramesRing->IsEmpty())
    {
        mPpFramesRing->Extract(ppFrame);

        if (ppFrame->data.CodedBuffer != NULL)
        {
            ppFrame->data.CodedBuffer->DecrementReferenceCount(IdentifierHevcIntermediate);
        }
    }

    delete ppFrame;
    SE_INFO(group_decoder_video, "Exiting HEVC Intermediate Process Thread\n");

    // We are not accessing any more member variables so it is safe to destroy
    // this object now.
    OS_SemaphoreSignal(&mIntThreadStopped);
}


// /////////////////////////////////////////////////////////////////////////
//
//      Function to fill out a single preprocessor command for a HEVC coded frame.
//
void Codec_MmeVideoHevc_c::FillOutPreprocessorCommand(ppFrame_t<HevcPpFrame_t> *ppFrame,
                                                      ParsedFrameParameters_t *ParsedFrameParameters)
{
    HevcFrameParameters_t *FrameParameters = (HevcFrameParameters_t *)ParsedFrameParameters->FrameParameterStructure;
    HevcPpFrame_t *ppData = &ppFrame->data;
    struct hadespp_ioctl_queue_t *QueInfo = &ppData->param.in;
    void *IntermediateBufferPhysAddr[HEVC_NUM_INTERMEDIATE_POOLS];
    uint32_t DecodeFrameIndex = ParsedFrameParameters->DecodeFrameIndex;
    unsigned int coded_slice_size = FrameParameters->CodedSlicesSize;
    void *CodedDataPhysAddr;
    uint32_t i;

    QueInfo->codec = CODEC_HEVC;

    // Preprocessor driver requires coded buffer's physical
    ppData->CodedBuffer->ObtainDataReference(NULL, NULL, &CodedDataPhysAddr, PhysicalAddress);
    SE_ASSERT(CodedDataPhysAddr != NULL);

    // Get cached address of intermediate buffer so driver preproc can compute CRCs if required
    for (i = 0; i < HEVC_NUM_INTERMEDIATE_POOLS; i++)
    {
        ppData->PreProcessorBuffer[i]->ObtainDataReference(NULL, NULL,
                                                           &IntermediateBufferPhysAddr[i], PhysicalAddress);
        SE_ASSERT(IntermediateBufferPhysAddr[i] != NULL);
    }

    hevcpreproc_transform_param_t *cmd = &QueInfo->iCmd.hevc_iCmd;
    cmd->bit_buffer.base_addr = (uint32_t) CodedDataPhysAddr & (~HADESPP_BUFFER_ALIGNMENT);
    cmd->bit_buffer.length = coded_slice_size;
#if defined (HEVC_HADES_CANNES25) || defined (HEVC_HADES_CANNESWIFI)
    cmd->bit_buffer.start_offset = ParsedFrameParameters->DataOffset + ((uint32_t) CodedDataPhysAddr & HADESPP_BUFFER_ALIGNMENT);
#else
    cmd->bit_buffer.start_offset = ((uint32_t) CodedDataPhysAddr & HADESPP_BUFFER_ALIGNMENT);
#endif
    cmd->bit_buffer.end_offset = cmd->bit_buffer.start_offset + coded_slice_size;

    SE_VERBOSE(group_decoder_video, "DEC %d: bit_buffer: base_addr: 0x%x start 0x%x end: 0x%x length: 0x%x \n",
               ParsedFrameParameters->DecodeFrameIndex, cmd->bit_buffer.base_addr,
               cmd->bit_buffer.start_offset, cmd->bit_buffer_stop_offset,
               cmd->bit_buffer.length);

    intermediate_buffer_t *ib = &cmd->intermediate_buffer;
    /* Addr must be aligned on HADESPP_BUFFER_ALIGNMENT for HW (as IBSize[i]) */
    ib->slice_table_base_addr = BUF_ALIGN_UP(IntermediateBufferPhysAddr[HEVC_SLICE_TABLE], HADESPP_BUFFER_ALIGNMENT);
    ib->slice_table_length = mPreprocessorBufferSize[HEVC_SLICE_TABLE];
    ib->ctb_table_base_addr = BUF_ALIGN_UP(IntermediateBufferPhysAddr[HEVC_CTB_TABLE], HADESPP_BUFFER_ALIGNMENT);
    ib->ctb_table_length = mPreprocessorBufferSize[HEVC_CTB_TABLE];
    ib->slice_headers_base_addr = BUF_ALIGN_UP(IntermediateBufferPhysAddr[HEVC_SLICE_HEADERS], HADESPP_BUFFER_ALIGNMENT);
    ib->slice_headers_length = mPreprocessorBufferSize[HEVC_SLICE_HEADERS];
    ib->ctb_commands.base_addr = BUF_ALIGN_UP(IntermediateBufferPhysAddr[HEVC_CTB_COMMANDS], HADESPP_BUFFER_ALIGNMENT);
    ib->ctb_commands.length = mPreprocessorBufferSize[HEVC_CTB_COMMANDS];
    ib->ctb_commands.start_offset = 0;
    ib->ctb_commands.end_offset = mPreprocessorBufferSize[HEVC_CTB_COMMANDS];
    ib->ctb_residuals.base_addr = BUF_ALIGN_UP(IntermediateBufferPhysAddr[HEVC_CTB_RESIDUALS], HADESPP_BUFFER_ALIGNMENT);
    ib->ctb_residuals.length = mPreprocessorBufferSize[HEVC_CTB_RESIDUALS];
    ib->ctb_residuals.start_offset = 0;
    ib->ctb_residuals.end_offset = mPreprocessorBufferSize[HEVC_CTB_RESIDUALS];

    QueInfo->iDecodeIndex = DecodeFrameIndex;
    cmd->max_slice = MAX_NUM_SLICES;
    cmd->num_poc_total_curr =  ParsedFrameParameters->ReferenceFrameList[REF_PIC_LIST_0].EntryCount;

    // cmd->chksyn_dis : set for ERC in driver
    cmd->disable_hevc     = 0;
    cmd->disable_ovhd     = 0;
    cmd->rdplug_dma_1_cid = 0;
    cmd->wrplug_dma_1_cid = 0;
    cmd->wrplug_dma_2_cid = 0;
    cmd->wrplug_dma_3_cid = 0;
    cmd->wrplug_dma_4_cid = 0;
    cmd->wrplug_dma_5_cid = 0;
    // ---------------
    // SPS parameters
    HevcSequenceParameterSet_t *sps = FrameParameters->SliceHeader.SequenceParameterSet;
    cmd->chroma_format_idc = sps->chroma_format_idc;
    cmd->separate_colour_plane_flag = sps->separate_colour_plane_flag;
    cmd->pic_width_in_luma_samples = sps->pic_width_in_luma_samples;
    cmd->pic_height_in_luma_samples = sps->pic_height_in_luma_samples;
    cmd->pcm_enabled_flag = sps->pcm_enabled_flag;

    if (cmd->pcm_enabled_flag)
    {
        cmd->pcm_bit_depth_luma_minus1 = sps->pcm_bit_depth_luma_minus1;
        cmd->pcm_bit_depth_chroma_minus1 = sps->pcm_bit_depth_chroma_minus1;
        cmd->log2_min_pcm_coding_block_size_minus3 = sps->log2_min_pcm_coding_block_size - 3;
        cmd->log2_diff_max_min_pcm_coding_block_size = sps->log2_max_pcm_coding_block_size - sps->log2_min_pcm_coding_block_size;
    }
    else
    {
        cmd->pcm_bit_depth_luma_minus1 = 0;
        cmd->pcm_bit_depth_chroma_minus1 = 0;
        cmd->log2_min_pcm_coding_block_size_minus3 = 0;
        cmd->log2_diff_max_min_pcm_coding_block_size = 0;
    }

    HevcPictureParameterSet_t *pps = FrameParameters->SliceHeader.PictureParameterSet;
    cmd->log2_max_pic_order_cnt_lsb_minus4 = sps->log2_max_pic_order_cnt_lsb - 4;
    cmd->lists_modification_present_flag = pps->lists_modification_present_flag;
    cmd->log2_min_coding_block_size_minus3 = sps->log2_min_coding_block_size - 3;
    cmd->log2_diff_max_min_coding_block_size = sps->log2_max_coding_block_size - sps->log2_min_coding_block_size;
    cmd->log2_min_transform_block_size_minus2 = sps->log2_min_transform_block_size - 2;
    cmd->log2_diff_max_min_transform_block_size = sps->log2_max_transform_block_size - sps->log2_min_transform_block_size;
    cmd->max_transform_hierarchy_depth_inter = sps->max_transform_hierarchy_depth_inter;
    cmd->max_transform_hierarchy_depth_intra = sps->max_transform_hierarchy_depth_intra;
    cmd->transform_skip_enabled_flag = pps->transform_skip_enabled_flag;
    cmd->pps_loop_filter_across_slices_enabled_flag = pps->pps_loop_filter_across_slices_enabled_flag;
    cmd->amp_enabled_flag = sps->amp_enabled_flag;

#if defined (HEVC_HADES_CANNES25) || defined (HEVC_HADES_CANNESWIFI)
    cmd->bit_depth_luma_minus8 = sps->bit_depth_luma_minus8;
    cmd->bit_depth_chroma_minus8 = sps->bit_depth_chroma_minus8;
#endif
    cmd->sample_adaptive_offset_enabled_flag = sps->sample_adaptive_offset_enabled_flag;
    cmd->num_short_term_ref_pic_sets = sps->num_short_term_ref_pic_sets;
    cmd->long_term_ref_pics_present_flag = sps->long_term_ref_pics_present_flag;
    cmd->num_long_term_ref_pics_sps = sps->num_long_term_ref_pics_sps;
    cmd->sps_temporal_mvp_enabled_flag = sps->sps_temporal_mvp_enabled_flag;

    // Only the num_short_term_ref_pic_sets first elements are filled.
    // Unused values shall be set to 0
    for (i = 0; i < cmd->num_short_term_ref_pic_sets; i++)
    {
        cmd->num_delta_pocs[i] = sps->st_rps[i].rps_s0.num_pics + sps->st_rps[i].rps_s1.num_pics;
    }

    for (; i < HEVC_MAX_SHORT_TERM_RPS; i++)
    {
        cmd->num_delta_pocs[i] = 0;
    }

    // ---------------
    // PPS parameters
    cmd->dependent_slice_segments_enabled_flag = pps->dependent_slice_segments_enabled_flag;
    cmd->sign_data_hiding_flag = pps->sign_data_hiding_flag;
    cmd->cabac_init_present_flag = pps->cabac_init_present_flag;
    cmd->num_ref_idx_l0_default_active_minus1 = pps->num_ref_idx_l0_default_active - 1;
    cmd->num_ref_idx_l1_default_active_minus1 = pps->num_ref_idx_l1_default_active - 1;
    cmd->init_qp_minus26 = pps->init_qp_minus26;
    cmd->cu_qp_delta_enabled_flag = pps->cu_qp_delta_enabled_flag;
    cmd->diff_cu_qp_delta_depth = pps->diff_cu_qp_delta_depth;
    cmd->weighted_pred_flag = pps->weighted_pred_flag[P_SLICE];
    cmd->weighted_bipred_flag = pps->weighted_pred_flag[B_SLICE];
    cmd->output_flag_present_flag = pps->output_flag_present_flag;
    cmd->deblocking_filter_control_present_flag = pps->deblocking_filter_control_present_flag;
    cmd->deblocking_filter_override_enabled_flag = pps->deblocking_filter_override_enabled_flag;
    cmd->pps_deblocking_filter_disable_flag = pps->pps_deblocking_filter_disable_flag;
    cmd->pps_beta_offset_div2 = pps->pps_beta_offset_div2;
    cmd->pps_tc_offset_div2 = pps->pps_tc_offset_div2;
    cmd->transquant_bypass_enable_flag = pps->transquant_bypass_enable_flag;
    cmd->tiles_enabled_flag = pps->tiles_enabled_flag;
    cmd->entropy_coding_sync_enabled_flag = pps->entropy_coding_sync_enabled_flag;
    //! Number of tiles in width inside the picture. Shall be >= 1
    cmd->num_tile_columns = pps->num_tile_columns;
    //! Number of tiles in height inside the picture. Shall be >= 1
    cmd->num_tile_rows = pps->num_tile_rows;

    //! Width of each tile column, in number of ctb. Unused values shall be set to 0
    for (i = 0; i < cmd->num_tile_columns; i++)
    {
        cmd->tile_column_width[i] = pps->tile_width[i];
    }

    for (; i < HEVC_MAX_TILE_COLUMNS; i++)
    {
        cmd->tile_column_width[i] = 0;
    }

    //! Height of each tile row, in number of ctb. Unused values shall be set to 0
    for (i = 0; i < cmd->num_tile_rows; i++)
    {
        cmd->tile_row_height[i] = pps->tile_height[i];
    }

    for (; i < HEVC_MAX_TILE_ROWS; i++)
    {
        cmd->tile_row_height[i] = 0;
    }

    cmd->slice_segment_header_extension_flag = pps->slice_segment_header_extension_flag;
    cmd->num_extra_slice_header_bits = pps->num_extra_slice_header_bits;
    cmd->pps_slice_chroma_qp_offsets_present_flag = pps->pps_slice_chroma_qp_offsets_present_flag;
    // ---------------
    // Slice header parameters
    //! This parameter is computed using syntax elements found in the first slice header of the picture
    //! (short_term_ref_pic_set(), short_term_ref_pic_set_idx, num_long_term_pics...)
    // TBD: SAO (*sample_adaptive*)???
    // TBD: how to parse aps_id?
    // Dump MME command via strelay
#ifdef HEVC_CODEC_DUMP_MME
    st_relayfs_write_mme_text(ST_RELAY_TYPE_MME_TEXT1, "\n\t/* Picture #%d */", DecodeFrameIndex);
    LOG_NEXT_STRUCT(ST_RELAY_TYPE_MME_TEXT1, 1, ROTBUF, cmd, bit_buffer);
    LOG_NEXT_ELEM(ST_RELAY_TYPE_MME_TEXT1, 1, cmd, max_slice);
    LOG_NEXT_STRUCT(ST_RELAY_TYPE_MME_TEXT1, 1, INTBUF, cmd, intermediate_buffer);
    LOG_NEXT_ELEM(ST_RELAY_TYPE_MME_TEXT1, 1, cmd, chroma_format_idc);
    LOG_NEXT_ELEM(ST_RELAY_TYPE_MME_TEXT1, 1, cmd, separate_colour_plane_flag);
    LOG_NEXT_ELEM(ST_RELAY_TYPE_MME_TEXT1, 1, cmd, pic_width_in_luma_samples);
    LOG_NEXT_ELEM(ST_RELAY_TYPE_MME_TEXT1, 1, cmd, pic_height_in_luma_samples);
    LOG_NEXT_ELEM(ST_RELAY_TYPE_MME_TEXT1, 1, cmd, pcm_enabled_flag);
    LOG_NEXT_ELEM(ST_RELAY_TYPE_MME_TEXT1, 1, cmd, pcm_bit_depth_luma_minus1);
    LOG_NEXT_ELEM(ST_RELAY_TYPE_MME_TEXT1, 1, cmd, pcm_bit_depth_chroma_minus1);
    LOG_NEXT_ELEM(ST_RELAY_TYPE_MME_TEXT1, 1, cmd, log2_max_pic_order_cnt_lsb_minus4);
    LOG_NEXT_ELEM(ST_RELAY_TYPE_MME_TEXT1, 1, cmd, lists_modification_present_flag);
    LOG_NEXT_ELEM(ST_RELAY_TYPE_MME_TEXT1, 1, cmd, log2_min_coding_block_size_minus3);
    LOG_NEXT_ELEM(ST_RELAY_TYPE_MME_TEXT1, 1, cmd, log2_diff_max_min_coding_block_size);
    LOG_NEXT_ELEM(ST_RELAY_TYPE_MME_TEXT1, 1, cmd, log2_min_transform_block_size_minus2);
    LOG_NEXT_ELEM(ST_RELAY_TYPE_MME_TEXT1, 1, cmd, log2_diff_max_min_transform_block_size);
    LOG_NEXT_ELEM(ST_RELAY_TYPE_MME_TEXT1, 1, cmd, log2_min_pcm_coding_block_size_minus3);
    LOG_NEXT_ELEM(ST_RELAY_TYPE_MME_TEXT1, 1, cmd, log2_diff_max_min_pcm_coding_block_size);
    LOG_NEXT_ELEM(ST_RELAY_TYPE_MME_TEXT1, 1, cmd, max_transform_hierarchy_depth_inter);
    LOG_NEXT_ELEM(ST_RELAY_TYPE_MME_TEXT1, 1, cmd, max_transform_hierarchy_depth_intra);
    LOG_NEXT_ELEM(ST_RELAY_TYPE_MME_TEXT1, 1, cmd, transform_skip_enabled_flag);
    LOG_NEXT_ELEM(ST_RELAY_TYPE_MME_TEXT1, 1, cmd, pps_loop_filter_across_slices_enabled_flag);
    LOG_NEXT_ELEM(ST_RELAY_TYPE_MME_TEXT1, 1, cmd, amp_enabled_flag);
    LOG_NEXT_ELEM(ST_RELAY_TYPE_MME_TEXT1, 1, cmd, sample_adaptive_offset_enabled_flag);
    LOG_NEXT_ELEM(ST_RELAY_TYPE_MME_TEXT1, 1, cmd, num_short_term_ref_pic_sets);
    LOG_NEXT_ELEM(ST_RELAY_TYPE_MME_TEXT1, 1, cmd, long_term_ref_pics_present_flag);
    LOG_NEXT_ELEM(ST_RELAY_TYPE_MME_TEXT1, 1, cmd, num_long_term_ref_pics_sps);
    LOG_NEXT_ELEM(ST_RELAY_TYPE_MME_TEXT1, 1, cmd, sps_temporal_mvp_enabled_flag);
    LOG_NEXT_TABLE(ST_RELAY_TYPE_MME_TEXT1, 1, cmd, num_delta_pocs, HEVC_MAX_SHORT_TERM_RPS);
    LOG_NEXT_ELEM(ST_RELAY_TYPE_MME_TEXT1, 1, cmd, dependent_slice_segments_enabled_flag);
    LOG_NEXT_ELEM(ST_RELAY_TYPE_MME_TEXT1, 1, cmd, sign_data_hiding_flag);
    LOG_NEXT_ELEM(ST_RELAY_TYPE_MME_TEXT1, 1, cmd, cabac_init_present_flag);
    LOG_NEXT_ELEM(ST_RELAY_TYPE_MME_TEXT1, 1, cmd, num_ref_idx_l0_default_active_minus1);
    LOG_NEXT_ELEM(ST_RELAY_TYPE_MME_TEXT1, 1, cmd, num_ref_idx_l1_default_active_minus1);
    LOG_NEXT_ELEM(ST_RELAY_TYPE_MME_TEXT1, 1, cmd, init_qp_minus26);
    LOG_NEXT_ELEM(ST_RELAY_TYPE_MME_TEXT1, 1, cmd, cu_qp_delta_enabled_flag);
    LOG_NEXT_ELEM(ST_RELAY_TYPE_MME_TEXT1, 1, cmd, diff_cu_qp_delta_depth);
    LOG_NEXT_ELEM(ST_RELAY_TYPE_MME_TEXT1, 1, cmd, weighted_pred_flag);
    LOG_NEXT_ELEM(ST_RELAY_TYPE_MME_TEXT1, 1, cmd, weighted_bipred_flag);
    LOG_NEXT_ELEM(ST_RELAY_TYPE_MME_TEXT1, 1, cmd, output_flag_present_flag);
    LOG_NEXT_ELEM(ST_RELAY_TYPE_MME_TEXT1, 1, cmd, deblocking_filter_control_present_flag);
    LOG_NEXT_ELEM(ST_RELAY_TYPE_MME_TEXT1, 1, cmd, deblocking_filter_override_enabled_flag);
    LOG_NEXT_ELEM(ST_RELAY_TYPE_MME_TEXT1, 1, cmd, pps_deblocking_filter_disable_flag);
    LOG_NEXT_ELEM(ST_RELAY_TYPE_MME_TEXT1, 1, cmd, pps_beta_offset_div2);
    LOG_NEXT_ELEM(ST_RELAY_TYPE_MME_TEXT1, 1, cmd, pps_tc_offset_div2);
    LOG_NEXT_ELEM(ST_RELAY_TYPE_MME_TEXT1, 1, cmd, transquant_bypass_enable_flag);
    LOG_NEXT_ELEM(ST_RELAY_TYPE_MME_TEXT1, 1, cmd, tiles_enabled_flag);
    LOG_NEXT_ELEM(ST_RELAY_TYPE_MME_TEXT1, 1, cmd, entropy_coding_sync_enabled_flag);
    LOG_NEXT_ELEM(ST_RELAY_TYPE_MME_TEXT1, 1, cmd, num_tile_columns);
    LOG_NEXT_ELEM(ST_RELAY_TYPE_MME_TEXT1, 1, cmd, num_tile_rows);
    LOG_NEXT_TABLE(ST_RELAY_TYPE_MME_TEXT1, 1, cmd, tile_column_width, HEVC_MAX_TILE_COLUMNS);
    LOG_NEXT_TABLE(ST_RELAY_TYPE_MME_TEXT1, 1, cmd, tile_row_height, HEVC_MAX_TILE_ROWS);
    LOG_NEXT_ELEM(ST_RELAY_TYPE_MME_TEXT1, 1, cmd, slice_segment_header_extension_flag);
    LOG_NEXT_ELEM(ST_RELAY_TYPE_MME_TEXT1, 1, cmd, num_extra_slice_header_bits);
    LOG_NEXT_ELEM(ST_RELAY_TYPE_MME_TEXT1, 1, cmd, pps_slice_chroma_qp_offsets_present_flag);
    LOG_NEXT_ELEM(ST_RELAY_TYPE_MME_TEXT1, 1, cmd, num_poc_total_curr);
    st_relayfs_write_mme_text(ST_RELAY_TYPE_MME_TEXT1, "\n\t},");
#endif // HEVC_CODEC_DUMP_MME
}

////////////////////////////////////////////////////////////////////////////
///
/// Unconditionally return success.
///
/// Success and failure codes are located entirely in the generic MME structures
/// allowing the super-class to determine whether the decode was successful. This
/// means that we have no work to do here.
///
/// \return CodecNoError
///
CodecStatus_t   Codec_MmeVideoHevc_c::ValidateDecodeContext(CodecBaseDecodeContext_t *Context)
{
    HevcCodecDecodeContext_t    *HevcContext    = (HevcCodecDecodeContext_t *)Context;

    if (HevcContext->frame.status.error_code != HEVC_DECODER_NO_ERROR)
    {
        SE_INFO(group_decoder_video, "HEVC decode error %08x at frame %d\n",
                HevcContext->frame.status.error_code, HevcContext->DecodeIndex);
    }

    return CodecNoError;
}

//{{{  CheckCodecReturnParameters
// Convert the return code into human readable form.
static const char *LookupError(unsigned int Error)
{
#define E(e) case e: return #e

    switch (Error)
    {
        E(HEVC_DECODER_ERROR_RECOVERED);
        E(HEVC_DECODER_ERROR_NOT_RECOVERED);

    default: return "HEVC_DECODER_UNKNOWN_ERROR";
    }

#undef E
}

CodecStatus_t   Codec_MmeVideoHevc_c::CheckCodecReturnParameters(CodecBaseDecodeContext_t *Context)
{
    unsigned int              i, j;
    unsigned int              ErroneousCTBs;
    unsigned int              log2_max_coding_block_size, max_coding_block_size;
    unsigned int              TotalCTBs;
    unsigned long long        DecodeHadesTime;
    HevcCodecDecodeContext_t *HevcContext        = (HevcCodecDecodeContext_t *)Context;
    MME_Command_t            *MMECommand         = (MME_Command_t *)(&Context->MMECommand);
    MME_CommandStatus_t      *CmdStatus          = (MME_CommandStatus_t *)(&MMECommand->CmdStatus);
    hevcdecpix_status_t      *AdditionalInfo_p   = (hevcdecpix_status_t *)CmdStatus->AdditionalInfo_p;

    if (AdditionalInfo_p != NULL)
    {
        // Read CEH register values
        FillCEHRegisters(Context, HevcContext->frame.status.ceh_registers);

        if (AdditionalInfo_p->error_code != HEVC_DECODER_NO_ERROR)
        {
            SE_INFO(group_decoder_video, "%s  %x\n", LookupError(AdditionalInfo_p->error_code), AdditionalInfo_p->error_code);

            // Calculate decode quality from error status fields
            log2_max_coding_block_size = 3 + HevcContext->frame.cmd.TransformParameters.log2_max_coding_block_size_minus3;
            max_coding_block_size = 1;

            while (log2_max_coding_block_size-- > 0)
            {
                max_coding_block_size <<= 1;
            }

            if (!max_coding_block_size) { max_coding_block_size = 1; }

            TotalCTBs = (HevcContext->frame.cmd.TransformParameters.pic_width_in_luma_samples *
                         HevcContext->frame.cmd.TransformParameters.pic_height_in_luma_samples)
                        / (max_coding_block_size * max_coding_block_size);
            ErroneousCTBs = 0;

            for (i = 0; i < HEVC_ERROR_LOC_PARTITION; i++)
                for (j = 0; j < HEVC_ERROR_LOC_PARTITION; j++)
                {
                    ErroneousCTBs += HevcContext->frame.status.error_loc[i][j];
                }

            Context->DecodeQuality = (ErroneousCTBs < TotalCTBs) ?
                                     Rational_t((TotalCTBs - ErroneousCTBs), TotalCTBs) : 0;
#if 0
            SE_DEBUG(group_decoder_video, "AZAAZA - %4d %4d - %d.%09d\n", TotalCTBs, ErroneousCTBs,
                     Context->DecodeQuality.IntegerPart(),  Context->DecodeQuality.RemainderDecimal(9));

            for (unsigned int i = 0; i < HEVC_STATUS_PARTITION; i++)
                SE_DEBUG(group_decoder_video, "  %02x %02x %02x %02x %02x %02x\n",
                         HevcContext->DecodeStatus.status[0][i], HevcContext->DecodeStatus.status[1][i],
                         HevcContext->DecodeStatus.status[2][i], HevcContext->DecodeStatus.status[3][i],
                         HevcContext->DecodeStatus.status[4][i], HevcContext->DecodeStatus.status[5][i]);

#endif

            switch (AdditionalInfo_p->error_code)
            {
            case HEVC_DECODER_ERROR_RECOVERED:
                Stream->Statistics().FrameDecodeRecoveredError++;
                break;

            case HEVC_DECODER_ERROR_NOT_RECOVERED:
                Stream->Statistics().FrameDecodeNotRecoveredError++;
                break;

            default:
                Stream->Statistics().FrameDecodeError++;
                break;
            }

            if (CmdStatus->Error == MME_COMMAND_TIMEOUT)
            {
                Stream->Statistics().FrameDecodeErrorTaskTimeOutError++;
            }
        }
        else
        {
            // Fabric controller clock value needed to calculate hades decode time on cannes (333 MHz)
            // TODO: Get rate from hades
            const unsigned int  HadesFCClockCannes = 333;
            DecodeHadesTime = HevcContext->frame.status.decode_time_in_tick / HadesFCClockCannes;
            // For field, decode time is to be doubled
            IsFieldPicture = (HevcContext->DecodePictureStructure != StructureFrame);

            PictureDecodeTime = DecodeHadesTime;
            Codec_MmeVideo_c::FillDecodeTimeStatistics(HevcContext->DecodeIndex, HevcContext->PTS);
        }
    }

    return CodecNoError;
}
//}}}

CodecStatus_t   Codec_MmeVideoHevc_c::DumpDecodeParameters(void *Parameters)
{
    return DumpDecodeParameters(Parameters, 0);
}

// /////////////////////////////////////////////////////////////////////////
//
//      Function to dump out the decode
//      parameters from an mme command.
//

CodecStatus_t   Codec_MmeVideoHevc_c::DumpDecodeParameters(void *Parameters, unsigned int DecodeFrameIndex)
{
#ifdef HEVC_CODEC_DUMP_MME
    hevcdecpix_transform_param_t *cmd = (hevcdecpix_transform_param_t *)Parameters;
    st_relayfs_write_mme_text(ST_RELAY_TYPE_MME_TEXT2, "\n\t/* Picture #%d */", DecodeFrameIndex);
    LOG_FIRST_ELEM(ST_RELAY_TYPE_MME_TEXT2, 1, cmd, slice_table_entries);
    LOG_NEXT_STRUCT(ST_RELAY_TYPE_MME_TEXT2, 1, INTBUF, cmd, intermediate_buffer);
    LOG_NEXT_ELEM(ST_RELAY_TYPE_MME_TEXT2, 1, cmd, dpb_base_addr);
    LOG_NEXT_ELEM(ST_RELAY_TYPE_MME_TEXT2, 1, cmd, ppb_base_addr);
    LOG_NEXT_STRUCT_TABLE(ST_RELAY_TYPE_MME_TEXT2, 1, REFBUF, cmd, ref_picture_buffer, HEVC_MAX_REFERENCE_PICTURES);
    LOG_NEXT_STRUCT_TABLE(ST_RELAY_TYPE_MME_TEXT2, 1, PICINFO, cmd, ref_picture_info, HEVC_MAX_REFERENCE_PICTURES);
    LOG_NEXT_ELEM(ST_RELAY_TYPE_MME_TEXT2, 1, cmd, erc_ref_pic_idx);
    LOG_NEXT_ELEM(ST_RELAY_TYPE_MME_TEXT2, 1, cmd, v_dec_factor);
    LOG_NEXT_ELEM(ST_RELAY_TYPE_MME_TEXT2, 1, cmd, h_dec_factor);
    LOG_NEXT_STRUCT(ST_RELAY_TYPE_MME_TEXT2, 1, RESIZE, cmd, luma_resize_param);
    LOG_NEXT_STRUCT(ST_RELAY_TYPE_MME_TEXT2, 1, RESIZE, cmd, chroma_resize_param);
    LOG_NEXT_STRUCT(ST_RELAY_TYPE_MME_TEXT2, 1, REFBUF, cmd, curr_picture_buffer);
    LOG_NEXT_STRUCT(ST_RELAY_TYPE_MME_TEXT2, 1, DISPBUF, cmd, curr_display_buffer);
    LOG_NEXT_STRUCT(ST_RELAY_TYPE_MME_TEXT2, 1, DISPBUF, cmd, curr_resize_buffer);
    LOG_NEXT_STRUCT(ST_RELAY_TYPE_MME_TEXT2, 1, PICINFO, cmd, curr_picture_info);
    LOG_NEXT_ELEM(ST_RELAY_TYPE_MME_TEXT2, 1, cmd, num_reference_pictures);
    LOG_NEXT_TABLE(ST_RELAY_TYPE_MME_TEXT2, 1, cmd, initial_ref_pic_list_l0, HEVC_MAX_REFERENCE_INDEX);
    LOG_NEXT_TABLE(ST_RELAY_TYPE_MME_TEXT2, 1, cmd, initial_ref_pic_list_l1, HEVC_MAX_REFERENCE_INDEX);
    LOG_NEXT_ELEM(ST_RELAY_TYPE_MME_TEXT2, 1, cmd, mcc_mode);
    LOG_NEXT_ELEM(ST_RELAY_TYPE_MME_TEXT2, 1, cmd, mcc_opcode);
    LOG_NEXT_ELEM(ST_RELAY_TYPE_MME_TEXT2, 1, cmd, decoding_mode);
    LOG_NEXT_ELEM(ST_RELAY_TYPE_MME_TEXT2, 1, cmd, additional_flags);
    LOG_NEXT_ELEM(ST_RELAY_TYPE_MME_TEXT2, 1, cmd, first_picture_in_sequence_flag);
    LOG_NEXT_ELEM(ST_RELAY_TYPE_MME_TEXT2, 1, cmd, level_idc);
    LOG_NEXT_ELEM(ST_RELAY_TYPE_MME_TEXT2, 1, cmd, sps_max_dec_pic_buffering_minus1);
    LOG_NEXT_ELEM(ST_RELAY_TYPE_MME_TEXT2, 1, cmd, loop_filter_across_tiles_enabled_flag);
    LOG_NEXT_ELEM(ST_RELAY_TYPE_MME_TEXT2, 1, cmd, pcm_loop_filter_disable_flag);
    LOG_NEXT_ELEM(ST_RELAY_TYPE_MME_TEXT2, 1, cmd, sps_temporal_mvp_enabled_flag);
    LOG_NEXT_ELEM(ST_RELAY_TYPE_MME_TEXT2, 1, cmd, log2_parallel_merge_level_minus2);
    LOG_NEXT_ELEM(ST_RELAY_TYPE_MME_TEXT2, 1, cmd, pic_width_in_luma_samples);
    LOG_NEXT_ELEM(ST_RELAY_TYPE_MME_TEXT2, 1, cmd, pic_height_in_luma_samples);
    LOG_NEXT_ELEM(ST_RELAY_TYPE_MME_TEXT2, 1, cmd, pcm_bit_depth_luma_minus1);
    LOG_NEXT_ELEM(ST_RELAY_TYPE_MME_TEXT2, 1, cmd, pcm_bit_depth_chroma_minus1);
    LOG_NEXT_ELEM(ST_RELAY_TYPE_MME_TEXT2, 1, cmd, log2_max_coding_block_size_minus3);
    LOG_NEXT_ELEM(ST_RELAY_TYPE_MME_TEXT2, 1, cmd, scaling_list_enable);
    LOG_NEXT_ELEM(ST_RELAY_TYPE_MME_TEXT2, 1, cmd, transform_skip_enabled_flag);
    LOG_NEXT_ELEM(ST_RELAY_TYPE_MME_TEXT2, 1, cmd, lists_modification_present_flag);
    LOG_NEXT_ELEM(ST_RELAY_TYPE_MME_TEXT2, 1, cmd, num_tile_columns);
    LOG_NEXT_ELEM(ST_RELAY_TYPE_MME_TEXT2, 1, cmd, num_tile_rows);
    LOG_NEXT_TABLE(ST_RELAY_TYPE_MME_TEXT2, 1, cmd, tile_column_width, HEVC_MAX_TILE_COLUMNS);
    LOG_NEXT_TABLE(ST_RELAY_TYPE_MME_TEXT2, 1, cmd, tile_row_height, HEVC_MAX_TILE_ROWS);
    LOG_NEXT_ELEM(ST_RELAY_TYPE_MME_TEXT2, 1, cmd, sign_data_hiding_flag);
    LOG_NEXT_ELEM(ST_RELAY_TYPE_MME_TEXT2, 1, cmd, weighted_pred_flag);
    LOG_NEXT_ELEM(ST_RELAY_TYPE_MME_TEXT2, 1, cmd, weighted_bipred_flag);
    LOG_NEXT_ELEM(ST_RELAY_TYPE_MME_TEXT2, 1, cmd, constrained_intra_pred_flag);
    LOG_NEXT_ELEM(ST_RELAY_TYPE_MME_TEXT2, 1, cmd, strong_intra_smoothing_enable_flag);
    LOG_NEXT_ELEM(ST_RELAY_TYPE_MME_TEXT2, 1, cmd, pps_cb_qp_offset);
    LOG_NEXT_ELEM(ST_RELAY_TYPE_MME_TEXT2, 1, cmd, pps_cr_qp_offset);
    LOG_NEXT_ELEM(ST_RELAY_TYPE_MME_TEXT2, 1, cmd, scaling_list_command_size);
    LOG_NEXT_TABLE(ST_RELAY_TYPE_MME_TEXT2, 1, cmd, scaling_list_command, HEVC_MAX_SCALING_LIST_BUFFER);
    st_relayfs_write_mme_text(ST_RELAY_TYPE_MME_TEXT2, "\n\t},");
#else
    (void)Parameters; // warning removal
    (void)DecodeFrameIndex; // warning removal
#endif // HEVC_CODEC_DUMP_MME
    return CodecNoError;
}

