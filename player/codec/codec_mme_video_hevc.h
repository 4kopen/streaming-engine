/************************************************************************
Copyright (C) 2003-2014 STMicroelectronics. All Rights Reserved.

This file is part of the Streaming Engine.

Streaming Engine is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License version 2 as
published by the Free Software Foundation.

Streaming Engine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with Streaming Engine; see the file COPYING.  If not, write to the Free
Software Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307,
USA.

The Streaming Engine Library may alternatively be licensed under a
proprietary license from ST.
************************************************************************/

#ifndef H_CODEC_MME_VIDEO_HEVC
#define H_CODEC_MME_VIDEO_HEVC

#include "codec_mme_video.h"
#include "hevc.h"
#include "hadesppinline.h"
#include "hevc_hard_host_transformer.h"

#include "codec_mme_video_utils.h"

// TODO: Move to common HEVC header file (hevc.h ?)
#define MAX_NUM_SLICES 200
#define MAX_SCALING_SIZEID      4 //!< Maximum number of SizeID signifying different transformation size for scaling.
#define MAX_SCALING_MATRIXID    6 //!< Maximum number of MatrixID for scaling.
#define SLICE_TABLE_ENTRY_SIZE  8
#define MAX_SLICE_HEADER_SIZE   102
#define MAX_CTB_TABLE_ENTRIES   1100
#define CTB_TABLE_ENTRY_SIZE    4
#define MAX_CTB_32x32_COMMAND_SIZE 73
#define MAX_PIC_SIZE_IN_CTB_32x32     9600
#define MAX_RESIDUAL_IN_CTB_32x32      400

// /////////////////////////////////////////////////////////////////////////
//
// The C task entry stubs
//

extern "C" {
    void *Codec_MmeVideoHevc_IntermediateProcess(void *Parameter);
}

/////////////////////////////////////////////////////////////////////////

typedef struct HevcPpFrame_s
{
    Buffer_t CodedBuffer;
    Buffer_t PreProcessorBuffer[HEVC_NUM_INTERMEDIATE_POOLS];
    hadespp_ioctl_synchronous_call_t param;

    HevcPpFrame_s()
        : CodedBuffer()
        , PreProcessorBuffer()
        , param() {}
    HevcPpFrame_s(const HevcPpFrame_s &f)
        : CodedBuffer(f.CodedBuffer)
        , param(f.param)
    {
        memcpy(PreProcessorBuffer, f.PreProcessorBuffer, sizeof(PreProcessorBuffer));
    }
    ~HevcPpFrame_s() {};

    HevcPpFrame_s &operator=(const HevcPpFrame_s &f)
    {
        if (this != &f)
        {
            CodedBuffer = f.CodedBuffer;
            memcpy(PreProcessorBuffer, f.PreProcessorBuffer, sizeof(PreProcessorBuffer));
            param = f.param;
        }
        return *this;
    }

} HevcPpFrame_t;

/////////////////////////////////////////////////////////////////////////

class Codec_MmeVideoHevc_c : public Codec_MmeVideo_c
{
public:
    Codec_MmeVideoHevc_c();
    CodecStatus_t FinalizeInit();
    ~Codec_MmeVideoHevc_c();

    CodecStatus_t   Halt();

    //
    // Superclass functions
    //

    CodecStatus_t   Connect(Port_c *Port);
    CodecStatus_t   OutputPartialDecodeBuffers();
    CodecStatus_t   DiscardQueuedDecodes();
    CodecStatus_t   ReleaseReferenceFrame(unsigned int              ReferenceFrameDecodeIndex);
    CodecStatus_t   CheckReferenceFrameList(unsigned int              NumberOfReferenceFrameLists,
                                            ReferenceFrameList_t      ReferenceFrameList[]);
    CodecStatus_t   Input(Buffer_t                  CodedBuffer);

    CodecStatus_t   ValidateDecodeContext(CodecBaseDecodeContext_t *Context);
    CodecStatus_t   CheckCodecReturnParameters(CodecBaseDecodeContext_t *Context);

    bool            IsDecimationValueSupported(int DecimationPolicy);

    void IntermediateProcess();

private:
    Hevc_InitTransformerParam_fmw_t      InitTransformerParam;

    ppFramesRing_c<HevcPpFrame_t>        *mPpFramesRing;
    bool                                  mDiscardMode;

    BufferType_t                          mPreProcessorBufferType[HEVC_NUM_INTERMEDIATE_POOLS];
    allocator_device_t                    mPreProcessorBufferAllocator;
    unsigned int                          mPreprocessorBufferSize[HEVC_NUM_INTERMEDIATE_POOLS]; // maximum size of IB according to stream level
    BufferPool_t                          mPreProcessorBufferPool[HEVC_NUM_INTERMEDIATE_POOLS];
    HadesppDevice_t                      *mPreProcessorDevice;
    OSDEV_DeviceIdentifier_t             *mHadesDevice;

    uint32_t                              mLastLevelIdc; // used to cache PreprocessorBufferSize[] when level does not change between pictures

#ifdef WA_PPB_OUT_NON_REF
    allocator_device_t                    mFakePPBAllocator;
    unsigned char                        *mFakePPBAddress;
#endif
// TODO: Uncomment member attributes as they become necessary
    //bool                                  ReferenceFrameSlotUsed[HEVC_MAX_REFERENCE_FRAMES];    // A usage array for reference frame slots in the transform data
    //H264_HostData_t                       RecordedHostData[MAX_DECODE_BUFFERS];                 // A record of hostdata for each reference frame
    //unsigned int                          OutstandingSlotAllocationRequest;
    //
    //unsigned int                          NumberOfUsedDescriptors;                              // Map of used descriptors when constructing a reference list
    //unsigned char                         DescriptorIndices[3 * H264_MAX_REFERENCE_FRAMES];
    //
    ReferenceFrameList_t                  LocalReferenceFrameList[HEVC_NUM_REF_FRAME_LISTS];    // A reference frame list used for local processing, that the 2.4 compiler
    // discovered is a little large for the stack frame.
    //
    DecodeBufferManager_t                 mDecodeBufferManager;

    hevcdecpix_reference_buffer_t         mSavedReferenceForSubstitution;
    bool                                  mSavedReferenceForSubstitutionCanBeUsed;

    OS_Semaphore_t                        mIntThreadStopped;

    // Functions

    CodecStatus_t   HandleCapabilities();

    CodecStatus_t   AllocatePreProcBufferPool(HevcFrameParameters_t *FrameParameters);
    void            DeallocatePreProcBufferPool();
    int             UpdatePreprocessorBufferSize(HevcFrameParameters_t *FrameParameters);
    CodecStatus_t   GetIntermediateBufferSize(int level_index,  uint32_t pic_width_in_luma_samples,
                                              uint32_t pic_height_in_luma_samples, uint32_t bits_per_sample, unsigned int *IBSize);

    CodecStatus_t   FillOutTransformerInitializationParameters();
    CodecStatus_t   FillOutSetStreamParametersCommand();
    void            FillOutBufferCountRequest(DecodeBufferRequest_t *Request);
    CodecStatus_t   FillOutDecodeCommand();
    void            FillOutDebugInfo(void *ctx, HevcSliceHeader_t *sliceheader);
    CodecStatus_t   SendDecodeCommand();

    CodecStatus_t   DumpDecodeParameters(void *Parameters);
    CodecStatus_t   DumpDecodeParameters(void *Parameters, unsigned int DecodeFrameIndex);
    void            SubstituteSavedPreviousReference(hevcdecpix_transform_param_t *cmd, unsigned int index);

    CodecStatus_t   FillOutDecodeBufferRequest(DecodeBufferRequest_t    *Request);
    void            FillOutPreprocessorCommand(ppFrame_t<HevcPpFrame_t> *ppFrame, ParsedFrameParameters_t *fp);
    CodecStatus_t   CheckPPStatus(ppFrame_t<HevcPpFrame_t> *ppFrame, ParsedFrameParameters_t *fp);
    void            DumpIntermediateBuffers(ppFrame_t<HevcPpFrame_t> *ppFrame, ParsedFrameParameters_t *fp);


    static uint32_t scaling_list_command_init(uint32_t *pt, uint8_t use_default, scaling_list_t *scaling_list);

    void            TrimCodedBuffer(uint8_t *buffer, unsigned int DataOffset, unsigned int *CodedSlicesSize);

    void            ClearInternalBufferMap();
    void            CreateInternalBufferMap();

    DISALLOW_COPY_AND_ASSIGN(Codec_MmeVideoHevc_c);
};

#endif // H_CODEC_MME_VIDEO_HEVC
