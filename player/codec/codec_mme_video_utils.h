/************************************************************************
Copyright (C) 2003-2014 STMicroelectronics. All Rights Reserved.

This file is part of the Streaming Engine.

Streaming Engine is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License version 2 as
published by the Free Software Foundation.

Streaming Engine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with Streaming Engine; see the file COPYING.  If not, write to the Free
Software Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307,
USA.

The Streaming Engine Library may alternatively be licensed under a
proprietary license from ST.
************************************************************************/

#ifndef H_CODEC_MME_VIDEO_UTILS
#define H_CODEC_MME_VIDEO_UTILS

#include "new.h"

// Define the depth of the fifo for PP (correlated with PP buffering)
// It is defined so that the limiting factor is the PP pool size (for UHD)
#define MAX_FRAME_NB_IN_PP_RING 130

typedef enum
{
    ppActionNull = 0,
    ppActionCallOutputPartialDecodeBuffers,
    ppActionCallDiscardQueuedDecodes,
    ppActionCallReleaseReferenceFrame,
    ppActionPassOnFrame,
    ppActionPassOnPreProcessedFrame,
    ppActionTermination,
    ppActionEnterDiscardMode
} ppAction_t;

template <typename T>
struct ppFrame_t
{
    ppFrame_t()
        : Used(false)
        , FakeCmd(false)
        , Action(ppActionNull)
        , DecodeFrameIndex(0)
        , data() {}

    ppFrame_t(ppAction_t action, unsigned int decodeframeindex)
        : Used(false)
        , FakeCmd(false)
        , Action(action)
        , DecodeFrameIndex(decodeframeindex)
        , data() {}

    ~ppFrame_t() {}

    bool           Used;
    bool           FakeCmd;
    ppAction_t     Action;
    unsigned int   DecodeFrameIndex;
    T              data;
};

// ppFramesRing_c is meant for thread-safe communication between codec
// input and PP HW output stage (intermediate process). Each time a frame
// is sent to PP HW, the corresponding parameters are inserted into
// ppFrameRing (struct of type T).
// In order to keep sequentiality between tasks, some "fake" tasks
// are handled in this ring to trigger specific action (discard
// queued tasks, output partial buffers or release reference frames)
// Fake tasks are inserted one at a time to block the input until
// previous one has been processed (useful for fast trickmodes and
// seek operations).
// Ring designed for one producer only, insert / extract checks on emptyness
// event must be re-implemented in case of multi-producer scenarii.
template <typename T>
class ppFramesRing_c
{
public:
    ppFramesRing_c(int maxSize, int limitTaskNb = 0);
    int FinalizeInit();
    ~ppFramesRing_c();

    bool IsEmpty() const;
    bool CheckRefListDecodeIndex(int idx);

    int Insert(struct ppFrame_t<T> *ppFrame);
    int InsertFakeEntry(int frameIdx, ppAction_t action);
    int InsertFakeEntryHead(int frameIdx, ppAction_t action);
    int Extract(struct ppFrame_t<T> *ppFrame);
    int GetSize() { return mSize; }

private:
    void IncPtr(ppFrame_t<T> **dataPtr);
    void DecPtr(ppFrame_t<T> **dataPtr);
    ppFrame_t<T> *GetPrevData(ppFrame_t<T> *dataPtr) const;
    bool IsEmptyLocked() const;
    bool IsFullLocked() const;
    int WaitForFifoAvailabilityLocked();

    struct ppFrame_t<T> *mData;
    struct ppFrame_t<T> *mRData;
    struct ppFrame_t<T> *mWData;
    int mSize;
    int mFakeTaskNb;
    int mMaxSize;
    int mLimitTaskNb;
    OS_Semaphore_t mSemaLimitTaskNb;
    mutable OS_Mutex_t mLock;
    OS_Event_t mEv;
    OS_Event_t mEvFifoNotFull;
    int mMaxUsedPPBuffer;

    DISALLOW_COPY_AND_ASSIGN(ppFramesRing_c);
};

// /////////////////////////////////////////////////////////////////////////
//
// Ring for intermediate process synchro
// maxSize > maxTaskNb + maxFakeTaskNb
//
template <typename T>
ppFramesRing_c<T>::ppFramesRing_c(int maxSize, int limitTaskNb)
    : mData(NULL)
    , mRData(NULL)
    , mWData(NULL)
    , mSize(0)
    , mFakeTaskNb(0)
    , mMaxSize(maxSize)
    , mLimitTaskNb(limitTaskNb)
    , mSemaLimitTaskNb()
    , mLock()
    , mEv()
    , mEvFifoNotFull()
    , mMaxUsedPPBuffer(0)
{
    OS_SemaphoreInitialize(&mSemaLimitTaskNb, limitTaskNb);
    OS_InitializeMutex(&mLock);
    OS_InitializeEvent(&mEv);
    OS_InitializeEvent(&mEvFifoNotFull);
}

template <typename T>
int ppFramesRing_c<T>::FinalizeInit()
{
    mData = new struct ppFrame_t<T>[mMaxSize];
    if (mData == NULL)
    {
        SE_ERROR("Failed to allocate ppFramesRing data\n");
        return -1;
    }

    mRData = mData;
    mWData = mData;

    return 0;
}

template <typename T>
ppFramesRing_c<T>::~ppFramesRing_c()
{
    SE_INFO(group_decoder_video, "mMaxUsedPPBuffer: %d\n", mMaxUsedPPBuffer);
    OS_TerminateEvent(&mEv);
    OS_TerminateEvent(&mEvFifoNotFull);
    OS_TerminateMutex(&mLock);
    OS_SemaphoreTerminate(&mSemaLimitTaskNb);

    delete [] mData;
}

template <typename T>
bool ppFramesRing_c<T>::IsEmpty() const
{
    OS_LockMutex(&mLock);
    bool ret = (mSize == 0);
    OS_UnLockMutex(&mLock);
    return ret;
}

// Must be called in critical section
template <typename T>
bool ppFramesRing_c<T>::IsEmptyLocked() const
{
    OS_AssertMutexHeld(&mLock);
    return (mSize == 0);
}

// Must be called in critical section
template <typename T>
bool ppFramesRing_c<T>::IsFullLocked() const
{
    OS_AssertMutexHeld(&mLock);
    return (mSize == mMaxSize);
}

// Increment pointer, or reset it to base pointer
// if mMaxSize is reached.
// Must be called in critical section
template <typename T>
inline void ppFramesRing_c<T>::IncPtr(ppFrame_t<T> **dataPtr)
{
    OS_AssertMutexHeld(&mLock);
    if (*dataPtr == mData + mMaxSize - 1)
    {
        *dataPtr = mData;
    }
    else
    {
        (*dataPtr)++;
    }
}
// Decrement pointer, or reset it to base pointer
// if mMaxSize is reached.
// Must be called in critical section
template <typename T>
inline void ppFramesRing_c<T>::DecPtr(ppFrame_t<T> **dataPtr)
{
    OS_AssertMutexHeld(&mLock);
    if (*dataPtr == mData)
    {
        *dataPtr =  mData + mMaxSize - 1;
    }
    else
    {
        (*dataPtr)--;
    }
}

// Return ptr on previous data stored
// Must be called in critical section
template <typename T>
inline ppFrame_t<T> *ppFramesRing_c<T>::GetPrevData(ppFrame_t<T> *dataPtr) const
{
    OS_AssertMutexHeld(&mLock);
    if (dataPtr == mData)
    {
        return mData + mMaxSize - 1;
    }
    else
    {
        return dataPtr - 1;
    }
}

// Must be called in critical section
template <typename T>
int ppFramesRing_c<T>::WaitForFifoAvailabilityLocked()
{
    OS_AssertMutexHeld(&mLock);
    int ret = 0;
    if (IsFullLocked())
    {
        OS_ResetEvent(&mEvFifoNotFull);
        OS_UnLockMutex(&mLock);
        ret = OS_WaitForEventInterruptible(&mEvFifoNotFull, OS_INFINITE);
        OS_LockMutex(&mLock);
        // Test IsFullLocked is required !
        if ((ret != OS_NO_ERROR) || (IsFullLocked()))
        {
            SE_DEBUG(group_decoder_video, "ppFramesRing full timeout or interrupted\n");
            return -1;
        }
    }
    return 0;
}

template <typename T>
bool ppFramesRing_c<T>::CheckRefListDecodeIndex(int idx)
{
    bool found = false;

    OS_LockMutex(&mLock);
    struct ppFrame_t<T> *data = mRData;
    while (data != mWData)
    {
        if (data->Used)
        {
            if (data->DecodeFrameIndex == idx)
            {
                found = true;
                break;
            }
        }
        IncPtr(&data);
    }
    OS_UnLockMutex(&mLock);
    return found;
}

// Blocking in case fifo is full
template <typename T>
int ppFramesRing_c<T>::Insert(ppFrame_t<T> *ppFrame)
{
    if (mLimitTaskNb > 0)
    {
        OS_SemaphoreWaitAuto(&mSemaLimitTaskNb);
    }
    OS_LockMutex(&mLock);
    int ret = WaitForFifoAvailabilityLocked();
    if (ret < 0)
    {
        SE_ERROR("Failed to insert PP frame (ring full)\n");
        goto bail;
    }

    ret = 0;
    // Copy ppFrame in internal mData array
    *mWData = *ppFrame;
    mWData->Used = true;
    mWData->FakeCmd = false;
    mSize++;
    if (mSize - mFakeTaskNb > mMaxUsedPPBuffer)
    {
        mMaxUsedPPBuffer = (mSize - mFakeTaskNb);
    }
    IncPtr(&mWData);

bail:
    OS_UnLockMutex(&mLock);
    OS_SetEventInterruptible(&mEv);
    return ret;
}

// Blocking in case fifo is full
template <typename T>
int ppFramesRing_c<T>::InsertFakeEntry(int frameIdx, ppAction_t action)
{
    OS_LockMutex(&mLock);
    int ret = WaitForFifoAvailabilityLocked();
    if (ret < 0)
    {
        SE_ERROR("Failed to insert PP frame (ring full)\n");
        goto bail;
    }

    ret = 0;
    // Check previous and warn when adding the same new task
    if (mSize > 1)
    {
        struct ppFrame_t<T> *tmp = GetPrevData(mWData);
        if ((tmp->Used == true) && (tmp->FakeCmd == true) &&
            (tmp->Action == action) && (tmp->DecodeFrameIndex == frameIdx))
        {
            SE_INFO(group_decoder_video, "Inserting SAME fake PP task as the previous one (action:%d frameIdx:%d)\n",
                    (int)action, frameIdx);
        }
    }

    new(mWData) ppFrame_t<T>(action, frameIdx);
    mWData->Used = true;
    mWData->FakeCmd = true;
    mSize++;
    mFakeTaskNb++;
    IncPtr(&mWData);
    SE_VERBOSE(group_decoder_video, "Inserted fake action: %d frameIdx:%d (mFakeTaskNb:%d size=%d)\n",
               (int)action, frameIdx, mFakeTaskNb, mSize);

bail:
    OS_UnLockMutex(&mLock);
    OS_SetEventInterruptible(&mEv);
    return ret;
}


// Blocking in case fifo is full
template <typename T>
int ppFramesRing_c<T>::InsertFakeEntryHead(int frameIdx, ppAction_t action)
{
    OS_LockMutex(&mLock);
    int ret = WaitForFifoAvailabilityLocked();
    if (ret < 0)
    {
        SE_ERROR("Failed to insert PP frame (ring full)\n");
        goto bail;
    }

    ret = 0;

    // Decremented Read Pointer first,because want to insert on head
    DecPtr(&mRData);


    new(mRData) ppFrame_t<T>(action, frameIdx);
    mRData->Used = true;
    mRData->FakeCmd = true;
    mSize++;
    mFakeTaskNb++;
    SE_VERBOSE(group_decoder_video, "Inserted head fake action: %d frameIdx:%d (mFakeTaskNb:%d size=%d)\n",
               (int)action, frameIdx, mFakeTaskNb, mSize);

bail:
    OS_UnLockMutex(&mLock);
    OS_SetEventInterruptible(&mEv);
    return ret;
}





template <typename T>
int ppFramesRing_c<T>::Extract(ppFrame_t<T> *ppFrame)
{
    int ret = 0;

    OS_LockMutex(&mLock);
    if (IsEmptyLocked())
    {
        OS_ResetEvent(&mEv);
        OS_UnLockMutex(&mLock);
        // Timeout is arbitrary set to 100ms to avoid useless context changes (could be set to OS_INFINITE)
        ret = OS_WaitForEventInterruptible(&mEv, 100);
        OS_LockMutex(&mLock);
        // Test IsEmpty is required !
        if ((ret != OS_NO_ERROR) || (IsEmptyLocked()))
        {
            SE_DEBUG(group_decoder_video, "ppFramesRing extract timeout or interrupted\n");
            ret = -1;
            goto bail;
        }
    }
    ret = 0;
    // Copy data to avoid potential race conditions at the end of IntermediateProcess
    *ppFrame = *mRData;
    mSize--;
    if (ppFrame->FakeCmd)
    {
        mFakeTaskNb--;
        SE_VERBOSE(group_decoder_video, "Extracted fake action: %d (mFakeTaskNb:%d size=%d)\n",
                   (int)ppFrame->Action, mFakeTaskNb, mSize);
    }
    else
    {
        OS_SemaphoreSignal(&mSemaLimitTaskNb);
    }

    IncPtr(&mRData);

    SE_VERBOSE(group_decoder_video, "Extracted action: %d (size=%d PPbufferNb=%d mLimitTaskNb=%d)\n",
               ppFrame->Action, mSize, mSize - mFakeTaskNb, mLimitTaskNb);
bail:
    OS_UnLockMutex(&mLock);
    OS_SetEventInterruptible(&mEvFifoNotFull);
    return ret;
}

#endif //H_CODEC_MME_VIDEO_UTILS
