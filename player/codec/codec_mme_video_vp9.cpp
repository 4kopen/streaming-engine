/************************************************************************
Copyright (C) 2003-2014 STMicroelectronics. All Rights Reserved.

This file is part of the Streaming Engine.

Streaming Engine is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License version 2 as
published by the Free Software Foundation.

Streaming Engine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with Streaming Engine; see the file COPYING.  If not, write to the Free
Software Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307,
USA.

The Streaming Engine Library may alternatively be licensed under a
proprietary license from ST.
************************************************************************/

#include "vp9decapi.h"
#include "vp9.h"
#include "codec_mme_video_vp9.h"

#undef TRACE_TAG
#define TRACE_TAG "Codec_MmeVideoVp9_c"

//{{{
typedef struct Vp9CodecStreamParameterContext_s
{
    CodecBaseStreamParameterContext_t   BaseContext;
} Vp9CodecStreamParameterContext_t;

typedef struct Vp9CodecDecodeContext_s
{
    CodecBaseDecodeContext_t            BaseContext;
} Vp9CodecDecodeContext_t;

#define BUFFER_VP9_CODEC_STREAM_PARAMETER_CONTEXT               "Vp9CodecStreamParameterContext"
#define BUFFER_VP9_CODEC_STREAM_PARAMETER_CONTEXT_TYPE {BUFFER_VP9_CODEC_STREAM_PARAMETER_CONTEXT, BufferDataTypeBase, AllocateFromOSMemory, 32, 0, true, true, sizeof(Vp9CodecStreamParameterContext_t)}
static BufferDataDescriptor_t                   Vp9CodecStreamParameterContextDescriptor = BUFFER_VP9_CODEC_STREAM_PARAMETER_CONTEXT_TYPE;

#define BUFFER_VP9_CODEC_DECODE_CONTEXT         "Vp9CodecDecodeContext"
#define BUFFER_VP9_CODEC_DECODE_CONTEXT_TYPE    {BUFFER_VP9_CODEC_DECODE_CONTEXT, BufferDataTypeBase, AllocateFromOSMemory, 32, 0, true, true, sizeof(Vp9CodecDecodeContext_t)}
static BufferDataDescriptor_t                   Vp9CodecDecodeContextDescriptor        = BUFFER_VP9_CODEC_DECODE_CONTEXT_TYPE;

//}}}

Codec_MmeVideoVp9_c::Codec_MmeVideoVp9_c()
    : mVp9DecInst(NULL)
    , mOutputPort(NULL)
    , mDecInfo()
    , mDecInput()
    , mDecOutput()
    , mPictureNumber(1)
    , mDecPicture()
    , mVp9DecodeBufferInfo()
    , mEndOfStreamDone(false)
    , dscale_cfg()
    , mDisplayFrameIndex(0)
    , isFirstTimeConfigure(true)
    , StartCodeList(NULL)
    , mFrameCount(0)
    , mFrameSizes()
    , mHeaderBuffer(NULL)
    , mLock()
{
    OS_InitializeMutex(&mLock);
    Configuration.CodecName                           = "Vp9 video";
    Configuration.StreamParameterContextCount          = 1;
    Configuration.StreamParameterContextDescriptor      = &Vp9CodecStreamParameterContextDescriptor;
    Configuration.DecodeContextCount                   = 0;
    Configuration.DecodeContextDescriptor              = &Vp9CodecDecodeContextDescriptor;
    Configuration.MaxDecodeIndicesPerBuffer            = 2;
    Configuration.SliceDecodePermitted                 = false;
    Configuration.ShrinkCodedDataBuffersAfterDecode     = true;
    Configuration.AddressingMode                       = PhysicalAddress;
    Configuration.DecimatedDecodePermitted              = true;
}

enum DecRet  Codec_MmeVideoVp9_c::ManageFrameToDisplay()
{
    enum DecRet Vp9Ret;
    struct Vp9DecPicture localDecPicture;
    OS_LockMutex(&mLock);
    do
    {
        memset((void *)&localDecPicture, 0, sizeof(localDecPicture));
        Vp9Ret =  Vp9DecNextPicture(mVp9DecInst, &localDecPicture);
        if (Vp9Ret == DEC_PIC_RDY)
        {
            ParsedFrameParameters_t *LocalParsedParameters;
            Buffer_t Vp9DecodeBuffer;
            SE_ASSERT(localDecPicture.index < VP9DEC_MAX_PIC_BUFFERS);

            /* Get correct decode buffer ptr on the basis on index */
            Vp9DecGetDecodeBufferPtr(mVp9DecInst, localDecPicture.index, (void **)&Vp9DecodeBuffer);
            SE_DEBUG(group_decoder_video, "DEC_PIC_RDY decbuff : 0x%x arraydec : 0x%x index : %d coded : 0x%d\n", (unsigned int)Vp9DecodeBuffer,
                     (unsigned int)mVp9DecodeBufferInfo[localDecPicture.index].DecodeBuffer, localDecPicture.index, (unsigned int)mVp9DecodeBufferInfo[localDecPicture.index].CodedBuffer);
            if (mVp9DecodeBufferInfo[localDecPicture.index].DecodeBuffer == NULL)
            {
                SE_FATAL("DecodeBuffer NULL, index : %d DecBuff : 0x%x\n", localDecPicture.index, (unsigned int)Vp9DecodeBuffer);
            }
            SE_ASSERT(mVp9DecodeBufferInfo[localDecPicture.index].IsDecodeBufferPushedForManifestation == false);

            mDecPicture[localDecPicture.index] = localDecPicture;
            mVp9DecodeBufferInfo[localDecPicture.index].IsDecodeBufferPushedForManifestation = true;

            // managing display index
            Vp9DecodeBuffer->ObtainMetaDataReference(Player->MetaDataParsedFrameParametersReferenceType, (void **)(&LocalParsedParameters));
            LocalParsedParameters->DisplayFrameIndex = mDisplayFrameIndex++;
            Vp9DecodeBuffer->IncrementReferenceCount();
            mOutputPort->Insert((uintptr_t)Vp9DecodeBuffer);
            Stream->Statistics().FrameCountFromCodec++;
        }

        if (Vp9Ret == DEC_END_OF_STREAM)
        {
            SE_INFO(group_decoder_video, "DEC_END_OF_STREAM received\n");
        }
    }
    while (Vp9Ret == DEC_PIC_RDY);
    OS_UnLockMutex(&mLock);

    return  Vp9Ret;
}

Codec_MmeVideoVp9_c::~Codec_MmeVideoVp9_c()
{
    Halt();
    OS_TerminateMutex(&mLock);
}

bool   Codec_MmeVideoVp9_c::IsDecimationValueSupported(int DecimationPolicy)
{
    switch (DecimationPolicy)
    {
    //Below decimations supported for this codec
    case PolicyValueDecimateDecoderOutputHalf:
    case PolicyValueDecimateDecoderOutputQuarter:
    case PolicyValueDecimateDecoderOutputH2V4:
    case PolicyValueDecimateDecoderOutputH2V8:
    case PolicyValueDecimateDecoderOutputH4V4:
    case PolicyValueDecimateDecoderOutputH4V8:
    case PolicyValueDecimateDecoderOutputH8V2:
    case PolicyValueDecimateDecoderOutputH8V4:
    case PolicyValueDecimateDecoderOutputH8V8:
        return true;
    default:
        SE_WARNING_ONCE("Decimation value not supported\n");
        return false;
    }
}

void   Codec_MmeVideoVp9_c::GetDecimationValues(struct DecDownscaleCfg *dscale_cfg)
{
    int  Decimate = Player->PolicyValue(Playback, Stream, PolicyDecimateDecoderOutput);
    //Check if the decimation value is supported by the codec
    if (!IsDecimationValueSupported(Decimate))
    {
        SE_WARNING_ONCE("(%s) - Decimation value not supported, would be disabled\n", Configuration.CodecName);
        Decimate = PolicyValueDecimateDecoderOutputDisabled;
    }

    //Policy values arranged in switch case blocks
    switch (Decimate)
    {
    case PolicyValueDecimateDecoderOutputHalf:
        dscale_cfg->down_scale_x = 2; //H
        dscale_cfg->down_scale_y = 2; //V
        break;
    case PolicyValueDecimateDecoderOutputQuarter:
        dscale_cfg->down_scale_x = 4; //H
        dscale_cfg->down_scale_y = 2; //V
        break;
    case PolicyValueDecimateDecoderOutputH2V4:
        dscale_cfg->down_scale_x = 2; //H
        dscale_cfg->down_scale_y = 4; //V
        break;
    case PolicyValueDecimateDecoderOutputH2V8:
        dscale_cfg->down_scale_x = 2;
        dscale_cfg->down_scale_y = 8;
        break;
    case PolicyValueDecimateDecoderOutputH4V4:
        dscale_cfg->down_scale_x = 4;
        dscale_cfg->down_scale_y = 4;
        break;
    case PolicyValueDecimateDecoderOutputH4V8:
        dscale_cfg->down_scale_x = 4;
        dscale_cfg->down_scale_y = 8;
        break;
    case PolicyValueDecimateDecoderOutputH8V2:
        dscale_cfg->down_scale_x = 8;
        dscale_cfg->down_scale_y = 2;
        break;
    case PolicyValueDecimateDecoderOutputH8V4:
        dscale_cfg->down_scale_x = 8;
        dscale_cfg->down_scale_y = 4;
        break;
    case PolicyValueDecimateDecoderOutputH8V8:
        dscale_cfg->down_scale_x = 8;
        dscale_cfg->down_scale_y = 8;
        break;
    default:
        dscale_cfg->down_scale_x = 1;
        dscale_cfg->down_scale_y = 1;
        SE_VERBOSE(group_decoder_video, "(%s) - Continue without decimation\n", Configuration.CodecName);
        break;
    }
    SE_DEBUG(group_decoder_video, "xscale : %d  yscale : %d\n", dscale_cfg->down_scale_x, dscale_cfg->down_scale_y);
}

CodecStatus_t   Codec_MmeVideoVp9_c::ConnectVp9Decoder()
{
    GetDecimationValues(& dscale_cfg);
    Vp9DecBuild vp9buildinfo = Vp9DecGetBuild();
    SE_INFO(group_decoder_video, "VP9 build information hw build verion : %d vp9_supported : %d\n", vp9buildinfo.hw_build, vp9buildinfo.hw_config.vp9_support);

    enum DecRet Vp9Ret = Vp9DecInit(&mVp9DecInst, DISABLE_OUTPUT_REORDERING, NUM_FRAME_BUFFERS , DEC_REF_FRM_RASTER_SCAN,
#ifdef DOWN_SCALER
                                    & dscale_cfg,
#endif
                                    DEC_OUT_FRM_RASTER_SCAN);
    if (Vp9Ret != DEC_OK)
    {
        SE_ERROR("Vp9DecInit failed (0x%x)\n", (unsigned int)Vp9Ret);
        return CodecError;
    }

    /* api to register codec pointer with current instance of vp9DecInst */
    Vp9RegisterClientPtr(mVp9DecInst, (void *)this);
    Vp9ExecuteClientMemoryInit(mVp9DecInst);
    return CodecNoError;
}

CodecStatus_t   Codec_MmeVideoVp9_c::Connect(Port_c *Port)
{
    if (Port == NULL)
    {
        SE_ERROR("Incorrect parameter\n");
        return CodecError;
    }

    //
    // Let the ancestor classes setup the framework
    //
    HandleCapabilities();
    CodecStatus_t Status = Codec_MmeVideo_c::Connect(Port);
    if (Status != CodecNoError)
    {
        SetComponentState(ComponentInError);
        return Status;
    }

    if (mOutputPort != NULL)
    {
        SE_WARNING("Port already connected\n");
    }

    mOutputPort  = Port;
    SetComponentState(ComponentRunning);
    return CodecNoError;
}

/* Its a callback function called from inside Vp9 hantro code */
int InitializeDecodeBufferPools(void *codecPtr, void **decodebufferptr, unsigned int index, unsigned int width, unsigned int height)
{
    ((Codec_MmeVideoVp9_c *)codecPtr)->AllocateComponentBuffer(decodebufferptr, index, width, height);
    return 0;
}

/* Its a callback function called from inside Vp9 hantro code */
void DeInitializeDecodeBufferPools(void *codecPtr, void **decodebufferptr)
{
    ((Codec_MmeVideoVp9_c *)codecPtr)->DeAllocateComponentBuffer(decodebufferptr);
}


void   Codec_MmeVideoVp9_c::DeAllocateComponentBuffer(void **decodebufferptr)
{
    Stream->GetDecodeBufferManager()->ReleaseBuffer((Buffer_t)*decodebufferptr, false);
}

void   Codec_MmeVideoVp9_c::FillOutBufferCountRequest(DecodeBufferRequest_t   *Request)
{
    Request->ManifestationBufferCount = 9;
    Request->ReferenceBufferCount     = Request->ManifestationBufferCount;
    SE_DEBUG(group_decoder_video, "%p Set Manifestation buffer count:%d reference buffer count:%d frame size %d x %d\n",
             this , Request->ManifestationBufferCount , Request->ReferenceBufferCount, Request->Dimension[0] , Request->Dimension[1]);
}


void   Codec_MmeVideoVp9_c::AllocateComponentBuffer(void **ptr, unsigned int index, unsigned int width, unsigned int height)
{
    DecodeBufferRequest_t    BufferRequest;

    ParsedVideoParameters->Content.DecodeHeight = height;
    ParsedVideoParameters->Content.DecodeWidth = width;
    ParsedVideoParameters->Content.DisplayHeight = height;
    ParsedVideoParameters->Content.DisplayWidth = width;
    ParsedVideoParameters->Content.Height = height;
    ParsedVideoParameters->Content.Width = width;

    (void)index; // warning removal

    //
    // Get a buffer
    //
    memset(&BufferRequest, 0, sizeof(DecodeBufferRequest_t));
    CodecStatus_t Status = FillOutDecodeBufferRequest(&BufferRequest);
    if (Status != CodecNoError)
    {
        SE_ERROR("(%s) - Failed to fill out a buffer request structure\n", Configuration.CodecName);
        return;
    }

    DecodeBufferManagerStatus_t DbStatus = Stream->GetDecodeBufferManager()->GetDecodeBuffer(&BufferRequest, &CurrentDecodeBuffer);
    if (DbStatus != DecodeBufferManagerNoError)
    {
        // TODO(pht) caller shall handle DecodeBufferManagerFailedToAllocateComponents case and mark stream unplayable
        // but not possible with current design => doing it now
        if (Status == DecodeBufferManagerFailedToAllocateComponents)
        {
            SE_ERROR("(%s) - Failed to obtain a decode buffer from the decode buffer manager : components allocation failure\n",
                     Configuration.CodecName);
            // raise the event to signal stream unplayable
            Stream->MarkUnPlayable(STM_SE_PLAY_STREAM_MSG_REASON_CODE_INSUFFICIENT_MEMORY, true);
        }
        else
        {
            SE_ERROR("(%s) - Failed to obtain a decode buffer from the decode buffer manager\n", Configuration.CodecName);
        }
        return;
    }

    if (CurrentDecodeBuffer)
    {
        CurrentDecodeBuffer->TransferOwnership(IdentifierCodec);
        *ptr = (void *)CurrentDecodeBuffer;
    }
}

void AllocateActualBuffer(void *codecPtr, void *decodebufferptr, unsigned int *phy, unsigned int **virt, unsigned int *size, unsigned int type)
{
    ((Codec_MmeVideoVp9_c *)codecPtr)->AllocateActualBufferPointer(decodebufferptr, phy, virt, size, type);
}

void Codec_MmeVideoVp9_c::AllocateActualBufferPointer(void *decodebufferptr, unsigned int *phy, unsigned int **virt, unsigned int *size, unsigned int type)
{
    DecodeBufferComponentType_t component_type;
    switch (type)
    {
    case DECODECOPY_LUMA : component_type = VideoDecodeCopy; break; /* luma */
    case DECODECOPY_CHROMA : component_type = VideoDecodeCopy; break; /* chroma */
    case MBSTRUCT_MVS : component_type = VideoMacroblockStructure; break;
    case DISPLAY_LUMA : component_type = PrimaryManifestationComponent; break; /* luma */
    case DISPLAY_CHROMA : component_type = PrimaryManifestationComponent; break; /* luma */
    case DECIMATED_LUMA : component_type = DecimatedManifestationComponent; break; /* luma */
    case DECIMATED_CHROMA : component_type = DecimatedManifestationComponent; break; /* luma */
    default: component_type = UndefinedComponent; break;
    }

    if (type == DECODECOPY_LUMA || type == DISPLAY_LUMA || type == DECIMATED_LUMA)
    {
        *phy = (unsigned int)Stream->GetDecodeBufferManager()->Luma((Buffer_t)decodebufferptr, component_type);
        *size = (unsigned int)Stream->GetDecodeBufferManager()->LumaSize((Buffer_t)decodebufferptr, component_type);
        if (virt != NULL) { *virt = (unsigned int *)Stream->GetDecodeBufferManager()->Luma((Buffer_t)decodebufferptr, component_type, CachedAddress); }
    }

    if (type == DECODECOPY_CHROMA || type == DISPLAY_CHROMA || type == DECIMATED_CHROMA)
    {
        *phy = (unsigned int)Stream->GetDecodeBufferManager()->Chroma((Buffer_t)decodebufferptr, component_type);
        *size = (unsigned int)Stream->GetDecodeBufferManager()->ChromaSize((Buffer_t)decodebufferptr, component_type);
        if (virt != NULL) { *virt = (unsigned int *)Stream->GetDecodeBufferManager()->Chroma((Buffer_t)decodebufferptr, component_type, CachedAddress); }
    }

    if (type == MBSTRUCT_MVS)
    {
        *phy = (unsigned int)Stream->GetDecodeBufferManager()->ComponentBaseAddress((Buffer_t)decodebufferptr, component_type);
        *size = (unsigned int)Stream->GetDecodeBufferManager()->ComponentSize((Buffer_t)decodebufferptr, component_type);
        if (virt != NULL) { *virt = (unsigned int *)Stream->GetDecodeBufferManager()->ComponentBaseAddress((Buffer_t)decodebufferptr, component_type, CachedAddress); }
    }
}

CodecStatus_t   Codec_MmeVideoVp9_c::OutputPartialDecodeBuffers()
{
    if (mEndOfStreamDone || (mVp9DecInst == NULL))
    {
        return CodecNoError;
    }
    SE_DEBUG(group_decoder_video, "To call Vp9DecEndOfStreamf\n");
    enum DecRet Vp9Ret = Vp9DecEndOfStream(mVp9DecInst);
    if (Vp9Ret != DEC_OK)
    {
        SE_ERROR("Vp9DecEndOfStream failed :%d\n", Vp9Ret);
        return CodecError;
    }
    while (ManageFrameToDisplay() != DEC_END_OF_STREAM)
    {
        OS_SleepMilliSeconds(1000);
    }
    mEndOfStreamDone = true;

    OS_LockMutex(&mLock);
    for (int index = 0; index < VP9DEC_MAX_PIC_BUFFERS; index++)
    {
        if ((!mVp9DecodeBufferInfo[index].IsDecodeBufferPushedForManifestation) && (mVp9DecodeBufferInfo[index].DecodeBuffer != NULL))
        {
            SE_DEBUG(group_decoder_video, "Buffer to remove : 0x%x index : %d\n", (unsigned int)mVp9DecodeBufferInfo[index].DecodeBuffer, index);
            Buffer_t OriginalCodedFrameBuffer = mVp9DecodeBufferInfo[index].CodedBuffer;
            Buffer_t DecodeBuffer = mVp9DecodeBufferInfo[index].DecodeBuffer;
            SE_DEBUG(group_decoder_video, "Freeing coded buffer from output: 0x%x decode buffer : 0x%x\n", (unsigned int)OriginalCodedFrameBuffer, (unsigned int)DecodeBuffer);
            SE_ASSERT(OriginalCodedFrameBuffer != NULL);
            DecodeBuffer->DetachBuffer(OriginalCodedFrameBuffer);
            OriginalCodedFrameBuffer->DecrementReferenceCount();
            DecodeBuffer->DetachMetaData(Player->MetaDataParsedFrameParametersReferenceType);
            mVp9DecodeBufferInfo[index].CodedBuffer = NULL;
            mVp9DecodeBufferInfo[index].DecodeBuffer = NULL;
        }
    }
    OS_UnLockMutex(&mLock);

    return CodecNoError;
}

CodecStatus_t   Codec_MmeVideoVp9_c::Halt()
{
    Vp9DecRelease(mVp9DecInst);
    mVp9DecInst = NULL;
    isFirstTimeConfigure = true;
    mEndOfStreamDone = false;
    return CodecNoError;
}

CodecStatus_t   Codec_MmeVideoVp9_c::ReleaseDecodeBuffer(Buffer_t Buffer)
{
    unsigned int index;
    OS_LockMutex(&mLock);
    for (index = 0; index < VP9DEC_MAX_PIC_BUFFERS; index++)
    {
        if (Buffer == mVp9DecodeBufferInfo[index].DecodeBuffer) { break; }
    }
    if (index == VP9DEC_MAX_PIC_BUFFERS)
    {
        SE_ERROR("Implementation issue, release buffer not found\n");
        OS_UnLockMutex(&mLock);
        return CodecError;
    }
    enum DecRet Vp9Ret = Vp9DecPictureConsumed(mVp9DecInst, &mDecPicture[index]);
    if (Vp9Ret != DEC_OK)
    {
        SE_ERROR("Vp9DecPictureConsumed : 0x%x status : 0x%x\n", (unsigned int)Buffer, (unsigned int)Vp9Ret);
        OS_UnLockMutex(&mLock);
        return CodecError;
    }

    Buffer_t OriginalCodedFrameBuffer = mVp9DecodeBufferInfo[index].CodedBuffer;
    SE_ASSERT(OriginalCodedFrameBuffer != NULL);
    Buffer->DetachBuffer(OriginalCodedFrameBuffer);
    Buffer->DetachMetaData(Player->MetaDataParsedFrameParametersReferenceType);
    OriginalCodedFrameBuffer->DecrementReferenceCount();
    Buffer->DecrementReferenceCount();
    //resetting the fields
    SE_DEBUG(group_decoder_video, "Buffer released : 0x%x index : %d coded : 0x%x\n", (unsigned int)mVp9DecodeBufferInfo[index].DecodeBuffer, index, (unsigned int)mVp9DecodeBufferInfo[index].CodedBuffer);
    mVp9DecodeBufferInfo[index].DecodeBuffer = NULL;
    mVp9DecodeBufferInfo[index].CodedBuffer = NULL;
    mVp9DecodeBufferInfo[index].IsDecodeBufferPushedForManifestation = false;
    OS_UnLockMutex(&mLock);
    return CodecNoError;
}

CodecStatus_t   Codec_MmeVideoVp9_c::SendMMEDecodeCommand()
{
    return CodecNoError;
}

enum DecRet Codec_MmeVideoVp9_c::vp9_decode(void *inst, struct Vp9DecInput *vp9_input,
                                            struct Vp9DecOutput *vp9_output)
{
    enum DecRet rv = DEC_OK;
    for (int i = 0; i < mFrameCount; i++)
    {
        ManageFrameToDisplay();
        vp9_input->data_len = mFrameSizes[i];
        Stream->Statistics().FrameCountLaunchedDecode++;
        unsigned int sw_decode_duration;
        rv = Vp9DecDecodeWrapper(inst, vp9_input, vp9_output, &sw_decode_duration, &PictureDecodeTime);
        SE_DEBUG(group_decoder_video, "Software dec time : %d us Hardware Dec time : %d us\n", sw_decode_duration, PictureDecodeTime);
        Codec_MmeVideo_c::FillDecodeTimeStatistics();
        /* Headers decoded or error occurred */
        if (rv == DEC_HDRS_RDY || rv != DEC_PIC_DECODED) { break; }
        if (Collator_c::GetCollatorOffloadTuneable() != CollatorNoOffload)
        {
            vp9_input->stream += MAX_VP9_FRAME_HEADER_SIZE;
        }
        else
        {
            vp9_input->stream += mFrameSizes[i];
        }
        vp9_input->stream_bus_address += mFrameSizes[i];
    }
    return rv;
}

enum DecRet Codec_MmeVideoVp9_c::HandleDecode(struct Vp9DecInput *DecInput)
{
    enum DecRet Vp9Ret = DEC_HDRS_RDY;
    while (Vp9Ret == DEC_HDRS_RDY)
    {
        Vp9Ret = vp9_decode((void *)mVp9DecInst, DecInput, &mDecOutput);
        if (Vp9Ret == DEC_HDRS_RDY)
        {
            SE_DEBUG(group_decoder_video, "DEC_HDRS_RDY event reveived\n");
            enum DecRet Status = Vp9DecGetInfo(mVp9DecInst, &mDecInfo);
            if (Status != DEC_OK)
            {
                SE_ERROR("Vp9DecGetInfo failed (%d)\n", Status);
                Vp9Ret = DEC_STRM_ERROR;
            }
            else
            {
                ParsedVideoParameters->Content.DecodeHeight = mDecInfo.coded_height;
                ParsedVideoParameters->Content.DecodeWidth = mDecInfo.coded_width;
                ParsedVideoParameters->Content.DisplayHeight = mDecInfo.frame_height;
                ParsedVideoParameters->Content.DisplayWidth = mDecInfo.frame_width;
                ParsedVideoParameters->Content.Height = mDecInfo.frame_height;
                ParsedVideoParameters->Content.Width = mDecInfo.frame_width;
                ParsedVideoParameters->DisplayCount[0]                      = 1;
                ParsedVideoParameters->DisplayCount[1]                      = 0;
                ParsedVideoParameters->Content.PixelAspectRatio             = 1;
                ParsedVideoParameters->PictureStructure                     = StructureFrame;
                ParsedVideoParameters->InterlacedFrame                      = false;
                ParsedVideoParameters->TopFieldFirst                        = true;
                ParsedVideoParameters->PanScanCount                         = 0;
            }
            Stream->Statistics().FrameCountLaunchedDecode--;
        }
    }
    return Vp9Ret;
}

CodecStatus_t   Codec_MmeVideoVp9_c::Input(Buffer_t                  CodedBuffer)
{
    unsigned int CodedLength = 0;
    if (!Configuration.IgnoreFindCodedDataBuffer)
    {
        BufferStatus_t BufStatus = CodedBuffer->ObtainDataReference(NULL, &CodedLength, (void **)(NULL), Configuration.AddressingMode);
        if ((BufStatus != BufferNoError) && (BufStatus != BufferNoDataAttached))
        {
            SE_ERROR("(%s) - Unable to obtain data reference - 0x%x\n", Configuration.CodecName, (unsigned int)BufStatus);
            return CodecError;
        }
    }

    if (CodedLength == 0)
    {
        OutputPartialDecodeBuffers();
        return Codec_MmeBase_c::Input(CodedBuffer);
    }
    else
    {
        mEndOfStreamDone = false;
    }

    CodecStatus_t Status = Codec_MmeBase_c::Input(CodedBuffer);
    if (Status != CodecNoError)
    {
        return Status;
    }

    if ((isFirstTimeConfigure == false) && (mVp9DecInst == NULL))
    {
        SE_ERROR("Invalid configuration\n");
        return CodecError;
    }
    if (isFirstTimeConfigure)
    {
        isFirstTimeConfigure = false;
        Status = ConnectVp9Decoder();
        if (Status == CodecError)
        {
            SE_ERROR("ConnectVp9Decoder failed\n");
            return CodecError;
        }
    }

    // the number of elementary frames and each elementary frame offset would be retrieved
    // from StartCodeList structure members
    CodedBuffer->ObtainMetaDataReference(Player->MetaDataStartCodeListType, (void **)(&StartCodeList));
    SE_ASSERT(StartCodeList != NULL);

    Buffer_t StartcodeHeadersBuffer = NULL;

    if (Collator_c::GetCollatorOffloadTuneable() != CollatorNoOffload)
    {
        CodedBuffer->ObtainAttachedBufferReference(Player->GetStartCodeHeadersBufferType(), &StartcodeHeadersBuffer);
        SE_ASSERT(StartcodeHeadersBuffer != NULL);
        StartcodeHeadersBuffer->ObtainDataReference(NULL, NULL, (void **) &mHeaderBuffer);
        SE_ASSERT(mHeaderBuffer != NULL);
        mDecInput.stream = mHeaderBuffer;
    }
    else
    {
        mDecInput.stream = CodedData_Cp;
    }
    CodedBuffer->IncrementReferenceCount();
    mDecInput.data_len = CodedDataLength;

    mDecInput.stream_bus_address = (unsigned int)CodedData;
    mDecInput.pic_id = mPictureNumber;
    mFrameCount = StartCodeList->NumberOfStartCodes;
    for (int i = 0; i < mFrameCount; i++)
    {
        mFrameSizes[i] = (unsigned int)StartCodeList->StartCodes[i];
    }

    enum DecRet Vp9Ret = HandleDecode(&mDecInput);

    if (Collator_c::GetCollatorOffloadTuneable() != CollatorNoOffload)
    {
        // detaching the header buffer as it would not be required further
        CodedBuffer->DetachBuffer(StartcodeHeadersBuffer);
    }

    switch (Vp9Ret)
    {
    case DEC_END_OF_STREAM:
        SE_DEBUG(group_decoder_video, "DEC_END_OF_STREAM received\n");
        break;

    case DEC_OK:
    case DEC_NONREF_PIC_SKIPPED:
    case DEC_STRM_PROCESSED:
        break;
    case DEC_PIC_DECODED:
        OS_LockMutex(&mLock);
        Buffer_t OriginalCodedFrameBuffer;
        Buffer_t DecodeBuffer;
        BufferStatus_t BufStatus;
        SE_DEBUG(group_decoder_video, "DEC_PIC_DECODED received with Decode Index %d\n", ParsedFrameParameters->DecodeFrameIndex);
        Stream->Statistics().FrameCountDecoded++;
        mPictureNumber++;
        unsigned int  height, width, pic_id, is_intra_frame, idx;
        Vp9DecInfoLastDecode(mVp9DecInst, &width, &height, &pic_id, &is_intra_frame, &idx);
        Vp9DecGetDecodeBufferPtr(mVp9DecInst, idx, (void **)&DecodeBuffer);

        /* During multiple seek operation current decode buffer is sometimes not pushed to manifestor
        as a result metadata is getting attached to same decode buffer */
        DecodeBuffer->ObtainAttachedBufferReference(Stream->GetCodedFrameBufferType(), &OriginalCodedFrameBuffer);
        if (OriginalCodedFrameBuffer)
        {
            DecodeBuffer->DetachBuffer(OriginalCodedFrameBuffer);
            DecodeBuffer->DetachMetaData(Player->MetaDataParsedFrameParametersReferenceType);
            OriginalCodedFrameBuffer->DecrementReferenceCount();
            SE_WARNING("Detaching already attached coded buffer:  0x%x DecodeBuffer: 0x%x\n", (unsigned int)OriginalCodedFrameBuffer, (unsigned int)DecodeBuffer);
        }

        // ParsedFrameParameters is set in base class
        SE_ASSERT(ParsedFrameParameters != NULL);
        ParsedFrameParameters->KeyFrame = is_intra_frame;
        ParsedFrameParameters->ReferenceFrame = false;
        BufStatus = DecodeBuffer->AttachMetaData(Player->MetaDataParsedFrameParametersReferenceType, UNSPECIFIED_SIZE, ParsedFrameParameters);
        if (BufStatus != BufferNoError)
        {
            SE_ERROR("(%s) - Unable to attach a reference to \"ParsedFrameParameters\" to the decode buffer\n", Configuration.CodecName);
            Status = CodecError;
            OS_UnLockMutex(&mLock);
            break;
        }

        SE_ASSERT(ParsedVideoParameters != NULL);
        ParsedVideoParameters->Content.DecodeHeight = height;
        ParsedVideoParameters->Content.DecodeWidth = width;
        ParsedVideoParameters->Content.DisplayHeight = height;
        ParsedVideoParameters->Content.DisplayWidth = width;
        ParsedVideoParameters->Content.Width = width;
        ParsedVideoParameters->Content.Height = height;

        ParsedVideoParameters_t  *TheParsedVideoParameters;
        DecodeBuffer->ObtainMetaDataReference(Player->MetaDataParsedVideoParametersType, (void **)(&TheParsedVideoParameters));
        memcpy(TheParsedVideoParameters, ParsedVideoParameters, sizeof(ParsedVideoParameters_t));
        DecodeBuffer->AttachBuffer(CodedFrameBuffer);

        mVp9DecodeBufferInfo[idx].DecodeBuffer = DecodeBuffer;
        mVp9DecodeBufferInfo[idx].IsDecodeBufferPushedForManifestation = false;
        mVp9DecodeBufferInfo[idx].CodedBuffer = CodedFrameBuffer;
        SE_VERBOSE(group_decoder_video, "End of DEC_PIC_DECODED DecBuff : 0x%x Coded : 0x%x index : %d\n", (unsigned int)DecodeBuffer, (unsigned int)CodedFrameBuffer, idx);
        OS_UnLockMutex(&mLock);

        ManageFrameToDisplay();
        break;

    case DEC_ADVANCED_TOOLS:
        break;
    case DEC_PENDING_FLUSH:
        SE_DEBUG(group_decoder_video, "Flush message received from decoder\n");
        break;
    case DEC_STREAM_NOT_SUPPORTED:
    case DEC_STRM_ERROR:
        SE_ERROR("Decoding failed (%d)\n", Vp9Ret);
        Status = CodecError;
        CodedBuffer->DecrementReferenceCount();
        OutputPartialDecodeBuffers();
        break;
    default:
        SE_ERROR("Some unexpected message received : %d\n", (int)Vp9Ret);
        CodedBuffer->DecrementReferenceCount();
        Status = CodecError;
        break;
    }

    return Status;
}

CodecStatus_t   Codec_MmeVideoVp9_c::VerifyMMECapabilities(unsigned int ActualTransformer)
{
    (void)ActualTransformer; // warning removal
    return CodecNoError;
}

//{{{  HandleCapabilities
// /////////////////////////////////////////////////////////////////////////
//
//      Function to deal with the returned capabilities
//      structure for an Vp9 mme transformer.
//
CodecStatus_t   Codec_MmeVideoVp9_c::HandleCapabilities()
{
    /* we need 4 buffer types in VP9 - raster/reference/decimated and macroblock */
    /* assumption that buffer pools would be created here */
    BufferFormat_t  DisplayFormat = FormatVideo420_Raster2B;
    Configuration.AddressingMode    = PhysicalAddress;
    DecodeBufferComponentElementMask_t Elements = PrimaryManifestationElement | VideoDecodeCopyElement |
                                                  DecimatedManifestationElement |
                                                  VideoMacroblockStructureElement;
    Stream->GetDecodeBufferManager()->FillOutDefaultList(DisplayFormat,
                                                         Elements,
                                                         Configuration.ListOfDecodeBufferComponents);

    Configuration.ListOfDecodeBufferComponents[0].ComponentBorders[0]   = 16;
    Configuration.ListOfDecodeBufferComponents[0].ComponentBorders[1]   = 16;
    Configuration.ListOfDecodeBufferComponents[1].ComponentBorders[0]   = 16;
    Configuration.ListOfDecodeBufferComponents[1].ComponentBorders[1]   = 16;
    Configuration.ListOfDecodeBufferComponents[2].ComponentBorders[0]   = 16;
    Configuration.ListOfDecodeBufferComponents[2].ComponentBorders[1]   = 16;

    return CodecNoError;
}

//}}}

//{{{  FillOutTransformerInitializationParameters
// /////////////////////////////////////////////////////////////////////////
//
//      Function to deal with the returned capabilities
//      structure for an Vp9 mme transformer.
//
CodecStatus_t   Codec_MmeVideoVp9_c::FillOutTransformerInitializationParameters()
{
    return CodecNoError;
}
//}}}

//{{{  SendMMEStreamParameters
CodecStatus_t Codec_MmeVideoVp9_c::SendMMEStreamParameters()
{
    return CodecNoError;
}
//}}}

//{{{  FillOutSetStreamParametersCommand
// /////////////////////////////////////////////////////////////////////////
//
//      Function to fill out the stream parameters
//      structure for an Vp9 mme transformer.
//
CodecStatus_t   Codec_MmeVideoVp9_c::FillOutSetStreamParametersCommand()
{
    return CodecNoError;
}

//}}}
//{{{  FillOutDecodeBufferRequest
// /////////////////////////////////////////////////////////////////////////
//
//      Function to extend the buffer request to incorporate the mvs sizing
//

CodecStatus_t   Codec_MmeVideoVp9_c::FillOutDecodeBufferRequest(DecodeBufferRequest_t    *BufferRequest)
{
    BufferRequest->ReferenceFrame = false;
//  BufferRequest.DecimationFactors = 0;
    BufferRequest->PixelDepth = 8;
    //BufferRequest->mIsMarkerFrame  = false;

    Codec_MmeVideo_c::FillOutDecodeBufferRequest(BufferRequest);
    // From VP9 orginal code
    // num_ctbs = ((asic_buff->width + 63) / 64) * ((asic_buff->height + 63) / 64);
    // dir_mvs_size = num_ctbs * 64 * 16; /* MVs (16 MBs / CTB * 16 bytes / MB) */
    BufferRequest->MacroblockSize = 256;
    BufferRequest->PerMacroblockMacroblockStructureSize = 64;
    BufferRequest->PerMacroblockMacroblockStructureFifoSize = 0;
    return CodecNoError;
}


//}}}
//{{{  FillOutDecodeCommand
// /////////////////////////////////////////////////////////////////////////
//
//      Function to fill out the decode parameters
//      structure for an Vp9 mme transformer.
//

CodecStatus_t   Codec_MmeVideoVp9_c::FillOutDecodeCommand()
{
    return CodecNoError;
}

//
// Low power enter method
// For CPS mode, all MME transformers must be terminated
//
CodecStatus_t Codec_MmeVideoVp9_c::LowPowerEnter()
{
    return CodecNoError;
}

//
// Low power exit method
// For CPS mode, all MME transformers must be re-initialized
//
CodecStatus_t Codec_MmeVideoVp9_c::LowPowerExit()
{
    return CodecNoError;
}

//}}}

void   Codec_MmeVideoVp9_c::CallbackFromMME(MME_Event_t Event, MME_Command_t *Command)
{
    (void)Event; // warning removal
    (void)Command; // warning removal
}

// /////////////////////////////////////////////////////////////////////////
//
//      SetModuleParameters function
//      Action  : Allows external user to set up important environmental parameters
//      Input   :
//      Output  :
//      Result  :
//

CodecStatus_t   Codec_MmeVideoVp9_c::SetModuleParameters(
    unsigned int   ParameterBlockSize,
    void          *ParameterBlock)
{
    (void)ParameterBlock; // warning removal

    if (ParameterBlockSize != sizeof(struct CodecParameterBlock_s))
    {
        SE_ERROR("Invalid parameter block\n");
        return CodecError;
    }
    return CodecNoError;
}

//}}}

