/************************************************************************
Copyright (C) 2003-2014 STMicroelectronics. All Rights Reserved.

This file is part of the Streaming Engine.

Streaming Engine is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License version 2 as
published by the Free Software Foundation.

Streaming Engine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with Streaming Engine; see the file COPYING.  If not, write to the Free
Software Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307,
USA.

The Streaming Engine Library may alternatively be licensed under a
proprietary license from ST.
************************************************************************/

#ifndef H_CODEC_MME_VIDEO_VP9
#define H_CODEC_MME_VIDEO_VP9

#include "codec_mme_video.h"
#include "vp9decapi.h"
#include "vp9inline.h"
#include "codec_mme_video_vp9_ext.h"

#undef TRACE_TAG
#define TRACE_TAG "Codec_MmeVideoVp9_c"

#define DISABLE_OUTPUT_REORDERING 0
#define NUM_FRAME_BUFFERS 4

/// The Vp9 video codec proxy.
class Codec_MmeVideoVp9_c : public Codec_MmeVideo_c
{
public:
    Codec_MmeVideoVp9_c();
    ~Codec_MmeVideoVp9_c();

    CodecStatus_t   LowPowerEnter();
    CodecStatus_t   LowPowerExit();

private:
    CodecStatus_t   HandleCapabilities();

    CodecStatus_t   FillOutTransformerInitializationParameters();
    CodecStatus_t   FillOutSetStreamParametersCommand();
    CodecStatus_t   FillOutDecodeCommand();
    CodecStatus_t   FillOutDecodeBufferRequest(DecodeBufferRequest_t    *Request);
    void            FillOutBufferCountRequest(DecodeBufferRequest_t   *Request);

    CodecStatus_t   SendMMEStreamParameters();
    CodecStatus_t   SendMMEDecodeCommand();

    CodecStatus_t   VerifyMMECapabilities(unsigned int ActualTransformer);

    CodecStatus_t   VerifyMMECapabilities() { return CodecNoError; }

    CodecStatus_t   InitializeMMETransformer() { return CodecNoError; }

    CodecStatus_t   TerminateMMETransformer() { return CodecNoError; }      // Required by StreamBase audio decoder classes to flush buffer queues
    void   CallbackFromMME(MME_Event_t               Event, MME_Command_t            *Command);
    CodecStatus_t   Connect(Port_c *Port);
    CodecStatus_t   OutputPartialDecodeBuffers();
    CodecStatus_t   Halt();
    CodecStatus_t   ReleaseDecodeBuffer(Buffer_t                  Buffer);
    CodecStatus_t   Input(Buffer_t                  CodedBuffer);
    bool            IsDecimationValueSupported(int DecimationPolicy);
    CodecStatus_t   SetModuleParameters(unsigned int      ParameterBlockSize,
                                        void             *ParameterBlock);

    enum DecRet     vp9_decode(void *inst, struct Vp9DecInput *vp9_input, struct Vp9DecOutput *output);
    enum DecRet     ManageFrameToDisplay();
    void            GetDecimationValues(struct DecDownscaleCfg *dscale_cfg);
    CodecStatus_t   ConnectVp9Decoder();
    enum DecRet     HandleDecode(struct Vp9DecInput *DecInput);
public:
    void            AllocateComponentBuffer(void **ptr, unsigned int index, unsigned int width, unsigned int height);
    void            DeAllocateComponentBuffer(void **ptr);
    void            AllocateActualBufferPointer(void *decodebufferptr, unsigned int *phy, unsigned int **virt, unsigned int *size, unsigned int type);
private:
    typedef struct Vp9DecodeBufferInfo_s
    {
        Buffer_t CodedBuffer;
        Buffer_t DecodeBuffer;
        bool     IsDecodeBufferPushedForManifestation;
    } Vp9DecodeBufferInfo_t;

    Vp9DecInst                         mVp9DecInst;
    Port_c                            *mOutputPort;
    struct Vp9DecInfo                  mDecInfo;
    struct Vp9DecInput                 mDecInput;
    struct Vp9DecOutput                mDecOutput;
    unsigned int                       mPictureNumber;
    struct Vp9DecPicture               mDecPicture[VP9DEC_MAX_PIC_BUFFERS];
    Vp9DecodeBufferInfo_t              mVp9DecodeBufferInfo[VP9DEC_MAX_PIC_BUFFERS];
    bool                               mEndOfStreamDone;
    struct DecDownscaleCfg             dscale_cfg;
    unsigned int                       mDisplayFrameIndex;
    bool                               isFirstTimeConfigure;
    StartCodeList_t                   *StartCodeList;
    unsigned int                       mFrameCount;
    unsigned int                       mFrameSizes[MAX_VP9_ELEMENTARY_FRAMES];
    unsigned char                     *mHeaderBuffer;
    OS_Mutex_t                         mLock;

    DISALLOW_COPY_AND_ASSIGN(Codec_MmeVideoVp9_c);
};

#endif
