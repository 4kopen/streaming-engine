/************************************************************************
Copyright (C) 2003-2014 STMicroelectronics. All Rights Reserved.

This file is part of the Streaming Engine.

Streaming Engine is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License version 2 as
published by the Free Software Foundation.

Streaming Engine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with Streaming Engine; see the file COPYING.  If not, write to the Free
Software Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307,
USA.

The Streaming Engine Library may alternatively be licensed under a
proprietary license from ST.
************************************************************************/

/* SE UPDATE new file added inorder to call external apis from inside hantro stack inorder to
    map buffers required in vp9 decoder to streaming engine decode buffer manager class */

#ifndef CODEC_MME_VIDEO_VP9_EXT_H__
#define CODEC_MME_VIDEO_VP9_EXT_H__

#ifdef __cplusplus
extern "C" {
#endif

#define VP9DEC_MAX_PIC_BUFFERS 16

extern int InitializeDecodeBufferPools(void *, void **, unsigned int index, unsigned int width, unsigned int height);
extern void DeInitializeDecodeBufferPools(void *, void **);
extern void AllocateActualBuffer(void *, void *, unsigned int *, unsigned int **, unsigned int *, unsigned int);

#ifdef __cplusplus
}
#endif

#endif /* CODEC_MME_VIDEO_VP9_EXT_H__ */
