/************************************************************************
Copyright (C) 2003-2014 STMicroelectronics. All Rights Reserved.

This file is part of the Streaming Engine.

Streaming Engine is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License version 2 as
published by the Free Software Foundation.

Streaming Engine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with Streaming Engine; see the file COPYING.  If not, write to the Free
Software Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307,
USA.

The Streaming Engine Library may alternatively be licensed under a
proprietary license from ST.
************************************************************************/

#include "marker_queue.h"

#undef TRACE_TAG
#define TRACE_TAG "MarkerQueue_c"

MarkerQueue_c::MarkerQueue_c()
    : mMarkerQueueLock()
    , mRing(MAX_MARKERS_IN_FLIGHT)
    , mInPossibleStall(false)
    , mTimeOfPossibleStall(0)
    , mNbCmdsIssuedAtPossibleStall(0)
    , mNbCmdsCompletedAtPossibleStall(0)
{
    OS_InitializeMutex(&mMarkerQueueLock);
}

PlayerStatus_t MarkerQueue_c::FinalizeInit()
{
    return mRing.FinalizeInit() == RingNoError ? PlayerNoError : PlayerError;
}

// Destroy this queue.  Callers must ensure the queue is empty.
MarkerQueue_c::~MarkerQueue_c()
{
    SE_ASSERT(mRing.NonEmpty() == false);
    OS_TerminateMutex(&mMarkerQueueLock);
}

// Append at end of queue new marker that must stay in queue until given number
// of MME commands have been completed.
void MarkerQueue_c::Queue(Buffer_t Marker, unsigned int NbCmdsIssued)
{
    OS_LockMutex(&mMarkerQueueLock);

    SE_ASSERT(mRing.NonEmpty() == false || NextNbCmdsToWaitFor() <= NbCmdsIssued);

    RingStatus_t Status = mRing.Insert(Slot_t(Marker, NbCmdsIssued));
    if (Status != RingNoError)
    {
        SE_ERROR("Overflow, discarding marker\n");
        Marker->DecrementReferenceCount();
    }

    OS_UnLockMutex(&mMarkerQueueLock);
}

// Return front marker in queue or NULL if empty.
Buffer_t MarkerQueue_c::Dequeue()
{
    OS_LockMutex(&mMarkerQueueLock);
    Buffer_t Marker = DequeueLocked();
    OS_UnLockMutex(&mMarkerQueueLock);
    return Marker;
}

Buffer_t MarkerQueue_c::DequeueLocked()
{
    OS_AssertMutexHeld(&mMarkerQueueLock);

    Slot_t Slot;
    RingStatus_t Status = mRing.Extract(&Slot);
    return Status == RingNoError ? Slot.MarkerBuffer : NULL;
}

// Return front marker in queue if enough commands have been completed by
// companion processor.  Return NULL otherwise.
//
// Callers must call this function in a loop as several markers may be ready.
Buffer_t MarkerQueue_c::DequeueIfReady(unsigned int NbCmdsCompleted)
{
    OS_LockMutex(&mMarkerQueueLock);

    Buffer_t Marker = NULL;
    if (mRing.NonEmpty())
    {
        if (NbCmdsCompleted >= NextNbCmdsToWaitFor())
        {
            Marker = DequeueLocked();
        }
    }

    OS_UnLockMutex(&mMarkerQueueLock);

    return Marker;
}

// Check using given command counts if companion processor has progressed since
// last call and return oldest marker if no progress has been made for more than
// MAXIMUM_STALL_PERIOD.
//
// This is a hack because this breaks FIFO order (returned marker passes over
// buffers passed to companion).  It exists merely because
// Codec_MmeAudioStream_c implemented this stall check when this class was
// created.
//
// Callers should call in a loop as several markers can be stalled.
Buffer_t MarkerQueue_c::NoteProgressOrDequeueIfStalled(unsigned int NbCmdsIssued,
                                                       unsigned int NbCmdsCompleted)
{
    OS_LockMutex(&mMarkerQueueLock);

    Buffer_t StalledMarker = NULL;

    if (mRing.NonEmpty())
    {
        unsigned long long Now = OS_GetTimeInMicroSeconds();

        if (!mInPossibleStall ||
            (NbCmdsIssued != mNbCmdsIssuedAtPossibleStall) ||
            (NbCmdsCompleted != mNbCmdsCompletedAtPossibleStall))
        {
            mTimeOfPossibleStall = Now;
            mNbCmdsIssuedAtPossibleStall = NbCmdsIssued;
            mNbCmdsCompletedAtPossibleStall = NbCmdsCompleted;
            mInPossibleStall = true;
        }
        else if ((Now - mTimeOfPossibleStall) >= MAXIMUM_STALL_PERIOD)
        {
            mInPossibleStall = mRing.NonEmpty() ? true : false;
            StalledMarker = DequeueLocked();
        }
    }
    else
    {
        mInPossibleStall = false;
    }

    OS_UnLockMutex(&mMarkerQueueLock);

    return StalledMarker;
}

unsigned int MarkerQueue_c::NextNbCmdsToWaitFor() const
{
    SE_ASSERT(mRing.NonEmpty());
    Slot_t Slot;
    mRing.Peek(&Slot);
    return Slot.NbCmdsToWaitFor;
}
