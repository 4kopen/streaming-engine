/************************************************************************
Copyright (C) 2003-2014 STMicroelectronics. All Rights Reserved.

This file is part of the Streaming Engine.

Streaming Engine is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License version 2 as
published by the Free Software Foundation.

Streaming Engine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with Streaming Engine; see the file COPYING.  If not, write to the Free
Software Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307,
USA.

The Streaming Engine Library may alternatively be licensed under a
proprietary license from ST.
************************************************************************/

#ifndef H_MARKER_QUEUE
#define H_MARKER_QUEUE

#include "osinline.h"
#include "player_types.h"
#include "buffer.h"
#include "raw_ring.h"

// Staging queue for marker buffers not passed to companion processors.
//
// Edges must ensure FIFO order for markers wrt to payload buffers.  Failing
// this a drain marker could, for example, pass over some data and cause a drain
// call to complete prematuraly.
//
// FIFO order comes for free for markers passed to companion processors.
// However, when an edge receives an ARM-only marker, it must store it until all
// buffers passed to companion processor have been returned by latter and
// forwarded to next edge.
//
// This class is basically a FIFO queue storing markers alongside the number of
// MME commands that must complete before the marker can be forwarded to next
// edge.
//
// Thread-safe.
class MarkerQueue_c
{
public:
    MarkerQueue_c();
    PlayerStatus_t FinalizeInit();
    ~MarkerQueue_c();

    bool IsFullyInitialized() const     { return mRing.IsFullyInitialized(); }

    void Queue(Buffer_t Marker, unsigned int NbCmdsToWaitFor);
    Buffer_t DequeueIfReady(unsigned int NbCmdsCompleted);
    Buffer_t NoteProgressOrDequeueIfStalled(unsigned int NbCmdsIssued,
                                            unsigned int NbCmdsCompleted);
    Buffer_t Dequeue();

private:
    // Big enough to never overflow the ring.
    static const int MAX_MARKERS_IN_FLIGHT = 16;

    // See NoteProgressOrDequeueIfStalled().
    static const unsigned int MAXIMUM_STALL_PERIOD = 250000;          /* 1/4 second */

    struct Slot_t
    {
        Buffer_t        MarkerBuffer;

        // Number of MME commands that must complete before forwarding this
        // buffer.
        unsigned int    NbCmdsToWaitFor;

        Slot_t(Buffer_t Marker = NULL, unsigned int NbCmdsToWaitFor = 0)
            : MarkerBuffer(Marker)
            , NbCmdsToWaitFor(NbCmdsToWaitFor)
        {
        }
    };

    // Protect all members.
    OS_Mutex_t mMarkerQueueLock;

    RawRing_c<Slot_t> mRing;

    // Stall detection.
    bool                        mInPossibleStall;
    unsigned long long          mTimeOfPossibleStall;
    unsigned int                mNbCmdsIssuedAtPossibleStall;
    unsigned int                mNbCmdsCompletedAtPossibleStall;

    Buffer_t DequeueLocked();
    unsigned int NextNbCmdsToWaitFor() const;

    DISALLOW_COPY_AND_ASSIGN(MarkerQueue_c);
};

#endif
