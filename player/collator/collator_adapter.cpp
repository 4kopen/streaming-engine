/************************************************************************
Copyright (C) 2003-2014 STMicroelectronics. All Rights Reserved.

This file is part of the Streaming Engine.

Streaming Engine is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License version 2 as
published by the Free Software Foundation.

Streaming Engine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with Streaming Engine; see the file COPYING.  If not, write to the Free
Software Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307,
USA.

The Streaming Engine Library may alternatively be licensed under a
proprietary license from ST.
************************************************************************/

#include "uncompressed.h"
#include "collator_adapter.h"
#include "player_stream.h"
#include "player_playback.h"
#include "st_relayfs_se.h"

#undef  TRACE_TAG
#define TRACE_TAG "Collator_Adapter_c"

#undef TRACE_TAG
#define TRACE_TAG "Collator_Adapter_c"

// /////////////////////////////////////////////////////////////////////////
//
// Buffering window in microseconds

#define PIPELINE_LATENCY                    100000
#define REORDERING_GUARD                    100000
#define LIMITED_EARLY_INJECTION_WINDOW      1500000
// We want to keep a limit
// to avoid sleeping forever
// in case of bad PTS
#define MAXIMUM_LIMITED_EARLY_INJECTION_DELAY   20000000

#define INTER_INPUT_STALL_WARNING_TIME      500000      // Say half a second
#define BUFFER_MAX_EXPECTED_WAIT_PERIOD     30000000ULL // Say half a minute
#define MAX_BUFFER_FOR_REDUCE_COLLATOR_DATA 20          // Maximun number of buffer used (collator + reference frames)

#define MAX_FRAME_HEADER_PARTITION_SIZE           (1024*1024)    // for frame headers for HD cases (and below)
#define MAX_FRAME_HEADER_PARTITION_SIZE_FOR_UHD   (3*1024*1024)  // for frame headers for UHD cases

CollatorOffload_t Collator_c::gCollatorOffloadTuneable = CollatorNoOffload;

PlayerStatus_t Collator_Adapter_c::Halt()
{
    SE_DEBUG(mTraceGroup, ">\n");

    OS_LockMutex(&mCollatorLock);

    SetComponentState(ComponentHalted);

    mOutputPort = NULL;
    mCodedFrameBufferPool.Reset();

    if (mCodedFrameBuffer != NULL)
    {
        mCodedFrameBuffer->DecrementReferenceCount(IdentifierCollator);
        mCodedFrameBuffer = NULL;
    }

    if (mStartCodeHeadersBuffer != NULL)
    {
        mStartCodeHeadersBuffer->DecrementReferenceCount(IdentifierCollator);
        mStartCodeHeadersBuffer = NULL;
    }

    // Halt the collator processor before detaching metadata from mCodedFrameBufferPool
    // This is to avoid keeping any reference to those metadata before destroying them (e.g. StartCodeList pointer in Collator_Base_c)
    PlayerStatus_t Status = mCollatorProcessor.Halt();

    if (mCodedFrameBufferPool.Get() != NULL)
    {
        mCodedFrameBufferPool->DetachMetaData(mPlayer->MetaDataCodedFrameParametersType);
        mCodedFrameBufferPool.Reset();
    }

    delete [] mStartCodeList;
    mStartCodeList = NULL;

    DestroyHeaderBufferPool();

    OS_UnLockMutex(&mCollatorLock);

    SE_DEBUG(mTraceGroup, "<\n");
    return Status;
}

CollatorStatus_t Collator_Adapter_c::CreateHeaderBufferPool()
{
    void *HeaderBufferMemoryPool[3];
    int   frameHeaderPartitionSize = MAX_FRAME_HEADER_PARTITION_SIZE_FOR_UHD;

    SE_DEBUG(mTraceGroup, "\n");

#ifndef UNITTESTS
    int MemProfilePolicy = mStream->GetPlayer()->PolicyValue(mStream->GetPlayback(),
                                                             mStream, PolicyVideoPlayStreamMemoryProfile);
    if (!mStream->isMemProfileAboveHd(MemProfilePolicy))
    {
        frameHeaderPartitionSize = MAX_FRAME_HEADER_PARTITION_SIZE;
    }

    allocator_status_t AllocatorStatus = PartitionAllocatorOpen(&mHeaderBufferAllocator,
                                                                "vid-codec-data",
                                                                frameHeaderPartitionSize,
                                                                MEMORY_VIDEO_VMA_CACHED_ACCESS);
    if (AllocatorStatus != allocator_ok)
    {
        SE_ERROR("Stream %p Failed to allocate header buffer partition (status:%d)\n",
                 mStream, AllocatorStatus);
        return PlayerInsufficientMemory;
    }

    HeaderBufferMemoryPool[CachedAddress] = AllocatorUserAddress(mHeaderBufferAllocator);
    HeaderBufferMemoryPool[PhysicalAddress] = AllocatorPhysicalAddress(mHeaderBufferAllocator);
    SE_INFO(mTraceGroup, "Stream %p Alloc partition Header frame of %d bytes (mMaxStartCodeHeadersBufferSize:%d)\n",
            mStream, frameHeaderPartitionSize, mMaxStartCodeHeadersBufferSize);
#endif
    // Create pool of StartCodeHeaders buffer that will be attached to CodedFrameBuffers
    BufferStatus_t BufStatus  = mStream->GetPlayer()->GetBufferManager()->CreatePool(
                                    &mStartCodeHeadersBufferPool,
                                    mStream->GetPlayer()->GetStartCodeHeadersBufferType(),
                                    UNRESTRICTED_NUMBER_OF_BUFFERS,
                                    frameHeaderPartitionSize, HeaderBufferMemoryPool,
                                    NULL, NULL, true,
                                    MEMORY_VMA_CACHED_ACCESS);
    if (BufStatus != BufferNoError)
    {
        SE_ERROR("Stream 0x%p Failed to create the pool Status %d\n", mStream, BufStatus);
        return PlayerInsufficientMemory;
    }

    return PlayerNoError;
}

void Collator_Adapter_c::DestroyHeaderBufferPool()
{
    SE_DEBUG(mTraceGroup, "\n");

    if (mStartCodeHeadersBufferPool != NULL)
    {
        mStream->GetPlayer()->GetBufferManager()->DestroyPool(mStartCodeHeadersBufferPool);
        mStartCodeHeadersBufferPool  = NULL;
    }
#ifndef UNITTESTS
    AllocatorClose(&mHeaderBufferAllocator);
#endif
}

CollatorStatus_t   Collator_Adapter_c::Connect(Port_c *Port)
{
    BufferStatus_t  Status;
    bool isMemProfileAboveHd = false;
#ifndef UNITTESTS
    int MemProfilePolicy = mStream->GetPlayer()->PolicyValue(mStream->GetPlayback(),
                                                             mStream, PolicyVideoPlayStreamMemoryProfile);
    isMemProfileAboveHd = mStream->isMemProfileAboveHd(MemProfilePolicy);
#endif
    SE_DEBUG(mTraceGroup, "\n");

    if (Port == NULL)
    {
        SE_ERROR("Incorrect input param\n");
        return CollatorError;
    }

    if (mOutputPort != NULL)
    {
        SE_WARNING("Port already connected\n");
    }

    OS_LockMutex(&mCollatorLock);

    mOutputPort = Port;

    // Obtain the class list, and the coded frame buffer pool
    mCodedFrameBufferPool = mStream->GetCodedFrameBufferPool(&mMaximumCodedFrameSize);

    // Attach the coded frame parameters to every element of the pool
    Status = mCodedFrameBufferPool->AttachMetaData(mPlayer->MetaDataCodedFrameParametersType);
    if (Status != BufferNoError)
    {
        SE_ERROR("Failed to attach coded frame descriptor to all coded frame buffers\n");
        goto error_attach_codedframeparams;
    }

    bool requireFrameHeaders;
    mCollatorProcessor.GetCapabilities(isMemProfileAboveHd, &mGeneratesStartCodeList, &mMaxStartCodes,
                                       &requireFrameHeaders, &mMaxStartCodeHeadersBufferSize);
    if (Collator_c::GetCollatorOffloadTuneable() == CollatorNoOffload)
    {
        SE_DEBUG(mTraceGroup, "Collator is not offloaded, set mFillsStartCodeHeaderBuffer to 0\n");
        mFillsStartCodeHeadersBuffer = false;
        mMaxStartCodeHeadersBufferSize = 0;
    }
    else
    {
        SE_DEBUG(mTraceGroup, "Collator is offloaded, setting mFillsStartCodeHeaderBuffer to %d\n", requireFrameHeaders);
        mFillsStartCodeHeadersBuffer = requireFrameHeaders;
    }

    if (mMaxStartCodes != 0)
    {
        mStartCodeList = (StartCodeList_t *) new char [SizeofStartCodeList(mMaxStartCodes)];
        if (mStartCodeList == NULL) { goto  error_allocate_startcodelist; }
    }


    // create optional Header buffer pool
    if (mFillsStartCodeHeadersBuffer)
    {
        CollatorStatus_t collatorStatus = CreateHeaderBufferPool();
        if (collatorStatus != CollatorNoError) { goto error_create_headerpool; }
    }

    Status = mCollatorProcessor.SetSink(this, mMaximumCodedFrameSize);
    if (Status != CollatorNoError) { goto error_setsink; }

    // Go live
    SetComponentState(ComponentRunning);

    OS_UnLockMutex(&mCollatorLock);

    return CollatorNoError;

error_setsink:
    if (mFillsStartCodeHeadersBuffer)
    {
        DestroyHeaderBufferPool();
    }

error_create_headerpool:
    delete [] mStartCodeList;
    mStartCodeList = NULL;

error_allocate_startcodelist:
    mCodedFrameBufferPool->DetachMetaData(mPlayer->MetaDataCodedFrameParametersType);

error_attach_codedframeparams:
    mCodedFrameBufferPool.Reset();
    OS_UnLockMutex(&mCollatorLock);

    return CollatorError;
}

CollatorStatus_t   Collator_Adapter_c::Input(PlayerInputDescriptor_t *Input,
                                             const DataBlock_t *BlockList,
                                             int BlockCount,
                                             int *NbBlocksConsumed)
{
    SE_VERBOSE(mTraceGroup, ">Stream 0x%p count %d\n", mStream, BlockCount);

    OS_LockMutex(&mCollatorLock);

    if (!mInputConnected)
    {
        OS_UnLockMutex(&mCollatorLock);
        SE_ERROR("Input not connected\n");
        return CollatorInputNotConnected;
    }

    CollatorStatus_t Status = CollatorNoError;
    int i;

    for (i = 0; i < BlockCount; ++i)
    {
        Status = InputOneBlock(Input, BlockList[i].Len, BlockList[i].DataAddr);

        // once the firstBlock is inserted to collator Base there is no need to
        // to re-send the same InputDescriptor, otherwise for ES injection it
        // will cause a PES packet insertion for each block
        if ((i == 0) && (Input->StreamType == STM_SE_STREAMTYPE_ES))
        {
            Input->PlaybackTimeValid = false;
            Input->DecodeTimeValid   = false;
        }
        if (Status != CollatorNoError)
        {
            break;
        }
    }

    OS_UnLockMutex(&mCollatorLock);

    SE_VERBOSE(mTraceGroup, "<Stream 0x%p Consumed %d blocks Status %d\n", mStream, i, Status);
    *NbBlocksConsumed = i;
    return Status;
}

CollatorStatus_t   Collator_Adapter_c::InputOneBlock(const PlayerInputDescriptor_t *Input,
                                                     unsigned int                   DataLength,
                                                     const void                    *Data)
{
    SE_VERBOSE(mTraceGroup, ">Stream 0x%p data 0x%p len %d\n", mStream, Data, DataLength);

    if (!TestComponentState(ComponentRunning))
    {
        SE_ERROR("Stream 0x%p Component not in running state: current:%d\n", mStream, mComponentState);
        return CollatorError;
    }

    // Early return on empty input since we expect a valid buffer
    if ((Data == NULL) || (DataLength == 0))
    {
        SE_WARNING("Stream 0x%p Invalid buffer Data 0x%p DataLength %u\n", mStream, Data, DataLength);
        return CollatorNoError;
    }

    // Perform Input stall checking for live playback
    bool LivePlayback  = mPlayer->PolicyValue(mPlayback, mStream, PolicyLivePlayback) == PolicyValueApply;
    unsigned long long InputEntryTime  = OS_GetTimeInMicroSeconds();

    if (LivePlayback &&
        (mInputExitTime != INVALID_TIME) &&
        ((InputEntryTime - mInputExitTime) > INTER_INPUT_STALL_WARNING_TIME))
    {
        SE_WARNING("Stream 0x%p Stall warning, time between input exit and next entry = %lldms\n",
                   mStream, (InputEntryTime - mInputExitTime) / 1000);
    }

    {
        // allow st_relay capture of the input buffers
        st_relayfs_write_se(
            mStream->GetStreamType() == StreamTypeAudio ?
            ST_RELAY_TYPE_PES_AUDIO_BUFFER : ST_RELAY_TYPE_PES_VIDEO_BUFFER,
            ST_RELAY_SOURCE_SE, (unsigned char *)Data, DataLength, true);
        st_relayfs_write_se(
            mStream->GetStreamType() == StreamTypeAudio ?
            ST_RELAY_TYPE_PES_AUDIO_WITH_API : ST_RELAY_TYPE_PES_VIDEO_WITH_API,
            ST_RELAY_SOURCE_SE, (unsigned char *)Data, DataLength, true);
    }

    CollatorStatus_t Status = mCollatorProcessor.Input(Input, DataLength, Data);

    mInputExitTime   = OS_GetTimeInMicroSeconds();

    if (LivePlayback &&
        ((mInputExitTime - InputEntryTime) > INTER_INPUT_STALL_WARNING_TIME))
    {
        SE_WARNING("Stream 0x%p Stall warning, time between input entry and exit = %lldms\n",
                   mStream, (mInputExitTime - InputEntryTime) / 1000);
    }

    SE_VERBOSE(mTraceGroup, "<Stream 0x%p\n", mStream);
    return Status;
}

CollatorStatus_t   Collator_Adapter_c::InputJump(int discontinuity)
{
    if (!TestComponentState(ComponentRunning))
    {
        SE_ERROR("Component not in running state: current:%d\n", mComponentState);
        return CollatorError;
    }

    // To avoid throttling mechanism when a flush is on-going
    SetDrainingStatus(true);

    OS_LockMutex(&mCollatorLock);

    CollatorStatus_t Status = mCollatorProcessor.InputJump(discontinuity);

    OS_UnLockMutex(&mCollatorLock);

    SetDrainingStatus(false);

    return Status;
}

CollatorStatus_t   Collator_Adapter_c::GetNewBuffer(unsigned char     **bufferBase,
                                                    unsigned int       *bufferSize,
                                                    StartCodeList_t   **startCodeList,
                                                    unsigned int       *startCodeBufferSize,
                                                    unsigned char     **headerBufferBase,
                                                    unsigned int       *headerBufferSize)
{
    SE_VERBOSE(mTraceGroup, ">\n");

    BufferStatus_t Status = mCodedFrameBufferPool->GetBuffer(&mCodedFrameBuffer, IdentifierCollator, mMaximumCodedFrameSize);
    if (Status != BufferNoError)
    {
        mCodedFrameBuffer = NULL;
        *bufferBase = NULL;
        SE_ERROR("Failed to obtain a coded frame buffer Status=%d\n", Status);
        if (Status == BufferGetBufferRejected)
        {
            return CollatorInputNotConnected;
        }
        return CollatorError;
    }

    *bufferSize = mMaximumCodedFrameSize;

    mCodedFrameBuffer->ObtainDataReference(NULL, NULL, (void **) bufferBase);
    SE_ASSERT(*bufferBase != NULL); // cannot be empty

    if (mGeneratesStartCodeList)
    {
        SE_ASSERT(mStartCodeList != NULL);
        *startCodeList = mStartCodeList;
        (*startCodeList)->NumberOfStartCodes = 0;
        *startCodeBufferSize = SizeofStartCodeList(mMaxStartCodes);
    }
    else
    {
        *startCodeList       = NULL;
        *startCodeBufferSize = 0;
    }

    if (mFillsStartCodeHeadersBuffer)
    {
        Status = mStartCodeHeadersBufferPool->GetBuffer(&mStartCodeHeadersBuffer,
                                                        IdentifierCollator, mMaxStartCodeHeadersBufferSize);
        if (Status != BufferNoError)
        {
            mCodedFrameBuffer->DecrementReferenceCount();
            mStartCodeHeadersBuffer = NULL;
            SE_ERROR("Failed to obtain a header buffer\n");
            return CollatorError;
        }

        *headerBufferSize = mMaxStartCodeHeadersBufferSize;

        mStartCodeHeadersBuffer->ObtainDataReference(NULL, NULL, (void **) headerBufferBase);
        SE_ASSERT(*headerBufferBase != NULL);
    }
    else
    {
        *headerBufferBase = NULL;
        *headerBufferSize = 0;
    }

    SE_VERBOSE(mTraceGroup, "<\n");

    return CollatorNoError;
}

CollatorStatus_t Collator_Adapter_c::InsertInOutputPort(unsigned int                  codedFrameSize,
                                                        unsigned int                  HeaderSize,
                                                        const CodedFrameParameters_t &CodedFrameParameters)
{
    SE_VERBOSE(mTraceGroup, "> codedFrameSize=%d HeaderSize=%d\n", codedFrameSize, HeaderSize);

    DelayForReducedCollatedData();

    mCodedFrameBuffer->SetUsedDataSize(codedFrameSize);

    BufferStatus_t BufStatus = mCodedFrameBuffer->ShrinkBuffer(Max(codedFrameSize, 1U));
    if (BufStatus != BufferNoError)
    {
        SE_ERROR("Failed to shrink the coded frame buffer to size %d (%08x)\n", codedFrameSize, BufStatus);
    }

    //  once buffer is sent to next stage, list is copied from 'worst case sized'
    //  member mStartCodeList to metadata
    if (mGeneratesStartCodeList)
    {
        BufStatus = mCodedFrameBuffer->AttachMetaData(mPlayer->MetaDataStartCodeListType, SizeofStartCodeList(mStartCodeList->NumberOfStartCodes));
        if (BufStatus != BufferNoError)
        {
            SE_ERROR("Failed to attach metadata start code list (%08x)\n", BufStatus);
            return CollatorError;
        }

        void          *startcodelist;
        mCodedFrameBuffer->ObtainMetaDataReference(mPlayer->MetaDataStartCodeListType, &startcodelist);
        SE_ASSERT(startcodelist != NULL);
        memcpy(startcodelist , (void *) mStartCodeList, SizeofStartCodeList(mStartCodeList->NumberOfStartCodes));
    }

    if (mFillsStartCodeHeadersBuffer)
    {
        mStartCodeHeadersBuffer->SetUsedDataSize(HeaderSize);

        BufStatus = mStartCodeHeadersBuffer->ShrinkBuffer(Max(HeaderSize, 1U));

        if (BufStatus != BufferNoError)
        {
            SE_ERROR("Failed to shrink the header buffer to size %d (%08x)\n", HeaderSize, BufStatus);
        }

        BufStatus = mCodedFrameBuffer->AttachBuffer(mStartCodeHeadersBuffer);
        mStartCodeHeadersBuffer->DecrementReferenceCount();

        if (BufStatus != BufferNoError)
        {
            SE_ERROR("Failed to attach header buffer(%08x)\n", BufStatus);
            return CollatorError;
        }
    }

    CodedFrameParameters_t *codedFrameParameters;
    mCodedFrameBuffer->ObtainMetaDataReference(mPlayer->MetaDataCodedFrameParametersType, (void **) &codedFrameParameters);
    SE_ASSERT(codedFrameParameters != NULL);

    memcpy(codedFrameParameters, &CodedFrameParameters, sizeof(CodedFrameParameters));

    codedFrameParameters->CollationTime = OS_GetTimeInMicroSeconds();
    DelayForInjectionThrottling(codedFrameParameters);

    PlayerSequenceNumber_t *SequenceNumberStructure;
    mCodedFrameBuffer->ObtainMetaDataReference(mPlayer->MetaDataSequenceNumberType, (void **)(&SequenceNumberStructure));
    SE_ASSERT(SequenceNumberStructure != NULL);

    memset(SequenceNumberStructure, 0, sizeof(*SequenceNumberStructure));

    RingStatus_t RingStatus = mOutputPort->Insert((uintptr_t)mCodedFrameBuffer);
    if (RingStatus != RingNoError)
    {
        SE_ERROR("Stream 0x%p Failed to insert buffer in ouput port\n", mStream);
        return CollatorError;
    }

    mStream->Statistics().BufferCountFromCollator++;

    SE_VERBOSE(mTraceGroup, "<\n");

    return CollatorNoError;
}

// Get a Stream-Specific Policy
int Collator_Adapter_c::GetStreamPolicy(PlayerPolicy_t Policy)
{
    return mPlayer->PolicyValue(mPlayback, mStream, Policy);
}

// Get the Playback's Low-Power state
bool Collator_Adapter_c::PlaybackIsInLowPowerState()
{
    return mPlayback->IsLowPowerState();
}

void Collator_Adapter_c::AttachBuffer(const void *Data)
{
    //
    // This function is used in the uncompressed video path and used to
    // attach the decode buffer mentioned in the input to the current
    // coded frame buffer, to ensure release at the appropriate time.
    //
    const UncompressedBufferDesc_t *UncompressedDataDesc = (UncompressedBufferDesc_t *)Data;
    Buffer_t    CapturedBuffer    = (Buffer_t)(UncompressedDataDesc->BufferClass);
    mCodedFrameBuffer->AttachBuffer(CapturedBuffer);

    // Now that CapturedBuffer(Decode Buffer) is attached to the coded buffer
    // decrement its ref count, so that DecodeBuffer's life is tied to the life
    // of coded frame buffer
    CapturedBuffer->DecrementReferenceCount();
}

void Collator_Adapter_c::UpdateStreamStatistics(CollatorStreamStatistics_t statistic, int value)
{
    switch (statistic)
    {
    case    CollatorAudioElementrySyncLostCount:
        mStream->Statistics().CollatorAudioElementrySyncLostCount += value;
        break;

    case CollatorAudioPesSyncLostCount:
        mStream->Statistics().CollatorAudioPesSyncLostCount += value;
        break;

    default:
        SE_ASSERT(0);
        break;
    }
}

void   Collator_Adapter_c::DelayForInjectionThrottling(CodedFrameParameters_t *CodedFrameParameters)
{
    if (mStream->GetStreamType() != StreamTypeAudio)
    {
        return;
    }

    //
    // Is throttling enabled, and do we have a pts to base the limit on
    //

    if (
        (mPlayer->PolicyValue(mPlayback, mStream, PolicyLimitInputInjectAhead) == PolicyValueDisapply) ||
        (mPlayer->PolicyValue(mPlayback, mStream, PolicyLivePlayback) == PolicyValueApply) ||
        (mPlayer->PolicyValue(mPlayback, mStream, PolicyCaptureProfile) != PolicyValueCaptureProfileDisabled) ||
        (mPlayer->PolicyValue(mPlayback, mStream, PolicyAVDSynchronization) == PolicyValueDisapply)
    )
    {
        SE_VERBOSE(group_avsync, "Stream 0x%p Throttling not applied. policy mismatch\n", mStream);
        return;
    }

    if (mIsDraining)
    {
        SE_VERBOSE(group_avsync, "Stream 0x%p Throttling not applied. Drain on-going\n", mStream);
        return;
    }

    if (NotValidTime(CodedFrameParameters->PlaybackTime) || (false == CodedFrameParameters->PlaybackTimeValid))
    {
        SE_VERBOSE(group_avsync, "Stream 0x%p Throttling not applied. Invalid playback time : %lld %d (%p)\n"
                   , mStream
                   , CodedFrameParameters->PlaybackTime
                   , CodedFrameParameters->PlaybackTimeValid
                   , CodedFrameParameters);
        return;
    }

    Rational_t          Speed;
    PlayDirection_t     Direction;
    mPlayback->GetSpeed(&Speed, &Direction);

    if (Direction == PlayBackward)
    {
        SE_VERBOSE(group_avsync, "Stream 0x%p Throttling not applied. Playing backward\n", mStream);
        return;
    }

    unsigned long long  SystemPlaybackTime;
    PlayerStatus_t Status    = mOutputCoordinator->TranslatePlaybackTimeToSystem(PlaybackContext,
                                                                                 TimeStamp_c(CodedFrameParameters->PlaybackTime, CodedFrameParameters->SourceTimeFormat),
                                                                                 &SystemPlaybackTime);

    if (Status != PlayerNoError)
    {
        SE_VERBOSE(group_avsync
                   , "Stream 0x%p Throttling not applied. Could not retrieve playback time (%d), got %lld from %lld\n"
                   , mStream
                   , Status
                   , SystemPlaybackTime
                   , CodedFrameParameters->PlaybackTime);
        return;
    }

    if (Speed == 0)
    {
        SE_VERBOSE(group_avsync, "Stream 0x%p Throttling not applied. Speed is 0\n", mStream);
        return;
    }

    //
    // Calculate and perform a delay if necessary
    //
    unsigned long long  Now = OS_GetTimeInMicroSeconds();

    // Playback speed should compensate the window size
    int window = (LIMITED_EARLY_INJECTION_WINDOW * Speed).LongLongIntegerPart();
    long long EarliestInjectionTime = SystemPlaybackTime - PIPELINE_LATENCY - (REORDERING_GUARD / Speed).LongLongIntegerPart() - window;
    long long Delay = EarliestInjectionTime - (long long)Now;

    if (Delay <= 1000)
    {
        SE_VERBOSE(group_avsync, "Stream 0x%p Throttling not applied. Delay too short (%lld)\n", mStream, Delay);
        return;
    }

    if (Delay > MAXIMUM_LIMITED_EARLY_INJECTION_DELAY)
    {
        SE_VERBOSE(group_avsync, "Stream 0x%p Computed delay is above threshold (%lld > %d) for playback time %lld\n"
                   , mStream
                   , Delay
                   , MAXIMUM_LIMITED_EARLY_INJECTION_DELAY
                   , SystemPlaybackTime);

        Delay = 0;
    }
    SE_VERBOSE(group_avsync, "Stream 0x%p - Sleeping for %lld (window=%d earliest=%lld playbacktime=%lld now=%lld)\n"
               , mStream
               , Delay
               , window
               , EarliestInjectionTime
               , SystemPlaybackTime
               , Now);

    OS_Status_t WaitStatus = OS_WaitForEventAuto(&mThrottlingWakeUpEvent, Delay / 1000);
    if (WaitStatus != OS_TIMED_OUT)
    {
        SE_VERBOSE(group_avsync, "Stream 0x%p Throttling wait was woken up\n" , mStream);
    }

    OS_ResetEvent(&mThrottlingWakeUpEvent);
}

void Collator_Adapter_c::SetDrainingStatus(bool isDraining)
{
    mIsDraining = isDraining;
    if (isDraining)
    {
        SE_VERBOSE(group_avsync, "Stream 0x%p Drain requested, waking up collation thread\n", mStream);
        OS_SetEvent(&mThrottlingWakeUpEvent);
    }
    else
    {
        OS_ResetEvent(&mThrottlingWakeUpEvent);
    }
}

//      Insert marker frame to the output port
CollatorStatus_t   Collator_Adapter_c::InsertMarkerFrameToOutputPort(const MarkerFrame_t &MarkerFrame)
{
    BufferStatus_t            Status;
    CodedFrameParameters_t   *CodedFrameParameters;
    PlayerSequenceNumber_t   *SequenceNumberStructure;
    Buffer_t                  MarkerFrameBuffer;

    Status = mStream->GetCodedFrameBufferPool()->GetBuffer(&MarkerFrameBuffer, IdentifierDrain, 0);
    if (Status != BufferNoError)
    {
        SE_ERROR("Stream 0x%p Unable to obtain a marker frame\n", mStream);
        return CollatorError;
    }

    MarkerFrameBuffer->ObtainMetaDataReference(mPlayer->MetaDataCodedFrameParametersType, (void **)(&CodedFrameParameters));
    SE_ASSERT(CodedFrameParameters != NULL);

    memset(CodedFrameParameters, 0, sizeof(*CodedFrameParameters));

    MarkerFrameBuffer->ObtainMetaDataReference(mPlayer->MetaDataSequenceNumberType, (void **)(&SequenceNumberStructure));
    SE_ASSERT(SequenceNumberStructure != NULL);

    SequenceNumberStructure->mIsMarkerFrame = true;
    SequenceNumberStructure->mMarkerFrame   = MarkerFrame;

    // Allocate minimum startCodelist because Parser always check for presence of this metadata,
    // and want to keep this modification local to collator to not impact parsers.
    if (mGeneratesStartCodeList)
    {
        Status = MarkerFrameBuffer->AttachMetaData(mPlayer->MetaDataStartCodeListType, 1);
        if (Status != BufferNoError)
        {
            SE_ERROR("Stream 0x%p Unable to attach start code list meta data to markerbuffer\n", mStream);
            return CollatorError;
        }
    }

    switch (MarkerFrame.mMarkerType)
    {
    case SplicingMarker:
    {
        SE_INFO(mTraceGroup, "Stream 0x%p inserting SplicingMarker Flags %u Pts Offset %lld\n",
                mStream, SequenceNumberStructure->mMarkerFrame.mSplicingMarkerData.mSplicingFlags, SequenceNumberStructure->mMarkerFrame.mSplicingMarkerData.mPtsOffset);
        break;
    }
    case ReqTimeMarker:
    {
        SE_DEBUG(mTraceGroup, "Stream 0x%p inserting ReqTimeMarker Marker Id0 %u Marker Id1 %u\n",
                 mStream, SequenceNumberStructure->mMarkerFrame.mReqTimeMarkerData.mMarkerId0, SequenceNumberStructure->mMarkerFrame.mReqTimeMarkerData.mMarkerId1);
        break;
    }
    case DiscontinuityMarker:
    {
        SE_DEBUG(mTraceGroup, "Stream 0x%p inserting DiscontinuityMarker\n", mStream);
        break;
    }
    case EosMarker:
    {
        SE_DEBUG(mTraceGroup, "Stream 0x%p inserting EosMarker\n", mStream);
        break;
    }
    default:
    {
        SE_ERROR("Stream 0x%p Marker Type %d not supported\n", mStream, MarkerFrame.mMarkerType);
        return CollatorError;
    }
    }

    //
    // Insert the marker into the flow
    //
    RingStatus_t RingStatus = mOutputPort->Insert((uintptr_t)MarkerFrameBuffer);
    if (RingStatus != RingNoError)
    {
        SE_ERROR("Stream 0x%p Failed to insert MarkerFrame type %d in ouput port\n", mStream, MarkerFrame.mMarkerType);
        return CollatorError;
    }

    return CollatorNoError;
}


// Allow to not consume more than the reduced collated data level
// in case the policy 'ReduceCollatedData' is set AND the AVSync is off
// and not drain on-going
void Collator_Adapter_c::DelayForReducedCollatedData()
{
    //
    // Is Reduce callated Data enabled
    //
    if (mPlayer->PolicyValue(mPlayback, mStream, PolicyReduceCollatedData) != PolicyValueApply)
    {
        return;
    }
    if (mPlayer->PolicyValue(mPlayback, mStream, PolicyAVDSynchronization) != PolicyValueDisapply)
    {
        SE_ERROR("Stream 0x%p Reduced collated data not applied. AVSync not disabled\n", mStream);
        return;
    }

    if (mIsDraining)
    {
        SE_VERBOSE(mTraceGroup, "Stream 0x%p Reduced collated data not applied. Drain on-going\n", mStream);
        return;
    }

    unsigned int      TotalBuffers;
    unsigned int      BuffersUsed;
    //
    // We use MAX_BUFFER_FOR_COLLATOR_DATA which is the min number of buffers required for a correct decoding
    // It is equal to (max collator buffer used(*1) + the max number of reference frames) ; (*1) = 2
    // If the number of bufferUsed is equal to MAX_BUFFER_FOR_COLLATOR_DATA then we wait a sufficient decode frame period to
    // allow release of buffer after decode and by the way get a bufferUsed < 'N'

    // Need to know how many buffers are used
    mCodedFrameBufferPool->GetPoolUsage(&TotalBuffers, &BuffersUsed);

    SE_EXTRAVERB(mTraceGroup, "Stream 0x%p MaxBufferForReduceCollatedData = %d, BuffersUsed =%d\n", mStream, MAX_BUFFER_FOR_REDUCE_COLLATOR_DATA, BuffersUsed);

    if (BuffersUsed >= MAX_BUFFER_FOR_REDUCE_COLLATOR_DATA)
    {
        //
        // Wait until a buffer be released
        //
        unsigned long long EntryTime  = OS_GetTimeInMicroSeconds();

        while (true)
        {
            // Sleep a while before check again (100ms is fast enough)
            // Reuse of mThrottlingWakeUpEvent to break the wait in case of a drain
            // as the the "throttling" and "reduce collated data" are mutually exclusive
            // thanks to PolicyAVDSynchronization policy

            OS_Status_t WaitStatus = OS_WaitForEventAuto(&mThrottlingWakeUpEvent, 100);

            // 1. mThrottlingWakeUpEvent may have been set by ::SetDrainingStatus() or ::Wakeup()
            //    which may have caused the wakeup of this event
            // 2. Wait again if there was a OS_TIMED_OUT
            // So reset mThrottlingWakeUpEvent in any case
            OS_ResetEvent(&mThrottlingWakeUpEvent);

            if (WaitStatus != OS_TIMED_OUT)
            {
                // Wake up to signal that a drain is on-going. Reduce collated data aborted
                SE_DEBUG(mTraceGroup, "Stream 0x%p Reduce collated data waiting is aborted by playstream drain/Wakeup\n" , mStream);
                break;
            }

            mCodedFrameBufferPool->GetPoolUsage(&TotalBuffers, &BuffersUsed);
            if (BuffersUsed < MAX_BUFFER_FOR_REDUCE_COLLATOR_DATA)
            {
                break;
            }

            //
            // The wait can last until a drain or a decoded buffer has been freed specfically when
            // all decode buffers are queued to manifestor.
            // We can assume that decoded buffers will be released before playstream delete
            //
            if ((OS_GetTimeInMicroSeconds() - EntryTime) > BUFFER_MAX_EXPECTED_WAIT_PERIOD)
            {
                SE_DEBUG(mTraceGroup, "Stream 0x%p Waiting for buffer : Reduce collated buffer (%d) in use (%d)\n", mStream, MAX_BUFFER_FOR_REDUCE_COLLATOR_DATA, BuffersUsed);
                EntryTime = OS_GetTimeInMicroSeconds();
            }
        }
    }
}

bool Collator_Adapter_c::TestComponentState(EngineComponentState_t s)
{
    bool ret = (mComponentState == s);
    // This memory barrier must occur after Reading Component State.
    // The ordering is very specific to ensure that mComponentState
    // is checked before any operation depending on it occurs
    OS_Smp_Mb();
    return ret;
}

void Collator_Adapter_c::SetComponentState(EngineComponentState_t s)
{
    OS_Smp_Mb();
    // This memory barrier must occur before Setting Component State.
    // The ordering is very specific to ensure that mComponentState
    // Cannot be changed until all operations preceeding are complete
    mComponentState = s;
}

CollatorStatus_t Collator_Adapter_c::ConnectInput()
{
    if (mInputConnected)
    {
        SE_ERROR("Input already connected\n");
        return CollatorInputAlreadyConnected;
    }

    mInputConnected = true;

    return CollatorNoError;
}

CollatorStatus_t Collator_Adapter_c::DisconnectInput()
{
    if (!mInputConnected)
    {
        SE_ERROR("Input not connected\n");
        return CollatorInputNotConnected;
    }

    mInputConnected = false;

    mCodedFrameBufferPool->RejectGetBuffers();

    // acquire mCollatorLock to guarantee that any call to Input has returned
    // any new call to Input would be rejected due to input not connected
    OS_LockMutex(&mCollatorLock);

    mCodedFrameBufferPool->AcceptGetBuffers();

    OS_UnLockMutex(&mCollatorLock);

    return CollatorNoError;
}
