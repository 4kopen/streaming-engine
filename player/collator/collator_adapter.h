/************************************************************************
Copyright (C) 2003-2014 STMicroelectronics. All Rights Reserved.

This file is part of the Streaming Engine.

Streaming Engine is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License version 2 as
published by the Free Software Foundation.

Streaming Engine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with Streaming Engine; see the file COPYING.  If not, write to the Free
Software Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307,
USA.

The Streaming Engine Library may alternatively be licensed under a
proprietary license from ST.
************************************************************************/

#ifndef H_COLLATOR_ADAPTER
#define H_COLLATOR_ADAPTER

#include "collator.h"
#include "shared_ptr.h"

#undef  TRACE_TAG
#define TRACE_TAG "Collator_Adapter_c"

class Collator_Adapter_c : public Collator_c, public CollatorSinkInterface_c
{
public:
    Collator_Adapter_c(CollatorProcessorInterface_c &collatorProcessor, int traceGroup)
        : mCollatorProcessor(collatorProcessor)
        , mGeneratesStartCodeList(false)
        , mPlayer(NULL)
        , mPlayback(NULL)
        , mStream(NULL)
        , mOutputCoordinator(NULL)
        , mOutputPort(NULL)
        , mInputConnected(false)
        , mCodedFrameBufferPool()
        , mCodedFrameBuffer(NULL)
        , mMaximumCodedFrameSize(0)
        , mMaxStartCodes(0)
        , mThrottlingWakeUpEvent()
        , mIsDraining(false)
        , mTraceGroup(traceGroup)
        , mInputExitTime(INVALID_TIME)
        , mCollatorLock()
        , mComponentState(ComponentReset)
        , mFillsStartCodeHeadersBuffer(false)
        , mMaxStartCodeHeadersBufferSize(0)
        , mStartCodeHeadersBufferPool(NULL)
        , mHeaderBufferAllocator()
        , mStartCodeHeadersBuffer(NULL)
        , mStartCodeList(NULL)
    {
        OS_InitializeEvent(&mThrottlingWakeUpEvent);
        OS_InitializeMutex(&mCollatorLock);
    }

    virtual ~Collator_Adapter_c()
    {
        OS_TerminateEvent(&mThrottlingWakeUpEvent);
        OS_TerminateMutex(&mCollatorLock);
        delete &mCollatorProcessor;
    }

    //
    // Collator_c methods
    //

    virtual PlayerStatus_t Halt();

    virtual PlayerStatus_t RegisterPlayer(
        Player_t             Player,
        PlayerPlayback_t     Playback,
        PlayerStream_t       Stream,
        OutputCoordinator_t  OutputCoordinator)
    {
        mPlayer     = Player;
        mPlayback   = Playback;
        mStream     = Stream;
        mOutputCoordinator = OutputCoordinator;

        return PlayerNoError;
    }

    virtual PlayerStatus_t      SpecifySignalledEvents(
        PlayerEventMask_t        EventMask,
        void                    *EventUserData)
    {
        return mCollatorProcessor.SpecifySignalledEvents(EventMask, EventUserData);
    }

    virtual CollatorStatus_t   Connect(Port_c *Port);

    virtual CollatorStatus_t   InputJump(int discontinuity);

    virtual CollatorStatus_t   Input(PlayerInputDescriptor_t *Input,
                                     const DataBlock_t       *BlockList,
                                     int                      BlockCount,
                                     int                     *NbBlocksConsumed);

    virtual void WakeUp()
    {
        SE_VERBOSE(group_avsync, "Stream 0x%p waking up collation thread\n", mStream);
        OS_SetEvent(&mThrottlingWakeUpEvent);
    }

    virtual CollatorStatus_t ConnectInput();
    virtual CollatorStatus_t DisconnectInput();

    //
    // CollatorSinkInterface_c methods
    //

    virtual CollatorStatus_t GetNewBuffer(unsigned char     **bufferBase,
                                          unsigned int       *bufferSize,
                                          StartCodeList_t   **startCodeList,
                                          unsigned int       *startcodeBufferSize,
                                          unsigned char     **headerBufferBase,
                                          unsigned int       *headerBufferSize);

    virtual CollatorStatus_t InsertInOutputPort(unsigned int                  codedFrameSize,
                                                unsigned int                  headerSize,
                                                const CodedFrameParameters_t &CodedFrameParameters);

    virtual CollatorStatus_t InsertMarkerFrameToOutputPort(const MarkerFrame_t &MarkerFrame);

    // Get a Stream-Specific Policy
    virtual int GetStreamPolicy(PlayerPolicy_t Policy);

    // Get the Playback's Low-Power state
    virtual bool PlaybackIsInLowPowerState();

    virtual void AttachBuffer(const void *data);

    virtual void UpdateStreamStatistics(CollatorStreamStatistics_t statistic, int value);

    virtual bool TestComponentState(EngineComponentState_t s);
    virtual void SetComponentState(EngineComponentState_t s);

private:
    CollatorProcessorInterface_c   &mCollatorProcessor;
    bool                            mGeneratesStartCodeList;
    Player_t                        mPlayer;
    PlayerPlayback_t                mPlayback;
    PlayerStream_t                  mStream;
    OutputCoordinator_t             mOutputCoordinator;
    Port_c                         *mOutputPort;
    bool                            mInputConnected;
    SharedPtr_c<BufferPool_c>       mCodedFrameBufferPool;
    Buffer_t                        mCodedFrameBuffer;
    unsigned int                    mMaximumCodedFrameSize;
    unsigned int                    mMaxStartCodes;
    // mThrottlingWakeUpEvent is used for Throttling and for Reduce collated data
    OS_Event_t                      mThrottlingWakeUpEvent;
    bool                            mIsDraining;
    int                             mTraceGroup;
    unsigned long long              mInputExitTime;
    OS_Mutex_t                      mCollatorLock; // Used to serialize all calls on Collator_c interface
    EngineComponentState_t          mComponentState;
    bool                            mFillsStartCodeHeadersBuffer;
    unsigned int                    mMaxStartCodeHeadersBufferSize;
    BufferPool_t                    mStartCodeHeadersBufferPool;
    allocator_device_t              mHeaderBufferAllocator;
    Buffer_t                        mStartCodeHeadersBuffer;
    StartCodeList_t                *mStartCodeList;

    // private methods
    void    SetDrainingStatus(bool DrainingStatus);
    void    DelayForInjectionThrottling(CodedFrameParameters_t *CodedFrameParameters);
    void    DelayForReducedCollatedData();

    CollatorStatus_t    CreateHeaderBufferPool();
    void                DestroyHeaderBufferPool();
    CollatorStatus_t    InputOneBlock(const PlayerInputDescriptor_t  *Input,
                                      unsigned int                    DataLength,
                                      const void                     *Data);

    DISALLOW_COPY_AND_ASSIGN(Collator_Adapter_c);

};

#endif
