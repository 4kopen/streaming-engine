/************************************************************************
Copyright (C) 2003-2014 STMicroelectronics. All Rights Reserved.

This file is part of the Streaming Engine.

Streaming Engine is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License version 2 as
published by the Free Software Foundation.

Streaming Engine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with Streaming Engine; see the file COPYING.  If not, write to the Free
Software Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307,
USA.

The Streaming Engine Library may alternatively be licensed under a
proprietary license from ST.
************************************************************************/

#include "collator_base.h"

// /////////////////////////////////////////////////////////////////////////
//
//      The Constructor function
//

Collator_Base_c::Collator_Base_c()
    : EventMask(0)
    , EventUserData(NULL)
    , mGroupTrace(0)
    , Configuration()
    , AccumulatedDataSize(0)
    , BufferBase(NULL)
    , MaximumCodedFrameSize(0)
    , CodedFrameParameters()
    , StartCodeList(NULL)
    , mStartCodeBufferSize(0)
    , mStartCodeHeadersBufferBase(NULL)
    , mStartCodeHeadersBufferSize(0)
    , mAccumulatedStartCodeHeadersSize(0)
    , mFoundReqTimeControlData(false)
    , mReqTimeMarker()
    , mCollatorSink(NULL)
    , mControlDataDetectionState(SeekingControlData)
    , mControlDataRemainingData(NULL)
    , mControlDataRemainingLength(0)
    , mControlDataTrailingStartCodeBytes(0)
    , mGotPartialControlDataBytes(0)
    , mStoredControlData()
    , mInputDescriptor()
{
}

// /////////////////////////////////////////////////////////////////////////
//
//      The Destructor function
//

Collator_Base_c::~Collator_Base_c()
{
    Collator_Base_c::Halt();
}

// /////////////////////////////////////////////////////////////////////////
//
//      The Halt function, give up access to any registered resources
//

CollatorStatus_t   Collator_Base_c::Halt()
{
    BufferBase                  = NULL;
    StartCodeList               = NULL;

    return CollatorNoError;
}

PlayerStatus_t      Collator_Base_c::SpecifySignalledEvents(PlayerEventMask_t        EventMask,
                                                            void                    *EventUserData)
{
    this->EventMask         = EventMask;
    this->EventUserData     = EventUserData;
    return  PlayerNoError;
}

CollatorStatus_t Collator_Base_c::SetSink(CollatorSinkInterface_c *sink, unsigned int maximumCodedFrameSize)
{
    (void)maximumCodedFrameSize; // warning removal

    mCollatorSink = sink;

    // Acquire an operating buffer
    CollatorStatus_t Status = mCollatorSink->GetNewBuffer(
                                  &BufferBase, &MaximumCodedFrameSize,
                                  &StartCodeList, &mStartCodeBufferSize,
                                  &mStartCodeHeadersBufferBase, &mStartCodeHeadersBufferSize);
    if (Status != CollatorNoError) { return Status; }

    SE_ASSERT(!Configuration.GenerateStartCodeList || mStartCodeBufferSize == SizeofStartCodeList(Configuration.MaxStartCodes));

    ResetBufferState();

    return CollatorNoError;
}

// /////////////////////////////////////////////////////////////////////////
//
//      The Frame Flush implementation
//

CollatorStatus_t   Collator_Base_c::InternalFrameFlush()
{
    CollatorStatus_t  Status = CollatorNoError;
    unsigned char     TerminalStartCode[4];

    if ((AccumulatedDataSize != 0) ||
        (CodedFrameParameters.StreamDiscontinuity ||
         CodedFrameParameters.ContinuousReverseJump))
    {
        if ((AccumulatedDataSize != 0) && Configuration.InsertFrameTerminateCode)
        {
            TerminalStartCode[0]    = 0x00;
            TerminalStartCode[1]    = 0x00;
            TerminalStartCode[2]    = 0x01;
            TerminalStartCode[3]    = Configuration.TerminalCode;
            Status  = AccumulateData(4, TerminalStartCode);
            if (Status != CollatorNoError)
            {
                SE_ERROR("Failed to add terminal start code\n");
                DiscardAccumulatedData();
            }
        }

        if (Configuration.RequireFrameHeaders && mStartCodeHeadersBufferBase != NULL && mStartCodeHeadersBufferSize != 0)
        {
            FillFrameHeaders();
        }

        Status = mCollatorSink->InsertInOutputPort(AccumulatedDataSize, mAccumulatedStartCodeHeadersSize, CodedFrameParameters);
        if (Status != CollatorNoError) { return Status; }

        Status = mCollatorSink->GetNewBuffer(
                     &BufferBase, &MaximumCodedFrameSize,
                     &StartCodeList, &mStartCodeBufferSize,
                     &mStartCodeHeadersBufferBase, &mStartCodeHeadersBufferSize);

        ResetBufferState();
    }

    if (DelayOnInputDescriptor() && (mInputDescriptor.StreamType == STM_SE_STREAMTYPE_ES))
    {
        ActOnInputDescriptor(&mInputDescriptor);
        //nInputDescriptor already forwarded to next stage so reset Pts/Dts flags
        mInputDescriptor.PlaybackTimeValid = false;
        mInputDescriptor.DecodeTimeValid   = false;
    }

    return Status;
}

void Collator_Base_c::ResetBufferState()
{
    AccumulatedDataSize     = 0;
    mAccumulatedStartCodeHeadersSize  = 0;

    if (Configuration.GenerateStartCodeList && StartCodeList)
    {
        StartCodeList->NumberOfStartCodes = 0;
    }

    memset(&CodedFrameParameters, 0, sizeof(CodedFrameParameters));
    CodedFrameParameters.SourceTimeFormat = TIME_FORMAT_PTS;
}

// /////////////////////////////////////////////////////////////////////////
//
//      The discard all accumulated data function
//

CollatorStatus_t Collator_Base_c::DiscardAccumulatedData()
{
    ResetBufferState();

    return CollatorNoError;
}

// /////////////////////////////////////////////////////////////////////////
//
//      Handle a jump
//

CollatorStatus_t   Collator_Base_c::InputJump(int discontinuity)
{
    CollatorStatus_t Status = RetryGetBuffer();
    if (Status != CollatorNoError) { return Status; }

    if (mControlDataDetectionState == GotPartialControlData)
    {
        SE_DEBUG(GetGroupTrace(), "Flushing control data\n");

        Status = InputSecondStage(mGotPartialControlDataBytes, mStoredControlData);
        if (Status != CollatorNoError)
        {
            return Status;
        }
    }

    bool ContinuousReverseJump = discontinuity & STM_SE_DISCONTINUITY_SMOOTH_REVERSE ? true : false;
    bool SurplusDataInjected   = discontinuity & STM_SE_DISCONTINUITY_SURPLUS_DATA   ? true : false;
    bool EndOfStream           = discontinuity & STM_SE_DISCONTINUITY_END_OF_STREAM  ? true : false;
    bool EndOfFrame            = discontinuity & STM_SE_DISCONTINUITY_END_OF_FRAME   ? true : false;

    if (SurplusDataInjected)
    {
        DiscardAccumulatedData();
    }
    else
    {
        Status = InternalFrameFlush();
        if (Status != CollatorNoError) { return Status; }
    }

    if (EndOfFrame && !EndOfStream)
    {
        return CollatorNoError;
    }

    MarkerFrame_t MarkerFrame;
    if (EndOfStream)
    {
        MarkerFrame.mMarkerType = EosMarker;
    }
    else
    {
        MarkerFrame.mMarkerType = DiscontinuityMarker;
    }

    CollatorStatus_t status =  mCollatorSink->InsertMarkerFrameToOutputPort(MarkerFrame);
    if (status != CollatorNoError) { return status; }

    CodedFrameParameters.StreamDiscontinuity      = true;
    CodedFrameParameters.ContinuousReverseJump    = ContinuousReverseJump;

    status = InternalFrameFlush();
    if (status != CollatorNoError) { return status; }

    return CollatorNoError;
}

// /////////////////////////////////////////////////////////////////////////
//
//      Protected - The accumulate data into the buffer function
//

CollatorStatus_t   Collator_Base_c::AccumulateData(unsigned int              Length,
                                                   unsigned char            *Data)
{
    if ((AccumulatedDataSize + Length) > MaximumCodedFrameSize)
    {
        SE_ERROR("Buffer overflow. (%d > %d)\n", (AccumulatedDataSize + Length) , MaximumCodedFrameSize);
        return CollatorBufferOverflow;
    }

    memcpy(BufferBase + AccumulatedDataSize, Data, Length);
    AccumulatedDataSize += Length;
    return CollatorNoError;
}


// /////////////////////////////////////////////////////////////////////////
//
//      Protected - The accumulate data into the buffer function
//

CollatorStatus_t   Collator_Base_c::AccumulateStartCode(PackedStartCode_t      Code)
{
    if (!Configuration.GenerateStartCodeList)
    {
        return CollatorNoError;
    }

    if ((StartCodeList->NumberOfStartCodes + 1) > Configuration.MaxStartCodes)
    {
        SE_ERROR("Start code list overflow\n");
        return CollatorBufferOverflow;
    }

    StartCodeList->StartCodes[StartCodeList->NumberOfStartCodes++]      = Code;
    return CollatorNoError;
}

////////////////////////////////////////////////////////////////////////////
///
///     Copy any important information held in the input descriptor to the
///     coded frame parameters.
///
///     If the input descriptor provides any hints about playback or
///     presentation time then initialize Collator_Base_c::CodedFrameParameters
///     with these values.
///
///     \param Input The input descriptor
///
void Collator_Base_c::ActOnInputDescriptor(const PlayerInputDescriptor_t   *Input)
{
    if (Input->PlaybackTimeValid && !CodedFrameParameters.PlaybackTimeValid &&
        TimeStamp_c::IsNativeTimeFormatCoherent(Input->SourceTimeFormat, 0))
    {
        CodedFrameParameters.PlaybackTimeValid = true;
        CodedFrameParameters.PlaybackTime      = Input->PlaybackTime;
        CodedFrameParameters.SourceTimeFormat  = Input->SourceTimeFormat;
    }

    if (Input->DecodeTimeValid && !CodedFrameParameters.DecodeTimeValid &&
        TimeStamp_c::IsNativeTimeFormatCoherent(Input->SourceTimeFormat, 0))
    {
        CodedFrameParameters.DecodeTimeValid   = true;
        CodedFrameParameters.DecodeTime        = Input->DecodeTime;
        CodedFrameParameters.SourceTimeFormat  = Input->SourceTimeFormat;
    }

    CodedFrameParameters.DataSpecificFlags     |= Input->DataSpecificFlags;
}

// /////////////////////////////////////////////////////////////////////////
//
//      Protected - Check value in the ignore codes ranges
//

bool  Collator_Base_c::IsCodeTobeIgnored(unsigned char Value)
{
    for (int i = 0 ; i < Configuration.IgnoreCodesRanges.NbEntries ; i++)
    {
        if ((Value >= Configuration.IgnoreCodesRanges.Table[i].Start) && (Value <= Configuration.IgnoreCodesRanges.Table[i].End))
        {
            return true;
        }
    }

    return false;
}

CollatorStatus_t Collator_Base_c::PackHeader(int startCodeCode, unsigned char *header, unsigned int headerSize)
{
    if (mAccumulatedStartCodeHeadersSize + headerSize + COLLATOR_NB_BYTES_FOR_FRAME_HEADER_SIZE > mStartCodeHeadersBufferSize)
    {
        SE_ERROR("Header Buffer overflow\n");
        return CollatorError;
    }

    SE_EXTRAVERB(GetGroupTrace(), "startCodeCode=0x%x headerSize=%u\n", startCodeCode, headerSize);

    // Collated frame header size is coded on a COLLATOR_NB_BYTES_FOR_FRAME_HEADER_SIZE-byte value
    // (32-bit is necessary for HEVC and H264)
    for (int i = 0; i < COLLATOR_NB_BYTES_FOR_FRAME_HEADER_SIZE; i++)
    {
        *(mStartCodeHeadersBufferBase + mAccumulatedStartCodeHeadersSize++) = ((headerSize >> (8 * i)) & 0xFF);
    }

    // Pack header
    memcpy(mStartCodeHeadersBufferBase + mAccumulatedStartCodeHeadersSize, header, headerSize);
    mAccumulatedStartCodeHeadersSize += headerSize;

    return CollatorNoError;
}

////////////////////////////////////////////////////////////////////////////
///
/// Function to take care of Elementary Streams. It will insert a dummy PES
/// header if there is a valid pts in the Input descriptor.
/// Also this function will avoid the control sync word detection as for
/// ES stream it is assumed that there is no Control sync word in ES frame
///
/// \return Collator status code, CollatorNoError indicates success.
///
CollatorStatus_t   Collator_Base_c::HandleEsStream(const PlayerInputDescriptor_t *Input,
                                                   unsigned int                   DataLength,
                                                   const void                    *Data)
{
    if (Input->PlaybackTimeValid)
    {
        // create a PES header and Insert that to Input second stage
        char pes_header[PES_MAX_HEADER_SIZE];

        // No need to insert a valid pts in pes header as ActOnInputDescriptor() already updated
        // CodedFrameParameters
        PlayerInputDescriptor_t InputDescriptor = *Input;

        InputDescriptor.PlaybackTimeValid = false;
        InputDescriptor.DecodeTimeValid   = false;

        unsigned int length = FillPesHeader(&InputDescriptor, Configuration.StreamIdentifierCode,
                                            DataLength, pes_header);

        // forward the pesHeader to the second stage
        CollatorStatus_t Status = InputSecondStage(length, pes_header);
        if (Status != CollatorNoError)
        {
            return Status;
        }
    }

    // In ES stream there should not be any  control sync word, so forward the
    // Data to second stage for further processing
    CollatorStatus_t Status = InputSecondStage(DataLength, Data);

    return Status;
}

CollatorStatus_t Collator_Base_c::RetryGetBuffer()
{
    if (BufferBase) { return CollatorNoError; }

    SE_DEBUG(GetGroupTrace(), "\n");

    // Acquire an operating buffer
    CollatorStatus_t Status = mCollatorSink->GetNewBuffer(
                                  &BufferBase, &MaximumCodedFrameSize,
                                  &StartCodeList, &mStartCodeBufferSize,
                                  &mStartCodeHeadersBufferBase, &mStartCodeHeadersBufferSize);
    if (Status != CollatorNoError) { return Status; }

    ResetBufferState();

    return CollatorNoError;
}


////////////////////////////////////////////////////////////////////////////
///
/// De-packetize incoming data and pass it to the elementary stream handler.
///
/// \return Collator status code, CollatorNoError indicates success.
///
CollatorStatus_t   Collator_Base_c::Input(const PlayerInputDescriptor_t *Input,
                                          unsigned int                   DataLength,
                                          const void                    *Data)
{
    SE_DEBUG(GetGroupTrace(), "DataLength=%u\n", DataLength);
    CollatorStatus_t Status = CollatorNoError;

    Status = RetryGetBuffer();
    if (Status != CollatorNoError) { return Status; }

    // For some cases CodedFrameParameter update is done after InternalFrameFlush
    // while for some cases(divx) it is done before.
    if (DelayOnInputDescriptor() && (Input->StreamType == STM_SE_STREAMTYPE_ES))
    {
        // Store Input descriptor and use it later to update CodedFrameParameter
        // after InternalFrameFlush
        mInputDescriptor = *Input;
        Status = HandleEsStream(Input, DataLength, Data);
        return Status;
    }
    else
    {
        ActOnInputDescriptor(Input);
    }

    if (ESSupportsControlDataInsertion())
    {
        const unsigned char *ChunkStart  = static_cast<const unsigned char *>(Data);
        mControlDataRemainingData        = static_cast<const unsigned char *>(Data);
        mControlDataRemainingLength      = DataLength;

        // Continually handle units of input until all input (including the Stored Control Bytes) is exhausted (or an error occurs).
        //
        // Be aware that our helper functions may, during their execution, cause state changes
        // that result in a different branch being taken next time round the loop.
        //

        while (Status == CollatorNoError &&
               (mControlDataRemainingLength != 0 ||
                (mGotPartialControlDataBytes == PES_CONTROL_SIZE &&
                 mControlDataDetectionState != SeekingControlData)))
        {
            // Check if not in low power state
            if (mCollatorSink->PlaybackIsInLowPowerState())
            {
                // Stop processing data to speed-up low power enter procedure (bug 24248)
                break;
            }

            if (mControlDataDetectionState == SeekingControlData)
            {
                SearchForControlData();

                if (ChunkStart != mControlDataRemainingData)
                {
                    // pass the data that has been consumed to second stage collation
                    Status = InputSecondStage(mControlDataRemainingData - ChunkStart, ChunkStart);
                }
            }
            else if (mControlDataDetectionState == GotPartialControlData)
            {
                ReadPartialControlData();

                if (mControlDataDetectionState == SeekingControlData)
                {
                    Status = HandleWronglyAccumulatedControlData();
                    if (mControlDataDetectionState == SeekingControlData) { ChunkStart = mControlDataRemainingData; }
                }
            }
            else if (mControlDataDetectionState == GotControlData)
            {
                Status = ProcessControlData();

                // switch back to SeekingControlData state from position
                // right after the Control Data we just found
                mControlDataDetectionState = SeekingControlData;
                ChunkStart = mControlDataRemainingData;
            }
        }
    }
    else
    {
        Status = InputSecondStage(DataLength, Data);
    }

    return Status;
}


void Collator_Base_c::SearchForControlData(void)
{
    int CodeOffset;
    int TrailingStartCodeBytes;

    bool ControlDataStartCodeFound = FindNextControlDataStartCode(mControlDataRemainingData, mControlDataRemainingLength, &CodeOffset, &TrailingStartCodeBytes);

    if (ControlDataStartCodeFound)
    {
        SE_DEBUG(GetGroupTrace(), "Found marker frame start code at offset %d!\n", CodeOffset);

        // discard any data leading up to the start code
        mControlDataRemainingData        += CodeOffset;
        mControlDataRemainingLength      -= CodeOffset;

        // switch state
        mControlDataDetectionState       = GotPartialControlData;
        mGotPartialControlDataBytes      = 0;
    }
    else
    {
        SE_DEBUG(GetGroupTrace(), "Discarded %d bytes while searching for marker frame header\n", mControlDataRemainingLength - TrailingStartCodeBytes);

        mControlDataRemainingData        += (mControlDataRemainingLength - TrailingStartCodeBytes);

        // Set the remaining length to 0 as if there are some trailing start code bytes
        // they will be consumed by accumulating them in our mStoredControlData below
        mControlDataRemainingLength      = 0;

        switch (TrailingStartCodeBytes)
        {
        case 3:
            mStoredControlData[2] = 0x01; // FALLTHROUGH
        case 2:
            mStoredControlData[1] = 0x00; // FALLTHROUGH
        case 1:
            mStoredControlData[0] = 0x00;
            SE_DEBUG(GetGroupTrace(), "%d trailing start code bytes => Changing state to GotPartialControlData\n", TrailingStartCodeBytes);
            mControlDataDetectionState = GotPartialControlData;
            mGotPartialControlDataBytes = TrailingStartCodeBytes;
            break;
        case 0:
            break;
        default:
            SE_ASSERT(0);
            break;
        }
    }
}

////////////////////////////////////////////////////////////////////////////
///
/// Accumulate a marker frame
///
/// Accumulate incoming data until we have the size of a marker frame
/// Then check that it is indeed a marker frame
///
/// \return Collator status code, CollatorNoError indicates success.
///
void Collator_Base_c::ReadPartialControlData(void)
{
    if (mGotPartialControlDataBytes < PES_CONTROL_SIZE)
    {
        SE_DEBUG(GetGroupTrace(), "Accumulating ControlData Frame\n");

        unsigned int BytesToRead = Min(mControlDataRemainingLength, PES_CONTROL_SIZE - mGotPartialControlDataBytes);
        memcpy(mStoredControlData + mGotPartialControlDataBytes, mControlDataRemainingData, BytesToRead);

        mGotPartialControlDataBytes += BytesToRead;
        mControlDataRemainingData += BytesToRead;
        mControlDataRemainingLength -= BytesToRead;
        return;
    }

    //
    // We now have accumulated the size of a marker frame. Let's check if it is one!
    //

    // Check start code. This is needed as in case of trailing start code bytes,
    // we may have an invalid start code
    if (mStoredControlData[0] != 0x00 || mStoredControlData[1] != 0x00 || mStoredControlData[2] != 0x01 || mStoredControlData[3] != 0xFB) { goto invalid_marker_frame; }

    // Check PES_packet_length
    if (mStoredControlData[4] != 0x00 || mStoredControlData[5] != 0x14) { goto invalid_marker_frame; }

    // Check flags
    if (mStoredControlData[6] != 0x80 || mStoredControlData[7] != 0x01) { goto invalid_marker_frame; }

    // Check PES_header_data_length
    if (mStoredControlData[8] != 0x11) { goto invalid_marker_frame; }

    // Check extension flags
    if (mStoredControlData[9] != 0x80) { goto invalid_marker_frame; }

    // Check ascii marker pattern
    if (mStoredControlData[10] != 'S' || mStoredControlData[11] != 'T' || mStoredControlData[12] != 'M' || mStoredControlData[13] != 'M') { goto invalid_marker_frame; }

    SE_DEBUG(GetGroupTrace(), "Found a marker frame!\n");

    mControlDataDetectionState = GotControlData;

    return;

invalid_marker_frame:
    SE_DEBUG(GetGroupTrace(), "Not a marker frame!\n");

    mControlDataDetectionState = SeekingControlData;
}


CollatorStatus_t   Collator_Base_c::HandleWronglyAccumulatedControlData()
{
    CollatorStatus_t Status;

    // there could be a marker frame in the accumulated data
    // start searching at offset 1 to avoid infinite loop in case
    // the accumulated data has a marker start code at the beginning
    int offset, trailingStartCodeBytes;
    bool ControlDataStartCodeFound = FindNextControlDataStartCode(mStoredControlData + 1, PES_CONTROL_SIZE - 1, &offset, &trailingStartCodeBytes);

    if (ControlDataStartCodeFound || trailingStartCodeBytes)
    {
        if (ControlDataStartCodeFound)
        {
            SE_DEBUG(GetGroupTrace(), "ControlData Start Code found in accumulated data\n");
            mGotPartialControlDataBytes = PES_CONTROL_SIZE - 1 - offset;
        }
        else
        {
            SE_DEBUG(GetGroupTrace(), "%d Trailing startcode bytes in accumulated data\n", trailingStartCodeBytes);
            mGotPartialControlDataBytes = trailingStartCodeBytes;
        }
        Status = InputSecondStage(PES_CONTROL_SIZE - mGotPartialControlDataBytes, mStoredControlData);
        memmove(mStoredControlData, mStoredControlData + PES_CONTROL_SIZE - mGotPartialControlDataBytes, mGotPartialControlDataBytes);
        mControlDataDetectionState = GotPartialControlData;
    }
    else
    {
        // no start code found, so pass the accumulated data to second stage collation
        // and stay in SeekingControlData state
        Status = InputSecondStage(PES_CONTROL_SIZE, mStoredControlData);
    }

    return Status;
}

CollatorStatus_t Collator_Base_c::ProcessControlData()
{
    int controlDataType = mStoredControlData[14];

    SE_DEBUG(GetGroupTrace(), "Received Control Data Type: %d\n", controlDataType);

    switch (controlDataType)
    {
    case PES_CONTROL_REQ_TIME:
        mReqTimeMarker.mMarkerType = ReqTimeMarker;
        // markerID0 is 4 byte long, from byte 16 to 19
        mReqTimeMarker.mReqTimeMarkerData.mMarkerId0 = (((unsigned int)mStoredControlData[15]) << 24) + (((unsigned int)mStoredControlData[16]) << 16)
                                                       + (((unsigned int)mStoredControlData[17]) << 8) + ((unsigned int)mStoredControlData[18]);

        // markerID1 is 4 byte long, from byte 20 to 23
        mReqTimeMarker.mReqTimeMarkerData.mMarkerId1 = (((unsigned int)mStoredControlData[19]) << 24) + (((unsigned int)mStoredControlData[20]) << 16)
                                                       + (((unsigned int)mStoredControlData[21]) << 8) + ((unsigned int)mStoredControlData[22]);

        mFoundReqTimeControlData = true;
        return CollatorNoError;

    case PES_CONTROL_BRK_FWD:
    case PES_CONTROL_BRK_BWD:
    {
        return InputJump(STM_SE_DISCONTINUITY_CONTINUOUS);
    }

    case PES_CONTROL_BRK_BWD_SMOOTH:
    {
        return InputJump(STM_SE_DISCONTINUITY_SMOOTH_REVERSE);
    }

    case PES_CONTROL_BRK_END_OF_STREAM:
    {
        return InputJump(STM_SE_DISCONTINUITY_END_OF_STREAM);
    }

    case PES_CONTROL_BRK_SPLICING:
    {
        //Flush the last frame out before the splicing marker
        CollatorStatus_t Status = InternalFrameFlush();
        if (Status != CollatorNoError) { return Status; }

        //Send discontinuity to reset the PTS extrapolation algorithm
        Status = InputJump(STM_SE_DISCONTINUITY_CONTINUOUS);
        if (Status != CollatorNoError) { return Status; }

        MarkerFrame_t MarkerFrame;
        MarkerFrame.mMarkerType = SplicingMarker;

        MarkerFrame.mSplicingMarkerData.mSplicingFlags = (unsigned int) mStoredControlData[15];
        MarkerFrame.mSplicingMarkerData.mPtsOffset = ((int64_t)mStoredControlData[20] << 32) + (((uint64_t) mStoredControlData[16]) << 24) + (((uint64_t)mStoredControlData[17]) << 16)
                                                     + (((uint64_t)mStoredControlData[18]) << 8) + ((uint64_t)mStoredControlData[19]);

        // Sign extend from 40 bits to 64 bits
        MarkerFrame.mSplicingMarkerData.mPtsOffset = (MarkerFrame.mSplicingMarkerData.mPtsOffset << 24) >> 24;

        return mCollatorSink->InsertMarkerFrameToOutputPort(MarkerFrame);
    }

    default:
        SE_ERROR("Unsupported Control Data type\n");
        return CollatorError;
    }
}

////////////////////////////////////////////////////////////////////////////
///
/// Scan the input until an appropriate PES start code is found.
///
/// Scan the buffer given in parameter until an appropriate
/// ControlData start code is found (0x00 0x00 0x01 0xFB)
///
/// \return true if a start code is found, in which case the
/// position of the start code is returned in the offset parameter
/// \return false if no start code is found, in which case the number
/// of trailing start code bytes is returned in the trailingStartCodeBytes
/// parameter. trailingStartCodeBytes is in [0..3]
///
bool Collator_Base_c::FindNextControlDataStartCode(const unsigned char *data, int length,
                                                   int *offset, int *trailingStartCodeBytes)
{
    unsigned int SearchedSynchro, Code1, Code2, Code3;
    unsigned int *data32;
    int          i, n;

    switch (length)
    {
    case 0:
        *trailingStartCodeBytes = 0;
        return false;

    case 1:
        *trailingStartCodeBytes = ((data[0] == 0x00U) ? 1 : 0);
        return false;

    case 2:
        if ((data[0] == 0x00U) && (data[1] == 0x00U))
        {
            *trailingStartCodeBytes = 2;
        }
        else if (data[1] == 0x00U)
        {
            *trailingStartCodeBytes = 1;
        }
        else
        {
            *trailingStartCodeBytes = 0;
        }
        return false;

    case 3:
        if (data[0] == 0x00U && data[1] == 0x00U && data[2] == 0x01U)
        {
            *trailingStartCodeBytes = 3;
        }
        else if ((data[1] == 0x00U) && (data[2] == 0x00U))
        {
            *trailingStartCodeBytes = 2;
        }
        else if (data[2] == 0x00U)
        {
            *trailingStartCodeBytes = 1;
        }
        else
        {
            *trailingStartCodeBytes = 0;
        }
        return false;

    default:
        data32 = (unsigned int *)((((int) data) + 3) & ~3);     // int (4 bytes) aligned next address
        n      = ((int) data32) - ((int) data);

        // prologue : start to fill first 4 bytes word from 32 bits unaligned leading bytes
        switch (n)
        {
        default:
            Code2 = 0xFFFFFFFFU;
            break;
        case 1:
            Code2 = 0x00FFFFFFU                      |
                    (((unsigned int) data[0]) << 24);
            break;
        case 2:
            Code2 = 0x0000FFFFU                      |
                    (((unsigned int) data[0]) << 16) |
                    (((unsigned int) data[1]) << 24);
            break;
        case 3:
            Code2 = 0x000000FFU                      |
                    (((unsigned int) data[0]) <<  8) |
                    (((unsigned int) data[1]) << 16) |
                    (((unsigned int) data[2]) << 24);
            break;
        }

        // main search loop on int (4 bytes) basis
        SearchedSynchro = 0xFB010000U;  // 4 bytes synchro ordered in little-endian
        for (i = (length - n) / 4; i > 0; i--, n += 4)
        {
            if (i > 16)
            {
                __builtin_prefetch(data32 + 16);
            }
            Code1 = Code2;
            Code2 = *data32++;
            if (Code1 == SearchedSynchro)
            {
                *offset = n - 4;
                return true;
            }
            Code3 = (Code1 >> 8);
            if (Code3 <= 0x00010000U)   // optimization
            {
                if (((Code2 << 24) | Code3) == SearchedSynchro)
                {
                    *offset = n - 3;
                    return true;
                }
                if (((Code2 << 16) | (Code1 >> 16)) == SearchedSynchro)
                {
                    *offset = n - 2;
                    return true;
                }
                if (((Code2 <<  8) | (Code1 >> 24)) == SearchedSynchro)
                {
                    *offset = n - 1;
                    return true;
                }
            }
        }

        // epilogue : search synchro in trailing bytes
        data = (unsigned char *) data32;
        switch (length - n)
        {
        case 0:
            if (Code2 == SearchedSynchro)
            {
                *offset = n - 4;
                return true;
            }
            break;
        case 1:
            if (Code2 == SearchedSynchro)
            {
                *offset = n - 4;
                return true;
            }
            Code1 = (unsigned int) data[0];
            Code2 = (Code2 >> 8) | (Code1 << 24);
            if (Code2 == SearchedSynchro)
            {
                *offset = n - 3;
                return true;
            }
            break;
        case 2:
            if (Code2 == SearchedSynchro)
            {
                *offset = n - 4;
                return true;
            }
            Code1 = (unsigned int) data[0];
            Code2 = (Code2 >> 8) | (Code1 << 24);
            if (Code2 == SearchedSynchro)
            {
                *offset = n - 3;
                return true;
            }
            Code1 = (unsigned int) data[1];
            Code2 = (Code2 >> 8) | (Code1 << 24);
            if (Code2 == SearchedSynchro)
            {
                *offset = n - 2;
                return true;
            }
            break;
        case 3:
            if (Code2 == SearchedSynchro)
            {
                *offset = n - 4;
                return true;
            }
            Code1 = (unsigned int) data[0];
            Code2 = (Code2 >> 8) | (Code1 << 24);
            if (Code2 == SearchedSynchro)
            {
                *offset = n - 3;
                return true;
            }
            Code1 = (unsigned int) data[1];
            Code2 = (Code2 >> 8) | (Code1 << 24);
            if (Code2 == SearchedSynchro)
            {
                *offset = n - 2;
                return true;
            }
            Code1 = (unsigned int) data[2];
            Code2 = (Code2 >> 8) | (Code1 << 24);
            if (Code2 == SearchedSynchro)
            {
                *offset = n - 1;
                return true;
            }
            break;
        }

        // synchro not found
        if ((Code2 >> 8) == 0x00010000U)
        {
            *trailingStartCodeBytes = 3;
        }
        else if ((Code2 >> 16) == 0x00000000U)
        {
            *trailingStartCodeBytes = 2;
        }
        else if ((Code2 >> 24) == 0x00000000U)
        {
            *trailingStartCodeBytes = 1;
        }
        else
        {
            *trailingStartCodeBytes = 0;
        }
        return false;
    }
}

unsigned int Collator_Base_c::FillPesHeader(const PlayerInputDescriptor_t *Input, const unsigned int StreamId,
                                            const unsigned int DataLength, char *Data)
{
    unsigned int filled_length = 0;
    // Fill PES startcode 001<streamId>
    Data[filled_length++] = 0x00;       // Byte 0
    Data[filled_length++] = 0x00;       // Byte 1
    Data[filled_length++] = 0x01;       // Byte 2
    Data[filled_length++] = StreamId;   // Byte 3

    unsigned int pes_packet_length = DataLength + 3 +
                                     (Input->PlaybackTimeValid ? 5 : 0) +
                                     ((Input->PlaybackTimeValid && Input->DecodeTimeValid) ? 5 : 0);

    SE_DEBUG(GetGroupTrace(), "pes_packet_length =%d\n", pes_packet_length);
    Data[filled_length++] = pes_packet_length >> 8;        // Byte 4
    Data[filled_length++] = pes_packet_length & 0xff;      // Byte 5

    Data[filled_length++] = 0x80;  // Byte6 - 0b1000 0000

    if (Input->PlaybackTimeValid)
    {
        if (Input->DecodeTimeValid)
        {
            // Both PTS and DTS are present
            Data[filled_length++] = 0xC0; // Byte 7 : 0b1100 0000
            Data[filled_length++] = 0x0A; // Byte 8 : header_data_length = 10 ( 5 byte pts + 5 Byte Dts)
            Data[filled_length++] = ((Input->PlaybackTime >> 29) & 0x0E) | 0x31;   // Byte 9 : pts : 0b0011 ppp1
            Data[filled_length++] = ((Input->PlaybackTime >> 22) & 0xFF);          // Byte 10 : pts : 0bpppp pppp
            Data[filled_length++] = ((Input->PlaybackTime >> 14) & 0xFE) | 0x01;   // Byte 11 : pts : 0bpppp ppp1
            Data[filled_length++] = ((Input->PlaybackTime >> 7)  & 0xFF);          // Byte 12 : pts : 0bpppp pppp
            Data[filled_length++] = ((Input->PlaybackTime << 1) & 0xFE) | 0x01;    // Byte 13 : pts : 0bpppp ppp1

            Data[filled_length++] = ((Input->DecodeTime >> 29) & 0x0E) | 0x11;   // Byte 14  : pts : 0b0001 ddd1
            Data[filled_length++] = ((Input->DecodeTime >> 22) & 0xFF);          // Byte 15 : pts : 0bdddd dddd
            Data[filled_length++] = ((Input->DecodeTime >> 14) & 0xFE) | 0x01;   // Byte 16 : pts : 0bdddd ddd1
            Data[filled_length++] = ((Input->DecodeTime >> 7)  & 0xFF);          // Byte 17 : pts : 0bdddd dddd
            Data[filled_length++] = ((Input->DecodeTime << 1) & 0xFE) | 0x01;    // Byte 18 : pts : 0bdddd ddd1
        }
        else
        {
            // Only PTS

            Data[filled_length++] = 0x80; // Byte 7 : 0b1000 0000
            Data[filled_length++] = 5; // Byte 8 ; header_data_length = 5 byte pts
            Data[filled_length++] = ((Input->PlaybackTime >> 29) & 0x0E) | 0x21;   // Byte 9 : pts : 0b0010 ppp1
            Data[filled_length++] = ((Input->PlaybackTime >> 22) & 0xFF);          // Byte 10 : pts : 0bpppp pppp
            Data[filled_length++] = ((Input->PlaybackTime >> 14) & 0xFE) | 0x01;   // Byte 11 : pts : 0bpppp ppp1
            Data[filled_length++] = ((Input->PlaybackTime >> 7)  & 0xFF);          // Byte 12 : pts : 0bpppp pppp
            Data[filled_length++] = ((Input->PlaybackTime << 1) & 0xFE) | 0x01;    // Byte 13 : pts : 0bpppp ppp1
        }
    }
    else
    {
        // Neither Pts nor Dts i.e. pes header with Invalid time stamps
        // Byte 7 : 0b0000 0000
        Data[filled_length++] = 0x00;
        Data[filled_length++] = 0; // Byte 8 ; header_data_length = 0
    }

    return filled_length;
}
