/************************************************************************
Copyright (C) 2003-2014 STMicroelectronics. All Rights Reserved.

This file is part of the Streaming Engine.

Streaming Engine is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License version 2 as
published by the Free Software Foundation.

Streaming Engine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with Streaming Engine; see the file COPYING.  If not, write to the Free
Software Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307,
USA.

The Streaming Engine Library may alternatively be licensed under a
proprietary license from ST.
************************************************************************/

#ifndef H_COLLATOR_BASE
#define H_COLLATOR_BASE

#include "player.h"
#include "player_generic.h"
#include "player_playback.h"
#include "player_stream.h"
#include "pes.h"

#undef  TRACE_TAG
#define TRACE_TAG "Collator_Base_c"

#define MAX_IGNORE_CODE_RANGES  5

// H264/HEVC collated headers size must be coded on at least 3 bytes
#define COLLATOR_NB_BYTES_FOR_FRAME_HEADER_SIZE 4

typedef struct IgnoreRange_s
{
    unsigned char Start;
    unsigned char End;  // Start and End are part of the ignored codes list
} IgnoreRange_t;

typedef struct IgnoreCodesRanges_s
{
    int NbEntries;
    IgnoreRange_t  Table[MAX_IGNORE_CODE_RANGES];
} IgnoreCodesRanges_t;


typedef struct CollatorConfiguration_s
{
    bool                  GenerateStartCodeList;        // Start code list control
    unsigned int          MaxStartCodes;

    bool                  RequireFrameHeaders;             // true if Frame Headers are required by the codec
    unsigned int          MaxHeaderSize;

    unsigned int          StreamIdentifierMask;         ///< For PES indicates the pes stream identifier
    unsigned int          StreamIdentifierCode;

    unsigned int          SubStreamIdentifierMask;      ///< For PES of type extended_stream_id indicates the pes sub stream identifier (set to zero if no filtering is to be done)
    unsigned int          SubStreamIdentifierCodeStart;
    unsigned int          SubStreamIdentifierCodeStop;

    unsigned char         BlockTerminateMask;           ///< Which Start codes indicate frame complete
    unsigned char         BlockTerminateCode;

    unsigned char         StreamTerminateFlushesFrame;  // Use a stream termination code to force a frame flush (helps display of last frame)
    unsigned char         StreamTerminationCode;

    IgnoreCodesRanges_t   IgnoreCodesRanges;             // Start codes to ignore

    bool                  InsertFrameTerminateCode;     // if set causes a terminal code to be inserted into the
    unsigned char         TerminalCode;                 // buffer, but not recorded in the start code list.

    unsigned int          ExtendedHeaderLength;         ///< Number of bytes of extended PES header (to be skipped)

    bool                  DeferredTerminateFlag;        // Terminate after finding Terminal Code

    bool                  DetermineFrameBoundariesByPresentationToFrameParser;  // Ask frame parser about frame boundaries
} CollatorConfiguration_t;


class Collator_Base_c : public CollatorProcessorInterface_c
{
public:
    Collator_Base_c();  // TODO(pht) shall take CollatorConfiguration_t *configuration in input param

    virtual ~Collator_Base_c();

    // CollatorProcessorInterface_c

    virtual CollatorStatus_t SetSink(CollatorSinkInterface_c *sink, unsigned int maximumCodedFrameSize);

    CollatorStatus_t   Halt();

    virtual PlayerStatus_t     SpecifySignalledEvents(PlayerEventMask_t        EventMask,
                                                      void                    *EventUserData);

    virtual CollatorStatus_t   InputJump(int discontinuity);

    virtual void GetCapabilities(bool           isAboveHdStream,
                                 bool          *generateStartCodeList,
                                 unsigned int  *maxStartCodes,
                                 bool          *requireFrameHeaders,
                                 unsigned int  *maxHeaderSize)
    {
        (void)isAboveHdStream; // warning removal
        *generateStartCodeList  = Configuration.GenerateStartCodeList;
        *maxStartCodes          = Configuration.MaxStartCodes;
        *requireFrameHeaders    = Configuration.RequireFrameHeaders;
        *maxHeaderSize          = Configuration.MaxHeaderSize;
    }

    virtual CollatorStatus_t   Input(const PlayerInputDescriptor_t  *Input,
                                     unsigned int                    DataLength,
                                     const void                     *Data);

    // Accordingly with PackHeader function : unpack header and return its size
    static unsigned int GetFrameHeaderSize(unsigned char *header)
    {
        unsigned int size = header[0];
        for (int i = 1; i < COLLATOR_NB_BYTES_FOR_FRAME_HEADER_SIZE; i++)
        {
            size |= (header[i] << (8 * i));
        }
        return size;
    }

protected:
    PlayerEventMask_t        EventMask;
    void                    *EventUserData;
    int                      mGroupTrace;

    CollatorConfiguration_t  Configuration;


    unsigned int             AccumulatedDataSize;
    unsigned char           *BufferBase;
    unsigned int             MaximumCodedFrameSize;
    CodedFrameParameters_t   CodedFrameParameters;
    StartCodeList_t         *StartCodeList;
    unsigned int             mStartCodeBufferSize;
    unsigned char           *mStartCodeHeadersBufferBase;
    unsigned int             mStartCodeHeadersBufferSize;
    unsigned int             mAccumulatedStartCodeHeadersSize;
    bool                     mFoundReqTimeControlData;
    MarkerFrame_t            mReqTimeMarker;

    CollatorSinkInterface_c    *mCollatorSink;

    // methods

    CollatorStatus_t   AccumulateData(unsigned int              Length,
                                      unsigned char            *Data);

    CollatorStatus_t   AccumulateStartCode(PackedStartCode_t         Code);

    void               ActOnInputDescriptor(const PlayerInputDescriptor_t  *Input);

    bool               IsCodeTobeIgnored(unsigned char Value);

    void                ResetBufferState();

    CollatorStatus_t PackHeader(int startCodeCode, unsigned char *header, unsigned int headerSize);

    inline void SetGroupTrace(int group) { mGroupTrace = group; }
    inline int GetGroupTrace() const { return mGroupTrace; }

    virtual CollatorStatus_t   InternalFrameFlush();
    virtual CollatorStatus_t   DiscardAccumulatedData();
    virtual CollatorStatus_t   FillFrameHeaders() { SE_ASSERT(0); return CollatorNoError; }

    // derived classes can override this method if control data insertion
    // is not supported by some formats
    virtual bool ESSupportsControlDataInsertion() { return true; }

private:
    // Control Data Management
    enum ControlDataDetectionState
    {
        SeekingControlData,
        GotPartialControlData,
        GotControlData
    };

    ControlDataDetectionState       mControlDataDetectionState;
    const unsigned char            *mControlDataRemainingData;
    int                             mControlDataRemainingLength;
    int                             mControlDataTrailingStartCodeBytes;
    int                             mGotPartialControlDataBytes;
    unsigned char                   mStoredControlData[PES_CONTROL_SIZE];
    PlayerInputDescriptor_t         mInputDescriptor;

    void SearchForControlData(void);
    void ReadPartialControlData(void);
    CollatorStatus_t HandleWronglyAccumulatedControlData();
    CollatorStatus_t ProcessControlData(void);
    bool FindNextControlDataStartCode(const unsigned char *data,
                                      int length,
                                      int *CodeOffset,
                                      int *trailingStartCodeBytes);
    virtual CollatorStatus_t InputSecondStage(unsigned int  DataLength,
                                              const void   *Data) = 0;

    virtual bool DelayOnInputDescriptor() { return false;}

    unsigned int FillPesHeader(const PlayerInputDescriptor_t *Input,
                               const unsigned int             StreamId,
                               const unsigned int             DataLength,
                               char                          *Data);

    CollatorStatus_t   HandleEsStream(const PlayerInputDescriptor_t *Input,
                                      unsigned int                   DataLength,
                                      const void                    *Data);

    CollatorStatus_t RetryGetBuffer();

    DISALLOW_COPY_AND_ASSIGN(Collator_Base_c);
};

#endif
