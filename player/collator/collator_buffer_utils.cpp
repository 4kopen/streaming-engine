/************************************************************************
Copyright (C) 2003-2014 STMicroelectronics. All Rights Reserved.

This file is part of the Streaming Engine.

Streaming Engine is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License version 2 as
published by the Free Software Foundation.

Streaming Engine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with Streaming Engine; see the file COPYING.  If not, write to the Free
Software Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307,
USA.

The Streaming Engine Library may alternatively be licensed under a
proprietary license from ST.
************************************************************************/
#include "collator_buffer_utils.h"

////////////////////////////////////////////////////////////////////////////
/// Utility Functions <Start>
////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////
/// \fn CollatorStatus_t  CollatorBufferUtils_c::FindNextStartId(
///                                             CollatorBufferDesc_s *PrevBuf,
///                                             CollatorBufferDesc_s *CurBuf,
///                                             int &StartCodeOffset,
///                                             unsigned char &StreamId,
///                                             bool &Completed)
///
///  PrevBuf (in) : previous buffer pointer - normally containing max of 3 bytes
///  CurBuf (in)  : Current ES buffer pointer
///  StartCodeOffset (out) : returns position of start code (001) (wrt to
///       total buffer length (Previous length + Current length)
///  Stream Id (out) : contains SID of Sequence 001<SID>
///  Completed flag (out) : is set to true if Start code was found, else false
/// \return Collator status code, CollatorNoError indicates success.
///
CollatorStatus_t   CollatorBufferUtils_c::FindNextStartId(CollatorBufferDesc_s *PrevBuf,
                                                          CollatorBufferDesc_s *CurBuf,
                                                          int *StartCodeOffset,
                                                          unsigned char *StreamId,
                                                          bool *Completed,
                                                          bool SearchId)
{
    unsigned int TotalLength = PrevBuf->Len + CurBuf->Len;
    *Completed = false;

    if (TotalLength < 4)
    {
        // Insufficient data, start code can't be found
        // It's not an error, though we considering operation
        // not Completed.
        return CollatorNoError;
    }

    // Sanity check for Previous Buffer
    if ((PrevBuf->Len != 0) && (PrevBuf->Payload == NULL))
    {
        SE_FATAL("Corruption in Previous Buffer\n");
        return CollatorBufferUnderflow;
    }

    //sanity check for Current Buffer
    if ((CurBuf->Len != 0) && (CurBuf->Payload == NULL))
    {
        SE_FATAL("Corruption in Current Buffer\n");
        return CollatorBufferUnderflow;
    }

    unsigned char *RemainingData = NULL; // Pointer to search Start code between both buffers
    // In incoming loop Need to reference 2 previous bytes for "00"
    // To simplify checking, simply copy 3 bytes (add. byte for SDI)
    // from Beginning of CurBuf to PrevBuf
    if (PrevBuf->Len)
    {
        if (PrevBuf->Len + Min(CurBuf->Len, 3U) > PrevBuf->MaxSize)
        {
            SE_FATAL("Corruption in Previous Buffer\n");
            return CollatorBufferUnderflow;
        }

        for (unsigned int i = 0 ; i < Min(CurBuf->Len, 3U); i++)
        {
            PrevBuf->Payload[PrevBuf->Len + i] = CurBuf->Payload[i];
        }
        // Start from Payload[2] as logic is to check current
        // byte as "0x1" and previous 2 bytes as "0x0"
        RemainingData = &PrevBuf->Payload[2];
    }

    // Check buffer for the start code prefix 001
    // to return SID make sure we have enough 4bytes, that's why loop
    // is till TotalLength-1
    for (unsigned int i = 2; i < TotalLength - 1; i++)
    {
        if ((i - 2) == PrevBuf->Len)
        {
            //buffer1 is exhausted, changeover to buffer2
            RemainingData = &CurBuf->Payload[2];
        }

        if (RemainingData[0] == 1)
        {
            if (RemainingData[-1] == 0  && RemainingData[-2] == 0)
            {
                // If 001 is found, return -2 index
                *StartCodeOffset = i - 2;
                if (SearchId == true)
                {
                    if (RemainingData[1] == *StreamId)
                    {
                        *Completed = true;
                        break;
                    }
                }
                else
                {
                    *StreamId = RemainingData[1];
                    *Completed = true;
                    break;
                }
            }
        }

        RemainingData++;
    }

    return CollatorNoError;
}

////////////////////////////////////////////////////////////////////////////
/// \fn CollatorStatus_t CollatorBufferUtils_c::CopyBufferTillNextStartCode(
///                                           CollatorBufferDesc_s &PrevBuf,
///                                           CollatorBufferDesc_s &CurBuf,
///                                           unsigned char *DestBuffer,
///                                           unsigned int &NumBytesCopied,
///                                           unsigned char &StreamId,
///                                           bool &Completed)
///
///  Copy Buffer Till Beginning of Next Start Code (001 SID)
///  Updates last 3 bytes in Previous buffer in case Start code is not found
///  till end of buffer
/// Logic brief
/// 1. Assuming Start code is at end of frame 001SID, then copy till beginning of it
/// 2. Assuming Start code is not found, then skip 3 bytes and wait for next frame to process it
///    Copy these 3 bytes
///
///  PrevBuf (in/out) : Point to Previous Input buffer (containing trailing
///         bytes), this function also updates Previous buffer depending
///         on needs e.g. if new bytes needs to be saved / previous
///         buffer has been used
///  CurBuf (in/out) : Point to Current Input buffer, this function updates
///        current buffer length and also updates current buffer position
///  DestBuffer (in) : points to Output buffer (Could be collated buffer
///        or header)
///  NumBytesCopies (out) : returns Bytes copied by the function
///  Stream Id (out) : contains SID of Sequence 001<SID>
///  Completed flag (out) : is set to true if Start code was found, else false
///
/// Note : Caller must keep calling this function by its State machine across
///        multiple ES packets till Completed flag is not true
///
/// \return Collator status code, CollatorNoError indicates success.
///
CollatorStatus_t CollatorBufferUtils_c::CopyBufferTillNextStartCode(
    CollatorBufferDesc_s *PrevBuf,
    CollatorBufferDesc_s *CurBuf,
    CollatorBufferDesc_s *DestBuffer,
    unsigned int *NumBytesCopied,
    unsigned char *StreamId,
    bool *Completed)

{
    unsigned int TotalLength = PrevBuf->Len + CurBuf->Len;
    *Completed = false;

    SE_ASSERT(PrevBuf);
    SE_ASSERT(CurBuf);

    SE_ASSERT(TotalLength >= CurBuf->Len);

    // If TotalLength is 3 or less then no hope to find any SID, however still need
    // to update Previous buffer
    if (TotalLength < 4)
    {
        SE_DEBUG(mGroupTrace, "Not enough data to process\n");
        for (unsigned int i = 0; i < CurBuf->Len; i++)
        {
            PrevBuf->Payload[PrevBuf->Len + i] = CurBuf->Payload[i];
        }

        // Processing complete length, but no copy done
        *NumBytesCopied = 0;
        PrevBuf->Len += CurBuf->Len;
        CurBuf->Len = 0;
        return CollatorNoError;
    }

    int StartCodeOffset = 0;
    CollatorStatus_t Status = FindNextStartId(PrevBuf, CurBuf, &StartCodeOffset, StreamId, Completed, 0);
    if (Status != CollatorNoError)
    {
        SE_ERROR("FindNextStartId failed\n");
        return Status;
    }

    unsigned int BytesToCopy;
    unsigned int PrevBufNewLen;
    if (!(*Completed))
    {
        //  Don't copy min of 3 or remaining bytes, as need to wait for next frame to process it
        BytesToCopy = TotalLength - 3;
        // Previous frame needs shifting <<
        // Suppose Previous :  x y z, New : a b c
        // Total [ x y z a b c]
        // New length of previous frame = TotalLength - BytesToCopy
        PrevBufNewLen = 3;
    }
    else
    {
        // Found, so copy till StartCodeOffset
        BytesToCopy = StartCodeOffset;
        // Exhausted complete buffer, so put new length = 0
        PrevBufNewLen = (PrevBuf->Len > BytesToCopy) ? (PrevBuf->Len - BytesToCopy) : 0;
    }

    // Didn't find SID, so 2 actions
    // 1. Copy Total Buffer Length - 3 bytes (StartCodeOffset is no use to us in this case as it will be 0)
    // 2. Need to update Previous buffer (for the last 3 remaining bytes)
    int PrevBufBytesToCopy = Min(PrevBuf->Len, BytesToCopy);
    int CurBufBytesToCopy = BytesToCopy - PrevBufBytesToCopy;

    // Copy to Destination Buffer if any
    // If not, it's equivalent to discarding buffers
    if (DestBuffer)
    {
        if ((DestBuffer->Len + BytesToCopy) > DestBuffer->MaxSize)
        {
            SE_ERROR("Buffer Overflow, trying to copy : %u\n", BytesToCopy);
            return CollatorBufferOverflow;
        }

        memcpy((DestBuffer->Payload + DestBuffer->Len), PrevBuf->Payload, PrevBufBytesToCopy);
        memcpy((DestBuffer->Payload + DestBuffer->Len) + PrevBufBytesToCopy, CurBuf->Payload, CurBufBytesToCopy);

        // Update Accumulated buffer length
        DestBuffer->Len += BytesToCopy;
    }

    // Update Previous Buffer
    for (unsigned int i = 0; i < PrevBufNewLen; i++)
    {
        if ((i + BytesToCopy) < PrevBuf->Len)
        {
            PrevBuf->Payload[i] = PrevBuf->Payload[i + BytesToCopy];
        }
        else
        {
            PrevBuf->Payload[i] = CurBuf->Payload[i + BytesToCopy - PrevBuf->Len];
        }
    }

    PrevBuf->Len = PrevBufNewLen;
    if (!(*Completed))
    {
        CurBuf->Len = 0;
    }
    else
    {
        // Update New buffer
        CurBuf->Payload += CurBufBytesToCopy;

        SE_ASSERT(CurBuf->Len >= CurBufBytesToCopy);
        CurBuf->Len -= CurBufBytesToCopy;
    }

    *NumBytesCopied = BytesToCopy;
    return CollatorNoError;
}

////////////////////////////////////////////////////////////////////////////
/// \fn CollatorStatus_t CollatorBufferUtils_c::CopyBufferByLength(
///                                             CollatorBufferDesc_s &PrevBuf,
///                                             CollatorBufferDesc_s &CurBuf,
///                                             unsigned int DesiredLength,
///                                             unsigned char *DestBuffer,
///                                             unsigned int &BytesToCopy,
///                                             bool &Completed)
///
///  Copy specific number of bytes (Desired length) to Destination buffer
///
///  PrevBuf (in/out) : Point to Previous Input buffer (containing trailing
///         bytes), this function also updates Previous buffer depending
///         on needs e.g. if new bytes needs to be saved / previous
///         buffer has been used
///  CurBuf (in/out) : Point to Current Input buffer, this function updates
///        current buffer length and also updates current buffer position
///  Desired Length (in) : Number of bytes caller wants to copy
///  DestBuffer (in) : points to Output buffer (Could be collated buffer
///        or header)
///  NumBytesCopied (out) : returns Bytes copied by the function
///  Completed flag (out) : is set to true if Desired length was copied,
///        else false
///
/// Note : Caller must keep calling this function by its State machine across
///        multiple ES packets till Completed flag is not true
///
/// \return Collator status code, CollatorNoError indicates success.
///
CollatorStatus_t    CollatorBufferUtils_c::CopyBufferByLength(
    CollatorBufferDesc_s *PrevBuf,
    CollatorBufferDesc_s *CurBuf,
    unsigned int DesiredLength,
    CollatorBufferDesc_s *DestBuffer,
    unsigned int *NumBytesCopied,
    bool *Completed)
{
    unsigned int TotalLength = PrevBuf->Len + CurBuf->Len;

    SE_ASSERT(PrevBuf);
    SE_ASSERT(CurBuf);

    if (DesiredLength > TotalLength)
    {
        //  Not feasible to copy complete length
        *Completed = false;
        *NumBytesCopied = TotalLength;
    }
    else
    {
        *NumBytesCopied = DesiredLength;
        *Completed = true;
    }

    unsigned int PrevBufBytesToCopy = Min(PrevBuf->Len, *NumBytesCopied);
    unsigned int CurBufBytesToCopy = *NumBytesCopied - PrevBufBytesToCopy;

    if (DestBuffer)
    {
        if ((DestBuffer->Len + *NumBytesCopied) > DestBuffer->MaxSize)
        {
            SE_ERROR("Buffer Overflow, trying to copy : %u\n", *NumBytesCopied);
            return CollatorBufferOverflow;
        }
        SE_ASSERT(DestBuffer->Payload);
        SE_ASSERT(PrevBuf->Payload);
        SE_ASSERT(CurBuf->Payload);

        memcpy((DestBuffer->Payload + DestBuffer->Len), PrevBuf->Payload, PrevBufBytesToCopy);
        memcpy((DestBuffer->Payload + DestBuffer->Len) + PrevBufBytesToCopy, CurBuf->Payload, CurBufBytesToCopy);

        // Update Accumulated buffer length
        DestBuffer->Len += *NumBytesCopied;
    }

    // Update Previous Buffer
    PrevBuf->Len = Min(0U, (PrevBuf->Len - *NumBytesCopied));

    // Update Current Buffer
    SE_ASSERT(CurBuf->Len >= CurBufBytesToCopy);
    CurBuf->Payload += CurBufBytesToCopy;
    CurBuf->Len -= CurBufBytesToCopy;

    return CollatorNoError;
}

////////////////////////////////////////////////////////////////////////////
/// Utility Functions <End>
////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////
/// CollatorBufferUtils_c Functions <Start>
////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////
/// \fn CollatorBufferUtils_c::CollatorBufferUtils_c
///
///  Constructor
///
///
CollatorBufferUtils_c::CollatorBufferUtils_c(int GroupTrace)
    : mGroupTrace(GroupTrace)
    , mBuf()
    , mRemainingLength()
{
}

////////////////////////////////////////////////////////////////////////////
/// \fn unsigned int CollatorBufferUtils_c::GetBuffer()
///
///  Getter for Buffer pointer managed by CollatorBufferUtils_c object
///
/// \return Buffer pointer (Pointing to start of Collated Buffer)
///
unsigned char *CollatorBufferUtils_c::GetBuffer()
{
    return mBuf.Payload;
}

////////////////////////////////////////////////////////////////////////////
/// \fn unsigned int CollatorBufferUtils_c::SetBuffer(CollatorBufferDesc_s Buf,
///                                                 unsigned int MaxSize)
///
///  Setter function for buffer managed by CollatorBufferUtils_c object
///
///  Buf (in) : Input Buffer (contains data pointer / length)
///  MaxSize (in) : Maximum allowed limit for accumulation
///
/// \return void
///

void CollatorBufferUtils_c::SetBuffer(CollatorBufferDesc_s Buf,  unsigned int MaxSize)
{
    mBuf.Payload = Buf.Payload;
    mBuf.Len = Buf.Len;
    mBuf.MaxSize = MaxSize;
}

////////////////////////////////////////////////////////////////////////////
/// \fn unsigned int CollatorBufferUtils_c::GetBufferFilledLen()
///
///  Getter for Length filled so far for CollatorBufferUtils_c object
///
/// \return Length of filled buffer
///

unsigned int CollatorBufferUtils_c::GetBufferFilledLen()
{
    return mBuf.Len;
};

////////////////////////////////////////////////////////////////////////////
/// \fn void CollatorBufferUtils_c::ResetBufferFlags()
///
///  Reset all internals associated with CollatorBufferUtils_c object
///  Normally called when restarting collation
///
/// \return void
///
void CollatorBufferUtils_c::ResetBufferFlags()
{
    mBuf.Len = 0;
    mRemainingLength = 0;
};

////////////////////////////////////////////////////////////////////////////
/// \fn CollatorStatus_t CollatorBufferUtils_c::AccumulateAbsoluteLength(
///                                             CollatorBufferDesc_s &PrevBuf,
///                                             CollatorBufferDesc_s &CurBuf,
///                                             unsigned int DesiredLen,
///                                             unsigned int &NumBytesCopied,
///                                             bool &Completed)
///
///  Copy specific number of bytes (Desired length) to CollatorBuffer data
///  Wrapper over CopyBufferByLength
///
///  PrevBuf (in/out) : Point to Previous Input buffer (containing trailing
///         bytes), this function also updates Previous buffer depending
///         on needs e.g. if new bytes needs to be saved / previous
///         buffer has been used
///  CurBuf (in/out) : Point to Current Input buffer, this function updates
///        current buffer length and also updates current buffer position
///  Desired Length (in) : Number of bytes caller wants to copy
///  NumBytesCopied (out) : returns Bytes copied by the function
///  Completed flag (out) : is set to true if Desired length was copied,
///        else false
///
/// Note : Caller must keep calling this function by its State machine across
///        multiple ES packets till Completed flag is not true
///
/// \return Collator status code, CollatorNoError indicates success.
///
CollatorStatus_t CollatorBufferUtils_c::AccumulateAbsoluteLength(
    CollatorBufferDesc_s *PrevBuf,
    CollatorBufferDesc_s *CurBuf,
    unsigned int DesiredLen,
    unsigned int *NumBytesCopied,
    bool *Completed)
{
    if (mBuf.Len + DesiredLen > mBuf.MaxSize)
    {
        SE_ERROR("Overshooting Max length : %u\n", mBuf.MaxSize);
        return CollatorBufferOverflow;
    }

    CollatorStatus_t Status = CopyBufferByLength(PrevBuf, CurBuf, DesiredLen,
                                                 &mBuf, NumBytesCopied, Completed);
    return Status;
}

////////////////////////////////////////////////////////////////////////////
/// \fn CollatorStatus_t CollatorBufferUtils_c::DiscardAbsoluteLength(
///                                             CollatorBufferDesc_s &PrevBuf,
///                                             CollatorBufferDesc_s &CurBuf,
///                                             unsigned int Offset,
///                                             unsigned int DesiredLen,
///                                             unsigned int &NumBytesCopied,
///                                             bool &Completed)
///
///  Discard specific number of bytes (Desired length) from PrevBuf and CurBuf
///
///  PrevBuf (in/out) : Point to Previous Input buffer (containing trailing
///         bytes), this function also updates Previous buffer depending
///         on needs e.g. if new bytes needs to be saved / previous
///         buffer has been used
///  CurBuf (in/out) : Point to Current Input buffer, this function updates
///        current buffer length and also updates current buffer position
///  Offset(in)  : Offset/Location from where bytes to be discarded.
///  Desired Length (in) : Number of bytes caller wants to copy
///  NumBytesDiscarded (out) : returns Bytes disacded by the function
///  Completed flag (out) : is set to true if Desired length was discarded,
///        else false
///
/// \return Collator status code, CollatorNoError indicates success.
///
CollatorStatus_t CollatorBufferUtils_c::DiscardAbsoluteLength(
    CollatorBufferDesc_s *PrevBuf,
    CollatorBufferDesc_s *CurBuf,
    unsigned int Offset,
    unsigned int DesiredLen,
    unsigned int *NumBytesDiscarded,
    bool *Completed)
{
    if (Offset > (PrevBuf->Len + CurBuf->Len))
    {
        SE_ERROR("Overshooting Offset length : %u\n", Offset);
        return CollatorBufferOverflow;
    }

    if (Offset > PrevBuf->Len)
    {
        //bytes to be discarded only from CurBuf
        unsigned int CurBufBytesToBeDiscard = Offset - PrevBuf->Len;
        CurBuf->Payload += CurBufBytesToBeDiscard;
        CurBuf->Len -= CurBufBytesToBeDiscard;
        *Completed = false;
        *NumBytesDiscarded = 0;
        return CollatorNoError;
    }

    unsigned int PrevBufBytesToBeDiscard = PrevBuf->Len - Offset;
    unsigned int CurBufBytesToBeDiscard = DesiredLen - PrevBufBytesToBeDiscard ;
    PrevBuf->Len -= PrevBufBytesToBeDiscard;
    CurBuf->Len  -= CurBufBytesToBeDiscard;
    CurBuf->Payload += CurBufBytesToBeDiscard;
    *NumBytesDiscarded = DesiredLen;
    *Completed = true;
    return CollatorNoError;
}

////////////////////////////////////////////////////////////////////////////
/// \fn CollatorStatus_t CollatorBufferUtils_c::AccumulateTillStartCode(
///                                           CollatorBufferDesc_s &PrevBuf,
///                                           CollatorBufferDesc_s &CurBuf,
///                                           unsigned char *DestBuffer,
///                                           unsigned int &NumBytesCopied,
///                                           unsigned char &StreamId,
///                                           bool &Completed)
///
///  Copy Buffer Till Beginning of Next Start Code (001 SID)
///  Wrapper over CopyBufferTillNextStartCode
///
///  Updates last 3 bytes in Previous buffer in case Start code is not found
///          till end of buffer
///
///  PrevBuf (in/out) : Point to Previous Input buffer (containing trailing
///         bytes), this function also updates Previous buffer depending
///         on needs e.g. if new bytes needs to be saved / previous
///         buffer has been used
///  CurBuf (in/out) : Point to Current Input buffer, this function updates
///        current buffer length and also updates current buffer position
///  NumBytesCopies (out) : returns Bytes copied by the function
///  Stream Id (out) : contains SID of Sequence 001<SID>
///  Completed flag (out) : is set to true if Start code was found, else false
///
/// Note : Caller must keep calling this function by its State machine across
///        multiple ES packets till Completed flag is not true
///
/// \return Collator status code, CollatorNoError indicates success.
///
CollatorStatus_t CollatorBufferUtils_c::AccumulateTillStartCode(
    CollatorBufferDesc_s *PrevBuf,
    CollatorBufferDesc_s *CurBuf,
    unsigned int *NumBytesCopied,
    unsigned char *StreamId,
    bool *Completed)
{
    return CopyBufferTillNextStartCode(PrevBuf, CurBuf,  &mBuf,
                                       NumBytesCopied, StreamId, Completed);
}

////////////////////////////////////////////////////////////////////////////
/// \fn CollatorStatus_t CollatorBufferUtils_c::DiscardTillStartCode(
///                                           CollatorBufferDesc_s &PrevBuf,
///                                           CollatorBufferDesc_s &CurBuf,
///                                           unsigned char *DestBuffer,
///                                           unsigned int &NumBytesCopied,
///                                           unsigned char &StreamId,
///                                           bool &Completed)
///
///  Discard Buffer Till Beginning of Next Start Code (001 SID)
///     Wrapper over CopyBufferTillNextStartCode (with NULL destination)
///
///  Updates last 3 bytes in Previous buffer in case Start code is not found
///          till end of buffer
///
///  PrevBuf (in/out) : Point to Previous Input buffer (containing trailing
///         bytes), this function also updates Previous buffer depending
///         on needs e.g. if new bytes needs to be saved / previous
///         buffer has been used
///  CurBuf (in/out) : Point to Current Input buffer, this function updates
///        current buffer length and also updates current buffer position
///  NumBytesCopies (out) : returns Bytes discarded by the function
///  Stream Id (out) : contains SID of Sequence 001<SID>
///  Completed flag (out) : is set to true if Start code was found, else false
///
/// Note : Caller must keep calling this function by its State machine across
///        multiple ES packets till Completed flag is not true
///
/// \return Collator status code, CollatorNoError indicates success.
///
CollatorStatus_t CollatorBufferUtils_c::DiscardTillStartCode(
    CollatorBufferDesc_s *PrevBuf,
    CollatorBufferDesc_s *CurBuf,
    unsigned int *NumBytesCopied,
    unsigned char *StreamId,
    bool *Completed)
{
    return CopyBufferTillNextStartCode(PrevBuf, CurBuf,
                                       NULL, NumBytesCopied, StreamId, Completed);
}

////////////////////////////////////////////////////////////////////////////
/// \fn CollatorStatus_t CollatorBufferUtils_c::AccumulateRemainingLength(
///                                             CollatorBufferDesc_s &PrevBuf,
///                                             CollatorBufferDesc_s &CurBuf,
///                                             unsigned int &NumBytesCopied,
///                                             bool &Completed)
///
///  Copy specific number of bytes (Desired length, already set by
///  SetRemainingLength function) to CollatorBuffer data
///
///  Another Wrapper over CopyBufferByLength
///
///  This Remaining Length is kept in another function intentionally
///  : This function is designed to be called in state machine and
///  so desired length can't be in arguments (else function will not
///  behave as expected)
///  Keeping remaining bytes in separate function, this function decrease
///  remaining length by NumBytesCopied till it doesn't reach zero
///
///  PrevBuf (in/out) : Point to Previous Input buffer (containing trailing
///         bytes), this function also updates Previous buffer depending
///         on needs e.g. if new bytes needs to be saved / previous
///         buffer has been used
///  CurBuf (in/out) : Point to Current Input buffer, this function updates
///        current buffer length and also updates current buffer position
///  NumBytesCopied (out) : returns Bytes copied by the function
///  Completed flag (out) : is set to true if Desired length was copied,
///        else false
///
/// Note : Caller must keep calling this function by its State machine across
///        multiple ES packets till Completed flag is not true
/// Note : This function must be preceded by SetRemainingLength function
///        which is needed to set Desired length
///
/// \return Collator status code, CollatorNoError indicates success.
///
CollatorStatus_t CollatorBufferUtils_c::AccumulateRemainingLength(
    CollatorBufferDesc_s *PrevBuf,
    CollatorBufferDesc_s *CurBuf,
    unsigned int *NumBytesCopied,
    bool *Completed)
{
    CollatorStatus_t Status = AccumulateAbsoluteLength(PrevBuf, CurBuf,
                                                       mRemainingLength, NumBytesCopied,
                                                       Completed);
    mRemainingLength -= *NumBytesCopied;

    return Status;
}

////////////////////////////////////////////////////////////////////////////
/// \fn CollatorStatus_t CollatorBufferUtils_c::SetRemainingLength (
///                                                 unsigned int DesiredLen)
///
///  Set Remaining Length for "AccumulateRemainingLength" function
///
///  DesiredLen (in) : Desired length to be copied
///
/// \return Collator status code, CollatorNoError indicates success.
///
CollatorStatus_t CollatorBufferUtils_c::SetRemainingLength(
    unsigned int DesiredLen)
{
    if (mBuf.Len + DesiredLen > mBuf.MaxSize)
    {
        SE_ERROR("Overshooting Max length : %u\n", mBuf.MaxSize);
        return CollatorBufferOverflow;
    }

    mRemainingLength += DesiredLen;

    return CollatorNoError;
};

////////////////////////////////////////////////////////////////////////////
/// \fn void CollatorBufferUtils_c::DumpAccumulatedFrame()
///
///  Debug function / To dump accumulated frame
///
/// \return void
///
void CollatorBufferUtils_c::DumpAccumulatedFrame()
{
    // Check for Verbose traces, otherwise we will be wasting
    // un-necessary CPU cycles
    if (SE_IS_VERBOSE_ON(mGroupTrace))
    {
        if (mBuf.Len > 0)
        {
            SE_VERBOSE(mGroupTrace, "Dumping Accumulated Buffer, Length: %u\n", mBuf.Len);
            for (unsigned int i = 0; i < mBuf.Len; i++)
            {
                SE_VERBOSE(mGroupTrace, "0x%x\n", mBuf.Payload[i]);
            }
            SE_VERBOSE(mGroupTrace, "\n\n");
        }
    }
}

////////////////////////////////////////////////////////////////////////////
/// CollatorBufferUtils_c Functions <End>
////////////////////////////////////////////////////////////////////////////
