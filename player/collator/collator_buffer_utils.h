/************************************************************************
Copyright (C) 2003-2014 STMicroelectronics. All Rights Reserved.

This file is part of the Streaming Engine.

Streaming Engine is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License version 2 as
published by the Free Software Foundation.

Streaming Engine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with Streaming Engine; see the file COPYING.  If not, write to the Free
Software Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307,
USA.

The Streaming Engine Library may alternatively be licensed under a
proprietary license from ST.
************************************************************************/
#ifndef COLLATOR_BUFFER_UTILS_H_
#define COLLATOR_BUFFER_UTILS_H_

#include "report.h"
#include "collator.h"

/// Basic Buffer structure to track buffer pointer , its filled length and
/// maximum allowed size to be filled.
struct CollatorBufferDesc_s
{
    unsigned char *Payload;
    unsigned int   Len;
    unsigned int   MaxSize;
};

////////////////////////////////////////////////////////////////////////////
/// \class CollatorBufferUtils_c
///
/// Collator Buffer utility Class
///
/// Provides Utility functions related to frame parsing across multiple
///  input packets
///
///
class CollatorBufferUtils_c
{
public:
    explicit CollatorBufferUtils_c(int GroupTrace);
    ~CollatorBufferUtils_c() {}

    // Accumulate absolute length should be used, when it's sure that buffer will not span to next packet
    CollatorStatus_t AccumulateAbsoluteLength(CollatorBufferDesc_s *PrevBuf,
                                              CollatorBufferDesc_s *CurBuf,
                                              unsigned int DesiredLen,
                                              unsigned int *NumBytesCopied,
                                              bool *Completed);
    // Discard buffer by length
    CollatorStatus_t DiscardAbsoluteLength(CollatorBufferDesc_s *PrevBuf,
                                           CollatorBufferDesc_s *CurBuf,
                                           unsigned int Offset,
                                           unsigned int DesiredLen,
                                           unsigned int *NumBytesDiscarded,
                                           bool *Completed);

    // Accumulate Start code should be called repeatedly by top level state machine, till Completed
    // flag is not true. Note that after this function, previous buffer must be updated
    // using UpdatePreviousBuffer function
    CollatorStatus_t AccumulateTillStartCode(CollatorBufferDesc_s *PrevBuf,
                                             CollatorBufferDesc_s *CurBuf,
                                             unsigned int *NumBytesCopied,
                                             unsigned char *StreamId,
                                             bool *Completed);

    // Move pointers, but Discard frame
    CollatorStatus_t DiscardTillStartCode(CollatorBufferDesc_s *PrevBuf,
                                          CollatorBufferDesc_s *CurBuf,
                                          unsigned int *NumBytesCopied,
                                          unsigned char *StreamId,
                                          bool *Completed);

    // Accumulate Remaining Length should be used, when remaining length could be spread
    // across multiple frames. In such case, this function will be called repeatedly
    // Till Completed is not true. Also before calling this function, User must call
    // SetRemainingLength with correct size
    CollatorStatus_t AccumulateRemainingLength(CollatorBufferDesc_s *PrevBuf,
                                               CollatorBufferDesc_s *CurBuf,
                                               unsigned int *NumBytesCopied,
                                               bool *Completed);

    CollatorStatus_t SetRemainingLength(unsigned int Size);

    // Getters / Setters
    unsigned char *GetBuffer();
    void SetBuffer(CollatorBufferDesc_s Buf,  unsigned int MaxSize);
    unsigned int GetBufferFilledLen();
    // Reset buffer flags after each buffer is complete
    void ResetBufferFlags();
    // For testing / debugging
    void DumpAccumulatedFrame();

private:
    int                     mGroupTrace;
    CollatorBufferDesc_s    mBuf;       // Track Buffer object manages
    // Tracks how much remaining length to be copied (used by AccumulateRemainingLength)
    unsigned int            mRemainingLength;

    // Find Next Start Code : Can find Start Code 001<SID> across 2 frames
    CollatorStatus_t FindNextStartId(CollatorBufferDesc_s *PrevBuf,
                                     CollatorBufferDesc_s *CurBuf,
                                     int *StartCodeOffset,
                                     unsigned char *StreamId,
                                     bool *Completed,
                                     bool SearchId);

    // Copy PrevBuf and CurBuf buffers into DestBuffer Buffer by Length(BytesToCopy)
    CollatorStatus_t CopyBufferByLength(CollatorBufferDesc_s *PrevBuf,
                                        CollatorBufferDesc_s *CurBuf,
                                        unsigned int DesiredLength,
                                        CollatorBufferDesc_s *DestBuffer,
                                        unsigned int *BytesToCopy,
                                        bool *Completed);
    // Copy PrevBuf and CurBuf Buffers to destination buffer until next start Code is found
    CollatorStatus_t CopyBufferTillNextStartCode(CollatorBufferDesc_s *PrevBuf,
                                                 CollatorBufferDesc_s *CurBuf,
                                                 CollatorBufferDesc_s *DestBuffer,
                                                 unsigned int *NumBytesCopied,
                                                 unsigned char *StreamId,
                                                 bool *Completed);
    DISALLOW_COPY_AND_ASSIGN(CollatorBufferUtils_c);
};

#endif //COLLATOR_BUFFER_UTILS_H_
