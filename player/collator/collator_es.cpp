/************************************************************************
Copyright (C) 2003-2014 STMicroelectronics. All Rights Reserved.

This file is part of the Streaming Engine.

Streaming Engine is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License version 2 as
published by the Free Software Foundation.

Streaming Engine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with Streaming Engine; see the file COPYING.  If not, write to the Free
Software Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307,
USA.

The Streaming Engine Library may alternatively be licensed under a
proprietary license from ST.
************************************************************************/

#include "collator_es.h"

////////////////////////////////////////////////////////////////////////////
///
/// Default constructor
///
Collator_Es_c::Collator_Es_c(int GroupTrace)
    : mCollatedBuf(GroupTrace)
    , mPreviousBuffer()
    , mPlaybackTimeValid(false)
    , mDecodeTimeValid(false)
    , mPlaybackTime(INVALID_TIME)
    , mDecodeTime(INVALID_TIME)
    , mPartialHeader()
{
    // Set Previous ES Frame (will contain trailing bytes from last ES frame)
    mPreviousBuffer.Payload = mPartialHeader;
    mPreviousBuffer.MaxSize = MAX_PREV_BUFFER_SIZE;
    SetGroupTrace(GroupTrace);
}

////////////////////////////////////////////////////////////////////////////
///
/// Default Destructor
///
Collator_Es_c::~Collator_Es_c()
{
    Halt();
}

////////////////////////////////////////////////////////////////////////////
///
/// Set Sink for ES Collator
/// Sink will ensure that Collated buffer is set properly
///
/// This function relies on functionality provided by Collator Base
/// Addition is to map internal buffer with Collator Buffer coming from Sink
///
/// Sink (in) : Sink Interface to which Collator is attached
/// MaximumCodedFrameSize (in) : Maximum allowed codded frame size
///
/// \return Collator status code, CollatorNoError indicates success.
///
CollatorStatus_t  Collator_Es_c::SetSink(CollatorSinkInterface_c *Sink,
                                         unsigned int MaximumCodedFrameSize)
{
    CollatorStatus_t Status = Collator_Base_c::SetSink(Sink,
                                                       MaximumCodedFrameSize);

    if (Status != CollatorNoError)
    {
        SE_ERROR("No collated buffer\n");
        return Status;
    }

    InitCollatedBuffer();
    return Status;
}

////////////////////////////////////////////////////////////////////////////
///
/// Init collated Buffer to Buffer base from codedbuffer pool
///
/// \return void
///
void Collator_Es_c::InitCollatedBuffer()
{
    CollatorBufferDesc_s CollatedBuffer = {BufferBase, 0, MaximumCodedFrameSize};
    mCollatedBuf.SetBuffer(CollatedBuffer, MaximumCodedFrameSize);
}

////////////////////////////////////////////////////////////////////////////
///
///  The Halt function, give up access to any registered resources
///
/// \return Collator status code, CollatorNoError indicates success.
///
CollatorStatus_t   Collator_Es_c::Halt()
{
    DiscardAccumulatedData();
    // Discard previous buffer
    ResetPreviousBuffer();

    return Collator_Base_c::Halt();
}

////////////////////////////////////////////////////////////////////////////
///
///  Flush buffer to outside world
///
/// \return Collator status code, CollatorNoError indicates success.
///
CollatorStatus_t   Collator_Es_c::InternalFrameFlush()
{
    CodedFrameParameters.PlaybackTimeValid = false;
    CodedFrameParameters.DecodeTimeValid   = false;
    CodedFrameParameters.SourceTimeFormat  = TIME_FORMAT_27MHz;

    if (mPlaybackTimeValid == true)
    {
        CodedFrameParameters.PlaybackTime       = mPlaybackTime;
        CodedFrameParameters.PlaybackTimeValid  = true;
    }

    if (mDecodeTimeValid == true)
    {
        CodedFrameParameters.DecodeTime      = mDecodeTime;
        CodedFrameParameters.DecodeTimeValid = true;
    }

    AccumulatedDataSize = mCollatedBuf.GetBufferFilledLen();
    CollatorStatus_t Status = Collator_Base_c::InternalFrameFlush();

    if (Status != CollatorNoError)
    {
        SE_ERROR("Base Internal frame flush failed\n");
    }

    // Base Internal frame flush would also return
    // new allocation, let's reset our buffer to new BufferBase
    InitCollatedBuffer();
    mPlaybackTimeValid          = false;
    mDecodeTimeValid            = false;

    return Status;
}

////////////////////////////////////////////////////////////////////////////
///
///  Discard accumulated data
///  Called in case of errors / invalid frames / discontinuities
///
/// \return Collator status code, CollatorNoError indicates success.
///
CollatorStatus_t   Collator_Es_c::DiscardAccumulatedData()
{
    mPlaybackTimeValid          = false;
    mDecodeTimeValid            = false;
    return Collator_Base_c::DiscardAccumulatedData();
}

////////////////////////////////////////////////////////////////////////////
///
///  Handle a Jump / discontinuity
///
CollatorStatus_t   Collator_Es_c::InputJump(int discontinuity)
{
    CollatorStatus_t Status = Collator_Base_c::InputJump(discontinuity);
    if (Status != CollatorNoError)
    {
        return Status;
    }

    ResetAllVariables();
    mPlaybackTimeValid           = false;
    mDecodeTimeValid             = false;
    return Status;
}

////////////////////////////////////////////////////////////////////////////
///
///  Reset All common buffers
///  Called after every InternalFrameFlush / DiscardAccumulatedData
///
/// \return void
///
void Collator_Es_c::ResetCommonBuffers()
{
    mCollatedBuf.ResetBufferFlags();
}

////////////////////////////////////////////////////////////////////////////
///
///  Reset Previous Buffers - mPreviousBuffer & mPreviousMarkerBuffer
///
/// \return void
///
void Collator_Es_c::ResetPreviousBuffer()
{
    mPreviousBuffer.Len = 0;
}
