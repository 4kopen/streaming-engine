/************************************************************************
Copyright (C) 2003-2014 STMicroelectronics. All Rights Reserved.

This file is part of the Streaming Engine.

Streaming Engine is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License version 2 as
published by the Free Software Foundation.

Streaming Engine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with Streaming Engine; see the file COPYING.  If not, write to the Free
Software Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307,
USA.

The Streaming Engine Library may alternatively be licensed under a
proprietary license from ST.
************************************************************************/

#ifndef COLLATOR_ES_H_
#define COLLATOR_ES_H_

#include "pes.h"
#include "bitstream_class.h"
#include "collator_base.h"
#include "collator_buffer_utils.h"

#undef  TRACE_TAG
#define TRACE_TAG "Collator_Es_c"

#define MARKER_FRAME_SIZE       26
#define MAX_PREV_BUFFER_SIZE    6
#define START_CODE_LENGTH       4

enum CollatorMarkerFrameState
{
    MarkerFrameSearchForStartCode,
    MarkerFrameSearchForHeader
};

////////////////////////////////////////////////////////////////////////////
/// \fn const char * StringifyMarkerFrameState(CollatorMarkerFrameState State)
///
/// Convert State to printable string
///
/// State (in) : Input Marker State
///
/// \return Marker State String
///
static inline const char *StringifyMarkerFrameState(
    CollatorMarkerFrameState State)
{
    switch (State)
    {
    case MarkerFrameSearchForStartCode :
        return "MarkerFrameSearchForStartCode";
    case MarkerFrameSearchForHeader :
        return "MarkerFrameSearchForHeader";
    default:
        return "MarkerFrameUnknownState";
    }
};

////////////////////////////////////////////////////////////////////////////
/// \class Collator_Es_c
///
/// Base Class for Elementary Stream Collators
///
/// This class provides a framework on which to base ES frame
/// analysis. Basically this class provides most of the machinary to
/// manage buffers but leaves the sub-class to implement
/// Collator_Es_c::InputSecondStage
/// This class also provides handling for marker frames.
///
///
class Collator_Es_c : public Collator_Base_c
{
public:
    explicit Collator_Es_c(int GroupTrace);
    virtual ~Collator_Es_c();

    CollatorStatus_t   Halt();

    CollatorStatus_t   DiscardAccumulatedData();

    CollatorStatus_t   InputJump(int discontinuity);

    CollatorStatus_t   SetSink(CollatorSinkInterface_c *sink, unsigned int MaximumCodedFrameSize);

    CollatorStatus_t   InternalFrameFlush();

protected:
    // ES Variables : Common for all ES
    // Actual buffer for mCollatedBuf descriptor should be provided by derived
    // class using SetBuffer API
    CollatorBufferUtils_c   mCollatedBuf;
    // Previous buffer descriptor to hold 3 trailing bytes.
    CollatorBufferDesc_s    mPreviousBuffer;

    bool                    mPlaybackTimeValid;
    bool                    mDecodeTimeValid;
    unsigned long long      mPlaybackTime;
    unsigned long long      mDecodeTime;

    virtual void ResetCommonBuffers();
    virtual void ResetPreviousBuffer();

    virtual void ResetAllVariables() = 0;

private:
    unsigned char mPartialHeader[MAX_PREV_BUFFER_SIZE];

    void InitCollatedBuffer();

    DISALLOW_COPY_AND_ASSIGN(Collator_Es_c);
};

#endif //COLLATOR_ES_H_

