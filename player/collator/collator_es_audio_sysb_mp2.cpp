/************************************************************************
Copyright (C) 2003-2014 STMicroelectronics. All Rights Reserved.

This file is part of the Streaming Engine.

Streaming Engine is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License version 2 as
published by the Free Software Foundation.

Streaming Engine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with Streaming Engine; see the file COPYING.  If not, write to the Free
Software Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307,
USA.

The Streaming Engine Library may alternatively be licensed under a
proprietary license from ST.
************************************************************************/
#include "collator_es_audio_sysb_mp2.h"

////////////////////////////////////////////////////////////////////////////
/// \fn Collator_Es_Audio_Sysb_Mp2_c::Collator_Es_Audio_Sysb_Mp2_c()
///
/// Default constructor
///
Collator_Es_Audio_Sysb_Mp2_c::Collator_Es_Audio_Sysb_Mp2_c() :
    Collator_Es_c(group_collator_audio),
    mHeader(group_collator_audio),
    mPadLength(0),
    mPayloadLength(0),
    mMpeg1SystemHeader(),
    mState(AudioSearchForStartCode)
{
    Configuration.IgnoreCodesRanges.NbEntries = 1;
    Configuration.IgnoreCodesRanges.Table[0].Start = 0x01; // All slice codes
    Configuration.IgnoreCodesRanges.Table[0].End = PES_START_CODE_AUDIO - 1;

    Configuration.InsertFrameTerminateCode          = false;
    Configuration.ExtendedHeaderLength              = 0; // Not needed

    CollatorBufferDesc_s OpHeader = {mMpeg1SystemHeader, 0, 0};
    mHeader.SetBuffer(OpHeader, MAX_MPEG1_SYSTEM_HEADER_LEN);
    mHeader.ResetBufferFlags();
}

////////////////////////////////////////////////////////////////////////////
/// \fn Collator_Es_Audio_Sysb_Mp2_c::~Collator_Es_Audio_Sysb_Mp2_c()
///
/// Default destructor
///
Collator_Es_Audio_Sysb_Mp2_c::~Collator_Es_Audio_Sysb_Mp2_c()
{
}

////////////////////////////////////////////////////////////////////////////
/// \fn Collator_Es_Audio_Sysb_Mp2_c::ChangeState (CollatorEsAudioState State)
///
///  Function to set new Audio SYSB MP2 state
///
///  State (in) : New State
/// \return void
///
void Collator_Es_Audio_Sysb_Mp2_c::ChangeState(CollatorEsAudioState State)
{
    SE_DEBUG(group_collator_audio, "Moving to State : %s\n",
             StringifyState(State));
    mState = State;
}

////////////////////////////////////////////////////////////////////////////
/// \fn Collator_Es_Audio_Sysb_Mp2_c::Halt ()
///
///  Function to halt Audio
///
/// \return CollatorStatus_t
///
CollatorStatus_t Collator_Es_Audio_Sysb_Mp2_c::Halt()
{
    SE_DEBUG(group_collator_audio, "Halt\n");
    Collator_Es_c::Halt();
    ResetAllVariables();
    return CollatorNoError;
}

////////////////////////////////////////////////////////////////////////////
/// \fn void Collator_Es_Video_Sysb_Mpeg2_c::ResetAllVariables ()
///
///  Reset All Internal Variables
///  Called after every InternalFrameFlush / DiscardAccumulatedData
///
/// \return void
///
void Collator_Es_Audio_Sysb_Mp2_c::ResetAllVariables()
{
    SE_DEBUG(group_collator_audio, "ResetAllVariables\n");
    Collator_Es_c::ResetCommonBuffers();
    mPayloadLength = 0;
    mPadLength = 0;

    mHeader.ResetBufferFlags();
    mState = AudioSearchForStartCode;
}

////////////////////////////////////////////////////////////////////////////
/// \fn CollatorStatus_t   Collator_Es_Audio_Sysb_Mp2_c::DiscardAccumulatedData()
///
///  Discard accumulated data
///  Called in case of errors / invalid frames / discontinuities
///
/// \return Collator status code, CollatorNoError indicates success.
///
CollatorStatus_t   Collator_Es_Audio_Sysb_Mp2_c::DiscardAccumulatedData()
{
    SE_DEBUG(group_collator_audio, "Discarding Accumulated Data\n");
    CollatorStatus_t Status = Collator_Es_c::DiscardAccumulatedData();
    if (Status != CollatorNoError)
    {
        return Status;
    }

    ResetAllVariables();
    return Status;
}

////////////////////////////////////////////////////////////////////////////
/// \fn unsigned int Collator_Es_Audio_Sysb_Mp2_c::GetPadLength (
///                                 CollatorBufferDesc_s &CurrentBuffer,
///                                 bool &Completed)
///
/// Find out total stuffing bytes available in the provided buffer.
///
/// CurrentBuffer (in) : Current Input buffer Descriptor
/// Completed (out) : true indicates end of padding, false means padding
///                   needs to be checked in next ES packet too
///
///
/// \return Total no of stuffing bytes in the buffer
///
unsigned int Collator_Es_Audio_Sysb_Mp2_c::GetPadLength(
    CollatorBufferDesc_s &CurrentBuffer,
    bool &Completed)
{
    unsigned int PadLength = 0;

    // Discard till pattern 0xFF (stuffing Bytes) is found
    for (unsigned int i = 0 ;
         (i < CurrentBuffer.Len) && (CurrentBuffer.Payload[i] == 0xff) ;
         i++, PadLength++)
    {
        // Skip
    }
    // Complete only if Padding finished before end of current buffer
    // Else we need to confirm padding is present in next frame or not
    Completed = (PadLength < CurrentBuffer.Len);

    CurrentBuffer.Payload += PadLength;
    CurrentBuffer.Len -= PadLength;
    SE_DEBUG(group_collator_audio, "Completed = %d PaddingLength = %u\n", Completed, PadLength);
    return PadLength;
}

////////////////////////////////////////////////////////////////////////////
/// \fn CollatorStatus_t Collator_Es_Audio_Sysb_Mp2_c::InputSecondStage (
///                                         unsigned int BufLen,
///                                         void  *Buffer)
///
/// Handle input buffer coming from Collator ES Base class
///
/// BufLen (in) : Current Input buffer length
/// Buffer (in) : Current Input buffer
///
///
/// \return Collator status code, CollatorNoError indicates success.
///
CollatorStatus_t Collator_Es_Audio_Sysb_Mp2_c::InputSecondStage(
    unsigned int BufLen,
    const void  *Buffer)
{
    CollatorStatus_t Status = CollatorNoError;
    // To keep track of Current ES Packet
    CollatorBufferDesc_s CurrentBuffer = {(unsigned char *)Buffer, BufLen, 0};

    unsigned char StreamId;
    // Position of Start Code in Combined Previous (remaining) +
    // Current ES packet, rewritten everytime
    unsigned int StartCodeOffset;
    // Keep track of how many Bytes are copied, rewritten everytime
    unsigned int NumBytesCopied;
    // To know whether function completed its task or not,
    // If not it will be called again with next ES packet
    bool Completed = false;

    SE_DEBUG(group_collator_audio, "Previous Buffer Length: %u, Current Buffer Length: %u\n",
             mPreviousBuffer.Len,  CurrentBuffer.Len);

    // First search for Start Code
    while ((CurrentBuffer.Len > 0) && (Status == CollatorNoError))
    {
        if (mCollatorSink->PlaybackIsInLowPowerState())
        {
            // Stop processing data to speed-up low power enter procedure (bug 24248)
            break;
        }

        switch (mState)
        {
        case AudioSearchForStartCode:
            Status = mCollatedBuf.DiscardTillStartCode(&mPreviousBuffer,
                                                       &CurrentBuffer,
                                                       &StartCodeOffset,
                                                       &StreamId,
                                                       &Completed);
            if ((!Completed) || (Status != CollatorNoError))
            {
                break;
            }

            SE_DEBUG(group_collator_audio, "Stream Id : 0x%x\n", StreamId);
            if (StreamId == 0xC0)
            {
                SE_DEBUG(group_collator_audio,
                         "Received Initial Audio Header : 0x%x\n", StreamId);
                mHeader.SetRemainingLength(MPEG1_SYSTEM_HEADER_INITIAL_SIZE);
                ChangeState(AudioSearchForInitialHeader);
            }
            else
            {
                // Not a start code, better to move 3 bytes forward (to avoid infinite loop)
                Status = mHeader.DiscardAbsoluteLength(&mPreviousBuffer,
                                                       &CurrentBuffer,
                                                       StartCodeOffset,
                                                       3,
                                                       &NumBytesCopied,
                                                       &Completed);
            }

            break;

        case AudioSearchForInitialHeader:
            Status = mHeader.AccumulateRemainingLength(&mPreviousBuffer,
                                                       &CurrentBuffer,
                                                       &NumBytesCopied,
                                                       &Completed);
            if ((!Completed) || (Status != CollatorNoError))
            {
                break;
            }

            // 5th & 6th Bytes contain Payload Length
            mPayloadLength = (unsigned int)(mHeader.GetBuffer()[4] << 8) +
                             (unsigned int) mHeader.GetBuffer()[5];
            SE_DEBUG(group_collator_audio, "Payload Length : %u\n", mPayloadLength);

            // Now Check for Padding/stuffing bytes
            ChangeState(AudioSearchForStuffingByteEnd);
            break;

        case AudioSearchForStuffingByteEnd:
            // Checking for only 1 byte repetition,
            // so no need to bother about previous frame
            mPadLength += GetPadLength(CurrentBuffer, Completed);

            mPayloadLength -= mPadLength;

            // Discard till pattern is found
            if (!Completed)
            {
                // Entire buffer exhausted
                mPadLength = 0;
                break;
            }

            // Remove Padding from Payload Length
            if ((CurrentBuffer.Payload[0] & 0xf0) == 0x20)
            {
                SE_DEBUG(group_collator_audio,
                         "Got PTS at input buffer offset : %u\n", CurrentBuffer.Len);

                mHeader.SetRemainingLength(MPEG1_SYSTEM_HEADER_PTS_DTS_SIZE);
                mCollatedBuf.SetRemainingLength(
                    mPayloadLength - MPEG1_SYSTEM_HEADER_PTS_DTS_SIZE);

                ChangeState(AudioSearchForHeaderPts);
            }
            else
            {
                mCollatedBuf.SetRemainingLength(mPayloadLength);
                ChangeState(AudioSearchForPayload);
            }

            break;

        case AudioSearchForHeaderPts:
            Status = mHeader.AccumulateRemainingLength(&mPreviousBuffer,
                                                       &CurrentBuffer,
                                                       &NumBytesCopied,
                                                       &Completed);
            if ((!Completed) || (Status != CollatorNoError))
            {
                break;
            }

            Status = ParseAudioMpeg1SystemPacketForPTS(mHeader.GetBuffer() +
                                                       (mHeader.GetBufferFilledLen() -
                                                        MPEG1_SYSTEM_HEADER_PTS_DTS_SIZE));
            ChangeState(AudioSearchForPayload);
            break;

        case AudioSearchForPayload:
            Status = mCollatedBuf.AccumulateRemainingLength(&mPreviousBuffer,
                                                            &CurrentBuffer,
                                                            &NumBytesCopied,
                                                            &Completed);
            if ((!Completed) || (Status != CollatorNoError))
            {
                break;
            }

            // Flush Data
            Status = InternalFrameFlush();
            ResetAllVariables();
            break;

        default:
            // Not expected
            SE_FATAL("Critical Error : State Machine : %d\n", mState);
            break;
        }   // Switch - case
    } // While Loop

    SE_DEBUG(group_collator_audio, "Payload length = %u\n", mPayloadLength);
    if (Status != CollatorNoError)
    {
        SE_ERROR("Discarding accumulated frame: Error in State %d\n", mState);
        DiscardAccumulatedData();
    }

    return Status;
}
