/************************************************************************
Copyright (C) 2003-2014 STMicroelectronics. All Rights Reserved.

This file is part of the Streaming Engine.

Streaming Engine is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License version 2 as
published by the Free Software Foundation.

Streaming Engine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with Streaming Engine; see the file COPYING.  If not, write to the Free
Software Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307,
USA.

The Streaming Engine Library may alternatively be licensed under a
proprietary license from ST.
************************************************************************/
#ifndef COLLATOR_ES_AUDIO_SYSB_MP2_H_
#define COLLATOR_ES_AUDIO_SYSB_MP2_H_

#include "collator_es.h"
#include "collator_es_sysb_parser.h"

////////////////////////////////////////////////////////////////////////////
// State Machine
////////////////////////////////////////////////////////////////////////////
enum CollatorEsAudioState
{
    AudioSearchForStartCode,
    AudioSearchForPayload,
    AudioSearchForInitialHeader,
    AudioSearchForHeaderPts,
    AudioSearchForStuffingByteEnd,
};

////////////////////////////////////////////////////////////////////////////
/// \fn const char * StringifyState(CollatorEsAudioState State)
///
/// Convert State to printable string
///
/// State (in) : Input State
///
/// \return  State String
///
static inline const char *StringifyState(
    CollatorEsAudioState State)
{
    switch (State)
    {
    case AudioSearchForStartCode :
        return "AudioSearchForStartCode";
    case AudioSearchForPayload :
        return "AudioSearchForPayload";
    case AudioSearchForInitialHeader :
        return "AudioSearchForInitialHeader";
    case AudioSearchForHeaderPts :
        return "AudioSearchForHeaderPts";
    case AudioSearchForStuffingByteEnd :
        return "AudioSearchForStuffingByteEnd";
    default:
        return "AudioUnknownState";
    }
}

#define MAX_MPEG1_SYSTEM_HEADER_LEN         30
#define MPEG1_SYSTEM_HEADER_INITIAL_SIZE    6   // StartCode + Packet_length
#define MPEG1_SYSTEM_HEADER_PTS_DTS_SIZE    5   // PTS / DTS Size
#define MAX_MPEG1_PAD_ALLOWED               16  // As per 711.1

////////////////////////////////////////////////////////////////////////////
/// \class Collator_Es_Audio_Sysb_Mp2_c
///
/// Audio SYSB ES Collator Class
///
/// Provides State machine & Data processing functions needed by SYSB ES
/// Audio MP2 parser
///
///
class Collator_Es_Audio_Sysb_Mp2_c : public Collator_Es_c
{
public:
    Collator_Es_Audio_Sysb_Mp2_c();
    ~Collator_Es_Audio_Sysb_Mp2_c();

private:
    CollatorBufferUtils_c       mHeader;
    // Pad length (used in User Data Packets)
    unsigned int                mPadLength;
    // Payload length tracks length of Payload after MPEG1 System Packet
    unsigned int                mPayloadLength;
    unsigned char               mMpeg1SystemHeader [MAX_MPEG1_SYSTEM_HEADER_LEN];
    CollatorEsAudioState        mState;

    // State machine related functions
    void ResetAllVariables();
    void ChangeState(CollatorEsAudioState State);

    // Data processing
    virtual CollatorStatus_t InputSecondStage(unsigned int DataLen, const void *Data);
    CollatorStatus_t   DiscardAccumulatedData();
    unsigned int GetPadLength(CollatorBufferDesc_s &CurrentBuffer,
                              bool &Completed);
    CollatorStatus_t ParseAudioMpeg1SystemPacketForPTS(unsigned char *Buf);

    CollatorStatus_t Halt();
};

#endif //COLLATOR_ES_AUDIO_SYSB_MP2_H_
