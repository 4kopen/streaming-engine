/************************************************************************
Copyright (C) 2003-2014 STMicroelectronics. All Rights Reserved.

This file is part of the Streaming Engine.

Streaming Engine is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License version 2 as
published by the Free Software Foundation.

Streaming Engine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with Streaming Engine; see the file COPYING.  If not, write to the Free
Software Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307,
USA.

The Streaming Engine Library may alternatively be licensed under a
proprietary license from ST.
************************************************************************/

#include "collator_es_audio_sysb_mp2.h"

////////////////////////////////////////////////////////////////////////////
/// \fn Collator_Es_Audio_Sysb_Mp2_c::ParseAudioMpeg1SystemPacketForPTS(
///                                         unsigned char* Buf)
///
/// Parser MPEG1 Header to retrieve PTS
///
/// Buf (in) : Input Buffer (Header starting position)
///
///
/// \return Collator status code, CollatorNoError indicates success.
///
CollatorStatus_t Collator_Es_Audio_Sysb_Mp2_c::ParseAudioMpeg1SystemPacketForPTS(
    unsigned char *Buf)
{
    unsigned long long PTS;

    mPlaybackTimeValid          = false;

///         | 7 | 6 | 5 | 4 | 3 | 2 | 1 | 0 |
/// 0       < 0   0   1   0>< PTS      > <MB>
/// 1       < PTS Continued -----------------
/// 2       ---------------------------> <MB>
/// 3       < PTS Continued -----------------
/// 4       ---------------------------> <MB>

    if (GET_BITS(Buf[0], 7, 4)  == 0x2)
    {
        SE_DEBUG(group_collator_audio, "Received PTS :\n");

        PTS = ((unsigned long)GET_BITS(Buf[0], 3, 3) << 30);
        PTS += ((unsigned long)Buf[1] << 22);
        PTS += ((unsigned long)GET_BITS(Buf[2], 7, 7) << 15);
        PTS += ((unsigned long)Buf[3] << 7);
        PTS += (unsigned long)Buf[4];

        if (GET_BITS(Buf[0], 0, 1) != 1)
        {
            SE_WARNING("First Marker Data 0 in Audio PTS\n");
        }

        if (GET_BITS(Buf[2], 0, 1) != 1)
        {
            SE_WARNING("Second Marker Data 0 in Audio PTS\n");
        }

        if (GET_BITS(Buf[4], 0, 1) != 1)
        {
            SE_WARNING("Third Marker Data 0 in Audio PTS\n");
        }

        mPlaybackTimeValid  = true;
        mPlaybackTime       = PTS;
    }
    else
    {
        SE_ERROR("User Data type 0x%x not supported\n",
                 GET_BITS(Buf[0], 7, 4));
    }

    SE_VERBOSE(group_collator_audio, "Finished Audio Parsing\n");
    return CollatorNoError;
}

