/************************************************************************
Copyright (C) 2003-2014 STMicroelectronics. All Rights Reserved.

This file is part of the Streaming Engine.

Streaming Engine is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License version 2 as
published by the Free Software Foundation.

Streaming Engine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with Streaming Engine; see the file COPYING.  If not, write to the Free
Software Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307,
USA.

The Streaming Engine Library may alternatively be licensed under a
proprietary license from ST.
************************************************************************/

#ifndef COLLATOR_ES_SYSB_PARSER_H_
#define COLLATOR_ES_SYSB_PARSER_H_

// Useful Macros to extract bits
#define SHIFT(msb_bit, length)          (msb_bit + 1 - length )
#define BIT_MASK(msb_bit, length)       ((0xff >> (8-length)) << SHIFT (msb_bit, length))
#define GET_BITS(byte, msb_bit, length) ((byte & BIT_MASK (msb_bit, length)) >> SHIFT (msb_bit,length))

// Video Codes
#define START_CODE_MPEG_PICTURE_CODE            0x00
#define START_CODE_MPEG_SLICE_CODE              0x01
#define END_CODE_MPEG_SLICE_CODE                0xAF
#define START_CODE_MPEG_USER_DATA               0xB2
#define START_CODE_MPEG_SEQUENCE_HEADER         0xB3
#define START_CODE_MPEG_SEQUENCE_ERROR_CODE     0xB4
#define START_CODE_MPEG_EXT_CODE                0xB5
#define END_CODE_MPEG_SEQUENCE                  0xB7
#define START_CODE_MPEG_GROUP_START_CODE        0xB8

// Audio Code
#define START_CODE_AUDIO_MPEG1_SYSTEM_PACKET    0xC0

#define USER_DATA_PTS                           0x02
#define USER_DATA_DTS                           0x04
#define USER_DATA_PAN_SCAN                      0x06
#define USER_DATA_FIELD_DISPLAY_FLAGS           0x07
#define USER_DATA_CLOSED_CAPTION                0x09
#define USER_DATA_EXT_DATA_SERVICES             0x0A
#define USER_DATA_EXT_USER_DATA_TYPE            0xFF

#endif // COLLATOR_ES_SYSB_PARSER_H_
