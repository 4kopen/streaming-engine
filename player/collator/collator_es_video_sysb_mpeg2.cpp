/************************************************************************
Copyright (C) 2003-2014 STMicroelectronics. All Rights Reserved.

This file is part of the Streaming Engine.

Streaming Engine is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License version 2 as
published by the Free Software Foundation.

Streaming Engine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with Streaming Engine; see the file COPYING.  If not, write to the Free
Software Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307,
USA.

The Streaming Engine Library may alternatively be licensed under a
proprietary license from ST.
************************************************************************/

#include "collator_es_video_sysb_mpeg2.h"

////////////////////////////////////////////////////////////////////////////
/// \fn Collator_Es_Video_Sysb_Mpeg2_c::Collator_Es_Video_Sysb_Mpeg2_c()
///
/// Default constructor
///
Collator_Es_Video_Sysb_Mpeg2_c::Collator_Es_Video_Sysb_Mpeg2_c()
    : Collator_Es_c(group_collator_video)
    , mState(VideoSearchForStartCode)
    , mWaitingForEnd(false)
{
    Configuration.GenerateStartCodeList             = true;
    Configuration.MaxStartCodes                     = 256;
    Configuration.RequireFrameHeaders               = true;
    Configuration.MaxHeaderSize                     = 32 * 1024;
    Configuration.IgnoreCodesRanges.NbEntries       = 1;
    Configuration.IgnoreCodesRanges.Table[0].Start  = MPEG2_FIRST_SLICE_START_CODE + 1; // Slice codes other than first
    Configuration.IgnoreCodesRanges.Table[0].End    =  MPEG2_GREATEST_SLICE_START_CODE;

    // Force the mme decode to terminate after a picture
    Configuration.InsertFrameTerminateCode          = false;
    Configuration.TerminalCode                      = MPEG2_SEQUENCE_END_CODE;
    Configuration.ExtendedHeaderLength              = 0; // Not needed
}

////////////////////////////////////////////////////////////////////////////
/// \fn Collator_Es_Video_Sysb_Mpeg2_c::~Collator_Es_Video_Sysb_Mpeg2_c()
///
/// Default destructor
///

Collator_Es_Video_Sysb_Mpeg2_c::~Collator_Es_Video_Sysb_Mpeg2_c()
{
}

////////////////////////////////////////////////////////////////////////////
/// \fn Collator_Es_Audio_Sysb_Mp2_c::Halt ()
///
///  Function to halt Audio
///
/// \return CollatorStatus_t
///
CollatorStatus_t Collator_Es_Video_Sysb_Mpeg2_c::Halt()
{
    Collator_Es_c::Halt();
    ResetAllVariables();
    return CollatorNoError;
}

////////////////////////////////////////////////////////////////////////////
/// \fn bool Collator_Es_Video_Sysb_Mpeg2_c::IsValidStartCode (unsigned char StreamId)
/// Check if Input Stream Id is one of Valid Start code
///
/// StreadId (in) : Stream Id (4th byte of 001<SID> sequence)
///
/// \return  bool (true if Stream Id is valid start code, else false)
///
bool Collator_Es_Video_Sysb_Mpeg2_c::IsValidStartCode(unsigned char StreamId)
{
    bool IsValid = false;
    switch (StreamId)
    {
    case STREAM_ID_SEQUENCE_HEADER :
    case STREAM_ID_GOP_HEADER :
    case STREAM_ID_PICTURE_HEADER :
        IsValid = true;
        break;
    default :
        break;
    }

    return IsValid;
}

////////////////////////////////////////////////////////////////////////////
/// \fn bool Collator_Es_Video_Sysb_Mpeg2_c::IsValidEndCode (unsigned char StreamId)
/// Check if Input Stream Id is one of Valid End code
///
/// StreadId (in) : Stream Id (4th byte of 001<SID> sequence)
///
/// \return  bool (true if Stream Id is valid end code, else false)
///
bool Collator_Es_Video_Sysb_Mpeg2_c::IsValidEndCode(unsigned char StreamId)
{
    bool IsValid = false;
    switch (StreamId)
    {
    case STREAM_ID_SEQUENCE_HEADER :
    case STREAM_ID_SEQUENCE_END :       // Extra End code which can't be start code
    case STREAM_ID_GOP_HEADER :
    case STREAM_ID_PICTURE_HEADER :
        IsValid = true;
        break;
    default :
        break;
    }
    return IsValid;
}

////////////////////////////////////////////////////////////////////////////
/// \fn Collator_Es_Video_Sysb_Mpeg2_c::ChangeState (CollatorEsVideoState State)
///
///  Function to set new Video SYSB MPEG2 state
///
///  State (in) : New State
/// \return void
///
void Collator_Es_Video_Sysb_Mpeg2_c::ChangeState(CollatorEsVideoState State)
{
    SE_DEBUG(group_collator_video, "New Collator State : %s\n", StringifyState(State));
    mState = State;
}

////////////////////////////////////////////////////////////////////////////
/// \fn void Collator_Es_Video_Sysb_Mpeg2_c::ResetAllVariables ()
///
///  Reset All Internal Variables
///  Called after every InternalFrameFlush / DiscardAccumulatedData
///
/// \return void
///
void Collator_Es_Video_Sysb_Mpeg2_c::ResetAllVariables()
{
    Collator_Es_c::ResetCommonBuffers();

    mWaitingForEnd = false;
    mState = VideoSearchForStartCode;
}

////////////////////////////////////////////////////////////////////////////
/// \fn CollatorStatus_t Collator_Es_Video_Sysb_Mpeg2_c::ProcessStartCodeList (
///                                         unsigned int StartCodeLen,
///                                         PackedStartCode_t* StartCodeList)
///
/// Process Start code list
/// 2 purpose : Get PTS / DTS + Verify constraints imposed by SYSB specs
/// StartCodeLen (in) : Number of Start Codes
/// StartCodeList (in) : Pointer to Start Code list
///
///
/// \return Collator status code, CollatorNoError indicates success.
///
CollatorStatus_t Collator_Es_Video_Sysb_Mpeg2_c::ProcessStartCodeList(
    unsigned int StartCodeLen,
    PackedStartCode_t *StartCodeList)
{
    CollatorStatus_t    Status = CollatorNoError;
    unsigned int Offset;
    unsigned char Code;

    SE_DEBUG(group_collator_video, "Printing Starting Code List : %d\n", StartCodeLen);
    for (unsigned int i = 0; i < StartCodeLen; i++)
    {
        // 32 bit offset, skip 4 bytes for start code
        Offset = (unsigned int)(StartCodeList[i] >> 8) + 4;
        // 8 bit code
        Code = (unsigned char)(StartCodeList[i] & 0xff);
        switch (Code)
        {
        case STREAM_ID_SEQUENCE_HEADER:
            Status = ValidateVideoSequenceHeader(
                         mCollatedBuf.GetBuffer() + Offset);
            break;
        case STREAM_ID_PICTURE_HEADER:
            Status = ValidateVideoPictureHeader(
                         mCollatedBuf.GetBuffer() + Offset);
            break;
        case STREAM_ID_USER_DATA:
        {
            unsigned int NumBytesParsed = 0;
            unsigned int MaxRemainingLength = mCollatedBuf.GetBufferFilledLen() - Offset;
            unsigned char *InputBuf = mCollatedBuf.GetBuffer() + Offset ;
            do
            {
                Status = ParseVideoUserData(InputBuf, &NumBytesParsed);
                InputBuf += NumBytesParsed;

                if (MaxRemainingLength <= NumBytesParsed)
                {
                    break;
                }

                MaxRemainingLength -= NumBytesParsed;
            }
            while (!((InputBuf[0] == 0x0) &&
                     (InputBuf[1] == 0x0) &&
                     (InputBuf[2] == 0x1)));

            break;
        }
        case STREAM_ID_EXTENSION:
            switch (*(mCollatedBuf.GetBuffer() + Offset))
            {
            case SEQUENCE_EXTENSION_ID:
                ValidateVideoSequenceExtension(mCollatedBuf.GetBuffer() + Offset);
                break;
            case SEQUENCE_EXTENSION_DISPLAY_ID:
                ValidateVideoSequenceDisplayExtension(mCollatedBuf.GetBuffer() + Offset);
                break;
            }
            break;
        default:
            break;
        }

        if (Status != CollatorNoError)
        {
            break;
        }
    }
    return Status;
}

////////////////////////////////////////////////////////////////////////////
/// \fn CollatorStatus_t Collator_Es_Video_Sysb_Mpeg2_c::InputSecondStage (
///                                         unsigned int BufLen,
///                                         void  *Buffer)
///
/// Handle input buffer coming from Collator ES Base class
///
/// BufLen (in) : Current Input buffer length
/// Buffer (in) : Current Input buffer
///
///
/// \return Collator status code, CollatorNoError indicates success.
///
CollatorStatus_t Collator_Es_Video_Sysb_Mpeg2_c::InputSecondStage(
    unsigned int  BufLen,
    const void   *Buffer)
{
    CollatorStatus_t Status = CollatorNoError;
    // To keep track of Current ES Packet
    CollatorBufferDesc_s CurrentBuffer = {(unsigned char *)Buffer, BufLen, 0};

    unsigned char StreamId;
    // Position of Start Code in Combined Previous (remaining) +
    // Current ES packet, rewritten everytime
    unsigned int StartCodeOffset;
    // Keep track of how many Bytes are copied, rewritten everytime
    unsigned int NumBytesCopied;
    // To know whether function completed its task or not,
    // If not it will be called again with next ES packet
    bool Completed = false;
    // To check valid start / end code
    bool IsValid = false;

    SE_DEBUG(group_collator_video, "Previous Buffer Length: %d, Current Buffer Length: %d\n",
             mPreviousBuffer.Len,  CurrentBuffer.Len);

    // First search for Start Code
    while ((CurrentBuffer.Len > 0) && (Status == CollatorNoError))
    {
        if (mCollatorSink->PlaybackIsInLowPowerState())
        {
            // Stop processing data to speed-up low power enter procedure (bug 24248)
            break;
        }

        switch (mState)
        {
        case VideoSearchForStartCode:
            Status = mCollatedBuf.DiscardTillStartCode(&mPreviousBuffer,
                                                       &CurrentBuffer,
                                                       &StartCodeOffset,
                                                       &StreamId,
                                                       &Completed);
            if ((!Completed) || (Status != CollatorNoError))
            {
                break;
            }

            // Move On, received Start Code & Stream Id
            SE_DEBUG(group_collator_video, "Received Stream Id : 0x%x, Offset in Current ES : 0x%x\n",
                     StreamId, BufLen - CurrentBuffer.Len);

            IsValid = IsValidStartCode(StreamId);
            if (IsValid)
            {
                if (StreamId == STREAM_ID_PICTURE_HEADER)
                {
                    mWaitingForEnd = true;
                }

                if (!IsCodeTobeIgnored(StreamId))
                {
                    // Save Start Code List Using Special PackStartCode Macro
                    // TODO : AccumulateRemainingLength only based on Configuration
                    AccumulateStartCode(
                        PackStartCode(mCollatedBuf.GetBufferFilledLen(), StreamId));
                }

                SE_DEBUG(group_collator_video, "Received Start Code : 0x%x, Offset in Current ES : 0x%x\n",
                         StreamId, BufLen - CurrentBuffer.Len);
                ChangeState(VideoSearchForPayload);
            }

            // No need to check Completed flag as 100% gaurantee for success
            Status = mCollatedBuf.AccumulateAbsoluteLength(&mPreviousBuffer,
                                                           &CurrentBuffer,
                                                           START_CODE_LENGTH,
                                                           &NumBytesCopied,
                                                           &Completed);
            if (Status != CollatorNoError)
            {
                SE_ERROR("VideoSearchForStartCode : AccumulateAbsoluteLength - 1 - failed\n");
                break;
            }
            break;

        case VideoSearchForPayload:
            // In this state unless there is special case, need to keep copying the data
            Status = mCollatedBuf.AccumulateTillStartCode(&mPreviousBuffer,
                                                          &CurrentBuffer,
                                                          &NumBytesCopied,
                                                          &StreamId,
                                                          &Completed);
            if ((!Completed) || (Status != CollatorNoError))
            {
                break;
            }

            IsValid = IsValidEndCode(StreamId);
            if (mWaitingForEnd && IsValid)
            {
                // Reached end
                // There is only one special case which is Sequence end
                // In case of Sequence end, include 001SID inside buffer
                if (StreamId == STREAM_ID_SEQUENCE_END)
                {
                    // Using AccumulateRemainingLength Length as it will pass for sure
                    // Accumulate start code in all cases
                    // however need to check if this is valid case for User data
                    if (!IsCodeTobeIgnored(StreamId))
                    {
                        AccumulateStartCode(
                            PackStartCode(mCollatedBuf.GetBufferFilledLen(), StreamId));
                    }

                    Status = mCollatedBuf.AccumulateAbsoluteLength(&mPreviousBuffer,
                                                                   &CurrentBuffer,
                                                                   START_CODE_LENGTH,
                                                                   &NumBytesCopied,
                                                                   &Completed);
                    if (Status != CollatorNoError)
                    {
                        SE_ERROR("VideoSearchForPayload : AccumulateAbsoluteLength - 1 - failed\n");
                        break;
                    }
                }

                ChangeState(VideoSearchForStartCode);

                // ProcessStartCodeList to extract PTS + Validate constraints
                // from SYSB specs
                Status = ProcessStartCodeList(StartCodeList->NumberOfStartCodes,
                                              StartCodeList->StartCodes);
                if (Status != CollatorNoError)
                {
                    SE_ERROR("VideoSearchForPayload : Invalid Start code list\n");
                    DiscardAccumulatedData();
                    break;
                }

                // One more frame complete, let's flush it to next stage
                Status = InternalFrameFlush();
                if (Status != CollatorNoError) { break; }

                // Reset All Variables
                ResetAllVariables();

                ChangeState(VideoSearchForStartCode);
            }
            else
            {
                if (StreamId == STREAM_ID_PICTURE_HEADER)
                {
                    mWaitingForEnd = true;
                }

                if (StreamId == STREAM_ID_USER_DATA)
                {
                    SE_DEBUG(group_collator_video, "Received User Data Info, Offset : 0x%x\n",
                             mCollatedBuf.GetBufferFilledLen());
                }

                // Accumulate start code in all cases
                // however also check if this is valid case for User data
                if (!IsCodeTobeIgnored(StreamId))
                {
                    AccumulateStartCode(
                        PackStartCode(mCollatedBuf.GetBufferFilledLen(), StreamId));
                }

                Status = mCollatedBuf.AccumulateAbsoluteLength(&mPreviousBuffer,
                                                               &CurrentBuffer,
                                                               START_CODE_LENGTH,
                                                               &NumBytesCopied,
                                                               &Completed);
                if (Status != CollatorNoError)
                {
                    SE_ERROR("VideoSearchForPayload : AccumulateAbsoluteLength - 2 -  failed\n");
                    break;
                }
            }
            break;

        default:
            // Not expected
            SE_ERROR("Critical Error : State Machine : %d\n", mState);
            Status = CollatorError;
            break;
        }   // Switch - case
    } // While Loop

    if (Status != CollatorNoError)
    {
        SE_ERROR("Discarding accumulated frame as Error Occurred in State %d\n", mState);
        DiscardAccumulatedData();
        ResetAllVariables();
    }

    return Status;
}

////////////////////////////////////////////////////////////////////////////
/// \fn CollatorStatus_t Collator_Es_Video_Sysb_Mpeg2_c::FillFrameHeaders ()
///
/// Prepare header meta data for frame parser usages.
/// function called from base class from InternalFrameFlush
///
/// \return Collator status code, CollatorNoError indicates success.
///
CollatorStatus_t Collator_Es_Video_Sysb_Mpeg2_c::FillFrameHeaders()
{
    CollatorStatus_t status;

    // Process StartCodes in CodedFrameBuffer
    for (int i = 0; i < StartCodeList->NumberOfStartCodes; i++)
    {
        PackedStartCode_t   startCode = StartCodeList->StartCodes[i];
        unsigned char      *header = BufferBase +
                                     ExtractStartCodeOffset(startCode) +
                                     START_CODE_SIZE;

        unsigned int        headerSize;

        // Calculate the user data length when its the last StartCode or an intermediate
        if (i == StartCodeList->NumberOfStartCodes - 1)
        {
            headerSize = AccumulatedDataSize -
                         ExtractStartCodeOffset(startCode) - START_CODE_SIZE;
        }
        else
        {
            headerSize = ExtractStartCodeOffset(StartCodeList->StartCodes[i + 1]) -
                         ExtractStartCodeOffset(startCode) -
                         START_CODE_SIZE;
        }

        int startCodeCode = ExtractStartCodeCode(startCode);
        switch (startCodeCode)
        {
        case MPEG2_PICTURE_START_CODE:
        case MPEG2_USER_DATA_START_CODE:
        case MPEG2_SEQUENCE_HEADER_CODE:
        case MPEG2_EXTENSION_START_CODE:
        case MPEG2_GROUP_START_CODE:
            status = PackHeader(startCodeCode, header, headerSize);
            if (status != CollatorNoError) { return status; }
            break;

        default:
            // Designate a header size 0 for SC not processed.
            status = PackHeader(startCodeCode, header, 0);
            if (status != CollatorNoError) { return status; }
            break;
        }
    }

    return CollatorNoError;
}
