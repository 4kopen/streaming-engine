/************************************************************************
Copyright (C) 2003-2014 STMicroelectronics. All Rights Reserved.

This file is part of the Streaming Engine.

Streaming Engine is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License version 2 as
published by the Free Software Foundation.

Streaming Engine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with Streaming Engine; see the file COPYING.  If not, write to the Free
Software Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307,
USA.

The Streaming Engine Library may alternatively be licensed under a
proprietary license from ST.
************************************************************************/
#ifndef COLLATOR_ES_VIDEO_SYSB_MPEG2_H_
#define COLLATOR_ES_VIDEO_SYSB_MPEG2_H_

#include "mpeg2.h"
#include "collator_es.h"
#include "collator_es_sysb_parser.h"

// TODO : MPEG2_PICTURE_USER_DATA_MAX_SIZE to be updated as per specs
#define MPEG2_PICTURE_USER_DATA_MAX_SIZE    30
#define STREAM_ID_SEQUENCE_HEADER           0xB3
#define STREAM_ID_EXTENSION                 0xB5    // User for All Seq / Picture Extns.
#define STREAM_ID_PICTURE_HEADER            0x00
#define STREAM_ID_SEQUENCE_END              0xB7
#define STREAM_ID_GOP_HEADER                0xB8
#define STREAM_ID_USER_DATA                 0xB2
#define STREAM_ID_MPEG1_SYSTEM_PACKET       0xC0

#define SEQUENCE_EXTENSION_ID               0x1
#define SEQUENCE_EXTENSION_DISPLAY_ID       0x2
#define PICTURE_DISPLAY_EXTENSION_ID        0x7
#define PICTURE_CODING_EXTENSION_ID         0x8

////////////////////////////////////////////////////////////////////////////
// Video states : Only 2 for now
////////////////////////////////////////////////////////////////////////////
enum CollatorEsVideoState
{
    VideoSearchForStartCode,
    VideoSearchForPayload
};

////////////////////////////////////////////////////////////////////////////
/// \fn const char * StringifyState(CollatorEsAudioState State)
/// Convert State to printable string
///
/// State (in) : Input State
///
/// \return  State String
///
static inline const char *StringifyState(
    CollatorEsVideoState State)
{
    switch (State)
    {
    case VideoSearchForStartCode :
        return "VideoSearchForStartCode";
    case VideoSearchForPayload :
        return "VideoSearchForPayload";
    default:
        return "VideoUnknownState";
    }
}

////////////////////////////////////////////////////////////////////////////
/// \class Collator_Es_Video_Sysb_Mpeg2_c
///
/// Audio SYSB ES Collator Class
///
/// Provides State machine & Data processing functions needed by SYSB ES
/// Video MPEG2 parser
///
///
class Collator_Es_Video_Sysb_Mpeg2_c : public Collator_Es_c
{
public:
    Collator_Es_Video_Sysb_Mpeg2_c();
    ~Collator_Es_Video_Sysb_Mpeg2_c();
    CollatorStatus_t InputSecondStage(unsigned int DataLength, const void *Data);
    CollatorStatus_t Halt();
private:
    CollatorEsVideoState    mState;
    bool                    mWaitingForEnd;

    // State machine related functions
    void ResetAllVariables();
    void ChangeState(CollatorEsVideoState State);
    bool IsValidStartCode(unsigned char StreamId);
    bool IsValidEndCode(unsigned char StreamId);

    // Data Processing
    CollatorStatus_t ProcessStartCodeList(unsigned int StartCodeLen,
                                          PackedStartCode_t *StartCodeList);

    CollatorStatus_t FillFrameHeaders();
    // Header Parsers - to validate constraints / extract PTS
    CollatorStatus_t ValidateVideoSequenceHeader(unsigned char *Buf);
    CollatorStatus_t ValidateVideoSequenceExtension(unsigned char *Buf);
    CollatorStatus_t ValidateVideoSequenceDisplayExtension(unsigned char *Buf);
    CollatorStatus_t ValidateVideoPictureHeader(unsigned char *Buf);

    // Extract PTS / DTS from User data
    CollatorStatus_t ParseVideoUserData(unsigned char *Buf, unsigned int *BytesParsed);
};

#endif // COLLATOR_ES_VIDEO_SYSB_MPEG2_H_
