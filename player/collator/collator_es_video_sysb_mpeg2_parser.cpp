/************************************************************************
Copyright (C) 2003-2014 STMicroelectronics. All Rights Reserved.

This file is part of the Streaming Engine.

Streaming Engine is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License version 2 as
published by the Free Software Foundation.

Streaming Engine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with Streaming Engine; see the file COPYING.  If not, write to the Free
Software Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307,
USA.

The Streaming Engine Library may alternatively be licensed under a
proprietary license from ST.
************************************************************************/

#include "collator_es_video_sysb_mpeg2.h"

////////////////////////////////////////////////////////////////////////////
/// \fn CollatorStatus_t Collator_Es_Video_Sysb_Mpeg2_c::ParseVideoUserData(
///                             unsigned char* Buf, unsigned int *BytesParsed)
///
/// Parser Video User data to retrieve PTS / DTS / Closed Caption etc.
///
/// Buf (in) :   Input Buffer (Header starting position)
/// Buf (out): Number of bytes parsed by the function
///
/// \return Collator status code, CollatorNoError indicates success.
///
CollatorStatus_t Collator_Es_Video_Sysb_Mpeg2_c::ParseVideoUserData(
    unsigned char *Buf,
    unsigned int *BytesParsed)
{
    /*
     * user_data_start_code 32 bslbf
     * while( nextbits() != '0000 0000 0000 0000 0000 0001' ) {
     * user_data_length 8 uimsbf
     * user_data_type 8 uimsbf
     * if (user_data_type==0xFF)
     * ext_user_data_type 8 uimsbf
     * user_data_info() (user_data_length-1)*8 uimsbf
     * }
     * next_start_code()
    */
    unsigned int UserDataLen;
    unsigned int UserDataType;

    // Byte 1
    UserDataLen = Buf[0];
    *BytesParsed = UserDataLen + 1;
    if (UserDataLen == 0)
    {
        return CollatorNoError;
    }

    // Byte 2
    UserDataType = Buf[1];

    if (UserDataType == USER_DATA_EXT_USER_DATA_TYPE)
    {
        unsigned int ExtUserDataType;
        // Byte 3
        ExtUserDataType = Buf[2];
        if (ExtUserDataType == 0x00)
        {
            //do nothing
        }
    }
    else
    {
        // Byte 3
        switch (UserDataType)
        {
        case USER_DATA_PTS:
        {
            unsigned char SparePadDiscard;
            unsigned long long PTS;

            SE_DEBUG(GetGroupTrace(), "Received PTS :\n");
            SparePadDiscard = GET_BITS(Buf[2], 7, 6);
            if (SparePadDiscard != 0)
            {
                SE_WARNING("PTS is wrong in User data, Spare bits are not 0\n");
            }

            if (GET_BITS(Buf[3], 7, 1) != 1)
            {
                SE_WARNING("PTS is wrong in User data, first marker data missing\n");
            }

            if (GET_BITS(Buf[5], 7, 1) != 1)
            {
                SE_WARNING("PTS is wrong in User data, Second marker data missing\n");
            }

            PTS = ((unsigned long long) GET_BITS(Buf[2], 1, 2) << 30);
            PTS += ((unsigned long long)GET_BITS(Buf[3], 6, 7) << 23);
            PTS += ((unsigned long long)Buf[4] << 15);
            PTS += ((unsigned long long)GET_BITS(Buf[5], 6, 7) << 8);
            PTS += (unsigned long long)Buf[6];
            // Received Valid PTS, let's set it
            mPlaybackTimeValid = true;
            mPlaybackTime = PTS;
            SE_DEBUG(GetGroupTrace(), "PTS is %lld\n", mPlaybackTime);
            break;
        }

        case USER_DATA_DTS:
        {
            unsigned long long DTS;
            unsigned char SparePadDiscard;

            SE_DEBUG(GetGroupTrace(), "Received DTS\n");
            SparePadDiscard = GET_BITS(Buf[2], 7, 6);
            if (SparePadDiscard != 0)
            {
                SE_WARNING("DTS is wrong in User data, Spare bits are not 0\n");
            }

            if (GET_BITS(Buf[3], 7, 1) != 1)
            {
                SE_WARNING("DTS is wrong in User data, first marker data missing\n");
            }

            if (GET_BITS(Buf[5], 7, 1) != 1)
            {
                SE_WARNING("DTS is wrong in User data, Second marker data missing\n");
            }

            DTS = ((unsigned long long) GET_BITS(Buf[2], 1, 2) << 30);
            DTS += ((unsigned long long)GET_BITS(Buf[3], 6, 7) << 23);
            DTS += ((unsigned long long)Buf[4] << 15);
            DTS += ((unsigned long long)GET_BITS(Buf[5], 6, 7) << 8);
            DTS += (unsigned long long)Buf[6];

            mDecodeTimeValid = true;
            mDecodeTime = DTS;
            break;
        }

        case USER_DATA_PAN_SCAN:
        {
            unsigned int PanAndScan;

            SE_DEBUG(GetGroupTrace(), "Received Pan & Scan\n");
            // Byte 1
            PanAndScan = Buf[2] << 4;
            // Byte 2
            PanAndScan += GET_BITS(Buf[3], 7, 4);

            /* Leave 1 bit for marker + three bits pad */
            if (GET_BITS(Buf[3], 3, 4) != 0x08)    // 1000
            {
                SE_WARNING("Pan Scan data is wrong, marker + padding is bad\n");
            }

            SE_DEBUG(GetGroupTrace(), "%u\n", PanAndScan);
            break;
        }

        case USER_DATA_FIELD_DISPLAY_FLAGS:
        {
            unsigned int FieldDisplayFlags;
            SE_DEBUG(GetGroupTrace(), "Received Field Display Flags :\n");
            // Byte 1
            FieldDisplayFlags = Buf[2];
            SE_DEBUG(GetGroupTrace(), "%u\n", FieldDisplayFlags);
            break;
        }

        case USER_DATA_CLOSED_CAPTION:
        {
            unsigned int ClosedCaption;
            SE_DEBUG(GetGroupTrace(), "Received Closed Caption :\n");
            ClosedCaption = Buf[2] << 8;
            ClosedCaption += Buf[3];
            SE_DEBUG(GetGroupTrace(), "%u\n", ClosedCaption);
            break;
        }

        case USER_DATA_EXT_DATA_SERVICES:
        {
            unsigned int ExtendedDataServices;
            SE_DEBUG(GetGroupTrace(), "Received Extended Data Services  :\n");
            ExtendedDataServices = Buf[2] << 8;
            ExtendedDataServices += Buf[3];
            SE_DEBUG(GetGroupTrace(), "%u\n", ExtendedDataServices);
            break;
        }

        default :
            SE_WARNING("Received Unexpected User code : %u\n", UserDataType);
            break;
        }
    }

    return CollatorNoError;
}

////////////////////////////////////////////////////////////////////////////
/// \fn CollatorStatus_t Collator_Es_Video_Sysb_Mpeg2_c::ValidateVideoPictureHeader(
///                                         unsigned char* Buf)
///
/// Validate Video Picture Header to check constraint specified by SYSB specs
///
/// Buf (in) : Input Buffer (Header starting position)
/// Hdr (out) : Filled Header structure (Containing PTS)
///
///
/// \return Collator status code, CollatorNoError indicates success.
///
CollatorStatus_t Collator_Es_Video_Sysb_Mpeg2_c::ValidateVideoPictureHeader(unsigned char *Buf)
{
//         | 7 | 6 | 5 | 4 | 3 | 2 | 1 | 0 |
/// 0       < Temporal Reference ------------
/// 1       --------><Pic Cod T><VBV Delay --
/// 2       ---------------------------------
/// 3       -------------------><Optional forward
/// 4       / backward vector--><EB>        // EB could be at 3 position dpending on Picture Coding

    unsigned int PictureCodingType = GET_BITS(Buf[1], 5, 3);
    unsigned char ExtraBitPicture;

    if (PictureCodingType == 2)
    {
        ExtraBitPicture = GET_BITS(Buf[4], 6, 1);
    }
    else if (PictureCodingType == 3)
    {
        ExtraBitPicture = GET_BITS(Buf[4], 2, 1);
    }
    else
    {
        ExtraBitPicture = GET_BITS(Buf[3], 2, 1);
    }

    if (ExtraBitPicture)
    {
        SE_WARNING("Extra_bit_picture in Picture Header is wrong : %u\n", ExtraBitPicture);
    }

    return CollatorNoError;
}

////////////////////////////////////////////////////////////////////////////
/// \fn CollatorStatus_t Collator_Es_Video_Sysb_Mpeg2_c::ValidateVideoSequenceHeader(
///                                         unsigned char* Buf,
///
/// Validate Video Sequence Header to check constraint specified by SYSB specs
///
/// Buf (in) : Input Buffer (Header starting position)
///
///
/// \return Collator status code, CollatorNoError indicates success.
///

CollatorStatus_t Collator_Es_Video_Sysb_Mpeg2_c::ValidateVideoSequenceHeader(
    unsigned char *Buf)
{
///         | 7 | 6 | 5 | 4 | 3 | 2 | 1 | 0 |
/// 0       < Horizontal Size Value ---------
/// 1       ---------------->< Vertical Size>
/// 2       Value -------------------------->
/// 3       < Aspect Ratio ><Frame rate code>
/// 4       < Bit rate Value ----------------
/// 5       ---------------------------------
/// 6       --------><MB>< VBV Buffer size --
/// 7       --------------------><CP><LI><If LI Then 8 Bytes of LI Matrix
///         -----------------------------<LNI>

    // In total 6 constraints to be checked apart from Marker bit

    unsigned int HorizontalSize = (Buf[0] << 4) + GET_BITS(Buf[1], 7, 4);
    unsigned int VerticalSize = (GET_BITS(Buf[1], 3, 4) << 8) + Buf[2];
    unsigned int AspectRatio = GET_BITS(Buf[3], 7, 4);
    unsigned int FrameRateCode = GET_BITS(Buf[3], 3, 4);
    unsigned int BitRateValue = (Buf[4] << 10) + (Buf[5] << 2) +
                                GET_BITS(Buf[6], 7, 2);
    unsigned char MarkerBit = GET_BITS(Buf[6], 5, 1);
    unsigned int VbvBufferSize = (GET_BITS(Buf[6], 4, 5) << 5) +
                                 GET_BITS(Buf[7], 7, 5);

    SE_VERBOSE(GetGroupTrace(), "Horizontal size : %u\n", HorizontalSize);
    SE_VERBOSE(GetGroupTrace(), "Vertical size : %u\n", VerticalSize);
    SE_VERBOSE(GetGroupTrace(), "aspect_ratio : %u\n", AspectRatio);
    SE_VERBOSE(GetGroupTrace(), "frame_rate_code : %u\n", FrameRateCode);
    SE_VERBOSE(GetGroupTrace(), "bit_rate_value : %u\n", BitRateValue);
    SE_VERBOSE(GetGroupTrace(), "marker_bit : %u\n", MarkerBit);
    SE_VERBOSE(GetGroupTrace(), "vbv_buffer_size : %u\n", VbvBufferSize);

    if (!MarkerBit)
    {
        SE_WARNING("marker_bit in Sequence Header is wrong : %u\n", MarkerBit);
    }

    if ((AspectRatio != 0x2) && (AspectRatio != 0x3))
    {
        SE_WARNING("aspect_ratio in Sequence Header is wrong : %u\n", AspectRatio);
    }

    if (FrameRateCode != 0x4)
    {
        SE_WARNING("frame_rate_code in Sequence Header is wrong : %u\n", FrameRateCode);
    }
    // Rest of constraints are not hard constraints, so not puting any check for now
    // Can be added if real need is seen in future

    return CollatorNoError;
}

////////////////////////////////////////////////////////////////////////////
/// \fn CollatorStatus_t Collator_Es_Video_Sysb_Mpeg2_c::ValidateVideoSequenceExtension(
///                                         unsigned char* Buf,
///                                         SysbAudioMpeg1SystemPacketPTS_s &Hdr)
///
/// Parser Video Sequence Header to check constraint specified by SYSB specs
///
/// Buf (in) : Input Buffer (Header starting position)
/// Hdr (out) : Filled Header structure (Containing PTS)
///
///
/// \return Collator status code, CollatorNoError indicates success.
///

CollatorStatus_t Collator_Es_Video_Sysb_Mpeg2_c::ValidateVideoSequenceExtension(
    unsigned char *Buf)
{
///         | 7 | 6 | 5 | 4 | 3 | 2 | 1 | 0 |
/// 0       < Start Code ID> <Profile & level
/// 1       Profile & level> <PS><  CF >< HS
/// 2       Ext><VS Ext> <bit rate extn
/// 3       -- bit rate extn -----------><MB>
/// 4       < vbv buffer size extension     >
/// 5       <LD> <fram n>< Frame D          >

    // For Video sequence extension there are only 2 constraints to be checked
    unsigned char ProgressiveSequence = GET_BITS(Buf[1], 3, 1);
    unsigned char LowDelay = GET_BITS(Buf[5], 7, 1);

    // Both progressive_sequence / low_delay should be 0
    if (ProgressiveSequence)
    {
        SE_WARNING("progressive_sequence in Sequence Extension is wrong : %u\n", ProgressiveSequence);
    }

    if (LowDelay)
    {
        SE_WARNING("low_delay in Sequence Display  is wrong : %u\n", LowDelay);
    }

    return CollatorNoError;
}

////////////////////////////////////////////////////////////////////////////
/// \fn CollatorStatus_t Collator_Es_Video_Sysb_Mpeg2_c::ValidateVideoSequenceDisplayExtension(
///                                         unsigned char* Buf)
///
/// Check Sequence Display Extension Constraints as imposed by Specs
///
/// Buf (in) : Input Buffer (Header starting position)
///
///
/// \return Collator status code, CollatorNoError indicates success.
///

CollatorStatus_t Collator_Es_Video_Sysb_Mpeg2_c::ValidateVideoSequenceDisplayExtension(
    unsigned char *Buf)
{
///         | 7 | 6 | 5 | 4 | 3 | 2 | 1 | 0 |
/// 0       <Start Cdoe Id  ><Vid Format><CD>
/// 1       <Display Horizontal Size --------
/// 2       Display Horizontal Size> <MB><Dis
/// 3       play Vertical Size --------------
/// 4       -------------------->

    // For Video sequence extension there are only 2 constraints to be checked
    unsigned char VideoFormat = GET_BITS(Buf[0], 3, 3);
    unsigned char ColourDescription = GET_BITS(Buf[0], 1, 1);

    unsigned int DisplayHorizSize = (Buf[1] << 6) + GET_BITS(Buf[2], 7, 6); // 14 bits
    unsigned int DisplayVertSize = (GET_BITS(Buf[2], 0, 1) << 13) +
                                   (Buf[3] << 5) +
                                   GET_BITS(Buf[4], 7, 5); // 14 bits

    SE_DEBUG(GetGroupTrace(), "video_format : %u, colour_description : %u\n", VideoFormat, ColourDescription);
    if (VideoFormat != 0x2)
    {
        SE_WARNING("video_format in Sequence Display Extension is wrong : %u\n", VideoFormat);
    }

    if (ColourDescription != 1)
    {
        SE_WARNING("colour_description in Sequence Display Extension is wrong : %u\n", ColourDescription);
    }

    // Display Horizontal / Vertical size should be ignored
    SE_DEBUG(GetGroupTrace(), "display_horizontal_size : %u, display_vertical_size : %u\n",
             DisplayHorizSize, DisplayVertSize);
    return CollatorNoError;
}

////////////////////////////////////////////////////////////////////////////
/// \fn CollatorStatus_t Collator_Es_Video_Sysb_Mpeg2_c::ValidateVideoSequenceDisplayExtension(
///                                         unsigned char* Buf)
///
/// Check Sequence Display Extension Constraints as imposed by Specs
///
/// Buf (in) : Input Buffer (Header starting position)
///
///
/// \return Collator status code, CollatorNoError indicates success.
///

///         | 7 | 6 | 5 | 4 | 3 | 2 | 1 | 0 |
/// 0       <Start Code Id  >< fcode (0)(0) >
/// 1       < fcode (0)(1) > < fcode (1)(0) >
/// 2       < fcode (1)(1) ><IntraDC><PicStr>
/// 3       <TF><FP><CMV><QS><IVF><AS><RF><CF>
/// 4       <PF><CD><VA>< Field Seq ><SC><Burst
/// 5       Amplitude --------------><Sub Carrier
/// 6       Phase>

// Do Nothing, as IRD should ignore values
