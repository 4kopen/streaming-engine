/************************************************************************
Copyright (C) 2003-2014 STMicroelectronics. All Rights Reserved.

This file is part of the Streaming Engine.

Streaming Engine is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License version 2 as
published by the Free Software Foundation.

Streaming Engine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with Streaming Engine; see the file COPYING.  If not, write to the Free
Software Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307,
USA.

The Streaming Engine Library may alternatively be licensed under a
proprietary license from ST.
************************************************************************/

#include "collator_pes.h"

Collator_Pes_c::Collator_Pes_c()
    : SeekingPesHeader(true)
    , GotPartialHeader(false)
    , GotPartialType()
    , GotPartialCurrentSize()
    , GotPartialDesiredSize()
    , StoredPartialHeader(NULL)
    , StoredPartialHeaderCopy()
    , GotPartialZeroHeader(false)
    , GotPartialZeroHeaderBytes()
    , StoredZeroHeader(NULL)
    , GotPartialPesHeader(false)
    , GotPartialPesHeaderBytes()
    , StoredPesHeader(NULL)
    , GotPartialPaddingHeader(false)
    , GotPartialPaddingHeaderBytes()
    , StoredPaddingHeader(NULL)
    , Skipping(0)
    , RemainingLength(0)
    , RemainingData(NULL)
    , PesPacketLength(0)
    , PesPayloadLength(0)
    , PlaybackTimeValid(false)
    , DecodeTimeValid(false)
    , PlaybackTime(INVALID_TIME)
    , DecodeTime(INVALID_TIME)
    , UseSpanningTime(false)
    , SpanningPlaybackTimeValid(false)
    , SpanningPlaybackTime(INVALID_TIME)
    , SpanningDecodeTimeValid(false)
    , SpanningDecodeTime(INVALID_TIME)
    , ControlUserData()
    , Bits()
    , AudioDescriptionInfo()
    , IsMpeg4p2Stream(false)
    , LastGotNesting(false)
    , LastGotPartialCurrentSize(0)
    , LastGotPartialDesiredSize(0)
    , LastGotPartialType()
{
}

Collator_Pes_c::~Collator_Pes_c()
{
    Halt();
}


// /////////////////////////////////////////////////////////////////////////
//
//      The Halt function, give up access to any registered resources
//

CollatorStatus_t   Collator_Pes_c::Halt()
{
    DiscardAccumulatedData();
    StoredPesHeader     = NULL;
    StoredPaddingHeader = NULL;
    RemainingData       = NULL;
    return Collator_Base_c::Halt();
}

// /////////////////////////////////////////////////////////////////////////
//
//      The discard all accumulated data function
//

CollatorStatus_t   Collator_Pes_c::DiscardAccumulatedData()
{
    CollatorStatus_t Status = Collator_Base_c::DiscardAccumulatedData();
    if (Status != CodecNoError)
    {
        return Status;
    }

    SeekingPesHeader            = true;
    GotPartialHeader            = false;    // New style most video
    GotPartialZeroHeader        = false;    // Old style used by divx only
    GotPartialPesHeader         = false;
    GotPartialPaddingHeader     = false;
    Skipping                    = 0;
    UseSpanningTime             = false;
    SpanningPlaybackTimeValid   = false;
    SpanningDecodeTimeValid     = false;
    return Status;
}


// /////////////////////////////////////////////////////////////////////////
//
//      The discard all accumulated data function
//

CollatorStatus_t   Collator_Pes_c::InputJump(int discontinuity)
{
    SE_DEBUG(GetGroupTrace(), "\n");

    CollatorStatus_t Status = Collator_Base_c::InputJump(discontinuity);
    if (Status != CodecNoError)
    {
        return Status;
    }

    PlaybackTimeValid           = false;
    DecodeTimeValid             = false;
    UseSpanningTime             = false;
    SpanningPlaybackTimeValid   = false;
    SpanningDecodeTimeValid     = false;
    SeekingPesHeader            = true;
    mFoundReqTimeControlData    = false;
    return Status;
}

// /////////////////////////////////////////////////////////////////////////
//
//      Protected - Find the next start code (apart from any one at offset 0)
//

CollatorStatus_t   Collator_Pes_c::FindNextStartCode(
    unsigned int             *CodeOffset)
{
    unsigned char *data;
    unsigned int  *data32;
    unsigned int  Code1, Code2, Code3, SearchedSynchro;
    int           i, n;

    //
    // If less than 4 bytes we do not bother
    //

    if (RemainingLength < 4)
    {
        return CollatorError;
    }

    UseSpanningTime             = false;
    SpanningPlaybackTimeValid   = false;
    SpanningDecodeTimeValid     = false;

    //
    // Check in body
    //

    data   = RemainingData;
    data32 = (unsigned int *)((((int) data) + 3) & ~3);     // int (4 bytes) aligned next address
    n      = ((int) data32) - ((int) data);

    // prologue : start to fill first 4 bytes word from 32 bits unaligned leading bytes
    switch (n)
    {
    default:
        Code2 = 0xFFFFFFFFU;
        break;
    case 1:
        Code2 = 0x00FFFFFFU                      |
                (((unsigned int) data[0]) << 24);
        break;
    case 2:
        Code2 = 0x0000FFFFU                      |
                (((unsigned int) data[0]) << 16) |
                (((unsigned int) data[1]) << 24);
        break;
    case 3:
        Code2 = 0x000000FFU                      |
                (((unsigned int) data[0]) <<  8) |
                (((unsigned int) data[1]) << 16) |
                (((unsigned int) data[2]) << 24);
        break;
    }

    // main search loop on int (4 bytes) basis
    SearchedSynchro = 0x00010000U;  // 3 bytes only synchro ordered in little-endian
    for (i = (((int) RemainingLength) - n) / 4; i > 0; i--, n += 4)
    {
        if (i > 16)
        {
            __builtin_prefetch(data32 + 16);
        }
        Code1 = Code2;
        Code2 = *data32++;
        Code3 = ((Code1 << 8) >> 8);
        if (Code3 <= SearchedSynchro)   // optimization
        {
            if ((Code3 == SearchedSynchro) &&
                (!IsCodeTobeIgnored((unsigned char)(Code1 >> 24))))
            {
                *CodeOffset = n - 4;
                return CollatorNoError;
            }
            if (((Code1 >> 8) == SearchedSynchro) &&
                (!IsCodeTobeIgnored((unsigned char)(Code2 & 0xFFU))))
            {
                *CodeOffset = n - 3;
                return CollatorNoError;
            }
            if (((((Code2 << 24) >> 8) | (Code1 >> 16)) == SearchedSynchro) &&
                (!IsCodeTobeIgnored((unsigned char)((Code2 >> 8) & 0xFFU))))
            {
                *CodeOffset = n - 2;
                return CollatorNoError;
            }
        }
        if (((((Code2 << 16) >> 8) | (Code1 >> 24)) == SearchedSynchro) &&
            (!IsCodeTobeIgnored((unsigned char)((Code2 >> 16) & 0xFFU))))
        {
            *CodeOffset = n - 1;
            return CollatorNoError;
        }
    }

    // epilogue : search synchro in trailing bytes
    data = (unsigned char *) data32;
    switch (((int) RemainingLength) - n)
    {
    case 0:
        if ((((Code2 << 8) >> 8) == SearchedSynchro) &&
            (!IsCodeTobeIgnored((unsigned char)(Code2 >> 24))))
        {
            *CodeOffset = n - 4;
            return CollatorNoError;
        }
        break;
    case 1:
        if ((((Code2 << 8) >> 8) == SearchedSynchro) &&
            (!IsCodeTobeIgnored((unsigned char)(Code2 >> 24))))
        {
            *CodeOffset = n - 4;
            return CollatorNoError;
        }
        Code1 = (unsigned int) data[0];
        if (((Code2 >> 8) == SearchedSynchro) &&
            (!IsCodeTobeIgnored((unsigned char) Code1)))
        {
            *CodeOffset = n - 3;
            return CollatorNoError;
        }
        break;
    case 2:
        if ((((Code2 << 8) >> 8) == SearchedSynchro) &&
            (!IsCodeTobeIgnored((unsigned char)(Code2 >> 24))))
        {
            *CodeOffset = n - 4;
            return CollatorNoError;
        }
        Code1 = (unsigned int) data[0];
        if (((Code2 >> 8) == SearchedSynchro) &&
            (!IsCodeTobeIgnored((unsigned char) Code1)))
        {
            *CodeOffset = n - 3;
            return CollatorNoError;
        }
        Code2 = (Code2 >> 16) | (Code1 << 16);
        Code1 = (unsigned int) data[1];
        if ((Code2 == SearchedSynchro) &&
            (!IsCodeTobeIgnored((unsigned char) Code1)))
        {
            *CodeOffset = n - 2;
            return CollatorNoError;
        }
        break;
    case 3:
        if ((((Code2 << 8) >> 8) == SearchedSynchro) &&
            (!IsCodeTobeIgnored((unsigned char)(Code2 >> 24))))
        {
            *CodeOffset = n - 4;
            return CollatorNoError;
        }
        Code1 = (unsigned int) data[0];
        if (((Code2 >> 8) == SearchedSynchro) &&
            (!IsCodeTobeIgnored((unsigned char) Code1)))
        {
            *CodeOffset = n - 3;
            return CollatorNoError;
        }
        Code2 = (Code2 >> 16) | (Code1 << 16);
        Code1 = (unsigned int) data[1];
        if ((Code2 == SearchedSynchro) &&
            (!IsCodeTobeIgnored((unsigned char) Code1)))
        {
            *CodeOffset = n - 2;
            return CollatorNoError;
        }
        Code2 = (Code2 >> 8) | (Code1 << 16);
        Code1 = (unsigned int) data[2];
        if ((Code2 == SearchedSynchro) &&
            (!IsCodeTobeIgnored((unsigned char) Code1)))
        {
            *CodeOffset = n - 1;
            return CollatorNoError;
        }
        break;
    }

    //
    // No matches
    //
    return CollatorError;
}


// /////////////////////////////////////////////////////////////////////////
//
//      Protected - Find the next start code (apart from any one at offset 0)
//

#define MarkerBits( n, v )  if( Bits.Get((n)) != (v) )                                        \
                                {                                                             \
                                    SE_ERROR( "Invalid marker bits value %d\n", __LINE__ );   \
                                    SE_DEBUG_DUMP_HEX(StoredPesHeader, 32, 16, NULL );        \
                                    PlaybackTimeValid           = SpanningPlaybackTimeValid;  \
                                    PlaybackTime                = SpanningPlaybackTime;       \
                                    DecodeTimeValid             = SpanningDecodeTimeValid;    \
                                    DecodeTime                  = SpanningDecodeTime;         \
                                    SpanningPlaybackTimeValid   = false;                      \
                                    SpanningDecodeTimeValid = false;                          \
                                    return CollatorError;                                     \
                                }

#define MarkerBit( v )      MarkerBits( 1, v )


CollatorStatus_t   Collator_Pes_c::ReadPesHeader()
{
    // Just check before we begin, that the stream identifier is the one we are interested in

    // In case if some stream does not play due to the below CollatorError then analyze the stream and add its StreamIdentifierCode.
    if (((StoredPesHeader[3] & Configuration.StreamIdentifierMask) != Configuration.StreamIdentifierCode) &&
        (StoredPesHeader[3] != PES_START_CODE_EXTENDED_STREAM_ID) &&
        ((StoredPesHeader[3] & PES_START_CODE_AUDIO_MASK) != PES_START_CODE_AUDIO) &&
        (StoredPesHeader[3] != PES_START_CODE_PRIVATE_STREAM_1) &&
        (StoredPesHeader[3] != PES_START_CODE_MPEG4FLEXMUX_STREAM_ID))
    {
        SE_ERROR("Wrong stream identifier %02x (%02x %02x) \n",
                 StoredPesHeader[3], Configuration.StreamIdentifierCode, Configuration.StreamIdentifierMask);

        return CollatorError;
    }

    //
    // Here we save the current pts state for use only in any
    // picture start code that spans this pes packet header.
    // Also make sure that SpanningPlaybackTime updated by a valid playback time,
    // otherwise if Picture Start Code spans more than two pes packet headers then
    // we may loose a valid PTS.
    if (PlaybackTimeValid)
    {
        SpanningPlaybackTimeValid   = PlaybackTimeValid;
        SpanningPlaybackTime        = PlaybackTime;
        SpanningDecodeTimeValid     = DecodeTimeValid;
        SpanningDecodeTime          = DecodeTime;
        UseSpanningTime             = true;
    }

    // We have 'consumed' the old values by transferring them to the spanning values.
    PlaybackTimeValid           = false;
    DecodeTimeValid             = false;
    //
    // Read the length of the payload (which for video packets within transport stream may be zero)
    //
    PesPacketLength = (StoredPesHeader[4] << 8) + StoredPesHeader[5];

    if (PesPacketLength)
    {
        PesPayloadLength = PesPacketLength - StoredPesHeader[8] - 3 - Configuration.ExtendedHeaderLength;
    }
    else
    {
        PesPayloadLength = 0;
    }

    SE_DEBUG(GetGroupTrace(), "PesPacketLength %d; PesPayloadLength %d\n", PesPacketLength, PesPayloadLength);

    //
    // Bits 0xc0 of byte 6 determine PES or system stream, for PES they are always 0x80,
    // for system stream they are never 0x80 (may be a number of other values).
    //

    if ((StoredPesHeader[6] & 0xc0) == 0x80)
    {
        Bits.SetPointer(StoredPesHeader + 9);           // Set bits pointer ready to process optional fields

        //
        // Check not scrambled
        //

        if ((StoredPesHeader[6] & 0x20) == 0x20)
        {
            SE_ERROR("Packet scrambled\n");
            PlaybackTimeValid           = SpanningPlaybackTimeValid;                // Pop the spanning values back into the main ones
            PlaybackTime                = SpanningPlaybackTime;
            DecodeTimeValid             = SpanningDecodeTimeValid;
            DecodeTime                  = SpanningDecodeTime;
            SpanningPlaybackTimeValid   = false;
            SpanningDecodeTimeValid = false;
            return CollatorError;
        }

        //
        // Commence header parsing, moved initialization of bits class here,
        // because code has been added to parse the other header fields, and
        // this assumes that the bits pointer has been initialized.
        //

        if ((StoredPesHeader[7] & 0x80) == 0x80)
        {
            //
            // Read the PTS
            //
#if 0
            MarkerBits(4, (StoredPesHeader[7] >> 6));
#else
            // Older version of user software give invalid value for these 4 bits so we don't check them
            Bits.FlushUnseen(4);
#endif
            PlaybackTime         = (unsigned long long)(Bits.Get(3)) << 30;
            MarkerBit(1);
            PlaybackTime        |= Bits.Get(15) << 15;
            MarkerBit(1);
            PlaybackTime        |= Bits.Get(15);
            MarkerBit(1);
            PlaybackTimeValid    = true;

            if (mFoundReqTimeControlData)
            {
                mFoundReqTimeControlData = false;
                mReqTimeMarker.mReqTimeMarkerData.mPts = PlaybackTime;

                CollatorStatus_t Status = mCollatorSink->InsertMarkerFrameToOutputPort(mReqTimeMarker);
                if (Status != CollatorNoError)
                {
                    SE_WARNING("Insert of Req Time marker failed\n");
                    return Status;
                }
            }
        }

        if ((StoredPesHeader[7] & 0xC0) == 0xC0)
        {
            //
            // Read the DTS
            //
#if 0
            MarkerBits(4, 1);
#else
            // the TV.Berlin station (at least) transmits this field incorrectly so I have removed the check the 4 bit value
            Bits.FlushUnseen(4);
#endif
            DecodeTime           = (unsigned long long)(Bits.Get(3)) << 30;
            MarkerBit(1);
            DecodeTime          |= Bits.Get(15) << 15;
            MarkerBit(1);
            DecodeTime          |= Bits.Get(15);
            MarkerBit(1);
            DecodeTimeValid      = true;
        }
        else if ((StoredPesHeader[7] & 0xC0) == 0x40)
        {
            SE_ERROR("Malformed pes header contains DTS without PTS\n");
        }

        // The following code aims at verifying if the Pes packet sub_stream_id is the one required by the collator...
        // Also this checks if encoding type is mpeg4p2 then we check the private data flag
        if (((StoredPesHeader[3]) == PES_START_CODE_EXTENDED_STREAM_ID) || ((StoredPesHeader[3] & 0xE0) == 0xC0) || (StoredPesHeader[3] == 0xBD) || (IsMpeg4p2Stream))
        {
            // skip the escr data  if any
            if ((StoredPesHeader[7] & 0x20) == 0x20)
            {
                Bits.FlushUnseen(48);
            }

            // skip the es_rate data if any
            if ((StoredPesHeader[7] & 0x10) == 0x10)
            {
                Bits.FlushUnseen(24);
            }

            // skip the dsm trick mode data if any
            if ((StoredPesHeader[7] & 0x8U) == 0x8U)
            {
                Bits.FlushUnseen(8);
            }

            // skip the additional_copy_info data data if any
            if ((StoredPesHeader[7] & 0x4) == 0x4)
            {
                Bits.FlushUnseen(8);
            }

            // skip the pes_crc data data if any
            if ((StoredPesHeader[7] & 0x2) == 0x2)
            {
                Bits.FlushUnseen(16);
            }

            // handle the pes_extension
            if ((StoredPesHeader[7] & 0x1) == 0x1)
            {
                int PesPrivateFlag = Bits.Get(1);
                int PackHeaderFieldFlag = Bits.Get(1);
                int PrgCounterFlag = Bits.Get(1);
                int PstdFlag = Bits.Get(1);
                Bits.FlushUnseen(3);
                int PesExtensionFlag2 = Bits.Get(1);

                if (PesPrivateFlag)
                {
                    if (IsMpeg4p2Stream)
                    {
                        CodedFrameParameters.IsMpeg4p2MetaDataPresent = Bits.Get(8);
                        Bits.FlushUnseen(120);
                    }
                    else
                    {
                        unsigned long long ADTextTag;
                        Bits.FlushUnseen(8);
                        //unsigned int ADDesLength = Bits.Get(4);
                        ADTextTag = (unsigned long long)(Bits.Get(32)) << 8 ;
                        ADTextTag |=   Bits.Get(8);
                        AudioDescriptionInfo.ADInfoAvailable = true;

                        if (ADTextTag != 0x4454474144ll)
                        {
                            Bits.FlushUnseen(48);
                            AudioDescriptionInfo.ADValidFlag = false;
                        }
                        else
                        {
                            unsigned int RevisionTextTag =  Bits.Get(8);
                            AudioDescriptionInfo.ADFadeValue = Bits.Get(8);
                            AudioDescriptionInfo.ADPanValue = Bits.Get(8);
                            AudioDescriptionInfo.ADValidFlag = true;

                            if (RevisionTextTag == 0x32)
                            {
                                AudioDescriptionInfo.ADGainCenter = Bits.Get(8);
                                AudioDescriptionInfo.ADGainFront = Bits.Get(8);
                                AudioDescriptionInfo.ADGainSurround = Bits.Get(8);
                            }
                            else if (RevisionTextTag == 0x31)
                            {
                                Bits.FlushUnseen(24);
                            }
                            else
                            {
                                AudioDescriptionInfo.ADValidFlag = false;
                                Bits.FlushUnseen(24);
                            }
                        }

                        Bits.FlushUnseen(32);
                    }
                }
                else
                {
                    AudioDescriptionInfo.ADInfoAvailable = false;
                }

                if ((AudioDescriptionInfo.ADInfoAvailable == true) && (AudioDescriptionInfo.ADValidFlag == false))
                {
                    AudioDescriptionInfo.ADFadeValue = 0;
                    AudioDescriptionInfo.ADPanValue  = 0;
                    AudioDescriptionInfo.ADGainCenter = 0;
                    AudioDescriptionInfo.ADGainFront = 0;
                    AudioDescriptionInfo.ADGainSurround = 0;
                }

                Bits.FlushUnseen((PackHeaderFieldFlag ? 8 : 0) + (PrgCounterFlag ? 16 : 0) + (PstdFlag ? 16 : 0));

                if (PesExtensionFlag2)
                {
                    Bits.FlushUnseen(8);
                    int StreamIdExtFlag = Bits.Get(1);

                    if (!StreamIdExtFlag)
                    {
                        int SubStreamId = Bits.Get(7); //stream_id_extension

                        if (!inrange((SubStreamId & Configuration.SubStreamIdentifierMask), Configuration.SubStreamIdentifierCodeStart, Configuration.SubStreamIdentifierCodeStop))
                        {
                            SE_INFO(GetGroupTrace(), "Rejected Pes packet of type extended_stream_id with SubStreamId: %x\n", SubStreamId);
                            // Get rid of this packet !
                            PlaybackTimeValid           = SpanningPlaybackTimeValid;              // Pop the spanning values back into the main ones
                            PlaybackTime                = SpanningPlaybackTime;
                            DecodeTimeValid             = SpanningDecodeTimeValid;
                            DecodeTime                  = SpanningDecodeTime;
                            SpanningPlaybackTimeValid = false;
                            SpanningDecodeTimeValid   = false;
                            return CollatorError;
                        }
                    }
                }
            }
        }
    }
    //
    // Alternatively read a system stream
    //
    else
    {
        Bits.SetPointer(StoredPesHeader + 6);

        while (Bits.Show(8) == 0xff)
        {
            Bits.Flush(8);
        }

        if (Bits.Show(2) == 0x01)
        {
            Bits.Flush(2);
            Bits.FlushUnseen(1);                // STD scale
            Bits.FlushUnseen(13);               // STD buffer size
        }

        unsigned int Flags = Bits.Get(4);

        if ((Flags == 0x02) || (Flags == 0x03))
        {
            PlaybackTime         = (unsigned long long)(Bits.Get(3)) << 30;
            MarkerBit(1);
            PlaybackTime        |= Bits.Get(15) << 15;
            MarkerBit(1);
            PlaybackTime        |= Bits.Get(15);
            MarkerBit(1);
            PlaybackTimeValid    = true;
        }

        if (Flags == 0x03)
        {
            MarkerBits(4, 1);
            DecodeTime           = (unsigned long long)(Bits.Get(3)) << 30;
            MarkerBit(1);
            DecodeTime          |= Bits.Get(15) << 15;
            MarkerBit(1);
            DecodeTime          |= Bits.Get(15);
            MarkerBit(1);
            DecodeTimeValid      = true;
        }
    }

    //
    // We either save it to be loaded at the next buffer, or if we
    // have not started picture aquisition we use it immediately.
    //

    if (AccumulatedDataSize == 0)
    {
        if (false == CodedFrameParameters.PlaybackTimeValid)
        {
            CodedFrameParameters.PlaybackTimeValid = PlaybackTimeValid;
            CodedFrameParameters.PlaybackTime      = PlaybackTime;
        }
        PlaybackTimeValid                       = false;
        if (false == CodedFrameParameters.DecodeTimeValid)
        {
            CodedFrameParameters.DecodeTimeValid   = DecodeTimeValid;
            CodedFrameParameters.DecodeTime        = DecodeTime;
        }
        DecodeTimeValid                         = false;
    }

    return CollatorNoError;
}
