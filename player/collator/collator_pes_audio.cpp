/************************************************************************
Copyright (C) 2003-2014 STMicroelectronics. All Rights Reserved.

This file is part of the Streaming Engine.

Streaming Engine is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License version 2 as
published by the Free Software Foundation.

Streaming Engine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with Streaming Engine; see the file COPYING.  If not, write to the Free
Software Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307,
USA.

The Streaming Engine Library may alternatively be licensed under a
proprietary license from ST.
************************************************************************/

// /////////////////////////////////////////////////////////////////////////
/// \class Collator_PesAudio_c
///
/// Specialized PES collator implementing audio specific features.
///
/// None of the common audio standards use MPEG start codes to demark
/// frame boundaries. Instead, once the stream has been de-packetized
/// we must perform a limited form of frame analysis to discover the
/// frame boundaries.
///
/// This class provides a framework on which to base that frame
/// analysis. Basically this class provides most of the machinary to
/// manage buffers but leaves the sub-class to implement
/// Collator_PesAudio_c::ScanForSyncWord
/// and Collator_PesAudio_c::GetFrameLength.
///
/// \todo Currently this class does not honour PES padding (causing
///       accumulated data to be spuriously discarded when playing back
///       padded streams).
///

#include "collator_pes_audio.h"

////////////////////////////////////////////////////////////////////////////
/// \fn CollatorStatus_t Collator_PesAudio_c::FindNextSyncWord( int *CodeOffset )
///
/// Scan the input until an appropriate synchronization sequence is found.
///
/// If the encoding format does not have any useable synchronization sequence
/// this method should set CodeOffset to zero and return CollatorNoError.
///
/// In addition to scanning the input found in Collator_PesAudio_c::RemainingElementaryData
/// this method should also examine Collator_PesAudio_c::PotentialFrameHeader to see
/// if there is a synchronization sequence that spans blocks. If such a sequence
/// if found this is indicated by providing a negative offset.
///
/// \return Collator status code, CollatorNoError indicates success.
///

////////////////////////////////////////////////////////////////////////////
/// \fn CollatorStatus_t Collator_PesAudio_c::DecideCollatorNextStateAndGetLength( unsigned int *FrameLength )
///
/// Examine the frame header pointed to by Collator_PesAudioEAc3_c::StoredFrameHeader
/// and determine how many bytes should be accumulated. Additionally this method
/// sets the collator state.
///
/// For trivial bitstreams where the header identifies the exact length of the frame
/// (e.g. MPEG audio) the state is typically set to GotCompleteFrame to indicate that
/// the previously accumulated frame should be emitted.
///
/// More complex bitstreams may require secondary analysis (by a later call to
/// this method) and these typically use either the ReadSubFrame state if they want to
/// accumulate data prior to subsequent analysis or the SkipSubFrame state to discard
/// data prior to subsequent analysis.
///
/// Note that even for trivial bitstreams the collator must use the ReadSubFrame state
/// for the first frame after a glitch since there is nothing to emit.
///
/// If the encoding format does not have any useable synchronization sequence
/// and therefore no easy means to calculate the frame length then an arbitary
/// chunk size should be selected. The choice of chunk size will depend on the
/// level of compresion achieved by the encoder but in general 1024 bytes would
/// seem appropriate for all but PCM data.
///
/// \return Collator status code, CollatorNoError indicates success.
///

////////////////////////////////////////////////////////////////////////////
///
///
Collator_PesAudio_c::Collator_PesAudio_c(unsigned int frameheaderlength)
    : PotentialFrameHeader()
    , CollatorState(SeekingSyncWord)
    , PassPesPrivateDataToElementaryStreamHandler(true)
    , DiscardPesPacket(false)
    , ReprocessAccumulatedDataDuringErrorRecovery(true)
    , AlreadyHandlingMissingNextFrameHeader(false)
    , PotentialFrameHeaderLength(0)
    , StoredFrameHeader(NULL)
    , RemainingElementaryLength(0)
    , RemainingElementaryData(NULL)
    , RemainingElementaryOrigin(NULL)
    , FrameHeaderLength(frameheaderlength)
    , Framecount(0)
    , PesPayloadRemaining(0)
    , TrailingStartCodeBytes(0)
    , GotPartialFrameHeaderBytes(0)
    , FramePayloadRemaining(0)
    , AccumulatedFrameReady(false)
    , NextPlaybackTimeValid(false)
    , NextPlaybackTime(0)
    , NextDecodeTimeValid(false)
    , NextDecodeTime(0)
    , NextPesADInfo()
{
    SetGroupTrace(group_collator_audio);
}

// /////////////////////////////////////////////////////////////////////////
//
// (Protected) The accumulate data into the buffer function
//
CollatorStatus_t   Collator_PesAudio_c::AccumulateData(unsigned int              Length,
                                                       unsigned char            *Data)
{
    CollatorStatus_t status = Collator_Base_c::AccumulateData(Length, Data);

    SE_DEBUG(group_collator_audio, ">> %d bytes from %p [Total %d]\n", Length, Data, AccumulatedDataSize);
    return status;
}



////////////////////////////////////////////////////////////////////////////
///
/// Scan the input until an appropriate synchronization sequence is found.
///
/// Scans any ::RemainingElementaryData searching for a
/// synchronization sequence using ::FindNextSyncWord,
/// a pure virtual method provided by sub-classes.
///
/// \return Collator status code, CollatorNoError indicates success.
///
CollatorStatus_t Collator_PesAudio_c::SearchForSyncWord()
{
    CollatorStatus_t Status;
    int CodeOffset;

    SE_DEBUG(group_collator_audio, ">><<\n");
    Status = FindNextSyncWord(&CodeOffset);

    if (Status == CollatorNoError)
    {
        SE_DEBUG(group_collator_audio, "Tentatively locked to synchronization sequence (at %d)\n", CodeOffset);
        // switch state
        CollatorState = GotSynchronized;
        GotPartialFrameHeaderBytes = 0;

        if (CodeOffset >= 0)
        {
            // discard any data leading up to the start code
            RemainingElementaryData += CodeOffset;
            RemainingElementaryLength -= CodeOffset;
        }
        else
        {
            SE_DEBUG(group_collator_audio, "Synchronization sequence spans multiple PES packets\n");
            // accumulate any data from the old block
            Status = AccumulateData(-1 * CodeOffset,
                                    PotentialFrameHeader + PotentialFrameHeaderLength + CodeOffset);

            if (Status != CollatorNoError)
            {
                SE_DEBUG(group_collator_audio, "Cannot accumulate data #1 (%d)\n", Status);
            }

            GotPartialFrameHeaderBytes += (-1 * CodeOffset);
        }

        PotentialFrameHeaderLength = 0;
    }
    else
    {
        // copy the last few bytes of the frame into PotentialFrameHeader (so that FindNextSyncWord
        // can return a negative CodeOffset if the synchronization sequence spans blocks)
        if (RemainingElementaryLength < FrameHeaderLength)
        {
            if (PotentialFrameHeaderLength + RemainingElementaryLength > FrameHeaderLength)
            {
                // shuffle the existing potential frame header downwards
                unsigned int BytesToKeep = FrameHeaderLength - RemainingElementaryLength;
                unsigned int ShuffleBy = PotentialFrameHeaderLength - BytesToKeep;
                memmove(PotentialFrameHeader, PotentialFrameHeader + ShuffleBy, BytesToKeep);
                PotentialFrameHeaderLength = BytesToKeep;
            }

            memcpy(PotentialFrameHeader + PotentialFrameHeaderLength,
                   RemainingElementaryData,
                   RemainingElementaryLength);
            PotentialFrameHeaderLength += RemainingElementaryLength;
        }
        else
        {
            memcpy(PotentialFrameHeader,
                   RemainingElementaryData + RemainingElementaryLength - FrameHeaderLength,
                   FrameHeaderLength);
            PotentialFrameHeaderLength = FrameHeaderLength;
        }

        SE_DEBUG(group_collator_audio, "Discarded %d bytes while searching for synchronization sequence\n",
                 RemainingElementaryLength);
        RemainingElementaryLength = 0;
        // we have contained the 'error' and don't want to propagate it
        Status = CollatorNoError;
    }

    return Status;
}

////////////////////////////////////////////////////////////////////////////
///
/// Accumulate incoming data until we have the parseable frame header.
///
/// Accumulate data until we have ::FrameHeaderLength
/// bytes stashed away. At this point there is sufficient data accumulated
/// to determine how many bytes will pass us by before the next frame header
/// is expected.
///
/// \return Collator status code, CollatorNoError indicates success.
///
CollatorStatus_t Collator_PesAudio_c::ReadPartialFrameHeader()
{
    CollatorStatus_t Status;
    unsigned int BytesNeeded, BytesToRead, FrameLength;
    unsigned int OldPartialFrameHeaderBytes, OldAccumulatedDataSize;
    CollatorState_t OldCollatorState;
    unsigned int FramePayLoadBytes = 0;
    //
    SE_DEBUG(group_collator_audio, ">><<\n");
    OldPartialFrameHeaderBytes = GotPartialFrameHeaderBytes;
    OldAccumulatedDataSize = AccumulatedDataSize;
    //
    if (GotPartialFrameHeaderBytes > FrameHeaderLength)
    {
        BytesNeeded = 0;
        BytesToRead = 0;
        FramePayLoadBytes = GotPartialFrameHeaderBytes - FrameHeaderLength;
        Status = CollatorNoError;
    }
    else
    {
        BytesNeeded = FrameHeaderLength - GotPartialFrameHeaderBytes;
        BytesToRead = Min(RemainingElementaryLength, BytesNeeded);
        Status = AccumulateData(BytesToRead, RemainingElementaryData);
    }


    if (Status == CollatorNoError)
    {
        GotPartialFrameHeaderBytes += BytesToRead;
        RemainingElementaryData += BytesToRead;
        RemainingElementaryLength -= BytesToRead;
        SE_DEBUG(group_collator_audio, "BytesNeeded %d; BytesToRead %d\n", BytesNeeded, BytesToRead);

        if (BytesNeeded == BytesToRead)
        {
            //
            // Check for inconsistent class state, dump some diagnostics and
            // attempt error correction.
            //
            if (AccumulatedDataSize < FrameHeaderLength)
            {
                SE_ERROR("Internal error; GotPartialFrameHeaderBytes (%d) inconsistent with AccumulatedDataSize (%d)\n",
                         OldPartialFrameHeaderBytes, OldAccumulatedDataSize);
                // Correct the inconsistancy and give up in this frame
                GotPartialFrameHeaderBytes = 0;
                return HandleMissingNextFrameHeader();
            }

            //
            // We've got the whole header, examine it and change state
            //
            StoredFrameHeader = BufferBase + (AccumulatedDataSize - FrameHeaderLength) - FramePayLoadBytes;
            SE_DEBUG(group_collator_audio, "Got entire frame header packet\n");

            OldCollatorState = CollatorState;
            Status = DecideCollatorNextStateAndGetLength(&FrameLength);

            if (Status != CollatorNoError)
            {
                SE_DEBUG(group_collator_audio, "Badly formed frame header; seeking new frame header\n");
                return HandleMissingNextFrameHeader();
            }

            if (FrameLength == 0)
            {
                // The LPCM collator needs to do this in order to get a frame evicted before
                // accumulating data from the PES private data area into the frame. The only
                // way it can do this is by reporting a zero length frame and updating some
                // internal state variables. On the next call it will report a non-zero value
                // (i.e. we won't loop forever accumulating no data).
                SE_DEBUG(group_collator_audio, "Sub-class reported unlikely (but potentially legitimate) frame length (%d)\n", FrameLength);
            }

            if (FrameLength > MaximumCodedFrameSize)
            {
                SE_ERROR("Sub-class reported absurd frame length (%d)\n", FrameLength);
                return HandleMissingNextFrameHeader();
            }

            // this is the number of bytes we must absorb before switching state to SeekingFrameEnd.
            // if the value is negative then we've already started absorbing the subsequent frame
            // header.
            FramePayloadRemaining = FrameLength - FrameHeaderLength;

            if (CollatorState == GotCompleteFrame)
            {
                AccumulatedFrameReady = true;
                //
                // update the coded frame parameters using the parameters calculated the
                // last time we saw a frame header.
                //
                if (false == CodedFrameParameters.PlaybackTimeValid)
                {
                    CodedFrameParameters.PlaybackTimeValid = NextPlaybackTimeValid;
                    CodedFrameParameters.PlaybackTime = NextPlaybackTime;
                }
                if (false == CodedFrameParameters.DecodeTimeValid)
                {
                    CodedFrameParameters.DecodeTimeValid = NextDecodeTimeValid;
                    CodedFrameParameters.DecodeTime = NextDecodeTime;
                }
                //
                //AD related parameter filled to CodedFrameParameters structure for
                //passing this info to neighbour module(frame parser).
                //
                memcpy(&CodedFrameParameters.ADMetaData, &NextPesADInfo, sizeof(NextPesADInfo));
            }
            else if (CollatorState == SkipSubFrame)
            {
                /* discard the accumulated frame header */
                AccumulatedDataSize -= FrameHeaderLength;
            }

            if (CollatorState == GotCompleteFrame || OldCollatorState == GotSynchronized)
            {
                //
                // at this point we have discovered a frame header and need to attach a time to it.
                // we can choose between the normal stamp (the stamp of the current PES packet) or the
                // spanning stamp (the stamp of the previous PES packet). Basically if we have accumulated
                // a greater number of bytes than our current offset into the PES packet then we want to
                // use the spanning time.
                //
                bool WantSpanningTime = GetOffsetIntoPacket() < (int) GotPartialFrameHeaderBytes;

                if (WantSpanningTime || UseSpanningTime)
                {
                    NextPlaybackTimeValid = SpanningPlaybackTimeValid;
                    NextPlaybackTime  = SpanningPlaybackTime;
                    SpanningPlaybackTimeValid = false;
                    NextDecodeTimeValid   = SpanningDecodeTimeValid;
                    NextDecodeTime    = SpanningDecodeTime;
                    SpanningDecodeTimeValid = false;
                    UseSpanningTime       = false;
                }
                else
                {
                    NextPlaybackTimeValid = PlaybackTimeValid;
                    NextPlaybackTime  = PlaybackTime;
                    PlaybackTimeValid = false;
                    NextDecodeTimeValid   = DecodeTimeValid;
                    NextDecodeTime    = DecodeTime;
                    DecodeTimeValid   = false;
                    //Audio Description related value stored to update in CodedFrameParameters
                    NextPesADInfo.NextADFadeValue = AudioDescriptionInfo.ADFadeValue;
                    NextPesADInfo.NextADPanValue = AudioDescriptionInfo.ADPanValue;
                    NextPesADInfo.NextADGainCenter = AudioDescriptionInfo.ADGainCenter ;
                    NextPesADInfo.NextADGainFront = AudioDescriptionInfo.ADGainFront ;
                    NextPesADInfo.NextADGainSurround = AudioDescriptionInfo.ADGainSurround;
                    NextPesADInfo.NextADValidFlag = AudioDescriptionInfo.ADValidFlag;
                    NextPesADInfo.NextADInfoAvailable = AudioDescriptionInfo.ADInfoAvailable;
                }
            }

            // switch states and absorb the packet
            SE_DEBUG(group_collator_audio, "Discovered frame header (frame length %d bytes)\n", FrameLength);
        }
    }
    else
    {
        SE_DEBUG(group_collator_audio, "Cannot accumulate data #3 (%d)\n", Status);
    }

    return Status;
}


////////////////////////////////////////////////////////////////////////////
///
/// Handle losing lock on the frame headers.
///
/// This function is called to handle the data that was spuriously accumulated
/// when the frame header was badly parsed.
///
/// In principle this function is quite simple. We allocate a new accumulation buffer and
/// use the currently accumulated data is the data source to run the elementary stream
/// state machine. There is however a little extra logic to get rid of recursion.
/// Specificially we change the error handling behaviour if this method is re-entered
/// so that there error is reported back to the already executing copy of the method.
///
/// \return Collator status code, CollatorNoError indicates success.
///
CollatorStatus_t Collator_PesAudio_c::HandleMissingNextFrameHeader()
{
    CollatorStatus_t Status;

    //
    // Mark the collator as having lost frame lock.
    // Yes! We really do want to do this before the re-entry checks.
    //
    CollatorState = SeekingSyncWord; // we really do want to do this before the re-entry checks
    AccumulatedFrameReady = false;

    //
    // Check for re-entry
    //

    if (AlreadyHandlingMissingNextFrameHeader)
    {
        SE_DEBUG(group_collator_audio, "Re-entered the error recovery handler, initiating stack unwind\n");
        return CollatorUnwindStack;
    }

    //
    // Check whether the sub-class wants trivial or aggressive error recovery
    //
    mCollatorSink->UpdateStreamStatistics(CollatorAudioElementrySyncLostCount, 1);

    if (!ReprocessAccumulatedDataDuringErrorRecovery)
    {
        DiscardAccumulatedData();
        return CollatorNoError;
    }

    //
    // Remember the original elementary stream pointers for when we return to 'normal' processing.
    //
    unsigned char *OldRemainingElementaryOrigin = RemainingElementaryOrigin;
    unsigned char *OldRemainingElementaryData   = RemainingElementaryData;
    unsigned int OldRemainingElementaryLength   = RemainingElementaryLength;
    //
    // Take ownership of the already accumulated data
    //
    unsigned char *TempBuf          = new unsigned char [AccumulatedDataSize];
    if (TempBuf == NULL)
    {
        SE_ERROR("Failed to allocated %d bytes tempBuffer\n", AccumulatedDataSize);
        return CollatorError;
    }

    unsigned char *ReprocessingData = TempBuf;
    unsigned int ReprocessingDataLength = AccumulatedDataSize;

    memcpy(ReprocessingData, BufferBase, AccumulatedDataSize);

    ResetBufferState();

    //
    // Remember that we are re-processing the previously accumulated elementary stream
    //
    AlreadyHandlingMissingNextFrameHeader = true;

    //
    // WARNING: From this point on we own the ReprocessingDataBuffer, have set the recursion avoidance
    //          marker and may have damaged the RemainingElementaryData pointer. There should be no
    //          short-circuit exit paths used after this point otherwise we risk avoiding the clean up
    //          at the bottom of the method.
    //

    while (ReprocessingDataLength > 1)
    {
        //
        // Remove the first byte from the recovery buffer (to avoid detecting again the same start code).
        //
        ReprocessingData += 1;
        ReprocessingDataLength -= 1;
        //
        // Search for a start code in the reprocessing data. This allows us to throw away data that we
        // know will never need reprocessing which makes the recursion avoidance code more efficient.
        //
        RemainingElementaryOrigin = ReprocessingData;
        RemainingElementaryData   = ReprocessingData;
        RemainingElementaryLength = ReprocessingDataLength;
        PotentialFrameHeaderLength = 0; // ensure no (now voided) historic data is considered by sub-class
        //
        // Process the elementary stream
        //
        Status = HandleElementaryStream(ReprocessingDataLength, ReprocessingData);

        if (CollatorNoError == Status)
        {
            SE_DEBUG(group_collator_audio, "Error recovery completed, returning to normal processing\n");
            // All data consumed and stored in the subsequent accumulation buffer
            break; // Success will propagate when we return Status
        }
        else if (CollatorUnwindStack == Status)
        {
            SE_DEBUG(group_collator_audio, "Stack unwound successfully, re-trying error recovery\n");

            // Double check that we really are acting upon the reprocessing buffer
            if ((AccumulatedDataSize > ReprocessingDataLength) ||
                (ReprocessingData + ReprocessingDataLength !=
                 RemainingElementaryData + RemainingElementaryLength))
            {
                SE_ERROR("Internal failure during error recovery, returning to normal processing\n");
                break; // Failure will propagate when we return Status
            }

            // We found a frame header (or potentially a couple of valid frame)
            // but lost lock again... let's restart from where we got to
            ReprocessingData = RemainingElementaryData - AccumulatedDataSize;
            ReprocessingDataLength = RemainingElementaryLength + AccumulatedDataSize;
            AccumulatedDataSize = 0;
            continue;
        }
        else
        {
            SE_ERROR("Error handling elementary stream during error recovery\n");
            break; // Failure will propagate when we return Status
        }
    }

    //
    // Free the buffer we just consumed and restore the original elementary stream pointers
    //
    RemainingElementaryOrigin = OldRemainingElementaryOrigin;
    RemainingElementaryData   = OldRemainingElementaryData;
    RemainingElementaryLength = OldRemainingElementaryLength;

    AlreadyHandlingMissingNextFrameHeader = false;
    delete [] TempBuf;
    return Status;
}


////////////////////////////////////////////////////////////////////////////
///
/// Accumulate data until we reach the next frame header.
///
/// The function has little or no intelligence. Basically it will squirrel
/// away data until Collator_PesAudio_c::GotPartialFrameHeaderBytes reaches
/// Collator_PesAudio_c::FrameHeaderLength and then set
/// Collator_PesAudio_c::GotPartialFrameHeader.
///
/// \return Collator status code, CollatorNoError indicates success.
///
CollatorStatus_t Collator_PesAudio_c::ReadFrame()
{
    CollatorStatus_t Status;
    unsigned int BytesToRead;
    //
    SE_DEBUG(group_collator_audio, ">><<\n");

    //

    // if the FramePayloadRemaining is -ve then we have no work to do except
    // record the fact that we've already accumulated part of the next frame header
    if (FramePayloadRemaining < 0)
    {
        GotPartialFrameHeaderBytes = -FramePayloadRemaining;
        CollatorState = SeekingFrameEnd;
        return CollatorNoError;
    }

    if (GotPartialFrameHeaderBytes > FrameHeaderLength)
    {
        GotPartialFrameHeaderBytes -= FrameHeaderLength;

        if (GotPartialFrameHeaderBytes < FramePayloadRemaining)
        {
            BytesToRead = FramePayloadRemaining - GotPartialFrameHeaderBytes;
        }
        else
        {
            BytesToRead = 0;
            FramePayloadRemaining = 0;
            GotPartialFrameHeaderBytes -= FramePayloadRemaining;
        }
    }
    else
    {
        // FramePayloadRemaining is postive so no worry about casting
        BytesToRead = Min((unsigned int)FramePayloadRemaining, RemainingElementaryLength);
    }

    if (CollatorState == ReadSubFrame)
    {
        Status = AccumulateData(BytesToRead, RemainingElementaryData);

        if (Status != CollatorNoError)
        {
            SE_DEBUG(group_collator_audio, "Cannot accumulate data #4 (%d)\n", Status);
        }
    }
    else
    {
        Status = CollatorNoError;
    }

    if (Status == CollatorNoError)
    {
        if (BytesToRead - FramePayloadRemaining == 0)
        {
            GotPartialFrameHeaderBytes = 0;
            CollatorState = SeekingFrameEnd;
        }

        RemainingElementaryData += BytesToRead;
        RemainingElementaryLength -= BytesToRead;
        FramePayloadRemaining -= BytesToRead;
    }

    //
    return Status;
}

////////////////////////////////////////////////////////////////////////////
///
/// Handle a block of data that is not frame aligned.
///
/// There may be the end of a frame, whole frames, the start of a frame or even just
/// the middle of the frame.
///
/// If we have incomplete blocks we build up a complete one in the saved data,
/// in order to process we need to acquire a frame plus the next header (for sync check)
/// we can end up with the save buffer having a frame + part of a header, and a secondary
/// save with just part of a header.
/// And when we have nothing to parse, depending upon "NoLookAheadBeforeSeekingFrameEnd"
/// loop for 1 more time to deliver collated frame.
///
/// \return Collator status code, CollatorNoError indicates success.
///
CollatorStatus_t Collator_PesAudio_c::HandleElementaryStream(unsigned int Length, unsigned char *Data)
{
    CollatorStatus_t Status;

    //

    if (DiscardPesPacket)
    {
        SE_DEBUG(group_collator_audio, "Discarding %d bytes of elementary stream\n", Length);
        return CodecNoError;
    }

    //
    // Copy our arguments to class members so the utility functions can
    // act upon it.
    //
    RemainingElementaryOrigin = Data;
    RemainingElementaryData   = Data;
    RemainingElementaryLength = Length;
    //
    // Continually handle units of input until all input is exhausted (or an error occurs).
    // When nothing to parse, depending upon "NoLookAheadBeforeSeekingFrameEnd" loop for 1 more time to deliver collated frame.
    // Be aware that our helper functions may, during their execution, cause state changes
    // that result in a different branch being taken next time round the loop.
    //
    Status = CollatorNoError;
    bool ExtraLoopEnable = false;

    while (Status == CollatorNoError && ((RemainingElementaryLength != 0) || ExtraLoopEnable))
    {
        SE_DEBUG(group_collator_audio, "ES loop has %d bytes remaining\n", RemainingElementaryLength);

        switch (CollatorState)
        {
        case SeekingSyncWord:
            //
            // Try to lock to incoming frame headers
            //
            Status = SearchForSyncWord();
            break;

        case GotSynchronized:
        case SeekingFrameEnd:
            //
            // Read in the remains of the frame header
            //
            Status = ReadPartialFrameHeader();
            break;

        case ReadSubFrame:
        case SkipSubFrame:
            //
            // Squirrel away the frame
            //
            Status = ReadFrame();
            break;

        case GotCompleteFrame:
            //
            // Pass the accumulated subframes to the frame parser
            //
            Status = InternalFrameFlush();
            CollatorState = ReadSubFrame;
            break;

        case ValidateFrame:
            //
            // Validate the accumulated frame for internal sync words
            //
            Status = ValidateCollatedFrame();
            break;

        default:
            // should not occur...
            SE_DEBUG(group_collator_audio, "General failure; wrong collator state\n");
            Status = CollatorError;
            break;
        }
        // To achieve low latency, enable the loop when CollatorState is SeekingFrameEnd or GotCompleteFrame and we have nothing to parse.
        if (RemainingElementaryLength == 0)
        {
            ExtraLoopEnable = (((CollatorState == SeekingFrameEnd) || (CollatorState == GotCompleteFrame))
                               && NoLookAheadBeforeSeekingFrameEnd());
        }
    }

    if ((Status != CollatorNoError) && (Status != CollatorUnwindStack))
    {
        if (Status == CollatorInputNotConnected)
        {
            SE_DEBUG(group_collator_audio, "InputNotConnected; Discard RemainingLength=%u data completely\n", RemainingLength);
            DiscardAccumulatedData();
            RemainingLength = 0;
            CollatorState = SeekingSyncWord;
            return Status;
        }

        // if anything when wrong then we need to resynchronize
        SE_DEBUG(group_collator_audio, "General failure; seeking new synchronization sequence\n");
        DiscardAccumulatedData();
        CollatorState = SeekingSyncWord;
    }

    return Status;
}


////////////////////////////////////////////////////////////////////////////
///
/// Scan the input until an appropriate PES start code is found.
///
/// Scans any Collator_Pes_c::RemainingData searching for a PES start code.
/// The configuration for this comes from Collator_Base_c::Configuration and
/// is this means that only the interesting (e.g. PES audio packets) start
/// codes will be detected.
///
/// \return Collator status code, CollatorNoError indicates success.
///
CollatorStatus_t Collator_PesAudio_c::SearchForPesHeader()
{
    CollatorStatus_t Status;
    unsigned int CodeOffset;

//

    //
    // If there are any trailing start codes handle those.
    //

    while (TrailingStartCodeBytes && RemainingLength)
    {
        if (TrailingStartCodeBytes == 3)
        {
            // We've got the 0, 0, 1 so if the code is *not* in the ignore range then we've got one
            unsigned char SpecificCode = RemainingData[0];

            if (!IsCodeTobeIgnored(SpecificCode))
            {
                SE_DEBUG(group_collator_audio, "Found a trailing startcode 00, 00, 01, %x\n", SpecificCode);
                // Consume the specific start code
                RemainingData++;
                RemainingLength--;
                // Switch state (and reflect the data we are about to accumulate)
                SeekingPesHeader = false;
                GotPartialPesHeader = true;
                //assert( AccumulatedDataSize == 0 );
                GotPartialPesHeaderBytes = 4;
                // There are now no trailing start code bytes
                TrailingStartCodeBytes = 0;
                // Finally, accumulate the data (by reconstructing it)
                unsigned char StartCode[4] = { 0, 0, 1, SpecificCode };
                Status = AccumulateData(4, StartCode);

                if (Status != CollatorNoError)
                {
                    SE_DEBUG(group_collator_audio, "Cannot accumulate data #5 (%d)\n", Status);
                }

                return Status;
            }

            // Nope, that's not a suitable start code.
            SE_DEBUG(group_collator_audio, "Trailing start code 00, 00, 01, %x was in the ignore range\n", SpecificCode);
            TrailingStartCodeBytes = 0;
            break;
        }
        else if (TrailingStartCodeBytes == 2)
        {
            // Got two zeros, a one gets us ready to read the code.
            if (RemainingData[0] == 1)
            {
                SE_DEBUG(group_collator_audio, "Trailing start code looks good (found 00, 00; got 01)\n");
                TrailingStartCodeBytes++;
                RemainingData++;
                RemainingLength--;
                continue;
            }

            // Got two zeros, another zero still leaves us with two zeros.
            if (RemainingData[0] == 0)
            {
                SE_DEBUG(group_collator_audio, "Trailing start code looks OK (found 00, 00; got 00)\n");
                RemainingData++;
                RemainingLength--;
                continue;
            }

            // Nope, that's not a suitable start code.
            SE_DEBUG(group_collator_audio, "Trailing 00, 00 was not part of a start code\n");
            TrailingStartCodeBytes = 0;
            break;
        }
        else if (TrailingStartCodeBytes == 1)
        {
            // Got one zero, another zero gives us two (duh).
            if (RemainingData[0] == 0)
            {
                SE_DEBUG(group_collator_audio, "Trailing start code looks good (found 00; got 00)\n");
                TrailingStartCodeBytes++;
                RemainingData++;
                RemainingLength--;
                continue;
            }

            // Nope, that's not a suitable start code.
            SE_DEBUG(group_collator_audio, "Trailing 00 was not part of a start code\n");
            TrailingStartCodeBytes = 0;
            break;
        }
        else
        {
            SE_ERROR("TrailingStartCodeBytes has illegal value: %d\n", TrailingStartCodeBytes);
            TrailingStartCodeBytes = 0;
            return CollatorError;
        }
    }

    if (RemainingLength == 0)
    {
        return CollatorNoError;
    }

    //assert(TrailingStartCodeBytes == 0);
//
    Status = FindNextStartCode(&CodeOffset);

    if (Status == CollatorNoError)
    {
        SE_DEBUG(group_collator_audio, "Locked to PES packet boundaries (offset=%d)\n", CodeOffset);
        // discard any data leading up to the start code
        RemainingData += CodeOffset;
        RemainingLength -= CodeOffset;
        // switch state
        SeekingPesHeader = false;
        GotPartialPesHeader = true;
        GotPartialPesHeaderBytes = 0;
    }
    else
    {
        // examine the end of the buffer to determine if there is a (potential) trailing start code
        //assert( RemainingLength >= 1 );
        if (RemainingData[RemainingLength - 1] <= 1)
        {
            unsigned char LastBytes[3];
            LastBytes[0] = (RemainingLength >= 3 ? RemainingData[RemainingLength - 3] : 0xff);
            LastBytes[1] = (RemainingLength >= 2 ? RemainingData[RemainingLength - 2] : 0xff);
            LastBytes[2] = RemainingData[RemainingLength - 1];

            if (LastBytes[0] == 0 && LastBytes[1] == 0 && LastBytes[2] == 1)
            {
                TrailingStartCodeBytes = 3;
            }
            else if (LastBytes[1] == 0 && LastBytes[2] == 0)
            {
                TrailingStartCodeBytes = 2;
            }
            else if (LastBytes[2] == 0)
            {
                TrailingStartCodeBytes = 1;
            }
        }

        SE_DEBUG(group_collator_audio, "Discarded %d bytes while searching for PES header (%d might be start code)\n",
                 RemainingLength, TrailingStartCodeBytes);
        RemainingLength = 0;
    }

//
    return CollatorNoError;
}

////////////////////////////////////////////////////////////////////////////
///
/// Accumulate incoming data until we have the full PES header.
///
/// Strictly speaking this method handles two sub-states. In the first state
/// we do not have sufficient data accumulated to determine how long the PES
/// header is. In the second we still don't have a complete PES packet but
/// at least we know how much more data we need.
///
/// This code assumes that PES packet uses >=9 bytes PES headers rather than
/// the 6 byte headers found in the program stream map, padding stream,
/// private stream 2, etc.
///
/// \return Collator status code, CollatorNoError indicates success.
///
CollatorStatus_t Collator_PesAudio_c::ReadPartialPesHeader()
{
    CollatorStatus_t Status;
    unsigned int PesHeaderBytes, BytesNeeded, BytesToRead;
    unsigned char PesPrivateData[MAX_PES_PRIVATE_DATA_LENGTH];

//

    if (GotPartialPesHeaderBytes < PES_INITIAL_HEADER_SIZE)
    {
        SE_DEBUG(group_collator_audio, "Waiting for first part of PES header\n");
        StoredPesHeader = BufferBase + AccumulatedDataSize - GotPartialPesHeaderBytes;
        BytesToRead = Min(RemainingLength, PES_INITIAL_HEADER_SIZE - GotPartialPesHeaderBytes);
        Status = AccumulateData(BytesToRead, RemainingData);

        if (Status == CollatorNoError)
        {
            GotPartialPesHeaderBytes += BytesToRead;
            RemainingData += BytesToRead;
            RemainingLength -= BytesToRead;
        }
        else
        {
            SE_DEBUG(group_collator_audio, "Cannot accumulate data #6 (%d)\n", Status);
        }

        return Status;
    }

    //
    // We now have accumulated sufficient data to know how long the PES header actually is!
    //
    // pass the stream_id field to the collator sub-class (might update Configuration.ExtendedHeaderLength)
    SetPesPrivateDataLength(StoredPesHeader[3]);
    PesHeaderBytes = PES_INITIAL_HEADER_SIZE + StoredPesHeader[8] + Configuration.ExtendedHeaderLength;
    BytesNeeded = PesHeaderBytes - GotPartialPesHeaderBytes;
    BytesToRead = Min(RemainingLength, BytesNeeded);
    Status = AccumulateData(BytesToRead, RemainingData);

    if (Status == CollatorNoError)
    {
        GotPartialPesHeaderBytes += BytesToRead;
        RemainingData += BytesToRead;
        RemainingLength -= BytesToRead;
        SE_DEBUG(group_collator_audio, "BytesNeeded %d; BytesToRead %d\n", BytesNeeded, BytesToRead);

        if (BytesNeeded == BytesToRead)
        {
            //
            // We've got the whole header, examine it and change state
            //
            SE_DEBUG(group_collator_audio, "Got entire PES header\n");
            Status = CollatorNoError; // strictly speaking this is a no-op but the code might change

            if (StoredPesHeader[0] != 0x00 || StoredPesHeader[1] != 0x00 || StoredPesHeader[2] != 0x01 ||
                CollatorNoError != (Status = ReadPesHeader()))
            {
                SE_DEBUG(group_collator_audio, "%s; seeking new PES header\n",
                         (Status == CollatorNoError ? "Start code not where expected" :
                          "Badly formed PES header"));
                SeekingPesHeader = true;
                DiscardAccumulatedData();
                mCollatorSink->UpdateStreamStatistics(CollatorAudioPesSyncLostCount, 1);
                // we have contained the error by changing states...
                return CollatorNoError;
            }

            //
            // Placeholder: Generic stream id based PES filtering (configured by sub-class) could be inserted
            //              here (set DiscardPesPacket to true to discard).
            //

            if (Configuration.ExtendedHeaderLength)
            {
                // store a pointer to the PES private header. it is located just above the end of the
                // accumulated data and is will be safely accumulated providing the private header is
                // smaller than the rest of the PES packet. if a very large PES private header is
                // encountered we will need to introduce a temporary buffer to store the header in.
                if (Configuration.ExtendedHeaderLength <= MAX_PES_PRIVATE_DATA_LENGTH)
                {
                    memcpy(PesPrivateData, BufferBase + AccumulatedDataSize - Configuration.ExtendedHeaderLength, Configuration.ExtendedHeaderLength);
                }
                else
                {
                    SE_ERROR("Implementation error: Pes Private data area too big for temporay buffer\n");
                }

                Status = HandlePesPrivateData(PesPrivateData);

                if (Status != CollatorNoError)
                {
                    SE_ERROR("Unhandled error when parsing PES private data\n");
                    return (Status);
                }
            }

            // discard the actual PES packet from the accumulate buffer
            if (AccumulatedDataSize >= PesHeaderBytes)
            {
                AccumulatedDataSize -= PesHeaderBytes;
            }
            else
            {
                SE_ERROR("Implementation Error AccumulatedDataSize Less than the PesHeaderBytes  : AccumulatedDataSize = %d PesHeaderBytes = %d\n", AccumulatedDataSize, PesHeaderBytes);
                SeekingPesHeader = true;
                DiscardAccumulatedData();
                mCollatorSink->UpdateStreamStatistics(CollatorAudioPesSyncLostCount, 1);
                // we have contained the error by changing states...
                return CollatorNoError;
            }

            // record the number of bytes we need to ignore before we reach the next start code
            PesPayloadRemaining = PesPayloadLength;
            // switch states and absorb the packet
            SE_DEBUG(group_collator_audio, "Discovered PES packet (header %d bytes, payload %d bytes)\n",
                     PesPacketLength - PesPayloadLength + 6, PesPayloadLength);
            GotPartialPesHeader = false;

            if (PassPesPrivateDataToElementaryStreamHandler && Configuration.ExtendedHeaderLength)
            {
                // update PesPacketLength (to ensure that GetOffsetIntoPacket gives the correct value)
                PesPayloadLength += Configuration.ExtendedHeaderLength;
                Status = HandleElementaryStream(Configuration.ExtendedHeaderLength, PesPrivateData);

                if (Status != CollatorNoError)
                {
                    SE_ERROR("Failed not accumulate the PES private data area\n");
                }
            }
        }
    }
    else
    {
        SE_DEBUG(group_collator_audio, "Cannot accumulate data #7 (%d)\n", Status);
    }

    return Status;
}

////////////////////////////////////////////////////////////////////////////
///
/// Pass data to the elementary stream handler until the PES payload is consumed.
///
/// When all data has been consumed then switch back to the GotPartialPesHeader
/// state since (if the stream is correctly formed) the next three bytes will
/// contain the PES start code.
///
/// \return Collator status code, CollatorNoError indicates success.
///
CollatorStatus_t Collator_PesAudio_c::ReadPesPacket()
{
    unsigned int BytesToRead = Min(PesPayloadRemaining, RemainingLength);
    CollatorStatus_t Status = HandleElementaryStream(BytesToRead, RemainingData);
    if (Status == CollatorNoError)
    {
        if (BytesToRead == PesPayloadRemaining)
        {
            GotPartialPesHeader = true;
            GotPartialPesHeaderBytes = 0;
        }

        RemainingData += BytesToRead;
        RemainingLength -= BytesToRead;
        PesPayloadRemaining -= BytesToRead;
    }
    else if (Status == BufferGetBufferRejected)
    {
        SE_DEBUG(group_collator_audio, "HandleElementaryStream failed Status=%d\n", Status);
        return CollatorInputNotConnected;
    }

    return CollatorNoError;
}

////////////////////////////////////////////////////////////////////////////
///
/// Reset the elementary stream state machine.
///
/// If we are forced to emit a frame the elementary stream state machine (and any
/// control variables) must be reset.
///
/// This method exists primarily for sub-classes to hook. For most sub-classes
/// the trivial reset logic below is sufficient but those that maintain state
/// (especially those handling PES private data) will need to work harder.
///
void Collator_PesAudio_c::ResetCollatorStateAfterForcedFrameFlush()
{
    // we have been forced to emit the frame against our will (e.g. someone other that the collator has
    // caused the data to be emitted). we therefore have to start looking for a new sync word. don't worry
    // if the external geezer got it right this will be right in front of our nose.
    CollatorState = SeekingSyncWord;
}

CollatorStatus_t   Collator_PesAudio_c::InputSecondStage(unsigned int        DataLength,
                                                         const void         *Data)
{
    CollatorStatus_t    Status;

    //
    // Copy our arguments to class members so the utility functions can
    // act upon it.
    //
    RemainingData   = (unsigned char *)Data;
    RemainingLength = DataLength;
    //
    // Continually handle units of input until all input is exhausted (or an error occurs).
    //
    // Be aware that our helper functions may, during their execution, cause state changes
    // that result in a different branch being taken next time round the loop.
    //
    Status = CollatorNoError;

    while (Status == CollatorNoError &&  RemainingLength != 0)
    {
        // Check if not in low power state
        if (mCollatorSink->PlaybackIsInLowPowerState())
        {
            // Stop processing data to speed-up low power enter procedure (bug 24248)
            break;
        }

        SE_VERBOSE(group_collator_audio, "De-PESing loop has %d bytes remaining\n", RemainingLength);
        if (SeekingPesHeader)
        {
            //
            // Try to lock to incoming PES headers
            //
            Status = SearchForPesHeader();
        }
        else if (GotPartialPesHeader)
        {
            //
            // Read in the remains of the PES header
            //
            Status = ReadPartialPesHeader();
        }
        else
        {
            //
            // Send the PES packet for frame level analysis
            //
            Status = ReadPesPacket();
        }
    }

    if (Status != CollatorNoError)
    {
        // if anything when wrong then we need to resynchronize
        SE_DEBUG(group_collator_audio, "General failure; seeking new PES header\n");
        DiscardAccumulatedData();
        SeekingPesHeader = true;
    }

    return Status;
}

////////////////////////////////////////////////////////////////////////////
///
/// Pass accumulated data to the output ring and maintain class state variables.
///
/// \todo If we were more clever about buffer management we wouldn't have to
///       copy the frame header onto the stack.
///
/// \return Collator status code, CollatorNoError indicates success.
///
CollatorStatus_t   Collator_PesAudio_c::InternalFrameFlush()
{
    CollatorStatus_t    Status;
    unsigned char       CopiedFrameHeader[MAX_FRAME_HEADER_LENGTH];

    SE_DEBUG(group_collator_audio, ">><<\n");

    // temporarily copy the following frame header (if there is one) to the stack
    if (AccumulatedFrameReady)
    {
        memcpy(CopiedFrameHeader, StoredFrameHeader, FrameHeaderLength);
        AccumulatedDataSize -= FrameHeaderLength;
        //Assert( BufferBase + AccumulatedDataLength == StoredFrameHeader );
    }

    // now pass the complete frame onward
    if ((!AccumulatedFrameReady) && (GotPartialFrameHeaderBytes != 0))  // Remove Partial header if any during drain
    {
        DiscardAccumulatedData();
    }

    Status                  = Collator_Pes_c::InternalFrameFlush();

    if (Status != CodecNoError)
    {
        return Status;
    }

    Framecount++;

    if (AccumulatedFrameReady)
    {
        // put the stored frame header into the new buffer
        Status = AccumulateData(FrameHeaderLength, CopiedFrameHeader);

        if (Status != CollatorNoError)
        {
            SE_DEBUG(group_collator_audio, "Cannot accumulate data #8 (%d)\n", Status);
        }

        AccumulatedFrameReady = false;
    }
    else
    {
        ResetCollatorStateAfterForcedFrameFlush();
    }

    return CodecNoError;
}

////////////////////////////////////////////////////////////////////////////
///
/// Call the super-class DiscardAccumulatedData and make sure that AccumlatedFrameReady is false.
///
/// \return Collator status code, CollatorNoError indicates success.
///
CollatorStatus_t   Collator_PesAudio_c::DiscardAccumulatedData()
{
    CollatorStatus_t Status;
    Status = Collator_Pes_c::DiscardAccumulatedData();
    AccumulatedFrameReady = false;
    GotPartialFrameHeaderBytes = 0;
    return Status;
}

////////////////////////////////////////////////////////////////////////////
///
/// Examine the PES private data header.
///
/// This is a stub implementation for collators that don't have a private
/// data area.
///
/// Collators that do must re-implement this method in order to extract any
/// useful data from it.
///
/// For collators that may, or may not, have a PES private data area this
/// method must also be responsible for determining which type of stream
/// is being played. See Collator_PesAudioEAc3_c::HandlePesPrivateData()
/// for more an example of such a collator.
///
/// \return Collator status code, CollatorNoError indicates success.
///
CollatorStatus_t Collator_PesAudio_c::HandlePesPrivateData(unsigned char *PesPrivateData)
{
    (void)PesPrivateData; // warning removal
    return CollatorNoError;
}

////////////////////////////////////////////////////////////////////////////
///
/// Validate the sanity of data in case of issue in frame size.
///
/// Assuming that the start has a sync word. we try to search a
/// new sync word after this
///
/// If we get a sync word we squirel of the frame and check for validity
/// of the next sync word found and check for any new frame.
/// We do not reenter any resync check to avoid any recursion
///
/// \return Collator status code, CollatorNoError indicates success.
///
CollatorStatus_t Collator_PesAudio_c::ValidateCollatedFrame()
{
    CollatorStatus_t Status;

    int CodeOffset = 0;
    SE_DEBUG(group_collator_audio, ">><<\n");

    if (AlreadyHandlingMissingNextFrameHeader)
    {
        SE_INFO(group_collator_audio, "Re-entered the error recovery handler, initiating stack unwind\n");
        return CollatorUnwindStack;
    }

    //
    // Remember the original elementary stream pointers for when we return to 'normal' processing.
    //
    unsigned char *OldRemainingElementaryOrigin = RemainingElementaryOrigin;
    unsigned char *OldRemainingElementaryData   = RemainingElementaryData;
    unsigned int OldRemainingElementaryLength   = RemainingElementaryLength;
    //
    // Take ownership of the already accumulated data
    //
    unsigned char *TempBuf          = new unsigned char [AccumulatedDataSize];
    if (TempBuf == NULL)
    {
        SE_ERROR("Failed to allocated %d bytes tempBuffer\n", AccumulatedDataSize);
        return CollatorError;
    }

    unsigned char *ReprocessingData = TempBuf;
    unsigned char *ReprocessingDataOrigin = ReprocessingData;

    unsigned int ReprocessingDataLength   = AccumulatedDataSize;

    memcpy(ReprocessingData, BufferBase, AccumulatedDataSize);

    ResetBufferState();

    //
    // Remember that we are re-processing the previously accumulated elementary stream
    //
    AlreadyHandlingMissingNextFrameHeader = true;
    //
    // WARNING: From this point on we own the ReprocessingDataBuffer
    //
    // Remove the first byte from the recovery buffer (to avoid detecting again the same start code).
    //
    ReprocessingData += 1;
    ReprocessingDataLength -= 1;
    //
    // Search for a start code in the reprocessing data after the first valid start code.
    // We will not drop any data as we will deliver the data upto the found sync word.
    //
    RemainingElementaryOrigin = ReprocessingData;
    RemainingElementaryData   = ReprocessingData;
    RemainingElementaryLength = ReprocessingDataLength;
    PotentialFrameHeaderLength = 0; // ensure no (now voided) historic data is considered by sub-class
    Status = FindNextSyncWord(&CodeOffset);

    if (Status == CodecNoError)
    {
        SE_ASSERT(CodeOffset >= 0);
        SE_DEBUG(group_collator_audio, "Found start code during preparser buffer (byte %d of %d)\n", CodeOffset, ReprocessingDataLength);
        // We found a start code, Deliver data upto this in this point.
        // to to that we need to copy the rest of the data to the
    }
    else
    {
        // We didn't find a start code, so send any way the whole of the data
        // snip off the last few bytes. This
        // final fragment may contain a partial start code so we want to pass if through the
        // elementary stream handler again.
        // we send anyway if we get start code or not.
        SE_DEBUG(group_collator_audio, "Found no start code during validate recovery (processing final %d )\n",
                 ReprocessingDataLength);
        CodeOffset = ReprocessingDataLength;
    }

    // we will deliver in any case.
    AccumulatedFrameReady = true;
    SE_DEBUG(group_collator_audio, "Validate : AccumulateData  Size %d, Src: %p\n", (CodeOffset + 1) , ReprocessingDataOrigin);
    Status = AccumulateData((CodeOffset + 1),  ReprocessingDataOrigin);

    if (Status != CollatorNoError)
    {
        SE_DEBUG(group_collator_audio, "Validate : cannot accumulate data (%d)\n", Status);
        delete [] TempBuf;
        return Status;
    }

    StoredFrameHeader = BufferBase + (AccumulatedDataSize - FrameHeaderLength);
    //
    // update the coded frame parameters using the parameters calculated the
    // last time we saw a frame header.
    //
    if (false == CodedFrameParameters.PlaybackTimeValid)
    {
        CodedFrameParameters.PlaybackTimeValid = NextPlaybackTimeValid;
        CodedFrameParameters.PlaybackTime      = NextPlaybackTime;
    }
    if (false == CodedFrameParameters.DecodeTimeValid)
    {
        CodedFrameParameters.DecodeTimeValid   = NextDecodeTimeValid;
        CodedFrameParameters.DecodeTime        = NextDecodeTime;
    }

    Status = InternalFrameFlush();
    if (Status != CollatorNoError) { return Status; }

    CollatorState = GotSynchronized;
    ReprocessingData += CodeOffset;
    ReprocessingDataLength -= CodeOffset;
    unsigned int FinalBytes = Min((unsigned int)CodeOffset, FrameHeaderLength - 1);
    ReprocessingDataLength += FinalBytes;
    ReprocessingData -= FinalBytes;

    //
    // Process the elementary stream
    //
    while (ReprocessingDataLength > 1)
    {
        //SE_INFO(group_collator_audio, "DataLength: %d, Data 0x%x\n",ReprocessingDataLength, ReprocessingData);
        Status = HandleElementaryStream(ReprocessingDataLength, ReprocessingData);

        if (CollatorNoError == Status)
        {
            SE_DEBUG(group_collator_audio, " Validate : Error recovery completed, returning to normal processing\n");
            // All data consumed and stored in the subsequent accumulation buffer
            break; // Success will propagate when we return Status
        }
        else if (CollatorUnwindStack == Status)
        {
            SE_DEBUG(group_collator_audio, "Validate :Stack unwound successfully, re-trying error recovery\n");

            // We found a frame header but lost lock again... let's have another go
            // Double check that we really are acting upon the reprocessing buffer
            if ((AccumulatedDataSize > ReprocessingDataLength) ||
                (ReprocessingData + ReprocessingDataLength !=
                 RemainingElementaryData + RemainingElementaryLength))
            {
                SE_ERROR("Internal failure during error recovery, returning to normal processing\n");
                break; // Failure will propagate when we return Status
            }

            // We found a frame header (or potentially a couple of valid frame)
            // but lost lock again... let's restart from the next byte from where we got to
            ReprocessingData       = RemainingElementaryData - (AccumulatedDataSize - 1);
            ReprocessingDataLength = RemainingElementaryLength + (AccumulatedDataSize - 1);
            AccumulatedDataSize    = 0; // make sure no accumulated data is carried round the loop
            RemainingElementaryOrigin = ReprocessingData;
            RemainingElementaryData   = ReprocessingData;
            RemainingElementaryLength = ReprocessingDataLength;
            continue;
        }
        else
        {
            SE_INFO(group_collator_audio, "Error handling elementary stream during error recovery\n");
            break; // Failure will propagate when we return Status
        }
    }

    //
    // Free the buffer we just consumed and restore the original elementary stream pointers
    //
    RemainingElementaryOrigin = OldRemainingElementaryOrigin;
    RemainingElementaryData   = OldRemainingElementaryData;
    RemainingElementaryLength = OldRemainingElementaryLength;

    AlreadyHandlingMissingNextFrameHeader = false;
    delete [] TempBuf;
    return Status;
}
