/************************************************************************
Copyright (C) 2003-2014 STMicroelectronics. All Rights Reserved.

This file is part of the Streaming Engine.

Streaming Engine is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License version 2 as
published by the Free Software Foundation.

Streaming Engine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with Streaming Engine; see the file COPYING.  If not, write to the Free
Software Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307,
USA.

The Streaming Engine Library may alternatively be licensed under a
proprietary license from ST.
************************************************************************/

////////////////////////////////////////////////////////////////////////////
/// \class Collator_PesAudioPcm_c
///
/// Implements PCM audio sync word scanning and frame length analysis.
///

#include "collator_pes_audio_pcm.h"
#define PCM_HEADER_SIZE 0

////////////////////////////////////////////////////////////////////////////
///
/// Initialize the class by resetting it.
///
Collator_PesAudioPcm_c::Collator_PesAudioPcm_c()
    : Collator_PesAudio_c(PCM_HEADER_SIZE)
{
    Configuration.StreamIdentifierMask             = PES_START_CODE_MASK_8BIT;  // Mask used for Stream ID
    Configuration.StreamIdentifierCode             = PES_START_CODE_PRIVATE_STREAM_1;
    Configuration.BlockTerminateMask               = 0xff;         // Picture
    Configuration.BlockTerminateCode               = 0x00;
    Configuration.IgnoreCodesRanges.NbEntries      = 0;
    Configuration.IgnoreCodesRanges.Table[0].Start = 0x01; // All slice codes
    Configuration.IgnoreCodesRanges.Table[0].End   = PES_START_CODE_AUDIO - 1;
    Configuration.InsertFrameTerminateCode         = false;
    Configuration.TerminalCode                     = 0;
    Configuration.ExtendedHeaderLength             = 0;
    Configuration.DeferredTerminateFlag            = false;
}

////////////////////////////////////////////////////////////////////////////
///
/// Search for the audio synchonrization word and, if found, report its offset.
///
/// The PCM has no sync. We must search for 14 consecutive
/// set bits (starting on a byte boundary).
///
/// Weak start codes are, in fact, the primary reason we have
/// to verify the header of the subsequent frame before emitting the preceding one.
///
/// \return Collator status code, CollatorNoError indicates success.
///
CollatorStatus_t Collator_PesAudioPcm_c::FindNextSyncWord(int *CodeOffset)
{
    // There is no sync on pcm, so we are always sync'ed!....
    *CodeOffset = 0;
    return CollatorNoError;
}

////////////////////////////////////////////////////////////////////////////
///
/// Determine the new state of the collator according to the incoming sub frame
/// Also returns this sub frame length
///
/// \return Collator status code, CollatorNoError indicates success.
///
CollatorStatus_t Collator_PesAudioPcm_c::DecideCollatorNextStateAndGetLength(unsigned int *FrameLength)
{
    *FrameLength = PesPayloadLength;//Next start code will be the length
    CollatorState = GotCompleteFrame;
    return CollatorNoError;
}


