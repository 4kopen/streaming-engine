/************************************************************************
Copyright (C) 2003-2014 STMicroelectronics. All Rights Reserved.

This file is part of the Streaming Engine.

Streaming Engine is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License version 2 as
published by the Free Software Foundation.

Streaming Engine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with Streaming Engine; see the file COPYING.  If not, write to the Free
Software Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307,
USA.

The Streaming Engine Library may alternatively be licensed under a
proprietary license from ST.
************************************************************************/

////////////////////////////////////////////////////////////////////////////
/// \class Collator_PesVideoH264_c
///
/// Implements initialisation of collator video class for H264
///

#include "h264.h"
#include "collator_pes_video_h264.h"

static const int MAX_NAL_SLICE_HEADER_SIZE_FIRST    = 1355;
static const int MAX_NAL_SLICE_HEADER_SIZE_OTHERS   = 10;

////////////////////////////////////////////////////////////////////////////
///
/// Initialize the class by resetting it.
///
Collator_PesVideoH264_c::Collator_PesVideoH264_c()
    : mSeenProbableReversalPoint(false)
    , mIsLastPartitionSei(false)
{
    Configuration.GenerateStartCodeList            = true;
    Configuration.MaxStartCodes                    = 300;  // If someone inserts 32 SPS and 256 PPS
    Configuration.RequireFrameHeaders              = true;
    Configuration.MaxHeaderSize                    = H264_MAX_HEADER_SIZE;
    Configuration.StreamIdentifierMask             = PES_START_CODE_MASK;
    Configuration.StreamIdentifierCode             = PES_START_CODE_VIDEO;
    Configuration.BlockTerminateMask               = 0x9b; // Slice normal or IDR
    Configuration.BlockTerminateCode               = 0x01;
    Configuration.IgnoreCodesRanges.NbEntries      = 0;
    Configuration.IgnoreCodesRanges.Table[0].Start = 0xff; // Ignore nothing
    Configuration.IgnoreCodesRanges.Table[0].End   = 0x00;
    Configuration.InsertFrameTerminateCode         = true; // Insert a filler data code, to guarantee thatNo terminal code
    Configuration.TerminalCode                     = 0x0C; // picture parameter sets will always be followed by a zero byte (makes MoreRsbpData implementation simpler)
    Configuration.ExtendedHeaderLength             = 0;
    Configuration.DeferredTerminateFlag            = false;
    Configuration.DetermineFrameBoundariesByPresentationToFrameParser = true;
}

unsigned int   Collator_PesVideoH264_c::RequiredPresentationLength(unsigned char StartCode)
{
    unsigned char   NalUnitType = (StartCode & 0x1f);
    unsigned int    ExtraBytes  = 0;

    if ((NalUnitType == NALU_TYPE_IDR) ||
        (NalUnitType == NALU_TYPE_AUX_SLICE) ||
        (NalUnitType == NALU_TYPE_SLICE))
    {
        ExtraBytes    = 1;
    }

    return ExtraBytes;
}

// /////////////////////////////////////////////////////////////////////////
//
//      The present collated header function
//
CollatorStatus_t   Collator_PesVideoH264_c::PresentCollatedHeader(
    unsigned char         StartCode,
    unsigned char        *HeaderBytes,
    FrameParserHeaderFlag_t  *Flags)
{
    unsigned char   NalUnitType = (StartCode & 0x1f);
    bool        AllowIResync;
    bool        Slice;
    bool        FirstSlice;
    bool        Iframe;

    *Flags  = 0;

    AllowIResync = (mCollatorSink->GetStreamPolicy(PolicyH264AllowNonIDRResynchronization) == PolicyValueApply);

    Slice       = (NalUnitType == NALU_TYPE_IDR) || (NalUnitType == NALU_TYPE_AUX_SLICE) || (NalUnitType == NALU_TYPE_SLICE);
    FirstSlice  = Slice && ((HeaderBytes[0] & 0x80) != 0);
    Iframe      = Slice && ((HeaderBytes[0] == 0x88) || ((HeaderBytes[0] & 0xf0) == 0xB0)); // H264_SLICE_TYPE_I_ALL or H264_SLICE_TYPE_I

    if (mSeenProbableReversalPoint && (Slice || (NalUnitType == NALU_TYPE_SPS)))
    {
        *Flags              |= FrameParserHeaderFlagConfirmReversiblePoint;

        if (NalUnitType == NALU_TYPE_SPS)
        {
            *Flags            |= FrameParserHeaderFlagPossibleReversiblePoint | FrameParserHeaderFlagPartitionPoint;    // Make this the reversible point
        }

        mSeenProbableReversalPoint    = false;
    }

    if ((NalUnitType == NALU_TYPE_IDR) || (AllowIResync && Iframe))
    {
        *Flags              |= FrameParserHeaderFlagPossibleReversiblePoint;
        mSeenProbableReversalPoint    = true;
    }

    // With partition only on FirstSlice, the UserData in SEI NALU of next frame can be incorrectly attached to the previous frame.
    // So partition on SEI NALU is needed, so that userdata is attached to the correct frame.
    // Partition on first SEI NALU and not the consecutive one, until VCL NALU FirstSlice is received
    if (!mIsLastPartitionSei && (NalUnitType == NALU_TYPE_SEI))
    {
        *Flags    |= FrameParserHeaderFlagPartitionPoint;
        mIsLastPartitionSei = true;
    }

    // Partition point is the First Slice, SEI or SPS NALU
    if (FirstSlice || (NalUnitType == NALU_TYPE_SPS))
    {
        *Flags    |= FrameParserHeaderFlagPartitionPoint;
        mIsLastPartitionSei = false;
    }

    return CollatorNoError;
}

CollatorStatus_t Collator_PesVideoH264_c::FillFrameHeaders()
{
    CollatorStatus_t status;

    // Process StartCodes in CodedFrameBuffer
    for (int i = 0; i < StartCodeList->NumberOfStartCodes; i++)
    {
        PackedStartCode_t   startCode = StartCodeList->StartCodes[i];
        unsigned char      *header = BufferBase + ExtractStartCodeOffset(startCode) + START_CODE_SIZE;
        unsigned int        unitLength, nal_unit_type, headerSize;


        // Calculate the user data length when its the last StartCode or an intermediate
        if (i == StartCodeList->NumberOfStartCodes - 1)
        {
            unitLength = AccumulatedDataSize - ExtractStartCodeOffset(startCode) - START_CODE_SIZE;
        }
        else
        {
            unitLength = ExtractStartCodeOffset(StartCodeList->StartCodes[i + 1]) - ExtractStartCodeOffset(startCode) - START_CODE_SIZE;
        }

        // Process the Header
        nal_unit_type = ExtractStartCodeCode(startCode) & 0x1f;
        switch (nal_unit_type)
        {
        case NALU_TYPE_IDR:
        case NALU_TYPE_AUX_SLICE:
        case NALU_TYPE_SLICE:
            headerSize = ReadNalSliceHeader(header);
            status = PackHeader(nal_unit_type, header, min(headerSize, unitLength));
            if (status != CollatorNoError) { return status; }
            break;

        case NALU_TYPE_SEI:
        case NALU_TYPE_SPS:
        case NALU_TYPE_SPS_EXT:
        case NALU_TYPE_PLAYER2_CONTAINER_PARAMETERS:
            status = PackHeader(nal_unit_type, header, unitLength);
            if (status != CollatorNoError) { return status; }
            break;

        case NALU_TYPE_PPS:
            // Extra byte is required due to way MoreRsbpData is implemented in frame parser
            status = PackHeader(nal_unit_type, header, unitLength + 1);
            if (status != CollatorNoError) { return status; }
            break;

        default:
            status = PackHeader(nal_unit_type, header, 0);
            if (status != CollatorNoError) { return status; }
            break;
        }
    }

    return CollatorNoError;
}

int Collator_PesVideoH264_c::ReadNalSliceHeader(unsigned char *header)
{
    BitStreamClass_c HeaderBits;
    int headerSize = 0;

    HeaderBits.SetPointer(header);

    // Check the first macroblock in slice field,
    // return if this is not the first slice in a picture.
    unsigned int first_mb_in_slice = HeaderBits.GetUe();

    if (first_mb_in_slice != 0)
    {
        headerSize = MAX_NAL_SLICE_HEADER_SIZE_OTHERS;
    }
    else
    {
        headerSize = MAX_NAL_SLICE_HEADER_SIZE_FIRST;
    }

    return headerSize;
}

