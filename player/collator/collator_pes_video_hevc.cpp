/************************************************************************
Copyright (C) 2003-2014 STMicroelectronics. All Rights Reserved.

This file is part of the Streaming Engine.

Streaming Engine is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License version 2 as
published by the Free Software Foundation.

Streaming Engine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with Streaming Engine; see the file COPYING.  If not, write to the Free
Software Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307,
USA.

The Streaming Engine Library may alternatively be licensed under a
proprietary license from ST.
************************************************************************/

////////////////////////////////////////////////////////////////////////////
/// \class Collator_PesVideoHevc_c
///
/// Implements initialisation of collator video class for Hevc
///

#include "hevc.h"
#include "collator_pes_video_hevc.h"

#undef  TRACE_TAG
#define TRACE_TAG "Collator_PesVideoHevc_c"

#define IsNalUnitSlice(NalUnitType) ((NalUnitType == NAL_UNIT_CODED_SLICE_TRAIL_N) ||   \
                     (NalUnitType == NAL_UNIT_CODED_SLICE_TRAIL_R ) ||   \
                     (NalUnitType == NAL_UNIT_CODED_SLICE_TSA_N) ||  \
                     (NalUnitType == NAL_UNIT_CODED_SLICE_TLA_R)   ||   \
                     (NalUnitType == NAL_UNIT_CODED_SLICE_STSA_N)  ||   \
                     (NalUnitType == NAL_UNIT_CODED_SLICE_STSA_R) || \
                     (NalUnitType == NAL_UNIT_CODED_SLICE_RADL_N) || \
                     (NalUnitType == NAL_UNIT_CODED_SLICE_RADL_R)  ||   \
                     (NalUnitType == NAL_UNIT_CODED_SLICE_RASL_N)  ||   \
                     (NalUnitType == NAL_UNIT_CODED_SLICE_RASL_R)  ||   \
                     (NalUnitType == NAL_UNIT_CODED_SLICE_BLA_W_LP)   ||    \
                     (NalUnitType == NAL_UNIT_CODED_SLICE_BLA_W_RADL) ||    \
                     (NalUnitType == NAL_UNIT_CODED_SLICE_BLA_N_LP)   ||    \
                     (NalUnitType == NAL_UNIT_CODED_SLICE_IDR_W_RADL) ||    \
                     (NalUnitType == NAL_UNIT_CODED_SLICE_IDR_N_LP) ||  \
                     (NalUnitType == NAL_UNIT_CODED_SLICE_CRA)      ||  \
                     (NalUnitType == NAL_UNIT_RESERVED_IRAP_VCL22)  ||  \
                     (NalUnitType == NAL_UNIT_RESERVED_IRAP_VCL23))


static const int MAX_NAL_SLICE_HEADER_SIZE_FIRST    = 1342;
static const int MAX_NAL_SLICE_HEADER_SIZE_OTHERS   = 10;

//
Collator_PesVideoHevc_c::Collator_PesVideoHevc_c()
    : SeenProbableReversalPoint(false)
{
    Configuration.GenerateStartCodeList            = true;
    Configuration.MaxStartCodes                    = 310;                 // If someone inserts 16 VPS, 32 SPS and 256 PPS
    Configuration.RequireFrameHeaders              = true;
    Configuration.MaxHeaderSize                    = HEVC_MAX_HEADER_SIZE_FOR_UHD; // Default max header size (for Auto mem profile)
    Configuration.StreamIdentifierMask             = PES_START_CODE_MASK;
    Configuration.StreamIdentifierCode             = PES_START_CODE_VIDEO;

    Configuration.IgnoreCodesRanges.NbEntries      = 0;
    Configuration.IgnoreCodesRanges.Table[0].Start = 0xff; // Ignore nothing
    Configuration.IgnoreCodesRanges.Table[0].End   = 0x00;
    /* We won't insert any terminate SC */
    /* In H264, a filler data code is inserted, to guarantee that no terminal */
    /* code picture parameter sets will always be followed by a zero byte */
    /* (makes the MoreRsbpData implementation a lot simpler).*/
    /* Not sure this trick is needed for HEVC */
    Configuration.InsertFrameTerminateCode         = false;
    Configuration.TerminalCode                     = 38 << 1;
    Configuration.ExtendedHeaderLength             = 0;
    Configuration.DetermineFrameBoundariesByPresentationToFrameParser = true;
}

unsigned int Collator_PesVideoHevc_c::RequiredPresentationLength(unsigned char StartCode)
{
    unsigned char   NalUnitType = ((StartCode >> 1) & 0x3f);
    unsigned int    ExtraBytes  = 0;

    // Needed to determine if slice is first slice
    if (IsNalUnitSlice(NalUnitType))
    {
        ExtraBytes = 2;
    }

    return ExtraBytes;
}

// -----------------------

CollatorStatus_t   Collator_PesVideoHevc_c::PresentCollatedHeader(
    unsigned char StartCode, unsigned char *HeaderBytes,
    FrameParserHeaderFlag_t  *Flags)
{
    unsigned char NalUnitType = ((StartCode >> 1) & 0x3f);
    bool        Slice;
    bool        FirstSlice;

    *Flags = 0;

    Slice = IsNalUnitSlice(NalUnitType);
    // NAL Header is two-byte long and HeaderBytes starts one byte after end of start code
    // first_slice_segment_in_pic_flag is Msb of 2nd byte following the startcode
    FirstSlice = Slice && ((HeaderBytes[1] & 0x80) != 0);

//TODO CL : check NAL_UNIT_SPS VPS are HEVC entry point as good as SPS was for H264 ????
    if (SeenProbableReversalPoint && (Slice || (NalUnitType == NAL_UNIT_VPS) || (NalUnitType == NAL_UNIT_SPS)))
    {
        *Flags |= FrameParserHeaderFlagConfirmReversiblePoint;

        if (NalUnitType == NAL_UNIT_SPS)
            *Flags |= FrameParserHeaderFlagPossibleReversiblePoint
                      | FrameParserHeaderFlagPartitionPoint; // Make this the reversible point

        SeenProbableReversalPoint    = false;
    }

// TODO CL : this condition may be reworked too, not yet sure IDR, CRA & BLA are the good and/or only ones...
    if ((NalUnitType == NAL_UNIT_CODED_SLICE_IDR_W_RADL) ||
        (NalUnitType == NAL_UNIT_CODED_SLICE_IDR_N_LP) ||
        (NalUnitType == NAL_UNIT_CODED_SLICE_CRA) ||
        (NalUnitType == NAL_UNIT_CODED_SLICE_BLA_W_LP) ||
        (NalUnitType == NAL_UNIT_CODED_SLICE_BLA_W_RADL) ||
        (NalUnitType == NAL_UNIT_CODED_SLICE_BLA_N_LP))
    {
        *Flags |= FrameParserHeaderFlagPossibleReversiblePoint;
        SeenProbableReversalPoint    = true;
    }

    // Partition point is the First Slice or VPS NALU
    if (FirstSlice || (NalUnitType == NAL_UNIT_VPS))
    {
        *Flags    |= FrameParserHeaderFlagPartitionPoint;
    }

    return CollatorNoError;
}

CollatorStatus_t Collator_PesVideoHevc_c::FillFrameHeaders()
{
    CollatorStatus_t status;

    // Process StartCodes in CodedFrameBuffer
    for (int i = 0; i < StartCodeList->NumberOfStartCodes; i++)
    {
        PackedStartCode_t   startCode = StartCodeList->StartCodes[i];
        unsigned char      *header = BufferBase + ExtractStartCodeOffset(startCode) + HEVC_START_CODE_SIZE;
        unsigned int        unitLength;
        unsigned int        nal_unit_type;
        unsigned int        headerSize;

        // Calculate the user data length when its the last StartCode or an intermediate
        if (i == StartCodeList->NumberOfStartCodes - 1)
        {
            unitLength = AccumulatedDataSize - ExtractStartCodeOffset(startCode) - HEVC_START_CODE_SIZE;
        }
        else
        {
            unitLength = ExtractStartCodeOffset(StartCodeList->StartCodes[i + 1]) -
                         ExtractStartCodeOffset(startCode) - HEVC_START_CODE_SIZE;
        }

        // Process the Header
        nal_unit_type = ((ExtractStartCodeCode(startCode) >> 1) & 0x3f);
        if (IsNalUnitSlice(nal_unit_type))
        {
            headerSize = ReadNalSliceHeader(header + 1);
            status = PackHeader(nal_unit_type, header, min(headerSize, unitLength));
            if (status != CollatorNoError) { return status; }
        }
        else
        {
            switch (nal_unit_type)
            {
            case NAL_UNIT_VPS:
            case NAL_UNIT_SPS:
            case NAL_UNIT_PPS:
            case NAL_UNIT_PREFIX_SEI:
            case NAL_UNIT_SUFFIX_SEI:
                // for SEI: it is needed to copy a little bit more data
                // i.e. fake data inserted in FrameHeader buffer (unitlength) must not
                // be interpreted/parsed (see ProcessSEI function) !
                status = PackHeader(nal_unit_type, header, unitLength + HEVC_PARAM_EXTRA_DATA);
                if (status != CollatorNoError) { return status; }
                break;
            case NAL_UNIT_ACCESS_UNIT_DELIMITER:
            case NAL_UNIT_FILLER_DATA:
                status = PackHeader(nal_unit_type, header, unitLength);
                if (status != CollatorNoError) { return status; }
                break;
            default:
                status = PackHeader(nal_unit_type, header, 0);
                if (status != CollatorNoError) { return status; }
                break;
            }
        }
    }

    return CollatorNoError;
}

int Collator_PesVideoHevc_c::ReadNalSliceHeader(unsigned char *header)
{
    int headerSize = 0;
    unsigned int first_slice_segment_in_pic_flag = (header[1] & 0x80);

    headerSize = (first_slice_segment_in_pic_flag != 0) ?
                 MAX_NAL_SLICE_HEADER_SIZE_FIRST : MAX_NAL_SLICE_HEADER_SIZE_OTHERS;

    return headerSize;
}

