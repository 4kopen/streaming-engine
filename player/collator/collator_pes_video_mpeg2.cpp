/************************************************************************
Copyright (C) 2003-2014 STMicroelectronics. All Rights Reserved.

This file is part of the Streaming Engine.

Streaming Engine is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License version 2 as
published by the Free Software Foundation.

Streaming Engine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with Streaming Engine; see the file COPYING.  If not, write to the Free
Software Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307,
USA.

The Streaming Engine Library may alternatively be licensed under a
proprietary license from ST.
************************************************************************/

////////////////////////////////////////////////////////////////////////////
/// \class Collator_PesVideoMpeg2_c
///
/// Implements initialisation of collator video class for mpeg2
///

#include "mpeg2.h"
#include "collator_pes_video_mpeg2.h"

///
Collator_PesVideoMpeg2_c::Collator_PesVideoMpeg2_c()
{
    Configuration.GenerateStartCodeList          = true;
    Configuration.MaxStartCodes                  = 256;
    Configuration.RequireFrameHeaders            = true;
    Configuration.MaxHeaderSize                  = 32 * 1024;
    Configuration.StreamIdentifierMask           = PES_START_CODE_MASK;
    Configuration.StreamIdentifierCode           = PES_START_CODE_VIDEO;
    Configuration.BlockTerminateMask             = 0xff;                            // Picture
    Configuration.BlockTerminateCode             = 0x00;
    Configuration.IgnoreCodesRanges.NbEntries      = 1;
    Configuration.IgnoreCodesRanges.Table[0].Start = MPEG2_FIRST_SLICE_START_CODE + 1; // Slice codes other than first
    Configuration.IgnoreCodesRanges.Table[0].End   = MPEG2_GREATEST_SLICE_START_CODE;
    Configuration.InsertFrameTerminateCode       = true;                            // Force the mme decode to terminate after a picture
    Configuration.TerminalCode                   = MPEG2_SEQUENCE_END_CODE;
    Configuration.ExtendedHeaderLength           = 0;
    Configuration.DeferredTerminateFlag          = false;
    Configuration.StreamTerminateFlushesFrame    = true;     // Use an end of sequence to force a frame flush
    Configuration.StreamTerminationCode          = MPEG2_SEQUENCE_END_CODE;
    Configuration.DetermineFrameBoundariesByPresentationToFrameParser = true;  // Frame boundaries are determined by PresentCollatedHeader
}

// /////////////////////////////////////////////////////////////////////////
//
//      The present collated header function
//
CollatorStatus_t Collator_PesVideoMpeg2_c::PresentCollatedHeader(
    unsigned char StartCode,
    unsigned char *HeaderBytes,
    FrameParserHeaderFlag_t *Flags)
{
    (void)HeaderBytes; // warning removal

    *Flags = 0;
    // Collator will partition frame on Picture, Sequence, GOP and Sequence end Start Code and pass it on to frame parser.
    // Finer split at Sequence, GOP was required as user data might come with them.
    bool StartOfFrame = (StartCode == MPEG2_PICTURE_START_CODE);
    bool StartofSeq = (StartCode == MPEG2_SEQUENCE_HEADER_CODE);
    bool StartofGop = (StartCode == MPEG2_GROUP_START_CODE);
    bool SeqEndCode = (StartCode == MPEG2_SEQUENCE_END_CODE); // StreamTerminationCode

    if (StartOfFrame || StartofSeq || StartofGop || SeqEndCode)
    {
        *Flags |= FrameParserHeaderFlagPartitionPoint;
    }

    return CollatorNoError;
}

CollatorStatus_t Collator_PesVideoMpeg2_c::FillFrameHeaders()
{
    CollatorStatus_t status;

    // Process StartCodes in CodedFrameBuffer
    for (int i = 0; i < StartCodeList->NumberOfStartCodes; i++)
    {
        PackedStartCode_t   startCode = StartCodeList->StartCodes[i];
        unsigned char      *header = BufferBase + ExtractStartCodeOffset(startCode) + START_CODE_SIZE;
        unsigned int        headerSize;

        // Calculate the user data length when its the last StartCode or an intermediate
        if (i == StartCodeList->NumberOfStartCodes - 1)
        {
            headerSize = AccumulatedDataSize - ExtractStartCodeOffset(startCode) - START_CODE_SIZE;
        }
        else
        {
            headerSize = ExtractStartCodeOffset(StartCodeList->StartCodes[i + 1]) - ExtractStartCodeOffset(startCode) - START_CODE_SIZE;
        }

        int startCodeCode = ExtractStartCodeCode(startCode);
        switch (startCodeCode)
        {
        case MPEG2_PICTURE_START_CODE:
        case MPEG2_USER_DATA_START_CODE:
        case MPEG2_SEQUENCE_HEADER_CODE:
        case MPEG2_EXTENSION_START_CODE:
        case MPEG2_GROUP_START_CODE:
            status = PackHeader(startCodeCode, header, headerSize);
            if (status != CollatorNoError) { return status; }
            break;

        default:
            // Designate a header size 0 for SC not processed.
            status = PackHeader(startCodeCode, header, 0);
            if (status != CollatorNoError) { return status; }
            break;
        }
    }

    return CollatorNoError;
}

