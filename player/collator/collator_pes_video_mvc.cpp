/************************************************************************
Copyright (C) 2003-2014 STMicroelectronics. All Rights Reserved.

This file is part of the Streaming Engine.

Streaming Engine is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License version 2 as
published by the Free Software Foundation.

Streaming Engine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with Streaming Engine; see the file COPYING.  If not, write to the Free
Software Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307,
USA.

The Streaming Engine Library may alternatively be licensed under a
proprietary license from ST.
************************************************************************/

////////////////////////////////////////////////////////////////////////////
/// \class Collator_PesVideoMVC_c
///
/// Implements initialisation of collator video class for MVC
/// Note: MVC collating is assisted by the Frame Parser (detects first slice of base picture)
///

#include "collator_pes_video_mvc.h"
#include "h264.h"
#include "mvc.h"

#undef  TRACE_TAG
#define TRACE_TAG "Collator_PesVideoMVC_c"

//
Collator_PesVideoMVC_c::Collator_PesVideoMVC_c()
    : SeenProbableReversalPoint(false)
{
    Configuration.GenerateStartCodeList      = true;
    Configuration.MaxStartCodes              = 1000;  // 32 SPS + 32 subset-SPS + 256 PPS + (340 slices x 2 views)
    Configuration.StreamIdentifierMask             = PES_START_CODE_MASK;
    Configuration.StreamIdentifierCode             = PES_START_CODE_VIDEO;
    Configuration.BlockTerminateMask               = 0x9b; // Slice normal or IDR
    Configuration.BlockTerminateCode               = 0x01;
    Configuration.IgnoreCodesRanges.NbEntries      = 0;
    Configuration.IgnoreCodesRanges.Table[0].Start = 0xff; // Ignore nothing
    Configuration.IgnoreCodesRanges.Table[0].End   = 0x00;
    Configuration.InsertFrameTerminateCode         = true; // Insert a filler data code, to guarantee thatNo terminal code
    Configuration.TerminalCode                     = 0x0C; // picture parameter sets will always be followed by a zero byte (makes MoreRsbpData implementation simpler)
    Configuration.ExtendedHeaderLength             = 0;
    Configuration.DeferredTerminateFlag            = false;
    Configuration.DetermineFrameBoundariesByPresentationToFrameParser = true;
}

unsigned int   Collator_PesVideoMVC_c::RequiredPresentationLength(unsigned char StartCode)
{
    unsigned char   NalUnitType = (StartCode & 0x1f);
    unsigned int    ExtraBytes  = 0;

    if ((NalUnitType == NALU_TYPE_IDR) ||
        (NalUnitType == NALU_TYPE_AUX_SLICE) ||
        (NalUnitType == NALU_TYPE_SLICE))
    {
        ExtraBytes    = 1;
    }

    return ExtraBytes;
}

CollatorStatus_t   Collator_PesVideoMVC_c::PresentCollatedHeader(
    unsigned char         StartCode,
    unsigned char        *HeaderBytes,
    FrameParserHeaderFlag_t  *Flags)
{
    unsigned char   NalUnitType = (StartCode & 0x1f);
    bool        AllowIResync;
    bool        Slice;
    bool        FirstSlice;
    bool        Iframe;

    *Flags  = 0;

    AllowIResync = (mCollatorSink->GetStreamPolicy(PolicyH264AllowNonIDRResynchronization) == PolicyValueApply);

    Slice       = (NalUnitType == NALU_TYPE_IDR) || (NalUnitType == NALU_TYPE_AUX_SLICE) || (NalUnitType == NALU_TYPE_SLICE);
    FirstSlice  = Slice && ((HeaderBytes[0] & 0x80) != 0);
    Iframe      = Slice && ((HeaderBytes[0] == 0x88) || ((HeaderBytes[0] & 0xf0) == 0xB0)); // H264_SLICE_TYPE_I_ALL or H264_SLICE_TYPE_I

    if (SeenProbableReversalPoint && (Slice || (NalUnitType == NALU_TYPE_SPS)))
    {
        *Flags              |= FrameParserHeaderFlagConfirmReversiblePoint;

        if (NalUnitType == NALU_TYPE_SPS)
        {
            *Flags            |= FrameParserHeaderFlagPossibleReversiblePoint | FrameParserHeaderFlagPartitionPoint;    // Make this the reversible point
        }

        SeenProbableReversalPoint    = false;
    }

    if ((NalUnitType == NALU_TYPE_IDR) || (AllowIResync && Iframe))
    {
        *Flags              |= FrameParserHeaderFlagPossibleReversiblePoint;
        SeenProbableReversalPoint    = true;
    }

    if (FirstSlice || NalUnitType == NALU_TYPE_PREFIX)
    {
        *Flags    |= FrameParserHeaderFlagPartitionPoint;
    }

    return CollatorNoError;
}
