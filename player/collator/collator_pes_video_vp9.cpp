/************************************************************************
Copyright (C) 2003-2014 STMicroelectronics. All Rights Reserved.

This file is part of the Streaming Engine.

Streaming Engine is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License version 2 as
published by the Free Software Foundation.

Streaming Engine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with Streaming Engine; see the file COPYING.  If not, write to the Free
Software Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307,
USA.

The Streaming Engine Library may alternatively be licensed under a
proprietary license from ST.
************************************************************************/

#include "collator_pes_video_vp9.h"
///
Collator_PesVideoVp9_c::Collator_PesVideoVp9_c()
    : mStreamMetadataInjected(false)
{
    SetGroupTrace(group_collator_video);

    Configuration.GenerateStartCodeList = true;
    Configuration.StreamIdentifierMask  = 0xff;
    Configuration.StreamIdentifierCode  = 0xfe;
    Configuration.RequireFrameHeaders   = true;
    Configuration.MaxHeaderSize         = MAX_VP9_ELEMENTARY_FRAMES * MAX_VP9_FRAME_HEADER_SIZE;
}

CollatorStatus_t   Collator_PesVideoVp9_c::InternalFrameFlush()
{
    if (AccumulatedDataSize)
    {
        /* first PES is always metadata incase of VP8/VP9 it should be
        passed directly to parser */
        if (mStreamMetadataInjected)
        {
            unsigned int frame_sizes[MAX_VP9_ELEMENTARY_FRAMES];
            int frame_count;
            /* Superframe Check routine */
            ParseSuperframeIndex(BufferBase, AccumulatedDataSize, frame_sizes, &frame_count);
            /* the number of elementary frames and each elementary frame offset is populated
            in StartCodeList structure members, so for Vp9 interpretation of parameter is different
            but prefer to use the same to be inline with common architecture */
            StartCodeList->NumberOfStartCodes = frame_count;
            for (int i = 0; i < StartCodeList->NumberOfStartCodes; i++)
            {
                StartCodeList->StartCodes[i] = (PackedStartCode_t)frame_sizes[i];
            }
        }
        else
        {
            StartCodeList->NumberOfStartCodes = 0;
            mStreamMetadataInjected = true;
        }
    }

    return Collator_PesFrame_c::InternalFrameFlush();
}

void Collator_PesVideoVp9_c::ParseSuperframeIndex(const unsigned char *data, int data_sz, unsigned int frame_sizes[MAX_VP9_ELEMENTARY_FRAMES], int *count)
{
    unsigned char marker = data[data_sz - 1];
    int counter = 1; //default value incase no superframes present
    frame_sizes[0] = data_sz;

    if ((marker & 0xe0) == 0xc0)
    {
        const unsigned int frames = (marker & 0x7) + 1;
        const unsigned int mag = ((marker >> 3) & 0x3) + 1;
        const unsigned int index_sz = 2 + mag * frames;

        if (data_sz >= index_sz && data[data_sz - index_sz] == marker)
        {
            /* found a valid superframe index */
            SE_VERBOSE(group_decoder_video, "Found a valid superframe index (%d)\n", index_sz);
            unsigned int i = 0;
            unsigned int j = 0;

            const unsigned char *x = data + data_sz - index_sz + 1;

            for (i = 0; i < frames; i++)
            {
                unsigned int this_sz = 0;

                for (j = 0; j < mag; j++) { this_sz |= (*x++) << (j * 8); }
                frame_sizes[i] = this_sz;
            }

            counter = frames;
        }
    }
    *count = counter;
}

CollatorStatus_t Collator_PesVideoVp9_c::FillFrameHeaders()
{
    unsigned char *temp_buffer = BufferBase;
    mAccumulatedStartCodeHeadersSize = 0;
    for (int i = 0; i < StartCodeList->NumberOfStartCodes; i++)
    {
        /* Header size is estimated based on a percentage of the whole frame length,
        based on study/experimentation, it is taken as 30% of the whole frame length
        with a max limit of 2K bytes and a min limit to 128 bytes. However if elementary
        frame is smaller than 128 bytes then all bytes are copied */
        unsigned int header_size = ((int)StartCodeList->StartCodes[i] * 30) / 100;

        if (header_size > MAX_VP9_FRAME_HEADER_SIZE)
        {
            header_size = MAX_VP9_FRAME_HEADER_SIZE;
        }
        else if (header_size < MIN_VP9_FRAME_HEADER_SIZE && StartCodeList->StartCodes[i] < MIN_VP9_FRAME_HEADER_SIZE)
        {
            header_size = (unsigned int)StartCodeList->StartCodes[i];
        }
        else if (header_size < MIN_VP9_FRAME_HEADER_SIZE && StartCodeList->StartCodes[i] > MIN_VP9_FRAME_HEADER_SIZE)
        {
            header_size = MIN_VP9_FRAME_HEADER_SIZE;
        }

        SE_VERBOSE(group_decoder_video, "Framesize : %d Header Size : %d Min: %d Max: %d\n", (int)StartCodeList->StartCodes[i], header_size, MIN_VP9_FRAME_HEADER_SIZE, MAX_VP9_FRAME_HEADER_SIZE);
        memcpy(mStartCodeHeadersBufferBase, temp_buffer, header_size);
        mStartCodeHeadersBufferBase += MAX_VP9_FRAME_HEADER_SIZE;
        mAccumulatedStartCodeHeadersSize += MAX_VP9_FRAME_HEADER_SIZE;
        temp_buffer += (int)StartCodeList->StartCodes[i];
    }
    return CollatorNoError;
}

