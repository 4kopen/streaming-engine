/************************************************************************
Copyright (C) 2003-2014 STMicroelectronics. All Rights Reserved.

This file is part of the Streaming Engine.

Streaming Engine is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License version 2 as
published by the Free Software Foundation.

Streaming Engine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with Streaming Engine; see the file COPYING.  If not, write to the Free
Software Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307,
USA.

The Streaming Engine Library may alternatively be licensed under a
proprietary license from ST.
************************************************************************/

#include "uncompressed.h"
#include "collator_video_uncompressed.h"

//
Collator_VideoUncompressed_c::Collator_VideoUncompressed_c()
{
    Configuration.GenerateStartCodeList      = false;  // Uncompressed data does not have start codes
    Configuration.MaxStartCodes              = 0;
    Configuration.StreamIdentifierMask       = 0x00;
    Configuration.StreamIdentifierCode       = 0x00;
    Configuration.BlockTerminateMask         = 0x00;
    Configuration.BlockTerminateCode         = 0x00;
    Configuration.InsertFrameTerminateCode   = false;
    Configuration.TerminalCode               = 0x00;
    Configuration.ExtendedHeaderLength       = 0;
    Configuration.DeferredTerminateFlag      = false;
}

CollatorStatus_t Collator_VideoUncompressed_c::Halt()
{
    DiscardAccumulatedData();
    return Collator_Base_c::Halt();
}

////////////////////////////////////////////////////////////////////////////
//
// Input, simply take the supplied uncompressed data and pass it on
//

CollatorStatus_t Collator_VideoUncompressed_c::InputSecondStage(unsigned int    DataLength,
                                                                const void     *Data)
{
    if (DataLength != sizeof(UncompressedBufferDesc_t))
    {
        SE_ERROR("Data is wrong size (%d != %d)\n", DataLength, sizeof(UncompressedBufferDesc_t));
        return CollatorError;
    }

    //
    // Attach the decode buffer mentioned in the input
    // to the current coded frame buffer, to ensure release
    // at the appropriate time.
    //
    mCollatorSink->AttachBuffer(Data);

    //
    // Copy the buffer descriptor to the CodedFrame and pass it on
    //
    CollatorStatus_t Status  = AccumulateData(DataLength, (unsigned char *)Data);
    if (Status == CollatorNoError)
    {
        Status    = InternalFrameFlush();
    }

    return Status;
}

