/************************************************************************
Copyright (C) 2003-2014 STMicroelectronics. All Rights Reserved.

This file is part of the Streaming Engine.

Streaming Engine is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License version 2 as
published by the Free Software Foundation.

Streaming Engine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with Streaming Engine; see the file COPYING.  If not, write to the Free
Software Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307,
USA.

The Streaming Engine Library may alternatively be licensed under a
proprietary license from ST.
************************************************************************/

#include "st_relayfs_se.h"
#include "decode_buffer_manager_base.h"

#undef TRACE_TAG
#define TRACE_TAG "DecodeBufferManager_Base_c"

#define MAX_BUFFER_RETRIES      2

// -----

#define BUFFER_DECODE_BUFFER        "DecodeBufferContext"
#define BUFFER_DECODE_BUFFER_TYPE   {BUFFER_DECODE_BUFFER, BufferDataTypeBase, AllocateFromOSMemory, 8, 0, true, true, sizeof(DecodeBufferContextRecord_t)}

static BufferDataDescriptor_t       InitialDecodeBufferContextDescriptor = BUFFER_DECODE_BUFFER_TYPE;

// -----
#define BUFFER_INDIVIDUAL_DECODE_BUFFER         "IndividualDecodeBuffer"
#define BUFFER_INDIVIDUAL_DECODE_BUFFER_TYPE        {BUFFER_INDIVIDUAL_DECODE_BUFFER, BufferDataTypeBase, AllocateIndividualSuppliedBlocks,1024,1024, true, true, DECODE_BUFFER_HD_SIZE}
static BufferDataDescriptor_t               IndividualBufferDescriptor = BUFFER_INDIVIDUAL_DECODE_BUFFER_TYPE;

#define BUFFER_SIMPLE_DECODE_BUFFER         "SimpleDecodeBuffer"
#define BUFFER_SIMPLE_DECODE_BUFFER_TYPE        {BUFFER_SIMPLE_DECODE_BUFFER, BufferDataTypeBase, AllocateFromSuppliedBlock, 1024, 1024, false, false, 0}
static BufferDataDescriptor_t               InitialSimpleBufferDescriptor = BUFFER_SIMPLE_DECODE_BUFFER_TYPE;

#define BUFFER_ONDEMAND_DECODE_BUFFER         "OnDemandDecodeBuffer"
#define BUFFER_ONDEMAND_DECODE_BUFFER_TYPE        {BUFFER_ONDEMAND_DECODE_BUFFER, BufferDataTypeBase, AllocateFromNamedDeviceMemory, 1024, 1024, false, false, 0}
static BufferDataDescriptor_t               OnDemandBufferDescriptor = BUFFER_ONDEMAND_DECODE_BUFFER_TYPE;

#define BUFFER_MACROBLOCK_STRUCTURE_BUFFER      "MacroBlockStructureBuffer"
#define BUFFER_MACROBLOCK_STRUCTURE_BUFFER_TYPE     {BUFFER_MACROBLOCK_STRUCTURE_BUFFER, BufferDataTypeBase, AllocateFromSuppliedBlock, 256, 256,  false, false, 0}
static BufferDataDescriptor_t           InitialMacroBlockStructureBufferDescriptor = BUFFER_MACROBLOCK_STRUCTURE_BUFFER_TYPE;

#define BUFFER_ONDEMAND_MACROBLOCK_STRUCTURE_BUFFER      "OnDemandMacroBlockStructureBuffer"
#define BUFFER_ONDEMAND_MACROBLOCK_STRUCTURE_BUFFER_TYPE     {BUFFER_ONDEMAND_MACROBLOCK_STRUCTURE_BUFFER, BufferDataTypeBase, AllocateFromNamedDeviceMemory, 256, 256,  false, false , 0}
static BufferDataDescriptor_t           OnDemandMacroBlockStructureBufferDescriptor = BUFFER_ONDEMAND_MACROBLOCK_STRUCTURE_BUFFER_TYPE;

#define BUFFER_VIDEO_POST_PROCESSING_CONTROL            "VideoPostProcessingControl"
#define BUFFER_VIDEO_POST_PROCESSING_CONTROL_TYPE       {BUFFER_VIDEO_POST_PROCESSING_CONTROL, BufferDataTypeBase, AllocateFromOSMemory, 4, 0, true, true, sizeof(VideoPostProcessingControl_t)}
static BufferDataDescriptor_t       InitialVideoPostProcessBufferDescriptor = BUFFER_VIDEO_POST_PROCESSING_CONTROL_TYPE;

#define BUFFER_ADDITIONAL_BLOCK                 "AdditionalBlock"
#define BUFFER_ADDITIONAL_BLOCK_TYPE                {BUFFER_ADDITIONAL_BLOCK, BufferDataTypeBase, AllocateFromSuppliedBlock, 32, 0, false, false, 0}
static BufferDataDescriptor_t           InitialAdditionalBlockDescriptor = BUFFER_ADDITIONAL_BLOCK_TYPE;


// ---------------------------------------------------------------------
//
//      The Constructor function
//
DecodeBufferManager_Base_c::DecodeBufferManager_Base_c()
    : Lock()
    , ScratchLock()
    , BufferManager(NULL)
    , NumberOfDecodeBuffers(0)
    , DecodeBufferPool(NULL)
    , mPreProcBufferPool()
    , StreamSwitchOccurring(false)
    , mResolutionChangeOccurring(false)
    , ComponentData()
    , StreamSwitchcopyOfComponentData()
    , mResolutionChangecopyOfComponentData()
    , AdditionalMemoryAccessType(0)
    , mMediaType(STM_SE_MEDIA_ANY)
{
    OS_InitializeMutex(&Lock);
    OS_InitializeMutex(&ScratchLock);
}

// ---------------------------------------------------------------------
//
//      The Destructor function
//

DecodeBufferManager_Base_c::~DecodeBufferManager_Base_c()
{
    DecodeBufferManager_Base_c::Halt();

    DecodeBufferManager_Base_c::Reset();

    OS_TerminateMutex(&Lock);
    OS_TerminateMutex(&ScratchLock);
}


// ---------------------------------------------------------------------
//
//      The Halt function, give up access to any registered resources
//

DecodeBufferManagerStatus_t   DecodeBufferManager_Base_c::Halt()
{
    //
    // We have no access other component parts in this class, so halt
    // has no function other than that expressed by the base class.
    BaseComponentClass_c::Halt();
    return DecodeBufferManagerNoError;
}

// ---------------------------------------------------------------------
// to call ics_region_remove at CPS entry

void   DecodeBufferManager_Base_c::ResetDecoderMap()
{
    SE_DEBUG(group_decodebufferm, "Reset Ics map\n");
    OS_LockMutex(&Lock);
    for (int i = 0; i < NUMBER_OF_COMPONENTS; i++)
    {
        if (ComponentData[i].AllocatorMemoryDevice != NULL)
        {
            AllocatorRemoveMapEx(ComponentData[i].AllocatorMemoryDevice->UnderlyingDevice);
        }
    }
    OS_UnLockMutex(&Lock);
}


DecodeBufferManagerStatus_t   DecodeBufferManager_Base_c::CreateDecoderMap()
{
    DecodeBufferManagerStatus_t Status = DecodeBufferManagerNoError;

    SE_DEBUG(group_decodebufferm, "Create Ics map\n");
    OS_LockMutex(&Lock);

    for (int i = 0; i < NUMBER_OF_COMPONENTS; i++)
    {
        if (ComponentData[i].AllocatorMemoryDevice != NULL)
        {
            allocator_status_t AStatus = AllocatorCreateMapEx(ComponentData[i].AllocatorMemoryDevice->UnderlyingDevice);
            if (AStatus == allocator_error)
            {
                Status = DecodeBufferManagerError;
                break;
            }
        }
    }
    OS_UnLockMutex(&Lock);
    return Status;
}

//
// After a stream Switch or a resolution change , release the pending components
//
void    DecodeBufferManager_Base_c::ReleaseCopyOfComponentData(bool SwitchOccurring,  DecodeBufferComponentData_t copyOfComponentData[NUMBER_OF_COMPONENTS])
{
    if (SwitchOccurring)
    {
        for (unsigned int i = 0; i < NUMBER_OF_COMPONENTS; i++)
        {
            if (copyOfComponentData[i].BufferPool != NULL)
            {
                // Release scratch buffer because new stream may have different characteristics
                // in term of memory cache and mapping attributes
                ReleaseComponentScratchBuffers(&copyOfComponentData[i].ScratchInfo);
                BufferManager->DestroyPool(copyOfComponentData[i].BufferPool);
                AllocatorClose(&copyOfComponentData[i].AllocatorMemoryDevice);
            }
        }
    }
    memset(copyOfComponentData, 0, NUMBER_OF_COMPONENTS * sizeof(DecodeBufferComponentData_t));
}

// ---------------------------------------------------------------------
// reset for stream switch

DecodeBufferManagerStatus_t   DecodeBufferManager_Base_c::Reset()
{
    SE_DEBUG(group_decodebufferm, ">\n");

    // First ensure we have released all the scratch buffers held by the DecodeBufferManager
    ReleaseAllScratchBuffers();

    OS_LockMutex(&Lock);

    // First we release any pools held over during a stream switch
    ReleaseCopyOfComponentData(StreamSwitchOccurring, StreamSwitchcopyOfComponentData);
    StreamSwitchOccurring        = false;

    ReleaseCopyOfComponentData(mResolutionChangeOccurring, mResolutionChangecopyOfComponentData);
    mResolutionChangeOccurring   = false;

    // Now the standard list
    for (int i = 0; i < NUMBER_OF_COMPONENTS; i++)
    {
        DestroyComponentPool(i);
    }
    memset(&ComponentData, 0, NUMBER_OF_COMPONENTS * sizeof(DecodeBufferComponentData_t));

    // Finally the buffer pool itself needs to go
    if (DecodeBufferPool != NULL)
    {
        BufferManager->DestroyPool(DecodeBufferPool);
        DecodeBufferPool = NULL;
    }

    OS_UnLockMutex(&Lock);
    SE_DEBUG(group_decodebufferm, "<\n");

    return BaseComponentClass_c::Reset();
}


// ---------------------------------------------------------------------
//
//      Override for component base class set module parameters function
//

DecodeBufferManagerStatus_t   DecodeBufferManager_Base_c::SetModuleParameters(
    unsigned int              ParameterBlockSize,
    void                     *ParameterBlock)
{
    DecodeBufferManagerParameterBlock_t *DecodeBufferManagerParameterBlock = (DecodeBufferManagerParameterBlock_t *)ParameterBlock;
    DecodeBufferManagerStatus_t      Status;

    if (ParameterBlockSize != sizeof(DecodeBufferManagerParameterBlock_t))
    {
        SE_ERROR("Invalid parameter block\n");
        return DecodeBufferManagerError;
    }

    Status  = DecodeBufferManagerNoError;
    mMediaType = DecodeBufferManagerParameterBlock->MediaType;

    switch (DecodeBufferManagerParameterBlock->ParameterType)
    {
    case DecodeBufferManagerPartitionData:
        //
        // Switch the parameters - assert that we are currently in a reset state
        //
        AssertComponentState(ComponentReset);
        NumberOfDecodeBuffers       = DecodeBufferManagerParameterBlock->PartitionData.NumberOfDecodeBuffers;

        // Primary Manifestation
        ComponentData[PrimaryManifestationComponent].OnDemand             = DecodeBufferManagerParameterBlock->PartitionData.OnDemand;
        ComponentData[PrimaryManifestationComponent].AllocationPartitionSize    = DecodeBufferManagerParameterBlock->PartitionData.PrimaryManifestationPartitionSize;
        ComponentData[PrimaryManifestationComponent].AllowReallocation   = (ComponentData[PrimaryManifestationComponent].AllocationPartitionSize == 0) ? true : false;
        memcpy(&ComponentData[PrimaryManifestationComponent].AllocationPartitionName, &DecodeBufferManagerParameterBlock->PartitionData.PrimaryManifestationPartitionName, ALLOCATOR_MAX_PARTITION_NAME_SIZE);
        ComponentData[PrimaryManifestationComponent].MaximumNumberOfUseBuffers = NumberOfDecodeBuffers;

        // Decimated Manifestation
        ComponentData[DecimatedManifestationComponent].OnDemand             = DecodeBufferManagerParameterBlock->PartitionData.OnDemand;
        ComponentData[DecimatedManifestationComponent].AllocationPartitionSize  = DecodeBufferManagerParameterBlock->PartitionData.DecimatedManifestationPartitionSize;
        ComponentData[DecimatedManifestationComponent].AllowReallocation   = (ComponentData[DecimatedManifestationComponent].AllocationPartitionSize == 0) ? true : false;
        memcpy(&ComponentData[DecimatedManifestationComponent].AllocationPartitionName, &DecodeBufferManagerParameterBlock->PartitionData.DecimatedManifestationPartitionName,
               ALLOCATOR_MAX_PARTITION_NAME_SIZE);
        ComponentData[DecimatedManifestationComponent].MaximumNumberOfUseBuffers = NumberOfDecodeBuffers;

        // Reference picture
        ComponentData[VideoDecodeCopy].OnDemand             = DecodeBufferManagerParameterBlock->PartitionData.OnDemand;
        ComponentData[VideoDecodeCopy].AllocationPartitionSize  = DecodeBufferManagerParameterBlock->PartitionData.VideoDecodeCopyPartitionSize;
        ComponentData[VideoDecodeCopy].AllowReallocation   = (ComponentData[VideoDecodeCopy].AllocationPartitionSize == 0) ? true : false;
        memcpy(&ComponentData[VideoDecodeCopy].AllocationPartitionName, &DecodeBufferManagerParameterBlock->PartitionData.VideoDecodeCopyPartitionName, ALLOCATOR_MAX_PARTITION_NAME_SIZE);
        ComponentData[VideoDecodeCopy].MaximumNumberOfUseBuffers = NumberOfDecodeBuffers;

        // Reference Macroblock
        ComponentData[VideoMacroblockStructure].OnDemand             = DecodeBufferManagerParameterBlock->PartitionData.OnDemand;
        ComponentData[VideoMacroblockStructure].AllocationPartitionSize = DecodeBufferManagerParameterBlock->PartitionData.VideoMacroblockStructurePartitionSize;
        ComponentData[VideoMacroblockStructure].AllowReallocation   = (ComponentData[VideoMacroblockStructure].AllocationPartitionSize == 0) ? true : false;

        memcpy(&ComponentData[VideoMacroblockStructure].AllocationPartitionName, &DecodeBufferManagerParameterBlock->PartitionData.VideoMacroblockStructurePartitionName, ALLOCATOR_MAX_PARTITION_NAME_SIZE);
        ComponentData[VideoMacroblockStructure].MaximumNumberOfUseBuffers = NumberOfDecodeBuffers;

        // Video Post process buffer
        ComponentData[VideoPostProcessControl].AllocationPartitionSize  = DecodeBufferManagerParameterBlock->PartitionData.VideoPostProcessControlPartitionSize;
        ComponentData[VideoPostProcessControl].AllowReallocation   = (ComponentData[VideoPostProcessControl].AllocationPartitionSize == 0) ? true : false;
        memcpy(&ComponentData[VideoPostProcessControl].AllocationPartitionName, &DecodeBufferManagerParameterBlock->PartitionData.VideoPostProcessControlPartitionName, ALLOCATOR_MAX_PARTITION_NAME_SIZE);
        ComponentData[VideoPostProcessControl].MaximumNumberOfUseBuffers = NumberOfDecodeBuffers;

        // Addtional memory
        ComponentData[AdditionalMemoryBlock].AllocationPartitionSize    = DecodeBufferManagerParameterBlock->PartitionData.AdditionalMemoryBlockPartitionSize;
        ComponentData[AdditionalMemoryBlock].AllowReallocation   = (ComponentData[AdditionalMemoryBlock].AllocationPartitionSize == 0) ? true : false;
        memcpy(&ComponentData[AdditionalMemoryBlock].AllocationPartitionName, &DecodeBufferManagerParameterBlock->PartitionData.AdditionalMemoryBlockPartitionName, ALLOCATOR_MAX_PARTITION_NAME_SIZE);
        ComponentData[AdditionalMemoryBlock].MaximumNumberOfUseBuffers = NumberOfDecodeBuffers;
        break;

    case DecodeBufferManagerPrimaryIndividualBufferList:
        //
        // Save the 'NumberOfDecodeBuffers' provided individual buffer
        //
        NumberOfDecodeBuffers = DecodeBufferManagerParameterBlock->PrimaryIndividualData.NumberOfBufs;
        strncpy(ComponentData[PrimaryManifestationComponent].AllocationPartitionName, IndividualBuffersPartition,
                sizeof(ComponentData[PrimaryManifestationComponent].AllocationPartitionName));
        ComponentData[PrimaryManifestationComponent].AllocationPartitionName[
            sizeof(ComponentData[PrimaryManifestationComponent].AllocationPartitionName) - 1] = '\0';
        ComponentData[PrimaryManifestationComponent].MaximumNumberOfUseBuffers = NumberOfDecodeBuffers;
        ComponentData[PrimaryManifestationComponent].IndividualBufferSize = DecodeBufferManagerParameterBlock->PrimaryIndividualData.BufSize;
        memcpy(ComponentData[PrimaryManifestationComponent].ArrayOfMemoryBlocks,
               DecodeBufferManagerParameterBlock->PrimaryIndividualData.AllocatorMemory,
               sizeof(DecodeBufferManagerParameterBlock->PrimaryIndividualData.AllocatorMemory));
        break;

    default:
        SE_ERROR("Unrecognised parameter block %d\n",
                 DecodeBufferManagerParameterBlock->ParameterType);
        Status  = DecodeBufferManagerError;
    }

    return  Status;
}


// ---------------------------------------------------------------------
//
//      Function to return the decode buffer pool to the caller.
//

BufferPool_t DecodeBufferManager_Base_c::GetDecodeBufferPool()
{
    OS_LockMutex(&Lock);

    if (DecodeBufferPool == NULL)
    {
        DecodeBufferManagerStatus_t Status = CreateDecodeBufferPool();

        if (Status != DecodeBufferManagerNoError)
        {
            OS_UnLockMutex(&Lock);
            return NULL;
        }
    }

    BufferPool_t Pool = DecodeBufferPool;
    OS_UnLockMutex(&Lock);

    return Pool;
}


// ---------------------------------------------------------------------
//
//      Function to check if some decode buffers are still in use
//

bool DecodeBufferManager_Base_c::AreBuffersStillInUse()
{
    OS_LockMutex(&Lock);

    unsigned int    BuffersInPool;
    unsigned int    BuffersWithNonZeroReferenceCount;
    for (int i = 0; i < NUMBER_OF_COMPONENTS; i++)
    {
        if (ComponentData[i].BufferPool != NULL)
        {
            ComponentData[i].BufferPool->GetPoolUsage(&BuffersInPool, &BuffersWithNonZeroReferenceCount);
            if (BuffersWithNonZeroReferenceCount != 0)
            {
                SE_DEBUG(group_decodebufferm, "%d buffers of type '%s' are still in use (pool %d)\n",
                         BuffersWithNonZeroReferenceCount, ComponentData[i].ComponentDescriptor.DataDescriptor->TypeName, i);
            }
        }
    }

    // Check DecodeBufferPool ref count
    DecodeBufferPool->GetPoolUsage(&BuffersInPool, &BuffersWithNonZeroReferenceCount);
    if (BuffersWithNonZeroReferenceCount != 0)
    {
        SE_DEBUG(group_decodebufferm, "DecodeBufferPool has still %d referenced buffers\n", BuffersWithNonZeroReferenceCount);
    }

    OS_UnLockMutex(&Lock);

    return (BuffersWithNonZeroReferenceCount != 0);
}


// ---------------------------------------------------------------------
//
//      Function that monitors the decode buffer pool usage and releases
//      all the component pools when no more used.
//
void DecodeBufferManager_Base_c::WaitAndReleaseAllComponentPools()
{
    SE_DEBUG(group_decodebufferm, ">\n");

    // First ensure we have released all the scratch buffers held by the DecodeBufferManager
    ReleaseAllScratchBuffers();

    // Wait for all decode buffers to be released
    unsigned int BackoffMs = 5;
    while (AreBuffersStillInUse())
    {
        SE_DEBUG(group_se_pipeline, "Stream 0x%p - Sleeping for %ums\n", Stream, BackoffMs);
        if (BackoffMs > 1000)
        {
            SE_FATAL("Some buffers not released\n");
        }
        OS_SleepMilliSeconds(BackoffMs);
        BackoffMs *= 2;
    }

    // Free all component pools
    OS_LockMutex(&Lock);
    for (int i = 0; i < NUMBER_OF_COMPONENTS; i++)
    {
        if (ComponentData[i].BufferPool != NULL)
        {
            BufferManager->DestroyPool(ComponentData[i].BufferPool);
            ComponentData[i].BufferPool  = NULL;
            ComponentData[i].PoolCreated = false;
            if (ComponentData[i].AllowReallocation) { ComponentData[i].AllocationPartitionSize = 0; }
            AllocatorClose(&ComponentData[i].AllocatorMemoryDevice);
            ComponentData[i].AllocatorMemoryDevice  = NULL;

            memset(&ComponentData[i].ComponentDescriptor, 0, sizeof(DecodeBufferComponentDescriptor_t));
            ComponentData[i].Enabled = false;
        }
    }
    OS_UnLockMutex(&Lock);

    SE_DEBUG(group_decodebufferm, "<\n");
}


// ---------------------------------------------------------------------
//
//      Function to support entry into stream switch mode
//

DecodeBufferManagerStatus_t   DecodeBufferManager_Base_c::EnterStreamSwitch()
{
    SE_DEBUG(group_decodebufferm, ">\n");
    if (StreamSwitchOccurring)
    {
        SE_WARNING("StreamSwitchOccurring already ongoing\n");
    }
    StreamSwitchOccurring   = true;
    SE_DEBUG(group_decodebufferm, "<\n");
    return  DecodeBufferManagerNoError;
}


// ---------------------------------------------------------------------
//
//      Function to inform the class of the components that make
//  up a decode buffer.
//

DecodeBufferManagerStatus_t   DecodeBufferManager_Base_c::FillOutDefaultList(
    BufferFormat_t               PrimaryFormat,
    DecodeBufferComponentElementMask_t   Elements,
    DecodeBufferComponentDescriptor_t    List[])
{
    unsigned int  Count   = 0;

    SE_DEBUG(group_decodebufferm, ">\n");
    if ((Elements & PrimaryManifestationElement) != 0)
    {
        memset(&List[Count], 0, sizeof(DecodeBufferComponentDescriptor_t));
        List[Count].Type        = PrimaryManifestationComponent;
        List[Count].Usage       = ((Elements & VideoDecodeCopyElement) != 0) ?
                                  ForManifestation :
                                  (ForManifestation | ForReference);
        List[Count].DataType        = PrimaryFormat;
        if (PrimaryFormat != FormatAudio)
        {
            List[Count].DefaultAddressMode = PhysicalAddress;
        }

        if (PrimaryFormat == FormatVideo420_MacroBlock)
        {
            // DELTA provides 16 multiple alignment for width along ref buffers
            List[Count].ComponentBorders[0] = 16;
            List[Count].ComponentBorders[1] = 32;
        }
        else
        {
            // Default alignment to 32
            // This is mandatory for (PrimaryFormat == FormatVideo420_Raster2B)
            // Although DELTA supports alignment 16 in width, display needs 32 for FormatVideo420_Raster2B
            List[Count].ComponentBorders[0] = 32;
            List[Count].ComponentBorders[1] = 32;
        }

        if (ComponentData[PrimaryManifestationComponent].OnDemand)
        {
            List[Count].DataDescriptor  = &OnDemandBufferDescriptor;
        }
        else
        {
            List[Count].DataDescriptor  = &InitialSimpleBufferDescriptor;
        }
        //
        // When partition name is "AllocatedLaterPartition", set Component usage as "notUsed" to avoid useless allocation
        // When partition name is "IndividualBuffersPartition", set Component whit the individual buffers list
        //
        if (strncmp(ComponentData[PrimaryManifestationComponent].AllocationPartitionName,
                    AllocatedLaterPartition,
                    ALLOCATOR_MAX_PARTITION_NAME_SIZE) == 0)
        {
            List[Count].Usage = NotUsed;
        }
        else if (strncmp(ComponentData[PrimaryManifestationComponent].AllocationPartitionName,
                         IndividualBuffersPartition,
                         ALLOCATOR_MAX_PARTITION_NAME_SIZE) == 0)
        {
            List[Count].DataDescriptor = &IndividualBufferDescriptor;
        }

        Count++;
    }

    if ((Elements & DecimatedManifestationElement) != 0)
    {
        memset(&List[Count], 0, sizeof(DecodeBufferComponentDescriptor_t));
        List[Count].Type        = DecimatedManifestationComponent;
        List[Count].Usage       = ForManifestation;
        //Decimation is always YUV420 Raster2B
        List[Count].DataType        = FormatVideo420_Raster2B;
        List[Count].DefaultAddressMode  = PhysicalAddress;
        // Although DELTA supports alignment 16, display needs 32 for RASTER2B
        // So, default alignment to 32.
        List[Count].ComponentBorders[0] = 32;
        List[Count].ComponentBorders[1] = 32;

        if (ComponentData[DecimatedManifestationComponent].OnDemand)
        {
            List[Count].DataDescriptor  = &OnDemandBufferDescriptor;
        }
        else
        {
            List[Count].DataDescriptor  = &InitialSimpleBufferDescriptor;
        }
        Count++;
    }

    if ((Elements & VideoDecodeCopyElement) != 0)
    {
        memset(&List[Count], 0, sizeof(DecodeBufferComponentDescriptor_t));
        List[Count].Type        = VideoDecodeCopy;
        List[Count].Usage       = ForReference | ForDecode;
        // Specific patch for HEVC only -> use FormatVideo420_HadesMacroBlock
        if ((Elements & VideoMacroblockStructureForNotReferenceElement) != 0)
        {
            List[Count].DataType = FormatVideo420_HadesMacroBlock;
        }
        else
        {
            List[Count].DataType = FormatVideo420_PairedMacroBlock;
        }
        List[Count].DefaultAddressMode  = PhysicalAddress;
        List[Count].ComponentBorders[0] = 32;
        List[Count].ComponentBorders[1] = 32;

        if (ComponentData[VideoDecodeCopy].OnDemand)
        {
            List[Count].DataDescriptor  = &OnDemandBufferDescriptor;
        }
        else
        {
            List[Count].DataDescriptor  = &InitialSimpleBufferDescriptor;
        }
        Count++;
    }

    if ((Elements & VideoMacroblockStructureElement) != 0)
    {
        memset(&List[Count], 0, sizeof(DecodeBufferComponentDescriptor_t));
        List[Count].Type        = VideoMacroblockStructure;
        List[Count].Usage       = ForReference;

        // HADES HW bug: while decoding HEVC stream it always requires a ppb buffer, what ever is the picture type
        if ((Elements & VideoMacroblockStructureForNotReferenceElement) != 0) { List[Count].Usage |= ForDecode; }

        // AVS specifities , firmware directly manage the PPB buffer address based on the pool base address
        //As a result PB buffer are not provided  (reference count are uncorrectly managed)
        if ((Elements & VideoMacroblockPoolStructureElement) != 0)
        {
            ComponentData[VideoMacroblockStructure].AllocationPartitionSize = MACRO_BLOCK_POOL_CONTEXT_SIZE;
            ComponentData[VideoMacroblockStructure].OnDemand = false;
            List[Count].DirectPoolUsage = true;
        }

        List[Count].DataType        = FormatPerMacroBlockLinear;
        List[Count].DefaultAddressMode  = PhysicalAddress;

        if (ComponentData[VideoMacroblockStructure].OnDemand)
        {
            List[Count].DataDescriptor  = &OnDemandMacroBlockStructureBufferDescriptor;
        }
        else
        {
            List[Count].DataDescriptor  = &InitialMacroBlockStructureBufferDescriptor;
        }
        Count++;
    }

    if ((Elements & VideoPostProcessControlElement) != 0)
    {
        memset(&List[Count], 0, sizeof(DecodeBufferComponentDescriptor_t));
        List[Count].Type        = VideoPostProcessControl;
        List[Count].Usage       = ForManifestation;
        List[Count].DataType        = FormatLinear;
        List[Count].DefaultAddressMode  = CachedAddress;
        List[Count].DataDescriptor  = &InitialVideoPostProcessBufferDescriptor;
        Count++;
    }

    if ((Elements & AdditionalMemoryBlockElement) != 0)
    {
        memset(&List[Count], 0, sizeof(DecodeBufferComponentDescriptor_t));
        List[Count].Type        = AdditionalMemoryBlock;
        List[Count].Usage       = ForManifestation | ForReference;
        List[Count].DataType        = FormatLinear;
        List[Count].DefaultAddressMode  = PhysicalAddress;
        List[Count].DataDescriptor  = &InitialAdditionalBlockDescriptor;
        Count++;
    }

    List[Count].Type            = UndefinedComponent;
    SE_DEBUG(group_decodebufferm, "<\n");
    return DecodeBufferManagerNoError;
}


// ---------------------------------------------------------------------
//
//      Function to inform the class of the components that make
//  up a decode buffer.
//

DecodeBufferManagerStatus_t   DecodeBufferManager_Base_c::InitializeComponentList(DecodeBufferComponentDescriptor_t *List)
{
    unsigned int            i;

    SE_DEBUG(group_decodebufferm, ">\n");

    // Dummy check for parameter validity
    SE_ASSERT(List[0].Type < UndefinedComponent);

    //
    // If we are in stream switching, copy the components, and
    // see which ones get re-used, attempt deletions later.
    //
    if (StreamSwitchOccurring)
    {
        bool ComponentAllocated             = false;
        bool StreamSwitchComponentAllocated = false;

        for (i = 0; i < NUMBER_OF_COMPONENTS; i++)
        {
            ReleaseComponentScratchBuffers(&ComponentData[i].ScratchInfo);
            if (ComponentData[i].BufferPool                   != NULL) { ComponentAllocated             = true; }
            if (StreamSwitchcopyOfComponentData[i].BufferPool != NULL) { StreamSwitchComponentAllocated = true; }
        }

        // Ensure that a Stream switch was not already ongoing
        if (ComponentAllocated && StreamSwitchComponentAllocated)
        {
            SE_FATAL("Stream switch already ongoing\n");
        }

        // When none Component have been allocated, no copy need to be performed, as a result the StreamSwitchOccuring
        // variable is set to false at the end of this function, enabling acceptance of a new StreamSwitch immediately
        if (ComponentAllocated)
        {
            memcpy(StreamSwitchcopyOfComponentData, ComponentData, NUMBER_OF_COMPONENTS * sizeof(DecodeBufferComponentData_t));
        }
    }

    //
    // reset descriptor
    //
    for (i = 0; i < NUMBER_OF_COMPONENTS; i++)
    {
        memset(&ComponentData[i].ComponentDescriptor, 0, sizeof(DecodeBufferComponentDescriptor_t));
        ComponentData[i].Enabled            = false;
        ComponentData[i].AllocatorMemoryDevice  = NULL;
        ComponentData[i].BufferPool         = NULL;
    }

    //
    // Scan the list and create the appropriate pools
    //
    for (i = 0; List[i].Type < UndefinedComponent; i++)
    {
        memcpy(&ComponentData[List[i].Type].ComponentDescriptor, &List[i], sizeof(DecodeBufferComponentDescriptor_t));
    }

    for (i = 0; i < NUMBER_OF_COMPONENTS; i++)
    {
        if (ComponentData[i].ComponentDescriptor.Usage != NotUsed)
        {
            // we create pool for which size is known thanks to a specific memory profile.
            // For On Demand and Auto memory profile, size is null and allocation is performed
            // later, based on stream characteristics.
            // For legacy memory profile, reallocation of pool is disabled to be aligned with
            // provided memory profile.
            if (ComponentData[i].AllocationPartitionSize != 0)
            {
                DecodeBufferManagerStatus_t Status = CreateComponentPool(i);
                if (Status != DecodeBufferManagerNoError)
                {
                    SE_ERROR("Failed to create component pools for decode buffers\n");
                    Reset();
                    return DecodeBufferManagerError;
                }
                ComponentData[i].PoolCreated   = true;
            }
            else
            {
                ComponentData[i].PoolCreated = false;
            }
            ComponentData[i].Enabled       = true;     // Default enable a component
        }
    }

    // Before returning, we check if in switch stream or in resolution change mode, can we let anything go
    if (StreamSwitchOccurring)
    {
        CheckForSwitchStreamTermination();
    }
    if (mResolutionChangeOccurring)
    {
        CheckForResolutionChangeTermination();
    }

    SE_DEBUG(group_decodebufferm, "<\n");
    return DecodeBufferManagerNoError;
}


// ---------------------------------------------------------------------
//
//      Simplifying function to allow those that just want a primary
//  with a varying buffer type.
//

DecodeBufferManagerStatus_t   DecodeBufferManager_Base_c::InitializeSimpleComponentList(BufferFormat_t Type)
{
    DecodeBufferComponentDescriptor_t   List[2];
    FillOutDefaultList(Type, PrimaryManifestationElement, List);
    return  InitializeComponentList(List);
}


// ---------------------------------------------------------------------
//
//      Functions to switch the format of the output (used in theora)
//

DecodeBufferManagerStatus_t   DecodeBufferManager_Base_c::ModifyComponentFormat(
    DecodeBufferComponentType_t      Component,
    BufferFormat_t               NewFormat)
{
    ComponentData[Component].ComponentDescriptor.DataType   = NewFormat;
    return DecodeBufferManagerNoError;
}


// ---------------------------------------------------------------------
//
//      Functions to enable/disable the generation of a particular component
//

DecodeBufferManagerStatus_t   DecodeBufferManager_Base_c::ComponentEnable(
    DecodeBufferComponentType_t  Component,
    bool                         Enabled)
{
    ComponentData[Component].Enabled = Enabled;
    return DecodeBufferManagerNoError;
}

// ---------------------------------------------------------------------
//
// It displays the content of the components.
//

void DecodeBufferManager_Base_c::DisplayComponents()
{
    SE_DEBUG(group_decodebufferm, ">\n");

    for (int j = 0; j < NUMBER_OF_COMPONENTS; j++)
    {
        unsigned char *DataStart = (unsigned char *)ComponentData[j].AllocatorMemory[PhysicalAddress];
        unsigned char *DataEnd = DataStart + ComponentData[j].AllocationPartitionSize;
        SE_DEBUG(group_decodebufferm, "@ Component: %d, DataStart=%p, DataEnd=%p, ComponentData[%d].BufferPool=%p, ComponentData[%d].AllocatorMemoryDevice=%p\n",
                 j, DataStart, DataEnd, j,
                 (void *)(ComponentData[j].BufferPool),
                 j, (void *)(ComponentData[j].AllocatorMemoryDevice));
    }

    SE_DEBUG(group_decodebufferm, "<\n");
}

// ---------------------------------------------------------------------
//
//      Functions to get a buffer and allocate all the appropriate components
//

DecodeBufferManagerStatus_t   DecodeBufferManager_Base_c::GetDecodeBuffer(
    DecodeBufferRequest_t   *Request,
    Buffer_t                *Buffer)
{
    unsigned int                 i;
    Buffer_t                     NewDecodeBuffer;
    DecodeBufferContextRecord_t *BufferContext;
    bool                         NeedToAllocateComponent;
    DecodeBufferUsage_t          NeedComponentsFor;

    *Buffer = NULL;

    //
    // Since people can release without calling us, we check for any stream switch or any
    //  resolution change being over
    //

    if (StreamSwitchOccurring)
    {
        CheckForSwitchStreamTermination();
    }

    if (mResolutionChangeOccurring)
    {
        CheckForResolutionChangeTermination();
    }

    //
    // Get me a new decode buffer
    //
    BufferStatus_t BufferStatus = DecodeBufferPool->GetBuffer(&NewDecodeBuffer);
    if (BufferStatus != BufferNoError)
    {
        SE_ERROR("Failed to allocate a decode buffer\n");
        return DecodeBufferManagerError;
    }

    //
    // Initialize the context record
    //
    NewDecodeBuffer->ObtainDataReference(NULL, NULL, (void **)(&BufferContext));
    SE_ASSERT(BufferContext != NULL); // cannot be empty
    memset(BufferContext, 0, sizeof(DecodeBufferContextRecord_t));
    BufferContext->DecodeBuffer     = NewDecodeBuffer;
    memcpy(&BufferContext->Request, Request, sizeof(DecodeBufferRequest_t));
    //
    // allocate the components - note no components for a marker frame.
    //
    NeedComponentsFor       = ForDecode     |
                              ForManifestation  |
                              (Request->ReferenceFrame ? ForReference : NotUsed);

    for (i = 0; i < NUMBER_OF_COMPONENTS; i++)
    {
        NeedToAllocateComponent = !Request->mIsMarkerFrame     &&
                                  ComponentData[i].Enabled  &&
                                  ((ComponentData[i].ComponentDescriptor.Usage & NeedComponentsFor) != NotUsed) &&
                                  (!ComponentData[i].ComponentDescriptor.DirectPoolUsage);

        if (NeedToAllocateComponent)
        {
            DecodeBufferManagerStatus_t Status  = AllocateComponent(i, BufferContext , ((ComponentData[i].ComponentDescriptor.Usage & NeedComponentsFor) == ForDecode));
            if (Status != DecodeBufferManagerNoError)
            {
                SE_ERROR("Failed to allocate all buffer components\n");
                NewDecodeBuffer->DecrementReferenceCount();
                return Status;
            }

            BufferContext->StructureComponent[i].Usage  = (ComponentData[i].ComponentDescriptor.Usage & NeedComponentsFor);
        }
    }

    *Buffer = NewDecodeBuffer;
    return DecodeBufferManagerNoError;
}


// ---------------------------------------------------------------------
//
//      Functions to get a buffer and allocate all the appropriate components
//

DecodeBufferManagerStatus_t   DecodeBufferManager_Base_c::EnsureReferenceComponentsPresent(
    Buffer_t                Buffer)
{
    unsigned int             i;
    PlayerStatus_t           Status;
    DecodeBufferContextRecord_t     *BufferContext  = GetBufferContext(Buffer);
    bool                 ComponentRelevent;
    bool                 NeedToAllocateComponent;
    OS_LockMutex(&Lock);

    for (i = 0; i < NUMBER_OF_COMPONENTS; i++)
    {
        ComponentRelevent   = ComponentData[i].Enabled &&
                              ((ComponentData[i].ComponentDescriptor.Usage & ForReference) != NotUsed);

        if (ComponentRelevent)
        {
            NeedToAllocateComponent = (BufferContext->StructureComponent[i].Usage == NotUsed);

            if (NeedToAllocateComponent)
            {
                Status  = AllocateComponent(i, BufferContext);

                if (Status != DecodeBufferManagerNoError)
                {
                    SE_ERROR("Failed to allocate all buffer components\n");
                    OS_UnLockMutex(&Lock);
                    return Status;
                }
            }

            BufferContext->StructureComponent[i].Usage  |= ForReference;
        }
    }

    OS_UnLockMutex(&Lock);
//
    return DecodeBufferManagerNoError;
}


// ---------------------------------------------------------------------
//
//      Function estimate the number of buffers that can be obtained
//

DecodeBufferManagerStatus_t   DecodeBufferManager_Base_c::GetEstimatedBufferCount(unsigned int      *Count,
                                                                                  DecodeBufferUsage_t   For)
{
    unsigned int    i;
    unsigned int    MinimumAvailable;

    OS_LockMutex(&Lock);

    MinimumAvailable    = NumberOfDecodeBuffers;

    for (i = 0; i < NUMBER_OF_COMPONENTS; i++)
    {
        if (ComponentData[i].Enabled &&
            ((ComponentData[i].ComponentDescriptor.Usage & For) != NotUsed) &&
            !ComponentData[i].ComponentDescriptor.DataDescriptor->FixedSize &&
            (ComponentData[i].LastAllocationSize != 0))
        {
            if ((ComponentData[i].OnDemand == false)  && (ComponentData[i].AllocationPartitionSize != 0))
            {
                MinimumAvailable    = min(MinimumAvailable, (ComponentData[i].AllocationPartitionSize / ComponentData[i].LastAllocationSize));
            }
            MinimumAvailable    = min(MinimumAvailable, ComponentData[i].MaximumNumberOfUseBuffers);
        }
    }

    *Count  = MinimumAvailable;

    OS_UnLockMutex(&Lock);

    return DecodeBufferManagerNoError;
}


// ---------------------------------------------------------------------
//
//      Function to release a hold on a whole buffer
//
//  This may involve the release of a reference pointer
//

DecodeBufferManagerStatus_t   DecodeBufferManager_Base_c::ReleaseBuffer(Buffer_t    Buffer,
                                                                        bool       ForReference)
{
    DecodeBufferContextRecord_t *BufferContext  = GetBufferContext(Buffer);
    unsigned int             Count;

    //
    // Lock to ensure no-one is releasing individual components
    // when we come to release the whole shebang
    //

    if (ForReference)
    {
        DecrementReferenceUseCount(Buffer);
    }
    else
    {
        OS_LockMutex(&Lock);
        // Can we let the manifestation components go
        Buffer->GetOwnerCount(&Count);

        if (Count == (BufferContext->ReferenceUseCount + 1))
        {
            OS_UnLockMutex(&Lock);
            ReleaseBufferComponents(Buffer, ForManifestation);
            OS_LockMutex(&Lock);
        }

        Buffer->DecrementReferenceCount();
        OS_UnLockMutex(&Lock);
    }

    //
    // Check for any stream switch or any resolution change being over
    //

    if (StreamSwitchOccurring)
    {
        CheckForSwitchStreamTermination();
    }

    if (mResolutionChangeOccurring)
    {
        CheckForResolutionChangeTermination();
    }

    return DecodeBufferManagerNoError;
}


// ---------------------------------------------------------------------
//
//      Function to free up components of a buffer, this is a memory
//  management issue, and cannot result in freeing up the whole buffer.
//

DecodeBufferManagerStatus_t   DecodeBufferManager_Base_c::ReleaseBufferComponents(
    Buffer_t         Buffer,
    DecodeBufferUsage_t  ForUse)
{
    unsigned int             i;
    DecodeBufferContextRecord_t *BufferContext  = GetBufferContext(Buffer);

    OS_LockMutex(&Lock);

    for (i = 0; i < NUMBER_OF_COMPONENTS; i++)
    {
        if ((BufferContext->StructureComponent[i].Usage & ForUse) != 0)
        {
            BufferContext->StructureComponent[i].Usage  &= ~ForUse;

            if (BufferContext->StructureComponent[i].Usage == NotUsed)
            {
                // Actually release component
                Buffer->DetachBuffer(BufferContext->StructureComponent[i].Buffer);
                memset(&BufferContext->StructureComponent[i], 0, sizeof(DecodeBufferRequestComponentStructure_t));
            }
        }
    }

    OS_UnLockMutex(&Lock);
    return DecodeBufferManagerNoError;
}


// ---------------------------------------------------------------------
//
//      Functions to handle reference frame usage, allowing us to free
//  up those components that are for reference only at an appropriate
//  point.
//

DecodeBufferManagerStatus_t   DecodeBufferManager_Base_c::IncrementReferenceUseCount(Buffer_t    Buffer)
{
    DecodeBufferContextRecord_t     *BufferContext  = GetBufferContext(Buffer);
    OS_LockMutex(&Lock);
    BufferContext->ReferenceUseCount++;
    Buffer->IncrementReferenceCount();
    OS_UnLockMutex(&Lock);
    return DecodeBufferManagerNoError;
}


// --------------------


DecodeBufferManagerStatus_t   DecodeBufferManager_Base_c::DecrementReferenceUseCount(Buffer_t    Buffer)
{
    DecodeBufferContextRecord_t     *BufferContext  = GetBufferContext(Buffer);
    OS_LockMutex(&Lock);
    BufferContext->ReferenceUseCount--;

    if (BufferContext->ReferenceUseCount == 0)
    {
        OS_UnLockMutex(&Lock);
        ReleaseBufferComponents(Buffer, ForReference);
        OS_LockMutex(&Lock);
    }

    Buffer->DecrementReferenceCount();
    OS_UnLockMutex(&Lock);
    return DecodeBufferManagerNoError;
}


// ---------------------------------------------------------------------
//
//      Accessor functions
//
BufferPool_t     DecodeBufferManager_Base_c::ComponentPool(DecodeBufferComponentType_t  Component)
{
    return ComponentData[Component].BufferPool;
}


BufferFormat_t   DecodeBufferManager_Base_c::ComponentDataType(DecodeBufferComponentType_t  Component)
{
    return ComponentData[Component].ComponentDescriptor.DataType;
}

// --------------------

unsigned int    DecodeBufferManager_Base_c::ComponentBorder(DecodeBufferComponentType_t Component,
                                                            unsigned int            Index)
{
    return ComponentData[Component].ComponentDescriptor.ComponentBorders[Index];
}

// --------------------

void    *DecodeBufferManager_Base_c::ComponentAddress(DecodeBufferComponentType_t   Component,
                                                      AddressType_t           AlternateAddressType)
{
    return ComponentData[Component].AllocatorMemory[AlternateAddressType];
}

// --------------------

bool         DecodeBufferManager_Base_c::ComponentPresent(
    Buffer_t            Buffer,
    DecodeBufferComponentType_t Component)
{
    DecodeBufferContextRecord_t     *BufferContext  = GetBufferContext(Buffer);
    return (BufferContext->StructureComponent[Component].Usage != NotUsed);
}

// --------------------

void         DecodeBufferManager_Base_c::ComponentDumpViaRelay(
    Buffer_t                     Buffer,
    DecodeBufferComponentType_t  Component,
    unsigned int                 id,
    unsigned int                 type)
{
    OS_LockMutex(&Lock);

    if (ComponentData[Component].BufferPool != NULL)
    {
        DecodeBufferContextRecord_t *BufferContext  = GetBufferContext(Buffer);
        unsigned char *BufferBase;
        BufferContext->StructureComponent[Component].Buffer->ObtainDataReference(NULL, NULL, (void **)(&BufferBase), PhysicalAddress);
        unsigned char *Data = (unsigned char *)(BufferBase + BufferContext->StructureComponent[Component].ComponentOffset[0]);
        unsigned int   Len  = (BufferContext->StructureComponent[Component].Size -  BufferContext->StructureComponent[Component].ComponentOffset[0]);

        st_relayfs_map_write_se(id, type, Data, ST_RELAY_PHYSICAL_ADDRESS, Len, false);
    }

    OS_UnLockMutex(&Lock);
}

// --------------------

unsigned int     DecodeBufferManager_Base_c::ComponentSize(
    Buffer_t            Buffer,
    DecodeBufferComponentType_t Component)
{
    DecodeBufferContextRecord_t     *BufferContext  = GetBufferContext(Buffer);
    return BufferContext->StructureComponent[Component].Size -  BufferContext->StructureComponent[Component].ComponentOffset[0];
}

// --------------------

void        *DecodeBufferManager_Base_c::ComponentBaseAddress(
    Buffer_t            Buffer,
    DecodeBufferComponentType_t Component)
{
    DecodeBufferContextRecord_t     *BufferContext  = GetBufferContext(Buffer);
    return BufferContext->StructureComponent[Component].BufferBase + BufferContext->StructureComponent[Component].ComponentOffset[0];
}

// --------------------

void        *DecodeBufferManager_Base_c::ComponentBaseAddress(
    Buffer_t            Buffer,
    DecodeBufferComponentType_t Component,
    AddressType_t           AlternateAddressType)
{
    DecodeBufferContextRecord_t *BufferContext  = GetBufferContext(Buffer);
    unsigned char           *BufferBase;
    BufferContext->StructureComponent[Component].Buffer->ObtainDataReference(NULL, NULL, (void **)(&BufferBase), AlternateAddressType);
    return BufferBase + BufferContext->StructureComponent[Component].ComponentOffset[0];
}

// --------------------

unsigned int     DecodeBufferManager_Base_c::ComponentDimension(
    Buffer_t            Buffer,
    DecodeBufferComponentType_t Component,
    unsigned int            DimensionIndex)
{
    DecodeBufferContextRecord_t     *BufferContext  = GetBufferContext(Buffer);
    return BufferContext->StructureComponent[Component].Dimension[DimensionIndex];
}

// --------------------

unsigned int     DecodeBufferManager_Base_c::ComponentStride(
    Buffer_t            Buffer,
    DecodeBufferComponentType_t Component,
    unsigned int            DimensionIndex,
    unsigned int            ComponentIndex)
{
    DecodeBufferContextRecord_t     *BufferContext  = GetBufferContext(Buffer);
    return BufferContext->StructureComponent[Component].Strides[DimensionIndex][ComponentIndex];
}

// --------------------

unsigned int     DecodeBufferManager_Base_c::DecimationFactor(
    Buffer_t            Buffer,
    unsigned int            DecimationIndex)
{
    DecodeBufferContextRecord_t     *BufferContext  = GetBufferContext(Buffer);
    return BufferContext->Request.DecimationFactors[DecimationIndex];
}

// --------------------

unsigned int     DecodeBufferManager_Base_c::LumaSize(Buffer_t          Buffer,
                                                      DecodeBufferComponentType_t Component)
{
    DecodeBufferContextRecord_t     *BufferContext  = GetBufferContext(Buffer);
    return BufferContext->StructureComponent[Component].ComponentOffset[1] - BufferContext->StructureComponent[Component].ComponentOffset[0];
}

// --------------------

unsigned char   *DecodeBufferManager_Base_c::Luma(Buffer_t          Buffer,
                                                  DecodeBufferComponentType_t Component)
{
    DecodeBufferContextRecord_t     *BufferContext  = GetBufferContext(Buffer);
    return BufferContext->StructureComponent[Component].BufferBase + BufferContext->StructureComponent[Component].ComponentOffset[0];
}

// --------------------

unsigned char   *DecodeBufferManager_Base_c::Luma(Buffer_t          Buffer,
                                                  DecodeBufferComponentType_t Component,
                                                  AddressType_t           AlternateAddressType)
{
    DecodeBufferContextRecord_t     *BufferContext  = GetBufferContext(Buffer);
    unsigned char                   *BufferBase;
    BufferContext->StructureComponent[Component].Buffer->ObtainDataReference(NULL, NULL, (void **)(&BufferBase), AlternateAddressType);
    SE_ASSERT(BufferBase != NULL); // cannot be empty
    return BufferBase + BufferContext->StructureComponent[Component].ComponentOffset[0];
}

// --------------------

unsigned int     DecodeBufferManager_Base_c::ChromaSize(Buffer_t            Buffer,
                                                        DecodeBufferComponentType_t    Component)
{
    DecodeBufferContextRecord_t     *BufferContext  = GetBufferContext(Buffer);
    return BufferContext->StructureComponent[Component].Size - BufferContext->StructureComponent[Component].ComponentOffset[1];
}

// --------------------

unsigned char   *DecodeBufferManager_Base_c::Chroma(Buffer_t            Buffer,
                                                    DecodeBufferComponentType_t Component)
{
    DecodeBufferContextRecord_t     *BufferContext  = GetBufferContext(Buffer);
    return BufferContext->StructureComponent[Component].BufferBase + BufferContext->StructureComponent[Component].ComponentOffset[1];
}

// --------------------

unsigned char   *DecodeBufferManager_Base_c::Chroma(Buffer_t            Buffer,
                                                    DecodeBufferComponentType_t Component,
                                                    AddressType_t           AlternateAddressType)
{
    DecodeBufferContextRecord_t     *BufferContext  = GetBufferContext(Buffer);
    unsigned char                   *BufferBase;
    BufferContext->StructureComponent[Component].Buffer->ObtainDataReference(NULL, NULL, (void **)(&BufferBase), AlternateAddressType);
    SE_ASSERT(BufferBase != NULL); // cannot be empty
    return BufferBase + BufferContext->StructureComponent[Component].ComponentOffset[1];
}

// --------------------

unsigned int     DecodeBufferManager_Base_c::RasterSize(Buffer_t            Buffer,
                                                        DecodeBufferComponentType_t    Component)
{
    DecodeBufferContextRecord_t     *BufferContext  = GetBufferContext(Buffer);
    return BufferContext->StructureComponent[Component].Size - BufferContext->StructureComponent[Component].ComponentOffset[0];
}

// ---------------------------------------------------------------------
//
//      PRIVATE Function to create a pool for a buffer component
//

DecodeBufferManagerStatus_t   DecodeBufferManager_Base_c::CreateComponentPool(unsigned int  Component)
{
    DecodeBufferComponentData_t *Data   = &ComponentData[Component];
    PlayerStatus_t           Status;
    allocator_status_t       AStatus;
    BufferType_t             BufferType;

    //
    // Ensure that the pool type exists
    //
    Status = BufferManager->FindBufferDataType(Data->ComponentDescriptor.DataDescriptor->TypeName, &BufferType);
    if (Status != BufferNoError)
    {
        Status = BufferManager->CreateBufferDataType(Data->ComponentDescriptor.DataDescriptor, &BufferType);
        if (Status != BufferNoError)
        {
            SE_ERROR("Failed to create component buffer type '%s'\n",
                     Data->ComponentDescriptor.DataDescriptor->TypeName);
            return DecodeBufferManagerError;
        }
    }

    bool    AllowCross64MbBoundary;
    if (ModuleParameter_Support64MbyteCrossing())
    {
        // no VDP 64Mbyte crossing HW limitation
        AllowCross64MbBoundary = true;
    }
    else
    {
        // limitation for manifestation components which can be processed by VDP
        AllowCross64MbBoundary = (Component != PrimaryManifestationComponent) && (Component != DecimatedManifestationComponent);
    }

    unsigned int MemoryAccessType = (mMediaType == STM_SE_MEDIA_AUDIO) ? MEMORY_AUDIO_MEDIA : MEMORY_VIDEO_MEDIA;
    if (mMediaType == STM_SE_MEDIA_AUDIO)
    {
        switch (Data->ComponentDescriptor.DefaultAddressMode)
        {
        case CachedAddress:
            // Below flag is converted to ICS_HOST_UNCACHED to declare uncached ICS region on host.
            MemoryAccessType |= MEMORY_ICS_CACHED_ACCESS;
            MemoryAccessType |= MEMORY_VMA_CACHED_ACCESS;
            break;
        case PhysicalAddress:
            MemoryAccessType |= MEMORY_ICS_UNCACHED_ACCESS;
            MemoryAccessType |= MEMORY_VMA_UNCACHED_ACCESS;
            break;
        default:
            SE_FATAL("Invalid ComponentDescriptor.DefaultAddressMode:%d\n", Data->ComponentDescriptor.DefaultAddressMode);
            break;
        }
        Data->ComponentDescriptor.DefaultAddressMode = CachedAddress;
    }
    else
    {
        MemoryAccessType |= (Data->ComponentDescriptor.DefaultAddressMode == CachedAddress) ? MEMORY_DEFAULT_ACCESS : MEMORY_HWONLY_ACCESS;
    }

    switch (Data->ComponentDescriptor.DataDescriptor->AllocationSource)
    {
    case AllocateFromSuppliedBlock :
    {
        if (Data->AllocationPartitionSize > 0)
        {
            if (AdditionalMemoryAccessType & MEMORY_VMA_UNCACHED_ACCESS)
            {
                //clear the cached access bit as we require to set it uncached
                MemoryAccessType &= ~MEMORY_VMA_CACHED_ACCESS;
            }
            MemoryAccessType |= AdditionalMemoryAccessType;
            AStatus = PartitionAllocatorOpen(&Data->AllocatorMemoryDevice, Data->AllocationPartitionName,
                                             Data->AllocationPartitionSize, MemoryAccessType);

            if (AStatus != allocator_ok)
            {
                SE_ERROR("@ Failed to allocate memory (Component %d, Size %d, Partition %s)\n",
                         Component, Data->AllocationPartitionSize, Data->AllocationPartitionName);
                return PlayerInsufficientMemory;
            }
            Data->AllocatorMemory[CachedAddress]    = AllocatorUserAddress(Data->AllocatorMemoryDevice);
            Data->AllocatorMemory[PhysicalAddress]  = AllocatorPhysicalAddress(Data->AllocatorMemoryDevice);
        }
        else
        {
            SE_ERROR("Expect an AllocationPartitionSize>0 for Component %d\n", Component);
            Data->AllocatorMemory[CachedAddress] = NULL;
            Data->AllocatorMemory[PhysicalAddress] = NULL;
        }
        Status  = BufferManager->CreatePool(&Data->BufferPool, BufferType, NumberOfDecodeBuffers,
                                            Data->AllocationPartitionSize, Data->AllocatorMemory,
                                            NULL, NULL, AllowCross64MbBoundary,
                                            MEMORY_DEFAULT_ACCESS);
        SE_DEBUG(group_buffer, "0x%p create pool %d from supplied block  size %d partition name %s  buffer count%d\n", this, Component, Data->AllocationPartitionSize, Data->AllocationPartitionName,
                 NumberOfDecodeBuffers);
    }
    break;

    case AllocateIndividualSuppliedBlocks :
        Status  = BufferManager->CreatePool(&Data->BufferPool, BufferType, NumberOfDecodeBuffers,
                                            Data->IndividualBufferSize, NULL, Data->ArrayOfMemoryBlocks,
                                            NULL, AllowCross64MbBoundary,
                                            MEMORY_DEFAULT_ACCESS);
        break;

    case AllocateFromNamedDeviceMemory:
        Status  = BufferManager->CreatePool(&Data->BufferPool, BufferType, NumberOfDecodeBuffers,
                                            UNSPECIFIED_SIZE, NULL , NULL, Data->AllocationPartitionName, AllowCross64MbBoundary,
                                            MemoryAccessType);
        SE_DEBUG(group_buffer, "0x%p create pool from partition %s, max buffer %d\n", this, Data->AllocationPartitionName, NumberOfDecodeBuffers);
        break;

    default:
        Status  = BufferManager->CreatePool(&Data->BufferPool, BufferType, NumberOfDecodeBuffers,
                                            UNSPECIFIED_SIZE, NULL, NULL,
                                            NULL, AllowCross64MbBoundary,
                                            MEMORY_DEFAULT_ACCESS);
        break;
    }

    //
    // Check creation
    //
    if (Status != BufferNoError)
    {
        SE_ERROR("Failed to create component pool %d type '%s'\n", Component, Data->ComponentDescriptor.DataDescriptor->TypeName);
        return PlayerInsufficientMemory;
    }

    return DecodeBufferManagerNoError;
}

// ---------------------------------------------------------------------
//
//      PRIVATE Function to destroy a pool for a buffer component
//

void    DecodeBufferManager_Base_c::DestroyComponentPool(unsigned int Component)
{
    if (ComponentData[Component].BufferPool != NULL)
    {
        ReleaseComponentScratchBuffers(&ComponentData[Component].ScratchInfo);
        BufferManager->DestroyPool(ComponentData[Component].BufferPool);
        ComponentData[Component].BufferPool = NULL;

        AllocatorClose(&ComponentData[Component].AllocatorMemoryDevice);
    }
}

// ---------------------------------------------------------------------
//
//      PRIVATE Function to create the actual decode buffer pool
//

DecodeBufferManagerStatus_t   DecodeBufferManager_Base_c::CreateDecodeBufferPool()
{
    PlayerStatus_t      Status;
    BufferType_t        DecodeBufferType;

    //
    // If we are called twice, we stick with what we have
    //

    if (DecodeBufferPool != NULL)
    {
        return DecodeBufferManagerNoError;
    }

    //
    // Attempt to register the types for decode buffer
    ///
    if (Player == NULL)
    {
        SE_ERROR("Decode Buffer Manager has been reset\n");
        return DecodeBufferManagerError;
    }

    BufferManager = Player->GetBufferManager();
    Status      = BufferManager->FindBufferDataType(InitialDecodeBufferContextDescriptor.TypeName, &DecodeBufferType);

    if (Status != BufferNoError)
    {
        Status  = BufferManager->CreateBufferDataType(&InitialDecodeBufferContextDescriptor, &DecodeBufferType);

        if (Status != BufferNoError)
        {
            SE_ERROR("Failed to create the decode buffer type\n");
            SetComponentState(ComponentInError);
            return Status;
        }
    }

    //
    // Create the pool of decode buffers
    //
    Status = BufferManager->CreatePool(&DecodeBufferPool, DecodeBufferType, NumberOfDecodeBuffers);

    if (Status != BufferNoError)
    {
        SE_ERROR("Failed to create the decode buffer pool\n");
        SetComponentState(ComponentInError);
        return Status;
    }

//
    return DecodeBufferManagerNoError;
}

// ---------------------------------------------------------------------
//
//      PRIVATE Function to check the switch stream termination
//

DecodeBufferManagerStatus_t   DecodeBufferManager_Base_c::CheckForSwitchStreamTermination()
{
    unsigned int    i;
    unsigned int    BuffersInPool;
    unsigned int    BuffersWithNonZeroReferenceCount;
//
    OS_LockMutex(&Lock);
    StreamSwitchOccurring   = false;

    for (i = 0; i < NUMBER_OF_COMPONENTS; i++)
    {
        if (StreamSwitchcopyOfComponentData[i].BufferPool != NULL)
        {
            ReleaseComponentScratchBuffers(&StreamSwitchcopyOfComponentData[i].ScratchInfo);
            StreamSwitchcopyOfComponentData[i].BufferPool->GetPoolUsage(&BuffersInPool, &BuffersWithNonZeroReferenceCount);

            if (BuffersWithNonZeroReferenceCount == 0)
            {
                BufferManager->DestroyPool(StreamSwitchcopyOfComponentData[i].BufferPool);
                AllocatorClose(&StreamSwitchcopyOfComponentData[i].AllocatorMemoryDevice);
                memset(&StreamSwitchcopyOfComponentData[i], 0, sizeof(DecodeBufferComponentData_t));
            }
            else
            {
                // we care only of manifestation buffer .stream switch is considered as over if all manifestation buffer are free
                if ((i == PrimaryManifestationComponent) || (i == DecimatedManifestationComponent)) { StreamSwitchOccurring   = true; }
            }
        }
    }

    OS_UnLockMutex(&Lock);
    return DecodeBufferManagerNoError;
}
// ---------------------------------------------------------------------
//
//      PRIVATE Function to check the resolution chnage termination
//

DecodeBufferManagerStatus_t   DecodeBufferManager_Base_c::CheckForResolutionChangeTermination()
{
    unsigned int    i;
    unsigned int    BuffersInPool;
    unsigned int    BuffersWithNonZeroReferenceCount;

    OS_LockMutex(&Lock);
    mResolutionChangeOccurring   = false;

    for (i = 0; i < NUMBER_OF_COMPONENTS; i++)
    {
        if (mResolutionChangecopyOfComponentData[i].BufferPool != NULL)
        {
            ReleaseComponentScratchBuffers(&mResolutionChangecopyOfComponentData[i].ScratchInfo);
            mResolutionChangecopyOfComponentData[i].BufferPool->GetPoolUsage(&BuffersInPool, &BuffersWithNonZeroReferenceCount);

            if (BuffersWithNonZeroReferenceCount == 0)
            {
                BufferManager->DestroyPool(mResolutionChangecopyOfComponentData[i].BufferPool);

                AllocatorClose(&mResolutionChangecopyOfComponentData[i].AllocatorMemoryDevice);
                memset(&mResolutionChangecopyOfComponentData[i], 0, sizeof(DecodeBufferComponentData_t));
            }
            else
            {
                SE_WARNING("%p component %d have still %d buffers in use\n", this, i, BuffersWithNonZeroReferenceCount);
                mResolutionChangeOccurring  = true;
            }
        }
    }
    OS_UnLockMutex(&Lock);
    return DecodeBufferManagerNoError;
}

static inline const char *LookupComponentType(DecodeBufferComponentType_t Component)
{
    switch (Component)
    {
#define T(x) case x: return #x
        T(PrimaryManifestationComponent);
        T(DecimatedManifestationComponent);
        T(VideoDecodeCopy);
        T(VideoMacroblockStructure);
        T(VideoPostProcessControl);
        T(AdditionalMemoryBlock);
        T(UndefinedComponent);
#undef E
    default:
        return "Undefined";
    }
}

// ---------------------------------------------------------------------
//
//      PRIVATE Function to check the switch stream termination
//

DecodeBufferManagerStatus_t   DecodeBufferManager_Base_c::AllocateComponent(
    unsigned int             Component,
    DecodeBufferContextRecord_t *BufferContext, bool IsScratch)
{
    int i;
    PlayerStatus_t               Status;
    DecodeBufferComponentDescriptor_t   *Descriptor;
    DecodeBufferRequestComponentStructure_t *Structure;
    unsigned int                 Size;
    Buffer_t                 ReservedBuffers[MAX_BUFFER_RETRIES + 1];

    if (BufferContext == NULL)
    {
        SE_ERROR("Unable to get a buffer context\n");
        return DecodeBufferManagerError;
    }

    memset(&ReservedBuffers, 0, sizeof(ReservedBuffers));

    // Default values to avoid div by 0 later ...
    for (i = 0; i < MAX_BUFFER_DIMENSIONS; i++)
    {
        if (BufferContext->Request.DecimationFactors[i] == 0)
        {
            BufferContext->Request.DecimationFactors[i] = 1;
        }
    }
    if (BufferContext->Request.MacroblockSize == 0)
    {
        BufferContext->Request.MacroblockSize = 256;
    }
    // Fill out a structure record for this
    Status = FillOutComponentStructure(Component, BufferContext);

    if (Status != DecodeBufferManagerNoError)
    {
        SE_FATAL("Unable to fill out a component structure %d\n", Component);
    }

    //
    // get me a buffer with this in
    //
    Descriptor = &ComponentData[Component].ComponentDescriptor;
    Structure  = &BufferContext->StructureComponent[Component];
    Size       = Descriptor->DataDescriptor->FixedSize ? Descriptor->DataDescriptor->FixedSize : Structure->Size;

    unsigned int ItemAlignement = Descriptor->DataDescriptor->RequiredAlignment;
    if (ItemAlignement != 0)
    {
        // Alignment is a power of 2
        ItemAlignement = ItemAlignement - 1;
        Size = (Size + ItemAlignement) & (~ItemAlignement);
    }

    // Resolution change detection of primary manifestation
    if (((Component == PrimaryManifestationComponent) || (Component == VideoMacroblockStructure)) &&
        ComponentData[Component].AllowReallocation && ComponentData[Component].PoolCreated  && (ComponentData[Component].LastAllocationSize != 0) &&
        (Size > ComponentData[Component].LastAllocationSize)
        && (Size * ComponentData[Component].MaximumNumberOfUseBuffers > ComponentData[Component].AllocationPartitionSize))
    {
        SE_INFO(group_decodebufferm, "%p Resolution change from %d to %d - Max Buffer Count %d , Current Pool Size %d , Reallocated Pool %d for Main Manifestation component\n", this,
                ComponentData[Component].LastAllocationSize, Size, ComponentData[Component].MaximumNumberOfUseBuffers,
                ComponentData[Component].AllocationPartitionSize, Size * ComponentData[Component].MaximumNumberOfUseBuffers);


        // Checks and forces re-creation of pools which are not on-demand pool
        for (i = 0 ; i < NUMBER_OF_COMPONENTS ; i++)
        {
            if (!ComponentData[i].OnDemand && !ComponentData[i].ComponentDescriptor.DirectPoolUsage)
            {
                mResolutionChangeOccurring    = true;

                // search a free slot in the mResolutionChangecopyOfComponentData array
                for (unsigned int j = 0 ; j < NUMBER_OF_COMPONENTS ; j++)
                {
                    if (mResolutionChangecopyOfComponentData[j].BufferPool == NULL)
                    {
                        memcpy(&mResolutionChangecopyOfComponentData[j], &ComponentData[i], sizeof(DecodeBufferComponentData_t));
                        break;
                    }
                    if (j == (NUMBER_OF_COMPONENTS - 1))
                    {
                        SE_ERROR("Could not find available slot, Resolution changes are too near\n");
                    }
                }

                ComponentData[i].BufferPool  = NULL;
                ComponentData[i].PoolCreated = false;
                memset(&ComponentData[i].ScratchInfo, 0, sizeof(ComponentData[i].ScratchInfo));
            }
        }
    }

    //
    // First time or Resolution change, we need to create the pool if not yet created
    //
    if (ComponentData[Component].PoolCreated == false)
    {
        if (Descriptor->Usage & ForReference)     { ComponentData[Component].MaximumNumberOfUseBuffers = BufferContext->Request.ReferenceBufferCount; }
        if (Descriptor->Usage & ForManifestation) { ComponentData[Component].MaximumNumberOfUseBuffers = BufferContext->Request.ManifestationBufferCount; }

        ComponentData[Component].AllocationPartitionSize =  Size * ComponentData[Component].MaximumNumberOfUseBuffers;
        if (ComponentData[Component].OnDemand) { ComponentData[Component].AllocationPartitionSize = 0; }

        Status  = CreateComponentPool(Component);
        if (Status != DecodeBufferManagerNoError)
        {
            SE_ERROR("%p : Unable to Allocate whole memory pool for component %d on first buffer request\n", this, Component);
            DestroyComponentPool(Component);
            return DecodeBufferManagerFailedToAllocateComponents;
        }
        ComponentData[Component].BufferPool->SetMaximumNumberOfUseBuffers(ComponentData[Component].MaximumNumberOfUseBuffers);
        ComponentData[Component].PoolCreated = true;
        SE_INFO(group_decodebufferm, "[ALLOC] %p Dynamic pool creation for component %d (%s): "
                "buffer count %d buff size %d While partition size %d bytes\n",
                this, Component, LookupComponentType((DecodeBufferComponentType_t)Component),
                ComponentData[Component].MaximumNumberOfUseBuffers, Size,
                ComponentData[Component].AllocationPartitionSize);
    }

    // Get one buffer from scratch or from pool
    Structure->Buffer = GetScratchBuffer(&ComponentData[Component].ScratchInfo, Size);
    if (Structure->Buffer == NULL)
    {
        // Adjusting Maximum Count of Manifestation primary buffers
        //  only for Auto or on Demand pool
        if (ComponentData[Component].AllowReallocation)
        {
            unsigned int buffercount = MAX_DECODE_BUFFERS;

            if (Descriptor->Usage & ForReference) { buffercount = BufferContext->Request.ReferenceBufferCount; }
            if (Descriptor->Usage & ForManifestation) { buffercount = BufferContext->Request.ManifestationBufferCount; }
            if (buffercount !=  ComponentData[Component].MaximumNumberOfUseBuffers)
            {
                SE_DEBUG(group_decodebufferm, "%p : Changing buffer count to : %d for component %d\n", this, buffercount, Component);
                ComponentData[Component].MaximumNumberOfUseBuffers = buffercount;
                ComponentData[Component].BufferPool->SetMaximumNumberOfUseBuffers(buffercount);
            }
        }
        SE_VERBOSE(group_decodebufferm, "%p Request buffer for component %d size %d\n", this, Component, Size);

        BufferStatus_t BufferStatus = ComponentData[Component].BufferPool->GetBuffer(&Structure->Buffer, UNSPECIFIED_OWNER, Size,
                                                                                     BufferContext->Request.NonBlockingInCaseOfFailure);
        if (BufferStatus != BufferNoError)
        {
            SE_ERROR("Unable to get a component buffer %d\n", Component);
            return DecodeBufferManagerError;
        }
        SE_VERBOSE(group_decodebufferm, "%p Got buffer for component %d size %d\n", this, Component, Size);
    }
    Structure->Buffer->ObtainDataReference(NULL, NULL, (void **)(&Structure->BufferBase), Descriptor->DefaultAddressMode);
    //
    // Now attach this to the decode buffer, and give up our hold on it
    //
    BufferContext->DecodeBuffer->AttachBuffer(Structure->Buffer);
    Structure->Buffer->DecrementReferenceCount();
    //
    // Record the allocation size of this component
    //
    ComponentData[Component].LastAllocationSize     = Size;

    //
    //  Manage scratch buffer here
    //
    if (IsScratch)
    {
        AddScratchBuffer(&ComponentData[Component].ScratchInfo, Structure->Buffer, Size);
    }

    return DecodeBufferManagerNoError;
}


// ---------------------------------------------------------------------
//
//      Function to free up components of a buffer, this is a memory
//  management issue, and cannot result in freeing up the whole buffer.
//
void   DecodeBufferManager_Base_c::AheadReleaseReferenceComponents(Buffer_t         Buffer)
{
    OS_LockMutex(&Lock);
    DecodeBufferContextRecord_t *BufferContext  = GetBufferContext(Buffer);

    for (unsigned int i = 0; i < NUMBER_OF_COMPONENTS; i++)
    {
        // ignore ForDecode usage
        unsigned int usage = BufferContext->StructureComponent[i].Usage  & ~ForDecode;
        if (usage == ForReference)
        {
            // a component was used as reference and maybe as decode
            // Reference is release , so it can be moved to scratch area and reused.
            AddScratchBuffer(&ComponentData[i].ScratchInfo, BufferContext->StructureComponent[i].Buffer, BufferContext->StructureComponent[i].Size);
        }
    }
    OS_UnLockMutex(&Lock);
}

//
// Search for a scratch buffer with same size
//
Buffer_t  DecodeBufferManager_Base_c::GetScratchBuffer(DecodeBufferScratch_t *scratch, unsigned int Size)
{
    if ((Size == 0) || (Size == UNSPECIFIED_SIZE)) { return NULL; }
    OS_LockMutex(&ScratchLock);

    // if size is different from available scratch buffer size, scratch buffers should be
    // removed from the list. Refcount is decremented, if reaching zero buffer is available
    // if not, buffer is still used by decoding pipeline
    for (int i = 0 ; i < MAX_SCRATCH_BUFFERS ; i++)
    {
        if (scratch->Size[i] != Size)
        {
            ReleaseScratchBuffer(scratch, i);
        }
    }
    // search for one matching buffer
    for (int i = 0 ; i < MAX_SCRATCH_BUFFERS ; i++)
    {
        if (scratch->Size[i] == Size)
        {
            scratch->Size[i] = 0;
            SE_VERBOSE(group_decodebufferm, "%p component %p  out of scratch area %p  size %d\n", this, scratch, scratch->Buffer[i], Size);
            OS_UnLockMutex(&ScratchLock);
            return  scratch->Buffer[i];
        }
    }
    OS_UnLockMutex(&ScratchLock);
    return NULL;
}

//
// Return a scratch buffer of a component pool to previous owner
// Caller must hold ScratchLock.
//
void  DecodeBufferManager_Base_c::ReleaseScratchBuffer(DecodeBufferScratch_t *scratch, int bufferIndex)
{
    OS_AssertMutexHeld(&ScratchLock);

    if (scratch->Size[bufferIndex] != 0)
    {
        SE_VERBOSE(group_decodebufferm, "%p component %p  Clean up scratch area, buffer %p\n", this, scratch, scratch->Buffer[bufferIndex]);
        scratch->Buffer[bufferIndex]->DecrementReferenceCount();
        scratch->Buffer[bufferIndex] = NULL;
        scratch->Size[bufferIndex] = 0;
    }
}

//
// Return all scratch buffers of a component pool to previous owner
//
void  DecodeBufferManager_Base_c::ReleaseComponentScratchBuffers(DecodeBufferScratch_t *scratch)
{
    OS_LockMutex(&ScratchLock);
    for (int i = 0 ; i < MAX_SCRATCH_BUFFERS ; i++)
    {
        ReleaseScratchBuffer(scratch, i);
    }
    OS_UnLockMutex(&ScratchLock);
}

//
// Return all scratch buffers held by the decode buffer manager to previous owner
//
void  DecodeBufferManager_Base_c::ReleaseAllScratchBuffers()
{
    OS_LockMutex(&Lock);
    for (int i = 0; i < NUMBER_OF_COMPONENTS; i++)
    {
        if (ComponentData[i].BufferPool != NULL)
        {
            ReleaseComponentScratchBuffers(&ComponentData[i].ScratchInfo);
        }
    }
    OS_UnLockMutex(&Lock);
}

//
// Add a buffer to the scratch buffer list
//
void  DecodeBufferManager_Base_c::AddScratchBuffer(DecodeBufferScratch_t *scratch, Buffer_t  b, unsigned int Size)
{
    if (b == NULL) { return; }
    OS_LockMutex(&ScratchLock);

    // if size is different from available scratch buffer size, scratch buffers should be
    // moved out from the list. Refcount is decremented, if reaching zero buffer is available
    // if not buffer is still used by decoding pipeline
    for (unsigned int i = 0 ; i < MAX_SCRATCH_BUFFERS ; i++)
    {
        if (scratch->Size[i] != Size)
        {
            ReleaseScratchBuffer(scratch, i);
        }
    }

    // search for free slot in scratch list
    for (unsigned int i = 0 ; i < MAX_SCRATCH_BUFFERS ; i++)
    {
        if (scratch->Size[i] == 0)
        {
            SE_VERBOSE(group_decodebufferm, "%p component %p  in scratch area %p  size %d\n", this, scratch, b, Size);
            scratch->Size[i] = Size;
            scratch->Buffer[i] = b;
            b->IncrementReferenceCount();
            break;
        }
    }
    OS_UnLockMutex(&ScratchLock);
}

// ---------------------------------------------------------------------
//
//      PRIVATE Function to check the switch stream termination
//
DecodeBufferManagerStatus_t   DecodeBufferManager_Base_c::FillOutComponentStructure(
    unsigned int             Component,
    DecodeBufferContextRecord_t *BufferContext)
{
    unsigned int i;
    DecodeBufferComponentDescriptor_t   *Descriptor;
    DecodeBufferRequest_t           *Request;
    DecodeBufferRequestComponentStructure_t *Structure;
    //
    // Copy over the dimensions
    //
    Descriptor  = &ComponentData[Component].ComponentDescriptor;
    Request = &BufferContext->Request;
    Structure   = &BufferContext->StructureComponent[Component];
//
    Structure->DimensionCount   = Request->DimensionCount;

    if (Component != DecimatedManifestationComponent)
    {
        for (i = 0; i < Structure->DimensionCount; i++)
        {
            Structure->Dimension[i] = Request->Dimension[i];
        }
    }
    else
    {
        for (i = 0; i < Structure->DimensionCount; i++)
        {
            if (Request->DecimationFactors[i] == 0)
            {
                SE_ERROR("DecimationFactors[%d] == 0 -> should NOT happen!\n", i);
                Structure->Dimension[i] = Request->Dimension[i];
            }
            else
            {
                Structure->Dimension[i] = Request->Dimension[i] / Request->DecimationFactors[i];
            }
        }
    }

    //
    // Fill out a structure record for this component
    //
    switch (Descriptor->DataType)
    {
    case FormatLinear:
        Structure->ComponentCount   = 1;
        Structure->ComponentOffset[0]   = 0;
        Structure->Size         = Request->AdditionalMemoryBlockSize;
        break;

    case FormatPerMacroBlockLinear:
        Structure->Dimension[0]     = BUF_ALIGN_UP(Structure->Dimension[0], 0x3FU);  // Maximal rounding for HEVC 64x64 CTBs
        Structure->Dimension[1]     = BUF_ALIGN_UP(Structure->Dimension[1], 0x3FU);
        if (Request->AlignmentNeededforInterlaced)
        {
            // For interlaced HEVC video the PPB buffer is halved, so ensure each half is aligned
            Structure->Dimension[1]     = BUF_ALIGN_UP(Structure->Dimension[1], 0x7FU);
        }
        Structure->ComponentCount   = 1;
        Structure->ComponentOffset[0]   = 0;
        if (Request->MacroblockSize == 0)
        {
            SE_ERROR("Request->MacroblockSize==0 should NOT happen -> forcing it to 16x16\n");
            Request->MacroblockSize = 256;
        }
        Structure->Size         = Request->PerMacroblockMacroblockStructureSize * ((Structure->Dimension[0] * Structure->Dimension[1]) / Request->MacroblockSize);
        Structure->Size         += Request->PerMacroblockMacroblockStructureFifoSize;
        break;

    case FormatAudio:
        Structure->ComponentCount   = 1;
        Structure->ComponentOffset[0]   = 0;
        Structure->Strides[0][0]    = Structure->Dimension[0] / 8;
        Structure->Strides[1][0]    = Structure->Dimension[1] * Structure->Strides[0][0];
        Structure->Size         = (Structure->Dimension[0] * Structure->Dimension[1] * Structure->Dimension[2]) / 8;
        break;

    case FormatVideo420_HadesMacroBlock:
        // For Cannes2.5, HEVC prediction is splitted on 2 HWPE, and
        // needs 4 lines of pixels more for reference (copy) buffers
        Structure->Dimension[1] += BUFFER_HEIGHT_EXTEND;
    // fallthrough
    case FormatVideo420_PairedMacroBlock:
    case FormatVideo420_MacroBlock:
        Structure->Dimension[0] = BUF_ALIGN_UP(Structure->Dimension[0], (Descriptor->ComponentBorders[0] - 1));
        Structure->Dimension[1] = BUF_ALIGN_UP(Structure->Dimension[1], (Descriptor->ComponentBorders[1] - 1));

        if (Request->AlignmentNeededforInterlaced)
        {
            // For interlaced HEVC video the DecodeCopy buffer is halved, so ensure each half is aligned
            Structure->Dimension[1]     = BUF_ALIGN_UP(Structure->Dimension[1], 0x3FU);
        }
        Structure->ComponentOffset[0]   = 0;
        Structure->ComponentOffset[1] = Structure->Dimension[0] * Structure->Dimension[1];
        Structure->Strides[0][0] = Structure->Dimension[0];
        Structure->Strides[0][1] = Structure->Dimension[0];
        if (Request->PixelDepth == 10)
        {
            // OmegaH10 definition: Each block of 4x4 pixels (i.e. 16 Luma + 8 chroma)
            // is stored in 256 bits instead of 240 bits + one macroblock line added
            // OmegaH10 size =  1.5 * 10/8 * 256/240 * W * (H+16) ~= 2 W * (H+16)
            Structure->Size = 2 * Structure->Dimension[0] * (Structure->Dimension[1] + 16);
        }
        else
        {
            // MB Chroma scan order in the MB buffer format is Z-order.
            // A padding has to added to match Z scan order constraints.
            // Chroma MB size has to be aligned to 4: (16*8) * 4
            Structure->Size = Structure->ComponentOffset[1] + BUF_ALIGN_UP(((Structure->Dimension[0] * Structure->Dimension[1]) / 2), 0x1FF);
        }

        Structure->ComponentCount = 2;
        break;

    case FormatVideo420_Raster2B:
        if (Descriptor->ComponentBorders[0] == 16)
        {
            Structure->Dimension[0] = BUF_ALIGN_UP(Structure->Dimension[0], 0xFU);
            Structure->Dimension[1] = BUF_ALIGN_UP(Structure->Dimension[1], 0xFU);
        }
        else if (Descriptor->ComponentBorders[0] == 64)
        {
            Structure->Dimension[0] = BUF_ALIGN_UP(Structure->Dimension[0], 0x3FU);
            Structure->Dimension[1] = BUF_ALIGN_UP(Structure->Dimension[1], 0x3FU);
        }
        else
        {
            // alignment on width is required because of decode hw constraints whereas
            // alignment on height is required because of display constraint
            Structure->Dimension[0] = BUF_ALIGN_UP(Structure->Dimension[0], 0x1FU);
            Structure->Dimension[1] = BUF_ALIGN_UP(Structure->Dimension[1], 0x1FU);
        }

        Structure->Strides[0][0]      = Structure->Dimension[0];
        Structure->Strides[0][1]      = Structure->Strides[0][0];
        Structure->ComponentOffset[0] = 0;
        Structure->ComponentOffset[1] = Structure->Strides[0][0] * Structure->Dimension[1];

        Structure->Size               = Structure->ComponentOffset[1] + ((Structure->Strides[0][0] * Structure->Dimension[1]) / 2);
        Structure->ComponentCount     = 2;
        break;

    case FormatVideo420_Raster2B_10B:

        // Buffer stride is aligned to 32
        Structure->Strides[0][0]      = BUF_ALIGN_UP(CONV_8TO10_BITS_CEILING(Structure->Dimension[0]), 0x1FU);
        Structure->Strides[0][1]      = BUF_ALIGN_UP(CONV_8TO10_BITS_CEILING(Structure->Dimension[0]), 0x1FU);
        Structure->Dimension[0]       = BUF_ALIGN_UP(Structure->Dimension[0], 0x1FU);
        Structure->Dimension[1]       = BUF_ALIGN_UP(Structure->Dimension[1], 0x1FU);
        Structure->ComponentOffset[0] = 0;
        Structure->ComponentOffset[1] = Structure->Strides[0][0] * Structure->Dimension[1];
        Structure->Size               = (Structure->ComponentOffset[1] * 3) / 2;
        Structure->ComponentCount     = 2;
        break;

    case FormatVideo422_Raster:
        //
        // Round up dimension 0 to 32, these buffers are used for dvp capture,
        // the capture hardware needs strides on a 16 byte boundary and
        // the display hardware needs strides on a 64 byte (32 pixel) boundary.
        //
        Structure->Dimension[0]            = BUF_ALIGN_UP(Structure->Dimension[0], 0x1FU);
        Structure->ComponentCount          = 1;
        Structure->ComponentOffset[0]      = 0;
        Structure->Strides[0][0]           = 2 * Structure->Dimension[0];
        Structure->Size                    = (2 * Structure->Dimension[0] * Structure->Dimension[1]);
        break;

    case FormatVideo420_Planar:
        Structure->Dimension[0]            = BUF_ALIGN_UP(Structure->Dimension[0] + Descriptor->ComponentBorders[0] * 2, 0xFU);
        Structure->Dimension[1]            = BUF_ALIGN_UP(Structure->Dimension[1] + Descriptor->ComponentBorders[1] * 2, 0xFU);
        Structure->ComponentCount          = 2;
        Structure->ComponentOffset[0]      = 0;
        Structure->ComponentOffset[1]      = Structure->Dimension[0] * Structure->Dimension[1];
        Structure->Strides[0][0]           = Structure->Dimension[0];
        Structure->Strides[0][1]           = Structure->Dimension[0] / 2;
        Structure->Size                    = (Structure->ComponentOffset[1] * 3) / 2;
        break;

    case FormatVideo420_PlanarAligned:
        Structure->Dimension[0]            = BUF_ALIGN_UP(Structure->Dimension[0] + Descriptor->ComponentBorders[0] * 2, 0x3FU);
        Structure->Dimension[1]            = BUF_ALIGN_UP(Structure->Dimension[1] + Descriptor->ComponentBorders[1] * 2, 0xFU);
        Structure->ComponentCount          = 2;
        Structure->ComponentOffset[0]      = 0;
        Structure->ComponentOffset[1]      = Structure->Dimension[0] * Structure->Dimension[1];
        Structure->Strides[0][0]           = Structure->Dimension[0];
        Structure->Strides[0][1]           = Structure->Dimension[0] / 2;
        Structure->Size                    = (Structure->ComponentOffset[1] * 3) / 2;
        break;

    case FormatVideo422_Planar:
        Structure->Dimension[0]            = BUF_ALIGN_UP(Structure->Dimension[0] + Descriptor->ComponentBorders[0] * 2, 0x3FU);
        Structure->Dimension[1]            = BUF_ALIGN_UP(Structure->Dimension[1] + Descriptor->ComponentBorders[1] * 2, 0x3FU);
        Structure->ComponentCount          = 2;
        Structure->ComponentOffset[0]      = 0;
        Structure->ComponentOffset[1]      = Structure->Dimension[0] * Structure->Dimension[1];
        Structure->Strides[0][0]           = Structure->Dimension[0];
        Structure->Strides[0][1]           = Structure->Dimension[0];
        Structure->Size                    = (Structure->ComponentOffset[1] * 2);
        break;

    case FormatVideo8888_ARGB:
        Structure->Dimension[0]            = BUF_ALIGN_UP(Structure->Dimension[0], 0x1FU);
        Structure->Dimension[1]            = BUF_ALIGN_UP(Structure->Dimension[1], 0x1FU);
        Structure->ComponentCount          = 1;
        Structure->ComponentOffset[0]      = 0;
        Structure->Strides[0][0]           = Structure->Dimension[0] * 4;
        Structure->Size                    = (Structure->Dimension[0] * Structure->Dimension[1] * 4);
        break;

    case FormatVideo888_RGB:
        Structure->Dimension[0]            = BUF_ALIGN_UP(Structure->Dimension[0], 0x1FU);
        Structure->Dimension[1]            = BUF_ALIGN_UP(Structure->Dimension[1], 0x1FU);
        Structure->ComponentCount          = 1;
        Structure->ComponentOffset[0]      = 0;
        Structure->Strides[0][0]           = Structure->Dimension[0] * 3;
        Structure->Size                    = (Structure->Dimension[0] * Structure->Dimension[1] * 3);
        break;

    case FormatVideo565_RGB:
        Structure->Dimension[0]            = BUF_ALIGN_UP(Structure->Dimension[0], 0x1FU);
        Structure->Dimension[1]            = BUF_ALIGN_UP(Structure->Dimension[1], 0x1FU);
        Structure->ComponentCount          = 1;
        Structure->ComponentOffset[0]      = 0;
        Structure->Strides[0][0]           = Structure->Dimension[0] * 2;
        Structure->Size                    = (Structure->Dimension[0] * Structure->Dimension[1] * 2);
        break;

    case FormatVideo422_YUYV:
        Structure->Dimension[0]            = BUF_ALIGN_UP(Structure->Dimension[0], 0x1FU);
        Structure->Dimension[1]            = BUF_ALIGN_UP(Structure->Dimension[1], 0x1FU);
        Structure->ComponentCount          = 1;
        Structure->ComponentOffset[0]      = 0;
        Structure->Strides[0][0]           = Structure->Dimension[0] * 2;
        Structure->Size                    = (Structure->Dimension[0] * Structure->Dimension[1] * 2);
        break;

    case FormatVideo422_Raster2B:
        if (Descriptor->ComponentBorders[0] == 16)
        {
            Structure->Dimension[0]        = BUF_ALIGN_UP(Structure->Dimension[0], 0xFU);
            Structure->Dimension[1]        = BUF_ALIGN_UP(Structure->Dimension[1], 0xFU) ;
        }
        else if (Descriptor->ComponentBorders[0] == 64)
        {
            Structure->Dimension[0]        = BUF_ALIGN_UP(Structure->Dimension[0], 0x3FU);
            Structure->Dimension[1]        = BUF_ALIGN_UP(Structure->Dimension[1], 0x3FU);
        }
        else
        {
            Structure->Dimension[0]        = BUF_ALIGN_UP(Structure->Dimension[0], 0x1FU);
            Structure->Dimension[1]        = BUF_ALIGN_UP(Structure->Dimension[1], 0x1FU);
        }
        Structure->ComponentCount          = 2;
        Structure->ComponentOffset[0]      = 0;
        Structure->ComponentOffset[1]      = Structure->Dimension[0] * Structure->Dimension[1];
        Structure->Strides[0][0]           = Structure->Dimension[0];
        Structure->Strides[0][1]           = Structure->Dimension[0];
        Structure->Size                    = (Structure->Dimension[0] * Structure->Dimension[1] * 2);
        break;

    case FormatVideo444_Raster:
        Structure->Dimension[0]            = BUF_ALIGN_UP(Structure->Dimension[0], 0x1FU);
        Structure->Dimension[1]            = BUF_ALIGN_UP(Structure->Dimension[1], 0x1FU);
        Structure->ComponentCount          = 1;
        Structure->ComponentOffset[0]      = 0;
        Structure->Strides[0][0]           = Structure->Dimension[0] * 3;
        Structure->Size                    = (Structure->Dimension[0] * Structure->Dimension[1] * 3);
        break;

    default:
        SE_ERROR("Unknown buffer format %d\n", Descriptor->DataType);
        return DecodeBufferManagerError;
    }

    return DecodeBufferManagerNoError;
}


// ---------------------------------------------------------------------
//
//      PRIVATE Function to get the buffer context
//

DecodeBufferContextRecord_t   *DecodeBufferManager_Base_c::GetBufferContext(Buffer_t     Buffer)
{
    DecodeBufferContextRecord_t     *Result;
    Buffer->ObtainDataReference(NULL, NULL, (void **)(&Result));
    return Result;
}
