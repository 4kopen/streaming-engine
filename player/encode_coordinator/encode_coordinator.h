/************************************************************************
Copyright (C) 2003-2013 STMicroelectronics. All Rights Reserved.

This file is part of the Streaming Engine.

Streaming Engine is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License version 2 as
published by the Free Software Foundation.

Streaming Engine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with Streaming Engine; see the file COPYING.  If not, write to the Free
Software Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307,
USA.

The Streaming Engine Library may alternatively be licensed under a
proprietary license from ST.
************************************************************************/

#ifndef H_ENCODE_COORDINATOR
#define H_ENCODE_COORDINATOR

#include "player_types.h"
#include "port.h"
#include "ring_generic.h"
#include "buffer.h"

#include "encode_stream.h"
#include "release_buffer_interface.h"
#include "encode_coordinator_interface.h"
#include "encode_coordinator_stream.h"

#define MAX_ENCODE_COORDINATOR_STREAM_NB    5   // Max number of streams that can be handled by encode coordinator
#define ENCODE_COORDINATOR_MAX_EVENT_WAIT   500 // Max wait for thread creation/termination (in milliseconds)
#define NRT_INACTIVE_SIGNAL_TIMEOUT         10000000 // Large timeout value used to detect not active streams

typedef enum EncodeCoordinatorState_e
{
    StartingStateStep0,
    StartingStateStep1,
    RunningState
} EncodeCoordinatorState_t;

class EncodeCoordinator_c : public EncodeCoordinatorInterface_c
{
public:
    EncodeCoordinator_c();
    virtual EncoderStatus_t FinalizeInit();
    virtual void            Halt();
    virtual ~EncodeCoordinator_c();

    virtual EncoderStatus_t Connect(EncodeStreamInterface_c  *EncodeStream,
                                    ReleaseBufferInterface_c *OriginalReleaseBufferItf,
                                    ReleaseBufferInterface_c **EncodeCoordinatorStreamReleaseBufferItf,
                                    Port_c **InputPort);

    virtual EncoderStatus_t Disconnect(EncodeStreamInterface_c *EncodeStream);
    virtual EncoderStatus_t Flush(EncodeStreamInterface_c *EncodeStream, bool FlushInputStageOnly);
    virtual void SignalNewStreamInput() { OS_SetEvent(&mInputStreamEvent); }

    void  ProcessEncodeCoordinator();

    void SetGlobalFlushAsked(bool GlobalFlushAsked) { mGlobalFlushAsked = GlobalFlushAsked; }
    bool IsGlobalFlushAsked() { return mGlobalFlushAsked; }

private:
    bool StreamsHaveGap(TimeStamp_c *GapEndTime);
    bool StreamsHaveData();
    bool StreamsUpdateTimeCompression(TimeStamp_c EndOfGapTime);
    bool StreamsReady();
    void StreamsEncode();
    bool StreamsGetNextCurrentTime(TimeStamp_c *nextCurrentTime);

    void  StartingStateStep0Processing();
    void  StartingStateStep1Processing();
    void  RunningStateProcessing();
    void  ProcessEncoderReturnedBuffer();
    void  ProcessPendingFlush();

    unsigned int                                  mEncodeCoordinatorStreamNb;
    EncodeCoordinatorStream_c                    *mEncodeCoordinatorStream[MAX_ENCODE_COORDINATOR_STREAM_NB];

    bool                                          mStreamValid[MAX_ENCODE_COORDINATOR_STREAM_NB];
    int                                           mInitialSyncIteration;
    unsigned int                                  mLastConnect;
    int                                           mLastStreamCount;
    TimeStamp_c                                   mHighestCurrentTime;
    uint64_t                                      mMinFrameDuration;

    OS_Mutex_t                                    mLock;
    OS_Event_t                                    mStartStopEvent;
    OS_Event_t                                    mInputStreamEvent;
    EncodeCoordinatorState_t                      mEncodeCoordinatorState;
    uint64_t                                      mTimeOut;
    TimeStamp_c                                   mCurrentTime;
    uint64_t                                      mStartingStateInitialTime_us;
    bool                                          mTerminating;
    bool                                          mGlobalFlushAsked;

    DISALLOW_COPY_AND_ASSIGN(EncodeCoordinator_c);
};

#endif // H_ENCODE_COORDINATOR
