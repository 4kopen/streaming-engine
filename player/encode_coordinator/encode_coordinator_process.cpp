/************************************************************************
Copyright (C) 2003-2013 STMicroelectronics. All Rights Reserved.

This file is part of the Streaming Engine.

Streaming Engine is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License version 2 as
published by the Free Software Foundation.

Streaming Engine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with Streaming Engine; see the file COPYING.  If not, write to the Free
Software Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307,
USA.

The Streaming Engine Library may alternatively be licensed under a
proprietary license from ST.
************************************************************************/

#include "encode_coordinator.h"

#undef TRACE_TAG
#define TRACE_TAG "EncodeCoordinator_c"

#define MAX_STARTCONDITION_WAIT   2000  // 2 seconds (typical stream offset is 1s)
#define MAX_STREAMCONNECT_WAIT    600   // After each stream addition, this delay
// is waited to allow a new stream to be
// connected until we move on to normal processing

void EncodeCoordinator_c::ProcessPendingFlush()
{
    for (unsigned int index = 0; index < MAX_ENCODE_COORDINATOR_STREAM_NB; index++)
    {
        if (mEncodeCoordinatorStream[index] != NULL)
        {
            mEncodeCoordinatorStream[index]->PerformPendingFlush();
        }
    }
}

void EncodeCoordinator_c::ProcessEncoderReturnedBuffer()
{
    for (unsigned int index = 0; index < MAX_ENCODE_COORDINATOR_STREAM_NB; index++)
    {
        if (mEncodeCoordinatorStream[index] != NULL)
        {
            mEncodeCoordinatorStream[index]->ProcessEncoderReturnedBuffer();
        }
    }
}

// Starting State processing
// Get the PTS of the first frame of all stream
// Set the higher PTS value as first pts for all streams
// Discard frames until each stream first frame has pts >= "first pts"
void EncodeCoordinator_c::StartingStateStep0Processing()
{
    SE_VERBOSE(group_encode_coordinator, "\n");
    unsigned int    StreamCount = mLastStreamCount;

    // We wait until each stream received a first frame
    bool    AllStreamReady = true;

    if (mLastConnect == 0)
    {
        mLastConnect = OS_GetTimeInMilliSeconds();
    }

    mInitialSyncIteration++;
    for (unsigned int index = 0; index < MAX_ENCODE_COORDINATOR_STREAM_NB; index++)
    {
        if (mEncodeCoordinatorStream[index] != NULL)
        {
            if (mStreamValid[index] == false)
            {
                SE_DEBUG(group_encode_coordinator, "Considering Stream 0x%p index %d\n", mEncodeCoordinatorStream[index], index);
                // Check first if we got the first frame for this stream
                if (mEncodeCoordinatorStream[index]->GetCurrentFrame())
                {
                    // EOS frame are transmitted directly
                    if (mEncodeCoordinatorStream[index]->TransmitEOS() == false)
                    {
                        mStreamValid[index] = true;
                        StreamCount++;
                        mLastConnect = OS_GetTimeInMilliSeconds();

                        TimeStamp_c streamTime;
                        uint64_t  FrameDuration = NRT_INACTIVE_SIGNAL_TIMEOUT;
                        // and determine the highest PTS at the same time
                        mEncodeCoordinatorStream[index]->GetTime(&streamTime);
                        if (!mHighestCurrentTime.IsValid() || (streamTime.IsValid() && streamTime > mHighestCurrentTime))
                        {
                            mHighestCurrentTime = streamTime;
                            mEncodeCoordinatorStream[index]->GetFrameDuration(&FrameDuration);
                            // ask for frame_duration to determine lowest value for mTimeOut
                            //time out set to a frame duration in ms
                            mMinFrameDuration = Min(mMinFrameDuration, (FrameDuration / 1000));
                        }

                        if (NotValidTime(mStartingStateInitialTime_us))
                        {
                            mStartingStateInitialTime_us = OS_GetTimeInMilliSeconds();
                        }

                        SE_DEBUG(group_encode_coordinator
                                 , "(mInitialSyncIteration %d] Got first frame for Stream 0x%p streamtime %lld mHighestCurrentTime %lld\n"
                                 , mInitialSyncIteration
                                 , mEncodeCoordinatorStream[index]
                                 , streamTime.NativeValue()
                                 , mHighestCurrentTime.NativeValue());
                    }
                    else { AllStreamReady = false; }
                }
                else
                {
                    SE_DEBUG(group_encode_coordinator, "Stream 0x%p index %d has no frame yet\n", mEncodeCoordinatorStream[index], index);
                    AllStreamReady = false;
                }
            }
        }
    }

    long long SinceLastConnect = OS_GetTimeInMilliSeconds() - mLastConnect;

    SE_VERBOSE(group_encode_coordinator, "Got %d streams (last %d) after %d iterations, last connect %d now %d (ms)\n"
               , StreamCount
               , mLastStreamCount
               , mInitialSyncIteration
               , mLastConnect
               , OS_GetTimeInMilliSeconds());

    // No stream connected, staying in Step0
    if (StreamCount == 0)
    {
        mLastStreamCount = StreamCount;
        SE_EXTRAVERB(group_encode_coordinator, "No stream !\n");
        return;
    }

    // Should we wait a bit more to get at least 2 streams to synchronize ?
    // If so, let's give a chance to others streams to join
    if (StreamCount < 2 && SinceLastConnect < MAX_STREAMCONNECT_WAIT)
    {
        SE_DEBUG(group_encode_coordinator, "Not yet time to move on with %d streams (last %d) after %d iterations, last connect %d now %d (ms)\n"
                 , StreamCount
                 , mLastStreamCount
                 , mInitialSyncIteration
                 , mLastConnect
                 , OS_GetTimeInMilliSeconds());

        // TODO(SH/PhT) check if this short sleeps really is helpful ?
        OS_SleepMilliSeconds(10);
        OS_SetEvent(&mInputStreamEvent);

        mLastStreamCount = StreamCount;
        return;
    }
    mLastStreamCount = StreamCount;


    // At least one stream has no data
    if (AllStreamReady == false)
    {
        // checking timeout
        uint64_t Now = OS_GetTimeInMilliSeconds();
        if (Now > (mStartingStateInitialTime_us + MAX_STARTCONDITION_WAIT))
        {
            // move to synchronised state with current streams
            mEncodeCoordinatorState = StartingStateStep1;
            mStartingStateInitialTime_us = Now;
            mCurrentTime = mHighestCurrentTime;
            mTimeOut = mMinFrameDuration;
            OS_SetEvent(&mInputStreamEvent);
            SE_WARNING("TimeOut Not all streams present after %d iterations, but move to Step1!\n", mInitialSyncIteration);
        }
        return;
    }

    // Got a frame for all streams, switch to StartingStateStep1
    mEncodeCoordinatorState = StartingStateStep1;
    mStartingStateInitialTime_us = OS_GetTimeInMilliSeconds();
    mCurrentTime = mHighestCurrentTime;
    mTimeOut = mMinFrameDuration;
    OS_SetEvent(&mInputStreamEvent);

    SE_DEBUG(group_encode_coordinator
             , "Got first frame for all %d streams after %d iterations (mHighestCurrentTime %lld), move to StartingStateStep1 with timeout set to %lld\n"
             , StreamCount
             , mInitialSyncIteration
             , mHighestCurrentTime.NativeValue()
             , mTimeOut);
}

//
// Discard frames until each stream first frame has pts >= "first pts"
//
void EncodeCoordinator_c::StartingStateStep1Processing()
{
    SE_VERBOSE(group_encode_coordinator, "\n");

    // Here we assume all streams got a first frame
    // We try to discard all frames under mHighestCurrentTime
    bool    AllStreamReady = true;
    for (unsigned index = 0; index < MAX_ENCODE_COORDINATOR_STREAM_NB; index++)
    {
        if (mEncodeCoordinatorStream[index] != NULL)
        {
            // Ask each stream if it succeeds to discard frames until mHighestCurrentTime
            if (mEncodeCoordinatorStream[index]->DiscardUntil(mCurrentTime) == false)
            {
                SE_DEBUG(group_encode_coordinator
                         , "Didn't Get all streams synchronized at time %lld\n"
                         , mCurrentTime.NativeValue());

                // At least one stream is not yet ready
                AllStreamReady = false;
            }
        }
    }

    if (AllStreamReady == false)
    {
        uint64_t Now = OS_GetTimeInMilliSeconds();
        if (Now > (mStartingStateInitialTime_us + MAX_STARTCONDITION_WAIT))
        {
            // We didn't manage to achieve synchronization of all streams before time-out
            // Force move to RunningState for valid streams
            mEncodeCoordinatorState = RunningState;
            OS_SetEvent(&mInputStreamEvent);
            SE_WARNING("Unable to achieve starting step1 synchronization, move to RunningState\n");
        }
        return;
    }

    // All streams are now synchronized, switch to RunningState and wake-up thread
    mEncodeCoordinatorState = RunningState;
    OS_SetEvent(&mInputStreamEvent);
    SE_DEBUG(group_encode_coordinator, "Got all streams synchronized, move to RunningState\n");
}

bool EncodeCoordinator_c::StreamsHaveGap(TimeStamp_c *StreamsGapEndTime)
{
    // Check for Gap present on all streams
    bool        AllStreamsInGap = true;
    int64_t mindelta = INVALID_TIME;

    *StreamsGapEndTime = TimeStamp_c();

    for (unsigned int index = 0; index < MAX_ENCODE_COORDINATOR_STREAM_NB; index++)
    {
        TimeStamp_c   GapEndTime;

        if (mEncodeCoordinatorStream[index] != NULL)
        {
            if (mEncodeCoordinatorStream[index]->HasGap(&GapEndTime))
            {
                int64_t delta = TimeStamp_c::DeltaUsec(GapEndTime, mCurrentTime);
                // search for the nearest end of GAP
                if (!ValidTime(mindelta) || delta < mindelta)
                {
                    *StreamsGapEndTime = GapEndTime;
                    mindelta = delta;
                }
            }
            else { AllStreamsInGap = false; }
        }
    }
    return AllStreamsInGap;
}

bool EncodeCoordinator_c::StreamsUpdateTimeCompression(TimeStamp_c EndOfGapTime)
{
    // Determine the max frame duration and the smallest gap of all stream gaps

    uint64_t     maxFrameDuration = 0;
    int64_t      minOffset = INVALID_TIME;

    for (unsigned int index = 0; index < MAX_ENCODE_COORDINATOR_STREAM_NB; index++)
    {
        uint64_t     streamFrameDuration = 0;
        if (mEncodeCoordinatorStream[index] != NULL)
        {
            if (mEncodeCoordinatorStream[index]->GetFrameDuration(&streamFrameDuration) == true)
            {
                TimeStamp_c streamEndOfGap;
                TimeStamp_c  streamTime;

                // get the EndOfGapTime for this stream
                if (mEncodeCoordinatorStream[index]->HasGap(&streamEndOfGap)
                    && mEncodeCoordinatorStream[index]->GetNextTime(&streamTime))
                {
                    int64_t curOffset = TimeStamp_c::DeltaUsec(streamEndOfGap, streamTime);

                    if (!ValidTime(minOffset)) { minOffset = curOffset; }

                    // select forward jump vs backward jump
                    if ((minOffset < 0) && (curOffset >= 0))
                    {
                        minOffset = curOffset;
                    }
                    else if ((minOffset >= 0) && (curOffset < 0))
                    {
                        // keep minOffset because is positive jump
                    }
                    else
                    {
                        if (curOffset < minOffset)
                        {
                            minOffset = curOffset;
                        }
                    }

                    SE_DEBUG(group_encode_coordinator
                             , "streamTime=%lld streamEndOfGap=%lld curOffset=%lld\n"
                             , streamTime.NativeValue()
                             , streamEndOfGap.NativeValue()
                             , curOffset);
                }

                if (streamFrameDuration > maxFrameDuration)
                {
                    maxFrameDuration = streamFrameDuration;
                }
            }
        }
    }

    if (minOffset == INVALID_TIME)
    {
        SE_ERROR("Failed to determine mininal Gap\n");
        return false;
    }

    if (maxFrameDuration == 0)
    {
        SE_ERROR("Failed to determine max frame duration Gap\n");
        return false;
    }

    // The selected offset is the smallest offset of all the stream gaps
    // It must be a multiple of the max frame duration.
    // The max frame duration is always the video one. So we must make sure
    // we never overlap 2 videos frames
    uint64_t absCurOffset = (minOffset > 0) ? minOffset : -minOffset;

    absCurOffset = (absCurOffset / (int64_t) maxFrameDuration) * (int64_t)maxFrameDuration;
    int64_t rounded_minOffset = (minOffset > 0) ? absCurOffset : -absCurOffset;

    // Adjust the value of the offset when it's a negative jump and that the stream with the min offset
    // is not the stream with the max frame duration. Otherwise the minOffset jumps after the Eof gap
    // of this stream.
    if ((rounded_minOffset != minOffset) && (minOffset < 0)) { rounded_minOffset -= maxFrameDuration; }

    SE_DEBUG(group_encode_coordinator
             , "minOffset=%lld aligned on %lld =>  minOffset=%lld\n"
             , minOffset
             , maxFrameDuration
             , rounded_minOffset);

    // Gap detected if greater than maximum frame duration
    if (absCurOffset > maxFrameDuration)
    {
        SE_DEBUG(group_encode_coordinator
                 , "Whole stream Gap at time %lld to %lld (rounded_minOffset %lld)\n"
                 , mCurrentTime.NativeValue()
                 , EndOfGapTime.NativeValue()
                 , rounded_minOffset);

        for (unsigned int index = 0; index < MAX_ENCODE_COORDINATOR_STREAM_NB; index++)
        {
            if (mEncodeCoordinatorStream[index] != NULL)
            {
                mEncodeCoordinatorStream[index]->AdjustJumpUs(rounded_minOffset);
            }
        }
        // Search the minCurrentTime of all the streams to set the new currenTime after time compression
        TimeStamp_c minStreamTime = TimeStamp_c();
        for (unsigned int index = 0; index < MAX_ENCODE_COORDINATOR_STREAM_NB; index++)
        {
            if (mEncodeCoordinatorStream[index] != NULL)
            {
                TimeStamp_c    t;
                SE_DEBUG(group_encode_coordinator
                         , "Fetching NextTime for %d\n"
                         , index);

                if (mEncodeCoordinatorStream[index]->GetNextTime(&t) == true)
                {
                    SE_DEBUG(group_encode_coordinator
                             , "  Next StreamTime is %lld\n"
                             , t.NativeValue());

                    if (!minStreamTime.IsValid() || t < minStreamTime)
                    {
                        minStreamTime = t;
                        SE_DEBUG(group_encode_coordinator
                                 , "  New minStreamTime is %lld\n"
                                 , minStreamTime.NativeValue());
                    }
                }
            }
        }
        mCurrentTime = minStreamTime;
        SE_DEBUG(group_encode_coordinator, "New mCurrentTime is %lld\n", mCurrentTime.NativeValue());

        return true;
    }
    else
    {
        // The detected gap is smaller than a max frame duration. Nothing to do
        SE_DEBUG(group_encode_coordinator
                 , "absCurOffset %lld <= maxFrameDuration %lld\n"
                 , absCurOffset
                 , maxFrameDuration);
    }

    return false;
}

bool EncodeCoordinator_c::StreamsReady()
{
    for (unsigned int index = 0; index < MAX_ENCODE_COORDINATOR_STREAM_NB; index++)
    {
        if (mEncodeCoordinatorStream[index] != NULL)
        {
            mEncodeCoordinatorStream[index]->ComputeState(mCurrentTime);
        }
    }

    for (unsigned int index = 0; index < MAX_ENCODE_COORDINATOR_STREAM_NB; index++)
    {
        if (mEncodeCoordinatorStream[index] != NULL)
        {
            if (mEncodeCoordinatorStream[index]->IsReady() == false)
            {
                // At least one stream is not ready yet
                return false;
            }
        }
    }

    // all stream ready , now check for timeout
    for (unsigned int index = 0; index < MAX_ENCODE_COORDINATOR_STREAM_NB; index++)
    {
        if (mEncodeCoordinatorStream[index] != NULL)
        {
            if (mEncodeCoordinatorStream[index]->IsTimeOut() == false)
            {
                // At least one stream is not in time out , so streams are considered as ready
                return true;
            }
        }
    }

    return false;
}

void EncodeCoordinator_c::StreamsEncode()
{
    for (unsigned int index = 0; index < MAX_ENCODE_COORDINATOR_STREAM_NB; index++)
    {
        if (mEncodeCoordinatorStream[index] != NULL)
        {
            // Ask stream to encode frame
            mEncodeCoordinatorStream[index]->Encode(mCurrentTime);
            mEncodeCoordinatorStream[index]->UpdateState();
        }
    }
}

bool EncodeCoordinator_c::StreamsGetNextCurrentTime(TimeStamp_c *nextCurrentTime)
{
    *nextCurrentTime = TimeStamp_c();
    TimeStamp_c minTime;
    bool         getTime = false;
    for (unsigned int index = 0; index < MAX_ENCODE_COORDINATOR_STREAM_NB; index++)
    {
        TimeStamp_c streamTime;

        if (mEncodeCoordinatorStream[index] != NULL)
        {
            SE_VERBOSE(group_encode_coordinator
                       , "[%d] Fetching next time\n"
                       , index);

            if (mEncodeCoordinatorStream[index]->GetNextTime(&streamTime) == true)
            {
                if (streamTime.IsValid() && (!minTime.IsValid() || streamTime < minTime))
                {
                    minTime = streamTime;
                    getTime = true;
                }
                SE_VERBOSE(group_encode_coordinator
                           , "[%d] minTime=%lld streamTime=%lld\n"
                           , index
                           , minTime.NativeValue()
                           , streamTime.NativeValue());
            }
        }
    }
    *nextCurrentTime = minTime;
    return getTime;
}

// Running State processing
void EncodeCoordinator_c::RunningStateProcessing()
{
    SE_VERBOSE(group_encode_coordinator, "\n");

    // Loops on all streams to check that all streams are ready for the CurrenTime
    // Ready means either a buffer is available to be transmitted or a buffer covering
    // this time has already been sent, or the stream is switched off
    if (StreamsReady() == false) { return; }

    // Check for Gap present on all streams
    TimeStamp_c    EndOfGapTime;

    if (StreamsHaveGap(&EndOfGapTime))
    {
        SE_EXTRAVERB(group_encode_coordinator
                     , "Streams have a gap : %lld\n"
                     , EndOfGapTime.NativeValue());

        if (EndOfGapTime != mCurrentTime)
        {
            SE_VERBOSE(group_encode_coordinator
                       , "Updating time compression with %lld\n"
                       , EndOfGapTime.NativeValue());

            if (StreamsUpdateTimeCompression(EndOfGapTime))
            {
                SE_EXTRAVERB(group_encode_coordinator, "Done. Exiting processing loop\n");
                return;
            }
        }
    }

    // At that stage some streams have data, so extract the buffers and transmit to encode
    StreamsEncode();

    // Determine next CurrentTime
    TimeStamp_c    nextCurrentTime;
    if (StreamsGetNextCurrentTime(&nextCurrentTime))
    {
        // Update CurrentTime
        mCurrentTime = nextCurrentTime;

        SE_EXTRAVERB(group_encode_coordinator
                     , "nextCurrentTime %lld\n"
                     , nextCurrentTime.NativeValue());
    }
}

// EncodeCoordinator thread
void EncodeCoordinator_c::ProcessEncodeCoordinator()
{
    SE_INFO(group_encode_coordinator, "thread started\n");

    // Signal thread has started
    OS_SetEvent(&mStartStopEvent);

    //
    // Main Loop
    //
    while (!mTerminating)
    {
        OS_WaitForEventAuto(&mInputStreamEvent, mTimeOut);
        OS_ResetEvent(&mInputStreamEvent);

        SE_EXTRAVERB(group_encode_coordinator, "wake up\n");

        // Lock access to mEncodeCoordinatorStream[] table
        OS_LockMutex(&mLock);

        // check and manage if buffers have been returned by encoder
        ProcessEncoderReturnedBuffer();

        // check and manage if a flush has to be performed
        ProcessPendingFlush();

        TimeStamp_c    nextCurrentTime;
        if (StreamsGetNextCurrentTime(&nextCurrentTime) == false)
        {
            // Handle cases where there are no more active streams: EOS or flush
            // Go back to StartingState0
            SE_DEBUG(group_encode_coordinator
                     , "No more active stream, move to StartingStateStep0\n");

            mTimeOut = NRT_INACTIVE_SIGNAL_TIMEOUT;
            mEncodeCoordinatorState = StartingStateStep0;
            // Reset initial sync variables
            memset(mStreamValid, 0, sizeof(mStreamValid));
            mInitialSyncIteration = 0;
            mLastConnect = 0;
            mLastStreamCount = 0;
            mHighestCurrentTime = TimeStamp_c();
            mMinFrameDuration = NRT_INACTIVE_SIGNAL_TIMEOUT;

            mStartingStateInitialTime_us = INVALID_TIME;
        }

        // do normal processing
        switch (mEncodeCoordinatorState)
        {
        case StartingStateStep0:
            StartingStateStep0Processing();
            break;

        case StartingStateStep1:
            StartingStateStep1Processing();
            break;

        case RunningState:
            RunningStateProcessing();
            break;

        default:
            SE_ERROR("Invalid EncodeCoordinatorState %d\n", mEncodeCoordinatorState);
            break;
        }

        //check again
        ProcessEncoderReturnedBuffer();

        // Unlock access to mEncodeCoordinatorStream[] table
        OS_UnLockMutex(&mLock);
    }

    OS_Smp_Mb(); // Read memory barrier: rmb_for_EncodeCoordinator_Terminating coupled with: wmb_for_EncodeCoordinator_Terminating

    // Signal we have terminated
    OS_SetEvent(&mStartStopEvent);
    SE_INFO(group_encode_coordinator, "thread terminated\n");
}

