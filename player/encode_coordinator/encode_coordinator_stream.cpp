/************************************************************************
Copyright (C) 2003-2013 STMicroelectronics. All Rights Reserved.

This file is part of the Streaming Engine.

Streaming Engine is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License version 2 as
published by the Free Software Foundation.

Streaming Engine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with Streaming Engine; see the file COPYING.  If not, write to the Free
Software Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307,
USA.

The Streaming Engine Library may alternatively be licensed under a
proprietary license from ST.
************************************************************************/

#include "encode_coordinator.h"
#include "encode_coordinator_stream.h"
#include "timestamps.h"

#undef TRACE_TAG
#define TRACE_TAG "EncodeCoordinatorStream_c"

#define RUNNING_STATE_STR(enum) case enum: return #enum
static const char *RunningStateStr(RunningState_t RunningState)
{
    switch (RunningState)
    {
        RUNNING_STATE_STR(RUNNINGSTATE);
        RUNNING_STATE_STR(GAPSTATE);
        RUNNING_STATE_STR(TIMEOUTSTATE);
        RUNNING_STATE_STR(OUTOFGAPSTATE);
        RUNNING_STATE_STR(EOSSTATE);
    default:
        return "???";
    }
}

#define OUTPUT_STR(enum) case enum: return #enum
static const char *OutputStr(DataOut_t DataOut)
{
    switch (DataOut)
    {
        OUTPUT_STR(NODATA);
        OUTPUT_STR(CURRENT);
        OUTPUT_STR(CLONE);
    default:
        return "???";
    }
}
// return true if  current_time in interval [low,high [
// low <= current_time < high
// "low" is signed because it can be negative in case of pts wrap around
bool EncodeCoordinatorStream_c::TimeInInterval(TimeStamp_c current_time, TimeStamp_c low, TimeStamp_c high)
{
    if (current_time < low) { return false; }
    if (current_time >= high) { return false; }
    return true;
}

EncodeCoordinatorStream_c::EncodeCoordinatorStream_c(EncodeStreamInterface_c *encodeStream,
                                                     EncodeCoordinatorInterface_c *encodeCoordinator,
                                                     ReleaseBufferInterface_c     *OriginalReleaseBufferInterface)
    : mLock()
    , mInputRing(NULL)
    , mReturnedRing(NULL)
    , mOutputPort(NULL)
    , mEncodeStream(encodeStream)
    , mEncodeCoordinator(encodeCoordinator)
    , mReleaseBufferInterface(OriginalReleaseBufferInterface)
    , mGotFirstFrame()
    , mFrameDuration(0)
    , mHasGap(false)
    , mReady(false)
    , mShift(false)
    , mIsLastRepeat(false)
    , mDiscontinuity()
    , mDataOut()
    , mLastReceivedTime(INVALID_TIME)
    , mPtsOffset(0)
    , mCurrentState()
    , mNextState()
    , mStreamTime()
    , mNextStreamTime()
    , mEndGapTime()
    , mNextEndGapTime()
    , mCurrentFrameSlot(NULL)
    , mLastCloneBuffer(NULL)
    , mAfterFrameSlot(NULL)
    , mEncoderBufferTypes(NULL)
    , mEncoder(NULL)
    , mTranscodeBuffer()
    , mMedia(STM_SE_ENCODE_STREAM_MEDIA_ANY)
    , mFlushAsked(false)
    , mFlushInputStageOnly(false)
    , mEndOfFlushEvent()
    , mEncIn(0)
    , mEncOut(0)
    , mEncCloneIn(0)
    , mEncCloneOut(0)
    , mBufferIn(0)
    , mBufferOut(0)
{
    OS_InitializeMutex(&mLock);
    OS_InitializeEvent(&mEndOfFlushEvent);
}

// FinalizeInit() function, to be called after the constructor
// Allocates all the resources needeed by the EncodeCoordinatorStream object
EncoderStatus_t EncodeCoordinatorStream_c::FinalizeInit()
{
    SE_DEBUG(group_encode_coordinator, "Stream 0x%p\n", mEncodeStream);

    // Allocate input ring
    mInputRing = RingGeneric_c::New(ENCODER_MAX_INPUT_BUFFERS, "EncodeCoordinatorStream_c::InputRing");
    if (mInputRing == NULL)
    {
        SE_ERROR("Stream 0x%p Failed to allocate input ring\n", mEncodeStream);
        return EncoderError;
    }

    // Allocate Returned ring
    mReturnedRing = RingGeneric_c::New(ENCODER_MAX_INPUT_BUFFERS, "EncodeCoordinatorStream_c::ReturnedRing");
    if (mReturnedRing == NULL)
    {
        SE_ERROR("Stream 0x%p Failed to allocate returned ring\n", mEncodeStream);
        delete mInputRing;
        return EncoderError;
    }

    // Retrieve Encoder object for buffer management
    mEncoder = mEncodeStream->GetEncoderObject();
    mEncoderBufferTypes = mEncoder->GetBufferTypes();

    // here we provide these information to Transcode Buffer
    for (unsigned int index = 0; index < ENCODER_MAX_INPUT_BUFFERS; index++)
    {
        mTranscodeBuffer[index].FinalizeInit(mEncoderBufferTypes);
    }

    // Set output port to EncodeStream input port
    mOutputPort = mEncodeStream->GetInputPort();

    // Init OK
    return EncoderNoError;
}

// Halt() function, to be called before the destructor
// Blocking until flush termination
void EncodeCoordinatorStream_c::Halt()
{
    SE_DEBUG(group_encode_coordinator, "Stream 0x%p %s\n", mEncodeStream, MEDIA_STR(mMedia));

    DoFlush();
}

EncodeCoordinatorStream_c::~EncodeCoordinatorStream_c()
{
    SE_DEBUG(group_encode_coordinator, "Stream 0x%p %s\n", mEncodeStream, MEDIA_STR(mMedia));

    delete mInputRing;
    delete mReturnedRing;

    OS_TerminateEvent(&mEndOfFlushEvent);
    OS_TerminateMutex(&mLock);
}

// EncodeStream flush, called by the coordinator
RingStatus_t EncodeCoordinatorStream_c::Flush(bool FlushInputStageOnly)
{
    SE_DEBUG(group_encode_coordinator, "Stream 0x%p %s>\n", mEncodeStream, MEDIA_STR(mMedia));

    // notify that a flush is requested
    mFlushAsked = true;
    mFlushInputStageOnly = FlushInputStageOnly;

    ((EncodeCoordinator_c *)mEncodeCoordinator)->SetGlobalFlushAsked(true);

    // Wake up coordinator process
    mEncodeCoordinator->SignalNewStreamInput();

    // Wait for flush to be terminated (will always finish, even the error)
    OS_WaitForEventAuto(&mEndOfFlushEvent, OS_INFINITE);

    ((EncodeCoordinator_c *)mEncodeCoordinator)->SetGlobalFlushAsked(false);

    OS_ResetEvent(&mEndOfFlushEvent);

    SE_DEBUG(group_encode_coordinator, "Stream 0x%p %s<\n", mEncodeStream, MEDIA_STR(mMedia));

    return RingNoError;
}

// New just decoded frame inserted into input ring
RingStatus_t EncodeCoordinatorStream_c::Insert(uintptr_t Value)
{
    RingStatus_t    RingStatus;

    SE_DEBUG(group_encode_coordinator, "Stream 0x%p %s Value %p\n", mEncodeStream, MEDIA_STR(mMedia), (void *)Value);
    if (Value != 0)
    {
        // Insert buffer in input ring
        RingStatus = mInputRing->Insert(Value);
        if (RingStatus != RingNoError)
        {
            SE_ERROR("Stream 0x%p %s Ring insert error - %p\n", mEncodeStream, MEDIA_STR(mMedia), (void *)Value);
            return RingStatus;
        }
        mBufferIn++;
    }

    // Signal new input to encode coordinator thread
    mEncodeCoordinator->SignalNewStreamInput();
    return RingNoError;
}

//***********************  all functions below are called from the internal thread ******************************
void EncodeCoordinatorStream_c::PerformPendingFlush()
{
    if (mFlushAsked == true)
    {
        DoFlush();
        mFlushAsked = false;
        OS_SetEvent(&mEndOfFlushEvent);
    }
}

void EncodeCoordinatorStream_c::DoFlush()
{
    SE_DEBUG(group_encode_coordinator, "Stream 0x%p %s >\n", mEncodeStream, MEDIA_STR(mMedia));

    // Release remaining buffers in the input ring
    if (mInputRing != NULL)
    {
        Buffer_t        Buffer;
        while (mInputRing->NonEmpty())
        {
            mInputRing->Extract((uintptr_t *)(&Buffer));

            if (Buffer != NULL)
            {
                mReleaseBufferInterface->ReleaseBuffer(Buffer);
                mBufferOut++;
            }
        }
    }

    // Ask Encoder to flush encode pipe according mFlushInputStageOnly
    mEncodeStream->InternalFlushStages(mFlushInputStageOnly);

    // Wait for all buffer return by encoder
    uint64_t WaitTimeout = OS_GetTimeInMicroSeconds() + WAIT_RETURN_ENCODER_MAX_TIME_US;
    while (BufferStateInEncoder())
    {
        ProcessEncoderReturnedBuffer();
        OS_SleepMilliSeconds(10);
        uint64_t Now = OS_GetTimeInMicroSeconds();
        if (Now > WaitTimeout)
        {
            BufferStateLog();
            break;
        }
    }

    // Release next frame if exists
    if (mAfterFrameSlot != NULL)
    {
        mAfterFrameSlot->BufferStateSet(BufferUnused);
        mReleaseBufferInterface->ReleaseBuffer(mAfterFrameSlot->GetBuffer());
        mBufferOut++;
        mAfterFrameSlot = NULL;
    }

    // Release current frame if exists
    if (mCurrentFrameSlot != NULL)
    {
        mCurrentFrameSlot->BufferStateSet(BufferUnused);
        mReleaseBufferInterface->ReleaseBuffer(mCurrentFrameSlot->GetBuffer());
        mBufferOut++;
        mCurrentFrameSlot = NULL;
    }

    // Reset state
    mNextState = EOSSTATE;
    mCurrentState = mNextState;
    mGotFirstFrame[mMedia] = false;

    SE_DEBUG(group_encode_coordinator, "Stream 0x%p %s released %d <\n", mEncodeStream, MEDIA_STR(mMedia), mBufferOut);
}

bool EncodeCoordinatorStream_c::BufferStateInEncoder()
{
    for (unsigned int index = 0; index < ENCODER_MAX_INPUT_BUFFERS; index++)
    {
        if (mTranscodeBuffer[index].IsBufferState(BufferOwnByEncoder)) { return true; }
    }
    return false;
}

#define MAX_STRING  80
void  EncodeCoordinatorStream_c::BufferStateLog()
{
    char s[MAX_STRING];
    for (unsigned int index = 0; index < ENCODER_MAX_INPUT_BUFFERS; index++)
    {
        if (mTranscodeBuffer[index].IsBufferStateUnused() == false)
        {
            SE_ERROR("Stream 0x%p %s Still some buffers not back from encode index %d state %s\n"
                     , mEncodeStream
                     , MEDIA_STR(mMedia)
                     , index
                     , mTranscodeBuffer[index].BufferStateString(s, sizeof(s)));
        }
    }
}

TranscodeBuffer_t EncodeCoordinatorStream_c::ExtractInputFrame()
{
    Buffer_t   buff = NULL;
    while (mInputRing->NonEmpty())
    {
        unsigned int index;
        uint64_t frameduration_us;
        bool    eos;

        TranscodeBuffer_t   tbuffer;
        mInputRing->Extract((uintptr_t *)(&buff));

        buff->GetIndex(&index);
        tbuffer = &mTranscodeBuffer[index];

        tbuffer->SetBuffer(buff);

        eos = tbuffer->IsEOS();
        tbuffer->GetBufferFrameDuration(&frameduration_us);

        SE_VERBOSE(group_encode_coordinator
                   , "Stream 0x%p %s INPUT_IO current frame 0x%p   index %d  pts:%lld frameduration_us:%lld eos:%d\n",
                   mEncodeStream
                   , MEDIA_STR(mMedia)
                   , buff
                   , index
                   , tbuffer->Pts().NativeValue()
                   , frameduration_us
                   , eos);

        mLastReceivedTime = OS_GetTimeInMicroSeconds();

        // We need to initialize the mStreamTime with current frame PTS
        // when getting the very first frame of the stream
        if (mMedia == STM_SE_ENCODE_STREAM_MEDIA_ANY)
        {
            mMedia = tbuffer->GetMedia();
            SE_DEBUG(group_encode_coordinator, "Stream 0x%p Media is %s\n", mEncodeStream, MEDIA_STR(mMedia));
        }

        // manage eos
        if (eos)
        {
            SE_DEBUG(group_encode_coordinator
                     , "Stream 0x%p %s Get EOS Pts:%lld Duration:%lld StreamTime:%lld\n"
                     , mEncodeStream
                     , MEDIA_STR(mMedia)
                     , tbuffer->Pts().NativeValue()
                     , frameduration_us
                     , mStreamTime.NativeValue());
            tbuffer->BufferStateSet(BufferDecode);
            return tbuffer;
        }

        // The flag FirstFrame informs that we must return the first frame extracted from the ring.
        // No need to check discontinuity if it is the first one.
        if (!mGotFirstFrame[mMedia])
        {
            if (!tbuffer->Pts().IsValid())
            {
                SE_DEBUG(group_encode_coordinator
                         , "Stream 0x%p %s Discarded first buffer with invalid Pts\n"
                         , mEncodeStream
                         , MEDIA_STR(mMedia));
                CheckForBufferRelease(tbuffer);
                continue;
            }
            mGotFirstFrame[mMedia] = true;
            mStreamTime = tbuffer->Pts();
            SE_DEBUG(group_encode_coordinator
                     , "Stream 0x%p %s Got First Pts :%lld  Duration:%lld StreamTime:%lld\n"
                     , mEncodeStream
                     , MEDIA_STR(mMedia)
                     , tbuffer->Pts().NativeValue()
                     , frameduration_us
                     , mStreamTime.NativeValue());
            tbuffer->BufferStateSet(BufferDecode);
            return tbuffer;
        }

        // Frame Duration is not correct or frame is in close past
        // The frame is discarded
        if ((frameduration_us <= MIN_FRAME_DURATION_US) ||
            (TimeInInterval(tbuffer->Pts()
                            , TimeStamp_c::AddUsec(mStreamTime, -mFrameDuration * MAX_FRAME_REWIND_DETECTION)
                            , mStreamTime)))
        {
            SE_DEBUG(group_encode_coordinator
                     , "Stream 0x%p %s Discarding Input frame Pts:%lld  Duration:%lld StreamTime:%lld\n"
                     , mEncodeStream
                     , MEDIA_STR(mMedia)
                     , tbuffer->Pts().NativeValue()
                     , frameduration_us
                     , mStreamTime.NativeValue());

            CheckForBufferRelease(tbuffer);
            continue;
        }

        // Frame PTS is as expected in interval [mStreamTime, mStreamTime + ( 1.5 x mFrameDuration) ]
        if (tbuffer->Pts() >= mStreamTime
            && tbuffer->Pts() < TimeStamp_c::AddUsec(mStreamTime, + (3 * mFrameDuration) / 2))
        {
            SE_DEBUG(group_encode_coordinator
                     , "Stream 0x%p %s Frame PTS is as expected in interval Pts:%lld  Duration:%lld StreamTime:%lld\n"
                     , mEncodeStream
                     , MEDIA_STR(mMedia)
                     , tbuffer->Pts().NativeValue()
                     , frameduration_us
                     , mStreamTime.NativeValue());

            tbuffer->BufferStateSet(BufferDecode);
            return tbuffer;
        }

        // A GAP on PTS is detected here. The next frame is used to confirm that it is a true GAP
        // and not a locally bad PTS value
        if (mInputRing->NonEmpty() == false)
        {
            mInputRing->InsertFront((uintptr_t)buff);
            return NULL;
        }

        // Extract next buffer
        TranscodeBuffer_t   tbuffernext;
        mInputRing->Extract((uintptr_t *)(&buff));
        buff->GetIndex(&index);
        tbuffernext = &mTranscodeBuffer[index];
        tbuffernext->SetBuffer(buff);
        eos = tbuffernext->IsEOS();

        // if eos, return the first extracted frame.
        // No need to check discontinuity as it is the last one.
        if (eos)
        {
            SE_DEBUG(group_encode_coordinator
                     , "Stream 0x%p %s Get EOS and discard pending buffer Pts:%lld  Duration:%lld StreamTime:%lld\n"
                     , mEncodeStream
                     , MEDIA_STR(mMedia)
                     , tbuffer->Pts().NativeValue()
                     , frameduration_us
                     , mStreamTime.NativeValue());
            mInputRing->InsertFront((uintptr_t)buff);
            tbuffer->BufferStateSet(BufferDecode);
            return tbuffer;
        }

        // Check that next buffer is in continuity interval [PTS,PTS+3/2frameduration_us]
        if (TimeInInterval(tbuffernext->Pts()
                           , tbuffer->Pts()
                           , TimeStamp_c::AddUsec(tbuffer->Pts(), (3 * frameduration_us) / 2)))
        {
            //  if yes transmit tbuffer
            SE_DEBUG(group_encode_coordinator
                     , "Stream 0x%p %s Confirm buffer PTS continuity Pts:%lld nextPts:%lld Duration:%lld StreamTime:%lld\n"
                     , mEncodeStream
                     , MEDIA_STR(mMedia)
                     , tbuffer->Pts().NativeValue()
                     , tbuffernext->Pts().NativeValue()
                     , frameduration_us
                     , mStreamTime.NativeValue());
            mInputRing->InsertFront((uintptr_t)buff);
            tbuffer->BufferStateSet(BufferDecode);
            return tbuffer;
        }
        else
        {
            SE_DEBUG(group_encode_coordinator
                     , "Stream 0x%p %s Discard buffer with unexpected Pts:%lld nextPts:%lld Duration:%lld StreamTime:%lld\n"
                     , mEncodeStream
                     , MEDIA_STR(mMedia)
                     , tbuffer->Pts().NativeValue()
                     , tbuffernext->Pts().NativeValue()
                     , frameduration_us
                     , mStreamTime.NativeValue());
            CheckForBufferRelease(tbuffer);
            mInputRing->InsertFront((uintptr_t)buff);
            continue;
        }
    }

    return NULL;
}

// Gets the PTS of the current frame
bool EncodeCoordinatorStream_c::GetTime(TimeStamp_c *StreamTime)
{
    SE_VERBOSE(group_encode_coordinator, "Stream 0x%p %s\n", mEncodeStream, MEDIA_STR(mMedia));

    if (mCurrentFrameSlot == NULL)
    {
        SE_DEBUG(group_encode_coordinator, "Stream 0x%p %s no current frame available\n"
                 , mEncodeStream
                 , MEDIA_STR(mMedia));
        return false;
    }
    *StreamTime = mCurrentFrameSlot->Pts();
    return true;
}

// In case of time compression, the next expected must be incremented to jump to the end of the gap
void EncodeCoordinatorStream_c::AdjustJumpUs(int64_t minOffset)
{
    uint64_t    currentFrameDuration = 0;
    int64_t     curOffset = 0;

    mCurrentFrameSlot->GetBufferFrameDuration(&currentFrameDuration);

    // The jump must be a multiple of the frame duration
    if (currentFrameDuration != 0)
    {
        // we must now adjust the offset regarding the currentFrameDuration
        // and the next stream time
        uint64_t absCurOffset = (minOffset > 0) ? minOffset : -minOffset;

        absCurOffset = (absCurOffset / (int64_t) currentFrameDuration) * (int64_t) currentFrameDuration;
        curOffset    = (minOffset > 0) ? absCurOffset : -absCurOffset;

        // Manage PTS offset to determine the current encoded time
        mPtsOffset += curOffset;

        SE_DEBUG(group_encode_coordinator, "Stream 0x%p %s mStreamTime %lld minOffset %lld\n"
                 , mEncodeStream
                 , MEDIA_STR(mMedia)
                 , mStreamTime.NativeValue()
                 , minOffset);

        mStreamTime = TimeStamp_c::AddUsec(mStreamTime, curOffset);

        mNextStreamTime = mStreamTime;

        SE_DEBUG(group_encode_coordinator, "Stream 0x%p %s Adjusted mStreamTime %lld offset %lld\n"
                 , mEncodeStream
                 , MEDIA_STR(mMedia)
                 , mStreamTime.NativeValue()
                 , curOffset);
    }
    else
    {
        SE_ERROR("Stream 0x%p %s < currentFrameDuration is 0 - %s\n"
                 , mEncodeStream
                 , MEDIA_STR(mMedia)
                 , RunningStateStr(mCurrentState));
    }
}

bool EncodeCoordinatorStream_c::GetFrameDuration(uint64_t *FrameDuration)
{
    *FrameDuration = 0;
    if (mCurrentFrameSlot == NULL)
    {
        // No current frame
        return false;
    }

    return mCurrentFrameSlot->GetBufferFrameDuration(FrameDuration);
}

// Transmit EOS buffer
bool EncodeCoordinatorStream_c::TransmitEOS()
{
    if ((mCurrentFrameSlot != NULL) && mCurrentFrameSlot->IsEOS())
    {
        SE_DEBUG(group_encode_coordinator
                 , "Stream 0x%p %s _OUT (transmit) :out %p(%lld) eos:1\n"
                 , mEncodeStream
                 , MEDIA_STR(mMedia)
                 , mCurrentFrameSlot->GetBuffer()
                 , mCurrentFrameSlot->Pts().NativeValue());
        SendBufferToEncoder(mCurrentFrameSlot);
        mCurrentFrameSlot->BufferStateClear(BufferDecode);
        mEncIn++;
        mCurrentFrameSlot = NULL;
        return true;
    }

    return false;
}

// Extracts current frame from the input ring
bool EncodeCoordinatorStream_c::GetCurrentFrame()
{
    // If current frame slot empty, fill it
    if (mCurrentFrameSlot == NULL) { mCurrentFrameSlot = ExtractInputFrame(); }
    return (mCurrentFrameSlot != NULL);
}

bool EncodeCoordinatorStream_c::GetNextTime(TimeStamp_c *streamTime)
{
    if (mCurrentFrameSlot == NULL || !mStreamTime.IsValid())
    {
        SE_DEBUG(group_encode_coordinator
                 , "mCurrentFrameSlot=%p mStreamTime.IsValid():%d\n"
                 , mCurrentFrameSlot
                 , mStreamTime.IsValid());

        // Happen if stream has received an EOS
        return false;
    }
    *streamTime = mStreamTime;
    return true;
}

// Check PTS of current frame and release it if it doesn't overlap highestCurrentTime
// then move to the next one until overlap is OK or no more buffer
bool EncodeCoordinatorStream_c::DiscardUntil(TimeStamp_c highestCurrentTime)
{
    // this is called from init phase
    // so use to set internal state value
    mNextState = RUNNINGSTATE;
    mCurrentState = RUNNINGSTATE;
    while ((mCurrentFrameSlot != NULL) || (mCurrentFrameSlot = ExtractInputFrame()))
    {
        TimeStamp_c    frameTime;
        uint64_t    frameDuration;
        frameTime = mCurrentFrameSlot->Pts();
        if (mCurrentFrameSlot->GetBufferFrameDuration(&frameDuration) == true)
            //FIXME     && (mCurrentFrameSlot->IsEOS() == false))
        {
            // If frame PTS in in the future stop discarding
            if (highestCurrentTime < frameTime)
            {
                SE_DEBUG(group_encode_coordinator
                         , "Stream 0x%p %s Buffer in future (time %lld, ref time %lld)\n"
                         , mEncodeStream
                         , MEDIA_STR(mMedia)
                         , frameTime.NativeValue()
                         , highestCurrentTime.NativeValue());

                return true;
            }

            if ((highestCurrentTime >= frameTime)
                && (highestCurrentTime < (TimeStamp_c::AddUsec(frameTime, frameDuration))))
            {
                SE_DEBUG(group_encode_coordinator
                         , "Stream 0x%p %s Buffer in synch (time %lld, ref time %lld)\n"
                         , mEncodeStream
                         , MEDIA_STR(mMedia)
                         , frameTime.NativeValue()
                         , highestCurrentTime.NativeValue());

                // The current time is now the next expected PTS
                mStreamTime = frameTime;
                return true;
            }

            // EOS frames are sent directly to encoder to trig EOS at application level
            if (TransmitEOS() == false)
            {
                SE_DEBUG(group_encode_coordinator
                         , "Stream 0x%p %s ReleaseBuffer %p\n"
                         , mEncodeStream, MEDIA_STR(mMedia)
                         , mCurrentFrameSlot);
                mCurrentFrameSlot->BufferStateSet(BufferUnused);
                mReleaseBufferInterface->ReleaseBuffer(mCurrentFrameSlot->GetBuffer());
                mBufferOut++;
                mCurrentFrameSlot = NULL;
            }
        }
    }

    SE_DEBUG(group_encode_coordinator
             , "Stream 0x%p %s No more input buffer to process\n"
             , mEncodeStream
             , MEDIA_STR(mMedia));
    return false;
}

// Try to release a buffer via the provided release_buffer_interface_c
// buffer could still be in use by Encoder
void EncodeCoordinatorStream_c::CheckForBufferRelease(TranscodeBuffer_t buffer)
{
    if (buffer == NULL) { return; }
    SE_VERBOSE(group_encode_coordinator
               , "Stream 0x%p %s Buffer %p\n"
               , mEncodeStream, MEDIA_STR(mMedia), buffer);

    if (buffer->IsBufferState(BufferOwnByEncoder))
    {
        return;
    }

    // still own by coordinator
    if (buffer->IsBufferState(BufferDecode))
    {
        return;
    }

    SE_VERBOSE(group_encode_coordinator
               , "Stream 0x%p %s Buffer not own by encoder %p\n"
               , mEncodeStream
               , MEDIA_STR(mMedia)
               , buffer);

    SE_DEBUG(group_encode_coordinator
             , "Stream 0x%p %s IO_RET %p Buffer(in %d,out %d):%d Enc(in %d,out %d):%d Clone(in %d :out %d )%d\n"
             , mEncodeStream
             , MEDIA_STR(mMedia)
             , buffer->GetBuffer()
             , mBufferIn
             , mBufferOut
             , mBufferIn - mBufferOut
             , mEncIn
             , mEncOut
             , mEncIn - mEncOut
             , mEncCloneIn
             , mEncCloneOut
             , mEncCloneIn - mEncCloneOut);

    // Deal with clone buffer only
    if (buffer->IsBufferState(BufferClone))
    {
        // for last buffer of the Gap , we take care of freeing the original source of the clone buffer
        if (buffer->IsBufferState(BufferLastClone))
        {
            TranscodeBuffer_t clonesource = buffer->BufferStateGetCloneReference();
            clonesource->BufferStateClear(BufferReference);
            SE_DEBUG(group_encode_coordinator
                     , "Stream 0x%p %s IO_RETURN Last clone reference: %p\n"
                     , mEncodeStream
                     , MEDIA_STR(mMedia)
                     , clonesource->GetBuffer());

            CheckForBufferRelease(clonesource);
        }
        buffer->GetBuffer()->DecrementReferenceCount(IdentifierGetInjectBuffer);
        buffer->BufferStateSet(BufferUnused);
    }
    // Deal with original buffer from decoder
    else
    {
        // if buffer is not used as a reference for a clone then it can be returned
        if (! buffer->IsBufferState(BufferReference))
        {
            buffer->BufferStateSet(BufferUnused);
            mReleaseBufferInterface->ReleaseBuffer(buffer->GetBuffer());
            mBufferOut++;
        }
        else
        {
            SE_DEBUG(group_encode_coordinator
                     , "Stream 0x%p %s Buffer %p is clone source\n"
                     , mEncodeStream
                     , MEDIA_STR(mMedia)
                     , buffer);
        }
    }
}

// Release callback called by EncodeStream after encoding
PlayerStatus_t EncodeCoordinatorStream_c::ReleaseBuffer(Buffer_c *buffer)
{
    (void) mReturnedRing->Insert((uintptr_t)(buffer));
    mEncodeCoordinator->SignalNewStreamInput();
    return PlayerNoError;
}

// Process buffer return by the encoders
void EncodeCoordinatorStream_c::ProcessEncoderReturnedBuffer()
{
    Buffer_t        Buffer;
    while (mReturnedRing->NonEmpty())
    {
        mReturnedRing->Extract((uintptr_t *)(&Buffer));

        if (Buffer != NULL)
        {
            unsigned int index;
            TranscodeBuffer_t tbuffer;

            Buffer->GetIndex(&index);
            tbuffer = &mTranscodeBuffer[index];
            SE_ASSERT(tbuffer->GetBuffer() == Buffer);
            tbuffer->BufferStateClear(BufferOwnByEncoder);
            if (tbuffer->IsBufferState(BufferClone)) { mEncCloneOut++; }
            else { mEncOut++; }
            CheckForBufferRelease(tbuffer);
        }
    }
}

// Check if there is a gap between current and next frame
// If yes, set the mCurrentState in [entering_gap/in the gap/leaving_gap]
// return true if current time is on the frame before the gap or in the gap
bool EncodeCoordinatorStream_c::HasGap(TimeStamp_c *endOfTimeGap)
{
    *endOfTimeGap = mNextEndGapTime;
    return mHasGap;
}

// Checks if stream is ready for the CurrenTime, that is the case when:
// - a buffer is available to be transmitted
// - a buffer covering this time has already been sent
// - the stream is switched off
bool EncodeCoordinatorStream_c::IsReady()
{
    return mReady;
}

bool EncodeCoordinatorStream_c::IsTimeOut()
{
    return (mNextState == TIMEOUTSTATE);
}

void EncodeCoordinatorStream_c::SendBufferToEncoder(TranscodeBuffer_t buffer)
{
    buffer->BufferStateSet(BufferOwnByEncoder);
    mOutputPort->Insert((uintptr_t)(buffer->GetBuffer()));
}

void EncodeCoordinatorStream_c::RunningState(TimeStamp_c current_time)
{
    //  Previous sent frame covered this time
    //  Please not that we increase the range of the intervale with half a frame duration
    //  in case the the previous PTS is not exactly a frame duration multiple but slightly less.
    if ((mStreamTime.IsValid())
        && TimeInInterval(current_time
                          , TimeStamp_c::AddUsec(mStreamTime, - (mFrameDuration + (mFrameDuration / 2 * 3)))
                          , mStreamTime))
    {
        // a frame has already been sent
        mReady  = true;
        mDataOut = NODATA;
        return;
    }

    // special case: can happen only if first frame is EOS
    // in normal case EOS is detected on AfterFrameSlot
    if (mCurrentFrameSlot->IsEOS())
    {
        mReady  = true;
        mDataOut = CURRENT;
        mNextStreamTime = TimeStamp_c::AddUsec(mStreamTime, mFrameDuration);
        mDiscontinuity = STM_SE_DISCONTINUITY_EOS;
        mShift = true;
        mNextState = EOSSTATE;
        return;
    }

    if (mAfterFrameSlot == NULL)
    {
        mReady  = false;
        mDataOut = NODATA;
        return;
    }
    else
    {
        // Got last stream buffer
        if (mAfterFrameSlot->IsEOS())
        {
            mReady = true;
            mShift = true;
            mDataOut = CURRENT;
            mNextState = EOSSTATE;
            mNextStreamTime = TimeStamp_c::AddUsec(mStreamTime, mFrameDuration);
            return;
        }

        // Next frame is present, so check for a GAP or not
        if (TimeInInterval(mAfterFrameSlot->Pts()
                           , mCurrentFrameSlot->Pts()
                           , TimeStamp_c::AddUsec(mCurrentFrameSlot->Pts() , (3 * mFrameDuration) / 2)))
        {
            // No gap, so send current frame
            mReady = true;
            mNextState = RUNNINGSTATE;
            mDataOut = CURRENT;
            mNextStreamTime = mAfterFrameSlot->Pts();
            mShift = true;
            return;
        }
        else
        {
            // That is the beginning of a GAP
            mHasGap = false;
            mReady = true;
            mNextEndGapTime = mAfterFrameSlot->Pts();
            mNextState = GAPSTATE;
            mDataOut = CURRENT;
            // Discontinuity for Audio only , video is sent normaly
            if (mMedia == STM_SE_ENCODE_STREAM_MEDIA_AUDIO)
            {
                mDiscontinuity = STM_SE_DISCONTINUITY_FADEOUT;
            }
            mNextStreamTime = TimeStamp_c::AddUsec(mStreamTime, mFrameDuration);
            return;
        }
    }
}

void EncodeCoordinatorStream_c::GapState(TimeStamp_c current_time)
{
    // A GAP has been detected on all streams and current time has been forced to mEndGapTime
    if (TimeStamp_c::DeltaUsec(current_time, mEndGapTime) == 0)
    {
        SE_DEBUG(group_encode_coordinator
                 , "Stream 0x%p %s _GAP_: Jump from %lld to %lld\n"
                 , mEncodeStream, MEDIA_STR(mMedia)
                 , mStreamTime.NativeValue()
                 , mEndGapTime.NativeValue());
        mReady  = true;
        mHasGap = false;
        mNextState = OUTOFGAPSTATE;
        mDataOut = NODATA;
        mIsLastRepeat = true;
        mNextStreamTime = mEndGapTime;
        mShift = true;
        return;
    }

    // a frame has already been sent
    if ((mStreamTime.IsValid())
        && TimeInInterval(current_time
                          , TimeStamp_c::AddUsec(mStreamTime, - mFrameDuration)
                          , mStreamTime))
    {
        SE_DEBUG(group_encode_coordinator
                 , "Stream 0x%p %s _GAP_ a frame has already been sent, mStreamTime=%lld current_time=%lld mEndGapTime=%lld\n"
                 , mEncodeStream
                 , MEDIA_STR(mMedia)
                 , current_time.NativeValue()
                 , mStreamTime.NativeValue()
                 , mEndGapTime.NativeValue());
        mReady  = true;
        mHasGap = true;
        mDataOut = NODATA;
        return;
    }

    // filling the Gap with repeated frame
    // until time reaches last frame of the Gap
    if (TimeInInterval(current_time
                       , TimeStamp_c::AddUsec(mEndGapTime, -mFrameDuration)
                       , mEndGapTime))
    {
        SE_DEBUG(group_encode_coordinator, "Stream 0x%p %s _GAP_ last gap frame mStreamTime=%lld current_time=%lld mEndGapTime=%lld\n"
                 , mEncodeStream
                 , MEDIA_STR(mMedia)
                 , mStreamTime.NativeValue()
                 , current_time.NativeValue()
                 , mEndGapTime.NativeValue());

        // That is the last frame of the Gap
        mReady  = true;
        mHasGap = false;
        mNextState = OUTOFGAPSTATE;
        mDataOut = CLONE;
        mIsLastRepeat = true;
        if (mMedia == STM_SE_ENCODE_STREAM_MEDIA_AUDIO)
        {
            mDiscontinuity = STM_SE_DISCONTINUITY_MUTE;
        }
        mNextStreamTime = mEndGapTime ;
        mShift = true;
        return;
    }

    // too early to send a frame
    if (current_time < mStreamTime)
    {
        SE_DEBUG(group_encode_coordinator
                 , "%s _GAP_:  %lld too early to send a frame\n"
                 , MEDIA_STR(mMedia)
                 , mStreamTime.NativeValue());
        mReady  = true;
        mHasGap = true;
        mDataOut = NODATA;
        return;
    }

    // Cloning
    SE_DEBUG(group_encode_coordinator
             , "Stream 0x%p %s _GAP_: Cloning %lld to %lld current_time %lld\n"
             , mEncodeStream
             , MEDIA_STR(mMedia)
             , mStreamTime.NativeValue()
             , mEndGapTime.NativeValue()
             , current_time.NativeValue());

    mReady  = true;
    mHasGap = true;
    mDataOut = CLONE;

    if (mMedia == STM_SE_ENCODE_STREAM_MEDIA_AUDIO)
    {
        mDiscontinuity = STM_SE_DISCONTINUITY_MUTE;
    }

    mNextStreamTime = TimeStamp_c::AddUsec(mStreamTime, mFrameDuration);
}

void EncodeCoordinatorStream_c::OutOfGapState(TimeStamp_c current_time)
{
    if (mStreamTime.IsValid()
        && TimeInInterval(current_time
                          , TimeStamp_c::AddUsec(mStreamTime, - mFrameDuration)
                          , mStreamTime))
    {
        // a frame has already been sent
        mReady  = true;
        mHasGap = false;
        mDataOut = NODATA;
        return;
    }

    if (mAfterFrameSlot == NULL)
    {
        //  wait
        mReady  = false;
        mHasGap = false;
        mDataOut = NODATA;
        return;
    }
    else
    {
        // Just Got EOS
        if (mAfterFrameSlot->IsEOS())
        {
            mReady = true;
            mHasGap = false;
            mDataOut = CURRENT;
            mNextState = EOSSTATE;
            SE_VERBOSE(group_encode_coordinator
                       , "Got EOS, inserting FADEIN and CLOSED_GOP_REQUEST\n");
            if (mMedia == STM_SE_ENCODE_STREAM_MEDIA_AUDIO)
            {
                mDiscontinuity = STM_SE_DISCONTINUITY_FADEIN;
            }
            else if (mMedia == STM_SE_ENCODE_STREAM_MEDIA_VIDEO)
            {
                mDiscontinuity = STM_SE_DISCONTINUITY_CLOSED_GOP_REQUEST;
            }
            mNextState = EOSSTATE;
            mShift = true;
            mNextStreamTime = TimeStamp_c::AddUsec(mStreamTime, mFrameDuration);
            return;
        }

        // Next frame is present, so check for a  GAP again or not
        if (TimeInInterval(mAfterFrameSlot->Pts()
                           , mCurrentFrameSlot->Pts()
                           , TimeStamp_c::AddUsec(mCurrentFrameSlot->Pts(), (3 * mFrameDuration) / 2)))
        {
            SE_VERBOSE(group_encode_coordinator
                       , "No more gap, inserting FADEIN and CLOSED_GOP_REQUEST\n");
            // No more gap, so send current frame with FadeIN or NewGOP
            mReady = true;
            mHasGap = false;
            mDataOut = CURRENT;
            if (mMedia == STM_SE_ENCODE_STREAM_MEDIA_AUDIO)
            {
                mDiscontinuity = STM_SE_DISCONTINUITY_FADEIN;
            }
            else if (mMedia == STM_SE_ENCODE_STREAM_MEDIA_VIDEO)
            {
                mDiscontinuity = STM_SE_DISCONTINUITY_CLOSED_GOP_REQUEST;
            }
            mNextState = RUNNINGSTATE;
            mNextStreamTime = TimeStamp_c::AddUsec(mStreamTime, mFrameDuration);
            mShift = true;
            return;
        }
        else
        {
            // Entering inside a new gap
            mReady = true;
            mHasGap = false;
            mNextEndGapTime = mAfterFrameSlot->Pts();
            mNextState = GAPSTATE;
            mDataOut = CURRENT;
            SE_VERBOSE(group_encode_coordinator
                       , "Entering a new gap, mNextEndGapTime=%lld, inserting MUTE\n"
                       , mNextEndGapTime.NativeValue());
            if (mMedia == STM_SE_ENCODE_STREAM_MEDIA_AUDIO)
            {
                mDiscontinuity = STM_SE_DISCONTINUITY_MUTE;
            }
            mNextStreamTime = TimeStamp_c::AddUsec(mStreamTime, mFrameDuration);
            return;
        }
    }
}

void EncodeCoordinatorStream_c::TimeOutState(TimeStamp_c current_time)
{
    if (mStreamTime.IsValid()
        && TimeInInterval(current_time
                          , TimeStamp_c::AddUsec(mStreamTime, -mFrameDuration)
                          , mStreamTime))
    {
        // a frame has already been sent
        mReady  = true;
        mHasGap = false;
        mDataOut = NODATA;
        return;
    }

    if (mAfterFrameSlot == NULL)
    {
        // we remain in TimeOut and clone forever
        mReady = true;
        mHasGap = false;
        mDataOut = CLONE;
        if (mMedia == STM_SE_ENCODE_STREAM_MEDIA_AUDIO)
        {
            mDiscontinuity = STM_SE_DISCONTINUITY_MUTE;
        }
        mNextStreamTime = TimeStamp_c::AddUsec(mStreamTime, mFrameDuration);
        return;
    }
    else
    {
        // Got EOS
        if (mAfterFrameSlot->IsEOS())
        {
            SE_DEBUG(group_encode_coordinator
                     , "Stream 0x%p %s TimeOutState Get EOS, current_time: %lld duration %lld mAfter PTS %lld\n"
                     , mEncodeStream
                     , MEDIA_STR(mMedia)
                     , current_time.NativeValue()
                     , mFrameDuration
                     , mAfterFrameSlot->Pts().NativeValue());

            mReady = true;
            mIsLastRepeat = true;
            mDataOut = CLONE;
            mNextState = EOSSTATE;
            if (mMedia == STM_SE_ENCODE_STREAM_MEDIA_AUDIO) { mDiscontinuity = STM_SE_DISCONTINUITY_MUTE; }
            mShift = true;
            mNextStreamTime = TimeStamp_c::AddUsec(mStreamTime, mFrameDuration);
            return;
        }

        // next frame would be on time, so move to transition state
        if (TimeInInterval(current_time
                           , TimeStamp_c::AddUsec(mAfterFrameSlot->Pts(), -mFrameDuration)
                           , mAfterFrameSlot->Pts()))
        {
            SE_DEBUG(group_encode_coordinator
                     , "Stream 0x%p %s TimeOutState On time, current_time: %lld duration %lld mAfter PTS %lld\n"
                     , mEncodeStream, MEDIA_STR(mMedia)
                     , current_time.NativeValue()
                     , mFrameDuration
                     , mAfterFrameSlot->Pts().NativeValue());

            mReady  = true;
            mHasGap = false;
            mDataOut = CLONE;
            mIsLastRepeat = true;

            if (mMedia == STM_SE_ENCODE_STREAM_MEDIA_AUDIO)
            {
                mDiscontinuity = STM_SE_DISCONTINUITY_MUTE;
            }

            mNextStreamTime = TimeStamp_c::AddUsec(mStreamTime, mFrameDuration);
            mNextState = OUTOFGAPSTATE;
            return;
        }

        // Frame is in future, after-pts > current_time
        // move to  Gap state again
        if (mAfterFrameSlot->Pts() >= TimeStamp_c::AddUsec(current_time, mFrameDuration))
        {
            SE_DEBUG(group_encode_coordinator
                     , "Stream 0x%p %s TimeOutState , frame in future, current_time: %lld duration %lld mAfter PTS %lld\n"
                     , mEncodeStream
                     , MEDIA_STR(mMedia)
                     , current_time.NativeValue()
                     , mFrameDuration
                     , mAfterFrameSlot->Pts().NativeValue());

            mReady = true;
            mNextState = GAPSTATE;
            mDataOut = CLONE;
            if (mMedia == STM_SE_ENCODE_STREAM_MEDIA_AUDIO)
            {
                mDiscontinuity = STM_SE_DISCONTINUITY_MUTE;
            }
            mNextEndGapTime = mAfterFrameSlot->Pts();
            mNextStreamTime = TimeStamp_c::AddUsec(mStreamTime, mFrameDuration);
            return;
        }

        // frame is in the past, so discarding incoming frame until catching back the current time
        // other streams wait because current_time is not updated
        if (TimeStamp_c::AddUsec(current_time, mFrameDuration) >= mAfterFrameSlot->Pts())
        {
            SE_DEBUG(group_encode_coordinator
                     , "Stream 0x%p %s TimeOutState , frame in past current_time: %lld duration %lld mAfter PTS %lld\n"
                     , mEncodeStream
                     , MEDIA_STR(mMedia)
                     , current_time.NativeValue()
                     , mFrameDuration
                     , mAfterFrameSlot->Pts().NativeValue());

            mReady  = true;
            mHasGap = false;
            mDataOut = NODATA;

            // release immediatly old frame
            mAfterFrameSlot->BufferStateClear(BufferDecode);
            CheckForBufferRelease(mAfterFrameSlot);
            mAfterFrameSlot = NULL;

            return;
        }
    }

    SE_ERROR("Unexpected case Stream 0x%p %s %lld end gap %lld frame duration %lld current\n"
             , mEncodeStream
             , MEDIA_STR(mMedia)
             , current_time.NativeValue()
             , mEndGapTime.NativeValue()
             , mFrameDuration);
}

void EncodeCoordinatorStream_c::EosState(TimeStamp_c current_time)
{
    SE_EXTRAVERB(group_encode_coordinator, "Checking whether a frame was sent\n");
    if (mStreamTime.IsValid()
        && current_time.IsValid()
        && TimeInInterval(current_time
                          , TimeStamp_c::AddUsec(mStreamTime, -mFrameDuration)
                          , mStreamTime))
    {
        // a frame has already been sent
        mReady  = true;
        mHasGap = false;
        mDataOut = NODATA;
        SE_EXTRAVERB(group_encode_coordinator, "Ready\n");
        return;
    }

    if ((mCurrentFrameSlot != NULL) && mCurrentFrameSlot->IsEOS())
    {
        SE_EXTRAVERB(group_encode_coordinator, "Processing EOS\n");
        mReady  = true;
        mHasGap = false;
        mDataOut = CURRENT;
        if (mStreamTime.IsValid())
        {
            mNextStreamTime = TimeStamp_c::AddUsec(mStreamTime, mFrameDuration);
        }
        else
        {
            mNextStreamTime = TimeStamp_c();
        }
        mDiscontinuity = STM_SE_DISCONTINUITY_EOS;
        mShift = true;
        mNextState = EOSSTATE;
        return;
    }

    // otherwise let other stream continue forever
    mReady  = true;
    mHasGap = false;
    mDataOut = NODATA;
    mNextStreamTime = TimeStamp_c();
    SE_EXTRAVERB(group_encode_coordinator, "Other streams will continue\n");
}

void EncodeCoordinatorStream_c::ComputeState(TimeStamp_c current_time)
{
    mHasGap  = false;
    mReady   = false;
    mDataOut = NODATA;
    mShift   = false;
    mIsLastRepeat = false;
    mDiscontinuity = STM_SE_DISCONTINUITY_CONTINUOUS;

    // loading frames
    if (mCurrentFrameSlot == NULL)
    {
        mCurrentFrameSlot = mAfterFrameSlot;
        mAfterFrameSlot = NULL;
    }
    if (mAfterFrameSlot == NULL)
    {
        mAfterFrameSlot = ExtractInputFrame();
    }

    if (mCurrentFrameSlot == NULL)
    {
        // inactive stream
        mReady = true;
        mNextStreamTime = TimeStamp_c();
        return;
    }

    mCurrentFrameSlot->GetBufferFrameDuration(&mFrameDuration);

    switch (mCurrentState)
    {
    case RUNNINGSTATE:
        RunningState(current_time);
        break;

    case GAPSTATE:
        GapState(current_time);
        break;

    case TIMEOUTSTATE:
        TimeOutState(current_time);
        break;

    case OUTOFGAPSTATE:
        OutOfGapState(current_time);
        break;

    case EOSSTATE:
        EosState(current_time);
        break;

    default:
        SE_ERROR("Stream 0x%p %s unexepected state\n", mEncodeStream, MEDIA_STR(mMedia));
        break;
    }
}

void EncodeCoordinatorStream_c::UpdateState()
{
    mCurrentState = mNextState;

    SE_DEBUG(group_encode_coordinator
             , "Stream 0x%p %s mNextStreamTime %lld mStreamTime%lld\n"
             , mEncodeStream
             , MEDIA_STR(mMedia)
             , mNextStreamTime.NativeValue()
             , mStreamTime.NativeValue());

    if (mNextStreamTime.IsValid())
    {
        mStreamTime = mNextStreamTime;
    }
    else
    {
        SE_WARNING("Stream 0x%p %s Invalid next stream time\n", mEncodeStream
                   , MEDIA_STR(mMedia));
    }
    if (mNextEndGapTime.IsValid())
    {
        mEndGapTime = mNextEndGapTime;
    }

    if (mShift)
    {
        mCurrentFrameSlot->BufferStateClear(BufferDecode);
        CheckForBufferRelease(mCurrentFrameSlot);
        mCurrentFrameSlot = mAfterFrameSlot;
        mAfterFrameSlot = NULL;
    }
}

void EncodeCoordinatorStream_c::Encode(TimeStamp_c current_time)
{
    TimeStamp_c  encodeTime = TimeStamp_c::AddUsec(mStreamTime, -mPtsOffset);

    SE_DEBUG(group_encode_coordinator
             , "Stream 0x%p %s encodeTime %lld mStreamTime%lld mFrameDuration %lld Adjusted %lld\n"
             , mEncodeStream
             , MEDIA_STR(mMedia)
             , encodeTime.NativeValue()
             , mStreamTime.NativeValue()
             , mFrameDuration
             , mPtsOffset);

    SE_DEBUG(group_encode_coordinator,
             "Stream 0x%p %s %s time:%lld stream_time:%lld xstreamtime:%lld duration:%lld Slot:0x%p NextSlot:0x%p Rdy:%d Gap:%d xGapend:%lld EOS:%d XState:%s DataOut:%s Discont:%s\n"
             , mEncodeStream
             , MEDIA_STR(mMedia)
             , RunningStateStr(mCurrentState)
             , current_time.NativeValue()
             , mStreamTime.NativeValue()
             , mAfterFrameSlot ? mAfterFrameSlot->Pts().NativeValue() : 0
             , mFrameDuration
             , mCurrentFrameSlot ? mCurrentFrameSlot->GetBuffer() : NULL
             , mAfterFrameSlot ? mAfterFrameSlot->GetBuffer() : NULL
             , mReady
             , mHasGap
             , mNextEndGapTime.NativeValue()
             , mAfterFrameSlot ? mAfterFrameSlot->IsEOS() : 0
             , RunningStateStr(mNextState)
             , OutputStr(mDataOut)
             , StringifyDiscontinuity(mDiscontinuity));

    if (mDataOut == CLONE)
    {
        unsigned long long      timeNowMs;
        TranscodeBuffer_c      *clonedBuffer = NULL;

        timeNowMs               = OS_GetTimeInMicroSeconds();
        while (true)
        {
            clonedBuffer = mCurrentFrameSlot->CloneBuffer(mTranscodeBuffer, mEncoder, encodeTime);

            if (clonedBuffer != NULL) { break; }

            if (OS_GetTimeInMicroSeconds() > (timeNowMs + 1000))
            {
                SE_WARNING("Stream 0x%p %s Waiting for encode buffer for clone\n"
                           , mEncodeStream, MEDIA_STR(mMedia));
                timeNowMs   = OS_GetTimeInMicroSeconds();
            }

            if (((EncodeCoordinator_c *)mEncodeCoordinator)->IsGlobalFlushAsked())
            {
                // Must abort waiting for clone buffer to allow the encode_coordinator
                // thread to process the flush
                SE_WARNING("%s: Flush asked while waiting for encode buffer for clone\n"
                           , MEDIA_STR(mMedia));
                return;
            }

            OS_SleepMilliSeconds(5);

            // release returned clone buffers
            ProcessEncoderReturnedBuffer();
        }

        mCurrentFrameSlot->BufferStateSet(BufferReference);
        if (mIsLastRepeat)
        {
            clonedBuffer->BufferStateSet(BufferLastClone);
            clonedBuffer->BufferStateSetCloneReference(mCurrentFrameSlot);
        }
        clonedBuffer->SetEncodeTime(encodeTime);
        clonedBuffer->SetNativeTime(mStreamTime);
#ifndef UNITTESTS
        // do not send CLOSED Gop discontinuity
        if (mDiscontinuity == STM_SE_DISCONTINUITY_CLOSED_GOP_REQUEST)
        {
            mDiscontinuity = STM_SE_DISCONTINUITY_CONTINUOUS;
        }
#endif
        clonedBuffer->SetEncodeDiscontinuity(mDiscontinuity);
        clonedBuffer->BufferStateSet(BufferClone);
        mLastCloneBuffer = clonedBuffer;

        SE_DEBUG(group_encode_coordinator
                 , "Stream 0x%p %s _OUT clone %p(%lld,%lld) %s\n"
                 , mEncodeStream
                 , MEDIA_STR(mMedia)
                 , clonedBuffer->GetBuffer()
                 , clonedBuffer->Pts().NativeValue()
                 , encodeTime.NativeValue()
                 , StringifyDiscontinuity(mDiscontinuity));

        SendBufferToEncoder(clonedBuffer);
        mEncCloneIn++;
    }

    if (mDataOut == CURRENT)
    {
        mCurrentFrameSlot->SetEncodeTime(encodeTime);
        SE_DEBUG(group_encode_coordinator
                 , "Stream 0x%p %s _OUT out %p(%lld,%lld) %s\n"
                 , mEncodeStream
                 , MEDIA_STR(mMedia)
                 , mCurrentFrameSlot->GetBuffer()
                 , mCurrentFrameSlot->Pts().NativeValue()
                 , encodeTime.NativeValue()
                 , StringifyDiscontinuity(mDiscontinuity));

#ifndef UNITTESTS
        // do not send CLOSED GOP discontinuity
        if (mDiscontinuity == STM_SE_DISCONTINUITY_CLOSED_GOP_REQUEST)
        {
            mDiscontinuity = STM_SE_DISCONTINUITY_CONTINUOUS;
        }
#endif
        mCurrentFrameSlot->SetEncodeDiscontinuity(mDiscontinuity);
        SendBufferToEncoder(mCurrentFrameSlot);
        mEncIn++;
    }

    if (mDataOut == NODATA && mIsLastRepeat)
    {
        // Condition is true only when coming from GAP state after a seek
        if (mCurrentFrameSlot->IsBufferState(BufferReference))
        {
            // Buffer has been cloned
            if (mLastCloneBuffer->IsBufferState(BufferOwnByEncoder))
            {
                // Clone Still in encoder, so mark it as last clone
                mLastCloneBuffer->BufferStateSet(BufferLastClone);
                mLastCloneBuffer->BufferStateSetCloneReference(mCurrentFrameSlot);
            }
            else
            {
                // Already returned, so clear the reference flag
                mCurrentFrameSlot->BufferStateClear(BufferReference);
            }
        }
    }
}
