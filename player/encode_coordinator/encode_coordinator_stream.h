/************************************************************************
Copyright (C) 2003-2013 STMicroelectronics. All Rights Reserved.

This file is part of the Streaming Engine.

Streaming Engine is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License version 2 as
published by the Free Software Foundation.

Streaming Engine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with Streaming Engine; see the file COPYING.  If not, write to the Free
Software Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307,
USA.

The Streaming Engine Library may alternatively be licensed under a
proprietary license from ST.
************************************************************************/

#ifndef H_ENCODE_COORDINATOR_STREAM
#define H_ENCODE_COORDINATOR_STREAM

#include "player_types.h"
#include "port.h"
#include "ring_generic.h"
#include "buffer.h"
#include "encoder.h"

#include "encode_coordinator_interface.h"
#include "encode_coordinator_buffer.h"
#define MAX_FRAME_REWIND_DETECTION      5
#define TIMEOUT_FRAME_COUNT             5
#define WAIT_RETURN_ENCODER_MAX_TIME_US 1000000ULL
#define MIN_FRAME_DURATION_US           1000ULL
#define DURATION_TIMEOUT                5000000

typedef enum RunningState_s
{
    // Normal running state
    RUNNINGSTATE = 0,
    // Gap state , this stream is filling a gap between two frames
    GAPSTATE,
    // streams is waiting for a frame and is repeating last correct frame
    TIMEOUTSTATE,
    // transition state betwen Gap and running , needed can we may face a new Gap
    OUTOFGAPSTATE,
    // stream has received an EOS and is stopped
    EOSSTATE

} RunningState_t;

typedef enum DataOut_s
{
    // Current frame is sent to encoder
    CURRENT = 0,
    // Current frame is clone , clone is sent to encoder
    CLONE,
    // no data are sent out
    NODATA,

} DataOut_t;

class EncodeCoordinatorStream_c : public Port_c, public ReleaseBufferInterface_c
{
public:
    EncodeCoordinatorStream_c(EncodeStreamInterface_c      *encodeStream,
                              EncodeCoordinatorInterface_c *encodeCoordinator,
                              ReleaseBufferInterface_c     *OriginalReleaseBufferInterface);

    virtual ~EncodeCoordinatorStream_c();

    virtual EncoderStatus_t FinalizeInit();
    virtual void            Halt();

    // Port interface
    RingStatus_t Insert(uintptr_t Value);
    RingStatus_t Extract(uintptr_t *Value, unsigned int BlockingPeriod = OS_INFINITE)
    {
        (void)Value; // warning removal
        (void)BlockingPeriod; // warning removal
        return RingNoError;
    }

    RingStatus_t Flush(bool FlushInputStageOnly);

    bool                        NonEmpty()         { return mInputRing->NonEmpty(); }
    Port_c                      *GetInputPort()    { return mInputRing; }
    EncodeStreamInterface_c     *GetEncodeStream() { return mEncodeStream; }

    // Release buffer interface
    PlayerStatus_t ReleaseBuffer(Buffer_t Buffer);

    // Getters
    bool GetCurrentFrame();
    bool GetNextValidFrame(TimeStamp_c CurrentTime);
    bool GetFrameDuration(uint64_t *FrameDuration);
    bool GetTime(TimeStamp_c *streamTime);
    bool GetNextTime(TimeStamp_c *streamTime);

    bool HasTimeOut(TimeStamp_c CurrentTime, bool *wait);
    bool HasGap(TimeStamp_c *endOfTimeGap);
    bool IsReady();
    bool IsTimeOut();

    // Features
    bool TransmitEOS();
    bool DiscardUntil(TimeStamp_c highestCurrentTime);
    void AdjustJumpUs(int64_t minOffset);
    void Encode(TimeStamp_c current_time);

    void PerformPendingFlush();
    void ProcessEncoderReturnedBuffer();

    // State management
    void ComputeState(TimeStamp_c current_time);
    void UpdateState();

private:
    OS_Mutex_t                       mLock;
    RingGeneric_c                   *mInputRing;
    RingGeneric_c                   *mReturnedRing;
    Port_c                          *mOutputPort;
    EncodeStreamInterface_c         *mEncodeStream;
    EncodeCoordinatorInterface_c    *mEncodeCoordinator;
    ReleaseBufferInterface_c        *mReleaseBufferInterface;

    // Status and state stream variables
    bool                             mGotFirstFrame[STM_SE_ENCODE_STREAM_MEDIA_ANY];
    uint64_t                         mFrameDuration;
    bool                             mHasGap;
    bool                             mReady;
    bool                             mShift;
    bool                             mIsLastRepeat;
    stm_se_discontinuity_t           mDiscontinuity;
    DataOut_t                        mDataOut;
    uint64_t                         mLastReceivedTime;
    uint64_t                         mPtsOffset;

    RunningState_t                   mCurrentState;
    RunningState_t                   mNextState;

    TimeStamp_c                      mStreamTime;
    TimeStamp_c                      mNextStreamTime;
    TimeStamp_c                      mEndGapTime;
    TimeStamp_c                      mNextEndGapTime;
    TranscodeBuffer_t                mCurrentFrameSlot;
    TranscodeBuffer_t                mLastCloneBuffer;
    TranscodeBuffer_t                mAfterFrameSlot;

    const EncoderBufferTypes_t      *mEncoderBufferTypes;
    Encoder_c                       *mEncoder;
    TranscodeBuffer_c                mTranscodeBuffer[ENCODER_MAX_INPUT_BUFFERS];
    stm_se_encode_stream_media_t     mMedia;
    bool                             mFlushAsked;
    bool                             mFlushInputStageOnly;
    OS_Event_t                       mEndOfFlushEvent;

    // Counter for debug purpose
    unsigned int                     mEncIn;
    unsigned int                     mEncOut;
    unsigned int                     mEncCloneIn;
    unsigned int                     mEncCloneOut;
    unsigned int                     mBufferIn;
    unsigned int                     mBufferOut;

    TranscodeBuffer_c   *ExtractInputFrame();

    bool GetCloneInputBuffer(
        TranscodeBuffer_t OriginalBuffer
        , TranscodeBuffer_t *ClonedBuffer
        , uint64_t encodeTime);

    void CheckForBufferRelease(TranscodeBuffer_t buffer);
    void DoFlush();
    void SendBufferToEncoder(TranscodeBuffer_t buffer);

    void RunningState(TimeStamp_c current_time);
    void GapState(TimeStamp_c current_time);
    void TimeOutState(TimeStamp_c current_time);
    void OutOfGapState(TimeStamp_c current_time);
    void EosState(TimeStamp_c current_time);

    bool TimeInInterval(TimeStamp_c current_time, TimeStamp_c low, TimeStamp_c high);

    bool BufferStateInEncoder();
    void BufferStateLog();

    DISALLOW_COPY_AND_ASSIGN(EncodeCoordinatorStream_c);
};

#endif // H_ENCODE_COORDINATOR_STREAM
