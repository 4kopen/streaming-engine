/************************************************************************
Copyright (C) 2003-2014 STMicroelectronics. All Rights Reserved.

This file is part of the Streaming Engine.

Streaming Engine is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License version 2 as
published by the Free Software Foundation.

Streaming Engine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with Streaming Engine; see the file COPYING.  If not, write to the Free
Software Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307,
USA.

The Streaming Engine Library may alternatively be licensed under a
proprietary license from ST.
************************************************************************/

#include "encoder.h"

#undef TRACE_TAG
#define TRACE_TAG "CoderToTransportEdge_c"

void   CoderToTransportEdge_c::MainLoop()
{
    RingStatus_t              RingStatus;
    Buffer_t                  Buffer;
    BufferType_t              BufferType;
    unsigned int              BufferIndex;
    EncoderSequenceNumber_t  *SequenceNumberStructure;
    unsigned long long        LastEntryTime;
    unsigned int              BufferLength;
    bool                      DiscardBuffer;

    //
    // Set parameters
    //
    LastEntryTime                     = OS_GetTimeInMicroSeconds();
    mSequenceNumber                   = INVALID_SEQUENCE_VALUE;
    mMaximumActualSequenceNumberSeen  = 0;

    //
    // Signal we have started
    //
    OS_LockMutex(&mStream->StartStopLock);
    mStream->ProcessRunningCount++;
    OS_SetEvent(&mStream->StartStopEvent);
    OS_UnLockMutex(&mStream->StartStopLock);

    SE_DEBUG(group_encoder_stream, "Stream 0x%p process starting\n", mStream);

    //
    // Main Loop
    //
    while (true)
    {
        RingStatus  = mInputRing->Extract((uintptr_t *)(&Buffer), ENCODE_STREAM_MAX_EVENT_WAIT);

        if ((RingStatus == RingNothingToGet) || (Buffer == NULL))
        {
            if (mStream->Terminating) { break; }

            continue;
        }

        Buffer->GetType(&BufferType);
        Buffer->GetIndex(&BufferIndex);
        Buffer->TransferOwnership(IdentifierEncoderCoderToOutput);

        //
        // If we were set to terminate while we were 'Extracting' we should
        // remove the buffer reference and exit.
        //
        if (mStream->Terminating)
        {
            Buffer->DecrementReferenceCount(IdentifierEncoderCoderToOutput);
            continue;
        }

        //
        // Deal with a coded frame buffer
        //
        if (BufferType == mStream->CodedFrameBufferType)
        {
            //
            // Apply a sequence number to the buffer
            //
            Buffer->ObtainMetaDataReference(mStream->MetaDataSequenceNumberType, (void **)(&SequenceNumberStructure));
            SE_ASSERT(SequenceNumberStructure != NULL);

            SequenceNumberStructure->TimeEntryInProcess1    = OS_GetTimeInMicroSeconds();
            SequenceNumberStructure->DeltaEntryInProcess1   = SequenceNumberStructure->TimeEntryInProcess1 - LastEntryTime;
            LastEntryTime                   = SequenceNumberStructure->TimeEntryInProcess1;
            SequenceNumberStructure->TimePassToOutput       = INVALID_TIME;

            if ((mMarkerInCodedFrameIndex != INVALID_INDEX) &&
                (mMarkerInCodedFrameIndex == BufferIndex))
            {
                // This is a marker frame
                SequenceNumberStructure->mIsMarkerFrame    = true;
                mNextBufferSequenceNumber                  = SequenceNumberStructure->Value + 1;
                mMarkerInCodedFrameIndex                   = INVALID_INDEX;
                StopDiscardingUntilDrainMarkerFrame();
            }

            mSequenceNumber                     = SequenceNumberStructure->Value;
            mMaximumActualSequenceNumberSeen    = max(mSequenceNumber, mMaximumActualSequenceNumberSeen);
            //
            // Process any outstanding control messages to be applied before this buffer
            //
            ProcessAccumulatedBeforeControlMessages(mSequenceNumber);
            //
            // Pass the buffer to the encoder for processing
            // then release our hold on this buffer. When discarding we
            // do not pass on, unless we have a zero length buffer, used when
            // signalling events.
            //
            BufferStatus_t BufStatus = Buffer->ObtainDataReference(NULL, &BufferLength, NULL);
            if (BufStatus != BufferNoError)
            {
                if (BufStatus == BufferNoDataAttached)
                {
                    SE_VERBOSE(group_encoder_stream, "Stream 0x%p Buffer 0x%p with zero buffer length\n", mStream, Buffer);
                }
                else
                {
                    SE_ERROR("Stream 0x%p Unable to obtain the data reference\n", mStream);
                }
                Buffer->DecrementReferenceCount(IdentifierEncoderCoderToOutput);
                continue;
            }
            else
            {
                unsigned int _cnt = 0;
                Buffer->GetOwnerCount(&_cnt);
                SE_DEBUG(group_se_pipeline, "Stream 0x%x - %d - #%lld PTS=%lld EC=%llu Buffer 0x%p BufferLength %d mNextBufferSequenceNumber %lld RefCount %d\n",
                         SequenceNumberStructure->StreamUniqueIdentifier,
                         SequenceNumberStructure->StreamTypeIdentifier,
                         SequenceNumberStructure->FrameCounter,
                         SequenceNumberStructure->PTS,
                         SequenceNumberStructure->TimeEntryInProcess1,
                         Buffer,
                         BufferLength,
                         mNextBufferSequenceNumber,
                         _cnt
                        );

                SequenceNumberStructure->mIsMarkerFrame    = false;
                SequenceNumberStructure->Value             = mNextBufferSequenceNumber++;
            }



            DiscardBuffer   = !SequenceNumberStructure->mIsMarkerFrame &&
                              (BufferLength != 0) &&
                              (mStream->UnEncodable || IsDiscardingUntilDrainMarkerFrame());

            if (!DiscardBuffer)
            {
                SE_VERBOSE(group_encoder_stream, "Stream 0x%p Send Buffer 0x%p to Transporter\n", mStream, Buffer);
                /*TransporterStatus_t Status =*/ mStream->GetTransporter()->Input(Buffer);
            }
            else
            {
                SE_VERBOSE(group_encoder_stream, "Stream 0x%p Discard Buffer 0x%p - mIsMarkerFrame %d BufferLength %d UnEncodable %d Discarding %d\n",
                           mStream, Buffer, SequenceNumberStructure->mIsMarkerFrame, BufferLength, mStream->UnEncodable, IsDiscardingUntilDrainMarkerFrame());
            }

            Buffer->DecrementReferenceCount(IdentifierEncoderCoderToOutput);

            //
            // Process any outstanding control messages to be applied after this buffer
            //
            ProcessAccumulatedAfterControlMessages(mSequenceNumber);
        }
        //
        // Deal with an Encoder control structure
        //
        else if (BufferType == mStream->BufferEncoderControlStructureType)
        {
            HandlePlayerControlStructure(Buffer, mSequenceNumber, mMaximumActualSequenceNumberSeen);
        }
        else
        {
            SE_ERROR("Stream 0x%p Unknown buffer type %d received\n", mStream, BufferType);
            Buffer->DecrementReferenceCount();
        }
    }
    SE_DEBUG(group_encoder_stream, "Stream 0x%p process terminating\n", mStream);

    // At this point, Terminating must be true
    OS_Smp_Mb(); // Read memory barrier: rmb_for_EncodeStream_Terminating coupled with: wmb_for_EncodeStream_Terminating

    //
    // Signal we have terminated
    //
    OS_LockMutex(&mStream->StartStopLock);
    mStream->ProcessRunningCount--;
    OS_SetEvent(&mStream->StartStopEvent);
    OS_UnLockMutex(&mStream->StartStopLock);
}


PlayerStatus_t   CoderToTransportEdge_c::CallInSequence(
    PlayerSequenceType_t      SequenceType,
    PlayerSequenceValue_t     SequenceValue,
    PlayerComponentFunction_t Fn,
    ...)
{
    Buffer_t                  ControlStructureBuffer;
    PlayerControlStructure_t *ControlStructure;
    //
    // Garner a control structure, fill it in
    //
    BufferStatus_t Status = mStream->GetEncode()->GetControlStructurePool()->GetBuffer(&ControlStructureBuffer, IdentifierInSequenceCall);
    if (Status != BufferNoError)
    {
        SE_ERROR("Stream 0x%p Failed to get a control structure buffer\n", mStream);
        return EncoderError;
    }

    ControlStructureBuffer->ObtainDataReference(NULL, NULL, (void **)(&ControlStructure));
    SE_ASSERT(ControlStructure != NULL);  // not supposed to be empty
    ControlStructure->Action            = ActionInSequenceCall;
    ControlStructure->SequenceType      = SequenceType;
    ControlStructure->SequenceValue     = SequenceValue;
    ControlStructure->InSequence.Fn     = Fn;

    switch (Fn)
    {

    default:
        SE_ERROR("Unsupported function call\n");
        ControlStructureBuffer->DecrementReferenceCount(IdentifierInSequenceCall);
        return EncoderNotSupported;
    }

    //
    // Send it to the appropriate process
    //
    return EncoderNoError;
}

EncoderStatus_t   CoderToTransportEdge_c::PerformInSequenceCall(PlayerControlStructure_t *ControlStructure)
{
    EncoderStatus_t  Status = EncoderNoError;

    switch (ControlStructure->InSequence.Fn)
    {

    default:
        SE_DEBUG(group_encoder_stream, "Stream 0x%p Not supported\n", mStream);
        Status  = EncoderNotSupported;
        break;
    }

    return Status;
}

EncoderStatus_t   CoderToTransportEdge_c::Flush()
{
    SE_DEBUG(group_encoder_stream, "Stream 0x%p>\n", mStream);

    Buffer_t Buffer;
    uint32_t     NbReleasedFrame = 0;

    while (true)
    {
        RingStatus_t RingStatus = mInputRing->ExtractLastInserted((uintptr_t *) &Buffer, RING_NONE_BLOCKING);

        // The ring can be filled out by the CoderToOutput thread
        if (RingStatus == RingNothingToGet)
        {
            break;
        }

        Buffer->DecrementReferenceCount(IdentifierEncoderCoderToOutput);
        NbReleasedFrame++;
    }
    SE_DEBUG(group_encoder_stream, "Stream 0x%p Released %d <\n", mStream, NbReleasedFrame);

    return EncoderNoError;
}
