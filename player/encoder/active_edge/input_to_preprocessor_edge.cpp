/************************************************************************
Copyright (C) 2003-2014 STMicroelectronics. All Rights Reserved.

This file is part of the Streaming Engine.

Streaming Engine is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License version 2 as
published by the Free Software Foundation.

Streaming Engine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with Streaming Engine; see the file COPYING.  If not, write to the Free
Software Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307,
USA.

The Streaming Engine Library may alternatively be licensed under a
proprietary license from ST.
************************************************************************/

#include "encoder.h"

#undef TRACE_TAG
#define TRACE_TAG "InputToPreprocessorEdge_c"

void   InputToPreprocessorEdge_c::MainLoop()
{
    EncoderStatus_t                           Status;
    RingStatus_t                              RingStatus;
    Buffer_t                                  Buffer;
    BufferType_t                              BufferType;
    stm_se_uncompressed_frame_metadata_t     *UncompressedMetaData;

    // Signal we have started
    OS_LockMutex(&mStream->StartStopLock);
    mStream->ProcessRunningCount++;
    OS_SetEvent(&mStream->StartStopEvent);
    OS_UnLockMutex(&mStream->StartStopLock);

    SE_DEBUG(group_encoder_stream, "Stream 0x%p process starting\n", mStream);

    // Main Loop
    while (true)
    {
        RingStatus  = mInputRing->Extract((uintptr_t *)(&Buffer), ENCODE_STREAM_MAX_EVENT_WAIT);
        if ((RingStatus == RingNothingToGet) || (Buffer == NULL))
        {
            if (mStream->Terminating) { break; }

            continue;
        }

        Buffer->GetType(&BufferType);
        Buffer->TransferOwnership(IdentifierEncoderInputToPreProcess);

        // Deal with a Input buffer
        if (BufferType == mStream->BufferInputBufferType)
        {
            // If we were set to terminate while we were 'Extracting' we should
            // remove the buffer reference and exit.
            if (mStream->Terminating)
            {
                mStream->ReleaseInputBuffer(Buffer);
                continue;
            }

            Buffer->ObtainMetaDataReference(mStream->InputMetaDataBufferType, (void **)(&UncompressedMetaData));
            SE_ASSERT(UncompressedMetaData != NULL);

            // First check that the buffer should be pushed to encode
            // May be an EOS standalone frame if buffer size is 0
            uint32_t    BufferSize = 0;
            Buffer->ObtainDataReference(NULL, &BufferSize, NULL);
            if (BufferSize != 0)
            {
                // Send buffer to encoder
                SE_VERBOSE(group_encoder_stream, "Stream 0x%p Send Buffer 0x%p to Preproc\n", mStream, Buffer);
                Status = mStream->Input(Buffer);
                if (Status != EncoderNoError)
                {
                    SE_ERROR("Stream 0x%p Unable to encode Buffer 0x%p\n", mStream, Buffer);
                }
            }
            else if ((UncompressedMetaData->discontinuity & STM_SE_DISCONTINUITY_EOS) == STM_SE_DISCONTINUITY_EOS)
            {
                // Inject discontinuity only in case of standalone EOS frame
                // When the EOS is carried by the last frame to be encoded, it is not needed because already handled by the preproc
                SE_VERBOSE(group_encoder_stream, "Stream 0x%p Send EOS to Preproc\n", mStream);
                mStream->InjectDiscontinuity(STM_SE_DISCONTINUITY_EOS);
            }

            // Release the encode input buffer
            mStream->ReleaseInputBuffer(Buffer);
        }
        //
        // Deal with an Encoder control structure
        //
        else if (BufferType == mStream->BufferEncoderControlStructureType)
        {
            // If we were set to terminate while we were 'Extracting' we should
            // remove the buffer reference and exit.
            if (mStream->Terminating)
            {
                Buffer->DecrementReferenceCount(IdentifierEncoderInputToPreProcess);
            }
            else
            {
                HandlePlayerControlStructure(Buffer, mSequenceNumber, mMaximumActualSequenceNumberSeen);
            }

        }
    }
    SE_DEBUG(group_encoder_stream, "Stream 0x%p process terminating\n", mStream);

    // At this point, Terminating must be true
    OS_Smp_Mb(); // Read memory barrier: rmb_for_EncodeStream_Terminating coupled with: wmb_for_EncodeStream_Terminating

    // Signal we have terminated
    OS_LockMutex(&mStream->StartStopLock);
    mStream->ProcessRunningCount--;
    OS_SetEvent(&mStream->StartStopEvent);
    OS_UnLockMutex(&mStream->StartStopLock);
}

PlayerStatus_t   InputToPreprocessorEdge_c::CallInSequence(
    PlayerSequenceType_t      SequenceType,
    PlayerSequenceValue_t     SequenceValue,
    PlayerComponentFunction_t Fn,
    ...)
{
    va_list                   List;
    Buffer_t                  ControlStructureBuffer;
    PlayerControlStructure_t *ControlStructure;
    //
    // Garner a control structure, fill it in
    //
    BufferStatus_t Status = mStream->GetEncode()->GetControlStructurePool()->GetBuffer(&ControlStructureBuffer, IdentifierInSequenceCall);
    if (Status != BufferNoError)
    {
        SE_ERROR("Stream 0x%p Failed to get a control structure buffer\n", mStream);
        return EncoderError;
    }

    ControlStructureBuffer->ObtainDataReference(NULL, NULL, (void **)(&ControlStructure));
    SE_ASSERT(ControlStructure != NULL);  // not supposed to be empty
    ControlStructure->Action            = ActionInSequenceCall;
    ControlStructure->SequenceType      = SequenceType;
    ControlStructure->SequenceValue     = SequenceValue;
    ControlStructure->InSequence.Fn     = Fn;

    switch (Fn)
    {
    case EncodeFnFlushPreprocessorEdge:
        SE_DEBUG(group_encoder_stream, "%s 0x%p: EncodeFnFlushPreprocessorEdge\n", MEDIA_STR(mStream->mMedia), mStream);
        va_start(List, Fn);
        ControlStructure->InSequence.Pointer    = (void *)va_arg(List, OS_Event_t *);
        va_end(List);
        mInputRing->InsertFront((uintptr_t)ControlStructureBuffer);
        break;

    default:
        SE_ERROR("Unsupported function call\n");
        ControlStructureBuffer->DecrementReferenceCount(IdentifierInSequenceCall);
        return EncoderNotSupported;
    }

    //
    // Send it to the appropriate process
    //
    // DestinationRing->Insert( (uintptr_t)ControlStructureBuffer );
    return EncoderNoError;
}

EncoderStatus_t   InputToPreprocessorEdge_c::PerformInSequenceCall(PlayerControlStructure_t *ControlStructure)
{
    EncoderStatus_t  Status = EncoderNoError;
    uint32_t    NbReleasedFrame = 0;

    switch (ControlStructure->InSequence.Fn)
    {
    case EncodeFnFlushPreprocessorEdge:
        SE_DEBUG(group_encoder_stream, "Stream 0x%p EncodeFnFlushPreprocessorEdge>\n", mStream);

        while (true)
        {
            Buffer_t Buffer;
            RingStatus_t RingStatus;

            RingStatus = mInputRing->Extract((uintptr_t *) &Buffer);
            if (RingStatus == RingNothingToGet)
            {
                break;
            }

            mStream->ReleaseInputBuffer(Buffer);
            NbReleasedFrame++;
        }

        // Allow GetBuffer call
        mStream->GetPreproc()->AcceptGetBufferCalls();

        // Perform specific actions on preprocessor
        mStream->GetPreproc()->Flush();

        SE_DEBUG(group_encoder_stream, "Stream 0x%p EncodeFnFlushPreprocessorEdge Released %d <\n", mStream, NbReleasedFrame);
        // Signal the end of Flush of the InoutToPreprocessorEdge
        OS_SetEvent((OS_Event_t *)ControlStructure->InSequence.Pointer);
        break;

    default:
        SE_DEBUG(group_encoder_stream, "Stream 0x%p Not supported\n", mStream);
        Status  = EncoderNotSupported;
        break;
    }

    return Status;
}

EncoderStatus_t   InputToPreprocessorEdge_c::Flush()
{
    EncoderStatus_t Status = EncoderNoError;

    SE_DEBUG(group_encoder_stream, "Stream 0x%p >\n", mStream);

    // Nothing more to do if no ring
    if (mInputRing == NULL)
    {
        SE_DEBUG(group_encoder_stream, "Stream 0x%p Nothing to do, ring does not exist\n", mStream);
        return Status;
    }

    // unblock any pending GetBuffer call
    mStream->GetPreproc()->RejectGetBufferCalls();

    OS_ResetEvent(&mStream->mEndOfStageFlushEvent);

    CallInSequence(SequenceTypeImmediate, TIME_NOT_APPLICABLE, EncodeFnFlushPreprocessorEdge, &mStream->mEndOfStageFlushEvent);

    // Wait for the end of flush event. Must not be interruptible to make sure CallInSequence is correctly over
    OS_Status_t WaitStatus = OS_WaitForEventAuto(&mStream->mEndOfStageFlushEvent, ENCODE_MAX_EVENT_WAIT);
    if (WaitStatus == OS_TIMED_OUT)
    {
        Status = EncoderTimedOut;
        SE_WARNING("Stream 0x%p Timeout waiting for End of Flush ; event->Valid:%d\n", mStream, OS_TestEventSet(&mStream->mEndOfStageFlushEvent));
    }

    SE_DEBUG(group_encoder_stream, "Stream 0x%p <\n", mStream);

    return Status;
}

