/************************************************************************
Copyright (C) 2003-2014 STMicroelectronics. All Rights Reserved.

This file is part of the Streaming Engine.

Streaming Engine is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License version 2 as
published by the Free Software Foundation.

Streaming Engine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with Streaming Engine; see the file COPYING.  If not, write to the Free
Software Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307,
USA.

The Streaming Engine Library may alternatively be licensed under a
proprietary license from ST.
************************************************************************/

#include "encoder.h"

#undef TRACE_TAG
#define TRACE_TAG "PreprocessorToCoderEdge_c"

void   PreprocessorToCoderEdge_c::MainLoop()
{
    RingStatus_t              RingStatus;
    Buffer_t                  Buffer;
    BufferType_t              BufferType;
    unsigned int              BufferIndex;
    EncoderSequenceNumber_t  *SequenceNumberStructure;
    unsigned long long        LastEntryTime;
    unsigned int              BufferLength;
    bool                      DiscardBuffer;
    EncoderStatus_t           Status;

    //
    // Set parameters
    //
    Status = EncoderNoError;
    LastEntryTime               = OS_GetTimeInMicroSeconds();
    mMaximumActualSequenceNumberSeen         = 0;

    //
    // Signal we have started
    //
    OS_LockMutex(&mStream->StartStopLock);
    mStream->ProcessRunningCount++;
    OS_SetEvent(&mStream->StartStopEvent);
    OS_UnLockMutex(&mStream->StartStopLock);

    SE_DEBUG(group_encoder_stream, "Stream 0x%p process starting\n", mStream);

    //
    // Main Loop
    //
    while (true)
    {
        // If low power state, thread must stay asleep until low power exit signal
        if (mStream->IsLowPowerState)
        {
            SE_INFO(group_encoder_stream, "Stream 0x%p entering low power..\n", mStream);
            // Signal end of low power processing
            // TBD: are we sure that all pending MME commands are completed here ???
            OS_SetEventInterruptible(&mStream->LowPowerEnterEvent);
            // Forever wait for wake-up event
            OS_Status_t WaitStatus = OS_WaitForEventInterruptible(&mStream->LowPowerExitEvent, OS_INFINITE);
            if (WaitStatus == OS_INTERRUPTED)
            {
                SE_INFO(group_encoder_stream, "Stream 0x%p wait for LP exit interrupted; LowPowerExitEvent:%d\n", mStream, mStream->LowPowerExitEvent.Valid);
            }
            SE_INFO(group_encoder_stream, "Stream 0x%p exiting low power..\n", mStream);
        }

        RingStatus  = mInputRing->Extract((uintptr_t *)(&Buffer), ENCODE_STREAM_MAX_EVENT_WAIT);

        if ((RingStatus == RingNothingToGet) || (Buffer == NULL))
        {
            if (mStream->Terminating) { break; }

            continue;
        }

        //
        // If we were set to terminate while we were 'Extracting' we should
        // remove the buffer reference and exit.
        //
        if (mStream->Terminating)
        {
            Buffer->DecrementReferenceCount(IdentifierEncoderPreProcessToCoder);
            continue;
        }


        Buffer->GetType(&BufferType);
        Buffer->GetIndex(&BufferIndex);
        Buffer->TransferOwnership(IdentifierEncoderPreProcessToCoder);

        //
        // Deal with a decoded frame buffer
        //
        if (BufferType == mStream->PreprocFrameBufferType)
        {
            //
            // Apply a sequence number to the buffer
            //
            Buffer->ObtainMetaDataReference(mStream->MetaDataSequenceNumberType, (void **)(&SequenceNumberStructure));
            SE_ASSERT(SequenceNumberStructure != NULL);

            SE_VERBOSE(group_se_pipeline, "Stream 0x%x - %d - #%lld EPP=%llu Buffer 0x%p\n",
                       SequenceNumberStructure->StreamUniqueIdentifier,
                       SequenceNumberStructure->StreamTypeIdentifier,
                       SequenceNumberStructure->FrameCounter,
                       OS_GetTimeInMicroSeconds(),
                       Buffer
                      );

            SequenceNumberStructure->TimeEntryInProcess0    = OS_GetTimeInMicroSeconds();
            SequenceNumberStructure->DeltaEntryInProcess0   = SequenceNumberStructure->TimeEntryInProcess0 - LastEntryTime;
            LastEntryTime                                   = SequenceNumberStructure->TimeEntryInProcess0;
            SequenceNumberStructure->TimeEntryInProcess1    = INVALID_TIME;
            SequenceNumberStructure->TimePassToCoder        = INVALID_TIME;
            SequenceNumberStructure->TimePassToOutput       = INVALID_TIME;

            if ((mMarkerInPreprocFrameIndex != INVALID_INDEX) &&
                (mMarkerInPreprocFrameIndex == BufferIndex))
            {
                // This is a marker frame
                SequenceNumberStructure->mIsMarkerFrame    = true;
                mNextBufferSequenceNumber                  = SequenceNumberStructure->Value + 1;
                mMarkerInPreprocFrameIndex                 = INVALID_INDEX;
                StopDiscardingUntilDrainMarkerFrame();
            }
            else
            {
                SequenceNumberStructure->mIsMarkerFrame   = false;
                SequenceNumberStructure->Value            = mNextBufferSequenceNumber++;
            }

            mSequenceNumber                     = SequenceNumberStructure->Value;
            mMaximumActualSequenceNumberSeen    = max(mSequenceNumber, mMaximumActualSequenceNumberSeen);
            //
            // Process any outstanding control messages to be applied before this buffer
            //
            ProcessAccumulatedBeforeControlMessages(mSequenceNumber);

            //
            // Pass the buffer to the encoder for processing
            // then release our hold on this buffer. When discarding we
            // do not pass on, unless we have a zero length buffer, used when
            // signalling events.
            //
            Buffer->ObtainDataReference(NULL, &BufferLength, NULL);
            DiscardBuffer   = !SequenceNumberStructure->mIsMarkerFrame &&
                              (BufferLength != 0) &&
                              (mStream->UnEncodable || IsDiscardingUntilDrainMarkerFrame() || (Status != EncoderNoError));

            if (!DiscardBuffer)
            {
                SE_VERBOSE(group_encoder_stream, "Stream 0x%p Send Buffer 0x%p to Coder\n", mStream, Buffer);
                Status = mStream->GetCoder()->Input(Buffer);
                if (Status != EncoderNoError)
                {
                    mStream->SignalEvent(STM_SE_ENCODE_STREAM_EVENT_FATAL_ERROR);
                    SE_ERROR("Stream 0x%p Input coder failed, all other input buffers will be dropped => encode stream must be destroyed, Status %u\n", mStream, Status);
                }
            }
            else
            {
                SE_VERBOSE(group_encoder_stream, "Stream 0x%p Discard Buffer 0x%p - mIsMarkerFrame %d BufferLength %d UnEncodable %d Discarding %d\n",
                           mStream, Buffer, SequenceNumberStructure->mIsMarkerFrame, BufferLength, mStream->UnEncodable, IsDiscardingUntilDrainMarkerFrame());
            }

            Buffer->DecrementReferenceCount(IdentifierEncoderPreProcessToCoder);
            //
            // Process any outstanding control messages to be applied after this buffer
            //
            ProcessAccumulatedAfterControlMessages(mSequenceNumber);

        }
        //
        // Deal with an Encoder control structure
        //
        else if (BufferType == mStream->BufferEncoderControlStructureType)
        {
            HandlePlayerControlStructure(Buffer, mSequenceNumber, mMaximumActualSequenceNumberSeen);
        }
        else
        {
            SE_ERROR("Stream 0x%p Unknown buffer type %d received\n", mStream, BufferType);
            Buffer->DecrementReferenceCount();
        }
    }
    SE_DEBUG(group_encoder_stream, "Stream 0x%p process terminating\n", mStream);

    // At this point, Terminating must be true
    OS_Smp_Mb(); // Read memory barrier: rmb_for_EncodeStream_Terminating coupled with: wmb_for_EncodeStream_Terminating

    //
    // Signal we have terminated
    //
    OS_LockMutex(&mStream->StartStopLock);
    mStream->ProcessRunningCount--;
    OS_SetEvent(&mStream->StartStopEvent);
    OS_UnLockMutex(&mStream->StartStopLock);
}

PlayerStatus_t   PreprocessorToCoderEdge_c::CallInSequence(
    PlayerSequenceType_t      SequenceType,
    PlayerSequenceValue_t     SequenceValue,
    PlayerComponentFunction_t Fn,
    ...)
{
    va_list                   List;
    Buffer_t                  ControlStructureBuffer;
    PlayerControlStructure_t *ControlStructure;
    //
    // Garner a control structure, fill it in
    //
    BufferStatus_t Status = mStream->GetEncode()->GetControlStructurePool()->GetBuffer(&ControlStructureBuffer, IdentifierInSequenceCall);
    if (Status != BufferNoError)
    {
        SE_ERROR("Stream 0x%p Failed to get a control structure buffer\n", mStream);
        return EncoderError;
    }

    ControlStructureBuffer->ObtainDataReference(NULL, NULL, (void **)(&ControlStructure));
    SE_ASSERT(ControlStructure != NULL);  // not supposed to be empty
    ControlStructure->Action            = ActionInSequenceCall;
    ControlStructure->SequenceType      = SequenceType;
    ControlStructure->SequenceValue     = SequenceValue;
    ControlStructure->InSequence.Fn     = Fn;

    va_start(List, Fn);
    ControlStructure->InSequence.Pointer    = (void *)va_arg(List, OS_Event_t *);
    va_end(List);

    switch (Fn)
    {
    case EncodeFnFlushCoderObject:
        SE_DEBUG(group_encoder_stream, "%s 0x%p: EncodeFnFlushCoderObject\n", MEDIA_STR(mStream->mMedia), mStream);
        mInputRing->Insert((uintptr_t)ControlStructureBuffer);
        break;

    case EncodeFnFlushCoderEdge:
        SE_DEBUG(group_encoder_stream, "%s 0x%p: EncodeFnFlushCoderEdge\n", MEDIA_STR(mStream->mMedia), mStream);
        mInputRing->InsertFront((uintptr_t)ControlStructureBuffer);
        break;

    default:
        SE_ERROR("Unsupported function call\n");
        ControlStructureBuffer->DecrementReferenceCount(IdentifierInSequenceCall);
        return EncoderNotSupported;
    }

    //
    // Send it to the appropriate process
    //
    return EncoderNoError;
}

EncoderStatus_t   PreprocessorToCoderEdge_c::PerformInSequenceCall(PlayerControlStructure_t *ControlStructure)
{
    EncoderStatus_t  Status = EncoderNoError;
    uint32_t    NbReleasedFrame = 0;

    switch (ControlStructure->InSequence.Fn)
    {
    case EncodeFnFlushCoderEdge:
        SE_DEBUG(group_encoder_stream, "Stream 0x%p EncodeFnFlushCoderEdge\n", mStream);

        while (true)
        {
            Buffer_t Buffer;
            RingStatus_t RingStatus = mInputRing->Extract((uintptr_t *) &Buffer, RING_NONE_BLOCKING);

            // The ring can be filled out by the TransporterEdge thread
            if (RingStatus == RingNothingToGet)
            {
                break;
            }

            Buffer->DecrementReferenceCount(IdentifierEncoderPreProcessToCoder);
            NbReleasedFrame++;
        }

        // Allow GetBuffer call
        mStream->GetCoder()->AcceptGetBufferCalls();

        // Perform specific actions on preporcessor
        mStream->GetCoder()->Flush();

        SE_DEBUG(group_encoder_stream, "Stream 0x%p EncodeFnFlushCoderEdge Released %d <\n", mStream, NbReleasedFrame);
        // Signal the end of FlushPreprocessorEdge
        OS_SetEvent((OS_Event_t *)ControlStructure->InSequence.Pointer);
        break;

    case EncodeFnFlushCoderObject:
        SE_DEBUG(group_encoder_stream, "Stream 0x%p EncodeFnFlushCoderObject\n", mStream);
        if (mStream->mMedia != STM_SE_ENCODE_STREAM_MEDIA_AUDIO)
        {
            // Perform specific actions on video coder when FlushInputStageOnly is true.
            mStream->GetCoder()->Flush();
        }
        break;

    default:
        SE_DEBUG(group_encoder_stream, "Stream 0x%p Not supported\n", mStream);
        Status  = EncoderNotSupported;
        break;
    }

    return Status;
}

EncoderStatus_t   PreprocessorToCoderEdge_c::Flush(bool FlushInputStageOnly)
{
    EncoderStatus_t Status = EncoderNoError;

    SE_DEBUG(group_encoder_stream, "Stream 0x%p FlushInputStageOnly %d >\n", mStream, FlushInputStageOnly);

    if (FlushInputStageOnly)
    {
        // Inform CoderEdge to flush the CODER
        CallInSequence(SequenceTypeImmediate, TIME_NOT_APPLICABLE, EncodeFnFlushCoderObject, NULL);
    }
    else
    {
        // unblock any pending GetBuffer call
        mStream->GetCoder()->RejectGetBufferCalls();

        OS_ResetEvent(&mStream->mEndOfStageFlushEvent);
        CallInSequence(SequenceTypeImmediate, TIME_NOT_APPLICABLE, EncodeFnFlushCoderEdge, &mStream->mEndOfStageFlushEvent);

        // Wait for the end of flush
        OS_Status_t WaitStatus = OS_WaitForEventInterruptible(&mStream->mEndOfStageFlushEvent, OS_INFINITE);
        if (WaitStatus == OS_INTERRUPTED)
        {
            Status = EncoderTimedOut;
            SE_INFO(group_encoder_stream, "Stream 0x%p wait for End of Flush interrupted; event->Valid:%d\n", mStream, OS_TestEventSet(&mStream->mEndOfStageFlushEvent));
        }
    }

    SE_DEBUG(group_encoder_stream, "Stream 0x%p <\n", mStream);

    return Status;
}
