/************************************************************************
Copyright (C) 2003-2014 STMicroelectronics. All Rights Reserved.

This file is part of the Streaming Engine.

Streaming Engine is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License version 2 as
published by the Free Software Foundation.

Streaming Engine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with Streaming Engine; see the file COPYING.  If not, write to the Free
Software Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307,
USA.

The Streaming Engine Library may alternatively be licensed under a
proprietary license from ST.
************************************************************************/
#ifndef H_PREPROCESSOR_TO_CODER
#define H_PREPROCESSOR_TO_CODER

#include "player_types.h"
#include "player_generic.h"
#include "active_edge_base.h"


#undef TRACE_TAG
#define TRACE_TAG "InputToPreprocessorEdge_c"

class PreprocessorToCoderEdge_c  : public ActiveEdge_Base_c
{
public:
    PreprocessorToCoderEdge_c(int ThreadDescId, EncodeStream_t Stream, int InputRingSize)
        : ActiveEdge_Base_c(ThreadDescId, Stream, InputRingSize)
        , mStream(Stream)
        , mSequenceNumber(INVALID_SEQUENCE_VALUE)
        , mMarkerInPreprocFrameIndex(INVALID_INDEX)
        , mNextBufferSequenceNumber(0)
        , mMaximumActualSequenceNumberSeen(0)
    {}

    virtual PlayerStatus_t   CallInSequence(PlayerSequenceType_t      SequenceType,
                                            PlayerSequenceValue_t     SequenceValue,
                                            PlayerComponentFunction_t Fn,
                                            ...);

public:
    EncodeStream_c          *mStream;
    virtual EncoderStatus_t  Flush(bool FlushInputStageOnly);

private:
    unsigned long long        mSequenceNumber;
    unsigned int              mMarkerInPreprocFrameIndex;
    unsigned long long        mNextBufferSequenceNumber;
    unsigned long long        mMaximumActualSequenceNumberSeen;

    virtual void MainLoop();
    virtual EncoderStatus_t  PerformInSequenceCall(PlayerControlStructure_t *ControlStructure);

    DISALLOW_COPY_AND_ASSIGN(PreprocessorToCoderEdge_c);
};

#endif // H_PREPROCESSOR_TO_CODER
