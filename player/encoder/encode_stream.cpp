/************************************************************************
Copyright (C) 2003-2014 STMicroelectronics. All Rights Reserved.

This file is part of the Streaming Engine.

Streaming Engine is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License version 2 as
published by the Free Software Foundation.

Streaming Engine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with Streaming Engine; see the file COPYING.  If not, write to the Free
Software Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307,
USA.

The Streaming Engine Library may alternatively be licensed under a
proprietary license from ST.
************************************************************************/

#include <stm_event.h>

#include "player_threads.h"

#include "ring_generic.h"
#include "encode_stream.h"
#include "transporter_tsmux.h"
#include "transporter_memsrc.h"
#include "input_to_preprocessor_edge.h"
#include "preprocessor_to_coder_edge.h"

#undef TRACE_TAG
#define TRACE_TAG "EncodeStream_c"

EncodeStream_c::EncodeStream_c(stm_se_encode_stream_media_t media)
    : mMedia(media)
    , BufferManager(NULL)
    , Terminating(false)
    , ProcessRunningCount(0)
    , StartStopLock()
    , StartStopEvent()
    , PreprocFrameBufferType(0)
    , PreprocFrameAllocType(0)
    , CodedFrameBufferType(0)
    , BufferInputBufferType(0)
    , InputMetaDataBufferType(0)
    , EncodeCoordinatorMetaDataBufferType(0)
    , MetaDataSequenceNumberType(0)
    , BufferEncoderControlStructureType(0)
    , mEndOfStageFlushEvent()
    , UnEncodable(false)
    , IsLowPowerState(false)
    , LowPowerEnterEvent()
    , LowPowerExitEvent()
    , SyncProcess()
    , Lock()
    , mAudioEncodeNo(0)
    , Next(NULL)
    , Statistics()
    , VideoEncodeMemoryProfile()
    , VideoEncodeInputColorFormatForecasted()
    , VideoEncodeFilterLevel(0)
    , mPortConnected(false)
    , mReleaseBufferCallBack(NULL)
    , mInputToPreprocessorEdge(NULL)
    , mPreprocessorToCoderEdge(NULL)
    , mCoderToTransportEdge(NULL)
{
    SE_VERBOSE(group_encoder_stream, "Stream 0x%p %s\n", this, MEDIA_STR(mMedia));

    OS_InitializeMutex(&Lock);
    OS_InitializeMutex(&SyncProcess);
    OS_InitializeMutex(&StartStopLock);

    OS_InitializeEvent(&StartStopEvent);
    OS_InitializeEvent(&LowPowerEnterEvent);
    OS_InitializeEvent(&LowPowerExitEvent);
    OS_InitializeEvent(&mEndOfStageFlushEvent);
}

EncodeStream_c::~EncodeStream_c()
{
    SE_VERBOSE(group_encoder_stream, "Stream 0x%p %s\n", this, MEDIA_STR(mMedia));

    // StopStream() already performed previously in TerminateEncodeStream()
    // and in case of error conditions in AddStream()
    // so no need to perform a StopStream() here (would lead to Bug23237)
    // Terminates are called in the reverse order from Initialize for Symmetry.
    OS_TerminateEvent(&mEndOfStageFlushEvent);
    OS_TerminateEvent(&LowPowerExitEvent);
    OS_TerminateEvent(&LowPowerEnterEvent);
    OS_TerminateEvent(&StartStopEvent);

    OS_TerminateMutex(&StartStopLock);
    OS_TerminateMutex(&SyncProcess);
    OS_TerminateMutex(&Lock);
}

EncoderStatus_t EncodeStream_c::GetEncoder(Encoder_t *Encoder)
{
    *Encoder = this->Encoder.Encoder;
    return EncoderNoError;
}

EncoderStatus_t   EncodeStream_c::Input(Buffer_t    Buffer)
{
    SE_VERBOSE(group_encoder_stream, ">Stream 0x%p %s\n", this, MEDIA_STR(mMedia));

    // We pass this to the PreProc stage ...
    OS_LockMutex(&SyncProcess);
    EncoderStatus_t Status;
    Status = GetPreproc()->Input(Buffer);
    if (Status != EncoderNoError)
    {
        SE_ERROR("Stream 0x%p %s Failed to Input data on the stream, Status = %u\n", this, MEDIA_STR(mMedia), Status);
    }
    OS_UnLockMutex(&SyncProcess);

    SE_VERBOSE(group_encoder_stream, "<Stream 0x%p %s\n", this, MEDIA_STR(mMedia));
    return Status;
}


EncoderStatus_t     EncodeStream_c::FlushStages(bool FlushInputStageOnly)
{
    EncoderStatus_t Status = EncoderNoError;

    // In NRT mode, we also need to release remaining buffers still stored in EncodeCoordinator
    if (Encoder.Encode->IsModeNRT() == true)
    {
        SE_INFO(group_encoder_stream, "Stream 0x%p %s NRT mode FlushInputStageOnly %d\n", this, MEDIA_STR(mMedia), FlushInputStageOnly);
        Status = Encoder.EncodeCoordinator->Flush(this, FlushInputStageOnly);
        if (Status != EncoderNoError)
        {
            SE_ERROR("Stream 0x%p %s Failed to flush EncodeCoordinator\n", this, MEDIA_STR(mMedia));
        }
    }
    else
    {
        SE_INFO(group_encoder_stream, "Stream 0x%p %s FlushInputStageOnly %d\n", this, MEDIA_STR(mMedia), FlushInputStageOnly);
        Status = InternalFlushStages(FlushInputStageOnly);
    }
    return Status;
}

EncoderStatus_t     EncodeStream_c::InternalFlushStages(bool FlushInputStageOnly)
{
    EncoderStatus_t EncoderStatus;

    EncoderStatus = mInputToPreprocessorEdge->Flush();
    if (EncoderStatus != EncoderNoError)
    {
        return EncoderStatus;
    }

    EncoderStatus = mPreprocessorToCoderEdge->Flush(FlushInputStageOnly);
    if (EncoderStatus != EncoderNoError)
    {
        return EncoderStatus;
    }

    if (FlushInputStageOnly == false)
    {
        EncoderStatus = mCoderToTransportEdge->Flush();
        if (EncoderStatus != EncoderNoError)
        {
            return EncoderStatus;
        }
    }

    return EncoderNoError;
}

EncoderStatus_t   EncodeStream_c::GetStatistics(encode_stream_statistics_t *ExposedStatistics)
{
    SE_VERBOSE(group_encoder_stream, "Stream 0x%p %s\n", this, MEDIA_STR(mMedia));
    SE_ASSERT(sizeof(encode_stream_statistics_t) == sizeof(EncodeStreamStatistics_t));

    // Return a *copy* of the statistics.
    *(EncodeStreamStatistics_t *)ExposedStatistics = Statistics;
    return EncoderNoError;
}

EncoderStatus_t   EncodeStream_c::ResetStatistics()
{
    SE_DEBUG(group_encoder_stream, "Stream 0x%p %s\n", this, MEDIA_STR(mMedia));
    memset(&(Statistics), 0, sizeof(EncodeStreamStatistics_t));
    return EncoderNoError;
}

//
// Stream Lifetime Support
//

EncoderStatus_t   EncodeStream_c::StartStream()
{
    unsigned int    CurrentProcessRunningCount;
    unsigned int    ProcessCreatedCount;
    OS_Status_t     OSStatus = OS_NO_ERROR;

    SE_INFO(group_encoder_stream, ">Stream 0x%p %s\n", this, MEDIA_STR(mMedia));

    int ThreadIdItoP;
    int ThreadIdPtoC;
    int ThreadIdCtoO;

    switch (mMedia)
    {
    case STM_SE_ENCODE_STREAM_MEDIA_AUDIO:
        ThreadIdItoP = SE_TASK_ENCOD_AUDITOP;
        ThreadIdPtoC = SE_TASK_ENCOD_AUDPTOC;
        ThreadIdCtoO = SE_TASK_ENCOD_AUDCTOO;
        break;
    case STM_SE_ENCODE_STREAM_MEDIA_VIDEO:
        ThreadIdItoP = SE_TASK_ENCOD_VIDITOP;
        ThreadIdPtoC = SE_TASK_ENCOD_VIDPTOC;
        ThreadIdCtoO = SE_TASK_ENCOD_VIDCTOO;
        break;
    case STM_SE_ENCODE_STREAM_MEDIA_ANY:
    default:
        SE_ERROR("Stream 0x%p %s invalid / non supported media type %d\n", this, MEDIA_STR(mMedia), mMedia);
        return EncoderError;
    }


    // Create Edges
    OS_ResetEvent(&StartStopEvent);
    ProcessCreatedCount = 0;

    mInputToPreprocessorEdge = new InputToPreprocessorEdge_c(ThreadIdItoP,
                                                             this,
                                                             ENCODER_MAX_INPUT_BUFFERS);
    if (mInputToPreprocessorEdge) { OSStatus = mInputToPreprocessorEdge->Start(); }
    else
    {
        OSStatus = OS_ERROR;
        SE_ERROR("%s Stream 0x%p Unable to initialize mInputToPreprocessorEdge\n", MEDIA_STR(mMedia), this);
    }
    if (OSStatus == OS_NO_ERROR)
    {
        ProcessCreatedCount ++;

        mPreprocessorToCoderEdge = new PreprocessorToCoderEdge_c(ThreadIdPtoC,
                                                                 this,
                                                                 ENCODER_STREAM_MAX_PREPROC_BUFFERS + ENCODER_MAX_CONTROL_STRUCTURE_BUFFERS);
        if (mPreprocessorToCoderEdge) { OSStatus = mPreprocessorToCoderEdge->Start(); }
        else
        {
            OSStatus = OS_ERROR;
            SE_ERROR("%s Stream 0x%p Unable to initialize PreprocessorToCoderEdge\n", MEDIA_STR(mMedia), this);
        }
        if (OSStatus == OS_NO_ERROR)
        {
            ProcessCreatedCount ++;

            mCoderToTransportEdge = new CoderToTransportEdge_c(ThreadIdCtoO,
                                                               this,
                                                               ENCODER_STREAM_MAX_CODED_BUFFERS + ENCODER_MAX_CONTROL_STRUCTURE_BUFFERS);
            if (mCoderToTransportEdge) { OSStatus = mCoderToTransportEdge->Start(); }
            else
            {
                OSStatus = OS_ERROR;
                SE_ERROR("%s Stream 0x%p Unable to initialize CoderToTransportEdge\n", MEDIA_STR(mMedia), this);
            }
            if (OSStatus == OS_NO_ERROR)
            {
                ProcessCreatedCount ++;
            }
        }
    }

    // Wait for all threads to be running
    CurrentProcessRunningCount = 0;
    while (CurrentProcessRunningCount != ProcessCreatedCount)
    {
        OS_Status_t WaitStatus = OS_WaitForEventAuto(&StartStopEvent, ENCODE_STREAM_MAX_EVENT_WAIT);

        OS_LockMutex(&StartStopLock);
        OS_ResetEvent(&StartStopEvent);
        CurrentProcessRunningCount = ProcessRunningCount;
        OS_UnLockMutex(&StartStopLock);

        if (WaitStatus == OS_TIMED_OUT)
        {
            SE_WARNING("Stream 0x%p %s Still waiting for Stream processes to run - ProcessRunningCount %d (expected %d)\n",
                       this, MEDIA_STR(mMedia), CurrentProcessRunningCount, ProcessCreatedCount);
        }
    }

    // Terminate all threads if one of the creation had failed
    if (ProcessCreatedCount != ENCODE_STREAM_PROCESS_NB)
    {
        SE_ERROR("%s Stream 0x%p Failed to create all stream processes\n", MEDIA_STR(mMedia), this);
        StopStream();
        return EncoderError;
    }

    // Now exchange the appropriate information between the classes
    Encoder.Preproc->RegisterOutputBufferPort(mPreprocessorToCoderEdge->GetInputPort());
    Encoder.Coder->RegisterOutputBufferPort(mCoderToTransportEdge->GetInputPort());

    // Perform component initialization
    EncoderStatus_t Status = Encoder.Coder->InitializeCoder();
    if (Status != EncoderNoError)
    {
        SE_ERROR("%s Stream 0x%p: Unable to initialize the encoder hardware\n", MEDIA_STR(mMedia), this);
        StopStream();
        return Status;
    }

    SE_DEBUG(group_encoder_stream, "<%s Stream 0x%p\n", MEDIA_STR(mMedia), this);
    return EncoderNoError;
}

EncoderStatus_t   EncodeStream_c::StopStream()
{
    unsigned int            CurrentProcessRunningCount;

    SE_INFO(group_encoder_stream, ">%s Stream 0x%p\n", MEDIA_STR(mMedia), this);

    // Set Discard Flags

    if (mPreprocessorToCoderEdge)
    {
        mPreprocessorToCoderEdge->DiscardUntilDrainMarkerFrame();
    }

    if (mCoderToTransportEdge)
    {
        mCoderToTransportEdge->DiscardUntilDrainMarkerFrame();
    }

    // Stop Components
    if (Encoder.Preproc)
    {
        Encoder.Preproc->Halt();
    }

    if (Encoder.Coder)
    {
        Encoder.Coder->Halt();
        Encoder.Coder->TerminateCoder();
    }

    if (Encoder.Transporter)
    {
        Encoder.Transporter->Halt();
    }

    // Ask threads to terminate
    OS_LockMutex(&StartStopLock);
    OS_ResetEvent(&StartStopEvent);
    CurrentProcessRunningCount = ProcessRunningCount;
    OS_UnLockMutex(&StartStopLock);

    // Write memory barrier: wmb_for_EncodeStream_Terminating coupled with: rmb_for_EncodeStream_Terminating
    OS_Smp_Mb();
    Terminating = true;

    // Wait for all threads termination
    while (CurrentProcessRunningCount != 0)
    {
        OS_Status_t WaitStatus = OS_WaitForEventAuto(&StartStopEvent, 2 * ENCODE_STREAM_MAX_EVENT_WAIT);

        OS_LockMutex(&StartStopLock);
        OS_ResetEvent(&StartStopEvent);
        CurrentProcessRunningCount = ProcessRunningCount;
        OS_UnLockMutex(&StartStopLock);

        if (WaitStatus == OS_TIMED_OUT)
        {
            SE_WARNING("Stream 0x%p %s: Still waiting for stream processes to terminate - ProcessRunningCount %d\n",
                       this, MEDIA_STR(mMedia), CurrentProcessRunningCount);
        }
    }

    //
    // Strip the rings - NOTE we assume we are the first owner on any buffer
    //
    if (mInputToPreprocessorEdge)
    {
        delete mInputToPreprocessorEdge;
        mInputToPreprocessorEdge = NULL;
    }

    if (mPreprocessorToCoderEdge)
    {
        delete mPreprocessorToCoderEdge;
        mPreprocessorToCoderEdge = NULL;
    }

    if (mCoderToTransportEdge)
    {
        delete mCoderToTransportEdge;
        mCoderToTransportEdge = NULL;
    }

    // Extract from Encode list
    // TODO: Manage the Encode List Lifetimes.

    SE_DEBUG(group_encoder_stream, "<Stream 0x%p %s\n", this, MEDIA_STR(mMedia));
    return EncoderNoError;
}

// //////////////////////////////////////////////////////////////////////////////////////////////////
//
//      A function to allow components to mark a stream as UnEncodable
//

EncoderStatus_t   EncodeStream_c::MarkStreamUnEncodable()
{
    SE_INFO(group_encoder_stream, "Stream 0x%p %s\n", this, MEDIA_STR(mMedia));
    //
    // TODO: We don't yet have support for policies in the Encoder
    // Check to see if a policy has been set to ignore this call.
    //
    //
    // Mark the stream as UnEncodable
    //
    UnEncodable     = true;
    //
    // Raise the event to signal this to the user
    //
    SignalEvent(STM_SE_ENCODE_STREAM_EVENT_FATAL_ERROR);
    //
    // Report the Error
    //
    SE_ERROR("Stream 0x%p %s marked as un-encodable\n", this, MEDIA_STR(mMedia));
    // But the call has completed successfully
    return EncoderNoError;
}

void   EncodeStream_c::GetClassList(Encode_t         *Encode,
                                    Preproc_t        *Preproc,
                                    Coder_t          *Coder,
                                    Transporter_t        *Transporter,
                                    EncodeCoordinator_t  *EncodeCoordinator)
{
    if (Encode)
    {
        *Encode = Encoder.Encode;
    }

    if (Preproc)
    {
        *Preproc = Encoder.Preproc;
    }

    if (Coder)
    {
        *Coder = Encoder.Coder;
    }

    if (Transporter)
    {
        *Transporter = Encoder.Transporter;
    }

    if (EncodeCoordinator)
    {
        *EncodeCoordinator = Encoder.EncodeCoordinator;
    }
}

EncoderStatus_t   EncodeStream_c::ManageMemoryProfile()
{
    SE_DEBUG(group_encoder_stream, "Stream 0x%p %s\n", this, MEDIA_STR(mMedia));

    EncoderStatus_t Status = Encoder.Preproc->ManageMemoryProfile();
    if (Status != EncoderNoError)
    {
        SE_ERROR("Stream 0x%p %s Failed to Manage Memory Profile for the Encode Preprocessor\n", this, MEDIA_STR(mMedia));
        return Status;
    }

    Status = Encoder.Coder->ManageMemoryProfile();
    if (Status != EncoderNoError)
    {
        SE_ERROR("Stream 0x%p %s Failed to Manage Memory Profile for the Encode Coder\n", this, MEDIA_STR(mMedia));
        return Status;
    }

    return Status;
}

EncoderStatus_t   EncodeStream_c::RegisterBufferManager(BufferManager_t BufferManager)
{
    EncoderStatus_t Status = EncoderNoError;
    const EncoderBufferTypes_t *BufferTypes = Encoder.Encoder->GetBufferTypes();
    SE_DEBUG(group_encoder_stream, "Stream 0x%p %s\n", this, MEDIA_STR(mMedia));

    if (this->BufferManager != NULL)
    {
        SE_ERROR("Stream 0x%p %s Buffer manager already registered\n", this, MEDIA_STR(mMedia));
        return EncoderError;
    }
    this->BufferManager = BufferManager;

    // Register the Buffer Types locally
    PreprocFrameBufferType              = BufferTypes->PreprocFrameBufferType;
    PreprocFrameAllocType               = BufferTypes->PreprocFrameAllocType;
    CodedFrameBufferType                = BufferTypes->CodedFrameBufferType;
    BufferInputBufferType               = BufferTypes->BufferInputBufferType;
    InputMetaDataBufferType             = BufferTypes->InputMetaDataBufferType;
    EncodeCoordinatorMetaDataBufferType = BufferTypes->EncodeCoordinatorMetaDataBufferType;
    MetaDataSequenceNumberType          = BufferTypes->MetaDataSequenceNumberType;
    BufferEncoderControlStructureType   = BufferTypes->BufferEncoderControlStructureType;

    // Initialize the working classes
    Status = Encoder.Preproc->RegisterBufferManager(BufferManager);
    if (Status != EncoderNoError)
    {
        SE_ERROR("Stream 0x%p %s Failed to Register Buffer Manager with the Preprocessor\n", this, MEDIA_STR(mMedia));
        return Status;
    }

    Status = Encoder.Coder->RegisterBufferManager(BufferManager);
    if (Status != EncoderNoError)
    {
        SE_ERROR("Stream 0x%p %s Failed to Register Buffer Manager with the Coder\n", this, MEDIA_STR(mMedia));
        return Status;
    }

    Status = Encoder.Transporter->RegisterBufferManager(BufferManager);
    if (Status != EncoderNoError)
    {
        SE_ERROR("Stream 0x%p %s Failed to Register Buffer Manager with the Transporter\n", this, MEDIA_STR(mMedia));
        return Status;
    }

    return Status;
}

Transporter_t EncodeStream_c::FindTransporter(stm_object_h  Sink)
{
    EncoderStatus_t       Status;
    Transporter_t         Transporter = NULL;
    SE_DEBUG(group_encoder_stream, "Stream 0x%p %s\n", this, MEDIA_STR(mMedia));

    // Probe each transporter to find one that can support this Sink Object
    // First match wins...

    // The Transport Engine TS Mux Device
    Status = Transporter_TSMux_c::Probe(Sink);
    if (Status == EncoderNoError)
    {
        SE_INFO(group_encoder_stream, "Stream 0x%p %s Constructing a Transporter_TSMux_c for Sink 0x%p\n", this, MEDIA_STR(mMedia), Sink);
        Transporter = new Transporter_TSMux_c;
        if ((Transporter == NULL) || (Transporter->InitializationStatus != TransporterNoError))
        {
            SE_ERROR("Stream 0x%p %s Transporter_TSMux_c failed to initialize for Sink 0x%p\n", this, MEDIA_STR(mMedia), Sink);
            delete Transporter;
            Transporter = NULL;
        }
        return Transporter;
    }

    // (The Source to) A MemSink Pull interface
    Status = Transporter_MemSrc_c::Probe(Sink);
    if (Status == EncoderNoError)
    {
        SE_INFO(group_encoder_stream, "Stream 0x%p %s Constructing a Transporter_MemSrc_c for Sink 0x%p\n", this, MEDIA_STR(mMedia), Sink);
        Transporter = new Transporter_MemSrc_c;
        if ((Transporter == NULL) || (Transporter->InitializationStatus != TransporterNoError))
        {
            SE_ERROR("Stream 0x%p %s Transporter_MemSrc_c failed to initialize for Sink 0x%p\n", this, MEDIA_STR(mMedia), Sink);
            delete Transporter;
            Transporter = NULL;
        }
        return Transporter;
    }

    SE_ERROR("Stream 0x%p %s No transporter found for Sink 0x%p\n", this, MEDIA_STR(mMedia), Sink);
    return NULL;
}

// //////////////////////////////////////////////////////////////////////////////////////////////////
//
//    Try to establish connection to sink port
//
EncoderStatus_t   EncodeStream_c::AddTransport(stm_object_h  sink)
{
    EncoderStatus_t       Status;
    Transporter_t         Transporter;
    Transporter_t         OldTransporter;

    SE_DEBUG(group_encoder_stream, "Stream 0x%p %s\n", this, MEDIA_STR(mMedia));

    // We need to find a Transporter capable of communicating with this object
    Transporter = FindTransporter(sink);
    if (Transporter == NULL)
    {
        return EncoderError;    // Failed to find/create a new transporter
    }

    // The New Transporter needs all of its registrations early
    Transporter->RegisterEncoder(Encoder.Encoder, Encoder.Encode, this, Encoder.Preproc, Encoder.Coder, Transporter);

    // Bring up the new Transporter
    Status = Transporter->RegisterBufferManager(BufferManager);
    if (Status != EncoderNoError)
    {
        SE_ERROR("Stream 0x%p %s Failed to Register Buffer Manager with the Transporter\n", this, MEDIA_STR(mMedia));
        delete Transporter; // We failed to register the Buffer Manager - so we won't use this Transporter
        return Status;
    }

    // Tear down the OldTransporter
    OldTransporter = Encoder.Transporter;
    OldTransporter->Halt();

    // Register the new Transporter with all of the Stream Components.
    // The Encoder and Encode do not have a specific Transporter so we do not register with them.
    this /* Stream */->RegisterEncoder(NULL, NULL, NULL, NULL, NULL, Transporter);
    Encoder.Preproc  ->RegisterEncoder(NULL, NULL, NULL, NULL, NULL, Transporter);
    Encoder.Coder    ->RegisterEncoder(NULL, NULL, NULL, NULL, NULL, Transporter);
    delete OldTransporter;

    // Finally - connect the New Transporter to the Sink object
    return Encoder.Transporter->CreateConnection(sink);
}

// //////////////////////////////////////////////////////////////////////////////////////////////////
//
//    Try to remove connection with specified sink port
//
EncoderStatus_t   EncodeStream_c::RemoveTransport(stm_object_h  sink)
{
    SE_DEBUG(group_encoder_stream, "Stream 0x%p %s\n", this, MEDIA_STR(mMedia));

    // We do not need to replace the current transporter with a NULL Transporter
    // as upon RemoveConnection it will only discard output.
    // If we are re-connected to a new output - a New transporter will be
    // created by the Add Transport call
    return Encoder.Transporter->RemoveConnection(sink);
}

//
// Managing Controls
//

EncoderStatus_t   EncodeStream_c::DelegateGetControl(stm_se_ctrl_t   Control,
                                                     void           *Data)
{
    EncoderStatus_t Status;

    // Try control on all components.
    // If return value is EncoderControlNotMatch, try next one.
    // If return value is EncoderUnsupportedControl, return error.
    Status = Encoder.Preproc->GetControl(Control, Data);

    if ((Status == EncoderNoError) || (Status == EncoderUnsupportedControl))
    {
        return Status;
    }

    Status = Encoder.Coder->GetControl(Control, Data);

    if ((Status == EncoderNoError) || (Status == EncoderUnsupportedControl))
    {
        return Status;
    }

    Status = Encoder.Transporter->GetControl(Control, Data);

    if ((Status == EncoderNoError) || (Status == EncoderUnsupportedControl))
    {
        return Status;
    }

    // Control is not known by these components.
    SE_ERROR("Stream 0x%p %s Not match control %d (%s)\n", this, MEDIA_STR(mMedia), Control, StringifyControl(Control));
    return EncoderControlNotMatch;
}

EncoderStatus_t   EncodeStream_c::DelegateGetCompoundControl(stm_se_ctrl_t   Control,
                                                             void           *Data)
{
    EncoderStatus_t Status;

    // Try compound control on all components.
    // If return value is EncoderControlNotMatch, try next one.
    // If return value is EncoderUnsupportedControl, return error.
    Status = Encoder.Preproc->GetCompoundControl(Control, Data);

    if ((Status == EncoderNoError) || (Status == EncoderUnsupportedControl))
    {
        return Status;
    }

    Status = Encoder.Coder->GetCompoundControl(Control, Data);

    if ((Status == EncoderNoError) || (Status == EncoderUnsupportedControl))
    {
        return Status;
    }

    Status = Encoder.Transporter->GetCompoundControl(Control, Data);

    if ((Status == EncoderNoError) || (Status == EncoderUnsupportedControl))
    {
        return Status;
    }

    // Compound control is not known by these components.
    SE_ERROR("Stream 0x%p %s Not match compound control %d (%s)\n", this, MEDIA_STR(mMedia), Control, StringifyControl(Control));
    return EncoderControlNotMatch;
}

EncoderStatus_t   EncodeStream_c::DelegateSetControl(stm_se_ctrl_t   Control,
                                                     const void     *Data)
{
    EncoderStatus_t Status;

    // Try control on all components.
    // If return value is EncoderControlNotMatch, try next one.
    // If return value is EncoderUnsupportedControl, return error.
    Status = Encoder.Preproc->SetControl(Control, Data);

    if ((Status == EncoderNoError) || (Status == EncoderUnsupportedControl))
    {
        return Status;
    }

    Status = Encoder.Coder->SetControl(Control, Data);

    if ((Status == EncoderNoError) || (Status == EncoderUnsupportedControl))
    {
        return Status;
    }

    Status = Encoder.Transporter->SetControl(Control, Data);

    if ((Status == EncoderNoError) || (Status == EncoderUnsupportedControl))
    {
        return Status;
    }

    // Control is not known by these components.
    SE_ERROR("Stream 0x%p %s Not match control %d (%s)\n", this, MEDIA_STR(mMedia), Control, StringifyControl(Control));
    return EncoderControlNotMatch;
}

EncoderStatus_t   EncodeStream_c::DelegateSetCompoundControl(stm_se_ctrl_t   Control,
                                                             const void     *Data)
{
    EncoderStatus_t Status;

    // Try compound control on all components.
    // If return value is EncoderControlNotMatch, try next one.
    // If return value is EncoderUnsupportedControl, return error.
    Status = Encoder.Preproc->SetCompoundControl(Control, Data);

    if ((Status == EncoderNoError) || (Status == EncoderUnsupportedControl))
    {
        return Status;
    }

    Status = Encoder.Coder->SetCompoundControl(Control, Data);

    if ((Status == EncoderNoError) || (Status == EncoderUnsupportedControl))
    {
        return Status;
    }

    Status = Encoder.Transporter->SetCompoundControl(Control, Data);

    if ((Status == EncoderNoError) || (Status == EncoderUnsupportedControl))
    {
        return Status;
    }

    // Compound control is not known by these components.
    SE_ERROR("Stream 0x%p %s Not match compound control %d (%s)\n", this, MEDIA_STR(mMedia), Control, StringifyControl(Control));
    return EncoderControlNotMatch;
}

EncoderStatus_t   EncodeStream_c::GetControl(stm_se_ctrl_t   Control,
                                             void           *Data)
{
    EncoderStatus_t Status = EncoderNoError;

    switch (Control)
    {
    // Handle any Stream Specific controls
    default:
        // Working Classes also have the opportunity to provide control data.
        Status = DelegateGetControl(Control, Data);
        break;
    }

    return Status;
}

EncoderStatus_t   EncodeStream_c::GetCompoundControl(stm_se_ctrl_t   Control,
                                                     void           *Data)
{
    EncoderStatus_t Status = EncoderNoError;

    switch (Control)
    {
    // Handle any Stream Specific controls
    default:
        // Working Classes also have the opportunity to provide control data.
        Status = DelegateGetCompoundControl(Control, Data);
        break;
    }

    return Status;
}

EncoderStatus_t   EncodeStream_c::SetControl(stm_se_ctrl_t   Control,
                                             const void     *Data)
{
    EncoderStatus_t Status = EncoderNoError;

    switch (Control)
    {
    // Handle any Stream Specific controls
    default:
        // Working Classes also have the opportunity to set control data.
        Status = DelegateSetControl(Control, Data);
        break;
    }

    return Status;
}

EncoderStatus_t   EncodeStream_c::SetCompoundControl(stm_se_ctrl_t   Control,
                                                     const void     *Data)
{
    EncoderStatus_t Status = EncoderNoError;

    switch (Control)
    {
    // Handle any Stream Specific controls
    default:
        // Working Classes also have the opportunity to set control data.
        Status = DelegateSetCompoundControl(Control, Data);
        break;
    }

    return Status;
}

EncoderStatus_t   EncodeStream_c::InjectDiscontinuity(stm_se_discontinuity_t Discontinuity)
{
    EncoderStatus_t Status;

    SE_INFO(group_encoder_stream, "Stream 0x%p %s\n", this, MEDIA_STR(mMedia));

    // We pass this to the PreProc stage
    OS_LockMutex(&SyncProcess);
    Status = Encoder.Preproc->InjectDiscontinuity(Discontinuity);
    if (Status != EncoderNoError)
    {
        SE_ERROR("Stream 0x%p %s Failed to inject discontinuity 0x%x (status %d)\n", this, MEDIA_STR(mMedia), Discontinuity, Status);
    }
    OS_UnLockMutex(&SyncProcess);

    return Status;
}

EncoderStatus_t   EncodeStream_c::SignalEvent(stm_se_encode_stream_event_t   Event)
{
    SE_DEBUG(group_encoder_stream, "Stream 0x%p %s Event %d\n", this, MEDIA_STR(mMedia), Event);

    if (Event == STM_SE_ENCODE_STREAM_EVENT_INVALID)
    {
        SE_ERROR("Stream 0x%p %s Invalid event\n", this, MEDIA_STR(mMedia));
        return EncoderError;
    }

    int32_t err;
    stm_event_t StreamEvent; // can this be a local variable?
    StreamEvent.event_id = (uint32_t)Event;
    StreamEvent.object   = (stm_object_h)Encoder.EncodeStream;

    OS_LockMutex(&Lock);
    err = stm_event_signal(&StreamEvent);
    if (err != 0)
    {
        OS_UnLockMutex(&Lock);
        SE_ERROR("Stream 0x%p %s Failed to Signal Event %d (%d)\n", this, MEDIA_STR(mMedia), Event, err);
        return EncoderError;
    }
    OS_UnLockMutex(&Lock);

    return EncoderNoError;
}

//
// Encode streams linked list management
//

EncodeStream_t    EncodeStream_c::GetNext()
{
    return Next;
}

void    EncodeStream_c::SetNext(EncodeStream_t Stream)
{
    Next = Stream;
}

VideoEncodeMemoryProfile_t    EncodeStream_c::GetVideoEncodeMemoryProfile()
{
    SE_DEBUG(group_encoder_stream, "Stream 0x%p %s %u\n", this, MEDIA_STR(mMedia), VideoEncodeMemoryProfile);
    return VideoEncodeMemoryProfile;
}

void    EncodeStream_c::SetVideoEncodeMemoryProfile(VideoEncodeMemoryProfile_t MemoryProfile)
{
    SE_DEBUG(group_encoder_stream, "Stream 0x%p %s %u\n", this, MEDIA_STR(mMedia), MemoryProfile);
    VideoEncodeMemoryProfile = MemoryProfile;
}

surface_format_t    EncodeStream_c::GetVideoInputColorFormatForecasted()
{
    SE_DEBUG(group_encoder_stream, "Stream 0x%p %s %u\n", this, MEDIA_STR(mMedia), VideoEncodeInputColorFormatForecasted);
    return VideoEncodeInputColorFormatForecasted;
}

void    EncodeStream_c::SetVideoInputColorFormatForecasted(surface_format_t ForecastedFormat)
{
    SE_DEBUG(group_encoder_stream, "Stream 0x%p %s %u\n", this, MEDIA_STR(mMedia), ForecastedFormat);
    VideoEncodeInputColorFormatForecasted = ForecastedFormat;
}

int     EncodeStream_c::GetVideoEncodeFilterLevel()
{
    SE_VERBOSE(group_encoder_stream, "%s 0x%p: %u\n", MEDIA_STR(mMedia), this, VideoEncodeFilterLevel);
    return VideoEncodeFilterLevel;
}

void    EncodeStream_c::SetVideoEncodeFilterLevel(int FilterLevel)
{
    SE_VERBOSE(group_encoder_stream, "%s 0x%p: %u\n", MEDIA_STR(mMedia), this, FilterLevel);
    VideoEncodeFilterLevel = FilterLevel;
}

//
// Low power functions
//

EncoderStatus_t   EncodeStream_c::LowPowerEnter()
{
    SE_INFO(group_encoder_stream, ">Stream 0x%p %s\n", this, MEDIA_STR(mMedia));

    // Reset events used for putting PreprocessorToCoder thread into "low power"
    OS_ResetEvent(&LowPowerEnterEvent);
    OS_ResetEvent(&LowPowerExitEvent);
    // Save low power state
    IsLowPowerState = true;

    // Wait for PreprocessorToCoder thread to be in safe state (no more MME commands issued)
    OS_Status_t WaitStatus = OS_WaitForEventInterruptible(&LowPowerEnterEvent, OS_INFINITE);
    if (WaitStatus == OS_INTERRUPTED)
    {
        SE_INFO(group_encoder_stream, "Stream 0x%p wait for LP enter interrupted; LowPowerEnterEvent:%d\n", this, LowPowerEnterEvent.Valid);
    }

    // Ask preproc and coder to enter low power state
    // For CPS mode, MME transformers will be terminated now...
    if (Encoder.Preproc != NULL)
    {
        Encoder.Preproc->LowPowerEnter();
    }

    if (Encoder.Coder != NULL)
    {
        Encoder.Coder->LowPowerEnter();
    }

    SE_DEBUG(group_encoder_stream, "<Stream 0x%p %s\n", this, MEDIA_STR(mMedia));
    return EncoderNoError;
}

EncoderStatus_t   EncodeStream_c::LowPowerExit()
{
    SE_INFO(group_encoder_stream, ">Stream 0x%p %s\n", this, MEDIA_STR(mMedia));

    // Ask preproc and coder to exit low power state
    // For CPS mode, MME transformers will be re-initialized now...
    if (Encoder.Coder != NULL)
    {
        Encoder.Coder->LowPowerExit();
    }

    if (Encoder.Preproc != NULL)
    {
        Encoder.Preproc->LowPowerExit();
    }

    // Reset low power state
    IsLowPowerState = false;
    // Wake-up PreprocessorToCoder thread
    OS_SetEventInterruptible(&LowPowerExitEvent);

    SE_DEBUG(group_encoder_stream, "<Stream 0x%p %s\n", this, MEDIA_STR(mMedia));
    return EncoderNoError;
}

//{{{  EncodeStream_c::Disconnect
//{{{  doxynote
/// \brief                      Allow to disconnect EncodeStrean from source in tunneled mode.
/// \return                     EncoderError   : EncodeStream not connected in tunneled mode
///                             EncoderNoError : EncodeStream is disconnected from source in tunneled mode
//}}}
EncoderStatus_t     EncodeStream_c::Disconnect()
{
    SE_INFO(group_encoder_stream, ">Stream 0x%p %s\n", this, MEDIA_STR(mMedia));

    // Calling Preproc->Halt() and Coder->Halt() is a WA that simply aborts any calls to prevent us being blocked.
    // It must be done before locking the SyncProcess mutex that may be already locked the Input() method.

    // Calling Halt() effectively stops the Coder and Preproc from
    // being locked in requesting new *buffers* and prevents them from
    // requesting new ones, preventing further deadlocks.
    // Calling Halt() does not prevent the threads from waiting for *Contexts*,
    // for HW/st231 deferred commands to complete before existing:
    // this is required to exit clean.

    // R.G.: Current WA might have some issues with video due to different behavior
    // Audio and video stream have not the same behaviour at closure:
    // - audio waits for commands to complete in detach (inside Halt())
    // - video waits for commands to complete in delete (inside TerminateCoder())
    // That means that after detach() call, we might still get some command callbacks on video stream.

    PreprocStatus_t PStatus;
    PStatus = Encoder.Preproc->Halt();
    if (PreprocNoError != PStatus)
    {
        SE_ERROR("Stream 0x%p %s Preproc->Halt() error %08X\n", this, MEDIA_STR(mMedia), PStatus);
        //continue
    }

    CoderStatus_t   CStatus;
    CStatus = Encoder.Coder->Halt();
    if (CoderNoError != CStatus)
    {
        SE_ERROR("Stream 0x%p %s Coder->Halt() error %08X\n", this, MEDIA_STR(mMedia), CStatus);
        //continue
    }

    // make sure this encode stream is connected
    OS_LockMutex(&SyncProcess);

    if (mPortConnected == false)
    {
        SE_ERROR("Stream 0x%p %s Connection doesn't exist for this EncodeStream\n", this, MEDIA_STR(mMedia));
        OS_UnLockMutex(&SyncProcess);
        return EncoderError;
    }

    // Flush remaining buffers
    FlushStages(true);

    mPortConnected         = false;
    mReleaseBufferCallBack = NULL;

    OS_UnLockMutex(&SyncProcess);

    // In NRT mode, disconnect EncodeCoordinator
    if (Encoder.Encode->IsModeNRT() == true)
    {
        if (Encoder.EncodeCoordinator != NULL)
        {
            Encoder.EncodeCoordinator->Disconnect(this);
        }
    }

    SE_DEBUG(group_encoder_stream, "<Stream 0x%p %s\n", this, MEDIA_STR(mMedia));
    return EncoderNoError;
}
//}}}

//{{{  EncodeStream_c::Connect
//{{{  doxynote
/// \brief                      Allow to connect a source to this EncodeStrean in tunneled mode and to set the Release callback.
/// \param                      src_object        : Source to be connected with. This handle should be provided
///                                                 when inserted EncodeInputBuffer will be released
/// \param                      release_callback  : Callback to execute to released all EncodeInputBuffer
/// \param                      inputRing         : EncodeStream input port
/// \return                     EncoderError   : EncodeStream already connected to a source in tunneled mode
///                             EncoderNoError : EncodeStream is connected to a source in tunneled mode
//}}}
EncoderStatus_t     EncodeStream_c::Connect(ReleaseBufferInterface_c *ReleaseBufferItf, Port_c **InputPort)
{
    SE_INFO(group_encoder_stream, ">Stream 0x%p %s\n", this, MEDIA_STR(mMedia));

    //
    // make sure this encode stream is not yet connected
    //
    OS_LockMutex(&SyncProcess);

    if (mPortConnected == true)
    {
        SE_ERROR("Stream 0x%p %s Connection already exists for this EncodeStream\n", this, MEDIA_STR(mMedia));
        OS_UnLockMutex(&SyncProcess);
        return EncoderError;
    }

    // In NRT mode, we substitute EncodeStream connection by EncodeCoordinator connection
    // The input port and the release buffer interfaces are those from EncodeCoordinator
    if (Encoder.Encode->IsModeNRT() == true)
    {
        // NRT mode: connect to EncodeCoordinator
        EncoderStatus_t Status;
        ReleaseBufferInterface_c *EncodeCoordinatorReleaseBufferItf;
        Port_c                   *EncodeCoordinatorInputPort;

        Status = Encoder.EncodeCoordinator->Connect(this, ReleaseBufferItf, &EncodeCoordinatorReleaseBufferItf, &EncodeCoordinatorInputPort);
        if (Status != EncoderNoError)
        {
            SE_ERROR("Stream 0x%p %s Failed to connect to EncodeCoordinator\n", this, MEDIA_STR(mMedia));
            OS_UnLockMutex(&SyncProcess);
            return EncoderError;
        }

        mReleaseBufferCallBack = EncodeCoordinatorReleaseBufferItf;
        *InputPort             = EncodeCoordinatorInputPort;
    }
    else
    {
        // Real-time mode (legacy)
        // We connect directly to the EncodeStream input port, EncodeCoordinator is not used
        mReleaseBufferCallBack = ReleaseBufferItf;
        *InputPort             = mInputToPreprocessorEdge->GetInputPort();
    }

    mPortConnected = true;
    OS_UnLockMutex(&SyncProcess);

    SE_DEBUG(group_encoder_stream, "<Stream 0x%p %s\n", this, MEDIA_STR(mMedia));
    return  EncoderNoError;
}
//}}}

//{{{  EncodeStream_c::ReleaseInputBuffer
//{{{  doxynote
/// \brief                      Allow to release a previously pushed EncodeInputBuffer.
/// \param                      Buffer         : Encode input buffer to release
/// \return                     EncoderError   : no call back available or release refused
///                             EncoderNoError : buffer is released
//}}}
EncoderStatus_t     EncodeStream_c::ReleaseInputBuffer(Buffer_t  Buffer)
{
    SE_VERBOSE(group_encoder_stream, "Stream 0x%p %s Buffer 0x%p\n", this, MEDIA_STR(mMedia), Buffer);

    if (mReleaseBufferCallBack)
    {
        if (mReleaseBufferCallBack->ReleaseBuffer(Buffer) != PlayerNoError)
        {
            SE_ERROR("Stream 0x%p %s Release buffer callback failure - Buffer 0x%p\n", this, MEDIA_STR(mMedia), Buffer);
            return EncoderError;
        }
    }
    else
    {
        SE_ERROR("Stream 0x%p %s No release buffer callback available - Buffer 0x%p\n", this, MEDIA_STR(mMedia), Buffer);
        return EncoderError;
    }

    return EncoderNoError;
}
//}}}

