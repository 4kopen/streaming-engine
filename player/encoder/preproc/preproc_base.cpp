/************************************************************************
Copyright (C) 2003-2014 STMicroelectronics. All Rights Reserved.

This file is part of the Streaming Engine.

Streaming Engine is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License version 2 as
published by the Free Software Foundation.

Streaming Engine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with Streaming Engine; see the file COPYING.  If not, write to the Free
Software Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307,
USA.

The Streaming Engine Library may alternatively be licensed under a
proprietary license from ST.
************************************************************************/

#include "preproc_base.h"
#include "allocinline.h"

#undef TRACE_TAG
#define TRACE_TAG "Preproc_Base_c"

extern "C" int snprintf(char *buf, long size, const char *fmt, ...) __attribute__((format(printf, 3, 4))) ;

static BufferDataDescriptor_t PreprocContextDescriptor = BUFFER_PREPROC_CONTEXT_TYPE;

Preproc_Base_c::Preproc_Base_c()
    : Configuration()
    , PreprocFrameBuffer(NULL)
    , PreprocFrameBufferType(0)
    , PreprocFrameAllocType(0)
    , InputBufferType(0)
    , InputMetaDataBufferType(0)
    , EncodeCoordinatorMetaDataBufferType(0)
    , OutputMetaDataBufferType(0)
    , MetaDataSequenceNumberType(0)
    , OutputPort(NULL)
    , BufferManager(NULL)
    , PreprocFrameBufferPool(NULL)
    , PreprocFrameAllocPool(NULL)
    , PreprocFrameMemoryDevice(NULL)
    , PreprocFrameMemory()
    , PreprocMemorySize(0)        // to be set through SetFrameMemory() by derived class
    , PreprocFrameMaximumSize(0)  // to be set through SetFrameMemory() by derived class
    , PreprocMaxNbAllocBuffers(0) // to be set through SetFrameMemory() by derived class
    , PreprocMaxNbBuffers(ENCODER_STREAM_MAX_PREPROC_BUFFERS)
    , PreprocMemoryPartitionName()
    , InputMetaDataDescriptor(NULL)
    , EncodeCoordinatorMetaDataDescriptor(NULL)
    , PreprocMetaDataDescriptor(NULL)
    , PreprocFullMetaDataDescriptor(NULL)
    , PreprocContextBuffer(NULL)
    , PreprocContextBufferType(0)
    , PreprocContextBufferPool(NULL)
    , PreprocDiscontinuity(STM_SE_DISCONTINUITY_CONTINUOUS)
    , InputBufferSize(0)
{
    // Non null because not all classes use it.
    // This will be overwritten by child classes if needed
    Configuration.PreprocContextDescriptor = &PreprocContextDescriptor;
}

Preproc_Base_c::~Preproc_Base_c()
{
    Halt();

    AllocatorClose(&PreprocFrameMemoryDevice);

    if (PreprocFrameAllocPool != NULL)
    {
        PreprocFrameAllocPool->DetachMetaData(MetaDataSequenceNumberType);
        BufferManager->DestroyPool(PreprocFrameAllocPool);
    }

    if (PreprocFrameBufferPool != NULL)
    {
        PreprocFrameBufferPool->DetachMetaData(OutputMetaDataBufferType);
        PreprocFrameBufferPool->DetachMetaData(MetaDataSequenceNumberType);
        BufferManager->DestroyPool(PreprocFrameBufferPool);
    }

    if (PreprocContextBufferPool != NULL)
    {
        BufferManager->DestroyPool(PreprocContextBufferPool);
    }
}

PreprocStatus_t Preproc_Base_c::Halt()
{
    BaseComponentClass_c::Halt();
    return PreprocNoError;
}

PreprocStatus_t Preproc_Base_c::SetFrameMemory(unsigned int FrameSize, unsigned int NbBuffers)
{
    SE_DEBUG(GetGroupTrace(), "Stream 0x%p FrameSize:%d NbBuffers:%d\n", Encoder.EncodeStream, FrameSize, NbBuffers);
    PreprocMemorySize        = FrameSize * NbBuffers;
    PreprocFrameMaximumSize  = FrameSize;
    PreprocMaxNbAllocBuffers = NbBuffers;
    return PreprocNoError;
}

PreprocStatus_t Preproc_Base_c::ManageMemoryProfile()
{
    return PreprocNoError;
}

PreprocStatus_t Preproc_Base_c::RegisterBufferManager(BufferManager_t   BufferManager)
{
    allocator_status_t  AStatus = allocator_ok;

    if (this->BufferManager != NULL)
    {
        SE_ERROR("Stream 0x%p Attempt to change buffer manager, not permitted\n", Encoder.EncodeStream);
        return PreprocError;
    }
    SE_ASSERT(PreprocFrameMemoryDevice == NULL);
    SE_ASSERT(PreprocFrameAllocPool == NULL);
    SE_ASSERT(PreprocFrameBufferPool == NULL);

    this->BufferManager = BufferManager;

    const EncoderBufferTypes_t *BufferTypes = Encoder.Encoder->GetBufferTypes();
    PreprocFrameBufferType      = BufferTypes->PreprocFrameBufferType;
    PreprocFrameAllocType       = BufferTypes->PreprocFrameAllocType;
    //input metadata preproc buffer is of type METADATA_ENCODE_FRAME_PARAMETERS_TYPE (stm_se_uncompressed_frame_metadata_t)
    InputMetaDataBufferType     = BufferTypes->InputMetaDataBufferType;
    //encode coordinator metadata preproc buffer is of type METADATA_ENCODE_COORDINATOR_FRAME_PARAMETERS_TYPE (__stm_se_encode_coordinator_metadata_t)
    EncodeCoordinatorMetaDataBufferType = BufferTypes->EncodeCoordinatorMetaDataBufferType;
    //output metadata preproc buffer is of type METADATA_INTERNAL_ENCODE_FRAME_PARAMETERS_TYPE (__stm_se_frame_metadata_t)
    OutputMetaDataBufferType    = BufferTypes->InternalMetaDataBufferType;
    InputBufferType             = BufferTypes->BufferInputBufferType;
    MetaDataSequenceNumberType  = BufferTypes->MetaDataSequenceNumberType;

    // Input meta data management
    if ((PreprocFrameBufferType == NULL) || (PreprocFrameAllocType == NULL) || (InputMetaDataBufferType == NULL) || (OutputMetaDataBufferType == NULL) ||
        (EncodeCoordinatorMetaDataBufferType == NULL) || (InputBufferType == NULL) || (MetaDataSequenceNumberType == NULL))
    {
        SE_ERROR("Stream 0x%p Invalid type registrations\n", Encoder.EncodeStream);
        return EncoderError;
    }

    //
    // Get the memory and Create the pool with it
    //
    unsigned int EncodeID = Encoder.Encode->GetEncodeID();
    snprintf(PreprocMemoryPartitionName, sizeof(PreprocMemoryPartitionName), "%s-%d", PREPROC_FRAME_BUFFER_PARTITION, (EncodeID % 2));
    PreprocMemoryPartitionName[sizeof(PreprocMemoryPartitionName) - 1] = '\0';

    SE_DEBUG(GetGroupTrace(), "Stream 0x%p Allocate memory for %s %d\n", Encoder.EncodeStream, PreprocMemoryPartitionName, PreprocMemorySize);

    unsigned int        MemoryAccessType;
    if (Encoder.EncodeStream->GetMedia() == STM_SE_ENCODE_STREAM_MEDIA_VIDEO)
    {
        // Not memory mapped
        MemoryAccessType = MEMORY_VIDEO_HWONLY_ACCESS;
    }
    else
    {
        // Memory mapping needed for Audio
        MemoryAccessType = MEMORY_AUDIO_ICS_UNCACHED_ACCESS;
    }

    AStatus = PartitionAllocatorOpen(&PreprocFrameMemoryDevice, PreprocMemoryPartitionName, PreprocMemorySize, MemoryAccessType);

    if (AStatus != allocator_ok)
    {
        PreprocFrameMemoryDevice = NULL;
        SE_ERROR("Stream 0x%p Failed to allocate memory for %s %d\n", Encoder.EncodeStream, PreprocMemoryPartitionName, PreprocMemorySize);
        return EncoderNoMemory;
    }

    PreprocFrameMemory[CachedAddress]         = AllocatorUserAddress(PreprocFrameMemoryDevice);
    PreprocFrameMemory[PhysicalAddress]       = AllocatorPhysicalAddress(PreprocFrameMemoryDevice);

    // Initialize preproc frame alloc pool
    BufferStatus_t BufferStatus = BufferManager->CreatePool(&PreprocFrameAllocPool, PreprocFrameAllocType,
                                                            PreprocMaxNbAllocBuffers,
                                                            PreprocMemorySize,
                                                            PreprocFrameMemory);
    if (BufferStatus != BufferNoError)
    {
        PreprocFrameAllocPool = NULL;
        AllocatorClose(&PreprocFrameMemoryDevice);
        SE_ERROR("Stream 0x%p Failed to create the alloc pool\n", Encoder.EncodeStream);
        return PreprocError;
    }

    BufferStatus = PreprocFrameAllocPool->AttachMetaData(MetaDataSequenceNumberType, sizeof(EncoderSequenceNumber_t));
    if (BufferStatus != BufferNoError)
    {
        SE_ERROR("Stream 0x%p Failed to attach the sequence number meta data to preproc buffer\n", Encoder.EncodeStream);
        return PreprocError;
    }

    // Initialize preproc frame buffer pool
    BufferStatus  = BufferManager->CreatePool(&PreprocFrameBufferPool, PreprocFrameBufferType, PreprocMaxNbBuffers);
    if (BufferStatus != BufferNoError)
    {
        PreprocFrameBufferPool = NULL;
        SE_ERROR("Stream 0x%p Failed to create the buffer pool\n", Encoder.EncodeStream);
        return PreprocError;
    }

    BufferStatus = PreprocFrameBufferPool->AttachMetaData(OutputMetaDataBufferType, sizeof(__stm_se_frame_metadata_t));
    if (BufferStatus != BufferNoError)
    {
        SE_ERROR("Stream 0x%p Failed to attach the output meta data to preproc buffer\n", Encoder.EncodeStream);
        return PreprocError;
    }

    BufferStatus = PreprocFrameBufferPool->AttachMetaData(MetaDataSequenceNumberType, sizeof(EncoderSequenceNumber_t));
    if (BufferStatus != BufferNoError)
    {
        SE_ERROR("Stream 0x%p Failed to attach the sequence number meta data to preproc buffer\n", Encoder.EncodeStream);
        return PreprocError;
    }

    return PreprocNoError;
}

PreprocStatus_t Preproc_Base_c::RegisterOutputBufferPort(Port_c       *Port)
{
    OutputPort = Port;

    BufferStatus_t BufferStatus = BufferManager->FindBufferDataType(Configuration.PreprocContextDescriptor->TypeName,
                                                                    &PreprocContextBufferType);
    if (BufferStatus != BufferNoError)
    {
        // Create buffer data type if it does not exist
        BufferStatus = BufferManager->CreateBufferDataType(Configuration.PreprocContextDescriptor, &PreprocContextBufferType);

        if (BufferStatus != BufferNoError)
        {
            SE_ERROR("Stream 0x%p Unable to create preproc context buffer type\n", Encoder.EncodeStream);
            return PreprocError;
        }
    }

    SE_ASSERT(PreprocContextBufferPool == NULL);
    BufferStatus = BufferManager->CreatePool(&PreprocContextBufferPool, PreprocContextBufferType, Configuration.PreprocContextCount);
    if (BufferStatus != BufferNoError)
    {
        PreprocContextBufferPool = NULL;
        SE_ERROR("Stream 0x%p Unable to create preproc context pool\n", Encoder.EncodeStream);
        return PreprocError;
    }

    // We have an Output Ring, and can now be considered an object with a destination
    SetComponentState(ComponentRunning);
    return PreprocNoError;
}

PreprocStatus_t Preproc_Base_c::OutputPartialPreprocBuffers()
{
    return PreprocNoError;
}

PreprocStatus_t Preproc_Base_c::GetNewBuffer(Buffer_t *Buffer, bool IsADiscontinuityBuffer)
{
    BufferStatus_t BufferStatus = BufferNoError;
    Buffer_t       ContentBuffer;
    unsigned    int PreprocSize = PreprocFrameMaximumSize;
    uint64_t TimeBetweenMessageUs = 5000000ULL;
    uint64_t EntryTime            = OS_GetTimeInMicroSeconds();
    uint32_t GetBufferTimeOutMs   = 100;
    uint32_t GetBufferRetryMs      = 10;

    // For a pure discontinuity buffer, ask for a buffer with minimum size
    // Buffer of size 0 cannot be used because not well handled by other SE objects
    if (IsADiscontinuityBuffer) { PreprocSize = 1; }

    do
    {
        // Ask buffer in non blocking mode
        // TODO(FSO) : to revert to blocking call when bug53292 will be fixed
        BufferStatus = PreprocFrameAllocPool->GetBuffer(&ContentBuffer, IdentifierEncoderPreprocessor, PreprocSize,
                                                        true, false,
                                                        GetBufferTimeOutMs,
                                                        true);

        // Warning message every TimeBetweenMessageUs us
        if ((OS_GetTimeInMicroSeconds() - EntryTime) > TimeBetweenMessageUs)
        {
            EntryTime = OS_GetTimeInMicroSeconds();
            SE_WARNING("Stream 0x%p Preproc(%p)->PreprocFrameAllocPool(%p)->GetBuffer still NoFreeBufferAvailable with %s\n",
                       Encoder.EncodeStream, this, PreprocFrameAllocPool,
                       TestComponentState(ComponentRunning) ? "ComponentRunning" : "!ComponentRunning");
        }

        if ((BufferNoError == BufferStatus) ||
            (TestComponentState(ComponentRunning) == false))
        {
            break;
        }

        // Wait before retrying to get a buffer in NonBlocking mode
        OS_SleepMilliSeconds(GetBufferRetryMs);
    }
    while (true);

    if ((BufferStatus != BufferNoError) || (ContentBuffer == NULL))
    {
        PREPROC_ERROR_RUNNING("Stream 0x%p Unable to get preproc frame alloc buffer\n", Encoder.EncodeStream);
        *Buffer = NULL;
        return PreprocError;
    }

    PreprocStatus_t Status = GetBufferClone(ContentBuffer, Buffer);

    // Release the reference to the first clone of the content buffer
    ContentBuffer->DecrementReferenceCount(IdentifierEncoderPreprocessor);

    return Status;
}

PreprocStatus_t Preproc_Base_c::GetBufferClone(Buffer_t Buffer, Buffer_t *CloneBuffer)
{
    BufferStatus_t BufferStatus;
    uint32_t        DataSize;
    void           *BufferAddr[3];
    BufferType_t    BufferType;
    Buffer_t        ContentBuffer;

    *CloneBuffer = NULL;

    // Check if buffer type is correct before cloning
    Buffer->GetType(&BufferType);

    // Retrieve the content buffer
    if (BufferType == PreprocFrameAllocType)
    {
        ContentBuffer = Buffer;
    }
    else if (BufferType == PreprocFrameBufferType)
    {
        Buffer->ObtainAttachedBufferReference(PreprocFrameAllocType, &ContentBuffer);
        SE_ASSERT(ContentBuffer != NULL);
    }
    else
    {
        SE_ERROR("Stream 0x%p Cloning of buffer type %d not supported\n", Encoder.EncodeStream, BufferType);
        return PreprocError;
    }

    // Get new preproc frame buffer
    uint64_t TimeBetweenMessageUs = 5000000ULL;
    uint64_t EntryTime            = OS_GetTimeInMicroSeconds();
    uint32_t GetBufferTimeOutMs   = 100;
    do
    {
        BufferStatus = PreprocFrameBufferPool->GetBuffer(CloneBuffer, IdentifierEncoderPreprocessor,
                                                         UNSPECIFIED_SIZE, false, false,
                                                         GetBufferTimeOutMs);

        // Warning message every TimeBetweenMessageUs us
        if ((OS_GetTimeInMicroSeconds() - EntryTime) > TimeBetweenMessageUs)
        {
            EntryTime = OS_GetTimeInMicroSeconds();
            SE_WARNING("Stream 0x%p Preproc(%p)->PreProcFrameBufferPool(%p)->GetBuffer still NoFreeBufferAvailable with %s\n",
                       Encoder.EncodeStream, this, PreprocFrameBufferPool,
                       TestComponentState(ComponentRunning) ? "ComponentRunning" : "!ComponentRunning");
        }
    }
    while ((BufferNoFreeBufferAvailable == BufferStatus) && TestComponentState(ComponentRunning));

    if ((BufferStatus != BufferNoError) || (*CloneBuffer == NULL))
    {
        PREPROC_ERROR_RUNNING("Stream 0x%p Unable to get preproc frame buffer\n", Encoder.EncodeStream);
        *CloneBuffer = NULL;
        return PreprocError;
    }

    // Initialize buffer address
    BufferAddr[0] = NULL;
    BufferAddr[1] = NULL;
    BufferAddr[2] = NULL;
    // Get physical address of buffer
    ContentBuffer->ObtainDataReference(NULL, &DataSize, (void **)(&BufferAddr[PhysicalAddress]), PhysicalAddress);
    if (BufferAddr[PhysicalAddress] == NULL)
    {
        SE_ERROR("Stream 0x%p Unable to get buffer phys reference\n", Encoder.EncodeStream);
        (*CloneBuffer)->DecrementReferenceCount(IdentifierEncoderPreprocessor);
        *CloneBuffer = NULL;
        return PreprocError;
    }

    // Get virtual address of buffer for other Media than VIDEO
    if (Encoder.EncodeStream->GetMedia() != STM_SE_ENCODE_STREAM_MEDIA_VIDEO)
    {
        ContentBuffer->ObtainDataReference(NULL, &DataSize, (void **)(&BufferAddr[CachedAddress]), CachedAddress);
        if (BufferAddr[CachedAddress] == NULL)
        {
            SE_ERROR("Stream 0x%p Unable to get buffer reference\n", Encoder.EncodeStream);
            (*CloneBuffer)->DecrementReferenceCount(IdentifierEncoderPreprocessor);
            *CloneBuffer = NULL;
            return PreprocError;
        }
    }
    // Register Input buffer pointers into the buffer copy
    (*CloneBuffer)->RegisterDataReference(PreprocFrameMaximumSize, BufferAddr);
    (*CloneBuffer)->SetUsedDataSize(DataSize);

    // Attach the content buffer to the BufferCopy so that it is kept until Preprocessing is complete.
    (*CloneBuffer)->AttachBuffer(ContentBuffer);
    StampFrame(ContentBuffer, (*CloneBuffer));

    return PreprocNoError;
}

PreprocStatus_t Preproc_Base_c::GetNewContext(Buffer_t *Buffer)
{
    if (NULL == Buffer)
    {
        SE_ERROR("Stream 0x%p Buffer is NULL\n", Encoder.EncodeStream);
        return PreprocError;
    }

    uint64_t TimeBetweenMessageUs = 5000000ULL;
    uint64_t EntryTime            = OS_GetTimeInMicroSeconds();
    uint32_t GetBufferTimeOutMs   = 100;
    BufferStatus_t BufferStatus;

    do
    {
        BufferStatus = PreprocContextBufferPool->GetBuffer(Buffer, IdentifierEncoderPreprocessor,
                                                           UNSPECIFIED_SIZE, false, false,
                                                           GetBufferTimeOutMs);

        // Warning message every TimeBetweenMessageUs us
        if ((OS_GetTimeInMicroSeconds() - EntryTime) > TimeBetweenMessageUs)
        {
            EntryTime = OS_GetTimeInMicroSeconds();
            SE_WARNING("Stream 0x%p Preproc(%p)->PreprocContextBufferPool(%p)->GetBuffer still NoFreeBufferAvailable with %s\n",
                       Encoder.EncodeStream, this, PreprocContextBufferPool,
                       TestComponentState(ComponentRunning) ? "ComponentRunning" : "!ComponentRunning");
        }
    }
    while ((BufferNoFreeBufferAvailable == BufferStatus) && TestComponentState(ComponentRunning));

    if (BufferStatus != BufferNoError || *Buffer == NULL)
    {
        PREPROC_ERROR_RUNNING("Stream 0x%p Unable to get preproc context buffer (BufStatus %08X, Buffer %p)\n",
                              Encoder.EncodeStream, BufferStatus, *Buffer);
        *Buffer = NULL;
        return PreprocError;
    }

    return PreprocNoError;
}

PreprocStatus_t Preproc_Base_c::Output(Buffer_t   Buffer,
                                       bool       Marker)
{
    (void)Marker; // warning removal

    // Detach any input buffer now that the transform is complete
    Buffer_t InputBuffer;
    Buffer->ObtainAttachedBufferReference(InputBufferType, &InputBuffer);
    if (InputBuffer != NULL)
    {
        Buffer->DetachBuffer(InputBuffer);
    }

    RingStatus_t Status = OutputPort->Insert((uintptr_t) Buffer);
    if (Status != RingNoError)
    {
        SE_ERROR("Stream 0x%p Unable to insert buffer in Preproc Ring\n", Encoder.EncodeStream);
        Buffer->DecrementReferenceCount(IdentifierEncoderPreprocessor);
        return PreprocError;
    }
    else
    {
        Encoder.EncodeStream->EncodeStreamStatistics().BufferCountFromPreproc++;
    }

    return PreprocNoError;
}

/// This Function should only deal with common aspects regarding Input of a buffer
/// The implementation classes must deal with the actual input.
PreprocStatus_t Preproc_Base_c::Input(Buffer_t    Buffer)
{
    AssertComponentState(ComponentRunning);

    // Track all incoming buffers in statistics
    Encoder.EncodeStream->EncodeStreamStatistics().BufferCountToPreproc++;

    // Get a New 'PreprocFrameBuffer'
    PreprocStatus_t Status = GetNewBuffer(&PreprocFrameBuffer);
    if (Status != PreprocNoError)
    {
        PREPROC_ERROR_RUNNING("Stream 0x%p Unable to get new buffer, Status = %u\n", Encoder.EncodeStream, Status);
        return PreprocError;
    }

    StampFrame(Buffer, PreprocFrameBuffer);

    // Retrieve input metadata
    Buffer->ObtainMetaDataReference(InputMetaDataBufferType, (void **)(&InputMetaDataDescriptor));
    SE_ASSERT(InputMetaDataDescriptor != NULL);

    // Dump input metadata
    DumpInputMetadata(InputMetaDataDescriptor);

    // Check native time format
    bool format_is_ok = TimeStamp_c::IsNativeTimeFormatCoherent(InputMetaDataDescriptor->native_time_format,
                                                                InputMetaDataDescriptor->native_time);
    if (!format_is_ok)
    {
        SE_ERROR("Stream 0x%p Bad native time format, Status = %u\n", Encoder.EncodeStream, Status);
        PreprocFrameBuffer->DecrementReferenceCount(IdentifierEncoderPreprocessor);
        return PreprocError;
    }

    // Detect Buffer Discontinuity
    Status = DetectBufferDiscontinuity(Buffer);
    if (Status != PreprocNoError)
    {
        SE_ERROR("Stream 0x%p Discontinuity detection failed, Status = %u\n", Encoder.EncodeStream, Status);
        PreprocFrameBuffer->DecrementReferenceCount(IdentifierEncoderPreprocessor);
        return PreprocError;
    }

    // Retrieve encode coordinator metadata
    Buffer->ObtainMetaDataReference(EncodeCoordinatorMetaDataBufferType, (void **)(&EncodeCoordinatorMetaDataDescriptor));
    SE_ASSERT(EncodeCoordinatorMetaDataDescriptor != NULL);

    // In case of NRT mode, this metadata struct is filled by the EncodeCoordinator
    // Otherwise, it may be not initialized: we set it to native_time for further use in preproc
    if (Encoder.Encode->IsModeNRT() == false)
    {
        memset(EncodeCoordinatorMetaDataDescriptor, 0, sizeof(__stm_se_encode_coordinator_metadata_t));
        EncodeCoordinatorMetaDataDescriptor->encoded_time = InputMetaDataDescriptor->native_time;
        EncodeCoordinatorMetaDataDescriptor->encoded_time_format = InputMetaDataDescriptor->native_time_format;
    }
    else
    {
        // Dump encode coordinator metadata
        DumpEncodeCoordinatorMetadata(EncodeCoordinatorMetaDataDescriptor);

        // Check encoded time format
        format_is_ok = TimeStamp_c::IsNativeTimeFormatCoherent(EncodeCoordinatorMetaDataDescriptor->encoded_time_format,
                                                               EncodeCoordinatorMetaDataDescriptor->encoded_time);
        if (!format_is_ok)
        {
            SE_ERROR("Stream 0x%p Bad encoded time format, Status = %u\n", Encoder.EncodeStream, Status);
            PreprocFrameBuffer->DecrementReferenceCount(IdentifierEncoderPreprocessor);
            return PreprocError;
        }
    }

    // Retrieve preproc metadata
    PreprocFrameBuffer->ObtainMetaDataReference(OutputMetaDataBufferType, (void **)(&PreprocFullMetaDataDescriptor));
    SE_ASSERT(PreprocFullMetaDataDescriptor != NULL);

    // Initialize preproc metadata
    PreprocMetaDataDescriptor = &PreprocFullMetaDataDescriptor->uncompressed_frame_metadata;
    memcpy(PreprocMetaDataDescriptor, InputMetaDataDescriptor, sizeof(stm_se_uncompressed_frame_metadata_t));
    memcpy(&PreprocFullMetaDataDescriptor->encode_coordinator_metadata, EncodeCoordinatorMetaDataDescriptor, sizeof(__stm_se_encode_coordinator_metadata_t));

    // Attach the Input buffer to the PreprocFrameBuffer so that it is kept until Preprocessing is complete.
    PreprocFrameBuffer->AttachBuffer(Buffer);

    //Only deal with frame stat for relevant buffers (ignore pure discontinuity)
    //NB: InputBufferSize filled by previous DetectBufferDiscontinuity() call
    if (InputBufferSize != 0)
    {
        Encoder.EncodeStream->EncodeStreamStatistics().FrameCountToPreproc++;
    }

    return PreprocNoError;
}

PreprocStatus_t Preproc_Base_c::Flush()
{
    return PreprocNoError;
}

PreprocStatus_t Preproc_Base_c::StampFrame(Buffer_t Buffer, Buffer_t PreprocFrameBuffer)
{
    EncoderSequenceNumber_t  *EncoderSequenceNumberStructure;
    EncoderSequenceNumber_t  *PreprocSequenceNumberStructure;

    Buffer->ObtainMetaDataReference(MetaDataSequenceNumberType, (void **)(&EncoderSequenceNumberStructure));
    SE_ASSERT(EncoderSequenceNumberStructure != NULL);

    PreprocFrameBuffer->ObtainMetaDataReference(MetaDataSequenceNumberType, (void **)(&PreprocSequenceNumberStructure));
    SE_ASSERT(PreprocSequenceNumberStructure != NULL);

    PreprocSequenceNumberStructure->StreamUniqueIdentifier = (unsigned int)EncoderSequenceNumberStructure->StreamUniqueIdentifier;
    PreprocSequenceNumberStructure->StreamTypeIdentifier = EncoderSequenceNumberStructure->StreamTypeIdentifier;
    PreprocSequenceNumberStructure->FrameCounter = EncoderSequenceNumberStructure->FrameCounter;
    PreprocSequenceNumberStructure->PTS = EncoderSequenceNumberStructure->PTS;

    SE_DEBUG(group_se_pipeline, "Stream 0x%x - %d - #%lld PTS=%lld PPB=%llu SeqBuffer 0x%p PreprocFrameBuffer 0x%p\n",
             PreprocSequenceNumberStructure->StreamUniqueIdentifier,
             PreprocSequenceNumberStructure->StreamTypeIdentifier,
             PreprocSequenceNumberStructure->FrameCounter,
             PreprocSequenceNumberStructure->PTS,
             OS_GetTimeInMicroSeconds(),
             Buffer,
             PreprocFrameBuffer
            );

    return PreprocNoError;
}


PreprocStatus_t Preproc_Base_c::RejectGetBufferCalls()
{
    SetComponentState(ComponentHalted);

    return PreprocNoError;
}

PreprocStatus_t Preproc_Base_c::AcceptGetBufferCalls()
{
    SetComponentState(ComponentRunning);

    return PreprocNoError;
}

PreprocStatus_t Preproc_Base_c::GetControl(stm_se_ctrl_t    Control,
                                           void            *Data)
{
    (void)Data; // warning removal
    switch (Control)
    {
    default:
        // Cannot trace this as error because it can be a valid control for another object
        SE_DEBUG(GetGroupTrace(), "Stream 0x%p Not match preproc control %u\n", Encoder.EncodeStream, Control);
        return PreprocControlNotMatch;
    }
}

PreprocStatus_t Preproc_Base_c::GetCompoundControl(stm_se_ctrl_t    Control,
                                                   void            *Data)
{
    (void)Data; // warning removal
    switch (Control)
    {
    default:
        // Cannot trace this as error because it can be a valid control for another object
        SE_DEBUG(GetGroupTrace(), "Stream 0x%p Not match preproc control %u\n", Encoder.EncodeStream, Control);
        return PreprocControlNotMatch;
    }
}

PreprocStatus_t Preproc_Base_c::SetControl(stm_se_ctrl_t    Control,
                                           const void      *Data)
{
    (void)Data; // warning removal
    switch (Control)
    {
    default:
        // Cannot trace this as error because it can be a valid control for another object
        SE_DEBUG(GetGroupTrace(), "Stream 0x%p Not match preproc control %u\n", Encoder.EncodeStream, Control);
        return PreprocControlNotMatch;
    }
}

PreprocStatus_t Preproc_Base_c::SetCompoundControl(stm_se_ctrl_t    Control,
                                                   const void      *Data)
{
    (void)Data; // warning removal
    switch (Control)
    {
    default:
        // Cannot trace this as error because it can be a valid control for another object
        SE_DEBUG(GetGroupTrace(), "Stream 0x%p Not match preproc control %u\n", Encoder.EncodeStream, Control);
        return PreprocControlNotMatch;
    }
}

PreprocStatus_t Preproc_Base_c::CheckDiscontinuity(stm_se_discontinuity_t   Discontinuity)
{
    // Check for errors
    if (Discontinuity & STM_SE_DISCONTINUITY_OPEN_GOP_REQUEST)
    {
        return EncoderNotSupported;
    }

    if (Discontinuity & STM_SE_DISCONTINUITY_FRAME_SKIPPED)
    {
        return EncoderNotSupported;
    }

    if ((Discontinuity != STM_SE_DISCONTINUITY_CONTINUOUS) &&
        !(Discontinuity & STM_SE_DISCONTINUITY_DISCONTINUOUS) &&
        !(Discontinuity & STM_SE_DISCONTINUITY_EOS) &&
        !(Discontinuity & STM_SE_DISCONTINUITY_CLOSED_GOP_REQUEST) &&
        !(Discontinuity & STM_SE_DISCONTINUITY_MUTE) &&
        !(Discontinuity & STM_SE_DISCONTINUITY_FADEOUT) &&
        !(Discontinuity & STM_SE_DISCONTINUITY_FADEIN))
    {
        return EncoderNotSupported;
    }

    // GOP not supported in audio
    // PreprocBase does not have indication of audio/video, thus unable to check unless at leaf class
    return EncoderNoError;
}

PreprocStatus_t Preproc_Base_c::InjectDiscontinuity(stm_se_discontinuity_t  Discontinuity)
{
    PreprocStatus_t    Status;

    Encoder.EncodeStream->EncodeStreamStatistics().DiscontinuityBufferCountToPreproc++;

    Status = CheckDiscontinuity(Discontinuity);

    if (Status != EncoderNoError)
    {
        return Status;
    }

    return EncoderNoError;
}

PreprocStatus_t Preproc_Base_c::DetectBufferDiscontinuity(Buffer_t   Buffer)
{
    PreprocStatus_t                        Status;
    unsigned int                           BufferAddr;

    // Obtain MetaData from Input.
    // Retrieve input metadata
    // This operation is extra

    stm_se_uncompressed_frame_metadata_t *LocalInputMetaDataDescriptor;
    Buffer->ObtainMetaDataReference(InputMetaDataBufferType, (void **)(&LocalInputMetaDataDescriptor));
    SE_ASSERT(LocalInputMetaDataDescriptor != NULL);


    // In case of discontinuity, MetaData are associated to a null size buffer
    Buffer->ObtainDataReference(NULL, &InputBufferSize , (void **)(&BufferAddr) , PhysicalAddress);

    //  Check correctness of physicall address if buffer size is not zero
    if ((InputBufferSize != 0)  && (BufferAddr == NULL))
    {
        SE_ERROR("Stream 0x%p Unable to obtain input physical address, InputBufferSize = %u\n", Encoder.EncodeStream, InputBufferSize);
        return PreprocError;
    }

    // Check the discontinuity
    Status = CheckDiscontinuity(LocalInputMetaDataDescriptor->discontinuity);
    if (Status != EncoderNoError)
    {
        SE_ERROR("Stream 0x%p Check discontinuity failed, Status %u\n", Encoder.EncodeStream, Status);
        return Status;
    }

    // Check for invalid discontinuity cases and warn
    // Invalid Case 1: No discontinuity metadata + zero datasize
    if ((LocalInputMetaDataDescriptor->discontinuity == STM_SE_DISCONTINUITY_CONTINUOUS) && (InputBufferSize == 0))
    {
        SE_ERROR("Stream 0x%p Invalid input buffer (null buffer with no discontinuity)\n", Encoder.EncodeStream);
        return PreprocError;
    }

    // Store and mask out the discontinuity
    PreprocDiscontinuity                        = LocalInputMetaDataDescriptor->discontinuity;
    LocalInputMetaDataDescriptor->discontinuity = STM_SE_DISCONTINUITY_CONTINUOUS;

    return PreprocNoError;
}

PreprocStatus_t Preproc_Base_c::GenerateBufferDiscontinuity(stm_se_discontinuity_t Discontinuity)
{
    Buffer_t                               Buffer;
    stm_se_uncompressed_frame_metadata_t *LocalPreprocMetaDataDescriptor;
    __stm_se_frame_metadata_t             *LocalPreprocFullMetaDataDescriptor;

    // Get a new discontinuity buffer
    PreprocStatus_t Status = GetNewBuffer(&Buffer, true);
    if (Status != PreprocNoError)
    {
        PREPROC_ERROR_RUNNING("Stream 0x%p Unable to get new buffer, Status = %u\n", Encoder.EncodeStream, Status);
        return PreprocError;
    }

    // Retrieve preproc metadata
    Buffer->ObtainMetaDataReference(OutputMetaDataBufferType, (void **)(&LocalPreprocFullMetaDataDescriptor));
    SE_ASSERT(LocalPreprocFullMetaDataDescriptor != NULL);

    LocalPreprocMetaDataDescriptor = &LocalPreprocFullMetaDataDescriptor->uncompressed_frame_metadata;
    // Initialize preproc metadata with discontinuity EOS
    memset(LocalPreprocFullMetaDataDescriptor, '\0', sizeof(__stm_se_frame_metadata_t));
    LocalPreprocMetaDataDescriptor->discontinuity = Discontinuity;

    Encoder.EncodeStream->EncodeStreamStatistics().DiscontinuityBufferCountFromPreproc++;

    // Output frame into ring
    return Output(Buffer);
}

//      Low power methods (base)

PreprocStatus_t  Preproc_Base_c::LowPowerEnter()
{
    // destroy ICS maps
    if (PreprocFrameMemoryDevice != NULL)
    {
        AllocatorRemoveMapEx(PreprocFrameMemoryDevice->UnderlyingDevice);
    }

    return PreprocNoError;
}

PreprocStatus_t  Preproc_Base_c::LowPowerExit()
{
    // recreate ICS maps
    if (PreprocFrameMemoryDevice != NULL)
    {
        AllocatorCreateMapEx(PreprocFrameMemoryDevice->UnderlyingDevice);
    }

    return PreprocNoError;
}

void Preproc_Base_c::DumpInputMetadata(stm_se_uncompressed_frame_metadata_t *Metadata)
{
    int i = 0;

    if (SE_IS_EXTRAVERB_ON(GetGroupTrace()))
    {
        SE_EXTRAVERB(GetGroupTrace(), "Stream 0x%p [INPUT PREPROC METADATA]\n", Encoder.EncodeStream);
        SE_EXTRAVERB(GetGroupTrace(), "Stream 0x%p |-system_time = %llu\n", Encoder.EncodeStream, Metadata->system_time);
        SE_EXTRAVERB(GetGroupTrace(), "Stream 0x%p |-native_time_format = %u (%s)\n", Encoder.EncodeStream, Metadata->native_time_format, StringifyTimeFormat(Metadata->native_time_format));
        SE_EXTRAVERB(GetGroupTrace(), "Stream 0x%p |-native_time = %llu\n", Encoder.EncodeStream, Metadata->native_time);
        SE_EXTRAVERB(GetGroupTrace(), "Stream 0x%p |-user_data_size = %u bytes\n", Encoder.EncodeStream, Metadata->user_data_size);
        SE_EXTRAVERB(GetGroupTrace(), "Stream 0x%p |-user_data_buffer_address = 0x%p\n", Encoder.EncodeStream, Metadata->user_data_buffer_address);
        SE_EXTRAVERB(GetGroupTrace(), "Stream 0x%p |-discontinuity = %u (%s)\n", Encoder.EncodeStream, Metadata->discontinuity, StringifyDiscontinuity(Metadata->discontinuity));
        SE_EXTRAVERB(GetGroupTrace(), "Stream 0x%p |-media = %u (%s)\n", Encoder.EncodeStream, Metadata->media, StringifyEncodeMedia(Metadata->media));
        if (Metadata->media == STM_SE_ENCODE_STREAM_MEDIA_AUDIO)
        {
            SE_EXTRAVERB(GetGroupTrace(), "Stream 0x%p |-[audio]\n", Encoder.EncodeStream);
            SE_EXTRAVERB(GetGroupTrace(), "Stream 0x%p   |-[core_format]\n", Encoder.EncodeStream);
            SE_EXTRAVERB(GetGroupTrace(), "Stream 0x%p   | |-sample_rate = %u Hz\n", Encoder.EncodeStream, Metadata->audio.core_format.sample_rate);
            SE_EXTRAVERB(GetGroupTrace(), "Stream 0x%p   | |-[channel_placement]\n", Encoder.EncodeStream);
            SE_EXTRAVERB(GetGroupTrace(), "Stream 0x%p   |   |-channel_count = %u\n", Encoder.EncodeStream, Metadata->audio.core_format.channel_placement.channel_count);
            for (i = 0 ; i < Metadata->audio.core_format.channel_placement.channel_count ; i++)
            {
                SE_EXTRAVERB(GetGroupTrace(), "Stream 0x%p   |   |-chan[%d] = %u\n", Encoder.EncodeStream, i, Metadata->audio.core_format.channel_placement.chan[i]);
            }
            SE_EXTRAVERB(GetGroupTrace(), "Stream 0x%p   |\n", Encoder.EncodeStream);
            SE_EXTRAVERB(GetGroupTrace(), "Stream 0x%p   |-sample_format = %u (%s)\n", Encoder.EncodeStream, Metadata->audio.sample_format, StringifyPcmFormat(Metadata->audio.sample_format));
            SE_EXTRAVERB(GetGroupTrace(), "Stream 0x%p   |-program_level = %d\n", Encoder.EncodeStream, Metadata->audio.program_level);
            SE_EXTRAVERB(GetGroupTrace(), "Stream 0x%p   |-emphasis = %u (%s)\n", Encoder.EncodeStream, Metadata->audio.emphasis, StringifyEmphasisType(Metadata->audio.emphasis));
        }
        else if (Metadata->media == STM_SE_ENCODE_STREAM_MEDIA_VIDEO)
        {
            SE_EXTRAVERB(GetGroupTrace(), "Stream 0x%p |-[video]\n", Encoder.EncodeStream);
            SE_EXTRAVERB(GetGroupTrace(), "Stream 0x%p   |-[video_parameters]\n", Encoder.EncodeStream);
            SE_EXTRAVERB(GetGroupTrace(), "Stream 0x%p   | |-width = %u pixels\n", Encoder.EncodeStream, Metadata->video.video_parameters.width);
            SE_EXTRAVERB(GetGroupTrace(), "Stream 0x%p   | |-height = %u pixels\n", Encoder.EncodeStream, Metadata->video.video_parameters.height);
            SE_EXTRAVERB(GetGroupTrace(), "Stream 0x%p   | |-aspect_ratio = %u (%s)\n", Encoder.EncodeStream, Metadata->video.video_parameters.aspect_ratio,
                         StringifyAspectRatio(Metadata->video.video_parameters.aspect_ratio));
            SE_EXTRAVERB(GetGroupTrace(), "Stream 0x%p   | |-scan_type = %u (%s)\n", Encoder.EncodeStream, Metadata->video.video_parameters.scan_type,
                         StringifyScanType(Metadata->video.video_parameters.scan_type));
            SE_EXTRAVERB(GetGroupTrace(), "Stream 0x%p   | |-pixel_aspect_ratio = %u/%u\n", Encoder.EncodeStream, Metadata->video.video_parameters.pixel_aspect_ratio_numerator,
                         Metadata->video.video_parameters.pixel_aspect_ratio_denominator);
            SE_EXTRAVERB(GetGroupTrace(), "Stream 0x%p   | |-format_3d = %u (%s)\n", Encoder.EncodeStream, Metadata->video.video_parameters.format_3d,
                         StringifyFormat3d(Metadata->video.video_parameters.format_3d));
            SE_EXTRAVERB(GetGroupTrace(), "Stream 0x%p   | |-left_right_format = %u\n", Encoder.EncodeStream, Metadata->video.video_parameters.left_right_format);
            SE_EXTRAVERB(GetGroupTrace(), "Stream 0x%p   | |-colorspace = %u (%s)\n", Encoder.EncodeStream, Metadata->video.video_parameters.colorspace,
                         StringifyColorspace(Metadata->video.video_parameters.colorspace));
            SE_EXTRAVERB(GetGroupTrace(), "Stream 0x%p   | |-frame_rate = %u fps\n", Encoder.EncodeStream, Metadata->video.video_parameters.frame_rate);
            SE_EXTRAVERB(GetGroupTrace(), "Stream 0x%p   |\n", Encoder.EncodeStream);
            SE_EXTRAVERB(GetGroupTrace(), "Stream 0x%p   |-[window_of_interest]\n", Encoder.EncodeStream);
            SE_EXTRAVERB(GetGroupTrace(), "Stream 0x%p   | |-x = %u pixels\n", Encoder.EncodeStream, Metadata->video.window_of_interest.x);
            SE_EXTRAVERB(GetGroupTrace(), "Stream 0x%p   | |-y = %u pixels\n", Encoder.EncodeStream, Metadata->video.window_of_interest.y);
            SE_EXTRAVERB(GetGroupTrace(), "Stream 0x%p   | |-width = %u pixels\n", Encoder.EncodeStream, Metadata->video.window_of_interest.width);
            SE_EXTRAVERB(GetGroupTrace(), "Stream 0x%p   | |-height = %u pixels\n", Encoder.EncodeStream, Metadata->video.window_of_interest.height);
            SE_EXTRAVERB(GetGroupTrace(), "Stream 0x%p   |\n", Encoder.EncodeStream);
            SE_EXTRAVERB(GetGroupTrace(), "Stream 0x%p   |-frame_rate = %u/%u fps\n", Encoder.EncodeStream, Metadata->video.frame_rate.framerate_num, Metadata->video.frame_rate.framerate_den);
            SE_EXTRAVERB(GetGroupTrace(), "Stream 0x%p   |-pitch = %u bytes\n", Encoder.EncodeStream, Metadata->video.pitch);
            SE_EXTRAVERB(GetGroupTrace(), "Stream 0x%p   |-vertical_alignment = %u lines of pixels\n", Encoder.EncodeStream, Metadata->video.vertical_alignment);
            SE_EXTRAVERB(GetGroupTrace(), "Stream 0x%p   |-picture_type = %u (%s)\n", Encoder.EncodeStream, Metadata->video.picture_type, StringifyPictureType(Metadata->video.picture_type));
            SE_EXTRAVERB(GetGroupTrace(), "Stream 0x%p   |-surface_format = %u (%s)\n", Encoder.EncodeStream, Metadata->video.surface_format, StringifySurfaceFormat(Metadata->video.surface_format));
            SE_EXTRAVERB(GetGroupTrace(), "Stream 0x%p   |-top_field_first = %u\n", Encoder.EncodeStream, Metadata->video.top_field_first);
            SE_EXTRAVERB(GetGroupTrace(), "Stream 0x%p   |-picture_structure = %u (%s)\n", Encoder.EncodeStream, Metadata->video.picture_structure, StringifyPictureStructure(Metadata->video.picture_structure));
            for (i = 0; i < STM_SE_NUMBER_OF_CEH_INTERVALS; i++)
            {
                SE_EXTRAVERB(GetGroupTrace(), "Stream 0x%p   |-ceh_default_top[%d] = %u\n", Encoder.EncodeStream, i, Metadata->video.ceh_default_top[i]);
                SE_EXTRAVERB(GetGroupTrace(), "Stream 0x%p   |-ceh_bottom[%d] = %u\n", Encoder.EncodeStream, i, Metadata->video.ceh_bottom[i]);
            }
        }
        SE_EXTRAVERB(GetGroupTrace(), "Stream 0x%p\n", Encoder.EncodeStream);
    }
}

void Preproc_Base_c::DumpEncodeCoordinatorMetadata(__stm_se_encode_coordinator_metadata_t *Metadata)
{
    if (SE_IS_EXTRAVERB_ON(GetGroupTrace()))
    {
        SE_EXTRAVERB(GetGroupTrace(), "Stream 0x%p [ENCODE COORDINATOR METADATA]\n", Encoder.EncodeStream);
        SE_EXTRAVERB(GetGroupTrace(), "Stream 0x%p |-encoded_time_format = %u (%s)\n", Encoder.EncodeStream, Metadata->encoded_time_format, StringifyTimeFormat(Metadata->encoded_time_format));
        SE_EXTRAVERB(GetGroupTrace(), "Stream 0x%p |-encoded_time = %llu\n", Encoder.EncodeStream, Metadata->encoded_time);
        SE_EXTRAVERB(GetGroupTrace(), "Stream 0x%p\n", Encoder.EncodeStream);
    }
}
