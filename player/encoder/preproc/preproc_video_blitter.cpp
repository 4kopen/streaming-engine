/************************************************************************
Copyright (C) 2003-2014 STMicroelectronics. All Rights Reserved.

This file is part of the Streaming Engine.

Streaming Engine is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License version 2 as
published by the Free Software Foundation.

Streaming Engine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with Streaming Engine; see the file COPYING.  If not, write to the Free
Software Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307,
USA.

The Streaming Engine Library may alternatively be licensed under a
proprietary license from ST.
************************************************************************/

#include "preproc_video_blitter.h"
#include "st_relayfs_se.h"
#include "video_decoder_params.h"

#undef TRACE_TAG
#define TRACE_TAG "Preproc_Video_Blitter_c"

// modified from linux/err.h
#define BLITTER_ERR(x) ((x == NULL) || ((unsigned long)x >= (unsigned long)-4095))

#define MIN_BLITTER_RECT_WIDTH    8
#define MIN_BLITTER_RECT_HEIGHT   8

typedef enum blitter_buffer_flags_e
{
    BLITTER_BUFFER_INTERLACED = (1L << 0),
    BLITTER_BUFFER_TOP        = (1L << 1),
    BLITTER_BUFFER_BOTTOM     = (1L << 2)
} blitter_buffer_flags_t;

static inline const char *StringifyBlitterColorspace(stm_blitter_surface_colorspace_t aBlitterColorspace)
{
    switch (aBlitterColorspace)
    {
        ENTRY(STM_BLITTER_SCS_RGB);
        ENTRY(STM_BLITTER_SCS_RGB_VIDEORANGE);
        ENTRY(STM_BLITTER_SCS_BT601);
        ENTRY(STM_BLITTER_SCS_BT601_FULLRANGE);
        ENTRY(STM_BLITTER_SCS_BT709);
        ENTRY(STM_BLITTER_SCS_BT709_FULLRANGE);
        ENTRY(STM_BLITTER_SCS_BT2020);
        ENTRY(STM_BLITTER_SCS_BT2020_FULLRANGE);
    default: return "<unknown blitter colorspace>";
    }
}

static inline const char *StringifyBlitterSurfaceFormat(stm_blitter_surface_format_t aBlitterSurfaceFormat)
{
    switch (aBlitterSurfaceFormat)
    {
        ENTRY(STM_BLITTER_SF_INVALID);
        ENTRY(STM_BLITTER_SF_RGB565);
        ENTRY(STM_BLITTER_SF_RGB24);
        ENTRY(STM_BLITTER_SF_RGB32);
        ENTRY(STM_BLITTER_SF_ARGB1555);
        ENTRY(STM_BLITTER_SF_ARGB4444);
        ENTRY(STM_BLITTER_SF_ARGB8565);
        ENTRY(STM_BLITTER_SF_ARGB);
        ENTRY(STM_BLITTER_SF_BGRA);
        ENTRY(STM_BLITTER_SF_LUT8);
        ENTRY(STM_BLITTER_SF_LUT4);
        ENTRY(STM_BLITTER_SF_LUT2);
        ENTRY(STM_BLITTER_SF_LUT1);
        ENTRY(STM_BLITTER_SF_ALUT88);
        ENTRY(STM_BLITTER_SF_ALUT44);
        ENTRY(STM_BLITTER_SF_A8);
        ENTRY(STM_BLITTER_SF_A1);
        ENTRY(STM_BLITTER_SF_AVYU);
        ENTRY(STM_BLITTER_SF_VYU);
        ENTRY(STM_BLITTER_SF_YUYV);
        ENTRY(STM_BLITTER_SF_UYVY);
        ENTRY(STM_BLITTER_SF_YV12);
        ENTRY(STM_BLITTER_SF_I420);
        ENTRY(STM_BLITTER_SF_YV16);
        ENTRY(STM_BLITTER_SF_YV61);
        ENTRY(STM_BLITTER_SF_YCBCR444P);
        ENTRY(STM_BLITTER_SF_NV12);
        ENTRY(STM_BLITTER_SF_NV21);
        ENTRY(STM_BLITTER_SF_NV16);
        ENTRY(STM_BLITTER_SF_NV61);
        ENTRY(STM_BLITTER_SF_YCBCR420MB);
        ENTRY(STM_BLITTER_SF_YCBCR422MB);
        ENTRY(STM_BLITTER_SF_AlRGB8565);
        ENTRY(STM_BLITTER_SF_AlRGB);
        ENTRY(STM_BLITTER_SF_BGRAl);
        ENTRY(STM_BLITTER_SF_AlLUT88);
        ENTRY(STM_BLITTER_SF_Al8);
        ENTRY(STM_BLITTER_SF_AlVYU);
        ENTRY(STM_BLITTER_SF_BGR24);
        ENTRY(STM_BLITTER_SF_RLD_BD);
        ENTRY(STM_BLITTER_SF_RLD_H2);
        ENTRY(STM_BLITTER_SF_RLD_H8);
        ENTRY(STM_BLITTER_SF_NV24);
        ENTRY(STM_BLITTER_SF_ABGR);
        ENTRY(STM_BLITTER_SF_NV12_10B);
        ENTRY(STM_BLITTER_SF_NV21_10B);
        ENTRY(STM_BLITTER_SF_NV16_10B);
        ENTRY(STM_BLITTER_SF_NV61_10B);
        ENTRY(STM_BLITTER_SF_COUNT);
    default: return "<unknown blitter surface format>";
    }
}

static const PreprocVideoCaps_t PreprocCaps =
{
    {ENCODE_WIDTH_MAX, ENCODE_HEIGHT_MAX},                     // MaxResolution
    {ENCODE_FRAMERATE_MAX, STM_SE_PLAY_FRAME_RATE_MULTIPLIER}, // MaxFramerate
    true,                                                      // DEISupport
    true,                                                      // NRSupport
    ((1 << SURFACE_FORMAT_VIDEO_420_PLANAR)         |          // VideoFormatCaps
    (1 << SURFACE_FORMAT_VIDEO_420_PLANAR_ALIGNED) |
    (1 << SURFACE_FORMAT_VIDEO_422_PLANAR)         |
    (1 << SURFACE_FORMAT_VIDEO_422_RASTER)         |
    (1 << SURFACE_FORMAT_VIDEO_422_YUYV)           |
    (1 << SURFACE_FORMAT_VIDEO_420_RASTER2B)       |
    (1 << SURFACE_FORMAT_VIDEO_420_RASTER2B_10B)   |
    (1 << SURFACE_FORMAT_VIDEO_420_MACROBLOCK)     |
    (1 << SURFACE_FORMAT_VIDEO_420_PAIRED_MACROBLOCK))
};

Preproc_Video_Blitter_c::Preproc_Video_Blitter_c()
    : blitter(NULL)
    , src(NULL)
    , dst(NULL)
    , src_bitmap()
    , dst_bitmap()
    , PreprocFrameBuffer2(NULL)
    , PreprocFrameBufferIntermediate(NULL)
    , mSceneCutContext()
    , mRelayfsIndex(0)
{
    mRelayfsIndex = st_relayfs_getindex_fortype_se(ST_RELAY_TYPE_ENCODER_VIDEO_PREPROC_INPUT);

    // TODO(pht) move FinalizeInit to a factory method
    InitializationStatus = FinalizeInit();
}

Preproc_Video_Blitter_c::~Preproc_Video_Blitter_c()
{
    Halt();

    st_relayfs_freeindex_fortype_se(ST_RELAY_TYPE_ENCODER_VIDEO_PREPROC_INPUT, mRelayfsIndex);
}

PreprocStatus_t Preproc_Video_Blitter_c::GetBlitSurfaceFormat(surface_format_t      Format,
                                                              stm_blitter_surface_format_t    *BlitFormat,
                                                              uint32_t *VAlign)
{
    *BlitFormat = STM_BLITTER_SF_INVALID;
    *VAlign = 1; // Vertical alignment
    // Vertical alignment derived from DecodeBufferManager_Base_c::FillOutComponentStructure

    switch (Format)
    {
    case SURFACE_FORMAT_VIDEO_420_PLANAR:
    case SURFACE_FORMAT_VIDEO_420_PLANAR_ALIGNED:
        *BlitFormat = STM_BLITTER_SF_I420; // U followed by V plane
        *VAlign     = 16;
        break;

    case SURFACE_FORMAT_VIDEO_422_PLANAR:
        *BlitFormat = STM_BLITTER_SF_YV61;
        *VAlign     = 16;
        break;

    case SURFACE_FORMAT_VIDEO_422_RASTER:
        *BlitFormat = STM_BLITTER_SF_UYVY;
        *VAlign     = 1;
        break;

    case SURFACE_FORMAT_VIDEO_422_YUYV:
        *BlitFormat = STM_BLITTER_SF_YUYV;
        *VAlign     = 32; // shouldn't this be 1 instead of 32
        break;

    case SURFACE_FORMAT_VIDEO_420_RASTER2B:
        *BlitFormat = STM_BLITTER_SF_NV12;
        *VAlign     = 32;
        break;

    case SURFACE_FORMAT_VIDEO_420_RASTER2B_10B:
        *BlitFormat = STM_BLITTER_SF_NV12_10B;
        *VAlign     = 32;
        break;

    case SURFACE_FORMAT_VIDEO_420_PAIRED_MACROBLOCK: // paired MB width
    case SURFACE_FORMAT_VIDEO_420_MACROBLOCK:
        *BlitFormat = STM_BLITTER_SF_YCBCR420MB;
        *VAlign     = 32;
        break;

    case SURFACE_FORMAT_VIDEO_8888_ARGB:

    //  *BlitFormat = STM_BLITTER_SF_ARGB;
    //  *VAlign        = 32;
    //break;
    case SURFACE_FORMAT_VIDEO_888_RGB:

    //  *BlitFormat = STM_BLITTER_SF_RGB24;
    //  *VAlign        = 32;
    //break;
    case SURFACE_FORMAT_VIDEO_565_RGB:

    //  *BlitFormat = STM_BLITTER_SF_RGB565;
    //  *VAlign        = 32;
    //break;
    case SURFACE_FORMAT_UNKNOWN:
    case SURFACE_FORMAT_MARKER_FRAME:
    case SURFACE_FORMAT_AUDIO:
    default:
        SE_ERROR("Stream 0x%p Unsupported video surface format 0x%08x\n", Encoder.EncodeStream, Format);
        return PreprocError;
    }

    return PreprocNoError;
}

PreprocStatus_t Preproc_Video_Blitter_c::GetBufferOffsetSize(stm_blitter_surface_format_t   BlitFormat,
                                                             stm_blitter_dimension_t *AlignedSize, stm_blitter_surface_address_t *BufAddr, unsigned long *BufSize)
{
    uint32_t  ComponentSize;
    ComponentSize = AlignedSize->w * AlignedSize->h; //AlignedSize->w is initialized with pitch
    *BufSize      = ComponentSize;

    // 3 planes format
    // 444 STM_BLITTER_SF_YCBCR444P
    // 422 STM_BLITTER_SF_YV16 STM_BLITTER_SF_YV61
    // 420 STM_BLITTER_SF_YV12 STM_BLITTER_SF_I420
    // 2 planes format
    // 422 STM_BLITTER_SF_NV16 STM_BLITTER_SF_NV61
    // 420 STM_BLITTER_SF_NV12 STM_BLITTER_SF_NV21
    // 420 10 bits STM_BLITTER_SF_NV12_10B
    // 422 MB STM_BLITTER_SF_YCBCR422MB
    // 420 MB STM_BLITTER_SF_YCBCR420MB
    // pitch not considered yet.

    switch (BlitFormat)
    {
    case STM_BLITTER_SF_YCBCR444P:
        BufAddr->cb_offset = ComponentSize;
        BufAddr->cr_offset = ComponentSize * 2;
        *BufSize           = ComponentSize * 3;
        break;

    case STM_BLITTER_SF_YV16:
        BufAddr->cr_offset = ComponentSize;
        BufAddr->cb_offset = ComponentSize + ComponentSize / 2;
        *BufSize           = ComponentSize * 2;
        break;

    case STM_BLITTER_SF_YV61:
        BufAddr->cb_offset = ComponentSize;
        BufAddr->cr_offset = ComponentSize + ComponentSize / 2;
        *BufSize           = ComponentSize * 2;
        break;

    case STM_BLITTER_SF_YV12:
        BufAddr->cr_offset = ComponentSize;
        BufAddr->cb_offset = ComponentSize + ComponentSize / 4;
        *BufSize           = ComponentSize * 3 / 2;
        break;

    case STM_BLITTER_SF_I420:
        BufAddr->cb_offset = ComponentSize;
        BufAddr->cr_offset = ComponentSize + ComponentSize / 4;
        *BufSize           = ComponentSize * 3 / 2;
        break;

    case STM_BLITTER_SF_NV16:
    case STM_BLITTER_SF_NV61:
        BufAddr->cbcr_offset = ComponentSize;
        *BufSize             = ComponentSize * 2;
        break;

    case STM_BLITTER_SF_NV12:
    case STM_BLITTER_SF_NV21:
        BufAddr->cbcr_offset = ComponentSize;
        *BufSize             = ComponentSize * 3 / 2;
        break;

    case STM_BLITTER_SF_NV12_10B:
        BufAddr->cbcr_offset = ComponentSize;
        *BufSize             = ComponentSize * 3 / 2;
        AlignedSize->w       = AlignedSize->w * 8 / 10;
        break;

    case STM_BLITTER_SF_YCBCR422MB:
        BufAddr->cbcr_offset = ComponentSize;
        *BufSize             = ComponentSize * 2;
        break;

    case STM_BLITTER_SF_YCBCR420MB:
        BufAddr->cbcr_offset = ComponentSize;
        *BufSize             = ComponentSize * 3 / 2;
        break;

    // These formats have alpha: is it going to be supported?
    case STM_BLITTER_SF_ARGB:
        *BufSize             = ComponentSize;
        AlignedSize->w       /= 4;
        break;

    case STM_BLITTER_SF_RGB24:
        *BufSize             = ComponentSize;
        AlignedSize->w       /= 3;
        break;

    case STM_BLITTER_SF_YUYV:
    case STM_BLITTER_SF_UYVY:
    case STM_BLITTER_SF_RGB565:
        *BufSize             = ComponentSize;
        AlignedSize->w       /= 2;
        break;

    default:
        SE_ERROR("Stream 0x%p Blitting surface format 0x%x not supported\n", Encoder.EncodeStream, src_bitmap.format);
        return PreprocError;
    }

    return PreprocNoError;
}

stm_blitter_surface_colorspace_t Preproc_Video_Blitter_c::GetBlitterSurfaceColorspace(stm_se_colorspace_t InputColorspace, bool VideoFullRange)
{
    stm_blitter_surface_colorspace_t BlitterSurfaceColorspace;
    // Select the src/dest blitter surface colorspace for Full Range or Video Range, based on VideoFullRange
    // Same colorspace(and video/full range) is chosen at blitter src and dest, so no colorspace conversion is done
    switch (InputColorspace)
    {
    case STM_SE_COLORSPACE_SRGB:
        BlitterSurfaceColorspace = VideoFullRange ? STM_BLITTER_SCS_RGB : STM_BLITTER_SCS_RGB_VIDEORANGE;
        break;

    case STM_SE_COLORSPACE_SMPTE170M:
    // fallthrough
    case STM_SE_COLORSPACE_BT470_SYSTEM_M:
    // fallthrough
    case STM_SE_COLORSPACE_BT470_SYSTEM_BG:
        BlitterSurfaceColorspace = VideoFullRange ? STM_BLITTER_SCS_BT601_FULLRANGE : STM_BLITTER_SCS_BT601;
        break;

    case STM_SE_COLORSPACE_SMPTE240M:
    //fallthrough
    case STM_SE_COLORSPACE_BT709:
        BlitterSurfaceColorspace = VideoFullRange ? STM_BLITTER_SCS_BT709_FULLRANGE : STM_BLITTER_SCS_BT709;
        break;

    case STM_SE_COLORSPACE_BT2020:
        BlitterSurfaceColorspace = VideoFullRange ?  STM_BLITTER_SCS_BT2020_FULLRANGE : STM_BLITTER_SCS_BT2020;
        break;

    case STM_SE_COLORSPACE_UNSPECIFIED:
    // STM_SE_COLORSPACE_UNSPECIFIED is handled separately by the caller, so this case should not happen
    // fallthrough
    default:
        SE_ERROR("Stream 0x%p Invalid ColorSpace\n", Encoder.EncodeStream);
        BlitterSurfaceColorspace = STM_BLITTER_SCS_BT709;
    }

    return BlitterSurfaceColorspace;
}

stm_se_colorspace_t Preproc_Video_Blitter_c::GetDefaultOutputColorspace(stm_se_colorspace_t InputColorspace)
{
    stm_se_colorspace_t OutputColorspace;
    // If src Colorspace is BT2020, destination is set as BT709
    if (InputColorspace == STM_SE_COLORSPACE_BT2020)
    {
        OutputColorspace = STM_SE_COLORSPACE_BT709;
    }
    else
    {
        OutputColorspace = InputColorspace;
    }
    return OutputColorspace;
}

PreprocStatus_t Preproc_Video_Blitter_c::PrepareSrcBlitSurface(void *InputBufferAddrPhys, uint32_t BufferFlag)
{
    PreprocStatus_t Status;
    int blitStatus;
    uint32_t vAlign;
    stm_blitter_serial_t serial;
    void *PreprocBufferAddrPhys2;

    // Get surface format
    Status = GetBlitSurfaceFormat(PreprocMetaDataDescriptor->video.surface_format, &src_bitmap.format, &vAlign);
    if (Status != PreprocNoError)
    {
        SE_ERROR("Stream 0x%p Video surface format %d not supported\n", Encoder.EncodeStream, PreprocMetaDataDescriptor->video.surface_format);
        return Status;
    }

    // Additional vertical alignment check to ensure good value is used, otherwise SE alignment values are used
    if (PreprocMetaDataDescriptor->video.vertical_alignment >= 1)
    {
        vAlign = PreprocMetaDataDescriptor->video.vertical_alignment;
    }

    // Get base address
    src_bitmap.buffer_address.base = (unsigned long)InputBufferAddrPhys;
    src_bitmap.aligned_size.w      = PreprocMetaDataDescriptor->video.pitch; //initialize to pitch, will be changed by GetBufferOffsetSize
    src_bitmap.aligned_size.h      = ALIGN_UP(PreprocMetaDataDescriptor->video.video_parameters.height, vAlign);
    Status = GetBufferOffsetSize(src_bitmap.format, &src_bitmap.aligned_size, &src_bitmap.buffer_address, &src_bitmap.buffer_size);
    if (Status != PreprocNoError)
    {
        SE_ERROR("Stream 0x%p Failed in GetBufferOffsetSize\n", Encoder.EncodeStream);
        return Status;
    }

    src_bitmap.pitch           = PreprocMetaDataDescriptor->video.pitch;

    if (PreprocMetaDataDescriptor->video.video_parameters.colorspace == STM_SE_COLORSPACE_UNSPECIFIED)
    {
        src_bitmap.colorspace = (src_bitmap.format & STM_BLITTER_SF_YCBCR) ?
                                (PreprocMetaDataDescriptor->video.video_parameters.width > SD_WIDTH ?
                                 STM_BLITTER_SCS_BT709 : STM_BLITTER_SCS_BT601) : STM_BLITTER_SCS_RGB;
    }
    else
    {
        src_bitmap.colorspace = GetBlitterSurfaceColorspace(PreprocMetaDataDescriptor->video.video_parameters.colorspace, PreprocMetaDataDescriptor->video.video_parameters.video_full_range);
    }

    src_bitmap.size.w          = src_bitmap.aligned_size.w;
    src_bitmap.size.h          = src_bitmap.aligned_size.h;
    if (PreprocCtrl.EnableCropping == STM_SE_CTRL_VALUE_APPLY)
    {
        src_bitmap.rect.position.x = PreprocMetaDataDescriptor->video.window_of_interest.x;
        src_bitmap.rect.position.y = PreprocMetaDataDescriptor->video.window_of_interest.y;
        src_bitmap.rect.size.w     = PreprocMetaDataDescriptor->video.window_of_interest.width;
        src_bitmap.rect.size.h     = PreprocMetaDataDescriptor->video.window_of_interest.height;
    }
    else
    {
        src_bitmap.rect.position.x = 0;
        src_bitmap.rect.position.y = 0;
        src_bitmap.rect.size.w     = PreprocMetaDataDescriptor->video.video_parameters.width;
        src_bitmap.rect.size.h     = PreprocMetaDataDescriptor->video.video_parameters.height;
    }

    if (src_bitmap.format == STM_BLITTER_SF_YUYV)
    {
        // Convert YUYV to UYVY
        src = stm_blitter_surface_new_preallocated(src_bitmap.format, src_bitmap.colorspace, &src_bitmap.buffer_address,
                                                   src_bitmap.buffer_size, &src_bitmap.size, src_bitmap.pitch);

        if (BLITTER_ERR(src))
        {
            SE_ERROR("Stream 0x%p Failed to allocate src surface (%d)\n", Encoder.EncodeStream, BLITTER_ERR(src));
            return PreprocError;
        }

        Status = Preproc_Base_c::GetNewBuffer(&PreprocFrameBufferIntermediate);
        if (Status != PreprocNoError)
        {
            PREPROC_ERROR_RUNNING("Stream 0x%p Failed to get new intermediate buffer\n", Encoder.EncodeStream);
            return Status;
        }

        PreprocFrameBufferIntermediate->ObtainDataReference(NULL, NULL, (void **)(&PreprocBufferAddrPhys2), PhysicalAddress);
        if (PreprocBufferAddrPhys2 == NULL)
        {
            SE_ERROR("Stream 0x%p Failed to get preproc data buffer reference\n", Encoder.EncodeStream);
            return PreprocError;
        }

        dst_bitmap         = src_bitmap;
        dst_bitmap.format  = STM_BLITTER_SF_UYVY;
        dst_bitmap.buffer_address.base = (unsigned long)PreprocBufferAddrPhys2;
        dst = stm_blitter_surface_new_preallocated(dst_bitmap.format, dst_bitmap.colorspace, &dst_bitmap.buffer_address,
                                                   dst_bitmap.buffer_size, &dst_bitmap.size, dst_bitmap.pitch);
        if (dst == NULL)
        {
            SE_ERROR("Stream 0x%p Failed to allocate dst surface\n", Encoder.EncodeStream);
            return PreprocError;
        }

        DumpBlitterTaskDescriptor(&src_bitmap, &dst_bitmap, BufferFlag);

        // Blit surface using fence mode
        blitStatus = stm_blitter_surface_set_blitflags(dst, STM_BLITTER_SBF_NONE);
        if (blitStatus < 0)
        {
            SE_ERROR("Stream 0x%p Failed to set blit flags (%d)\n", Encoder.EncodeStream, blitStatus);
            stm_blitter_surface_put(src);
            stm_blitter_surface_put(dst);
            stm_blitter_put(blitter);
            return PreprocError;
        }

        blitStatus = stm_blitter_surface_add_fence(dst);
        if (blitStatus < 0)
        {
            SE_ERROR("Stream 0x%p Failed to add fence (%d)\n", Encoder.EncodeStream, blitStatus);
            stm_blitter_surface_put(src);
            stm_blitter_surface_put(dst);
            return PreprocError;
        }

        blitStatus = stm_blitter_surface_blit(blitter, src, &src_bitmap.rect, dst, &dst_bitmap.rect.position, 1);
        if (blitStatus < 0)
        {
            SE_ERROR("Stream 0x%p Failed to blit surface (%d)\n", Encoder.EncodeStream, blitStatus);
            stm_blitter_surface_put(src);
            stm_blitter_surface_put(dst);
            PreprocFrameBufferIntermediate->DecrementReferenceCount(IdentifierEncoderPreprocessor);
            return PreprocError;
        }

        blitStatus = stm_blitter_get_serial(blitter, &serial);

        if (blitStatus < 0)
        {
            SE_ERROR("Stream 0x%p Failed to get serial blit (%d)\n", Encoder.EncodeStream, blitStatus);
            stm_blitter_surface_put(src);
            stm_blitter_surface_put(dst);
            return PreprocError;
        }

        blitStatus = stm_blitter_wait(blitter, STM_BLITTER_WAIT_FENCE, serial);
        if (blitStatus < 0)
        {
            SE_ERROR("Stream 0x%p Failed to wait for serial blit (%d)\n", Encoder.EncodeStream, blitStatus);
            stm_blitter_surface_put(src);
            stm_blitter_surface_put(dst);
            return PreprocError;
        }

        // Clear the surfaces
        stm_blitter_surface_put(src);
        stm_blitter_surface_put(dst);
        src = dst = NULL;
        // reset source
        src_bitmap = dst_bitmap;
    }

    if (BufferFlag & BLITTER_BUFFER_INTERLACED)
    {
        src_bitmap.size.h       >>= 1; // half the height
        src_bitmap.rect.size.h  >>= 1; // half the height

        if ((src_bitmap.format == STM_BLITTER_SF_YCBCR420MB) || (src_bitmap.format == STM_BLITTER_SF_YCBCR422MB))
        {
            if (BufferFlag & BLITTER_BUFFER_BOTTOM)
            {
                src_bitmap.rect.position.y = 1; //y offset 1
            }
        }
        else
        {
            src_bitmap.pitch        <<= 1; //double the pitch

            if (BufferFlag & BLITTER_BUFFER_BOTTOM)
            {
                src_bitmap.buffer_address.base += PreprocMetaDataDescriptor->video.pitch;

                // contiguous chroma buffer has pitch half that of luma
                if (src_bitmap.format == STM_BLITTER_SF_YV16 || src_bitmap.format == STM_BLITTER_SF_YV12 ||
                    src_bitmap.format == STM_BLITTER_SF_YV61 || src_bitmap.format == STM_BLITTER_SF_I420)
                {
                    src_bitmap.buffer_address.cr_offset -= (PreprocMetaDataDescriptor->video.pitch >> 1);
                    src_bitmap.buffer_address.cb_offset -= (PreprocMetaDataDescriptor->video.pitch >> 1);
                }
            }
        }
    }

    src = stm_blitter_surface_new_preallocated(src_bitmap.format, src_bitmap.colorspace, &src_bitmap.buffer_address,
                                               src_bitmap.buffer_size, &src_bitmap.size, src_bitmap.pitch);
    if (BLITTER_ERR(src))
    {
        SE_ERROR("Stream 0x%p Failed to allocate src surface (%d)\n", Encoder.EncodeStream, BLITTER_ERR(src));
        return PreprocError;
    }

    return PreprocNoError;
}

PreprocStatus_t Preproc_Video_Blitter_c::PrepareDstBlitSurface(void *PreprocBufferAddrPhys, ARCCtrl_t *ARCCtrl)
{
    // Prepare blitting surface for destination
    if (PreprocMetaDataDescriptor->video.video_parameters.colorspace == STM_SE_COLORSPACE_UNSPECIFIED)
    {
        dst_bitmap.colorspace = src_bitmap.colorspace;
    }
    else
    {
        dst_bitmap.colorspace = GetBlitterSurfaceColorspace(GetDefaultOutputColorspace(PreprocMetaDataDescriptor->video.video_parameters.colorspace),
                                                            PreprocMetaDataDescriptor->video.video_parameters.video_full_range);
    }
    dst_bitmap.size.w              = ALIGN_UP(PreprocCtrl.Resolution.width, 16);
    dst_bitmap.size.h              = ALIGN_UP(PreprocCtrl.Resolution.height, 16);
    dst_bitmap.buffer_address.base = (unsigned long)PreprocBufferAddrPhys;
    dst_bitmap.rect.position.x     = ARCCtrl->DstRect.x;
    dst_bitmap.rect.position.y     = ARCCtrl->DstRect.y;
    dst_bitmap.rect.size.w         = ARCCtrl->DstRect.width;
    dst_bitmap.rect.size.h         = ARCCtrl->DstRect.height;

    // To optimize memory consumption, dst format is always STM_BLITTER_SF_NV12
    // (and not STM_BLITTER_SF_UYVY that is also managed by HVA as input color format)
    dst_bitmap.format                     = STM_BLITTER_SF_NV12;
    dst_bitmap.aligned_size.w             = dst_bitmap.size.w;
    dst_bitmap.aligned_size.h             = dst_bitmap.size.h;
    dst_bitmap.pitch                      = dst_bitmap.aligned_size.w;
    dst_bitmap.buffer_address.cbcr_offset = dst_bitmap.aligned_size.w * dst_bitmap.aligned_size.h;
    dst_bitmap.buffer_size                = dst_bitmap.aligned_size.w * dst_bitmap.aligned_size.h * 3 / 2;

    dst = stm_blitter_surface_new_preallocated(dst_bitmap.format, dst_bitmap.colorspace, &dst_bitmap.buffer_address,
                                               dst_bitmap.buffer_size, &dst_bitmap.size, dst_bitmap.pitch);
    if (BLITTER_ERR(dst))
    {
        SE_ERROR("Stream 0x%p Failed to allocate dst surface (%d)\n", Encoder.EncodeStream, BLITTER_ERR(dst));
        return PreprocError;
    }

    return PreprocNoError;
}

PreprocStatus_t Preproc_Video_Blitter_c::FinalizeInit()
{
    // Get a blitter handle
    int blitter_id = ModuleParameter_BlitterId();
    blitter = stm_blitter_get(blitter_id);
    if (BLITTER_ERR(blitter))
    {
        blitter = NULL;
        SE_ERROR("Stream 0x%p Failed to get blitter device %d (%d)\n", Encoder.EncodeStream, blitter_id, BLITTER_ERR(blitter));
        return PreprocError;
    }

    return PreprocNoError;
}

PreprocStatus_t Preproc_Video_Blitter_c::Halt()
{
    // Put a blitter handle
    if (blitter != NULL)
    {
        stm_blitter_put(blitter);
        blitter = NULL;
    }

    // Call the Base Class implementation
    return Preproc_Video_c::Halt();
}

void Preproc_Video_Blitter_c::AdjustBorderRectToHWConstraints(stm_blitter_dimension_t *buffer_dim, stm_blitter_rect_t *black_rect)
{
    long delta;

    if (black_rect->size.w < MIN_BLITTER_RECT_WIDTH)
    {
        // Get delta size to increase rectangle width for reaching minimum value
        delta = MIN_BLITTER_RECT_WIDTH - black_rect->size.w;
        // Expand width rectangle to its minimum value
        black_rect->size.w += delta;

        if (black_rect->position.x + black_rect->size.w > buffer_dim->w)
        {
            // width rectangle is outside the buffer area so set it back inside
            black_rect->position.x -= delta;
        }
    }

    if (black_rect->size.h < MIN_BLITTER_RECT_HEIGHT)
    {
        // Get delta size to increase rectangle height for reaching minimum value
        delta = MIN_BLITTER_RECT_HEIGHT - black_rect->size.h;
        // Expand height rectangle to its minimum value
        black_rect->size.h += delta;

        if (black_rect->position.y + black_rect->size.h > buffer_dim->h)
        {
            // height rectangle is outside the buffer area so set it back inside
            black_rect->position.y -= delta;
        }
    }
}

PreprocStatus_t Preproc_Video_Blitter_c::FillPictureBorder()
{
    int blitStatus;
    stm_blitter_color_t      black_color;
    stm_blitter_rect_t       black_rect;
    stm_blitter_rect_t       picture_area = dst_bitmap.rect;
    stm_blitter_dimension_t  buffer_dim   = dst_bitmap.aligned_size;

    // Destination surface is always YUV
    blitStatus = stm_blitter_surface_set_color_colorspace(dst, STM_BLITTER_COLOR_YUV);
    if (blitStatus < 0)
    {
        SE_ERROR("Stream 0x%p Failed to set color space (%d)\n", Encoder.EncodeStream, blitStatus);
        return PreprocError;
    }

    blitStatus = stm_blitter_surface_set_blitflags(dst, STM_BLITTER_SBF_NONE);
    if (blitStatus < 0)
    {
        SE_ERROR("Stream 0x%p Failed to set blit flags (%d)\n", Encoder.EncodeStream, blitStatus);
        return PreprocError;
    }


    black_color.a  = 0x80;
    black_color.y  = PreprocMetaDataDescriptor->video.video_parameters.video_full_range ? 0x00 : 0x10;
    black_color.cr = 0x80;
    black_color.cb = 0x80;

    // Set black color for drawing
    blitStatus = stm_blitter_surface_set_color(dst, &black_color);
    if (blitStatus < 0)
    {
        SE_ERROR("Stream 0x%p Failed to set color (%d)\n", Encoder.EncodeStream, blitStatus);
        return PreprocError;
    }

    // Fill picture border rectangles: top/left/right/bottom.
    // There are at least 4 possible rectangles to fill with black color around the picture
    // in the destination buffer.
    // ----------------------------------
    // |                                |
    // |           top_rect             |
    // |                                |
    // |--------------------------------|
    // | left_ |               | right_ |
    // | rect  | PICTURE AREA  | rect   |
    // |       |               |        |
    // |--------------------------------|
    // |                                |
    // |          bottom_rect           |
    // |                                |
    // |--------------------------------|
    // picture area is already truncated to buffer dimension
    // due to previous generic treatments

    if (picture_area.position.y > 0)
    {
        // Fill top rectangle coordinate
        black_rect.position.x = 0;
        black_rect.position.y = 0;
        black_rect.size.w     = buffer_dim.w;
        black_rect.size.h     = picture_area.position.y;

        AdjustBorderRectToHWConstraints(&buffer_dim, &black_rect);

        SE_EXTRAVERB(group_encoder_video_preproc, "Stream 0x%p TOP BORDER %ld %ld %ld %ld\n",
                     Encoder.EncodeStream,
                     black_rect.position.x,
                     black_rect.position.y,
                     black_rect.size.w,
                     black_rect.size.h);

        blitStatus = stm_blitter_surface_fill_rect(blitter, dst, &black_rect);
        if (blitStatus < 0)
        {
            SE_ERROR("Stream 0x%p Failed to fill top black border (%d)\n", Encoder.EncodeStream, blitStatus);
            return PreprocError;
        }
    }

    if (picture_area.position.x > 0)
    {
        // Fill left rectangle coordinate
        black_rect.position.x = 0;
        black_rect.position.y = picture_area.position.y;
        black_rect.size.w     = picture_area.position.x;
        black_rect.size.h     = picture_area.size.h;

        AdjustBorderRectToHWConstraints(&buffer_dim, &black_rect);

        SE_EXTRAVERB(group_encoder_video_preproc, "Stream 0x%p LEFT BORDER %ld %ld %ld %ld\n",
                     Encoder.EncodeStream,
                     black_rect.position.x,
                     black_rect.position.y,
                     black_rect.size.w,
                     black_rect.size.h);

        blitStatus = stm_blitter_surface_fill_rect(blitter, dst, &black_rect);
        if (blitStatus < 0)
        {
            SE_ERROR("Stream 0x%p Failed to fill left black border (%d)\n", Encoder.EncodeStream, blitStatus);
            return PreprocError;
        }
    }

    if ((picture_area.position.x + picture_area.size.w) < buffer_dim.w)
    {
        // Fill right rectangle coordinate
        black_rect.position.x = picture_area.position.x + picture_area.size.w;
        black_rect.position.y = picture_area.position.y;
        black_rect.size.w     = buffer_dim.w - (picture_area.position.x +
                                                picture_area.size.w);
        black_rect.size.h     = picture_area.size.h;

        AdjustBorderRectToHWConstraints(&buffer_dim, &black_rect);

        // Blitter workaround
        // It seems that blitter is not filling the last right column of picture area when both picture area x and picture area width are odd.
        if ((picture_area.position.x & 0x1) && (picture_area.size.w & 0x1)
            && (black_rect.position.x > 0))
        {
            // So expand right rectangle on the last right column of picture area
            black_rect.position.x -= 1;
            black_rect.size.w     += 1;
        }

        SE_EXTRAVERB(group_encoder_video_preproc, "Stream 0x%p RIGHT BORDER %ld %ld %ld %ld\n",
                     Encoder.EncodeStream,
                     black_rect.position.x,
                     black_rect.position.y,
                     black_rect.size.w,
                     black_rect.size.h);

        blitStatus = stm_blitter_surface_fill_rect(blitter, dst, &black_rect);
        if (blitStatus < 0)
        {
            SE_ERROR("Stream 0x%p Failed to fill right black border (%d)\n", Encoder.EncodeStream, blitStatus);
            return PreprocError;
        }
    }

    if ((picture_area.position.y + picture_area.size.h) < buffer_dim.h)
    {
        // Fill bottom rectangle coordinate
        black_rect.position.x = 0;
        black_rect.position.y = picture_area.position.y + picture_area.size.h;
        black_rect.size.w     = buffer_dim.w;
        black_rect.size.h     = buffer_dim.h - (picture_area.position.y +
                                                picture_area.size.h);

        AdjustBorderRectToHWConstraints(&buffer_dim, &black_rect);

        // Blitter workaround
        // It seems that blitter is not filling the last bottom line of picture area when both picture area y and picture area height are odd.
        if ((picture_area.position.y & 0x1) && (picture_area.size.h & 0x1)
            && (black_rect.position.y > 0))
        {
            // So expand bottom rectangle on the last bottom line of picture area
            black_rect.position.y -= 1;
            black_rect.size.h     += 1;
        }

        SE_EXTRAVERB(group_encoder_video_preproc, "Stream 0x%p BOTTOM BORDER %ld %ld %ld %ld\n",
                     Encoder.EncodeStream,
                     black_rect.position.x,
                     black_rect.position.y,
                     black_rect.size.w,
                     black_rect.size.h);

        blitStatus = stm_blitter_surface_fill_rect(blitter, dst, &black_rect);
        if (blitStatus < 0)
        {
            SE_ERROR("Stream 0x%p Failed to fill bottom black border (%d)\n", Encoder.EncodeStream, blitStatus);
            return PreprocError;
        }
    }

    // Alternative method is to use stm_blitter_surface_clear
    // It may consume higher bandwidth compared to drawing rectangles
    return PreprocNoError;
}

void Preproc_Video_Blitter_c::DumpBlitterTaskDescriptor(bitmap_t *Input, bitmap_t *Output, uint32_t BufferFlag)
{
    if (Input && Output && SE_IS_VERBOSE_ON(group_encoder_video_preproc))
    {
        SE_VERBOSE(group_encoder_video_preproc, "Stream 0x%p [BLITTER TASK DESCRIPTOR]\n", Encoder.EncodeStream);
        SE_VERBOSE(group_encoder_video_preproc, "Stream 0x%p |-[input]\n", Encoder.EncodeStream);
        SE_VERBOSE(group_encoder_video_preproc, "Stream 0x%p | |-format = 0x%x (%s)\n", Encoder.EncodeStream, Input->format, StringifyBlitterSurfaceFormat(Input->format));
        SE_VERBOSE(group_encoder_video_preproc, "Stream 0x%p | |-colorspace = %d (%s)\n", Encoder.EncodeStream, Input->colorspace, StringifyBlitterColorspace(Input->colorspace));
        SE_VERBOSE(group_encoder_video_preproc, "Stream 0x%p | |-[buffer_address]\n", Encoder.EncodeStream);
        SE_VERBOSE(group_encoder_video_preproc, "Stream 0x%p | | |-base = 0x%lx\n", Encoder.EncodeStream, Input->buffer_address.base);
        SE_VERBOSE(group_encoder_video_preproc, "Stream 0x%p | | |-cb_offset = %ld bytes\n", Encoder.EncodeStream, Input->buffer_address.cb_offset);
        SE_VERBOSE(group_encoder_video_preproc, "Stream 0x%p | | |-cr_offset = %ld bytes\n", Encoder.EncodeStream, Input->buffer_address.cr_offset);
        SE_VERBOSE(group_encoder_video_preproc, "Stream 0x%p | |\n", Encoder.EncodeStream);
        SE_VERBOSE(group_encoder_video_preproc, "Stream 0x%p | |-buffer_size = %lu bytes\n", Encoder.EncodeStream, Input->buffer_size);
        SE_VERBOSE(group_encoder_video_preproc, "Stream 0x%p | |-size.w = %ld pixels\n", Encoder.EncodeStream, Input->size.w);
        SE_VERBOSE(group_encoder_video_preproc, "Stream 0x%p | |-size.h = %ld pixels\n", Encoder.EncodeStream, Input->size.h);
        SE_VERBOSE(group_encoder_video_preproc, "Stream 0x%p | |-aligned_size.w = %ld pixels\n", Encoder.EncodeStream, Input->aligned_size.w);
        SE_VERBOSE(group_encoder_video_preproc, "Stream 0x%p | |-aligned_size.h = %ld pixels\n", Encoder.EncodeStream, Input->aligned_size.h);
        SE_VERBOSE(group_encoder_video_preproc, "Stream 0x%p | |-pitch = %lu bytes\n", Encoder.EncodeStream, Input->pitch);
        SE_VERBOSE(group_encoder_video_preproc, "Stream 0x%p | |-[rect]\n", Encoder.EncodeStream);
        SE_VERBOSE(group_encoder_video_preproc, "Stream 0x%p |   |-position.x = %ld pixels\n", Encoder.EncodeStream, Input->rect.position.x);
        SE_VERBOSE(group_encoder_video_preproc, "Stream 0x%p |   |-position.y = %ld pixels\n", Encoder.EncodeStream, Input->rect.position.y);
        SE_VERBOSE(group_encoder_video_preproc, "Stream 0x%p |   |-size.w = %ld pixels\n", Encoder.EncodeStream, Input->rect.size.w);
        SE_VERBOSE(group_encoder_video_preproc, "Stream 0x%p |   |-size.h = %ld pixels\n", Encoder.EncodeStream, Input->rect.size.h);
        SE_VERBOSE(group_encoder_video_preproc, "Stream 0x%p |\n", Encoder.EncodeStream);
        SE_VERBOSE(group_encoder_video_preproc, "Stream 0x%p |-[flags]\n", Encoder.EncodeStream);
        SE_VERBOSE(group_encoder_video_preproc, "Stream 0x%p   |-flags = 0x%x\n", Encoder.EncodeStream, BufferFlag);
        SE_VERBOSE(group_encoder_video_preproc, "Stream 0x%p |\n", Encoder.EncodeStream);
        SE_VERBOSE(group_encoder_video_preproc, "Stream 0x%p |-[output]\n", Encoder.EncodeStream);
        SE_VERBOSE(group_encoder_video_preproc, "Stream 0x%p   |-format = 0x%x (%s)\n", Encoder.EncodeStream, Output->format, StringifyBlitterSurfaceFormat(Output->format));
        SE_VERBOSE(group_encoder_video_preproc, "Stream 0x%p   |-colorspace = %d (%s)\n", Encoder.EncodeStream, Output->colorspace, StringifyBlitterColorspace(Output->colorspace));
        SE_VERBOSE(group_encoder_video_preproc, "Stream 0x%p   |-[buffer_address]\n", Encoder.EncodeStream);
        SE_VERBOSE(group_encoder_video_preproc, "Stream 0x%p   | |-base = 0x%lx\n", Encoder.EncodeStream, Output->buffer_address.base);
        SE_VERBOSE(group_encoder_video_preproc, "Stream 0x%p   | |-cb_offset = %ld bytes\n", Encoder.EncodeStream, Output->buffer_address.cb_offset);
        SE_VERBOSE(group_encoder_video_preproc, "Stream 0x%p   | |-cr_offset = %ld bytes\n", Encoder.EncodeStream, Output->buffer_address.cr_offset);
        SE_VERBOSE(group_encoder_video_preproc, "Stream 0x%p   |\n", Encoder.EncodeStream);
        SE_VERBOSE(group_encoder_video_preproc, "Stream 0x%p   |-buffer_size = %lu bytes\n", Encoder.EncodeStream, Output->buffer_size);
        SE_VERBOSE(group_encoder_video_preproc, "Stream 0x%p   |-size.w = %ld pixels\n", Encoder.EncodeStream, Output->size.w);
        SE_VERBOSE(group_encoder_video_preproc, "Stream 0x%p   |-size.h = %ld pixels\n", Encoder.EncodeStream, Output->size.h);
        SE_VERBOSE(group_encoder_video_preproc, "Stream 0x%p   |-aligned_size.w = %ld pixels\n", Encoder.EncodeStream, Output->aligned_size.w);
        SE_VERBOSE(group_encoder_video_preproc, "Stream 0x%p   |-aligned_size.h = %ld pixels\n", Encoder.EncodeStream, Output->aligned_size.h);
        SE_VERBOSE(group_encoder_video_preproc, "Stream 0x%p   |-pitch = %lu bytes\n", Encoder.EncodeStream, Output->pitch);
        SE_VERBOSE(group_encoder_video_preproc, "Stream 0x%p   |-[rect]\n", Encoder.EncodeStream);
        SE_VERBOSE(group_encoder_video_preproc, "Stream 0x%p     |-position.x = %ld pixels\n", Encoder.EncodeStream, Output->rect.position.x);
        SE_VERBOSE(group_encoder_video_preproc, "Stream 0x%p     |-position.y = %ld pixels\n", Encoder.EncodeStream, Output->rect.position.y);
        SE_VERBOSE(group_encoder_video_preproc, "Stream 0x%p     |-size.w = %ld pixels\n", Encoder.EncodeStream, Output->rect.size.w);
        SE_VERBOSE(group_encoder_video_preproc, "Stream 0x%p     |-size.h = %ld pixels\n", Encoder.EncodeStream, Output->rect.size.h);
        SE_VERBOSE(group_encoder_video_preproc, "Stream 0x%p\n", Encoder.EncodeStream);
    }
}

PreprocStatus_t Preproc_Video_Blitter_c::BlitSurface(Buffer_t InputBuffer, Buffer_t OutputBuffer, bool SecondField)
{
    unsigned int DataSize;
    unsigned int ShrinkSize;
    void *InputBufferAddrPhys;
    void *PreprocBufferAddrPhys;
    stm_blitter_serial_t serial;
    stm_blitter_surface_blitflags_t BlitFlags;
    int blitStatus;
    Buffer_t ContentBuffer;
    ARCCtrl_t ARCCtrl;
    uint32_t BufferFlag = 0;

    // Program buffer flag
    if ((PreprocCtrl.EnableDeInterlacer != STM_SE_CTRL_VALUE_DISAPPLY) && (PreprocMetaDataDescriptor->video.video_parameters.scan_type == STM_SE_SCAN_TYPE_INTERLACED))
    {
        BufferFlag  = (uint32_t)(BLITTER_BUFFER_INTERLACED);

        // Top field when top field first && first field or bot field first && second field
        if ((!(PreprocMetaDataDescriptor->video.top_field_first == SecondField) && (PreprocMetaDataDescriptor->video.picture_structure == STM_SE_PICTURE_STRUCTURE_FRAME)) ||
            (PreprocMetaDataDescriptor->video.picture_structure == STM_SE_PICTURE_STRUCTURE_TOP_FIELD))
        {
            BufferFlag |= (uint32_t)(BLITTER_BUFFER_TOP);
        }
        else
        {
            BufferFlag |= (uint32_t)(BLITTER_BUFFER_BOTTOM);
        }
    }

    // Copy data buffer
    InputBuffer->ObtainDataReference(NULL, &DataSize, (void **)(&InputBufferAddrPhys), PhysicalAddress);
    if (InputBufferAddrPhys == NULL)
    {
        SE_ERROR("Stream 0x%p Failed to get input data buffer reference\n", Encoder.EncodeStream);
        return PreprocError;
    }

    OutputBuffer->ObtainDataReference(NULL, NULL, (void **)(&PreprocBufferAddrPhys), PhysicalAddress);
    if (PreprocBufferAddrPhys == NULL)
    {
        SE_ERROR("Stream 0x%p Failed to get preproc data buffer reference\n", Encoder.EncodeStream);
        return PreprocError;
    }

    PreprocFrameBufferIntermediate = NULL;
    // Convert aspect ratio
    ConvertAspectRatio(&ARCCtrl);

    // Prepare blitting surface for source
    PreprocStatus_t Status = PrepareSrcBlitSurface(InputBufferAddrPhys, BufferFlag);
    if (Status != PreprocNoError)
    {
        PREPROC_ERROR_RUNNING("Stream 0x%p Failed to prepare src blit surface\n", Encoder.EncodeStream);
        return Status;
    }

    // Prepare blitting surface for destination
    Status = PrepareDstBlitSurface(PreprocBufferAddrPhys, &ARCCtrl);
    if (Status != PreprocNoError)
    {
        PREPROC_ERROR_RUNNING("Stream 0x%p Failed to prepare dest blit surface\n", Encoder.EncodeStream);
        return Status;
    }

    DumpBlitterTaskDescriptor(&src_bitmap, &dst_bitmap, BufferFlag);

    // Fill picture borders
    Status = FillPictureBorder();
    if (Status != PreprocNoError)
    {
        SE_ERROR("Stream 0x%p Failed to fill picture border\n", Encoder.EncodeStream);
        stm_blitter_surface_put(src);
        stm_blitter_surface_put(dst);
        return PreprocError;
    }

    // Blit surface using fence mode
    BlitFlags = STM_BLITTER_SBF_NONE;

    if (BufferFlag & BLITTER_BUFFER_INTERLACED)
    {
        if (BufferFlag & BLITTER_BUFFER_TOP)
        {
            BlitFlags = STM_BLITTER_SBF_DEINTERLACE_TOP;
        }
        else if (BufferFlag & BLITTER_BUFFER_BOTTOM)
        {
            BlitFlags = STM_BLITTER_SBF_DEINTERLACE_BOTTOM;
        }
    }

    blitStatus = stm_blitter_surface_set_blitflags(dst, BlitFlags);
    if (blitStatus < 0)
    {
        SE_ERROR("Stream 0x%p Failed to set blit flags (%d)\n", Encoder.EncodeStream, blitStatus);
        stm_blitter_surface_put(src);
        stm_blitter_surface_put(dst);
        return PreprocError;
    }

    // Scene change detection
    if ((PreprocCtrl.EnableSceneCut == STM_SE_CTRL_VALUE_APPLY) && mSceneCutSupported)
    {
        mSceneCutContext.sceneChange = (DetectSceneChange(SecondField) ? (mSceneCutContext.sceneChange + 1) : 0);
    }
    else
    {
        mSceneCutContext.sceneChange = 0;
    }

    int FilterLevel = 0;
    if (PreprocCtrl.EnableNoiseFilter == STM_SE_CTRL_VALUE_APPLY)
    {
        FilterLevel = Encoder.EncodeStream->GetVideoEncodeFilterLevel();
        PreprocFullMetaDataDescriptor->encode_process_metadata.video.filter_level = FilterLevel;
        blitStatus = stm_blitter_surface_set_rescale_filter_level(dst, FilterLevel);
        if (blitStatus < 0)
        {
            SE_ERROR("Stream 0x%p Failed to set filter level to %d (%d)\n", Encoder.EncodeStream, FilterLevel, blitStatus);
            stm_blitter_surface_put(src);
            stm_blitter_surface_put(dst);
            return PreprocError;
        }
    }

    blitStatus = stm_blitter_surface_add_fence(dst);
    if (blitStatus < 0)
    {
        SE_ERROR("Stream 0x%p Failed to add fence (%d)\n", Encoder.EncodeStream, blitStatus);
        stm_blitter_surface_put(src);
        stm_blitter_surface_put(dst);
        return PreprocError;
    }

    // If deinterlacing (BufferFlag != 0) or filtering or resize is required, stretch blit is used.
    if (src_bitmap.rect.size.w == dst_bitmap.rect.size.w && src_bitmap.rect.size.h == dst_bitmap.rect.size.h && BufferFlag == 0 && FilterLevel == 0)
    {
        //unsupported configuration will be identified at this stage with blit operation returning an error
        blitStatus = stm_blitter_surface_blit(blitter, src, &src_bitmap.rect, dst, &dst_bitmap.rect.position, 1);
        if (blitStatus < 0)
        {
            SE_ERROR("Stream 0x%p Failed to blit surface (%d)\n", Encoder.EncodeStream, blitStatus);
            stm_blitter_surface_put(src);
            stm_blitter_surface_put(dst);
            return PreprocError;
        }
    }
    else
    {
        //unsupported configuration will be identified at this stage with blit operation returning an error
        blitStatus = stm_blitter_surface_stretchblit(blitter, src, &src_bitmap.rect, dst, &dst_bitmap.rect, 1);
        if (blitStatus < 0)
        {
            SE_ERROR("Stream 0x%p Failed to blit surface stretch (%d)\n", Encoder.EncodeStream, blitStatus);
            stm_blitter_surface_put(src);
            stm_blitter_surface_put(dst);
            return PreprocError;
        }
    }

    if (FilterLevel != 0)
    {
        // Reset rescale filter level to 0
        blitStatus = stm_blitter_surface_set_rescale_filter_level(dst, 0);
        if (blitStatus < 0)
        {
            SE_ERROR("Stream 0x%p Failed to reset filter level to 0 (%d)\n", Encoder.EncodeStream, blitStatus);
            stm_blitter_surface_put(src);
            stm_blitter_surface_put(dst);
            return PreprocError;
        }
    }

    // Update meta data
    PreprocMetaDataDescriptor->video.video_parameters.width                          = PreprocCtrl.Resolution.width;
    PreprocMetaDataDescriptor->video.video_parameters.height                         = PreprocCtrl.Resolution.height;

    if (dst_bitmap.format == STM_BLITTER_SF_UYVY)
    {
        PreprocMetaDataDescriptor->video.surface_format                              = SURFACE_FORMAT_VIDEO_422_RASTER;
        PreprocMetaDataDescriptor->video.pitch                                       = PreprocCtrl.Resolution.width * 2;
    }
    else if (dst_bitmap.format == STM_BLITTER_SF_NV12)
    {
        PreprocMetaDataDescriptor->video.surface_format                              = SURFACE_FORMAT_VIDEO_420_RASTER2B;
        PreprocMetaDataDescriptor->video.pitch                                       = PreprocCtrl.Resolution.width;
    }

    // Update colorspace
    PreprocMetaDataDescriptor->video.video_parameters.colorspace = GetDefaultOutputColorspace(PreprocMetaDataDescriptor->video.video_parameters.colorspace);

    PreprocMetaDataDescriptor->video.frame_rate                                      = PreprocCtrl.Framerate;

    // As coder only needs fractional framerate, integer framerate is set to 0
    PreprocMetaDataDescriptor->video.video_parameters.frame_rate                     = 0;

    // As coder only needs pixel aspect ratio, display aspect ratio is set to unspecified
    PreprocMetaDataDescriptor->video.video_parameters.aspect_ratio                   = STM_SE_ASPECT_RATIO_UNSPECIFIED;
    PreprocMetaDataDescriptor->video.video_parameters.pixel_aspect_ratio_numerator   = ARCCtrl.DstPixelAspectRatio.num;
    PreprocMetaDataDescriptor->video.video_parameters.pixel_aspect_ratio_denominator = ARCCtrl.DstPixelAspectRatio.den;

    PreprocMetaDataDescriptor->video.video_parameters.scan_type                      = STM_SE_SCAN_TYPE_PROGRESSIVE;
    PreprocMetaDataDescriptor->video.vertical_alignment                              = 1;

    // Propagate scene change detection metadata to coder
    PreprocFullMetaDataDescriptor->encode_process_metadata.video.scene_change        = mSceneCutContext.sceneChange;

    blitStatus = stm_blitter_get_serial(blitter, &serial);

    if (blitStatus < 0)
    {
        SE_ERROR("Stream 0x%p Failed to get serial blit (%d)\n", Encoder.EncodeStream, blitStatus);
        stm_blitter_surface_put(src);
        stm_blitter_surface_put(dst);
        return PreprocError;
    }

    blitStatus = stm_blitter_wait(blitter, STM_BLITTER_WAIT_FENCE, serial);
    if (blitStatus < 0)
    {
        SE_ERROR("Stream 0x%p Failed to wait for serial blit (%d)\n", Encoder.EncodeStream, blitStatus);
        stm_blitter_surface_put(src);
        stm_blitter_surface_put(dst);
        return PreprocError;
    }

    // Add visual information for debugging scene change detection
    // TODO Use debugOption from control/debugfs instead of trace activation
    if (mSceneCutContext.sceneChange && SE_IS_DEBUG_ON(group_encoder_video_preproc_marker))
    {
        // Fill picture corners
        Status = FillPictureMarker(PreprocMetaDataDescriptor->video.video_parameters.video_full_range);
        if (Status != PreprocNoError)
        {
            SE_ERROR("Stream 0x%p Failed to fill picture corner\n", Encoder.EncodeStream);
            stm_blitter_surface_put(src);
            stm_blitter_surface_put(dst);
            return PreprocError;
        }
    }

    // Remove blitting surfaces
    stm_blitter_surface_put(src);
    stm_blitter_surface_put(dst);
    src = dst = NULL;

    // Clear the intermediate memory buffer due to format conversion.
    if (PreprocFrameBufferIntermediate != NULL)
    {
        PreprocFrameBufferIntermediate->DecrementReferenceCount(IdentifierEncoderPreprocessor);
        PreprocFrameBufferIntermediate = NULL;
    }

    // Completion
    DataSize = dst_bitmap.buffer_size;
    OutputBuffer->SetUsedDataSize(DataSize);

    OutputBuffer->ObtainAttachedBufferReference(PreprocFrameAllocType, &ContentBuffer);
    SE_ASSERT(ContentBuffer != NULL);

    ContentBuffer->SetUsedDataSize(DataSize);
    ShrinkSize = max(DataSize, 1);
    BufferStatus_t BufStatus = ContentBuffer->ShrinkBuffer(ShrinkSize);
    if (BufStatus != BufferNoError)
    {
        SE_ERROR("Stream 0x%p Failed to shrink the operating buffer to size %d\n", Encoder.EncodeStream, ShrinkSize);
        return PreprocError;
    }

    return PreprocNoError;
}

PreprocStatus_t Preproc_Video_Blitter_c::RepeatOutputFrame(Buffer_t InputBuffer, Buffer_t OutputBuffer, FRCCtrl_t *FRCCtrl)
{
    PreprocStatus_t Status;
    int32_t cnt;

    // Get ref preproc buffer metadata descriptor
    __stm_se_frame_metadata_t            *RefPreprocFullMetaDataDescriptor;
    OutputBuffer->ObtainMetaDataReference(OutputMetaDataBufferType, (void **)(&RefPreprocFullMetaDataDescriptor));
    SE_ASSERT(RefPreprocFullMetaDataDescriptor != NULL);

    for (cnt = 0; cnt < FRCCtrl->FrameRepeat; cnt ++)
    {
        // Get preproc buffer clone
        Buffer_t PreprocFrameBufferClone;
        Status = Preproc_Base_c::GetBufferClone(OutputBuffer, &PreprocFrameBufferClone);
        if (Status != PreprocNoError)
        {
            PREPROC_ERROR_RUNNING("Stream 0x%p Failed to clone output buffer\n", Encoder.EncodeStream);
            return Status;
        }

        SE_DEBUG(group_encoder_video_preproc, "Stream 0x%p Copy buffer %p to cloned buffer %p\n", Encoder.EncodeStream, OutputBuffer, PreprocFrameBufferClone);

        // Attach the Input buffer to the BufferClone so that it is kept until Preprocessing is complete.
        BufferStatus_t BufStatus = PreprocFrameBufferClone->AttachBuffer(InputBuffer);
        if (BufStatus != BufferNoError)
        {
            SE_ERROR("Stream 0x%p Failed to attach input buffer\n", Encoder.EncodeStream);
            PreprocFrameBufferClone->DecrementReferenceCount(IdentifierEncoderPreprocessor);
            return PreprocError;
        }

        // Obtain preproc metadata reference
        __stm_se_frame_metadata_t  *ClonePreprocFullMetaDataDescriptor;
        PreprocFrameBufferClone->ObtainMetaDataReference(OutputMetaDataBufferType, (void **)(&ClonePreprocFullMetaDataDescriptor));
        SE_ASSERT(ClonePreprocFullMetaDataDescriptor != NULL);

        // Copy metadata
        stm_se_uncompressed_frame_metadata_t *ClonePreprocMetaDataDescriptor;
        ClonePreprocMetaDataDescriptor = &ClonePreprocFullMetaDataDescriptor->uncompressed_frame_metadata;
        memcpy(ClonePreprocFullMetaDataDescriptor, RefPreprocFullMetaDataDescriptor, sizeof(__stm_se_frame_metadata_t));

        // Update PTS info
        uint64_t native_time = FRCCtrl->NativeTimeOutput + (cnt * FRCCtrl->TimePerOutputFrame);
        uint64_t encoded_time = FRCCtrl->EncodedTimeOutput + (cnt * FRCCtrl->TimePerOutputFrame);

        //ensure computed PTS fit in 33 bits as required in mpeg2 system standard
        if (ClonePreprocMetaDataDescriptor->native_time_format == TIME_FORMAT_PTS)
        {
            native_time = TimeStamp_c(native_time, TIME_FORMAT_PTS).PtsValue();
        }
        if (ClonePreprocFullMetaDataDescriptor->encode_coordinator_metadata.encoded_time_format == TIME_FORMAT_PTS)
        {
            encoded_time = TimeStamp_c(encoded_time, TIME_FORMAT_PTS).PtsValue();
        }

        ClonePreprocMetaDataDescriptor->native_time = native_time;
        ClonePreprocFullMetaDataDescriptor->encode_coordinator_metadata.encoded_time = encoded_time;

        SE_DEBUG(group_encoder_video_preproc, "Stream 0x%p PTS: In %llu Out %llu %llu fmt:%d %d\n",
                 Encoder.EncodeStream,
                 InputMetaDataDescriptor->native_time,
                 ClonePreprocMetaDataDescriptor->native_time,
                 ClonePreprocFullMetaDataDescriptor->encode_coordinator_metadata.encoded_time,
                 ClonePreprocMetaDataDescriptor->native_time_format,
                 ClonePreprocFullMetaDataDescriptor->encode_coordinator_metadata.encoded_time_format);

        // Output frame
        Status = Output(PreprocFrameBufferClone);
        if (Status != PreprocNoError)
        {
            SE_ERROR("Stream 0x%p Failed to output frame\n", Encoder.EncodeStream);
            return PreprocError;
        }
    }

    return PreprocNoError;
}

PreprocStatus_t Preproc_Video_Blitter_c::FillPictureMarker(bool VideoFullRange)
{
    int blitStatus;
    stm_blitter_color_t white_color;
    stm_blitter_rect_t  white_rect;

    // Destination surface is always YUV
    blitStatus = stm_blitter_surface_set_color_colorspace(dst, STM_BLITTER_COLOR_YUV);
    if (blitStatus < 0)
    {
        SE_ERROR("Stream 0x%p Failed to set color space (%d)\n", Encoder.EncodeStream, blitStatus);
        return PreprocError;
    }

    blitStatus = stm_blitter_surface_set_blitflags(dst, STM_BLITTER_SBF_NONE);
    if (blitStatus < 0)
    {
        SE_ERROR("Stream 0x%p Failed to set blit flags (%d)\n", Encoder.EncodeStream, blitStatus);
        return PreprocError;
    }

    white_color.a  = 0x80;
    white_color.y  = VideoFullRange ? 0xFF : 0xEB;
    white_color.cr = 0x80;
    white_color.cb = 0x80;

    // Set white color for drawing
    blitStatus = stm_blitter_surface_set_color(dst, &white_color);
    if (blitStatus < 0)
    {
        SE_ERROR("Stream 0x%p Failed to set color (%d)\n", Encoder.EncodeStream, blitStatus);
        return PreprocError;
    }

    // Fill top rectangle coordinate
    white_rect.position.x = 0;
    white_rect.position.y = 0;
    white_rect.size.w     = ENCODE_WIDTH_MIN;
    white_rect.size.h     = ENCODE_HEIGHT_MIN;

    SE_INFO(group_encoder_video_preproc, "Stream 0x%p TOP LEFT CORNER %ld %ld %ld %ld\n",
            Encoder.EncodeStream,
            white_rect.position.x,
            white_rect.position.y,
            white_rect.size.w,
            white_rect.size.h);

    blitStatus = stm_blitter_surface_fill_rect(blitter, dst, &white_rect);
    if (blitStatus < 0)
    {
        SE_ERROR("Stream 0x%p Failed to fill top black border (%d)\n", Encoder.EncodeStream, blitStatus);
        return PreprocError;
    }

    return PreprocNoError;
}

unsigned int Preproc_Video_Blitter_c::DetectSceneChange(bool SecondField)
{
    unsigned int SceneChange = 0;
    unsigned int CEHRegistersSum = 0;
    unsigned int SceneChangeThreshold = 0;
    int CEHRegistersDiff = 0;
    unsigned int Variance = 0;
    unsigned int FrameId = Encoder.EncodeStream->EncodeStreamStatistics().BufferCountFromPreproc;
    unsigned int *CEHRegisters = PreprocMetaDataDescriptor->video.ceh_default_top;
    int i = 0;

    // If input stream is interlaced, modify histogram for progressive output (no more bottom histogram).
    if (PreprocMetaDataDescriptor->video.video_parameters.scan_type == STM_SE_SCAN_TYPE_INTERLACED)
    {
        if (PreprocCtrl.EnableDeInterlacer == STM_SE_CTRL_VALUE_APPLY)
        {
            // Top field when top field first && first field or bot field first && second field
            if (!(PreprocMetaDataDescriptor->video.top_field_first == SecondField))
            {
                //TOP
                for (i = 0; i < STM_SE_NUMBER_OF_CEH_INTERVALS; i++)
                {
                    CEHRegisters[i] = 2 * PreprocMetaDataDescriptor->video.ceh_default_top[i];
                }
            }
            else
            {
                //BOTTOM
                for (i = 0; i < STM_SE_NUMBER_OF_CEH_INTERVALS; i++)
                {
                    CEHRegisters[i] = 2 * PreprocMetaDataDescriptor->video.ceh_bottom[i];
                }
            }
        }
        else
        {
            // TOP & BOTTOM OUTPUT AS PROGRESSIVE
            for (i = 0; i < STM_SE_NUMBER_OF_CEH_INTERVALS; i++)
            {
                CEHRegisters[i] = PreprocMetaDataDescriptor->video.ceh_default_top[i] + PreprocMetaDataDescriptor->video.ceh_bottom[i];
            }
        }

        for (i = 0; i < STM_SE_NUMBER_OF_CEH_INTERVALS; i++)
        {
            PreprocMetaDataDescriptor->video.ceh_bottom[i] = 0;
        }
    }

    // Compute CEH registers to detect scene change
    if (FrameId == 0)
    {
        mSceneCutContext.minSad = UINT_MAX;
        mSceneCutContext.maxSad = 0;
        SceneChange = 1;
    }
    else
    {
        for (i = 0; i < STM_SE_NUMBER_OF_CEH_INTERVALS; i++)
        {
            CEHRegistersDiff = ((int)CEHRegisters[i] - (int)mSceneCutContext.previousCEHRegisters[i]) / 256;
            Variance += CEHRegistersDiff * CEHRegistersDiff;
            CEHRegistersSum += CEHRegisters[i];
        }
        Variance /= 32;
        mSceneCutContext.minSad = min(Variance, mSceneCutContext.minSad);
        mSceneCutContext.maxSad = max(Variance, mSceneCutContext.maxSad);
        // First approximation of scene change threshold that depends on picture size (3000 * 941 / 100 * ((W * H / 100) ^ 2) / (192 * 108 * 192 * 108))
        // SceneChangeThreshold reduced by 70%, this is based on experimentation on a stream.
        SceneChangeThreshold = (((((((3000 * 941 * 256) / 192 / 100) * 256 * 7 / 10) / 100) * (CEHRegistersSum / 256)) / 108) / 108) * (CEHRegistersSum / 256) / 192 / 100;
        if (Variance > SceneChangeThreshold)
        {
            SceneChange = 1;
        }
    }

    SE_VERBOSE(group_encoder_video_preproc, "[SCENE CUT CONTEXT]\n");
    SE_VERBOSE(group_encoder_video_preproc, "|-frame = %u\n", FrameId + 1);
    SE_VERBOSE(group_encoder_video_preproc, "|-from %s (%u)\n", StringifyPictureType(PreprocMetaDataDescriptor->video.picture_type), PreprocMetaDataDescriptor->video.picture_type);
    SE_VERBOSE(group_encoder_video_preproc, "|-SceneChange = %u\n", SceneChange);
    SE_VERBOSE(group_encoder_video_preproc, "|-CEHRegistersSum = %u\n", CEHRegistersSum);
    SE_VERBOSE(group_encoder_video_preproc, "|-Variance = %u\n", Variance);
    SE_VERBOSE(group_encoder_video_preproc, "|-minSad = %u, maxSad = %u\n", mSceneCutContext.minSad, mSceneCutContext.maxSad);
    SE_VERBOSE(group_encoder_video_preproc, "|-SceneChangeThreshold = %u,\n", SceneChangeThreshold);
    SE_VERBOSE(group_encoder_video_preproc, "\n");

    // Save CEH registers for next frame computation
    for (i = 0; i < STM_SE_NUMBER_OF_CEH_INTERVALS; i++)
    {
        mSceneCutContext.previousCEHRegisters[i] = CEHRegisters[i];
    }

    return SceneChange;
}

PreprocStatus_t Preproc_Video_Blitter_c::ProcessField(Buffer_t InputBuffer, Buffer_t OutputBuffer, bool SecondField)
{
    PreprocStatus_t Status;
    BufferStatus_t BufStatus;
    FRCCtrl_t FRCCtrl;
    // Convert frame rate
    FRCCtrl.SecondField = SecondField;
    ConvertFrameRate(&FRCCtrl);

    // Check if input frame is processed
    if (!FRCCtrl.DiscardInput)
    {
        // Initialize buffer and metadata if second field because it is not done by default in base classes
        if (SecondField)
        {
            // Get new preproc buffer
            Status = Preproc_Base_c::GetNewBuffer(&OutputBuffer);
            if (Status != PreprocNoError)
            {
                return Status;
            }

            // Obtain preproc metadata reference
            OutputBuffer->ObtainMetaDataReference(OutputMetaDataBufferType, (void **)(&PreprocFullMetaDataDescriptor));
            SE_ASSERT(PreprocFullMetaDataDescriptor != NULL);

            // copy metadata
            PreprocMetaDataDescriptor = &PreprocFullMetaDataDescriptor->uncompressed_frame_metadata;
            memcpy(PreprocMetaDataDescriptor, InputMetaDataDescriptor, sizeof(stm_se_uncompressed_frame_metadata_t));
            memcpy(&PreprocFullMetaDataDescriptor->encode_coordinator_metadata, EncodeCoordinatorMetaDataDescriptor, sizeof(__stm_se_encode_coordinator_metadata_t));

            // Attach the input buffer to the output buffer so that it is kept until preprocessing is complete.
            BufStatus = OutputBuffer->AttachBuffer(InputBuffer);
            if (BufStatus != BufferNoError)
            {
                SE_ERROR("Stream 0x%p Failed to attach input buffer\n", Encoder.EncodeStream);
                OutputBuffer->DecrementReferenceCount(IdentifierEncoderPreprocessor);
                return PreprocError;
            }
        }

        // Update preproc control status
        PreprocFullMetaDataDescriptor->encode_process_metadata.video.deinterlacing_on   = (PreprocCtrl.EnableDeInterlacer != STM_SE_CTRL_VALUE_DISAPPLY) ? true : false;
        PreprocFullMetaDataDescriptor->encode_process_metadata.video.noise_filtering_on = (PreprocCtrl.EnableNoiseFilter != STM_SE_CTRL_VALUE_DISAPPLY) ? true : false;
        PreprocFullMetaDataDescriptor->encode_process_metadata.video.scene_cut_on = ((PreprocCtrl.EnableSceneCut != STM_SE_CTRL_VALUE_DISAPPLY) && mSceneCutSupported) ? true : false;

        // Blit surface and update preproc output metadata
        Status = BlitSurface(InputBuffer, OutputBuffer, SecondField);
        if (Status != PreprocNoError)
        {
            PREPROC_ERROR_RUNNING("Stream 0x%p Failed to blit surface\n", Encoder.EncodeStream);
            OutputBuffer->DecrementReferenceCount(IdentifierEncoderPreprocessor);
            return Status;
        }

        // Check if output frame is output
        if (!FRCCtrl.DiscardOutput)
        {
            SE_DEBUG(group_encoder_video_preproc, "Stream 0x%p Output buffer %p\n", Encoder.EncodeStream, OutputBuffer);

            // Repeat output frame
            if (FRCCtrl.FrameRepeat > 0)
            {
                Status = RepeatOutputFrame(InputBuffer, OutputBuffer, &FRCCtrl);
                if (Status != PreprocNoError)
                {
                    PREPROC_ERROR_RUNNING("Stream 0x%p Failed to repeat output frame\n", Encoder.EncodeStream);
                    OutputBuffer->DecrementReferenceCount(IdentifierEncoderPreprocessor);
                    return PreprocError;
                }
            }

            // Update PTS info
            uint64_t native_time = FRCCtrl.NativeTimeOutput + (FRCCtrl.FrameRepeat * FRCCtrl.TimePerOutputFrame);
            uint64_t encoded_time = FRCCtrl.EncodedTimeOutput + (FRCCtrl.FrameRepeat * FRCCtrl.TimePerOutputFrame);

            //ensure computed PTS fit in 33 bits as required in mpeg2 system standard
            if (PreprocMetaDataDescriptor->native_time_format == TIME_FORMAT_PTS)
            {
                native_time = TimeStamp_c(native_time, TIME_FORMAT_PTS).PtsValue();
            }
            if (PreprocFullMetaDataDescriptor->encode_coordinator_metadata.encoded_time_format == TIME_FORMAT_PTS)
            {
                encoded_time = TimeStamp_c(encoded_time, TIME_FORMAT_PTS).PtsValue();
            }

            PreprocMetaDataDescriptor->native_time = native_time;
            PreprocFullMetaDataDescriptor->encode_coordinator_metadata.encoded_time = encoded_time;

            SE_DEBUG(group_encoder_video_preproc, "Stream 0x%p PTS: In %llu Out %llu %llu fmt:%d %d\n",
                     Encoder.EncodeStream,
                     InputMetaDataDescriptor->native_time,
                     PreprocMetaDataDescriptor->native_time,
                     PreprocFullMetaDataDescriptor->encode_coordinator_metadata.encoded_time,
                     PreprocMetaDataDescriptor->native_time_format,
                     PreprocFullMetaDataDescriptor->encode_coordinator_metadata.encoded_time_format);

            // Output frame
            Status = Output(OutputBuffer);
            if (Status != PreprocNoError)
            {
                SE_ERROR("Stream 0x%p Failed to output frame\n", Encoder.EncodeStream);
                return PreprocError;
            }
        }
        else
        {
            // Silently discard frame
            OutputBuffer->DecrementReferenceCount(IdentifierEncoderPreprocessor);
            return PreprocNoError;
        }
    }
    else
    {
        if (!SecondField)
        {
            // Silently discard frame
            OutputBuffer->DecrementReferenceCount(IdentifierEncoderPreprocessor);
        }
    }

    return PreprocNoError;
}

PreprocStatus_t Preproc_Video_Blitter_c::Input(Buffer_t   Buffer)
{
    PreprocStatus_t        Status;
    SE_DEBUG(group_encoder_video_preproc, "Stream 0x%p\n", Encoder.EncodeStream);

    // Dump preproc input buffer via st_relay
    Buffer->DumpViaRelay(ST_RELAY_TYPE_ENCODER_VIDEO_PREPROC_INPUT + mRelayfsIndex, ST_RELAY_SOURCE_SE);

    // Call the Base Class implementation to update statistics
    Status = Preproc_Video_c::Input(Buffer);
    if (Status != PreprocNoError)
    {
        PREPROC_ERROR_RUNNING("Stream 0x%p Failed to call parent class, Status = %u\n", Encoder.EncodeStream, Status);
        return PREPROC_STATUS_RUNNING(Status);
    }

    EntryTrace(InputMetaDataDescriptor, &PreprocCtrl);

    // Check for valid blitter device
    if (blitter == NULL)
    {
        PREPROC_ERROR_RUNNING("Stream 0x%p Blitter device is not valid\n", Encoder.EncodeStream);
        PreprocFrameBuffer->DecrementReferenceCount(IdentifierEncoderPreprocessor);
        return PREPROC_STATUS_RUNNING(PreprocError);
    }

    // Generate buffer discontinuity if needed
    if (PreprocDiscontinuity &&
        !((PreprocDiscontinuity & STM_SE_DISCONTINUITY_EOS) && (InputBufferSize != 0)))
    {
        // Insert buffer discontinuity
        Status = GenerateBufferDiscontinuity(PreprocDiscontinuity);
        if (Status != PreprocNoError)
        {
            PREPROC_ERROR_RUNNING("Stream 0x%p Failed to insert discontinuity, Status = %u\n", Encoder.EncodeStream, Status);
            PreprocFrameBuffer->DecrementReferenceCount(IdentifierEncoderPreprocessor);
            return PREPROC_STATUS_RUNNING(Status);
        }

        // Silently discard frame for null buffer input
        if (InputBufferSize == 0)
        {
            if (PreprocDiscontinuity & STM_SE_DISCONTINUITY_EOS)
            {
                ExitTrace(InputMetaDataDescriptor, &PreprocCtrl);
            }
            PreprocFrameBuffer->DecrementReferenceCount(IdentifierEncoderPreprocessor);
            return PreprocNoError;
        }
    }

    // First field/frame is processed
    SE_DEBUG(group_encoder_video_preproc, "Stream 0x%p Processing first field/frame\n", Encoder.EncodeStream);
    Status = ProcessField(Buffer, PreprocFrameBuffer, false);
    if (Status != PreprocNoError)
    {
        PREPROC_ERROR_RUNNING("Stream 0x%p Failed to process first field, Status = %u\n", Encoder.EncodeStream, Status);
        return PREPROC_STATUS_RUNNING(Status);
    }

    // If second field is processed
    if ((PreprocCtrl.EnableDeInterlacer != STM_SE_CTRL_VALUE_DISAPPLY) &&
        (InputMetaDataDescriptor->video.video_parameters.scan_type == STM_SE_SCAN_TYPE_INTERLACED) &&
        (InputMetaDataDescriptor->video.picture_structure == STM_SE_PICTURE_STRUCTURE_FRAME))
    {
        SE_DEBUG(group_encoder_video_preproc, "Stream 0x%p Processing second field\n", Encoder.EncodeStream);
        Status = ProcessField(Buffer, PreprocFrameBuffer2, true);
        if (Status != PreprocNoError)
        {
            PREPROC_ERROR_RUNNING("Stream 0x%p Failed to process second field, Status = %u\n", Encoder.EncodeStream, Status);
            return PREPROC_STATUS_RUNNING(Status);
        }
    }

    // Special case to ensure that valid buffer is processed before EOS buffer is generated
    if ((PreprocDiscontinuity & STM_SE_DISCONTINUITY_EOS) && (InputBufferSize != 0))
    {
        // Insert buffer EOS
        Status = GenerateBufferDiscontinuity(PreprocDiscontinuity);
        if (Status != PreprocNoError)
        {
            PREPROC_ERROR_RUNNING("Stream 0x%p Failed to insert EOS discontinuity, Status = %u\n", Encoder.EncodeStream, Status);
            return PREPROC_STATUS_RUNNING(Status);
        }
        ExitTrace(InputMetaDataDescriptor, &PreprocCtrl);
    }

    return PreprocNoError;
}

PreprocStatus_t Preproc_Video_Blitter_c::GetControl(stm_se_ctrl_t  Control,
                                                    void          *Data)
{
    return Preproc_Video_c::GetControl(Control, Data);
}

PreprocStatus_t Preproc_Video_Blitter_c::GetCompoundControl(stm_se_ctrl_t  Control,
                                                            void          *Data)
{
    return Preproc_Video_c::GetCompoundControl(Control, Data);
}

PreprocStatus_t Preproc_Video_Blitter_c::SetControl(stm_se_ctrl_t  Control,
                                                    const void    *Data)
{
    switch (Control)
    {
    default:
        // Cannot trace this as error because it can be a valid control for another object
        SE_DEBUG(group_encoder_video_preproc, "Stream 0x%p Not match preproc video blitter control %u\n", Encoder.EncodeStream, Control);
        break;
    }

    return Preproc_Video_c::SetControl(Control, Data);
}

PreprocStatus_t Preproc_Video_Blitter_c::SetCompoundControl(stm_se_ctrl_t  Control,
                                                            const void    *Data)
{
    switch (Control)
    {
    default:
        // Cannot trace this as error because it can be a valid control for another object
        SE_DEBUG(group_encoder_video_preproc, "Stream 0x%p Not match preproc video blitter control %u\n", Encoder.EncodeStream, Control);
        break;
    }

    return Preproc_Video_c::SetCompoundControl(Control, Data);
}

PreprocStatus_t Preproc_Video_Blitter_c::InjectDiscontinuity(stm_se_discontinuity_t  Discontinuity)
{
    PreprocStatus_t Status;
    SE_DEBUG(group_encoder_video_preproc, "Stream 0x%p\n", Encoder.EncodeStream);

    Status = Preproc_Base_c::InjectDiscontinuity(Discontinuity);
    if (Status != PreprocNoError)
    {
        PREPROC_ERROR_RUNNING("Stream 0x%p Failed to inject discontinuity\n", Encoder.EncodeStream);
        return PREPROC_STATUS_RUNNING(Status);
    }

    if (Discontinuity)
    {
        // Generate Discontinuity Buffer
        Status = GenerateBufferDiscontinuity(Discontinuity);
        if (Status != PreprocNoError)
        {
            PREPROC_ERROR_RUNNING("Stream 0x%p Failed to insert discontinuity\n", Encoder.EncodeStream);
            return PREPROC_STATUS_RUNNING(Status);
        }
    }

    ExitTrace(InputMetaDataDescriptor, &PreprocCtrl);

    return PreprocNoError;
}

PreprocVideoCaps_t Preproc_Video_Blitter_c::GetCapabilities()
{
    return PreprocCaps;
}
