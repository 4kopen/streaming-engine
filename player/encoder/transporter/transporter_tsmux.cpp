/************************************************************************
Copyright (C) 2003-2014 STMicroelectronics. All Rights Reserved.

This file is part of the Streaming Engine.

Streaming Engine is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License version 2 as
published by the Free Software Foundation.

Streaming Engine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with Streaming Engine; see the file COPYING.  If not, write to the Free
Software Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307,
USA.

The Streaming Engine Library may alternatively be licensed under a
proprietary license from ST.
************************************************************************/

#include "transporter_tsmux.h"

#undef TRACE_TAG
#define TRACE_TAG "Transporter_TSMux_c"


int transporter_tsmux_release_buffer(stm_object_h         src_object,
                                     struct stm_data_block *block_list,
                                     uint32_t               block_count)
{
    (void)block_count; // warning removal

    Transporter_TSMux_c *Transporter_TSMux = (Transporter_TSMux_c *)src_object;
    return Transporter_TSMux->ReleaseBuffer(block_list);
}

stm_te_async_data_interface_src_t TSMuxAsyncDataInterface =
{
    transporter_tsmux_release_buffer,
};

//{{{  Constructor
//{{{  doxynote
/// \brief                      Set Initial state if success
Transporter_TSMux_c::Transporter_TSMux_c()
    : TSMuxHandle(NULL)
    , TSMuxBufferRing()
    , TSMuxInterface()
    , mTsmuxConnectionLock()
{
    // TODO(pht) move to a FinalizeInit method
    TSMuxBufferRing = RingGeneric_c::New(TRANSPORTER_STREAM_MAX_CODED_BUFFERS,
                                         "Transporter_TSMux_c::TSMuxBufferRing");
    if (TSMuxBufferRing == NULL)
    {
        SE_ERROR("Stream 0x%p TSMuxBufferRing alloc failed\n", Encoder.EncodeStream);
        InitializationStatus = TransporterError;
        return;
    }

    OS_InitializeMutex(&mTsmuxConnectionLock);
    SetComponentState(ComponentRunning);
}
//}}}

//{{{  Destructor
//{{{  doxynote
/// \brief                      Try to disconnect if not done
Transporter_TSMux_c::~Transporter_TSMux_c()
{
    Halt();
}
//}}}

TransporterStatus_t Transporter_TSMux_c::Halt()
{
    Transporter_Base_c::Halt();

    RemoveConnection(TSMuxHandle);

    //
    // Purge anything left on the Ring
    //
    if (TSMuxBufferRing)
    {
        PurgeBufferRing();
        delete TSMuxBufferRing;
        TSMuxBufferRing = NULL;
    }

    OS_TerminateMutex(&mTsmuxConnectionLock);
    return TransporterNoError;
}

//{{{  Probe
//{{{  doxynote
/// \brief                      Determine if this Class can connect to the Sink Object
/// \param Sink                 SinkHandle : The object to connect to
/// \return                     Success or fail
TransporterStatus_t Transporter_TSMux_c::Probe(stm_object_h  Sink)
{
    int           Status;
    stm_object_h  SinkType;
    char          TagTypeName [STM_REGISTRY_MAX_TAG_SIZE];
    int32_t       Size;
    stm_te_async_data_interface_sink_t            Interface;

    SE_DEBUG(group_encoder_transporter, "\n");

    // Check if the sink object supports the STM_TE_ASYNC_DATA_INTERFACE interface
    Status = stm_registry_get_object_type(Sink, &SinkType);
    if (Status != 0)
    {
        SE_ERROR("stm_registry_get_object_type(%p, &%p) failed (%d)\n", Sink, SinkType, Status);
        return TransporterError;
    }

    Status = stm_registry_get_attribute(SinkType,
                                        STM_TE_ASYNC_DATA_INTERFACE,
                                        TagTypeName,
                                        sizeof(Interface),
                                        &Interface,
                                        (int *)&Size);
    if ((Status) || (Size != sizeof(Interface)))
    {
        // no SE_ERROR since function used as test-check feature
        SE_DEBUG(group_encoder_transporter, "stm_registry_get_attribute() failed (%d)\n", Status);
        return TransporterError;
    }

    return TransporterNoError; // We can connect to this object
}
//}}}

//{{{  CreateConnection
//{{{  doxynote
/// \brief                      Connect to a TSMux Object for direct tunnelled Multiplexing
/// \param Sink                 Handle : sink object to connect to
/// \return                     Success or fail
TransporterStatus_t Transporter_TSMux_c::CreateConnection(stm_object_h  Sink)
{
    AssertComponentState(ComponentRunning);
    SE_INFO(group_encoder_transporter, "Stream 0x%p\n", Encoder.EncodeStream);
    int           Status;
    stm_object_h  SinkType;
    char          TagTypeName [STM_REGISTRY_MAX_TAG_SIZE];
    int32_t       Size;

    if (TSMuxHandle)
    {
        SE_ERROR("Stream 0x%p TSMux already connected on %p, refusing connection on %p\n", Encoder.EncodeStream, TSMuxHandle, Sink);
        return TransporterError;
    }

    // Check sink object support STM_TE_ASYNC_DATA_INTERFACE interface
    Status = stm_registry_get_object_type(Sink, &SinkType);
    if (Status != 0)
    {
        SE_ERROR("Stream 0x%p stm_registry_get_object_type(%p, &%p) failed (%d)\n", Encoder.EncodeStream, Sink, SinkType, Status);
        return TransporterError;
    }

    Status = stm_registry_get_attribute(SinkType,
                                        STM_TE_ASYNC_DATA_INTERFACE,
                                        TagTypeName,
                                        sizeof(TSMuxInterface),
                                        &TSMuxInterface,
                                        (int *)&Size);
    if ((Status) || (Size != sizeof(TSMuxInterface)))
    {
        // no SE_ERROR since function used as test-check feature
        SE_DEBUG(group_encoder_transporter, "Stream 0x%p stm_registry_get_attribute() failed (%d)\n", Encoder.EncodeStream, Status);
        return TransporterError;
    }

    // call the sink interface's connect handler to connect the consumer
    SE_DEBUG(group_encoder_transporter, "Stream 0x%p Connecting to the TSMuxInterface\n", Encoder.EncodeStream);
    Status = TSMuxInterface.connect((stm_object_h)this,    // This class is the handle for the Source
                                    Sink,
                                    &TSMuxAsyncDataInterface);
    if (Status)
    {
        // Connection fails : Free OS objects
        SE_ERROR("Stream 0x%p TSMuxInterface.connect(%p) failed; status:%d\n", Encoder.EncodeStream, Sink, Status);
        return TransporterError;
    }

    SE_DEBUG(group_encoder_transporter, "Stream 0x%p Successful connection to TSMuxInterface\n", Encoder.EncodeStream);

    // Save sink object for communication with the TSMux
    TSMuxHandle = Sink;
    return TransporterNoError;
}
//}}}

//{{{  RemoveConnection
//{{{  doxynote
/// \brief                      Disconnect from TSMux Sink port (if connected)
/// \return                     Success or fail
TransporterStatus_t Transporter_TSMux_c::RemoveConnection(stm_object_h  Sink)
{
    SE_DEBUG(group_encoder_transporter, "Stream 0x%p\n", Encoder.EncodeStream);

    // Check that Sink object correspond the connected one
    if (TSMuxHandle != Sink)
    {
        SE_ERROR("Stream 0x%p Sink (%p) does not match TSMuxHandle (%p)\n", Encoder.EncodeStream, Sink, TSMuxHandle);
        return TransporterError;
    }

    if (TSMuxHandle != 0)
    {
        // Mutex to protect ongoing buffer transmission in from function input called by another thread.
        // Not perfect, one can unconnect, so buffer are free (decrement ref count), while still under use
        // in TSMUX. In CDI, Tsmux is supposed to be stopped first, so pb cannot happen.
        OS_LockMutex(&mTsmuxConnectionLock);

        // Reset our Sink Handle to prevent it being re-used
        TSMuxHandle = NULL;

        // call the sink interface's disconnect handler to disconnect the consumer
        SE_DEBUG(group_encoder_transporter, "Stream 0x%p Disconnecting from TSMux Sink\n", Encoder.EncodeStream);
        int Status = TSMuxInterface.disconnect((stm_object_h)this, Sink);
        if (Status)
        {
            SE_ERROR("Stream 0x%p TSMuxInterface.disconnect(%p) failed\n", Encoder.EncodeStream, Sink);
            // We have removed our local handle - and we are actively in the process of shutdown.
            // We simply continue at this point (and quite likely purge our BufferRing)
        }
        else
        {
            SE_DEBUG(group_encoder_transporter, "Stream 0x%p Successfully disconnected from TSMux Sink\n", Encoder.EncodeStream);
        }

        // On Disconnect - TSMux will release all of its buffers.
        // If anything is left, there is an error and it needs to be purged.
        PurgeBufferRing();
        OS_UnLockMutex(&mTsmuxConnectionLock);
    }

    return TransporterNoError;
}
//}}}

//{{{  Input
//{{{  doxynote
/// \brief                      Transfer a buffer into the Mux
/// \return                     Success or fail
TransporterStatus_t Transporter_TSMux_c::Input(Buffer_t   Buffer)
{
    TransporterStatus_t                 TransporterStatus;
    // Retrieving Metadata
    __stm_se_frame_metadata_t          *OutputMetaDataDescriptor;
    stm_se_compressed_frame_metadata_t *CompressedFrameMetaData;
    // Retrieving Buffer data
    void                               *FrameVirtualAddress;
    uint32_t                            FrameSize;
    struct stm_data_block               BlockList;
    uint32_t                            DataBlocks;

    AssertComponentState(ComponentRunning);

    // to ensure one is not unconnecting TSMUX while sending a buffer
    OS_LockMutex(&mTsmuxConnectionLock);
    if (!TSMuxHandle)
    {
        // Buffer can be returned immediately
        SE_DEBUG(group_encoder_transporter, "Stream 0x%p No connection established to TSMux\n", Encoder.EncodeStream);
        OS_UnLockMutex(&mTsmuxConnectionLock);
        return TransporterNoError;
    }

    SE_VERBOSE(group_encoder_transporter, "Stream 0x%p\n", Encoder.EncodeStream);

    // Call the Base Class implementation for common processing
    TransporterStatus = Transporter_Base_c::Input(Buffer);
    if (TransporterStatus != TransporterNoError)
    {
        SE_ERROR("Stream 0x%p Error returned by base class Input method\n", Encoder.EncodeStream);
        OS_UnLockMutex(&mTsmuxConnectionLock);
        return TransporterStatus;
    }

    // Get the MetaData reference needed by TSMux interface
    Buffer->ObtainMetaDataReference(OutputMetaDataBufferType, (void **)(&OutputMetaDataDescriptor));
    SE_ASSERT(OutputMetaDataDescriptor != NULL);

    CompressedFrameMetaData = &OutputMetaDataDescriptor->compressed_frame_metadata;

    SE_DEBUG(group_encoder_transporter, "Stream 0x%p encoding:%d discontinuity:%d systime:%lld ms nattime:0x%llx (%d) enctime:0x%llx (%d)\n",
             Encoder.EncodeStream,
             CompressedFrameMetaData->encoding,
             CompressedFrameMetaData->discontinuity,
             CompressedFrameMetaData->system_time,
             CompressedFrameMetaData->native_time,
             CompressedFrameMetaData->native_time_format,
             CompressedFrameMetaData->encoded_time,
             CompressedFrameMetaData->encoded_time_format
            );

    // Fill in the Block List
    Buffer->ObtainDataReference(NULL, &FrameSize, &FrameVirtualAddress,  CachedAddress);
    if (FrameVirtualAddress == NULL)
    {
        SE_ERROR("Stream 0x%p Unable to retrieve buffer address\n", Encoder.EncodeStream);
        OS_UnLockMutex(&mTsmuxConnectionLock);
        return TransporterError;
    }

    EncoderSequenceNumber_t  *SequenceNumberStructure;
    const EncoderBufferTypes_t *BufferTypes = Encoder.Encoder->GetBufferTypes();
    Buffer->ObtainMetaDataReference(BufferTypes->MetaDataSequenceNumberType, (void **)(&SequenceNumberStructure));
    SE_ASSERT(SequenceNumberStructure != NULL);

    // Hold a reference to this buffer while it is being used by the TSMux.
    // We will decrement this value when the TXMux calls ReleaseBuffer
    Buffer->IncrementReferenceCount(IdentifierEncoderTransporter);

    RingStatus_t RingStatus = TSMuxBufferRing->Insert((uintptr_t) Buffer);
    if (RingStatus != RingNoError)
    {
        SE_ERROR("Stream 0x%p Failed to insert the buffer onto the local ring rs:%d\n", Encoder.EncodeStream, RingStatus);
        // Abandon the reference that we were going to use to keep the Buffer on the Ring.
        Buffer->DecrementReferenceCount(IdentifierEncoderTransporter);
        OS_LockMutex(&mTsmuxConnectionLock);
        return TransporterError;
    }
    TraceRing("Input: INSERT", Buffer);

    SE_VERBOSE(group_encoder_transporter, "Stream 0x%p buffer:%p framesize:%d\n", Encoder.EncodeStream, Buffer, FrameSize);
    BlockList.data_addr = FrameVirtualAddress;
    BlockList.len       = FrameSize;
    BlockList.next      = NULL;
    int Status = TSMuxInterface.queue_buffer(
                     TSMuxHandle,
                     CompressedFrameMetaData,
                     &BlockList,
                     1,
                     &DataBlocks);

    SE_VERBOSE(group_se_pipeline, "Stream 0x%x - %d - #%lld PTS=%lld Mux=%llu\n",
               SequenceNumberStructure->StreamUniqueIdentifier,
               SequenceNumberStructure->StreamTypeIdentifier,
               SequenceNumberStructure->FrameCounter,
               SequenceNumberStructure->PTS,
               OS_GetTimeInMicroSeconds()
              );

    if (Status)
    {
        Buffer_t BufferOut;
        RingStatus = TSMuxBufferRing->ExtractLastInserted((uintptr_t *)(&BufferOut), RING_NONE_BLOCKING);
        if (RingStatus == RingNoError)
        {
            TraceRing("Input: EXTRACT LAST", BufferOut);
            BufferOut->DecrementReferenceCount(IdentifierEncoderTransporter);
        }

        SE_ERROR("Stream 0x%p Failed to queue buffer:%p with TSMux:%p (status:%d - %d)\n", Encoder.EncodeStream, Buffer, TSMuxHandle, Status, RingStatus);
        Encoder.EncodeStream->EncodeStreamStatistics().TsMuxQueueError++;
        OS_UnLockMutex(&mTsmuxConnectionLock);
        return TransporterError;
    }

    Encoder.EncodeStream->EncodeStreamStatistics().BufferCountFromTransporter++;
    if (FrameSize == 0)
    {
        Encoder.EncodeStream->EncodeStreamStatistics().NullSizeBufferCountFromTransporter++;
    }

    OS_UnLockMutex(&mTsmuxConnectionLock);
    return TransporterNoError;
}
//}}}

//{{{  ReleaseBuffer
//{{{  doxynote
/// \brief                      The callback from the TSMux to indicate a buffer is free to be reused
/// \return                     0 Success or 1 fail (stm_te status compatible)
int Transporter_TSMux_c::ReleaseBuffer(struct stm_data_block *block_list)
{
    Buffer_t                           Buffer;
    void                              *FrameVirtualAddress;
    uint32_t                           FrameSize;
    RingStatus_t RingStatus = TSMuxBufferRing->Extract((uintptr_t *)(&Buffer), RING_NONE_BLOCKING);
    if (RingStatus == RingNoError)
    {
        TraceRing("ReleaseBuffer: EXTRACT", Buffer);
        if (Buffer != NULL)
        {
            // Verify that we are releasing the correct buffer...
            Buffer->ObtainDataReference(NULL, &FrameSize, &FrameVirtualAddress,  CachedAddress);
            if (FrameVirtualAddress == NULL)
            {
                SE_ERROR("Stream 0x%p Unable to retrieve buffer address\n", Encoder.EncodeStream);
                Encoder.EncodeStream->EncodeStreamStatistics().TsMuxTransporterBufferAddressError++;
                return 1;
            }

            if ((FrameVirtualAddress != block_list->data_addr) || (FrameSize != block_list->len))
            {
                SE_ERROR("Stream 0x%p buffer for release does not match one requested\n"
                         " Callback provided: %p (%d)\n"
                         " Our Ring provided: %p (%d)\n",
                         Encoder.EncodeStream,
                         block_list->data_addr, block_list->len,
                         FrameVirtualAddress, FrameSize);
                Encoder.EncodeStream->EncodeStreamStatistics().TsMuxTransporterUnexpectedReleasedBufferError++;
            }
            SE_VERBOSE(group_encoder_transporter, "Stream 0x%p buffer:%p framesize:%d\n",
                       Encoder.EncodeStream, Buffer, FrameSize);

            // Regardless of whether this is the correct buffer - we must now release...
            // As it has been removed from the Ring
            Buffer->DecrementReferenceCount(IdentifierEncoderTransporter);
            Encoder.EncodeStream->EncodeStreamStatistics().ReleaseBufferCountFromTsMuxTransporter++;
            return 0;
        }
    }

    SE_ERROR("Stream 0x%p Failed to match a buffer for release rs:%d\n", Encoder.EncodeStream, RingStatus);
    Encoder.EncodeStream->EncodeStreamStatistics().TsMuxTransporterRingExtractError++;
    return 1;
}
//}}}

//{{{  PurgeBufferRing
//{{{  doxynote
/// \brief                      This private internal function is used to ensure that
///                             our TSMuxBufferRing is completely emptied.
/// \return                     Success or fail
TransporterStatus_t Transporter_TSMux_c::PurgeBufferRing()
{
    Buffer_t Buffer;

    while (TSMuxBufferRing->NonEmpty())
    {
        RingStatus_t RingStatus = TSMuxBufferRing->Extract((uintptr_t *)(&Buffer), RING_NONE_BLOCKING);

        if (RingStatus == RingNoError)
        {
            TraceRing("PurgeBufferRing: EXTRACT", Buffer);
            if (Buffer != NULL)
            {
                // release buffer
                SE_WARNING("Stream 0x%p purging remaining buffer: %p\n", Encoder.EncodeStream, Buffer);
                Buffer->Dump(DumpBufferStates);
                Buffer->DecrementReferenceCount(IdentifierEncoderTransporter);
            }
        }
    }

    return TransporterNoError;
}
//}}}

// Debug traces helper function
void Transporter_TSMux_c::TraceRing(const char *ActionStr, Buffer_t Buffer)
{
    if (Buffer == NULL)
    {
        SE_DEBUG(group_encoder_transporter, "Stream 0x%p %s NULL => count %u\n",
                 Encoder.EncodeStream, ActionStr, TSMuxBufferRing->NbOfEntries());
    }
    else
    {
        __stm_se_frame_metadata_t *MetaDataDescriptor;
        Buffer->ObtainMetaDataReference(OutputMetaDataBufferType, (void **)(&MetaDataDescriptor));
        SE_ASSERT(MetaDataDescriptor != NULL);

        unsigned int BufferSize;
        Buffer->ObtainDataReference(NULL, &BufferSize, NULL);

        if ((BufferSize == 0) || (MetaDataDescriptor->compressed_frame_metadata.discontinuity & STM_SE_DISCONTINUITY_EOS))
        {
            SE_DEBUG(group_encoder_transporter, "Stream 0x%p %s EOS 0x%p size %u discont 0x%x => count %u\n",
                     Encoder.EncodeStream, ActionStr, Buffer, BufferSize, MetaDataDescriptor->compressed_frame_metadata.discontinuity, TSMuxBufferRing->NbOfEntries());
        }
        else
        {
            SE_VERBOSE(group_encoder_transporter, "Stream 0x%p %s 0x%p size %u discont 0x%x => count %u\n",
                       Encoder.EncodeStream, ActionStr, Buffer, BufferSize, MetaDataDescriptor->compressed_frame_metadata.discontinuity, TSMuxBufferRing->NbOfEntries());
        }
    }
}
