/************************************************************************
Copyright (C) 2003-2014 STMicroelectronics. All Rights Reserved.

This file is part of the Streaming Engine.

Streaming Engine is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License version 2 as
published by the Free Software Foundation.

Streaming Engine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with Streaming Engine; see the file COPYING.  If not, write to the Free
Software Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307,
USA.

The Streaming Engine Library may alternatively be licensed under a
proprietary license from ST.
************************************************************************/

#include "st_relayfs_se.h"
#include "frame_parser_audio.h"

#undef TRACE_TAG
#define TRACE_TAG "FrameParser_Audio_c"

///////////////////////////////////////////////////////////////////////////
///
///     Constructor
///
FrameParser_Audio_c::FrameParser_Audio_c()
    : ParsedAudioParameters(NULL)
    , mPtsExtrapolation()
    , UpdateStreamParameters(false)
    , RelayfsIndex()
{
    SetGroupTrace(group_frameparser_audio);
    mPtsExtrapolation.ResetExtrapolator();
    RelayfsIndex = st_relayfs_getindex_fortype_se(ST_RELAY_TYPE_CODED_AUDIO_BUFFER);
}

////////////////////////////////////////////////////////////////////////////
///      Destructor
////
FrameParser_Audio_c::~FrameParser_Audio_c()
{
    st_relayfs_freeindex_fortype_se(ST_RELAY_TYPE_CODED_AUDIO_BUFFER, RelayfsIndex);
}

// /////////////////////////////////////////////////////////////////////////
//
//      Method to connect to neighbor
//

FrameParserStatus_t   FrameParser_Audio_c::Connect(Port_c *Port)
{
    FrameParserStatus_t Status;
    //
    // First allow the base class to perform it's operations,
    // as we operate on the buffer pool obtained by it.
    //
    Status  = FrameParser_Base_c::Connect(Port);

    if (Status != FrameParserNoError)
    {
        return Status;
    }

    return FrameParserNoError;
}

// /////////////////////////////////////////////////////////////////////////
//
//  The input function perform audio specific operations
//

FrameParserStatus_t   FrameParser_Audio_c::Input(Buffer_t         CodedBuffer)
{
    FrameParserStatus_t Status;
    //
    // Are we allowed in here
    //
    AssertComponentState(ComponentRunning);
    //
    // Initialize context pointers
    //
    ParsedAudioParameters   = NULL;
    //
    // First perform base operations
    //
    Status  = FrameParser_Base_c::Input(CodedBuffer);
    if (Status != FrameParserNoError)
    {
        return Status;
    }

    //Audio Descriptor info fill in ParsedFrameParameters
    if (CodedFrameParameters->ADMetaData.ADInfoAvailable)
    {
        memcpy(&ParsedFrameParameters->ADMetaData, &CodedFrameParameters->ADMetaData, sizeof(ADMetaData_t));
    }

    // Allow st_relay capture of the audio coded buffers
    CodedBuffer->DumpViaRelay(ST_RELAY_TYPE_CODED_AUDIO_BUFFER + RelayfsIndex, ST_RELAY_SOURCE_SE);

    //
    // Attach  ParsedAudioParametersType MetaData
    //
    BufferStatus_t BufferStatus = Buffer->AttachMetaData(Player->MetaDataParsedAudioParametersType);
    SE_ASSERT(BufferStatus == BufferNoError);

    //
    // Obtain audio specific pointers to data associated with the buffer.
    //
    Buffer->ObtainMetaDataReference(Player->MetaDataParsedAudioParametersType, (void **)(&ParsedAudioParameters));
    SE_ASSERT(ParsedAudioParameters != NULL);

    memset(ParsedAudioParameters, 0, sizeof(ParsedAudioParameters_t));

    //
    // Now execute the processing chain for a buffer
    //
    return ProcessBuffer();
}

// /////////////////////////////////////////////////////////////////////////
///
///     \brief Common portion of read headers
///
///     Handle invalidation of what we knew about time on a discontinuity
///
///     \return Frame parser status code, FrameParserNoError indicates success.
///

FrameParserStatus_t   FrameParser_Audio_c::ReadHeaders()
{
    if (FirstDecodeAfterInputJump)
    {
        mPtsExtrapolation.ResetExtrapolator();
    }

    return FrameParserNoError;
}

////////////////////////////////////////////////////////////////////////////
///
/// Ensure that current frame has a suitable playback time.
/// If frame PTS is not valid then extrapolation will be done based on previous PTS,
/// SampleCount and SamplingFrequency.
/// It also take cares if PTS is valid but wrong. In this case if wrong PTS falls
/// between the correction range then it will also be corrected.
///

void FrameParser_Audio_c::HandleCurrentFramePts(unsigned int SampleCount, unsigned int SamplingFrequency)
{
    ParsedFrameParameters->PTS = mPtsExtrapolation.GetTimeStamp(
                                     ParsedFrameParameters->PTS, SampleCount, SamplingFrequency);
}

////////////////////////////////////////////////////////////////////////////
///
/// Handle the management of stream parameters in a uniform way.
///
/// The frame parser has an unexpected requirement imposed upon it; every
/// frame must have the stream parameters attached to it (in case frames
/// are discarded between parsing and decoding). This method handles the
/// emission of stream parameters in a uniform way.
///
/// It should be used by any (audio) frame parser that supports stream
/// parameters.
///
void FrameParser_Audio_c::HandleUpdateStreamParameters()
{
    void *StreamParameters;

    if (NULL == StreamParametersBuffer)
    {
        SE_ERROR("Cannot handle NULL stream parameters\n");
        return;
    }

    //
    // If this frame contains a new set of stream parameters mark this as such for the players
    // attention.
    //

    if (UpdateStreamParameters)
    {
        // the framework automatically zeros this structure (i.e. we need not ever set to false)
        ParsedFrameParameters->NewStreamParameters = true;
        UpdateStreamParameters = false;
    }

    //
    // Unconditionally send the stream parameters down the chain.
    //
    StreamParametersBuffer->ObtainDataReference(NULL, NULL, &StreamParameters);
    SE_ASSERT(StreamParameters != NULL);  // not supposed to be empty

    ParsedFrameParameters->SizeofStreamParameterStructure = FrameParametersDescriptor->FixedSize;
    ParsedFrameParameters->StreamParameterStructure = StreamParameters;
    Buffer->AttachBuffer(StreamParametersBuffer);
}

