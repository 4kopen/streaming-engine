/************************************************************************
Copyright (C) 2003-2014 STMicroelectronics. All Rights Reserved.

This file is part of the Streaming Engine.

Streaming Engine is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License version 2 as
published by the Free Software Foundation.

Streaming Engine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with Streaming Engine; see the file COPYING.  If not, write to the Free
Software Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307,
USA.

The Streaming Engine Library may alternatively be licensed under a
proprietary license from ST.
************************************************************************/

#ifndef H_FRAME_PARSER_AUDIO_RMA
#define H_FRAME_PARSER_AUDIO_RMA

#include "rma_audio.h"
#include "frame_parser_audio.h"

#undef TRACE_TAG
#define TRACE_TAG "FrameParser_AudioRma_c"

/// Frame parser for Real Media Rma audio
class FrameParser_AudioRma_c : public FrameParser_Audio_c
{
public:
    FrameParser_AudioRma_c();
    ~FrameParser_AudioRma_c();

    // FrameParser class functions

    FrameParserStatus_t   Connect(Port_c *Port);

    // Stream specific functions

    FrameParserStatus_t   ReadHeaders();

    FrameParserStatus_t   ResetReferenceFrameList();
    FrameParserStatus_t   PurgeQueuedPostDecodeParameterSettings();
    FrameParserStatus_t   PrepareReferenceFrameList();
    FrameParserStatus_t   ProcessQueuedPostDecodeParameterSettings();
    FrameParserStatus_t   GeneratePostDecodeParameterSettings();
    FrameParserStatus_t   UpdateReferenceFrameList();

    FrameParserStatus_t   ProcessReverseDecodeUnsatisfiedReferenceStack();
    FrameParserStatus_t   ProcessReverseDecodeStack();
    FrameParserStatus_t   PurgeReverseDecodeUnsatisfiedReferenceStack();
    FrameParserStatus_t   PurgeReverseDecodeStack();
    FrameParserStatus_t   TestForTrickModeFrameDrop();

private:
    RmaAudioParsedFrameHeader_t   ParsedFrameHeader;

    RmaAudioStreamParameters_t   *StreamParameters;
    RmaAudioStreamParameters_t    CurrentStreamParameters;
    RmaAudioFrameParameters_t    *FrameParameters;

    bool                          StreamFormatInfoValid;
    unsigned int                  DataOffset;

    FrameParserStatus_t  ReadStreamParameters();

    DISALLOW_COPY_AND_ASSIGN(FrameParser_AudioRma_c);
};

#endif
