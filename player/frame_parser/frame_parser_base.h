/************************************************************************
Copyright (C) 2003-2014 STMicroelectronics. All Rights Reserved.

This file is part of the Streaming Engine.

Streaming Engine is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License version 2 as
published by the Free Software Foundation.

Streaming Engine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with Streaming Engine; see the file COPYING.  If not, write to the Free
Software Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307,
USA.

The Streaming Engine Library may alternatively be licensed under a
proprietary license from ST.
************************************************************************/

#ifndef H_FRAME_PARSER_BASE
#define H_FRAME_PARSER_BASE

#include "bitstream_class.h"
#include "player.h"
#include "player_generic.h"
#include "player_playback.h"
#include "player_stream.h"
#include "parse_to_decode_edge.h"
#include "timestamp_extrapolator.h"
#include "shared_ptr.h"

#undef TRACE_TAG
#define TRACE_TAG "FrameParser_Base_c"

// /////////////////////////////////////////////////////////////////////////

#define MarkerBit( v )      if( Bits.Get(1) != (v) )                                    \
                {                                               \
                    SE_ERROR("Invalid marker bit value\n"); \
                    return FrameParserHeaderSyntaxError;                            \
                }

#define MarkerBits( n, v )  if( Bits.Get((n)) != (v) )                                  \
                {                                               \
                    SE_ERROR("Invalid marker bits value\n");    \
                    return FrameParserHeaderSyntaxError;                            \
                }

// Cleanup copies of the marker bit tests, that release a temporarily held buffer

#define MarkerBitClean( v ) if( Bits.Get(1) != (v) )                                    \
                {                                               \
                    SE_ERROR("Invalid marker bit value\n"); \
                    return FrameParserHeaderSyntaxError;                            \
                }

#define MarkerBitsClean( n, v ) if( Bits.Get((n)) != (v) )                                  \
                {                                               \
                    SE_ERROR("Invalid marker bits value\n");    \
                    return FrameParserHeaderSyntaxError;                            \
                }


typedef struct FrameParserConfiguration_s
{
    const char               *FrameParserName;
    unsigned int              MaxUserDataBlocks;

    unsigned int              StreamParametersCount;
    BufferDataDescriptor_t   *StreamParametersDescriptor;

    unsigned int              FrameParametersCount;
    BufferDataDescriptor_t   *FrameParametersDescriptor;

    unsigned int              MaxReferenceFrameCount;   // Used in reverse play resource management

    bool                      SupportSmoothReversePlay; // Can we support smooth reverse on this stream

    bool                      InitializeStartCodeList;  // Do we need to extract the start code list from input
} FrameParserConfiguration_t;


class FrameParser_Base_c : public FrameParser_c
{
public:
    FrameParser_Base_c();
    ~FrameParser_Base_c();

    // This variable defines what should be the value of EntryCount for an I frame
    // Can change for different codecs. By default initialse it to zero, and modify it
    // in the respective codec class if required(e.g theora)
    int IFrameEntryCount;

    FrameParserStatus_t   Halt();

    //
    // FrameParser class functions
    //

    FrameParserStatus_t   Connect(Port_c *Port);

    FrameParserStatus_t   Input(Buffer_t  CodedBuffer);

    FrameParserStatus_t   GetNextDecodeFrameIndex(unsigned int  *Index);
    FrameParserStatus_t   SetNextFrameIndices(unsigned int       Value);

    // Class function for default non-implemented functionality

    FrameParserStatus_t   ResetCollatedHeaderState() { return PlayerNotSupported; }

    unsigned int          RequiredPresentationLength(unsigned char       StartCode)
    {
        (void)StartCode; // warning removal
        return 0;
    }
    FrameParserStatus_t   PresentCollatedHeader(unsigned char            StartCode,
                                                unsigned char           *HeaderBytes,
                                                FrameParserHeaderFlag_t *Flags)
    {
        (void)StartCode; // warning removal
        (void)HeaderBytes; // warning removal
        (void)Flags; // warning removal
        return PlayerNotSupported;
    }

    void                IncrementErrorStatistics(FrameParserStatus_t  Status);

    // User data functions

    FrameParserStatus_t ReadUserData(unsigned int UserDataLength, unsigned char *InputBufferData);
    FrameParserStatus_t FillUserDataBuffer();
    FrameParserStatus_t ResetReverseFailureCounter() { return FrameParserNoError; };
    FrameParserStatus_t SetSmoothReverseSupport(bool SmoothReverseSupport) ;

protected:
    OS_Mutex_t                 Lock;
    FrameParserConfiguration_t Configuration;

    SharedPtr_c<BufferPool_c> CodedFrameBufferPool;
    unsigned int              FrameBufferCount;
    Port_c                   *mOutputPort;

    BufferManager_t           BufferManager;

    BufferDataDescriptor_t   *StreamParametersDescriptor;
    BufferType_t              StreamParametersType;
    BufferPool_t              StreamParametersPool;
    Buffer_t                  StreamParametersBuffer;

    BufferDataDescriptor_t   *FrameParametersDescriptor;
    BufferType_t              FrameParametersType;
    BufferPool_t              FrameParametersPool;
    Buffer_t                  FrameParametersBuffer;

    Buffer_t                  Buffer;
    unsigned int              BufferLength;
    unsigned char            *BufferData;
    CodedFrameParameters_t   *CodedFrameParameters;
    ParsedFrameParameters_t  *ParsedFrameParameters;
    StartCodeList_t          *StartCodeList;

    bool                      FirstDecodeAfterInputJump;

    unsigned int              NextDecodeFrameIndex;
    unsigned int              NextDisplayFrameIndex;

    BitStreamClass_c          Bits;
    bool                      FrameToDecode;

    Rational_t                PlaybackSpeed;
    PlayDirection_t           PlaybackDirection;

    stm_se_user_data_t        AccumulatedUserData[STM_SE_MAXIMUM_USER_DATA_BUFFERS];  // used to accumulate user data
    // user data index is reflecting the number of user data to be associated to the parsed picture
    unsigned char             UserDataIndex;

    FrameParserStatus_t   RegisterStreamAndFrameDescriptors();

    FrameParserStatus_t   GetNewStreamParameters(void           **Pointer);
    FrameParserStatus_t   GetNewFrameParameters(void            **Pointer);

    //
    // Extensions to the class that may be overridden by my inheritors
    //

    virtual FrameParserStatus_t   ProcessBuffer();
    virtual FrameParserStatus_t   QueueFrameForDecode();

    //
    // Extensions to the class to be fulfilled by my inheritors, all defaulted to null functions
    //
    // Note: This functions are doxygenated at the end of frame_parser_base.cpp, please keep the
    // doxygen comments up to date.
    //

    virtual FrameParserStatus_t   ReadHeaders() {return FrameParserNoError;}
    virtual FrameParserStatus_t   ProcessQueuedPostDecodeParameterSettings() {return FrameParserNoError;}
    virtual FrameParserStatus_t   PurgeQueuedPostDecodeParameterSettings() {return FrameParserNoError;}
    virtual FrameParserStatus_t   GeneratePostDecodeParameterSettings() {return FrameParserNoError;}

    virtual bool ReadAdditionalUserDataParameters() {return false;}

private:
    DISALLOW_COPY_AND_ASSIGN(FrameParser_Base_c);
};

#endif
