/************************************************************************
Copyright (C) 2003-2014 STMicroelectronics. All Rights Reserved.

This file is part of the Streaming Engine.

Streaming Engine is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License version 2 as
published by the Free Software Foundation.

Streaming Engine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with Streaming Engine; see the file COPYING.  If not, write to the Free
Software Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307,
USA.

The Streaming Engine Library may alternatively be licensed under a
proprietary license from ST.
************************************************************************/

#ifndef H_FRAME_PARSER_VIDEO_MPEG2
#define H_FRAME_PARSER_VIDEO_MPEG2

#include "mpeg2.h"
#include "frame_parser_video.h"

#undef TRACE_TAG
#define TRACE_TAG "FrameParser_VideoMpeg2_c"

class FrameParser_VideoMpeg2_c : public FrameParser_Video_c
{
public:
    FrameParser_VideoMpeg2_c();
    ~FrameParser_VideoMpeg2_c();

    //
    // FrameParser class functions
    //

    FrameParserStatus_t   Connect(Port_c *Port);

    //
    // Stream specific functions
    //

    FrameParserStatus_t   GetMpeg2TimeCode(stm_se_ctrl_mpeg2_time_code_t *TimeCode);
    FrameParserStatus_t   ReadHeaders();
    FrameParserStatus_t   ResetReferenceFrameList();
    FrameParserStatus_t   PrepareReferenceFrameList();

    FrameParserStatus_t   ForPlayUpdateReferenceFrameList();
    FrameParserStatus_t   ForPlayCheckForReferenceReadyForManifestation();

    FrameParserStatus_t   RevPlayProcessDecodeStacks();
    FrameParserStatus_t   RevPlayGeneratePostDecodeParameterSettings();
    FrameParserStatus_t   RevPlayAppendToReferenceFrameList();
    FrameParserStatus_t   RevPlayRemoveReferenceFrameFromList();
    FrameParserStatus_t   RevPlayJunkReferenceFrameList();
    bool                  ReadAdditionalUserDataParameters();

private:
    Mpeg2StreamParameters_t       CopyOfStreamParameters;

    Mpeg2StreamParameters_t      *StreamParameters;
    Mpeg2FrameParameters_t       *FrameParameters;

    int                   LastPanScanHorizontalOffset;
    int                   LastPanScanVerticalOffset;
    int                   LastSequencePanScanHorizontalOffset;
    int                   LastSequencePanScanVerticalOffset;

    bool                  EverSeenRepeatFirstField;     // Support for PolicyMPEG2DoNotHonourProgressiveFrameFlag

    bool                  LastFirstFieldWasAnI;         // Support for self referencing IP field pairs
    unsigned int          LastRecordedTemporalReference;

    unsigned int          FirstFieldDecodeFrameIndex;   // to manage properly reference field
    bool                  mFieldSequenceError;
    FrameParserStatus_t   ReadSequenceHeader();
    FrameParserStatus_t   ReadSequenceExtensionHeader();
    FrameParserStatus_t   ReadSequenceDisplayExtensionHeader();
    FrameParserStatus_t   ReadSequenceScalableExtensionHeader();
    FrameParserStatus_t   ReadGroupOfPicturesHeader();
    FrameParserStatus_t   ReadPictureHeader();
    FrameParserStatus_t   ReadPictureCodingExtensionHeader();
    FrameParserStatus_t   ReadQuantMatrixExtensionHeader();
    FrameParserStatus_t   ReadPictureDisplayExtensionHeader();
    FrameParserStatus_t   ReadPictureTemporalScalableExtensionHeader();
    FrameParserStatus_t   ReadPictureSpatialScalableExtensionHeader();

    void                  ReleaseReference();

    void                  StoreTemporalReferenceForLastRecordedFrame(ParsedFrameParameters_t *ParsedFrame);

    bool                  NewStreamParametersCheck();
    FrameParserStatus_t   CommitFrameForDecode();

    DISALLOW_COPY_AND_ASSIGN(FrameParser_VideoMpeg2_c);
};

#endif
