/************************************************************************
Copyright (C) 2003-2014 STMicroelectronics. All Rights Reserved.

This file is part of the Streaming Engine.

Streaming Engine is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License version 2 as
published by the Free Software Foundation.

Streaming Engine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with Streaming Engine; see the file COPYING.  If not, write to the Free
Software Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307,
USA.

The Streaming Engine Library may alternatively be licensed under a
proprietary license from ST.
************************************************************************/

#ifndef H_FRAME_PARSER_VIDEO_RMV
#define H_FRAME_PARSER_VIDEO_RMV

#include "rmv.h"
#include "frame_parser_video.h"

#undef TRACE_TAG
#define TRACE_TAG "FrameParser_VideoRmv_c"

/// Frame parser for RMV video.
class FrameParser_VideoRmv_c : public FrameParser_Video_c
{
public:
    FrameParser_VideoRmv_c();
    ~FrameParser_VideoRmv_c();

    //
    // FrameParser class functions
    //

    FrameParserStatus_t         Connect(Port_c *Port);

    //
    // Stream specific functions
    //

    FrameParserStatus_t   ReadHeaders();

    FrameParserStatus_t   ResetReferenceFrameList();
    FrameParserStatus_t   PrepareReferenceFrameList();

    FrameParserStatus_t   ForPlayUpdateReferenceFrameList();

    FrameParserStatus_t   RevPlayProcessDecodeStacks();

private:
    RmvStreamParameters_t      *StreamParameters;
    RmvFrameParameters_t       *FrameParameters;

    RmvStreamParameters_t       CopyOfStreamParameters;

    bool                        StreamFormatInfoValid;
    Rational_t                  FrameRate;

    unsigned int                LastTemporalReference;
    unsigned int                TemporalReferenceBase;
    unsigned long long          FramePTS;
    unsigned int                FrameNumber;
#if defined (RMV_RECALCULATE_FRAMERATE)
    unsigned int                InitialTemporalReference;
    unsigned int                PCount;
    Rational_t                  CalculatedFrameRate;
#endif

    FrameParserStatus_t         ReadStreamFormatInfo();
    FrameParserStatus_t         ReadPictureHeader();

    bool                        NewStreamParametersCheck();
    FrameParserStatus_t         CommitFrameForDecode();

    DISALLOW_COPY_AND_ASSIGN(FrameParser_VideoRmv_c);
};

#endif
