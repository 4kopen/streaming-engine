/************************************************************************
Copyright (C) 2003-2014 STMicroelectronics. All Rights Reserved.

This file is part of the Streaming Engine.

Streaming Engine is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License version 2 as
published by the Free Software Foundation.

Streaming Engine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with Streaming Engine; see the file COPYING.  If not, write to the Free
Software Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307,
USA.

The Streaming Engine Library may alternatively be licensed under a
proprietary license from ST.
************************************************************************/

#include "vp9.h"
#include "frame_parser_video_vp9.h"

#undef TRACE_TAG
#define TRACE_TAG "FrameParser_VideoVp9_c"

static BufferDataDescriptor_t     Vp9StreamParametersBuffer     = BUFFER_VP9_STREAM_PARAMETERS_TYPE;
static BufferDataDescriptor_t     Vp9FrameParametersBuffer      = BUFFER_VP9_FRAME_PARAMETERS_TYPE;

//#define DUMP_HEADERS


FrameParser_VideoVp9_c::FrameParser_VideoVp9_c(void)
    : StreamMetadataValid(false)
    , FrameRate()
    , MetaData()
{
    Configuration.FrameParserName               = "VideoVp9";
    Configuration.StreamParametersCount         = 32;
    Configuration.StreamParametersDescriptor    = &Vp9StreamParametersBuffer;
    Configuration.FrameParametersCount          = 32;
    Configuration.FrameParametersDescriptor     = &Vp9FrameParametersBuffer;
    mAllowDetachHeaderBuffer = false; // to prevent header buffer getting detached at frame parser
}

FrameParser_VideoVp9_c::~FrameParser_VideoVp9_c(void)
{
    Halt();
}

FrameParserStatus_t   FrameParser_VideoVp9_c::ReadStreamMetadata(void)
{
    memcpy(&MetaData, BufferData, sizeof(MetaData));

    if (MetaData.FrameRate == 0)
    {
        FrameRate                               = Rational_t (30000, 1000);
    }
    else if (MetaData.FrameRate < 23950000)
    {
        FrameRate                               = Rational_t (MetaData.FrameRate, 1000000);
    }
    else if (MetaData.FrameRate < 23990000)
    {
        FrameRate                               = Rational_t (24000, 1001);
    }
    else if (MetaData.FrameRate < 24050000)
    {
        FrameRate                               = Rational_t (24000, 1000);
    }
    else if (MetaData.FrameRate < 24950000)
    {
        FrameRate                               = Rational_t (MetaData.FrameRate, 1000000);
    }
    else if (MetaData.FrameRate < 25050000)
    {
        FrameRate                               = Rational_t (25000, 1000);
    }
    else if (MetaData.FrameRate < 29950000)
    {
        FrameRate                               = Rational_t (MetaData.FrameRate, 1000000);
    }
    else if (MetaData.FrameRate < 29990000)
    {
        FrameRate                               = Rational_t (30000, 1001);
    }
    else if (MetaData.FrameRate < 30050000)
    {
        FrameRate                               = Rational_t (30000, 1000);
    }
    else
    {
        FrameRate                               = Rational_t (MetaData.FrameRate, 1000000);
    }

    StreamMetadataValid                         = true;//(MetaData.Codec == CODEC_ID_VP8)
#ifdef DUMP_HEADERS
    SE_INFO(group_frameparser_video, "StreamMetadata :-\n");
    SE_INFO(group_frameparser_video, "    Codec             : %6d\n", MetaData.Codec);
    SE_INFO(group_frameparser_video, "    Width             : %6d\n", MetaData.Width);
    SE_INFO(group_frameparser_video, "    Height            : %6d\n", MetaData.Height);
    SE_INFO(group_frameparser_video, "    Duration          : %6d\n", MetaData.Duration);
    SE_INFO(group_frameparser_video, "    FrameRate         : %6d\n", MetaData.FrameRate);
#endif
    return FrameParserNoError;
}

//{{{  ReadHeaders
/// /////////////////////////////////////////////////////////////////////////
///
/// \brief      Scan the start code list reading header specific information
///
/// /////////////////////////////////////////////////////////////////////////
FrameParserStatus_t   FrameParser_VideoVp9_c::ReadHeaders(void)
{
    FrameParserStatus_t         Status  = FrameParserNoError;

    if (!StreamMetadataValid)
    {
        Status          = ReadStreamMetadata();
    }
    else
    {
        Status      = CommitFrameForDecode();
    }

    return Status;
}

//}}}

//{{{  Connect
/// /////////////////////////////////////////////////////////////////////////
///
/// \brief      Connect the output Port
///
/// /////////////////////////////////////////////////////////////////////////
FrameParserStatus_t   FrameParser_VideoVp9_c::Connect(Port_c *Port)
{
    //
    // Clear our parameter pointers
    //
    DeferredParsedFrameParameters       = NULL;
    DeferredParsedVideoParameters       = NULL;
    //
    // Pass the call on down (we need the frame parameters count obtained by the lower level function).
    //
    return FrameParser_Video_c::Connect(Port);
}
//}}}
//{{{  PrepareReferenceFrameList
/// /////////////////////////////////////////////////////////////////////////
///
/// \brief      Stream specific function to prepare a reference frame list
///
/// /////////////////////////////////////////////////////////////////////////
FrameParserStatus_t   FrameParser_VideoVp9_c::PrepareReferenceFrameList(void)
{
    ParsedFrameParameters->NumberOfReferenceFrameLists    = 0;
    return FrameParserNoError;
}
//}}}


//{{{  ForPlayUpdateReferenceFrameList
/// /////////////////////////////////////////////////////////////////////////
///
/// \brief      Stream specific function to prepare a reference frame list
///             A reference frame is only recorded as such on the last field, in order to
///             ensure the correct management of reference frames in the codec, the
///             codec is informed immediately of a release on the first field of a field picture.
///
/// /////////////////////////////////////////////////////////////////////////
FrameParserStatus_t   FrameParser_VideoVp9_c::ForPlayUpdateReferenceFrameList(void)
{
    return FrameParserNoError;
}

FrameParserStatus_t   FrameParser_VideoVp9_c::ForPlayGeneratePostDecodeParameterSettings(void)
{
    InitializePostDecodeParameterSettings();
    CalculateFrameIndexAndPts(ParsedFrameParameters, ParsedVideoParameters);
    ParsedFrameParameters->CollapseHolesInDisplayIndices = false;
    ParsedFrameParameters->DisplayFrameIndex = 0;
    return FrameParserNoError;
}

//}}}

//{{{  RevPlayProcessDecodeStacks
// /////////////////////////////////////////////////////////////////////////
//
//      Stream specific override function for processing decode stacks, this
//      initializes the post decode ring before passing itno the real
//      implementation of this function.
//

FrameParserStatus_t   FrameParser_VideoVp9_c::RevPlayProcessDecodeStacks(void)
{
    ReverseQueuedPostDecodeSettingsRing->Flush();
    return FrameParser_Video_c::RevPlayProcessDecodeStacks();
}
//}}}

FrameParserStatus_t   FrameParser_VideoVp9_c::CheckForResolutionConstraints(unsigned int Width, unsigned int Height)
{
    // TODO(PG) check
    (void)Width; // warning removal
    (void)Height; // warning removal
    return FrameParserNoError;
}

//{{{  CommitFrameForDecode
/// /////////////////////////////////////////////////////////////////////////
///
/// \brief      Send frame for decode
///
/// /////////////////////////////////////////////////////////////////////////
FrameParserStatus_t   FrameParser_VideoVp9_c::CommitFrameForDecode(void)
{
    FrameParserStatus_t                 Status = FrameParserNoError;
    // Parsed frame parameters
    ParsedFrameParameters->FirstParsedParametersForOutputFrame  = true;  //FirstDecodeOfFrame;
    ParsedFrameParameters->FirstParsedParametersAfterInputJump  = FirstDecodeAfterInputJump;
    ParsedFrameParameters->ReferenceFrame                       = true;
    ParsedFrameParameters->IndependentFrame                     = ParsedFrameParameters->KeyFrame;
    ParsedFrameParameters->NumberOfReferenceFrameLists          = 0;
    ParsedFrameParameters->NewStreamParameters                  = NewStreamParametersCheck();
    ParsedFrameParameters->SizeofStreamParameterStructure       = 0;//sizeof(Vp9StreamParameters_t);
    ParsedFrameParameters->StreamParameterStructure             = NULL;
    ParsedFrameParameters->NewFrameParameters                   = false;
    ParsedFrameParameters->SizeofFrameParameterStructure        = 0;//sizeof(Vp9FrameParameters_t);
    ParsedFrameParameters->FrameParameterStructure              = NULL;//mFrameParameters;
    // Parsed video parameters
    ParsedVideoParameters->Content.Width                        = 0;//SequenceHeader->encoded_width;
    ParsedVideoParameters->Content.Height                       = 0;//SequenceHeader->encoded_height;
    ParsedVideoParameters->Content.DisplayWidth                 = 0;//SequenceHeader->encoded_width;
    ParsedVideoParameters->Content.DisplayHeight                = 0;//equenceHeader->encoded_height;

    ParsedVideoParameters->Content.Progressive                  = true;
    ParsedVideoParameters->Content.OverscanAppropriate          = false;
    ParsedVideoParameters->Content.VideoFullRange               = false;                                // VP8 conforms to ITU_R_BT601 for source
    ParsedVideoParameters->Content.ColourMatrixCoefficients     = MatrixCoefficients_ITU_R_BT601;
    // Frame rate defaults to 23.976 if metadata not valid
    StreamEncodedFrameRate                  = FrameRate;
    ParsedVideoParameters->Content.FrameRate                    = ResolveFrameRate();
    ParsedVideoParameters->DisplayCount[0]                      = 1;
    ParsedVideoParameters->DisplayCount[1]                      = 0;
    ParsedVideoParameters->Content.PixelAspectRatio             = 1;
    ParsedVideoParameters->PictureStructure                     = StructureFrame;
    ParsedVideoParameters->InterlacedFrame                      = false;
    ParsedVideoParameters->TopFieldFirst                        = true;
    ParsedVideoParameters->PanScanCount                         = 0;

    Status = CheckForResolutionConstraints(ParsedVideoParameters->Content.Width, ParsedVideoParameters->Content.Height);

    if (Status != FrameParserNoError)
    {
        SE_ERROR("Unsupported resolution %d x %d\n", ParsedVideoParameters->Content.Width, ParsedVideoParameters->Content.Height);
        return Status;
    }

    // Record our claim on both the frame and stream parameters
    Buffer->AttachBuffer(StreamParametersBuffer);
    Buffer->AttachBuffer(FrameParametersBuffer);
    // Finally set the appropriate flag and return
    FrameToDecode                                               = true;
    return FrameParserNoError;
}

//}}}
//{{{  NewStreamParametersCheck
/// /////////////////////////////////////////////////////////////////////////
///
/// \brief      Boolean function to evaluate whether or not the stream
///             parameters are new.
///
/// /////////////////////////////////////////////////////////////////////////
bool   FrameParser_VideoVp9_c::NewStreamParametersCheck(void)
{
    return false;
}

//}}}

