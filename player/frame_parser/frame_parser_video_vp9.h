/************************************************************************
Copyright (C) 2003-2014 STMicroelectronics. All Rights Reserved.

This file is part of the Streaming Engine.

Streaming Engine is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License version 2 as
published by the Free Software Foundation.

Streaming Engine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with Streaming Engine; see the file COPYING.  If not, write to the Free
Software Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307,
USA.

The Streaming Engine Library may alternatively be licensed under a
proprietary license from ST.
************************************************************************/

#ifndef H_FRAME_PARSER_VIDEO_VP9
#define H_FRAME_PARSER_VIDEO_VP9

#include "vp9.h"
#include "frame_parser_video.h"

#undef TRACE_TAG
#define TRACE_TAG "FrameParser_VideoVp9_c"

/// Frame parser for Vp9
class FrameParser_VideoVp9_c : public FrameParser_Video_c
{
public:
    FrameParser_VideoVp9_c(void);
    ~FrameParser_VideoVp9_c(void);

    //
    // FrameParser class functions
    //
    FrameParserStatus_t         Connect(Port_c *Port);

    //
    // Stream specific functions
    //

    FrameParserStatus_t         ReadHeaders(void);
    FrameParserStatus_t         ReadStreamMetadata(void);

    FrameParserStatus_t         PrepareReferenceFrameList(void);

    FrameParserStatus_t         ForPlayUpdateReferenceFrameList(void);

    FrameParserStatus_t         RevPlayProcessDecodeStacks(void);

    FrameParserStatus_t         ForPlayGeneratePostDecodeParameterSettings(void);

    FrameParserStatus_t         CheckForResolutionConstraints(unsigned int Width, unsigned int Height);
private:
    FrameParserStatus_t         CommitFrameForDecode(void);
    bool                        NewStreamParametersCheck(void);
    bool                        StreamMetadataValid;
    Rational_t                  FrameRate;
    struct Vp9MetaData_s        MetaData;

    DISALLOW_COPY_AND_ASSIGN(FrameParser_VideoVp9_c);

};

#endif /* H_FRAME_PARSER_VIDEO_VP9 */
