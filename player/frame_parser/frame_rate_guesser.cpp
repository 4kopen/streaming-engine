/************************************************************************
Copyright (C) 2003-2015 STMicroelectronics. All Rights Reserved.

This file is part of the Streaming Engine.

Streaming Engine is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License version 2 as
published by the Free Software Foundation.

Streaming Engine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with Streaming Engine; see the file COPYING.  If not, write to the Free
Software Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307,
USA.

The Streaming Engine Library may alternatively be licensed under a
proprietary license from ST.
************************************************************************/

#include "frame_rate_guesser.h"

#undef TRACE_TAG
#define TRACE_TAG "FrameRateGuesser_c"

#define STABILITY_COUNTER_THRESOLD 64
#define INSTANTANEOUS_STABILITY_THRESHOLD 5
#define CLOSE_RATES_PERCENT 4
#define FRAME_RATE_CHANGE_PERCENT_THRESHOLD 40

FrameRateGuesser_c::FrameRateGuesser_c()
    : mPrecedence(PolicyValuePrecedencePtsStreamContainerDefault)
    , mProgressive(true)
    , mPTSDeducedFrameRateStabilityCounter(0)
    , mInstantaneousFrameRateStabilityCounter(0)
    , mPTSDeducedFrameRateIsStable(false)
    , mDefaultFrameRate(50)
    , mStreamFrameRate(INVALID_FRAMERATE)
    , mContainerFrameRate(INVALID_FRAMERATE)
    , mLastPTSDeducedFrameRate(INVALID_FRAMERATE)
    , mLastResolvedFrameRate(INVALID_FRAMERATE)
    , mFrameRateIntegrationCounter(-1)
    , mDeduceFrameRateElapsedFields()
    , mDeduceFrameRateElapsedTime()
    , mLastConsideredDisplayFieldIndex(0)
    , mLastPts()
    , mLastConsideredPts()
    , mWaitForNewPts(false)
    , mFR(0)
{
}

void FrameRateGuesser_c::Reset()
{
    mPTSDeducedFrameRateStabilityCounter = 0;
    mInstantaneousFrameRateStabilityCounter = 0;
    mFrameRateIntegrationCounter = -1;
    memset(mDeduceFrameRateElapsedFields, 0, sizeof(mDeduceFrameRateElapsedFields));
    memset(mDeduceFrameRateElapsedTime, 0, sizeof(mDeduceFrameRateElapsedTime));
    mLastConsideredDisplayFieldIndex = 0;
    mLastPts = TimeStamp_c();
    mLastConsideredPts = TimeStamp_c();
}

static Rational_t   SelectFrameRate(Rational_t  F0,
                                    Rational_t  F1,
                                    Rational_t  F2,
                                    Rational_t  F3)
{
    return  ValidFrameRate(F0) ? F0 : (ValidFrameRate(F1) ? F1 : (ValidFrameRate(F2) ? F2 : F3));
}

void FrameRateGuesser_c::AddPts(TimeStamp_c Pts, int FieldIndex, bool Progressive)
{
    SE_VERBOSE(group_frc, "New PTS : %lld index=%d progressive=%d\n"
               , Pts.NativeValue(), FieldIndex, Progressive);

    mWaitForNewPts = false;

    if (mLastPts.IsValid() && Pts != mLastPts)
    {
        mFrameRateIntegrationCounter++;

        SE_EXTRAVERB(group_frc, "counter=%d\n", mFrameRateIntegrationCounter);

        int index = mFrameRateIntegrationCounter % FRAME_RATE_INTEGRATION_MAX_WINDOW_WIDTH;

        mDeduceFrameRateElapsedFields[index]    = (FieldIndex - mLastConsideredDisplayFieldIndex);
        if (Progressive == true)
        {
            mDeduceFrameRateElapsedFields[index]  *= 2;
        }

        mDeduceFrameRateElapsedTime[index]      = TimeStamp_c::DeltaUsec(Pts, mLastConsideredPts);

        SE_EXTRAVERB(group_frc, "Registering val[%d] : fields=%d period=%lld\n"
                     , index
                     , mDeduceFrameRateElapsedFields[index]
                     , mDeduceFrameRateElapsedTime[index]);

        mLastConsideredPts = Pts;
        mLastConsideredDisplayFieldIndex = FieldIndex;
    }
    if (!mLastPts.IsValid())
    {
        mLastConsideredPts = Pts;
        mLastConsideredDisplayFieldIndex = FieldIndex;
    }
    mLastPts = Pts;
}
// /////////////////////////////////////////////////////////////////////////
//
//      Function computing a suitable FrameRate from current PTS value and history
//      Computation is performed on a sliding window to smooth framerate in case PTS
//      deltas are not constant locally.
//
//      It uses some rules to ensure that :
//      - Frame rate changed event raised only when computed rate is stable
//      - In segments where rate is unstable, when stability is back, transition is immediate
//

Rational_t FrameRateGuesser_c::GuessFrameRate()
{
    if (mWaitForNewPts && ValidFrameRate(mLastPTSDeducedFrameRate))
    {
        SE_EXTRAVERB(group_frc, "Reusing previously calculated rate : %d.%06d\n",
                     mLastPTSDeducedFrameRate.IntegerPart(), mLastPTSDeducedFrameRate.RemainderDecimal());
        return mLastPTSDeducedFrameRate;
    }
    mWaitForNewPts = true;

    Rational_t PTSDeducedFrameRate(INVALID_FRAMERATE);

    int DeduceFrameRateElapsedFieldsCount = 0;
    long long DeduceFrameRateElapsedTimeCount = 0;
    int index;
    // Integrate the sliding window
    int MaxIndex = (mFrameRateIntegrationCounter >= FRAME_RATE_INTEGRATION_MAX_WINDOW_WIDTH) ?
                   FRAME_RATE_INTEGRATION_MAX_WINDOW_WIDTH - 1 : mFrameRateIntegrationCounter;
    if (MaxIndex >= 0)
    {
        for (int i = 0; i <= MaxIndex; i++)
        {
            DeduceFrameRateElapsedFieldsCount += mDeduceFrameRateElapsedFields[i];
            DeduceFrameRateElapsedTimeCount += mDeduceFrameRateElapsedTime[i];
        }

        index = mFrameRateIntegrationCounter % FRAME_RATE_INTEGRATION_MAX_WINDOW_WIDTH;

        SE_EXTRAVERB(group_frc
                     , "FieldsCount=%d TimeCount=%lld index=%d v=%d MaxIndex=%d Fields[index]=%d\n"
                     , DeduceFrameRateElapsedFieldsCount
                     , DeduceFrameRateElapsedTimeCount
                     , index
                     , mDeduceFrameRateElapsedFields[index]
                     , MaxIndex
                     , mDeduceFrameRateElapsedFields[index]);
    }

    bool isInstantaneousStandard = false;
    bool isIntegratedStandard    = false;
    bool isCurrentStandard       = false;
    Rational_t InstantaneousFrameRate;
    Rational_t IntegratedFrameRate;

    if (DeduceFrameRateElapsedFieldsCount != 0 && mDeduceFrameRateElapsedFields[index] != 0)
    {
        long long int IntegratedMicroSecondsPerFrame =
            (2 * DeduceFrameRateElapsedTimeCount) / DeduceFrameRateElapsedFieldsCount;

        long long int InstantaneousMicroSecondsPerFrame =
            (2 * mDeduceFrameRateElapsedTime[index]) / mDeduceFrameRateElapsedFields[index];

        long long int LastMicroSecondsPerFrame = 0; // based on current framerate

        // Calculation of the current frame period using current framerate,
        // if possible (from a previous iteration)
        if (ValidFrameRate(mLastResolvedFrameRate))
        {
            LastMicroSecondsPerFrame = RoundedLongLongIntegerPart(1000000 / mLastResolvedFrameRate);
            int Class; // not meant to be used in this context
            GetRateFromFrameDuration(LastMicroSecondsPerFrame, &isCurrentStandard, &Class);
        }


        SE_EXTRAVERB(group_frc
                     , "us per frame : Instantaneous=%lld Last=%lld Integrated=%lld on %d fields over %lld us (%d samples) Progressive:%d last_standard=%d\n"
                     , InstantaneousMicroSecondsPerFrame
                     , LastMicroSecondsPerFrame
                     , IntegratedMicroSecondsPerFrame
                     , DeduceFrameRateElapsedFieldsCount
                     , DeduceFrameRateElapsedTimeCount
                     , MaxIndex
                     , mProgressive
                     , isCurrentStandard);

        InstantaneousFrameRate = GetRateFromFrameDuration(
                                     InstantaneousMicroSecondsPerFrame, &isInstantaneousStandard, &mFR);
        int Class; // Not meant to be used in this context
        IntegratedFrameRate = GetRateFromFrameDuration(
                                  IntegratedMicroSecondsPerFrame, &isIntegratedStandard, &Class);

        if (mDeduceFrameRateElapsedFields[index] > 2 && mStreamFrameRate == mLastResolvedFrameRate)
        {
            // When currently resolved rate is the stream rate and when we have a big number of fields (hence a gap in the stream)
            // we'll avoid to consider a standard "instantaneous rate" which is definitely not instantaneous
            isInstantaneousStandard = 0;
        }
        // By default, Integrated value will be used unless Instantaneous rate is higher (PTS delta smaller)
        // and Integrated value not standard
        if (InstantaneousFrameRate > IntegratedFrameRate && !isIntegratedStandard)
        {
            PTSDeducedFrameRate = InstantaneousFrameRate;
        }
        else
        {
            PTSDeducedFrameRate = IntegratedFrameRate;
        }

        SE_EXTRAVERB(group_frc, "IntegratedFrameRate = %d.%06d (standard:%d)\n"
                     , IntegratedFrameRate.IntegerPart()
                     , IntegratedFrameRate.RemainderDecimal(), isIntegratedStandard);
        SE_EXTRAVERB(group_frc, "InstantaneousFrameRate = %d.%06d (standard:%d)\n"
                     , InstantaneousFrameRate.IntegerPart()
                     , InstantaneousFrameRate.RemainderDecimal(), isInstantaneousStandard);

        bool PtsDeducedFrameRateReliable = true;

        // And now some heuristics to keep a proper framerate, avoid spurious changes,
        // ensure quick and clean transitions

        if (InstantaneousFrameRate != mLastPTSDeducedFrameRate)
        {
            mInstantaneousFrameRateStabilityCounter = 0;
        }

        // Let's compute how close are current rate and last resolved rate
        Rational_t DiffToLastAbs = mLastResolvedFrameRate - InstantaneousFrameRate;
        if (DiffToLastAbs.IsNegative())
        {
            DiffToLastAbs = -1 * DiffToLastAbs;
        }
        Rational_t Ratio;
        if (ValidFrameRate(mLastResolvedFrameRate))
        {
            Ratio = DiffToLastAbs / mLastResolvedFrameRate;
        }
        else
        {
            Ratio = 1;
        }

        // Maybe we'd rather keep last rate ?

        if (ValidFrameRate(mLastResolvedFrameRate)
            && InstantaneousFrameRate == mLastResolvedFrameRate)

        {
            mInstantaneousFrameRateStabilityCounter++;
            if (isInstantaneousStandard || mInstantaneousFrameRateStabilityCounter >= INSTANTANEOUS_STABILITY_THRESHOLD)
            {
                // We are stable
                PTSDeducedFrameRate = InstantaneousFrameRate;
            }
            else
            {
                PtsDeducedFrameRateReliable = false;
            }
        }
        else if (isIntegratedStandard && IntegratedFrameRate == mStreamFrameRate)
        {
            PTSDeducedFrameRate = IntegratedFrameRate;
        }
        else if (Ratio < Rational_t(CLOSE_RATES_PERCENT, 100) && isCurrentStandard && !isInstantaneousStandard)
        {
            SE_EXTRAVERB(group_frc
                         ,  "Rates are close : instantaneous : %d.%06d, reusing previous : %d.%06d\n"
                         , InstantaneousFrameRate.IntegerPart()
                         , InstantaneousFrameRate.RemainderDecimal()
                         , mLastResolvedFrameRate.IntegerPart()
                         , mLastResolvedFrameRate.RemainderDecimal()
                        );
            PTSDeducedFrameRate = mLastResolvedFrameRate;
        }
        // Case to handle missing frames during stable segment :
        // instantaneous rate is seen as k x previous rate
        // i.e ratio has a null decimal part
        else if (mPTSDeducedFrameRateIsStable == true
                 && Rational_t(mLastPTSDeducedFrameRate / InstantaneousFrameRate).RemainderDecimal() == 0)
        {
            PTSDeducedFrameRate = mLastPTSDeducedFrameRate;
        }
        // Case where instantaneous rate is standard and greater than previously resolved rate
        // (lowering framerate must be rare, especially in unstable segments)
        // in an unstable segment and either equal to stream rate
        // or when stream rate is not set
        // All these conditions are covered by unitary tests
        else if (isInstantaneousStandard == true
                 && InstantaneousFrameRate > mLastResolvedFrameRate
                 && mPTSDeducedFrameRateStabilityCounter == 0
                 && (mStreamFrameRate == InstantaneousFrameRate || !IsStandardFrameRate()))
        {
            PTSDeducedFrameRate = InstantaneousFrameRate;
        }
        // Case where Integrated rate and instantaneous rates are equal and standard
        // Case also covered by unit tests
        else if (InstantaneousFrameRate == IntegratedFrameRate
                 && (isInstantaneousStandard == true || mPTSDeducedFrameRateStabilityCounter > 0))
        {
            PTSDeducedFrameRate = InstantaneousFrameRate;
        }
        // If none of previous cases matches, maybe we should fallback to using rates from stream or container ?
        // Maybe we should skip to instantaneous version if it matches those of container or stream ?
        else if (mContainerFrameRate == InstantaneousFrameRate)
        {
            PTSDeducedFrameRate = mContainerFrameRate;
        }
        else if (mStreamFrameRate == InstantaneousFrameRate)
        {
            PTSDeducedFrameRate = mStreamFrameRate;
        }
        else if (isInstantaneousStandard && (mPTSDeducedFrameRateStabilityCounter > 2))
        {
            PtsDeducedFrameRateReliable = true;
            PTSDeducedFrameRate = InstantaneousFrameRate;
        }
        else if (!isInstantaneousStandard
                 && !isIntegratedStandard
                 && isCurrentStandard)
        {
            // We are in a bad shape !
            PTSDeducedFrameRate = mLastResolvedFrameRate;
        }
        // Well, the rate we might is probably not (yet) reliable.
        else
        {
            PtsDeducedFrameRateReliable = false;
        }

        // Are we stable ?
        if (ValidFrameRate(PTSDeducedFrameRate) && PTSDeducedFrameRate == mLastPTSDeducedFrameRate)
        {
            mPTSDeducedFrameRateStabilityCounter++;
            SE_EXTRAVERB(group_frc
                         ,  "DeduceFrameRateFromPresentationTime - Framerate = %d.%06d stability:%d\n"
                         , PTSDeducedFrameRate.IntegerPart()
                         , PTSDeducedFrameRate.RemainderDecimal()
                         , mPTSDeducedFrameRateStabilityCounter);

            // We have been very stable, our defaut rate in case we fell into instability
            // is reset to a stable value
            if (mPTSDeducedFrameRateStabilityCounter > STABILITY_COUNTER_THRESOLD)
            {
                mDefaultFrameRate = mLastPTSDeducedFrameRate;
            }

            // We consider ourselves stable
            if (mPTSDeducedFrameRateStabilityCounter >= FRAME_RATE_INTEGRATION_MAX_WINDOW_WIDTH)
            {
                mPTSDeducedFrameRateIsStable = true;
            }
        }
        else
        {
            // Do we trust the new value (-> reliable) ?
            if (PtsDeducedFrameRateReliable == false)
            {
                mPTSDeducedFrameRateStabilityCounter = 0;
                mPTSDeducedFrameRateIsStable = false;
            }
        }

        // Our computation gave a reliable output, let's assume we are going to be stable
        // This ensure quick transitions to a better rate detection
        if (PtsDeducedFrameRateReliable)
        {
            mPTSDeducedFrameRateIsStable = true;
        }

        mLastPTSDeducedFrameRate = PTSDeducedFrameRate;

        SE_EXTRAVERB(group_frc, "PTSDeducedFrameRate = %d.%06d (stable:%d, counter:%d)\n"
                     , PTSDeducedFrameRate.IntegerPart()
                     , PTSDeducedFrameRate.RemainderDecimal()
                     , mPTSDeducedFrameRateIsStable
                     , mPTSDeducedFrameRateStabilityCounter);
    }
    else
    {
        PTSDeducedFrameRate = INVALID_FRAMERATE;
    }

    bool isChosenRateStandard = false;
    if ((PTSDeducedFrameRate == InstantaneousFrameRate && isInstantaneousStandard)
        || (PTSDeducedFrameRate == IntegratedFrameRate && isIntegratedStandard)
        || PTSDeducedFrameRate == mStreamFrameRate
        || PTSDeducedFrameRate == mContainerFrameRate)
    {
        isChosenRateStandard = true;
    }

    ////////////////////////////////////////////////////////

    Rational_t  FrameRate;
    Rational_t  UsedPTSFrameRate = PTSDeducedFrameRate;

    SE_VERBOSE(group_frc
               ,  "Frame rates: Stream: %d.%06d Container: %d.%06d PTSDeduced: %d.%06d (stable:%d) Default: %d.%06d\n"
               , mStreamFrameRate.IntegerPart(), mStreamFrameRate.RemainderDecimal()
               , mContainerFrameRate.IntegerPart(), mContainerFrameRate.RemainderDecimal()
               , PTSDeducedFrameRate.IntegerPart(), PTSDeducedFrameRate.RemainderDecimal()
               , mPTSDeducedFrameRateIsStable
               , mDefaultFrameRate.IntegerPart(), mDefaultFrameRate.RemainderDecimal()
              );

    // If detected rate is unstable, we should not take it into account
    if (!mPTSDeducedFrameRateIsStable)
    {
        if (ValidFrameRate(mStreamFrameRate) || ValidFrameRate(mContainerFrameRate) || !isChosenRateStandard)
        {
            UsedPTSFrameRate = INVALID_FRAMERATE;
        }
    }

    switch (mPrecedence)
    {
    default:
    case PolicyValuePrecedenceStreamPtsContainerDefault:
        FrameRate   = SelectFrameRate(
                          mStreamFrameRate, UsedPTSFrameRate, mContainerFrameRate, mDefaultFrameRate);
        break;

    case PolicyValuePrecedenceStreamContainerPtsDefault:
        FrameRate   = SelectFrameRate(
                          mStreamFrameRate, mContainerFrameRate, UsedPTSFrameRate, mDefaultFrameRate);
        break;

    case PolicyValuePrecedencePtsStreamContainerDefault:

        FrameRate   = SelectFrameRate(
                          UsedPTSFrameRate, mStreamFrameRate, mContainerFrameRate, mDefaultFrameRate);
        break;

    case PolicyValuePrecedencePtsContainerStreamDefault:
        FrameRate   = SelectFrameRate(
                          UsedPTSFrameRate, mContainerFrameRate, mStreamFrameRate, mDefaultFrameRate);
        break;

    case PolicyValuePrecedenceContainerPtsStreamDefault:
        FrameRate   = SelectFrameRate(
                          mContainerFrameRate, UsedPTSFrameRate, mStreamFrameRate, mDefaultFrameRate);
        break;

    case PolicyValuePrecedenceContainerStreamPtsDefault:
        FrameRate   = SelectFrameRate(
                          mContainerFrameRate, mStreamFrameRate, UsedPTSFrameRate, mDefaultFrameRate);
        break;
    }
    SE_VERBOSE(group_frc,  "Selected FrameRate = %d.%06d\n"
               , FrameRate.IntegerPart()
               , FrameRate.RemainderDecimal());

    if (mLastResolvedFrameRate != FrameRate)
    {
        SE_INFO(group_frameparser_video,  "New Framerate : %d.%03d\n"
                , FrameRate.IntegerPart()
                , FrameRate.RemainderDecimal(3));
    }

    mLastResolvedFrameRate   = FrameRate;

    ////////////////////////////////////////////////////////

    return FrameRate;
}

#define ValueMatchesFrameTime(V,T)   inrange( V, T - 8, T + 8 )

bool FrameRateGuesser_c::IsStandardFrameRate()
{
    long long MicroSecondsPerFrame = RoundedLongLongIntegerPart(1000000 / mLastResolvedFrameRate);
    bool standard = false;
    int Class; // Not meant to be used in this context
    GetRateFromFrameDuration(MicroSecondsPerFrame, &standard, &Class);
    return standard;
}

Rational_t FrameRateGuesser_c::GetRateFromFrameDuration(long long int MicroSecondsPerFrame, bool *standard, int *FrameRateClass)
{
    Rational_t PTSDeducedFrameRate(INVALID_FRAMERATE);
    if (MicroSecondsPerFrame == 0)
    {
        *standard = false;
        return PTSDeducedFrameRate;
    }

    *standard = true;
    if (ValueMatchesFrameTime(MicroSecondsPerFrame, 4167) && mProgressive)         // 240fps = 4166.67 us
    {
        mFR = 240;

        PTSDeducedFrameRate = Rational_t(240, 1);
        if (mStreamFrameRate == Rational_t(240000, 1001))
        {
            *standard = false;
        }
    }
    else if (ValueMatchesFrameTime(MicroSecondsPerFrame, 8333) && mProgressive)         // 120fps = 8.33333 us
    {
        mFR = 120;

        PTSDeducedFrameRate = Rational_t(120, 1);
        if (mStreamFrameRate == Rational_t(120000, 1001))
        {
            *standard = false;
        }
    }
    else if (ValueMatchesFrameTime(MicroSecondsPerFrame, 16667) && mProgressive)         // 60fps = 16666.67 us
    {
        *FrameRateClass = 60;

        PTSDeducedFrameRate = Rational_t(60, 1);
        if (mStreamFrameRate == Rational_t(60000, 1001))
        {
            *standard = false;
        }
    }
    else if (ValueMatchesFrameTime(MicroSecondsPerFrame, 16683) && mProgressive)    // 59fps = 16683.33 us
    {
        *FrameRateClass = 60;
        PTSDeducedFrameRate = Rational_t(60000, 1001);
        if (mStreamFrameRate == 60)
        {
            *standard = false;
        }
    }
    else if (ValueMatchesFrameTime(MicroSecondsPerFrame, 20000) && mProgressive)    // 50fps = 20000.00 us
    {
        *FrameRateClass = 50;
        PTSDeducedFrameRate = Rational_t(50, 1);
    }
    else if (ValueMatchesFrameTime(MicroSecondsPerFrame, 33333))                    // 30fps = 33333.33 us
    {
        *FrameRateClass = 30;
        PTSDeducedFrameRate = Rational_t(30, 1);
        if (mStreamFrameRate == Rational_t(30000, 1001))
        {
            *standard = false;
        }
    }
    else if (ValueMatchesFrameTime(MicroSecondsPerFrame, 33367))                    // 29fps = 33366.67 us
    {
        if (mFR >= 60 || mStreamFrameRate == 30)
        {
            *standard = false;
        }
        else
        {
            *FrameRateClass = 30;
        }
        PTSDeducedFrameRate = Rational_t(30000, 1001);
    }
    else if (ValueMatchesFrameTime(MicroSecondsPerFrame, 40000))                    // 25fps = 40000.00 us
    {
        if (mFR >= 50)
        {
            *standard = false;
        }
        else
        {
            *FrameRateClass = 25;
        }
        PTSDeducedFrameRate = Rational_t(25, 1);
    }
    else if (ValueMatchesFrameTime(MicroSecondsPerFrame, 41667))                    // 24fps = 41666.67 us
    {
        *FrameRateClass = 24;
        PTSDeducedFrameRate = Rational_t(24, 1);
    }
    else if (ValueMatchesFrameTime(MicroSecondsPerFrame, 41708))                    // 23fps = 41708.33 us
    {
        PTSDeducedFrameRate = Rational_t(24000, 1001);
        *standard = false;
    }
    else if (ValueMatchesFrameTime(MicroSecondsPerFrame, 66733))                    // 14.9fps = 66733.33 us
    {
        PTSDeducedFrameRate     = Rational_t(30000 / 2, 1001);
        if (mFR >= 24)
        {
            *standard = false;
        }
        else
        {
            *FrameRateClass = 15;
        }
    }
    else if (ValueMatchesFrameTime(MicroSecondsPerFrame, 133467))                   // 7fps = 133466.67 us
    {
        PTSDeducedFrameRate     = Rational_t(30000 / 4, 1001);
        if (mFR >= 15)
        {
            *standard = false;
        }
        else
        {
            *FrameRateClass = 7;
        }
    }
    else
    {
        *standard = false;
        PTSDeducedFrameRate     = Rational_t(1000000, MicroSecondsPerFrame);
        *FrameRateClass = PTSDeducedFrameRate.IntegerPart();
    }
    return PTSDeducedFrameRate;
}

