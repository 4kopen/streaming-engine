/************************************************************************
Copyright (C) 2003-2015 STMicroelectronics. All Rights Reserved.

This file is part of the Streaming Engine.

Streaming Engine is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License version 2 as
published by the Free Software Foundation.

Streaming Engine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with Streaming Engine; see the file COPYING.  If not, write to the Free
Software Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307,
USA.

The Streaming Engine Library may alternatively be licensed under a
proprietary license from ST.
************************************************************************/

#ifndef H_FRAME_RATE_GUESSER
#define H_FRAME_RATE_GUESSER

#include "osinline.h"
#include "se_limits.h"
#include "rational.h"
#include "player_types.h"
#include "timestamps.h"

#define INVALID_FRAMERATE           0
#define FRAME_RATE_INTEGRATION_MAX_WINDOW_WIDTH 12 // Fields to integrate to compute framerate from PTS
#define ValidFrameRate(F)   inrange( F, 7, 240 )

class FrameRateGuesser_c
{
public:
    FrameRateGuesser_c();
    ~FrameRateGuesser_c() {}

    void SetPrecedence(int Precedence)
    {
        mPrecedence = Precedence;
    }

    void SetProgressive(bool Progressive)
    {
        mProgressive = Progressive;
    }

    void SetStreamFrameRate(Rational_t StreamFrameRate)
    {
        mStreamFrameRate = StreamFrameRate;
    }
    void SetContainerFrameRate(Rational_t ContainerFrameRate)
    {
        mContainerFrameRate = ContainerFrameRate;
    }

    void AddPts(TimeStamp_c Pts, int FieldIndex, bool Progressive);

    void Reset();

    Rational_t GuessFrameRate();

    bool IsStandardFrameRate();
    Rational_t GetRateFromFrameDuration(long long int MicroSecondsPerFrame, bool *standard, int *FrameRateClass);

private :

    int  mPrecedence;
    bool mProgressive;
    int  mPTSDeducedFrameRateStabilityCounter;
    int  mInstantaneousFrameRateStabilityCounter;
    bool mPTSDeducedFrameRateIsStable;

    Rational_t mDefaultFrameRate;
    Rational_t mStreamFrameRate;
    Rational_t mContainerFrameRate;
    Rational_t mLastPTSDeducedFrameRate;
    Rational_t mLastResolvedFrameRate;

    // Sliding window arrays
    int         mFrameRateIntegrationCounter; // For modulo adressing in the sliding average array
    int         mDeduceFrameRateElapsedFields[FRAME_RATE_INTEGRATION_MAX_WINDOW_WIDTH];
    long long   mDeduceFrameRateElapsedTime[FRAME_RATE_INTEGRATION_MAX_WINDOW_WIDTH];

    unsigned int mLastConsideredDisplayFieldIndex;
    TimeStamp_c mLastPts;
    TimeStamp_c mLastConsideredPts;
    bool        mWaitForNewPts;
    // Used in "standard" rate decision making : when current rate is (say) 50fps,
    // and if a frame is missing, 25fps will NOT be a standard rate in a 50fps sequence
    int         mFR;
};

#endif

