/************************************************************************
Copyright (C) 2003-2014 STMicroelectronics. All Rights Reserved.

This file is part of the Streaming Engine.

Streaming Engine is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License version 2 as
published by the Free Software Foundation.

Streaming Engine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with Streaming Engine; see the file COPYING.  If not, write to the Free
Software Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307,
USA.

The Streaming Engine Library may alternatively be licensed under a
proprietary license from ST.
************************************************************************/

#ifndef STM_SE_CONTROLS_PSEUDOMIXER_H_
#define STM_SE_CONTROLS_PSEUDOMIXER_H_

/*!
 * \defgroup pseudo_mixer_interfaces pseudo mixer interfaces
 *
 * @{
 */

typedef void *component_handle_t;
/// API used by pseudo mixer to set mixer parameters
int  stm_se_component_set_module_parameters(component_handle_t      Component,
                                            void                    *Data,
                                            unsigned int            Size);

// >>> start of pseudo_mixer.h copy - also includes some linux types at the moment;

#define SND_PSEUDO_MIXER_ADJUST_GRAIN(x)        ((x + 63) & 0xFFC0) // round to 64
#define HDMIRX_MIXER_MIN_GRAIN                   512
#define SND_PSEUDO_MIXER_MIN_GRAIN              64
#define SND_PSEUDO_MIXER_MAX_GRAIN              2048
#define SND_PSEUDO_MIXER_DEFAULT_GRAIN          1536

#define SND_PSEUDO_MIXER_CHANNELS           8
#define SND_PSEUDO_MIXER_INTERACTIVE        8
#define SND_PSEUDO_MIXER_MAGIC              0xf051

#define SND_PSEUDO_TRANSFORMER_NAME_MAGIC   0xf052

/** Maximum number of supported outputs (subordinate ALSA soundcards) */
#define SND_PSEUDO_MAX_OUTPUTS              8

// if MIXER_0dB_uQ3_13 is defined, use it for Q3_13_UNITY and check that it is equal to (1 << 13)
// so that we can use this (1 << 13) value for Q3_13_UNITY when MIXER_0dB_uQ3_13 is not defined
#ifdef MIXER_0dB_uQ3_13
#define Q3_13_UNITY             MIXER_0dB_uQ3_13
#if MIXER_0dB_uQ3_13 != (1 << 13)
#error "MIXER_0dB_uQ3_13 != (1 << 13)"
#endif
#else
#define Q3_13_UNITY             (1 << 13)
#endif

#define Q3_13_MIN               0
#define Q3_13_M3DB              0x16A7  // ( 2^13 * 10^(-3/20))
#define Q3_13_MAX               0xffff
#define Q0_8_MIN                0
#define Q0_8_UNITY              ((1 << 8) -1 )
#define Q0_8_MAX                0xFF

enum snd_pseudo_mixer_metadata_update
{
    SND_PSEUDO_MIXER_METADATA_UPDATE_NEVER,
    SND_PSEUDO_MIXER_METADATA_UPDATE_PRIMARY_AND_SECONDARY_ONLY,  // never used
    SND_PSEUDO_MIXER_METADATA_UPDATE_ALWAYS,
};

enum snd_pseudo_mixer_interactive_audio_mode
{
    SND_PSEUDO_MIXER_INTERACTIVE_AUDIO_MODE_3_4,
    SND_PSEUDO_MIXER_INTERACTIVE_AUDIO_MODE_3_2,
    SND_PSEUDO_MIXER_INTERACTIVE_AUDIO_MODE_2_0
};

enum snd_pseudo_mixer_audiogen_audio_mode
{
    SND_PSEUDO_MIXER_AUDIOGEN_AUDIO_MODE_3_4,
    SND_PSEUDO_MIXER_AUDIOGEN_AUDIO_MODE_3_2,
    SND_PSEUDO_MIXER_AUDIOGEN_AUDIO_MODE_2_0
};

// deprecated
struct snd_pseudo_mixer_fatpipe
{
    unsigned short md[16];
};

#define SND_PSEUDO_TOPOLOGY_FLAGS_ENABLE_SPDIF_FORMATING 0x01
#define SND_PSEUDO_TOPOLOGY_FLAGS_FATPIPE                0x02
#define SND_PSEUDO_TOPOLOGY_FLAGS_ENABLE_HDMI_FORMATING  0x04
#define SND_PSEUDO_TOPOLOGY_FLAGS_ENABLE_PROMOTION       0x08

// Note : the following 3 flags are exclusive .. if none is flagged, then the driver will implement following default :
//  case HDMI   : default to TV (ToDO :: read from EDID )
//  case Analog : default to TV
//  case SPDIF  : default to AVR
#define SND_PSEUDO_TOPOLOGY_FLAGS_CONNECTED_TO_TV        0x10
#define SND_PSEUDO_TOPOLOGY_FLAGS_CONNECTED_TO_AVR       0x20
#define SND_PSEUDO_TOPOLOGY_FLAGS_CONNECTED_TO_HEADPHONE 0x40

struct snd_pseudo_mixer_downstream_card
{
    char          name[16]; /* e.g. Analog0, HDMI */

    char          alsaname[24]; /* card name (e.g. hw:0,0 or hw:ANALOG,1) */

    unsigned int  flags;
    unsigned int  max_freq;     /* in hz */
    unsigned char num_channels;

    char          reserved[7];

    int32_t       target_level; /* in mB */
    struct stm_se_audio_channel_assignment channel_assignment;
};

struct snd_pseudo_mixer_downstream_topology
{
    struct snd_pseudo_mixer_downstream_card card[SND_PSEUDO_MAX_OUTPUTS];
};

struct snd_pseudo_mixer_settings
{
    unsigned int magic;

    /* The next two indices help us getting an handle to the hdmi output device*/
    unsigned int display_device_id;
    int display_output_id;

    enum snd_pseudo_mixer_metadata_update metadata_update;

    /* switches */
    char all_speaker_stereo_enable;
    char dualmono_metadata_override;

    /* routes */
    enum snd_pseudo_mixer_interactive_audio_mode interactive_audio_mode;
    enum snd_pseudo_mixer_audiogen_audio_mode audiogen_audio_mode;

    /* latency tuning */
    int master_latency;

    /* fatpipe meta data: deprecated */
    struct snd_pseudo_mixer_fatpipe fatpipe_metadata;
    struct snd_pseudo_mixer_fatpipe fatpipe_mask;

    /* topological structure */
    struct snd_pseudo_mixer_downstream_topology downstream_topology;

    /* local copy of mixer channel assignment */
    struct stm_se_audio_channel_assignment MixerChannelAssignment;
};

struct snd_pseudo_transformer_name
{
    unsigned int magic;

    char name[128];
};

typedef void (snd_pseudo_mixer_observer_t)(void *ctx,
                                           const struct snd_pseudo_mixer_settings *mixer_settings);

// <<< end of pseudo_mixer.h copy

/*! @} */ /* pseudo_mixer_interfaces */

#endif /* STM_SE_CONTROLS_PSEUDOMIXER_H_ */
