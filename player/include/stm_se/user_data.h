/************************************************************************
Copyright (C) 2003-2014 STMicroelectronics. All Rights Reserved.

This file is part of the Streaming Engine.

Streaming Engine is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License version 2 as
published by the Free Software Foundation.

Streaming Engine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with Streaming Engine; see the file COPYING.  If not, write to the Free
Software Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307,
USA.

The Streaming Engine Library may alternatively be licensed under a
proprietary license from ST.
************************************************************************/

#ifndef STM_SE_USER_DATA_H_
#define STM_SE_USER_DATA_H_

/*!
 * \defgroup user_data User data
 *
 * User data are parsed from video streams then attached to picture frames.
 * They are interpreted in upper levels as closed captions and subtitling.
 *
 * See application note for layout of user-data buffer passed to clients.
 *
 * @{
 */

#define STM_SE_MAXIMUM_USER_DATA_BUFFERS           4

/// Maximum size in bytes of user data payload (headers excluded).
#define STM_SE_USER_DATA_MAX_PAYLOAD_SIZE   2048

/// @deprecated: Use STM_SE_USER_DATA_MAX_PAYLOAD_SIZE
#define STM_SE_MAX_USER_DATA_SIZE               STM_SE_USER_DATA_MAX_PAYLOAD_SIZE



// Codec IDs
#define STM_SE_USER_DATA_MPEG2_CODEC_ID       0
#define STM_SE_USER_DATA_H264_CODEC_ID        1
#define STM_SE_USER_DATA_HEVC_CODEC_ID        1 /* Since the Hevc_user_data_parameters_t and H264_user_data_parameters_s are typedef
                                                   structures referring to same data, keep the same codec id*/

/*!
 * This type describes the MPEG2 codec user data.
 *
 * User data buffers passed to clients contain this header after
 * stm_se_user_data_generic_parameters_t if codec_id ==
 * STM_SE_USER_DATA_MPEG2_CODEC_ID.
 */
typedef struct stm_se_mpeg2_user_data_parameters_s
{
    unsigned    reserved: 31; // reserved=31
    unsigned    top_field_first: 1; // top_field_first=1
} stm_se_mpeg2_user_data_parameters_t;


/*!
 * This type describes the H264 codec user data.
 *
 * User data buffers passed to clients contain this header after
 * stm_se_user_data_generic_parameters_t if codec_id ==
 * STM_SE_USER_DATA_H264_CODEC_ID.
 */
typedef struct stm_se_h264_user_data_parameters_s
{
    unsigned    reserved: 31; //  reserved=31
    unsigned    is_registered: 1; //  is_registered=1
    unsigned    itu_t_t35_country_code: 8; // size=8
    unsigned    itu_t_t35_country_code_extension_byte: 8; // size=8
    unsigned    itu_t_t35_provider_code: 16; // size=16
    char        uuid_iso_iec_11578[16]; // size=128
} stm_se_h264_user_data_parameters_t;

/*!
 * This type describes the Hevc codec user data.
 *
 * User data buffers passed to clients contain this header after
 * stm_se_user_data_generic_parameters_t if codec_id ==
 * STM_SE_USER_DATA_HEVC_CODEC_ID.
 */
typedef struct stm_se_h264_user_data_parameters_s stm_se_hevc_user_data_parameters_t;

/*!
 * This type describes the generic codec user data.
 *
 * This type is used internally but user data buffers passed to clients have a
 * different layout.
 */
typedef union stm_se_user_data_codec_parameters_s
{
    stm_se_mpeg2_user_data_parameters_t  MPEG2_user_data_parameters;
    stm_se_h264_user_data_parameters_t   H264_user_data_parameters;
    stm_se_hevc_user_data_parameters_t   Hevc_user_data_parameters;
} stm_se_codec_user_data_parameters_t;

/*!
 * This type describes the user data generic parameters. This structure is formatted.
 *
 * User data buffers passed to clients start with this header.
 */
typedef struct stm_se_user_data_generic_parameters_s
{
    char        reserved[4];  // reserved = 32 'STUD'
    unsigned    block_length: 16; // block_length=16
    unsigned    header_length: 16; // header_length=16
    unsigned    clipped: 1; // clipped=1
    unsigned    padding_bytes: 5; // padding bytes=5
    unsigned    stream_abridgement: 1; // stream_abridgement=1
    unsigned    overflow: 1; // buffering_overflow=1
    unsigned    codec_id: 8; // codec_id=8
    unsigned    reserved_2: 6; // reserved=6
    unsigned    is_there_a_pts: 1; // is_there_a_pts=1
    unsigned    is_pts_interpolated: 1; // is_pts_interpolated=1
    unsigned    reserved_3: 7; // reserved=7
    unsigned    pts_msb: 1; // pts_msb=1
    unsigned    pts: 32; // PTS=32
} stm_se_user_data_generic_parameters_t;


/*!
 * This type describes the user data additional parameters.
 *
 * This type is used internally but user data buffers passed to clients have a
 * different layout.
 */
typedef struct stm_se_user_data_additional_parameters_s
{
    unsigned char                     length;
    bool                              available;

    stm_se_codec_user_data_parameters_t         codec_user_data_parameters;
} stm_se_user_data_additional_parameters_t;


/*!
 * This type describes the global user data structure.
 *
 * This type is used internally but user data buffers passed to clients have a
 * different layout.
 */
typedef struct stm_se_user_data_s
{
    stm_se_user_data_generic_parameters_t       user_data_generic_parameters;           // Generic parameters
    stm_se_user_data_additional_parameters_t    user_data_additional_parameters;        // Additional parameters specific to codec
    unsigned char                               user_data_payload[STM_SE_USER_DATA_MAX_PAYLOAD_SIZE];
} stm_se_user_data_t;

#define SizeofUserData(Entries)       ((Entries) * (sizeof(stm_se_user_data_generic_parameters_t) + sizeof(stm_se_user_data_additional_parameters_t) + STM_SE_USER_DATA_MAX_PAYLOAD_SIZE))

// Maximum padding in bytes added before payload to align payload on 64-bit
// boundary.
#define STM_SE_USER_DATA_MAX_PADDING    8

/*!
 * Maximum size in bytes of buffer (headers + payload) copied by SE into
 * client-provided buffer via STM_SE_PLAY_STREAM_OUTPUT_PORT_ANCILLIARY.
 *
 * The ancilliary port does not deliver partial user data buffers.  If the
 * client-provided buffer is too small, no user data is delivered.  Clients must
 * therefore provide a buffer that is at least as big as this maximum size.
 */
#define STM_SE_USER_DATA_BUFFER_SIZE    \
        (sizeof(stm_se_user_data_generic_parameters_t) + \
         sizeof(stm_se_codec_user_data_parameters_t) + \
         STM_SE_USER_DATA_MAX_PAYLOAD_SIZE + \
         STM_SE_USER_DATA_MAX_PADDING)

/*! @} */ /* user_data */

#endif /* STM_SE_USER_DATA_H_ */
