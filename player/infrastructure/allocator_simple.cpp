/************************************************************************
Copyright (C) 2003-2014 STMicroelectronics. All Rights Reserved.

This file is part of the Streaming Engine.

Streaming Engine is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License version 2 as
published by the Free Software Foundation.

Streaming Engine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with Streaming Engine; see the file COPYING.  If not, write to the Free
Software Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307,
USA.

The Streaming Engine Library may alternatively be licensed under a
proprietary license from ST.
************************************************************************/

#include "osinline.h"

#include "report.h"
#include "allocator_simple.h"

#undef TRACE_TAG
#define TRACE_TAG "AllocatorSimple_c"

// ------------------------------------------------------------------------
// Macro

#define On64MBBoundary( addr ) ((!mAllowCross64MbBoundary) && ((((uintptr_t)(addr) + (uintptr_t)mPhysicalAddress) & 0x03ffffff) == 0))

/* report if waiting for memory more then 30s */
#define MAX_EXPECTED_WAIT_FOR_MEM_PERIOD  30000

/* report if waiting for Allocate exit more then 500ms */
#define MAX_EXPECTED_WAIT_FOR_ALLOCATE_EXIT  500


AllocatorSimple_c *AllocatorSimple_c::New(unsigned int   BufferSize,
                                          unsigned int   SegmentSize,
                                          unsigned char *PhysicalAddress,
                                          bool           AllowCross64MbBoundary)
{
    AllocatorSimple_c *p = new AllocatorSimple_c(BufferSize, SegmentSize, PhysicalAddress, AllowCross64MbBoundary);
    if (p != NULL)
    {
        if (p->FinalizeInit() != AllocatorNoError)
        {
            delete p;
            p = NULL;
        }
    }

    return p;
}

// ------------------------------------------------------------------------
// Constructor function
AllocatorSimple_c::AllocatorSimple_c(unsigned int     BufferSize,
                                     unsigned int     SegmentSize,
                                     unsigned char   *PhysicalAddress,
                                     bool             AllowCross64MbBoundary)
    : mBufferSize(BufferSize)
    , mSegmentSize(SegmentSize)
    , mPhysicalAddress(PhysicalAddress)
    , mAllowCross64MbBoundary(AllowCross64MbBoundary)
    , mLock()
    , mBeingDeleted(false)
    , mInAllocate(false)
    , mRejectAllocations(false)
    , mBlocks()
    , mMaxPossibleBlockSize(0)
    , mEntryFreed()
    , mExitAllocate()
{
    OS_InitializeMutex(&mLock);
    OS_InitializeEvent(&mEntryFreed);
    OS_InitializeEvent(&mExitAllocate);

    SE_VERBOSE(group_buffer, "allocator 0x%p BufferSize %d SegmentSize %d\n",
               this, mBufferSize, mSegmentSize);
    SE_ASSERT(SegmentSize > 0);
}

// ------------------------------------------------------------------------
// Destructor function

AllocatorSimple_c::~AllocatorSimple_c()
{
    SE_VERBOSE(group_buffer, ">allocator 0x%p\n", this);

    OS_LockMutex(&mLock);
    mBeingDeleted = true;
    OS_SetEvent(&mEntryFreed);
    OS_UnLockMutex(&mLock);

    if (mInAllocate)
    {
        unsigned long long EntryTime = OS_GetTimeInMicroSeconds();
        SE_INFO(group_buffer, "allocator 0x%p Waiting to exit allocate\n", this);

        OS_Status_t WaitStatus;
        do
        {
            WaitStatus = OS_WaitForEventAuto(&mExitAllocate, MAX_EXPECTED_WAIT_FOR_ALLOCATE_EXIT);
            if (WaitStatus == OS_TIMED_OUT)
            {
                SE_WARNING("allocator 0x%p still waiting to exit allocate\n", this);
            }
        }
        while (WaitStatus == OS_TIMED_OUT);

        SE_INFO(group_buffer, "allocator 0x%p Time to exit allocate %llu\n", this, OS_GetTimeInMicroSeconds() - EntryTime);
    }

    OS_TerminateEvent(&mEntryFreed);
    OS_TerminateEvent(&mExitAllocate);
    OS_TerminateMutex(&mLock);
    SE_VERBOSE(group_buffer, "<allocator 0x%p\n", this);
}

// ------------------------------------------------------------------------
// Allocate function

AllocatorStatus_t   AllocatorSimple_c::Allocate(
    unsigned int     *Size,
    unsigned int     *Offset,
    bool              AllocateLargest,
    bool              NonBlocking)
{
    if (*Size > mMaxPossibleBlockSize)
    {
        SE_ERROR("allocator 0x%p Could not allocate %d bytes from buffer of %d bytes with MaxPossibleBlockSize %d\n",
                 this, *Size, mBufferSize, mMaxPossibleBlockSize);
        return AllocatorNoMemory;
    }

    OS_LockMutex(&mLock);
    SE_VERBOSE(group_buffer, ">allocator 0x%p Offset 0x%u Size %d Free block count %d\n",
               this, *Offset, *Size, mBlocks.Size());
    OS_ResetEvent(&mExitAllocate);
    mInAllocate  = true;

    if (!AllocateLargest)
    {
        *Size = (((*Size + mSegmentSize - 1) / mSegmentSize) * mSegmentSize);
    }

    do
    {
        OS_ResetEvent(&mEntryFreed);

        if (mBeingDeleted)
        {
            SE_INFO(group_buffer, "allocator 0x%p is being deleted\n", this);
            // Need to unblock destructor waiting for Allocate exit
            OS_SetEvent(&mExitAllocate);
            break;
        }

        if (AllocateLargest)
        {
            int i   = LargestFreeBlock();

            if ((i >= 0) && (mBlocks[i].Size >= (*Size)))
            {
                *Size = mBlocks[i].Size;
                *Offset = mBlocks[i].Offset;
                mBlocks[i].Size  = 0;
                mBlocks[i].InUse = false;
                SE_VERBOSE(group_buffer, "<allocator 0x%p Size %d LargestFreeBlock block[%d]\n",
                           this, *Size, i);
                goto exit_no_error;
            }
        }
        else
        {
            for (int i = 0, MaxBlocks = mBlocks.Size(); i < MaxBlocks; i++)
                if (mBlocks[i].InUse && (mBlocks[i].Size >= *Size))
                {
                    *Offset = mBlocks[i].Offset;
                    mBlocks[i].Offset += *Size;
                    mBlocks[i].Size -= *Size;

                    if (mBlocks[i].Size == 0)
                    {
                        mBlocks[i].InUse = false;
                    }

                    SE_VERBOSE(group_buffer, "<allocator 0x%p Size %d Free block count %d block[%d].Size %d\n",
                               this, *Size, mBlocks.Size(), i, mBlocks[i].Size);
                    goto exit_no_error;
                }
        }

        SE_VERBOSE(group_buffer, "allocator 0x%p Waiting for memory size %d\n", this, *Size);
        OS_UnLockMutex(&mLock);

        if (!NonBlocking && !mRejectAllocations)
        {
            OS_Status_t WaitStatus;
            do
            {
                WaitStatus = OS_WaitForEventAuto(&mEntryFreed, MAX_EXPECTED_WAIT_FOR_MEM_PERIOD);
                if (WaitStatus == OS_TIMED_OUT)
                {
                    SE_WARNING("allocator 0x%p still waiting to allocate %d bytes\n", this, *Size);
                    TraceBlocks();
                }
            }
            while (WaitStatus == OS_TIMED_OUT && mBeingDeleted == false && !mRejectAllocations);
        }

        OS_LockMutex(&mLock);
    }
    while (!NonBlocking && !mRejectAllocations);

    mInAllocate   = false;
    OS_UnLockMutex(&mLock);

    if (mRejectAllocations)
    {
        SE_DEBUG(group_buffer, "<allocator 0x%p AllocatorAllocationRejected\n", this);
        return AllocatorAllocationRejected;
    }

    SE_INFO(group_buffer, "<allocator 0x%p AllocatorUnableToAllocate\n", this);
    return AllocatorUnableToAllocate;

exit_no_error:
    mInAllocate       = false;
    OS_UnLockMutex(&mLock);
    return AllocatorNoError;
}

// ------------------------------------------------------------------------
// Second-phase constructor
AllocatorStatus_t   AllocatorSimple_c::FinalizeInit()
{
    OS_LockMutex(&mLock);

    unsigned int LocalBufferSize     = mBufferSize;
    unsigned int LocalOffset         = 0;
    unsigned int MaxBlockSize        = 0x4000000 - ((uintptr_t)mPhysicalAddress & 0x03ffffff);

    if (mBlocks.Reserve(INITIAL_CAPACITY) != OS_NO_ERROR)
    {
        OS_UnLockMutex(&mLock);
        SE_ERROR("allocator 0x%p Unable to reserve free blocks\n", this);
        return AllocatorNoMemory;
    }

    mMaxPossibleBlockSize    = 0;

    if (mAllowCross64MbBoundary == true)
    {
        AllocatorSimpleBlock_t Block;
        Block.InUse      = true;
        Block.Size       = mBufferSize;
        Block.Offset     = 0;
        if (mBlocks.PushBack(Block) != OS_NO_ERROR)
        {
            OS_UnLockMutex(&mLock);
            SE_ERROR("allocator 0x%p Unable to allocate initial free block\n", this);
            return AllocatorNoMemory;
        }
        mMaxPossibleBlockSize  = mBlocks[0].Size;
    }
    else
    {
        for (int i = 0; LocalBufferSize != 0; i++)
        {
            AllocatorSimpleBlock_t Block;
            Block.InUse      = true;
            Block.Size       = Min(LocalBufferSize, MaxBlockSize);
            Block.Offset     = LocalOffset;
            if (mBlocks.PushBack(Block) != OS_NO_ERROR)
            {
                OS_UnLockMutex(&mLock);
                SE_ERROR("allocator 0x%p Unable to allocate 64MiB free block\n", this);
                return AllocatorNoMemory;
            }

            LocalBufferSize     -= Block.Size;
            LocalOffset         += Block.Size;
            MaxBlockSize         = 0x4000000;
            mMaxPossibleBlockSize  = Max(mMaxPossibleBlockSize, Block.Size);
        }
    }

    SE_VERBOSE(group_buffer, "allocator 0x%p BufferSize %d Free block count %d MaxPossibleBlockSize: %d mBlocks[%d].Size:%d\n",
               this, mBufferSize, mBlocks.Size(), mMaxPossibleBlockSize, mBlocks.Size() - 1, mBlocks.Back().Size);

    OS_SetEvent(&mEntryFreed);
    OS_UnLockMutex(&mLock);

    return AllocatorNoError;
}

// ------------------------------------------------------------------------
// Free range of entries

AllocatorStatus_t   AllocatorSimple_c::Free(unsigned int      Size,
                                            unsigned int      Offset)
{
    OS_LockMutex(&mLock);
    SE_VERBOSE(group_buffer, ">allocator 0x%p Offset 0x%u Size %d Free block count %d\n",
               this, Offset, Size, mBlocks.Size());
    Size                    = (((Size + mSegmentSize - 1) / mSegmentSize) * mSegmentSize);

    int LowestFreeBlock     = mBlocks.Size();

    //
    // Note by adding adjacent block records to the one we are trying to free,
    // this loop does concatenation of free blocks as well as freeing the
    // current block.
    //

    for (int i = 0, MaxBlocks = mBlocks.Size(); i < MaxBlocks; i++)
    {
        if (mBlocks[i].InUse)
        {
            if (((Offset + Size) == mBlocks[i].Offset) &&
                !On64MBBoundary(mBlocks[i].Offset))
            {
                Size            += mBlocks[i].Size;
                mBlocks[i].InUse  = false;
            }
            else if ((Offset == (mBlocks[i].Offset + mBlocks[i].Size)) &&
                     !On64MBBoundary(Offset))
            {
                Size            += mBlocks[i].Size;
                Offset            = mBlocks[i].Offset;
                mBlocks[i].InUse  = false;
            }
        }

        if (!mBlocks[i].InUse)
        {
            LowestFreeBlock      = Min(LowestFreeBlock, i);
        }
    }

    if (LowestFreeBlock < mBlocks.Size())
    {
        // Recycle existing unused free block.
        mBlocks[LowestFreeBlock].InUse       = true;
        mBlocks[LowestFreeBlock].Size        = Size;
        mBlocks[LowestFreeBlock].Offset      = Offset;

        // Remove top unused free blocks to minimize number of iteratiions when
        // looking for free block.
        while (mBlocks.Back().InUse == false)
        {
            mBlocks.PopBack();
        }
    }
    else
    {
        // Append new free block.
        AllocatorSimpleBlock_t NewBlock;
        NewBlock.InUse       = true;
        NewBlock.Size        = Size;
        NewBlock.Offset      = Offset;
        if (mBlocks.PushBack(NewBlock) != OS_NO_ERROR)
        {
            TraceBlocks();
            OS_SetEvent(&mEntryFreed);
            OS_UnLockMutex(&mLock);
            // TODO(theryn): Stop gap measure until ticket 92412 implemented.
            // In the unlikely case that we run out of memory when growing the
            // vector, we panic rather than logging an error and returning to
            // ease debugging .
            SE_FATAL("allocator 0x%p Unable to create a free block - OOM\n", this);
            return AllocatorNoMemory;
        }
    }

    SE_VERBOSE(group_buffer, "<allocator 0x%p Size %d Free block count %d mBlocks[LowestFreeBlock %d].Size %d\n",
               this, Size, mBlocks.Size(), LowestFreeBlock, mBlocks[LowestFreeBlock].Size);
    OS_SetEvent(&mEntryFreed);
    OS_UnLockMutex(&mLock);
    return AllocatorNoError;
}


// ------------------------------------------------------------------------
// Inform the caller of the size of the largest free block

AllocatorStatus_t   AllocatorSimple_c::LargestFreeBlock(unsigned int     *Size)
{
    int Index;
    OS_LockMutex(&mLock);
    Index   = LargestFreeBlock();
    *Size   = (Index < 0) ? 0 : mBlocks[Index].Size;
    OS_UnLockMutex(&mLock);
    return AllocatorNoError;
}


// ------------------------------------------------------------------------
// Private - internal function to find the largest free block

int   AllocatorSimple_c::LargestFreeBlock()
{
    int     Index = -1;
    for (int i = 0, MaxBlocks = mBlocks.Size(); i < MaxBlocks; i++)
    {
        if (mBlocks[i].InUse && ((Index < 0) || (mBlocks[i].Size > mBlocks[Index].Size)))
        {
            Index = i;
        }
    }

    return Index;
}

void AllocatorSimple_c::TraceBlocks()
{
    SE_INFO(group_buffer, "allocator 0x%p Free block count %d\n", this, mBlocks.Size());

    for (int i = 0, MaxBlocks = mBlocks.Size(); i < MaxBlocks; i++)
    {
        if (mBlocks[i].InUse)
        {
            SE_INFO(group_buffer, "mBlocks[%d] Offset 0x%u Size %d\n", i,
                    mBlocks[i].Offset, mBlocks[i].Size);
        }
    }
}

void AllocatorSimple_c::RejectAllocations()
{
    SE_DEBUG(group_buffer, "0x%p\n", this);
    mRejectAllocations = true;
    OS_SetEvent(&mEntryFreed);
}

void AllocatorSimple_c::AcceptAllocations()
{
    SE_DEBUG(group_buffer, "0x%p\n", this);
    mRejectAllocations = false;
}


