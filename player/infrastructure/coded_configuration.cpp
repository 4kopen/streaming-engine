/************************************************************************
Copyright (C) 2003-2016 STMicroelectronics. All Rights Reserved.

This file is part of the Streaming Engine.

Streaming Engine is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License version 2 as
published by the Free Software Foundation.

Streaming Engine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with Streaming Engine; see the file COPYING.  If not, write to the Free
Software Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307,
USA.

The Streaming Engine Library may alternatively be licensed under a
proprietary license from ST.
************************************************************************/

#include "coded_configuration.h"

#include "osinline.h"

CodedConfiguration_s::CodedConfiguration_s()
    : FrameCount(0)
    , MemorySize(0)
    , FrameMaximumSize(0)
    , MemoryPartitionName()
{
}

CodedConfiguration_s::CodedConfiguration_s(unsigned int FrameCount, unsigned int MemorySize,
                                           unsigned int FrameMaximumSize, const char *PartitionName)
    : FrameCount(FrameCount)
    , MemorySize(MemorySize)
    , FrameMaximumSize(FrameMaximumSize)
    , MemoryPartitionName()
{
    strlcpy(MemoryPartitionName, PartitionName, sizeof(MemoryPartitionName));
}
