/************************************************************************
Copyright (C) 2003-2014 STMicroelectronics. All Rights Reserved.

This file is part of the Streaming Engine.

Streaming Engine is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License version 2 as
published by the Free Software Foundation.

Streaming Engine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with Streaming Engine; see the file COPYING.  If not, write to the Free
Software Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307,
USA.

The Streaming Engine Library may alternatively be licensed under a
proprietary license from ST.
************************************************************************/

#ifndef H_DATA_BLOCK
#define H_DATA_BLOCK

#include "osinline.h"

struct stm_data_block; // do not #include definition - see below

// Toll-free stm_data_block wrapper.
//
// This structure can be directly casted to and from stm_data_block.
//
// This wrapper prevents including the corresponding STKPI header everywhere in
// SE.
typedef struct DataBlock_s
{
    void *DataAddr;
    uint32_t Len;
    DataBlock_s *Next;

    static DataBlock_s *FromStkpi(struct stm_data_block *Block);

    DataBlock_s()
        : DataAddr(NULL)
        , Len(0)
        , Next(NULL)
    {
    }
} DataBlock_t;

#endif
