/************************************************************************
Copyright (C) 2003-2014 STMicroelectronics. All Rights Reserved.

This file is part of the Streaming Engine.

Streaming Engine is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License version 2 as
published by the Free Software Foundation.

Streaming Engine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with Streaming Engine; see the file COPYING.  If not, write to the Free
Software Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307,
USA.

The Streaming Engine Library may alternatively be licensed under a
proprietary license from ST.
************************************************************************/

#if __KERNEL__

#include <linux/kernel.h>
#include <linux/sysrq.h>

#define errmsg(fmt, ...) pr_crit(fmt, ##__VA_ARGS__)

#else

#include <assert.h>
#include <stdlib.h>
#include <stdio.h>
#include <stdarg.h>

/* This differs from the __KERNEL__ version of the same macro because
 * there no need to concatenate KERN_CRIT to the format string (and
 * therefore we can avoid using GNU extensions for this version).
 */
#define errmsg(...) do { printf(__VA_ARGS__); fflush(stdout); } while (0)

#endif

#include "report.h"
#include "fatal_error.h"

// player/infrastructure internal API.  Do not use elsewhere
void vreport_print(const char *format, va_list ap);

/* --- */

static void default_error_handler(void *ctx, stm_object_h badobject)
{
    if (badobject)
    {
        errmsg("Streaming engine failure (or condition violation) when acting upon object at 0x%p (ctx:%p)\n",
               badobject, ctx);
    }
    else
    {
        errmsg("Internal streaming engine failure (or condition violation) (ctx:%p)\n", ctx);
    }

#if __KERNEL__
    BUG();
#else
    assert(0);
#endif
}

/* --- */

static void *error_ctx = NULL;
static stm_error_handler error_handler = default_error_handler;
static unsigned int dump_tasks_on_fatal_error;

void register_fatal_error_handler_tuneables(void)
{
    OS_RegisterTuneable("dump_tasks_on_fatal_error", &dump_tasks_on_fatal_error);
}

void unregister_fatal_error_handler_tuneables(void)
{
    OS_UnregisterTuneable("dump_tasks_on_fatal_error");
}

void set_fatal_error_handler(void *ctx, stm_error_handler handler)
{
    error_ctx = ctx;
    error_handler = handler;
}

void fatal_error(const char *fmt, ...)
{
    va_list ap;

#if __KERNEL__
    if (dump_tasks_on_fatal_error)
    {
        handle_sysrq('t');
    }
#endif

    va_start(ap, fmt);
    vreport_print(fmt, ap);
    va_end(ap);

    error_handler(error_ctx, NULL);
}

#if __KERNEL__
// Used by SDP Collator Host
EXPORT_SYMBOL(fatal_error);
#endif
