/************************************************************************
Copyright (C) 2003-2016 STMicroelectronics. All Rights Reserved.

This file is part of the Streaming Engine.

Streaming Engine is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License version 2 as
published by the Free Software Foundation.

Streaming Engine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with Streaming Engine; see the file COPYING.  If not, write to the Free
Software Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307,
USA.

The Streaming Engine Library may alternatively be licensed under a
proprietary license from ST.
************************************************************************/

#ifndef H_LIST
#define H_LIST

#include "raw_list.h"

// Type-safe intrusive linked list modeled after boost::intrusive::list.
//
// To link objects together in an intrusive list they must embed a Link_c
// instance.
//
// This template is parameterized by the type of items to thread together and
// the position of the Link_c object inside the item.  The latter is given as a
// pointer to a member variable (which is basically an offset from start of item
// wrapped in C++ clothes).
//
// For tutorial, please see ListExample unit tests.
//
// For more information and inspiration for extensions, please refer to boost
// documentation.
//
// Under the hood, this class is a thin wrapper over RawList_c that maps Link_c
// objects to/from the objects they are embedded into.  The meat is in
// RawList_c.  Please keep it this way to avoid code bloat caused by template
// instantiation.
//
// Thread-safety: Clients must protect access to lists shared by several
// threads.
template<class ITEM, Link_c ITEM::*LINK>
class List_c
{
public:

    // Bi-directional smart pointer to an item in a list generally used for
    // iterating lists front to back.
    //
    // Callers are responsible for no decrementing or incrementing iterators
    // past the bounds of the list they point to.
    class Iterator_c
    {
    public:
        Iterator_c() : mLink(NULL) {}

        // Comparison operators.
        // Passing by value is more efficient as Iterator_c is single word.
        inline bool operator==(Iterator_c Other) const { return mLink == Other.mLink; }
        inline bool operator!=(Iterator_c Other) const { return !(*this == Other); }

        // Move iterator to next item in list.
        Iterator_c &operator++()    { mLink = mLink->Next(); return *this; }
        Iterator_c operator++(int)  { Iterator_c Old = *this; mLink = mLink->Next(); return Old; }

        // Move iterator to previous item in list.
        Iterator_c &operator--()    { mLink = mLink->Prev(); return *this; }
        Iterator_c operator--(int)  { Iterator_c Old = *this; mLink = mLink->Prev(); return Old; }

        // Return pointed item.
        ITEM &operator*() const     { return *ContainerOf(mLink); }
        ITEM *operator->() const    { return ContainerOf(mLink); }

    private:
        Link_c *mLink;

        explicit Iterator_c(Link_c *Link) : mLink(Link) {}

        friend class List_c;
    };

    // Reverse iterator generally used for iterating list back to front.
    //
    // See Iterator_c.
    class ReverseIterator_c
    {
    public:
        ReverseIterator_c() : mLink(NULL) {}

        // Comparison operators.
        // Passing by value is more efficient as ReverseIterator_c is single word.
        inline bool operator==(ReverseIterator_c Other) const { return mLink == Other.mLink; }
        inline bool operator!=(ReverseIterator_c Other) const { return !(*this == Other); }

        // Move iterator to previous item in list.
        ReverseIterator_c &operator++()     { mLink = mLink->Prev(); return *this; }
        ReverseIterator_c operator++(int)   { ReverseIterator_c Old = *this; mLink = mLink->Prev(); return Old; }

        // Move iterator to next item in list.
        ReverseIterator_c &operator--()     { mLink = mLink->Next(); return *this; }
        ReverseIterator_c operator--(int)   { ReverseIterator_c Old = *this; mLink = mLink->Next(); return Old; }

        // Return pointed item.
        ITEM &operator*() const     { return *ContainerOf(mLink); }
        ITEM *operator->() const    { return ContainerOf(mLink); }

    private:
        Link_c *mLink;

        explicit ReverseIterator_c(Link_c *Link) : mLink(Link) {}

        friend class List_c;
    };

    // Create empty list.
    List_c() : mRaw() {}

    // Return iterator to first item in list.
    Iterator_c Begin()          { return Iterator_c(mRaw.Anchor()->Next()); }

    // Return iterator pointing one-past last item in list.
    Iterator_c End()            { return Iterator_c(mRaw.Anchor()); }

    // Return reverse iterator to last item in list.
    ReverseIterator_c Rbegin()  { return ReverseIterator_c(mRaw.Anchor()->Prev()); }

    // Return reverse iterator pointing one-past first item in list.
    ReverseIterator_c Rend()    { return ReverseIterator_c(mRaw.Anchor()); }

    // Return true if there are no item in list.
    bool IsEmpty() const        { return mRaw.IsEmpty(); }

    // Insert specified item at beginning of list.
    void PushFront(ITEM &Item)  { mRaw.PushFront(&(Item.*LINK)); }

    // Insert specified item at end of list.
    void PushBack(ITEM &Item)   { mRaw.PushBack(&(Item.*LINK)); }

    // Return first item in list.
    const ITEM &Front() const   { return *ContainerOf(mRaw.Front()); }
    ITEM &Front()               { return *ContainerOf(mRaw.Front()); }

    // Return last item in list.
    const ITEM &Back() const    { return *ContainerOf(mRaw.Back()); }
    ITEM &Back()                { return *ContainerOf(mRaw.Back()); }

    // Unlink first item in list.
    void PopFront()             { mRaw.Front()->Unlink(); };

    // Unlink last item in list.
    void PopBack()              { mRaw.Back()->Unlink(); };

    // Insert specified item before item pointed to by iterator.
    void Insert(Iterator_c Iter, ITEM &Item)    { Iter.mLink->InsertBefore(&(Item.*LINK)); }

    // Unlink item pointed to by iterator and return iterator to following item.
    Iterator_c Erase(Iterator_c Iter)   { Iterator_c Next(Iter.mLink->Next()); Iter.mLink->Unlink(); return Next; }

    // Unlink all items in list. O(N) as must unlink all items individually.
    void Clear()                        { mRaw.Clear(); }

    // Return iterator pointing to specified object.
    static Iterator_c ToIterator(ITEM &Item) { SE_ASSERT((Item.*LINK).IsLinked()); return Iterator_c(&(Item.*LINK)); }

#ifdef SE_ASSERT_ENABLED
    // Fire an assertion if list is malformed or if the number of linked items is not in
    // [Min, Max].  The lower (resp. upper) bound check is disabled when Min (resp.
    // Max) is negative.
    void AssertValid(int Min = -1, int Max = -1) const
    {
        mRaw.AssertValid(Min, Max);
    }
#else
    void AssertValid(int Min = -1, int Max = -1) const {}
#endif

private:
    RawList_c mRaw;

    // Magic for mapping a Link_c object to the item embedding it.
    static ITEM *ContainerOf(Link_c *Link)
    {
        unsigned long Offset = reinterpret_cast<unsigned long>(&(static_cast<ITEM *>(NULL)->*LINK));
        return reinterpret_cast<ITEM *>(reinterpret_cast<uint8_t *>(Link) - Offset);
    }

    DISALLOW_COPY_AND_ASSIGN(List_c);
};

#endif
