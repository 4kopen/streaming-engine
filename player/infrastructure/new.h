/************************************************************************
Copyright (C) 2003-2014 STMicroelectronics. All Rights Reserved.

This file is part of the Streaming Engine.

Streaming Engine is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License version 2 as
published by the Free Software Foundation.

Streaming Engine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with Streaming Engine; see the file COPYING.  If not, write to the Free
Software Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307,
USA.

The Streaming Engine Library may alternatively be licensed under a
proprietary license from ST.
************************************************************************/

//
// This file contains a subset of the standard C++ <new> header.
//

#include "osinline.h"

#ifndef H_NEW
#define H_NEW

#ifdef UNITTESTS

// Unit tests use Google Mock which includes the standard <new> header which
// conflicts with the local placement new declarations in this file so use
// the standard version in this case.
#include <new>

#else

//
// Default new and delete operators.
//

void *operator new(size_t size);
void *operator new[](size_t size);
void operator delete(void *mem);
void operator delete[](void *mem);

//
// Default explicit placement new operators.
// We do not need explicit placement delete operators because they are called
// only if placement new throws an exception and we do not use exceptions
// (http://en.cppreference.com/w/cpp/memory/new/operator_delete).
//

inline void *operator new(size_t /* size */, void *p)
{
    return p;
}

inline void *operator new[](size_t /* size */, void *p)
{
    return p;
}

#endif // __KERNEL__

#endif // H_NEW
