/************************************************************************
Copyright (C) 2003-2014 STMicroelectronics. All Rights Reserved.

This file is part of the Streaming Engine.

Streaming Engine is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License version 2 as
published by the Free Software Foundation.

Streaming Engine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with Streaming Engine; see the file COPYING.  If not, write to the Free
Software Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307,
USA.

The Streaming Engine Library may alternatively be licensed under a
proprietary license from ST.
************************************************************************/

#include "osinline.h"
#include "se_limits.h"
#include "rational.h"

#define MASK_MSB32_BITS 0XFFFFFFFF00000000ULL
#define MASK_LSB32_BITS 0X00000000FFFFFFFFULL

// allow some margin when checking equality (in 2^(-31) resolution)
#define UQ31_EQUALITY_MARGIN 50  // 0,23*10^-9

#undef TRACE_TAG
#define TRACE_TAG "Rational_c"

// default ctor
Rational_c::Rational_c()
    : mInteger(0)
    , mUq(0)
    , mStrictNegative(false)
    , mHintDen(0)
{
}

// ctors casts from integer
Rational_c::Rational_c(long long N)
    : mInteger((uint64_t)abs64(N))
    , mUq(0)
    , mStrictNegative(N < 0)
    , mHintDen(0)
{
}

Rational_c::Rational_c(unsigned long long N)
    : mInteger(N)
    , mUq(0)
    , mStrictNegative(false)
    , mHintDen(0)
{
}

Rational_c::Rational_c(int N)
    : mInteger((uint64_t)abs64(N))
    , mUq(0)
    , mStrictNegative(N < 0)
    , mHintDen(0)
{
}

Rational_c::Rational_c(unsigned int N)
    : mInteger((uint64_t)N)
    , mUq(0)
    , mStrictNegative(false)
    , mHintDen(0)
{
}

// ctor signed Numerator and Denominator
Rational_c::Rational_c(int64_t N, int64_t D)
    : mInteger(0)  // default - to be computed
    , mUq(0)   // default - to be computed
    , mStrictNegative((N < 0) ^ (D < 0))
    , mHintDen(0)
{
    FactoryNumDen(abs64(N), abs64(D));
}

// internal ctor unsigned integer, UQ31 and sign
// RemN is u64 as could be > 2^31 : will be reduced to In + UQn with UQn in u32
Rational_c::Rational_c(uint64_t I, uint64_t RemN, bool Neg)
    : mInteger(I)
    , mUq(0)   // default - to be computed
    , mStrictNegative((I == 0 && RemN == 0) ? false : Neg)
    , mHintDen(0)
{
    // update Integer and compute UQ31
    if (RemN >= (1ULL << 31))
    {
        uint64_t IntegerR = (RemN >> 31);
        mUq  = (uint32_t)(RemN - (IntegerR << 31)); // >= 0 and < 2^31 by definition
        mInteger += IntegerR;
    }
    else
    {
        mUq = (uint32_t)RemN;
    }
}

#define WARN_COUNTCALLS_LEVEL 16
void Rational_c::FactoryNumDen(uint64_t N, uint64_t D)
{
    // forbid building rational with denominator 0
    if (unlikely(D == 0))
    {
        SE_ERROR("denominator 0\n");
        if (mInteger == 0 && mUq == 0) { mStrictNegative = false; }
        return; // keep default values
    }

    // compute Integer part, update N,D to represent fractional part
    mInteger = ComputerIntegerPartN64D64(N, D);
    N = N - mInteger * D; // >= 0 by definition

    // compute UQ31 = E[2^31*N/D]
    if (N == 0)
    {
        mUq = 0;
    }
    else if ((D & MASK_MSB32_BITS) == 0)
    {
        uint32_t RestD = (uint32_t)D; // ok since denominator restricted to u32 range
        mHintDen = RestD; // remember initial Denominator U32
        mUq = (uint32_t)N; // > 0 and < RestD by definition

        if (RestD != (1 << 31))
        {
            // change to UQ31 mode
            // round to nearest integer (N << 31)/RestD + 0.5 => ((N<<32 + RestD)/2) / RestD
            mUq = (uint32_t)(((((uint64_t)mUq << 32) + RestD) >> 1) / RestD);
        }
    }
    else
    {
        // don't restrict N/D to Nu32/Du32 (with shifts) to avoid precision loss
        // also 2^31 * N might exceed u64 (if N above u32)
        // so find integer part of 2^31*N/D
        // = 2^(31-k)*(2^k*N/D) = 2^(31-k)*(I' + N'/D') = 2^(31-k)*I' + 2^(31-k)*(I''+N''/D''))
        // with k so that 2^k * N is within u64;
        // k forced to be > 0: if N too big, force a shift on N and D of 1
        // so recurse on 31-k
        // note: computing N/(D*2^-31) is not ok as might lead to too small denominator
        int targetshift = 31;
        uint32_t Uq = 0;
        uint64_t Nb = N;
        uint64_t Db = D;
        int countcalls = 0;
        while (targetshift != 0)
        {
            countcalls++;
            int deltashiftN = ((Nb & MASK_MSB32_BITS) != 0) ? 32 - OS_Fls(Nb >> 32) : 31; // so range 0..31
            if (deltashiftN > targetshift) { deltashiftN = targetshift; }
            if (deltashiftN == 0)
            {
                // N is big with bit63 set.. no alternative
                // can happen only when called from divide operator (not from ctors)
                SE_DEBUG(group_misc, "forced Rshift on %llx %llx\n", Nb, Db);
                Nb = (Nb == ULLONG_MAX) ? Nb >> 1 : (Nb + 1) >> 1;
                Db = (Db == ULLONG_MAX) ? Db >> 1 : (Db + 1) >> 1;
                deltashiftN = 1; // force algo convergence
            }

            Nb = Nb << deltashiftN;
            targetshift -= deltashiftN;
            uint64_t Ib = ComputerIntegerPartN64D64(Nb, Db);
            Nb = Nb - Ib * Db;
            Uq += (Ib << targetshift);

            if (targetshift == 0)
            {
                // final rounding part Nb/Db + 0.5 => 2Nb + Db / (2*Db)
                // can not divide by Db => direct compare
                if (((Nb << 1) + Db) >= (Db << 1)) { Uq += 1; }
            }
        }

        if (countcalls > WARN_COUNTCALLS_LEVEL)
        {
            SE_DEBUG(group_misc, "Uq 0x%x computed in %d iters for 0x%llx / 0x%llx\n", Uq, countcalls, N, D);
        }

        mUq = Uq;
    }

    if (mUq >= (1 << 31))
    {
        // can happen due to rounding: for instance 2^31*(2^32-1)/2^32=2^31-1/2
        mUq = 0;
        if (mInteger < ULLONG_MAX) { mInteger += 1; }
    }

    if (mUq == 0 && mInteger == 0)
    {
        mStrictNegative = false;
        if (N != 0)
        {
            // could be an issue if rational used later for division
            SE_DEBUG(group_misc, "very small 0x%llx / 0x%llx leading to UQ=0\n", N, D);
        }
    }
}

// compute integer part of N/D
uint64_t Rational_c::ComputerIntegerPartN64D64(uint64_t N, uint64_t D)
{
    uint64_t integer = 0;

    if (N >= D)
    {
        if ((D & MASK_MSB32_BITS) == 0)
        {
            // can divide by D since msb32 not set
            uint32_t d32 = (uint32_t)D;
            integer = (uint64_t)(N / d32); // prefer u64/u32 to u64/u64
        }
        else
        {
            // cannot divide by D since msb32 set (cf div64 / aeabi_ldivmod.c)
            // compute Integer part 'manually' to avoid Den u32 restrict operation
            // and associated loss of precision and overhead
            // N and D both above u32 (since N >= D)
            // N/D = 2^-k * N/(D*2^-k) with k such that D/2^k is u32
            // actually compute 2^k*E[N/(D >>k)] so might have to tune resulting integer
            // (D >> k) <= (D*2^-k) ==> E[2^k*N/(D >> k)] >= E[2^k*N/(D*2^-k)] == E[N/D]
            // but 2^k*E[N/(D>>k)] <= E[2^k*N/(D >> k)]
            int kshift = OS_Fls(D >> 32);
            integer = (N / (D >> kshift));
            integer >>= kshift;
            uint64_t multipleD = (D * integer);
            int countcalls = 0;
            while (multipleD > N)
            {
                multipleD -= D;
                integer--;
                countcalls++;
            }
            while (multipleD <= (N - D))
            {
                multipleD += D;
                integer++;
                countcalls++;
            }
            if (countcalls > WARN_COUNTCALLS_LEVEL)
            {
                SE_WARNING("countcalls %d %llx/%llx\n", countcalls, N, D);
            }
        }
    } // else trivial 0

    return integer;
}

// pgcd for a and b with a >= b
uint32_t Rational_c::PgcdR(uint32_t a, uint32_t b)
{
    SE_ASSERT(a >= b);
    if (b == 0) { return 1; }

    uint32_t rem;
    while ((rem = a % b) != 0)
    {
        a = b;
        b = rem;
    }
    return b;
}

// trivial 10 power factor
int Rational_c::TenPowerMultFactor(unsigned int Places)
{
    switch (Places)
    {
    case 0: return 1;
    case 3: return 1000;
    case 6: return 1000000;
    default:
    case 9: return 1000000000;
    }
}

// ////////////////////////////////////////////////////////////////////////
//
// operators
//

Rational_c &Rational_c::operator= (const Rational_c &R)
{
    if (this != &R)
    {
        mInteger = R.mInteger;
        mUq  = R.mUq;
        mStrictNegative = R.mStrictNegative;
        mHintDen = R.mHintDen;
    }

    return *this;
}

bool Rational_c::operator== (const Rational_c &F)
{
    // compare using difference to allow easy allowance of equality margin
    Rational_c Delta = *this - F;
    if (Delta.mInteger == 0)
    {
        if (Delta.mUq == 0)
        {
            return true;
        }
        else if (Delta.mUq <= UQ31_EQUALITY_MARGIN)
        {
            SE_DEBUG(group_misc, "small delta %x\n", Delta.mUq);
            return true;
        }
    }

    return false;
}

bool Rational_c::operator> (const Rational_c &F)
{
    // since operator== might tolerate some margin, check it first
    if (*this == F) { return false; }

    if (mStrictNegative == F.mStrictNegative)
    {
        if (mStrictNegative)
        {
            // both < 0
            return ((F.mInteger > mInteger) ||
                    ((mInteger == F.mInteger) && (F.mUq > mUq)));
        }
        else
        {
            // both > 0
            return ((mInteger > F.mInteger) ||
                    ((mInteger == F.mInteger) && (mUq > F.mUq)));
        }
    }
    else
    {
        // opposite signs
        if (!mStrictNegative) { return true; }
        // else false
    }

    return false;
}

Rational_c Rational_c::operator+ (const Rational_c &F)
{
    if (mStrictNegative == F.mStrictNegative)
    {
        // both > 0 or both < 0
        // Int1 + Int2 + (UQ31_1 + UQ31_2)/2^31 (case both same sign)
        return Rational_c(mInteger + F.mInteger, mUq + F.mUq, mStrictNegative);
    }
    else
    {
        if (mStrictNegative)
        {
            // this < 0, F >= 0
            // -Int1 + Int2 + (-UQ31_1 + UQ31_2)/2^31
            // move to form 'this >= 0, F < 0'
            Rational_c Fcopy = F;
            return Fcopy + *this;
        }

        // different signs, sum sign to be computed
        // this >= 0, F < 0
        // Int1 - Int2 + (UQ31_1 - UQ31_2)/2^31
        // DeltaI1I2 + DeltaU1U2/2^31
        // for rational, integer and uq31 have to be both same sign
        int64_t DeltaI1I2 = (mInteger >= F.mInteger) ? (int64_t)(mInteger - F.mInteger) : -(int64_t)(F.mInteger - mInteger);
        int32_t DeltaU1U2 = mUq > F.mUq ? (int32_t)(mUq - F.mUq) : -(int32_t)(F.mUq - mUq);

        if (DeltaU1U2 >= 0)
        {
            if (DeltaI1I2 >= 0)
            {
                return Rational_c((uint64_t)DeltaI1I2, DeltaU1U2, false);
            }
            else
            {
                // DeltaI1I2 (<0) + deltaU1U2.mUq (>=0)/2^31 => sign issue (have to be same sign)
                // -((-DeltaI1I2 -1) + (1 - deltaU1U2.mUq/2^31)) => ok since 2^31 - deltaU1U2 > 0 and -deltaI1I2 -1 >= 0
                return Rational_c((uint64_t)(-DeltaI1I2 - 1), (1ULL << 31) - DeltaU1U2, true);
            }
        }
        else
        {
            if (DeltaI1I2 <= 0)
            {
                return Rational_c((uint64_t)(-DeltaI1I2), (uint32_t)(-DeltaU1U2), true);
            }
            else
            {
                return Rational_c((uint64_t)(DeltaI1I2 - 1), (1ULL << 31) + DeltaU1U2, false);
            }
        }
    }
}

Rational_c Rational_c::operator- (const Rational_c &F)
{
    Rational_c Finvert = F;
    Finvert.mStrictNegative = F.mStrictNegative ? false : true;
    return *this + Finvert;
}

Rational_c Rational_c::operator* (const Rational_c &F)
{
    // (Int1 + UQ31_1/2^31)*(Int2 + UQ31_2/2^31)
    // Int1*Int2 + UQ31_1*Int2/2^31 + UQ31_2*Int1/2^31 + UQ31_1*UQ31_2/(2^31*2^31)

    // warn on overflow on Integer * F.Integer
    if (unlikely(((mInteger & MASK_MSB32_BITS) != 0) && ((F.mInteger & MASK_MSB32_BITS) != 0)))
    {
        SE_WARNING("ovflow I 0x%llx x I 0x%llx\n", mInteger, F.mInteger);
    }
    uint64_t I1I2 = mInteger * F.mInteger;

    // to avoid intermediate overflow => N1*I2/2^31 = (N1*I2h*2^32 + N1*I2l)/2^31 => N1 * 2 * I2h + (N1*I2l)/2^31
    // N1 * 2 * I2h won't overflow since N1 < 2^31
    uint64_t N1I2i = (uint64_t)(mUq << 1) * (uint32_t)(F.mInteger >> 32);
    uint64_t N1I2q = (uint64_t)mUq * (uint32_t)(F.mInteger & MASK_LSB32_BITS);
    uint64_t N2I1i = (uint64_t)(F.mUq << 1) * (uint32_t)(mInteger >> 32);
    uint64_t N2I1q = (uint64_t)F.mUq * (uint32_t)(mInteger & MASK_LSB32_BITS);

    // Uq1 * Uq2 / (2^31 * 2^31) ==> round to nearest integer Uq1*Uq2/2^31 => small precision loss
    uint64_t N1N2r = ((uint64_t)mUq * F.mUq + (1ULL << 30)) >> 31;

    return Rational_c(I1I2 + N1I2i + N2I1i, N1I2q + N2I1q + N1N2r, (mStrictNegative ^ F.mStrictNegative));
}

Rational_c Rational_c::operator/ (const Rational_c &F)
{
    // forbid divide by 0
    if (F.mInteger == 0 && F.mUq == 0)
    {
        SE_WARNING("op/ with 0 => forcing UQ 1\n");
        Rational_c Tmp = 0;
        Tmp.mUq = 1;
        return *this / Tmp;
    }

    // (Int1 + UQ31_1/2^31)/(Int2 + UQ31_2/2^31)
    // (Int1*2^31 + UQ31_1)/(Int2*2^31 + UQ31_2)
    // if Integer part is big, I1*D might overflows
    // ((Int1*2^31/Shift1 + UQ31_1/Shift1)/(Int2*2^31/Shift2 + UQ31_2/Shift2)) * Shift1/Shift2

    uint32_t shiftI1;
    uint64_t Num1 = GetRu64NumDenshift(&shiftI1);
    uint32_t shiftI2;
    uint64_t Num2 = F.GetRu64NumDenshift(&shiftI2);

    // since might use 'big' Num1 and Num2 => might exceed LLONG_MAX => dont use default ctor(N, D)
    // which takes int64_t and computes sign and takes absolute values so cast signed => unsigned
    Rational_c Inv1;
    Inv1.FactoryNumDen(Num1, Num2);

    if (shiftI1 != shiftI2)
    {
        Rational_c Inv2;
        Inv2.FactoryNumDen((1ULL << shiftI1), (1ULL << shiftI2));
        Inv1 *= Inv2;
    }

    return Rational_c(Inv1.mInteger, Inv1.mUq, (mStrictNegative ^ F.mStrictNegative));
}

// ////////////////////////////////////////////////////////////////////////
//
// Extraction
//

#define CHECK_FOR_OVERFLOW_LONG(value, ret) \
    if (mStrictNegative == false && value > LLONG_MAX) \
    { \
        SE_VERBOSE(group_misc, "high 0x%llx\n", value); \
    } \
    else if (mStrictNegative == true && value > LLONG_MAX) \
    { \
        SE_WARNING("ovflow - 0x%llx\n", value); \
        ret = LLONG_MAX; \
    }

#define CHECK_FOR_OVERFLOW_INT(value, ret) \
    if (mStrictNegative == false && value > UINT_MAX) \
    { \
        SE_WARNING("ovflow + 0x%llx\n", value); \
        ret = UINT_MAX; \
    } \
    else if (mStrictNegative == true && value > INT_MAX) \
    { \
        SE_WARNING("ovflow - 0x%llx\n", value); \
        ret = INT_MAX; \
    }

uint64_t Rational_c::UnsignedLongLongIntegerPart() const
{
    uint64_t ret = mInteger;
    CHECK_FOR_OVERFLOW_LONG(mInteger, ret);
    return ret;
}

uint32_t Rational_c::UnsignedIntegerPart() const
{
    uint32_t ret = (uint32_t)mInteger;
    CHECK_FOR_OVERFLOW_INT(mInteger, ret);
    return ret;
}

uint64_t Rational_c::RoundedUnsignedLongLongIntegerPart(unsigned int Places) const
{
    int      multfactor   = TenPowerMultFactor(Places);
    uint32_t remainderdec = (uint32_t)((mUq * (uint64_t)multfactor + (1ULL << 30)) >> 31); // can't overflow by definition
    uint64_t rounded      = mInteger + remainderdec / multfactor;
    CHECK_FOR_OVERFLOW_LONG(rounded, rounded);
    return rounded;
}

uint32_t Rational_c::RoundedUnsignedIntegerPart(unsigned int Places) const
{
    uint64_t rounded = RoundedUnsignedLongLongIntegerPart(Places);
    CHECK_FOR_OVERFLOW_INT(rounded, rounded);
    return (uint32_t)rounded;
}

uint64_t Rational_c::RoundedUpUnsignedLongLongIntegerPart() const
{
    // no places => rounding with multfactor 1
    uint64_t roundedup = mInteger + (uint32_t)((mUq + ((1ULL << 31) - 1)) >> 31);
    CHECK_FOR_OVERFLOW_LONG(roundedup, roundedup);
    return roundedup;
}

uint32_t Rational_c::RoundedUpUnsignedIntegerPart() const
{
    // no places => rounding with multfactor 1
    uint64_t roundedup = RoundedUpUnsignedLongLongIntegerPart();
    CHECK_FOR_OVERFLOW_INT(roundedup, roundedup);
    return (uint32_t)roundedup;
}

uint32_t Rational_c::UnsignedRemainderDecimal(unsigned int Places) const
{
    int multfactor = TenPowerMultFactor(Places);
    uint32_t remainderdec = (uint32_t)((mUq * (uint64_t)multfactor) >> 31); // can't overflow by definition
    return remainderdec;
}

uint32_t Rational_c::RoundedUnsignedRemainderDecimal(unsigned int Places) const
{
    int multfactor = TenPowerMultFactor(Places);
    uint32_t remainderdec = (uint32_t)((mUq * (uint64_t)multfactor + (1ULL << 30)) >> 31); // can't overflow by definition
    return remainderdec;
}

// restricted numerator U64 and denominator shift value
// integer part of numerator is exact, fractional part may be restricted (loss precision) if shift not 0
uint64_t Rational_c::GetRu64NumDenshift(uint32_t *den_shift) const
{
    SE_ASSERT(den_shift != NULL);
    SE_ASSERT((mInteger << 0) == mInteger);
    SE_ASSERT((mInteger >> 0) == mInteger);

    // (Int + UQ31/2^31) => (Int*2^31 + UQ31) / 2^31
    // (Int*2^31 + UQ31) numerator will overflow if Int is above u32
    // so transform to form
    // (Int*2^31/shift + UQ31/shift) * shift
    // with shift biggest such that Int*2^31/shift does not overflow
    uint32_t deltashift = ((mInteger & MASK_MSB32_BITS) != 0) ? (uint32_t)(32 - OS_Fls(mInteger >> 32)) : 31; // so range 0..31
    SE_ASSERT(deltashift <= 31);
    uint64_t Is = (mInteger << deltashift);
    // rounded UQ31/shift (no rounding in case deltashift is 31)
    uint32_t Nr = (mUq + ((1 << (31 - deltashift)) >> 1)) >> (31 - deltashift);

    if (deltashift != 31)
    {
        SE_DEBUG(group_misc, "shift %d 0x%llx 0x%x -> 0x%llx 0x%x\n", 31 - deltashift, mInteger, mUq, Is, Nr);
    }

    // check Is+Nr does not overflow => divide by 2 (but might have precision lost this way)
    if (Is + Nr >= ULLONG_MAX)
    {
        SE_DEBUG(group_misc, "ovflow %d 0x%llx 0x%x -> 0x%llx 0x%x\n", 31 - deltashift, mInteger, mUq, Is, Nr);
        if (deltashift > 0)
        {
            Is >>= 1;
            Nr >>= 1;
            deltashift--;
        }
    }

    // => return 'new' numerator and denominator shift
    *den_shift = (uint32_t)(31 - deltashift);
    return Is + Nr;
}

// retrieve *unsigned* numerator and denominator 32bits
// this API is used only for aspect ratio and frame rate
// => matchtype param to specialize the API ratio/framerate
void Rational_c::GetRu32NumeratorDenominator(uint32_t *Num, uint32_t *Den, int matchtype) const
{
    // have to return restricted numerator and denominator down to 32bits
    SE_ASSERT(Num != NULL);
    SE_ASSERT(Den != NULL);

    uint64_t gnum = 0;
    uint64_t gden = 0; // keep default value 0

    if (((mInteger & MASK_MSB32_BITS) != 0) || mUq == 0)
    {
        // if Integer above U32, can not provide numerator below u32
        // if mUq 0, direct gnum gden
        gnum = mInteger;
        gden = 1;
    }
    else
    {
        if (matchtype == MATCH_ASPECTRATIO || matchtype == MATCH_FRAMERATE)
        {
            // try to match for special 'known' denominators in case of aspect ratio or of framerate
            // first value is special => replaced by mHintDen
            // compare using rounding at 10^-6
            const unsigned int known_pixelratio_dens[] = { 0, 1, 2, 3, 9, 10, 11, 17, 30, 33, 99, 100 };
            const unsigned int known_framerate_dens[] = { 0, 1000, 1001, 32, 1000000 };
            const unsigned int *pdens;
            int asize;
            if (matchtype == MATCH_FRAMERATE)
            {
                pdens = known_framerate_dens;
                asize = ARRAY_SIZE(known_framerate_dens);
            }
            else
            {
                pdens = known_pixelratio_dens;
                asize = ARRAY_SIZE(known_pixelratio_dens);
            }

            for (int i = 0; i < asize; i++)
            {
                unsigned int rd = (i != 0) ? pdens[i] : mHintDen;
                if (rd == 0) { continue; } // case mHintDen not set

                // check if equal to rn / rd at 10^-6 with known rd
                Rational_c Tmp(mInteger, mUq, false);
                Tmp *= (Rational_c)rd;
                uint32_t rem = Tmp.RoundedUnsignedRemainderDecimal(6);
                if (rem == 0 || rem == 1000000)
                {
                    gnum = (uint64_t)Tmp.RoundedIntegerPart(6);
                    gden = (uint64_t)rd;

                    if (gnum != 0)
                    {
                        // little bit costly pgcd reduction but needed as CDI code has naive implementation
                        uint32_t pgcd = gnum > gden ? PgcdR(gnum, gden) : PgcdR(gden, gnum);
                        gnum /= pgcd;
                        gden /= pgcd;
                    }
                    break;
                }
            }

            if (gden == 0)
            {
                SE_VERBOSE(group_misc, "no match found for 0x%llx 0x%x I:%lld Rem:%d type %d\n",
                           mInteger, mUq, RoundedLongLongIntegerPart(6), (int)UnsignedRemainderDecimal(6), matchtype);
            }
        }

        if (gden == 0)
        {
            if (mInteger == 0)
            {
                // limit to INT_MAX => q30
                gnum = (uint64_t)(mUq + 1) >> 1;
                gden = (1ULL << 30);
            }
            else
            {
                // Integer part below U32 => restrict UQ31/2^31 so that (Int*2^31/shift + UQ31/shift) * shift / 2^31 within u32
                uint32_t deltashift = (uint32_t)(32 - OS_Fls(mInteger & MASK_LSB32_BITS)); // so range 0..31 since Integer not 0
                SE_ASSERT(deltashift <= 31);
                if (deltashift == 0)
                {
                    // if Integer above INT_MAX, can not provide u32
                    gnum = mInteger;
                    gden = 1;
                }
                else
                {
                    deltashift--; // so range 0..30
                    uint32_t Is = ((uint32_t)mInteger << deltashift);
                    uint32_t Nr = (mUq + ((1 << (31 - deltashift)) >> 1)) >> (31 - deltashift);

                    // check Is+Nr does not overflow on q31
                    gnum = Is + Nr;
                    if (gnum > INT_MAX && deltashift > 1)
                    {
                        deltashift--;
                        Is = ((uint32_t)mInteger << deltashift);
                        Nr = (mUq + ((1 << (31 - deltashift)) >> 1)) >> (31 - deltashift);
                    }
                    gden = (1ULL << deltashift);
                }
            }

            // little bit costly pgcd reduction, but ok since API shall not be called too often
            uint32_t pgcd = gnum > gden ? PgcdR(gnum, gden) : PgcdR(gden, gnum);
            gnum /= pgcd;
            gden /= pgcd;
        }
    }

    // limit range to int32 (and not uint32)
    // also check pixel aspect ratio and frame rate values not above uint16
    // since users might to use overflow-proof arithmetics
    if (gnum > INT_MAX)
    {
        SE_WARNING("ovflow gnum 0x%llx - 0x%llx 0x%x\n", gnum, mInteger, mUq);
        gnum = INT_MAX;
    }
    else if (gnum > USHRT_MAX && (matchtype == MATCH_ASPECTRATIO || matchtype == MATCH_FRAMERATE))
    {
        SE_WARNING("over u16 for type:%d gnum 0x%llx - 0x%llx 0x%x\n",
                   matchtype,
                   gnum, mInteger, mUq);
    }

    if (gden > INT_MAX)
    {
        SE_WARNING("ovflow gden 0x%llx - 0x%llx 0x%x\n", gden, mInteger, mUq);
        gden = INT_MAX;
    }
    else if (gden > USHRT_MAX && (matchtype == MATCH_ASPECTRATIO || matchtype == MATCH_FRAMERATE))
    {
        SE_WARNING("over u16 for type:%d gden 0x%llx - 0x%llx 0x%x\n",
                   matchtype,
                   gden, mInteger, mUq);
    }

    *Num = (uint32_t)gnum;
    *Den = (uint32_t)gden;
}

Rational_c Rational_c::ConvertDisplayFramerateToRational(uint32_t displayVerticalRefreshRate)
{
    switch (displayVerticalRefreshRate)
    {
    case 59940:
        return Rational_c(60000, 1001);

    case 23976:
        return Rational_c(24000, 1001);

    case 29970:
        return Rational_c(30000, 1001);

    default:
        return Rational_c(displayVerticalRefreshRate, 1000);
    }
}

