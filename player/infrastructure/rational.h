/************************************************************************
Copyright (C) 2003-2014 STMicroelectronics. All Rights Reserved.

This file is part of the Streaming Engine.

Streaming Engine is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License version 2 as
published by the Free Software Foundation.

Streaming Engine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with Streaming Engine; see the file COPYING.  If not, write to the Free
Software Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307,
USA.

The Streaming Engine Library may alternatively be licensed under a
proprietary license from ST.
************************************************************************/

#ifndef H_RATIONAL
#define H_RATIONAL

class Rational_c
{
public:
    // default ctor
    Rational_c();
    // ctor for implicit/explicit casts from integer values
    // do not use 64_t and 32_t types to be compatible with OS21 compilation
    Rational_c(long long N);
    explicit Rational_c(unsigned long long N);
    Rational_c(int N);
    explicit Rational_c(unsigned int N);
    // ctor signed Numerator and Denominator
    Rational_c(int64_t N, int64_t D);

    // static methods, as could be used for different purposes
    // compute integer part of N/D
    static uint64_t ComputerIntegerPartN64D64(uint64_t N, uint64_t D);
    // pgcd for a and b with a >= b
    static uint32_t PgcdR(uint32_t a, uint32_t b);
    // utility to return 10^places
    static int TenPowerMultFactor(unsigned int Places);

    // ////////////////////////////////////////////////////////////////////////
    //
    // operators
    //

    Rational_c &operator= (const Rational_c &R);

    bool operator== (const Rational_c &F);
    bool operator!= (const Rational_c &F) { return !(*this == F); }
    bool operator> (const Rational_c &F);
    bool operator>= (const Rational_c &F) { return (*this > F || *this == F); }
    bool operator< (const Rational_c &F)  { return !(*this >= F); }
    bool operator<= (const Rational_c &F) { return !(*this > F); }

    Rational_c operator+ (const Rational_c &F);
    Rational_c operator+= (const Rational_c &F) { return (*this = *this + F); }
    Rational_c operator- (const Rational_c &F);
    Rational_c operator-= (const Rational_c &F) { return (*this = *this - F); }

    Rational_c operator* (const Rational_c &F);
    Rational_c operator*= (const Rational_c &F) { return (*this = *this * F); }
    Rational_c operator/ (const Rational_c &F);
    Rational_c operator/= (const Rational_c &F) { return (*this = *this / F); }

    // ////////////////////////////////////////////////////////////////////////
    //
    // Extraction
    //

    // 'raw' accessors; 'raw' meaning unsigned and unrounded
    uint64_t GetRawInteger() const { return mInteger; }
    uint32_t GetRawUq31()    const { return mUq;      }
    bool     IsNegative()    const { return mStrictNegative; }

    // Remainder rational
    Rational_c Remainder() const { return Rational_c(0, mUq, mStrictNegative); }

    // unsigned integer part accessors: either u64 or u32, not rounded, rounded, rounded-up versions
    uint64_t UnsignedLongLongIntegerPart() const;
    uint32_t UnsignedIntegerPart() const;
    uint64_t RoundedUnsignedLongLongIntegerPart(unsigned int Places = 6) const;
    uint32_t RoundedUnsignedIntegerPart(unsigned int Places = 6) const;
    uint64_t RoundedUpUnsignedLongLongIntegerPart() const;
    uint32_t RoundedUpUnsignedIntegerPart() const;

    // signed integer part accessors: either s64 or s32, not rounded, rounded, rounded-up versions
    int64_t LongLongIntegerPart() const
    { return mStrictNegative ? -UnsignedLongLongIntegerPart() : UnsignedLongLongIntegerPart(); }
    int32_t IntegerPart() const
    { return mStrictNegative ? -UnsignedIntegerPart() : UnsignedIntegerPart(); }
    int64_t RoundedLongLongIntegerPart(unsigned int Places = 0) const
    { return mStrictNegative ? -RoundedUnsignedLongLongIntegerPart(Places) : RoundedUnsignedLongLongIntegerPart(Places); }
    int32_t RoundedIntegerPart(unsigned int Places = 0) const
    { return mStrictNegative ? -RoundedUnsignedIntegerPart(Places) : RoundedUnsignedIntegerPart(Places); }
    int64_t RoundedUpLongLongIntegerPart() const
    { return mStrictNegative ? -RoundedUpUnsignedLongLongIntegerPart() : RoundedUpUnsignedLongLongIntegerPart(); }
    int32_t RoundedUpIntegerPart() const
    { return mStrictNegative ? -RoundedUpUnsignedIntegerPart() : RoundedUpUnsignedIntegerPart(); }

    // unsigned decimal (power 10^places) remainders: not rounded and rounded versions
    uint32_t UnsignedRemainderDecimal(unsigned int Places = 6) const;
    uint32_t RoundedUnsignedRemainderDecimal(unsigned int Places = 6) const;

    // signed decimal (power 10^places) remainders: not rounded and rounded versions
    int32_t  RemainderDecimal(unsigned int Places = 6) const
    { return mStrictNegative ? -(int32_t)UnsignedRemainderDecimal(Places) : (int32_t)UnsignedRemainderDecimal(Places); }
    int32_t  RoundedRemainderDecimal(unsigned int Places = 6) const
    { return mStrictNegative ? -(int32_t)RoundedUnsignedRemainderDecimal(Places) : (int32_t)RoundedUnsignedRemainderDecimal(Places); }

    // restricted numerator U64 and denominator shift value
    // integer part of numerator is exact, fractional part may be restricted (loss precision) if shift not 0
    uint64_t GetRu64NumDenshift(uint32_t *den_shift) const;

    // retrieve *unsigned* numerator and denominator 32bits
    // this API is used only for aspect ratio and frame rate
    // => add param to specialize the search of best matching ratio
    enum MatchType_e { MATCH_NA = 0, MATCH_FRAMERATE = 1, MATCH_ASPECTRATIO = 2 };
    void GetRu32NumeratorDenominator(uint32_t *Num, uint32_t *Den, int matchtype = MATCH_NA) const;
    static Rational_c ConvertDisplayFramerateToRational(uint32_t displayVerticalRefreshRate);


private:
    // Numerator/Denominator = unsigned Integer64bits + unsigned Fractional (<1) + sign
    // fractional part done in UQ31 representation
    // UQ31 = E[2^31 * (N-I*D) / D]
    // resolution 2^31 == 0,47 10-9
    // designed to be safe to be 'memset zero' (even if not recommended)
    uint64_t mInteger;    // integer part
    uint32_t mUq;         // numerator of fractional part ==> UQ31 < 2^31
    bool mStrictNegative; // true if Numerator/Denominator < 0

    uint32_t mHintDen; // hint denominator U32

    // internal ctor unsigned integer, UQ31 and sign
    // RemN is u64 as could be > 2^31 : will be reduced to I + In + UQn with UQn in u32
    Rational_c(uint64_t I, uint64_t RemN, bool Neg);

    // internal factory
    void FactoryNumDen(uint64_t N, uint64_t D);
};

typedef Rational_c Rational_t;

// //////////////////////////////////////////////////////////////////////////////////////////
//
//  cases where the non rational is on the left
//

static inline Rational_c operator+ (int64_t I, Rational_c F) { return F + (Rational_c)I; }
static inline Rational_c operator- (int64_t I, Rational_c F) { return (Rational_c)I - F; }
static inline Rational_c operator* (int64_t I, Rational_c F) { return F * (Rational_c)I; }
static inline Rational_c operator/ (int64_t I, Rational_c F) { return (Rational_c)I / F; }

// //////////////////////////////////////////////////////////////////////////////////////////
//
//  Supplying the useful extraction bits as standalone functions
//

static inline Rational_c Remainder(Rational_c R) { return R.Remainder(); }

static inline uint64_t UnsignedLongLongIntegerPart(Rational_c R) { return R.UnsignedLongLongIntegerPart(); }
static inline uint32_t UnsignedIntegerPart(Rational_c R) { return R.UnsignedIntegerPart(); }

static inline int64_t LongLongIntegerPart(Rational_c R) { return R.LongLongIntegerPart(); }
static inline int32_t IntegerPart(Rational_c R) { return R.IntegerPart(); }

static inline int64_t RoundedLongLongIntegerPart(Rational_c R) { return R.RoundedLongLongIntegerPart(); }
static inline int32_t RoundedIntegerPart(Rational_c R) { return R.RoundedIntegerPart(); }

static inline uint32_t UnsignedRemainderDecimal(Rational_c R) { return R.UnsignedRemainderDecimal(); }
static inline uint32_t RoundedUnsignedRemainderDecimal(Rational_c R) { return R.RoundedUnsignedRemainderDecimal(); }

static inline int32_t RemainderDecimal(Rational_c R) { return R.RemainderDecimal(); }
static inline int32_t RoundedRemainderDecimal(Rational_c R) { return R.RoundedRemainderDecimal(); }

#endif
