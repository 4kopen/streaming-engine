/************************************************************************
Copyright (C) 2003-2016 STMicroelectronics. All Rights Reserved.

This file is part of the Streaming Engine.

Streaming Engine is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License version 2 as
published by the Free Software Foundation.

Streaming Engine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with Streaming Engine; see the file COPYING.  If not, write to the Free
Software Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307,
USA.

The Streaming Engine Library may alternatively be licensed under a
proprietary license from ST.
************************************************************************/

#include "raw_list.h"

//////////////////////////////////////////////////////////////////////////////
// Link_c

// Remove from linked list.  Must be in linked list.
void Link_c::Unlink()
{
    SE_ASSERT(IsLinked());

    mPrev->mNext = mNext;
    mNext->mPrev = mPrev;
    mPrev = mNext = NULL;
}

// Remove from linked list if linked.  Do nothing otherwise.
void Link_c::UnlinkIfLinked()
{
    if (IsLinked())
    {
        mPrev->mNext = mNext;
        mNext->mPrev = mPrev;
        mPrev = mNext = NULL;
    }
}

// Insert provided link in linked list before this link.
void Link_c::InsertBefore(Link_c *Other)
{
    SE_ASSERT(IsLinked());
    SE_ASSERT(Other->IsLinked() == false);

    Other->mPrev = mPrev;
    Other->mNext = this;
    Other->mPrev->mNext = Other;
    mPrev = Other;
}

// Insert provided link in linked list after this link.
void Link_c::InsertAfter(Link_c *Other)
{
    SE_ASSERT(IsLinked());
    SE_ASSERT(Other->IsLinked() == false);

    Other->mPrev = this;
    Other->mNext = mNext;
    Other->mNext->mPrev = Other;
    mNext = Other;
}

#ifdef SE_ASSERT_ENABLED

// Fire an assertion failure if linkage is incorrect.
void Link_c::AssertValid() const
{
    if (IsLinked())
    {
        SE_ASSERT(mNext->mPrev == this);
        SE_ASSERT(mPrev->mNext == this);
    }
    else
    {
        SE_ASSERT(mPrev == NULL && mNext == NULL);
    }
}

#endif

//////////////////////////////////////////////////////////////////////////////
// RawList_c

void RawList_c::Clear()
{
    while (IsEmpty() == false)
    {
        Front()->Unlink();
    }
}

#ifdef SE_ASSERT_ENABLED

// Fire an assertion if list is malformed or if the number of linked items is not in
// [Min, Max].  The lower (resp. upper) bound check is disabled when Min (resp.
// Max) is negative.
void RawList_c::AssertValid(int Min, int Max) const
{
    // Check forward links.
    int ForwardCount = 0;
    for (const Link_c *Link = mAnchor.mNext; Link != &mAnchor; Link = Link->mNext)
    {
        ForwardCount++;
    }

    // Check backward links.
    int BackwardCount = 0;
    for (const Link_c *Link = mAnchor.mPrev; Link != &mAnchor; Link = Link->mPrev)
    {
        BackwardCount++;
    }

    // Check number of linked items.
    SE_ASSERT(ForwardCount == BackwardCount);
    SE_ASSERT(Min < 0 || Min <= ForwardCount);
    SE_ASSERT(Max < 0 || ForwardCount <= Max);
}

#endif
