/************************************************************************
Copyright (C) 2003-2016 STMicroelectronics. All Rights Reserved.

This file is part of the Streaming Engine.

Streaming Engine is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License version 2 as
published by the Free Software Foundation.

Streaming Engine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with Streaming Engine; see the file COPYING.  If not, write to the Free
Software Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307,
USA.

The Streaming Engine Library may alternatively be licensed under a
proprietary license from ST.
************************************************************************/

#ifndef H_RAW_LIST
#define H_RAW_LIST

#include "osinline.h"

// Link in list managed by RawList_c or List_c.
//
// To use, embed at an arbitrary position in objects to link together:
//
// struct Foo_s
// {
//      ...
//      Link_c mLink;
//      ...
// };
//
// It is possible to embed several Link_c instances in a single object.
class Link_c
{
public:
    // Create an unlinked link.
    Link_c() : mNext(NULL), mPrev(NULL) {}

    bool IsLinked() const   { return mPrev != NULL && mNext != NULL; }
    Link_c *Next() const    { return mNext; }
    Link_c *Prev() const    { return mPrev; }

    void Unlink();
    void UnlinkIfLinked();
    void InsertBefore(Link_c *Other);
    void InsertAfter(Link_c *Other);

#ifdef SE_ASSERT_ENABLED
    void AssertValid() const;
#else
    void AssertValid() const    {}
#endif

private:
    Link_c *mNext;
    Link_c *mPrev;

    Link_c(Link_c *Prev, Link_c *Next) : mNext(Next), mPrev(Prev) {}

    DISALLOW_COPY_AND_ASSIGN(Link_c);

    friend class RawList_c;
};

// Type-unsafe generic intrusive doubly-linked list.
//
// This class is primarily a building block for the type-safe List_c.  Consider
// using the latter rather than using this class directly.
//
// This class maintains a dummy link, the anchor, that points to the first and
// last link in list.
class RawList_c
{
public:
    // Create an empty list.
    RawList_c() : mAnchor(&mAnchor, &mAnchor) {}

    Link_c *Anchor()        { return &mAnchor; }
    Link_c *Front() const   { SE_ASSERT(!IsEmpty()); return mAnchor.mNext; }
    Link_c *Back() const    { SE_ASSERT(!IsEmpty()); return mAnchor.mPrev; }
    bool IsEmpty() const    { return mAnchor.mPrev == &mAnchor && mAnchor.mNext == &mAnchor; }

    void PushFront(Link_c *Link)    { mAnchor.InsertAfter(Link); }
    void PushBack(Link_c *Link)     { mAnchor.InsertBefore(Link); }
    void Clear();

#ifdef SE_ASSERT_ENABLED
    void AssertValid(int Min = -1, int Max = -1) const;
#else
    void AssertValid(int Min = -1, int Max = -1) const {}
#endif

private:
    // Dummy link.  mNext points to front link and mPrev to back link.
    Link_c mAnchor;

    DISALLOW_COPY_AND_ASSIGN(RawList_c);
};

#endif
