/************************************************************************
Copyright (C) 2003-2014 STMicroelectronics. All Rights Reserved.

This file is part of the Streaming Engine.

Streaming Engine is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License version 2 as
published by the Free Software Foundation.

Streaming Engine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with Streaming Engine; see the file COPYING.  If not, write to the Free
Software Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307,
USA.

The Streaming Engine Library may alternatively be licensed under a
proprietary license from ST.
************************************************************************/

#ifndef H_RAW_RING
#define H_RAW_RING

#include "osinline.h"
#include "port.h"

#undef TRACE_TAG
#define TRACE_TAG "RawRing_c"

// Generic FIFO implemented as heap-allocated circular array.
//
// Clients must serialize access to instances if they are accessed concurrently.
template<typename T>
class RawRing_c
{
public:
    explicit RawRing_c(unsigned int MaxEntries);
    ~RawRing_c();
    RingStatus_t FinalizeInit();

    void Reset();

    RingStatus_t Insert(const T &Value);
    RingStatus_t InsertFront(const T &Value);
    RingStatus_t Extract(T *Value);
    RingStatus_t ExtractLastInserted(T *Value);
    RingStatus_t Peek(T *Value) const;

    bool IsFullyInitialized() const     { return mStorage != NULL; }
    bool NonEmpty() const               { return mNextExtract != mNextInsert; }
    unsigned int NbOfEntries() const    { return (mNextInsert + mLimit - mNextExtract) % mLimit; }
    bool Full() const                   { return NbOfEntries() == mLimit - 1; }

private:
    unsigned int    mLimit;
    unsigned int    mNextExtract;
    unsigned int    mNextInsert;
    T              *mStorage;

    DISALLOW_COPY_AND_ASSIGN(RawRing_c);
};

template<typename T>
RawRing_c<T>::RawRing_c(unsigned int MaxEntries)
    : mLimit(MaxEntries + 1)
    , mNextExtract(0)
    , mNextInsert(0)
    , mStorage(NULL)
{
}

template<typename T>
RingStatus_t RawRing_c<T>::FinalizeInit()
{
    mStorage = new T[mLimit];
    return (mStorage != NULL) ? RingNoError : RingNoMemory;
}

template<typename T>
RawRing_c<T>::~RawRing_c()
{
    delete[] mStorage;
}

template<typename T>
RingStatus_t RawRing_c<T>::Insert(const T &Value)
{
    unsigned int OldNextInsert;

    OldNextInsert       = mNextInsert;
    mStorage[mNextInsert] = Value;
    mNextInsert++;

    if (mNextInsert == mLimit)
    {
        mNextInsert = 0;
    }

    if (mNextInsert == mNextExtract)
    {
        mNextInsert      = OldNextInsert;
        return RingTooManyEntries;
    }

    return RingNoError;
}

template<typename T>
RingStatus_t RawRing_c<T>::InsertFront(const T &Value)
{
    unsigned int OldNextExtract;

    OldNextExtract = mNextExtract;
    if (mNextExtract == 0)
    {
        mNextExtract = mLimit - 1;
    }
    else
    {
        mNextExtract--;
    }

    if (mNextInsert == mNextExtract)
    {
        mNextExtract = OldNextExtract;
        return RingTooManyEntries;
    }

    mStorage[mNextExtract] = Value;

    return RingNoError;
}

template<typename T>
RingStatus_t RawRing_c<T>::Peek(T *Value) const
{
    if (mNextExtract != mNextInsert)
    {
        *Value = mStorage[mNextExtract];
        return RingNoError;
    }

    return RingNothingToGet;
}

template<typename T>
RingStatus_t RawRing_c<T>::Extract(T *Value)
{
    if (mNextExtract != mNextInsert)
    {
        *Value = mStorage[mNextExtract];
        mNextExtract++;

        if (mNextExtract == mLimit)
        {
            mNextExtract = 0;
        }

        return RingNoError;
    }

    return RingNothingToGet;
}

template<typename T>
RingStatus_t RawRing_c<T>::ExtractLastInserted(T *Value)
{
    if (mNextExtract != mNextInsert)
    {
        if (mNextInsert > 0)
        {
            mNextInsert--;
        }
        else
        {
            mNextInsert = mLimit - 1;
        }

        *Value = mStorage[mNextInsert];

        return RingNoError;
    }

    return RingNothingToGet;
}

template<typename T>
void RawRing_c<T>::Reset()
{
    mNextExtract = 0;
    mNextInsert  = 0;
}

#endif
