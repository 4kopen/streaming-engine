/************************************************************************
Copyright (C) 2003-2014 STMicroelectronics. All Rights Reserved.

This file is part of the Streaming Engine.

Streaming Engine is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License version 2 as
published by the Free Software Foundation.

Streaming Engine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with Streaming Engine; see the file COPYING.  If not, write to the Free
Software Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307,
USA.

The Streaming Engine Library may alternatively be licensed under a
proprietary license from ST.
************************************************************************/

#include "reference_count_log.h"

#include "stack_trace_store.h"

#undef TRACE_TAG
#define TRACE_TAG "ReferenceCountLog_c"

static const char *ActionToString(ReferenceCountLog_c::Action_t Action)
{
    switch (Action)
    {
    case ReferenceCountLog_c::EMPTY:
        return "EMPTY";
    case ReferenceCountLog_c::SET:
        return "SET";
    case ReferenceCountLog_c::INC:
        return "INC";
    case ReferenceCountLog_c::DEC:
        return "DEC";
    default:
        return "???";
    }

    return ""; // please compiler
}

ReferenceCountLog_c::ReferenceCountLog_c(StackTraceStore_c *Store)
    : mReferenceCountLogLock()
    , mEntries()
    , mStore(Store)
    , mFirstTimeStampUs(OS_GetTimeInMicroSeconds())
    , mLastTimeStampUs(mFirstTimeStampUs)
{
    // Bitfield storing action big enough?
    COMPILE_ASSERT(ACTION_MAX <= (1 << 2));

    OS_InitializeMutex(&mReferenceCountLogLock);
}

ReferenceCountLog_c::~ReferenceCountLog_c()
{
    SE_DEBUG(group_metadebug, "log: size=%u bytes capacity=%u bytes\n",
             mEntries.Size() * sizeof(mEntries[0]), mEntries.Capacity() * sizeof(mEntries[0]));

    OS_TerminateMutex(&mReferenceCountLogLock);
}

// Append new entry in log.
void ReferenceCountLog_c::Log(Action_t Action, int Value)
{
    SE_ASSERT(EMPTY < Action && Action < ACTION_MAX);

    OS_LockMutex(&mReferenceCountLogLock);

    // Make room for new log entry now to simplify error handling.
    OS_Status_t Status = mEntries.ReserveAtLeast(mEntries.Size() + 1);
    if (Status != OS_NO_ERROR)
    {
        OS_UnLockMutex(&mReferenceCountLogLock);
        SE_ERROR("log out-of-memory\n");
        return;
    }

    // Capture stack in temporary buffer.
    const size_t MAX_FRAMES = 32;
    unsigned long Frames[MAX_FRAMES];
    size_t FrameCount = OS_CaptureStackTrace(Frames, MAX_FRAMES, /* Skip = */ 1);
    if (FrameCount == 0)
    {
        OS_UnLockMutex(&mReferenceCountLogLock);
        SE_WARNING_ONCE("stack capture not supported\n");
        return;
    }

    // Copy stack trace to backend store.
    size_t Handle;
    Status = mStore->Store(Frames, FrameCount, &Handle);
    if (Status != OS_NO_ERROR)
    {
        OS_UnLockMutex(&mReferenceCountLogLock);
        SE_ERROR("stack trace store error\n");
        return;
    }

    // Commit new entry.
    unsigned long long Now = OS_GetTimeInMicroSeconds();
    Entry_s Entry;
    Entry.mAction = Action;
    Entry.mValue = Value;
    Entry.mStackTraceHandle = Handle;
    Entry.mTimeStampDeltaUs = Now - mLastTimeStampUs;
    Entry.mThreadId = OS_ThreadID();
    Status = mEntries.PushBack(Entry);
    SE_ASSERT(Status == OS_NO_ERROR);  // cannot fail as Reserve()d earlier

    mLastTimeStampUs = Now;

    OS_UnLockMutex(&mReferenceCountLogLock);
}

// Write all log entries to kernel log.
void ReferenceCountLog_c::Dump(const char *ObjectName, const void *ObjectAddr) const
{
    SE_TRACE("BEGIN REF COUNT LOG FOR %s @ 0x%p\n", ObjectName, ObjectAddr);

    OS_LockMutex(&mReferenceCountLogLock);

    unsigned long long TimeStampUs = mFirstTimeStampUs;
    for (size_t i = 0; i < mEntries.Size(); ++i)
    {
        DumpEntry(mEntries[i], TimeStampUs);
        TimeStampUs += mEntries[i].mTimeStampDeltaUs;
    }

    OS_UnLockMutex(&mReferenceCountLogLock);

    SE_TRACE("END REF COUNT LOG FOR %s @ 0x%p\n", ObjectName, ObjectAddr);
}

// Write one entry to kernel log.
void ReferenceCountLog_c::DumpEntry(const Entry_s &Entry, unsigned long long TimeStampUs) const
{
    OS_AssertMutexHeld(&mReferenceCountLogLock);

    SE_TRACE("%s TO %d AT %llu us BY TID %u IN\n", ActionToString(Entry.Action()), Entry.mValue,
             TimeStampUs, Entry.mThreadId);

    for (size_t i = mStore->FirstFrame(Entry.mStackTraceHandle),
         Count = i + mStore->FrameCount(Entry.mStackTraceHandle);
         i < Count;
         ++i)
    {
        void *Addr = reinterpret_cast<void *>(mStore->FrameAt(i));
        SE_TRACE("        [<%p>] %pS\n", Addr, Addr);
    }
}

// Discard all log entries.
//
// The underlying memory is not freed on the assumption that the log will be
// reused for capturing a similar profile.
void ReferenceCountLog_c::Reset()
{
    OS_LockMutex(&mReferenceCountLogLock);
    mEntries.Resize(0);
    OS_UnLockMutex(&mReferenceCountLogLock);
}
