/************************************************************************
Copyright (C) 2003-2014 STMicroelectronics. All Rights Reserved.

This file is part of the Streaming Engine.

Streaming Engine is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License version 2 as
published by the Free Software Foundation.

Streaming Engine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with Streaming Engine; see the file COPYING.  If not, write to the Free
Software Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307,
USA.

The Streaming Engine Library may alternatively be licensed under a
proprietary license from ST.
************************************************************************/

#ifndef H_REFERENCE_COUNT_LOG
#define H_REFERENCE_COUNT_LOG

#include "osinline.h"
#include "vector.h"

class StackTraceStore_c;

// Keep track of the history of a reference count for debugging purpose.
//
// A log entry stores the action (increment, decrement...), reference count
// value, and stack trace.
//
// To reduce memory footprint, the stack traces are stored in a separate
// StackTraceStore_c object that can be shared by many ReferenceCountLog_c
// objects.
//
// Thread-safe.
class ReferenceCountLog_c
{
public:
    // Reference count operations.
    // Keep in sync with Entry_s::mAction.
    enum Action_t
    {
        EMPTY = 0,      // empty entry in log
        SET,            // set to specific value
        INC,            // increment
        DEC,            // decrement
        ACTION_MAX      // keep last
    };

    explicit ReferenceCountLog_c(StackTraceStore_c *Store);
    ~ReferenceCountLog_c();

    void Log(Action_t Action, int Value);
    void Dump(const char *ObjectName, const void *ObjectAddr) const;
    void Reset();

private:
    // A log entry.  Optimized for size.
    struct Entry_s
    {
        Action_t Action() const { return static_cast<Action_t>(mAction); }

        unsigned int mAction            : 2;    // keep in sync with Action_s
        unsigned int mValue             : 14;
        unsigned int mStackTraceHandle  : 16;
        unsigned int mTimeStampDeltaUs;         // see mFirstTimeStampUs
        unsigned int mThreadId;
    };

    // Protect members of this object.
    mutable OS_Mutex_t mReferenceCountLogLock;

    // All log entries in chronological order.
    Vector_c<Entry_s> mEntries;

    // Backend storing stack traces.
    StackTraceStore_c *mStore;

    // Initial timestamp in microseconds.
    // Rather than storing an absolute timestamp a log entry stores the time
    // elapsed since the previous log entry or the initial timestamp in the
    // case of the first entry.  This makes entries smaller.
    unsigned long long mFirstTimeStampUs;

    // Timestamp of most recent entry in log in microseconds.
    unsigned long long mLastTimeStampUs;

    void DumpEntry(const Entry_s &Entry, unsigned long long TimeStampUs) const;

    DISALLOW_COPY_AND_ASSIGN(ReferenceCountLog_c);
};

#endif
