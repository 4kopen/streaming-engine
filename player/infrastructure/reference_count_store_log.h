/************************************************************************
Copyright (C) 2003-2014 STMicroelectronics. All Rights Reserved.

This file is part of the Streaming Engine.

Streaming Engine is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License version 2 as
published by the Free Software Foundation.

Streaming Engine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with Streaming Engine; see the file COPYING.  If not, write to the Free
Software Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307,
USA.

The Streaming Engine Library may alternatively be licensed under a
proprietary license from ST.
************************************************************************/

#ifndef H_REFERENCE_COUNT_STORE_LOG
#define H_REFERENCE_COUNT_STORE_LOG

#include "stack_trace_store.h"
#include "reference_count_log.h"

// Thin wrapper over ReferenceCountLog_c and StackTraceStore_c for cases
// where there is a 1-1 relationship between log and store.
class ReferenceCountStoreLog_c
{
public:
    ReferenceCountStoreLog_c()
        : mStore()
        , mLog(&mStore)
    {
    }

    void Log(ReferenceCountLog_c::Action_t Action, int Value) { mLog.Log(Action, Value); }
    void Dump(const char *ObjectName, const void *ObjectAddr) const { mLog.Dump(ObjectName, ObjectAddr); }

private:
    StackTraceStore_c mStore;
    ReferenceCountLog_c mLog;
};

#endif
