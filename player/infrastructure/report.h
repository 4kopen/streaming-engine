/************************************************************************
Copyright (C) 2003-2014 STMicroelectronics. All Rights Reserved.

This file is part of the Streaming Engine.

Streaming Engine is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License version 2 as
published by the Free Software Foundation.

Streaming Engine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with Streaming Engine; see the file COPYING.  If not, write to the Free
Software Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307,
USA.

The Streaming Engine Library may alternatively be licensed under a
proprietary license from ST.
************************************************************************/

#ifndef __REPORT_H
#define __REPORT_H

#include "fatal_error.h"
#include "osinline.h"

#ifdef __cplusplus
extern "C" {
#endif

#ifdef CONFIG_ARCH_STI
#include <linux/kern_levels.h>  // for kernel 3.10 and above
#else
#ifndef __KERNEL_PRINTK__
#define KERN_EMERG      "<0>"/* system is unusable*/
#define KERN_ALERT      "<1>"/* action must be taken immediately*/
#define KERN_CRIT       "<2>"/* critical conditions*/
#define KERN_ERR        "<3>"/* error conditions*/
#define KERN_WARNING    "<4>"/* warning conditions*/
#define KERN_NOTICE     "<5>"/* normal but significant condition*/
#define KERN_INFO       "<6>"/* informational*/
#define KERN_DEBUG      "<7>"/* debug-level messages*/
#endif  // __KERNEL_PRINTK__
#endif  // CONFIG_ARCH_STI

#define     SEVERITY_FATAL         0
#define     SEVERITY_ERROR         1
#define     SEVERITY_WARNING       2
#define     SEVERITY_INFO          3
#define     SEVERITY_DEBUG         4
#define     SEVERITY_VERBOSE       5
#define     SEVERITY_EXTRAVERB     6

// severity levels
typedef enum
{
    severity_fatal          = SEVERITY_FATAL,
    severity_error          = SEVERITY_ERROR,
    severity_warning        = SEVERITY_WARNING,
    severity_info           = SEVERITY_INFO,
    severity_debug          = SEVERITY_DEBUG,
    severity_verbose        = SEVERITY_VERBOSE,
    severity_extraverb      = SEVERITY_EXTRAVERB,
} report_severity_t;

// trace groups
// when adding a group:
// 1. report.c init_se_trace_groups() has to be updated for debugfs dir name and entry name
// 2. player_module.c trace_groups_levels_init[] has to be updated for module init time default level
enum report_groups
{
    // API groups
    group_api,

    // central/wrapper groups
    group_player,
    group_havana,
    group_buffer,
    group_decodebufferm,
    group_timestamps,
    group_misc,
    group_metadebug,
    group_event,

    // collator groups
    group_collator_audio,
    group_collator_video,
    group_esprocessor,

    // frameparser groups
    group_frameparser_audio,
    group_frameparser_video,

    // decoder groups
    group_decoder_audio,
    group_decoder_video,

    // encoder groups
    group_encoder_stream,
    group_encode_coordinator,
    group_encoder_audio_preproc,
    group_encoder_audio_coder,
    group_encoder_video_preproc,
    group_encoder_video_preproc_marker,
    group_encoder_video_coder,
    group_encoder_transporter,

    // manifestor groups
    group_manifestor_audio_ksound,
    group_manifestor_video_stmfb,
    group_manifestor_audio_grab,
    group_manifestor_video_grab,
    group_manifestor_audio_encode,
    group_manifestor_video_encode,

    // FRC group
    group_frc,

    // timer/AVSYNC groups
    group_output_timer,
    group_avsync,

    // audio groups
    group_audio_reader,
    group_audio_player,
    group_audio_generator,
    group_mixer,

    // se-pipeline group
    group_se_pipeline,

    // perf
    group_perf_audio_decoder,
    group_perf_audio_encoder,
    group_perf_audio_pcmproc,
    group_perf_audio_mixer,

    group_perf_video_decoder,

    // Interpolation
    group_interpolation,

    // dump stack on demand
    group_dump_stack,

    // not a true group : keep last, keep upper case
    GROUP_LAST,
};

enum trace_backend
{
    TRACE_BACKEND_KERNEL,
    TRACE_BACKEND_FTRACE,
    TRACE_BACKEND_KPTRACE,
    TRACE_BACKEND_LAST,

    TRACE_BACKEND_DEFAULT = TRACE_BACKEND_KERNEL,
};

static inline const char *StringifyTraceBackend(int backend)
{
#define E(x) case x: return #x"\n"
    switch (backend)
    {
        E(TRACE_BACKEND_KERNEL);
        E(TRACE_BACKEND_FTRACE);
        E(TRACE_BACKEND_KPTRACE);
    default:
        return "Invalid BACKEND";
    }
#undef E
}

// default trace tag; shall be redefined on each source/header file using trace macros
#ifndef TRACE_TAG
#define TRACE_TAG ""
#endif

#ifdef SE_REPORT_ENABLED
// public report functions that shall not be used directly: use only through macros
void __attribute__((format(printf, 1, 2))) report_print(const char *format, ...);
int report_is_group_active(report_severity_t report_severity, int trace_group);
int report_group_debug_level(int trace_group);
int report_is_severity_active(report_severity_t report_severity);
void report_dump_hex(unsigned char *data, int length, int width, void *start);
void report_dump_current_stack_trace(const char *func);
#endif // SE_REPORT_ENABLED

void report_init(int trace_groups_levels[GROUP_LAST], int trace_global_level, int trace_backend);
void report_term(void);

// stub
static inline void __attribute__((format(printf, 1, 2))) _stub_report(const char *format, ...) { }

// asserts / fatal trace macros
#ifdef SE_ASSERT_ENABLED
#define SE_FATAL(fmt, ...) \
    do {\
        report_dump_current_stack_trace(__func__); \
        fatal_error(KERN_EMERG "(%s,%d) FATAL: " TRACE_TAG "::%s - " fmt, OS_ThreadName(), OS_ThreadID(), __func__, ##__VA_ARGS__); \
    } while(0)
#define SE_ASSERT(x)        do if(!(x)) SE_FATAL("Assertion '%s' failed at %s:%d\n", #x, __FILE__, __LINE__); while(0)
#define SE_PRECONDITION(x)  do if(!(x)) SE_FATAL("Precondition '%s' failed at %s:%d\n", #x, __FILE__, __LINE__); while(0)
#define SE_POSTCONDITION(x) do if(!(x)) SE_FATAL("Postcondition '%s' failed at %s:%d\n", #x, __FILE__, __LINE__); while(0)
#else // undef SE_ASSERT_ENABLED
#define SE_FATAL(fmt, ...)  do { _stub_report("STUB" fmt, ##__VA_ARGS__); } while(0)
// Rather than defining assertion macros as nops, we rely on compiler to
// optimize out assertion arguments.  This works around "variable not used"
// errors in code such as:
//   PlayerStatus_t Status = Foo();
//   SE_ASSERT(Status == PlayerNoError);
//   // Status not used anymore in function
// Using sizeof() rather than just (void)x ensures that complex macros that
// call non-inline functions are optimized out too.
#define SE_ASSERT(x)            ((void)sizeof(x))
#define SE_PRECONDITION(x)      ((void)sizeof(x))
#define SE_POSTCONDITION(x)     ((void)sizeof(x))
#endif // SE_ASSERT_ENABLED

// error macros
#if defined(SE_REPORT_ENABLED) && (SE_REPORT_ENABLED >= SEVERITY_ERROR)
#define SE_ERROR(fmt, ...) \
    do {\
        if (report_is_severity_active(severity_error)) {\
            report_print(KERN_NOTICE "(%s,%d) ERROR: " TRACE_TAG "::%s - " fmt, OS_ThreadName(), OS_ThreadID(), __func__, ##__VA_ARGS__); \
            if (report_is_group_active(severity_error, group_dump_stack)) { \
                report_dump_current_stack_trace(__func__); \
            } \
}\
} while (0)
#define SE_ERROR_ONCE(fmt, ...) \
    do {\
        static bool DATA_UNLIKELY_SECTION __se_warned; \
        if(!__se_warned) { SE_ERROR(fmt, ##__VA_ARGS__); __se_warned = true; } \
    } while(0)
#else // undef SE_REPORT_ENABLED || SE_REPORT_ENABLED < SEVERITY_ERROR
#define SE_ERROR(fmt, ...)      do { _stub_report("STUB" fmt, ##__VA_ARGS__); } while(0)
#define SE_ERROR_ONCE(fmt, ...) do { _stub_report("STUB" fmt, ##__VA_ARGS__); } while(0)
#endif

// warning macros
#if defined(SE_REPORT_ENABLED) && (SE_REPORT_ENABLED >= SEVERITY_WARNING)
#define SE_WARNING(fmt, ...) \
    do {\
        if (report_is_severity_active(severity_warning)) {\
            report_print(KERN_NOTICE "(%s,%d) WARNING: " TRACE_TAG "::%s - " fmt, OS_ThreadName(), OS_ThreadID(), __func__, ##__VA_ARGS__); \
            if (report_is_group_active(severity_warning, group_dump_stack)) {  \
                report_dump_current_stack_trace(__func__); \
            } \
}\
} while (0)
#define SE_WARNING_ONCE(fmt, ...) \
    do {\
        static bool DATA_UNLIKELY_SECTION __se_warned; \
        if(!__se_warned) { SE_WARNING(fmt, ##__VA_ARGS__); __se_warned = true; } \
    } while(0)
#else // undef SE_REPORT_ENABLED || SE_REPORT_ENABLED < SEVERITY_WARNING
#define SE_WARNING(fmt, ...)      do { _stub_report("STUB" fmt, ##__VA_ARGS__); } while(0)
#define SE_WARNING_ONCE(fmt, ...) do { _stub_report("STUB" fmt, ##__VA_ARGS__); } while(0)
#endif

// info macros
#if defined(SE_REPORT_ENABLED) && (SE_REPORT_ENABLED >= SEVERITY_INFO)
#define SE_IS_INFO_ON(group) report_is_group_active(severity_info, group)
#define SE_INFO(group, fmt, ...) \
    do {\
        if (report_is_group_active(severity_info, group)) \
            report_print(KERN_INFO "(%s,%d) " TRACE_TAG "::%s - " fmt, OS_ThreadName(), OS_ThreadID(), __func__, ##__VA_ARGS__); \
    } while(0)
#define SE_INFO2(group1, group2, fmt, ...) \
    do {\
        if (report_is_group_active(severity_info, group1)) \
            report_print(KERN_INFO "(%s,%d) " TRACE_TAG "::%s - " fmt, OS_ThreadName(), OS_ThreadID(), __func__, ##__VA_ARGS__); \
        else if (report_is_group_active(severity_info, group2)) \
            report_print(KERN_INFO "(%s,%d) " TRACE_TAG "::%s - " fmt, OS_ThreadName(), OS_ThreadID(), __func__, ##__VA_ARGS__); \
    } while(0)
#else // undef SE_REPORT_ENABLED || SE_REPORT_ENABLED < SEVERITY_INFO
#define SE_IS_INFO_ON(group) false
#define SE_INFO(group, fmt, ...)            do { _stub_report("STUB" fmt, ##__VA_ARGS__); } while(0)
#define SE_INFO2(group1, group2, fmt, ...)  do { _stub_report("STUB" fmt, ##__VA_ARGS__); } while(0)
#endif

// debug macros
#if defined(SE_REPORT_ENABLED) && (SE_REPORT_ENABLED >= SEVERITY_DEBUG)
#define SE_IS_DEBUG_ON(group) report_is_group_active(severity_debug, group)
#define SE_DEBUG(group, fmt, ...) \
    do {\
        if (report_is_group_active(severity_debug, group)) \
            report_print(KERN_INFO "(%s,%d) " TRACE_TAG "::%s - " fmt, OS_ThreadName(), OS_ThreadID(), __func__, ##__VA_ARGS__); \
    } while(0)
#define SE_DEBUG2(group1, group2, fmt, ...) \
    do {\
        if (report_is_group_active(severity_debug, group1)) \
            report_print(KERN_INFO "(%s,%d) " TRACE_TAG "::%s - " fmt, OS_ThreadName(), OS_ThreadID(), __func__, ##__VA_ARGS__); \
        else if (report_is_group_active(severity_debug, group2)) \
            report_print(KERN_INFO "(%s,%d) " TRACE_TAG "::%s - " fmt, OS_ThreadName(), OS_ThreadID(), __func__, ##__VA_ARGS__); \
    } while(0)
#define SE_DEBUG_DUMP_HEX(data, length, width, start) \
    do {\
        if (report_is_severity_active(severity_debug)) \
            report_dump_hex(data, length, width, start); \
    } while(0)
#else // undef SE_REPORT_ENABLED || SE_REPORT_ENABLED < SEVERITY_DEBUG
#define SE_IS_DEBUG_ON(group) false
#define SE_DEBUG(group, fmt, ...)            do { _stub_report("STUB" fmt, ##__VA_ARGS__); } while(0)
#define SE_DEBUG2(group1, group2, fmt, ...)  do { _stub_report("STUB" fmt, ##__VA_ARGS__); } while(0)
#define SE_DEBUG_DUMP_HEX(data, length, width, start) do { } while(0)
#endif

// verbose macros
#if defined(SE_REPORT_ENABLED) && (SE_REPORT_ENABLED >= SEVERITY_VERBOSE)
#define SE_IS_VERBOSE_ON(group) report_is_group_active(severity_verbose, group)
#define SE_VERBOSE(group, fmt, ...) \
    do {\
        if (report_is_group_active(severity_verbose, group)) \
            report_print(KERN_INFO "(%s,%d) " TRACE_TAG "::%s - " fmt, OS_ThreadName(), OS_ThreadID(), __func__, ##__VA_ARGS__); \
    } while(0)
#define SE_VERBOSE2(group1, group2, fmt, ...) \
    do {\
        if (report_is_group_active(severity_verbose, group1)) \
            report_print(KERN_INFO "(%s,%d) " TRACE_TAG "::%s - " fmt, OS_ThreadName(), OS_ThreadID(), __func__, ##__VA_ARGS__); \
        else if (report_is_group_active(severity_verbose, group2)) \
            report_print(KERN_INFO "(%s,%d) " TRACE_TAG "::%s - " fmt, OS_ThreadName(), OS_ThreadID(), __func__, ##__VA_ARGS__); \
    } while(0)
#else // undef SE_REPORT_ENABLED || SE_REPORT_ENABLED < SEVERITY_DEBUG
#define SE_IS_VERBOSE_ON(group) false
#define SE_VERBOSE(group, fmt, ...)            do { _stub_report("STUB" fmt, ##__VA_ARGS__); } while(0)
#define SE_VERBOSE2(group1, group2, fmt, ...)  do { _stub_report("STUB" fmt, ##__VA_ARGS__); } while(0)
#endif

// extraverbose macros
#if defined(SE_REPORT_ENABLED) && (SE_REPORT_ENABLED >= SEVERITY_EXTRAVERB)
#define SE_IS_EXTRAVERB_ON(group) report_is_group_active(severity_extraverb, group)
#define SE_EXTRAVERB(group, fmt, ...) \
    do {\
        if (report_is_group_active(severity_extraverb, group)) \
            report_print(KERN_INFO "(%s,%d) " TRACE_TAG "::%s - " fmt, OS_ThreadName(), OS_ThreadID(), __func__, ##__VA_ARGS__); \
    } while(0)
#define SE_EXTRAVERB2(group1, group2, fmt, ...) \
    do {\
        if (report_is_group_active(severity_extraverb, group1)) \
            report_print(KERN_INFO "(%s,%d) " TRACE_TAG "::%s - " fmt, OS_ThreadName(), OS_ThreadID(), __func__, ##__VA_ARGS__); \
        else if (report_is_group_active(severity_extraverb, group2)) \
            report_print(KERN_INFO "(%s,%d) " TRACE_TAG "::%s - " fmt, OS_ThreadName(), OS_ThreadID(), __func__, ##__VA_ARGS__); \
    } while(0)
#else // undef SE_REPORT_ENABLED || SE_REPORT_ENABLED < SEVERITY_EXTRAVERB
#define SE_IS_EXTRAVERB_ON(group) false
#define SE_EXTRAVERB(group, fmt, ...)            do { _stub_report("STUB" fmt, ##__VA_ARGS__); } while(0)
#define SE_EXTRAVERB2(group1, group2, fmt, ...)  do { _stub_report("STUB" fmt, ##__VA_ARGS__); } while(0)
#endif

// unconditional macros
#if defined(SE_REPORT_ENABLED) && (SE_REPORT_ENABLED >= SEVERITY_ERROR)
#define SE_TRACE_RAW(...) \
    do {\
        report_print(KERN_INFO "%s", ##__VA_ARGS__); \
    } while(0)
#define SE_TRACE(fmt, ...) \
    do {\
        report_print(KERN_INFO "(%s,%d) " TRACE_TAG "::%s - " fmt, OS_ThreadName(), OS_ThreadID(), __func__, ##__VA_ARGS__); \
        if (report_is_group_active(severity_info, group_dump_stack)) { \
            report_dump_current_stack_trace(__func__); \
        } \
    } while(0)
#else // undef SE_REPORT_ENABLED || SE_REPORT_ENABLED < SEVERITY_ERROR
#define SE_TRACE_RAW(...)  do { _stub_report("STUB %s" , ##__VA_ARGS__); } while(0)
#define SE_TRACE(fmt, ...) do { _stub_report("STUB" fmt, ##__VA_ARGS__); } while(0)
#endif

// return debug level
#if defined(SE_REPORT_ENABLED) && (SE_REPORT_ENABLED >= SEVERITY_INFO)
#define SE_DEBUG_LEVEL(group) report_group_debug_level(group)
#else
#define SE_DEBUG_LEVEL(group) SEVERITY_ERROR
#endif

#ifdef __cplusplus
}
#endif

#endif
