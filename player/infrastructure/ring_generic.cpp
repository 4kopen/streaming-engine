/************************************************************************
Copyright (C) 2003-2014 STMicroelectronics. All Rights Reserved.

This file is part of the Streaming Engine.

Streaming Engine is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License version 2 as
published by the Free Software Foundation.

Streaming Engine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with Streaming Engine; see the file COPYING.  If not, write to the Free
Software Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307,
USA.

The Streaming Engine Library may alternatively be licensed under a
proprietary license from ST.
************************************************************************/

#include "ring_generic.h"

#undef TRACE_TAG
#define TRACE_TAG "RingGeneric_c"

RingGeneric_c *RingGeneric_c::New(unsigned int MaxEntries, const char *Name)
{
    RingGeneric_c *ring = new RingGeneric_c(MaxEntries, Name);
    if (ring != NULL && ring->FinalizeInit() != RingNoError)
    {
        delete ring;
        ring = NULL;
    }
    return ring;
}

RingGeneric_c::RingGeneric_c(unsigned int MaxEntries, const char *Name)
    : Lock()
    , Signal()
    , mName((Name != NULL) ? Name : "Anonymous Ring")
    , mRaw(MaxEntries)
{
    OS_InitializeSpinLock(&Lock);
    OS_InitializeEvent(&Signal);
}

RingStatus_t RingGeneric_c::FinalizeInit()
{
    return mRaw.FinalizeInit();
}

RingGeneric_c::~RingGeneric_c()
{
    if (NonEmpty()) { SE_DEBUG(group_buffer, "caution:%s ring (%p) is non empty on dtor\n", mName, this); }
    OS_TerminateSpinLock(&Lock);
    OS_TerminateEvent(&Signal);
}

RingStatus_t   RingGeneric_c::Insert(uintptr_t      Value)
{
    OS_LockSpinLockIRQSave(&Lock);

    RingStatus_t Status = mRaw.Insert(Value);

    OS_UnLockSpinLockIRQRestore(&Lock);

    OS_SetEvent(&Signal);

    return Status;
}

RingStatus_t   RingGeneric_c::InsertFront(uintptr_t      Value)
{
    OS_LockSpinLockIRQSave(&Lock);

    RingStatus_t Status = mRaw.InsertFront(Value);

    OS_UnLockSpinLockIRQRestore(&Lock);

    OS_SetEvent(&Signal);

    return Status;
}

RingStatus_t RingGeneric_c::Peek(uintptr_t    *Value)
{
    OS_LockSpinLockIRQSave(&Lock);
    RingStatus_t Status = mRaw.Peek(Value);
    OS_UnLockSpinLockIRQRestore(&Lock);

    return Status;
}

RingStatus_t   RingGeneric_c::Extract(uintptr_t       *Value,
                                      unsigned int     BlockingPeriod)
{
    //
    // If there is nothing in the ring we wait for up to the specified period.
    //
    OS_ResetEvent(&Signal);

    OS_LockSpinLockIRQSave(&Lock);
    if ((mRaw.NonEmpty() == false) && (BlockingPeriod != RING_NONE_BLOCKING))
    {
        OS_UnLockSpinLockIRQRestore(&Lock);
        OS_WaitForEventAuto(&Signal, BlockingPeriod);
        OS_LockSpinLockIRQSave(&Lock);
    }

    RingStatus_t Status = mRaw.Extract(Value);
    OS_UnLockSpinLockIRQRestore(&Lock);

    return Status;
}

RingStatus_t   RingGeneric_c::ExtractLastInserted(uintptr_t       *Value,
                                                  unsigned int     BlockingPeriod)
{
    //
    // If there is nothing in the ring we wait for up to the specified period.
    //
    OS_ResetEvent(&Signal);

    OS_LockSpinLockIRQSave(&Lock);
    if ((mRaw.NonEmpty() == false) && (BlockingPeriod != RING_NONE_BLOCKING))
    {
        OS_UnLockSpinLockIRQRestore(&Lock);
        OS_WaitForEventAuto(&Signal, BlockingPeriod);
        OS_LockSpinLockIRQSave(&Lock);
    }

    RingStatus_t Status = mRaw.ExtractLastInserted(Value);
    OS_UnLockSpinLockIRQRestore(&Lock);

    return Status;
}

RingStatus_t   RingGeneric_c::Flush()
{
    OS_LockSpinLockIRQSave(&Lock);
    mRaw.Reset();
    OS_UnLockSpinLockIRQRestore(&Lock);
    return RingNoError;
}

bool   RingGeneric_c::NonEmpty()
{
    OS_LockSpinLockIRQSave(&Lock);
    bool Result = mRaw.NonEmpty();
    OS_UnLockSpinLockIRQRestore(&Lock);
    return Result;
}

unsigned int RingGeneric_c::NbOfEntries()
{
    OS_LockSpinLockIRQSave(&Lock);
    unsigned int NbEntries = mRaw.NbOfEntries();
    OS_UnLockSpinLockIRQRestore(&Lock);
    return NbEntries;
}

bool RingGeneric_c::Full()
{
    OS_LockSpinLockIRQSave(&Lock);
    bool IsFull = mRaw.Full();
    OS_UnLockSpinLockIRQRestore(&Lock);
    return IsFull;
}
