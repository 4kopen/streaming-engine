/************************************************************************
Copyright (C) 2003-2014 STMicroelectronics. All Rights Reserved.

This file is part of the Streaming Engine.

Streaming Engine is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License version 2 as
published by the Free Software Foundation.

Streaming Engine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with Streaming Engine; see the file COPYING.  If not, write to the Free
Software Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307,
USA.

The Streaming Engine Library may alternatively be licensed under a
proprietary license from ST.
************************************************************************/

#include "shared_count.h"

#include "reference_count_store_log.h"

SharedCount_c::SharedCount_c(void *Ptr)
    : mSharedCountLock()
    , mUseCount(1)
    , mWeakCount(0)
    , mPtr(Ptr)
    , mLog(NULL)
{
    SE_ASSERT(Ptr != NULL);
    OS_InitializeMutex(&mSharedCountLock);
}

SharedCount_c::~SharedCount_c()
{
    OS_TerminateMutex(&mSharedCountLock);

    // Poison for catching use-after-free bugs.  Some unit tests rely on this.
    mUseCount = 0xFAFAFAFA;
    mWeakCount = 0xFAFAFAFA;
    mPtr = reinterpret_cast<void *>(0xFAFAFAFA);
}

void SharedCount_c::IncStrong()
{
    OS_LockMutex(&mSharedCountLock);
    IncStrongLocked();
    OS_UnLockMutex(&mSharedCountLock);
}

// Increment strong reference count if and only if it is strictly positive.
// Return shared object if count was positive and NULL otherwise.
void *SharedCount_c::IncStrongIfPositive()
{
    void *Ret = NULL;

    OS_LockMutex(&mSharedCountLock);
    if (mUseCount > 0)
    {
        IncStrongLocked();
        Ret = mPtr;
    }
    OS_UnLockMutex(&mSharedCountLock);

    return Ret;
}

void SharedCount_c::IncStrongLocked()
{
    OS_AssertMutexHeld(&mSharedCountLock);
    mUseCount++;
    if (mLog != NULL)
    {
        mLog->Log(ReferenceCountLog_c::INC, mUseCount);
    }
}

// Decrement strong reference count.
// Return ALL_REFS_GONE if count dropped to zero.  The caller is responsible for
// deleting the shared object in this case.
// Delete this object if both strong and weak counts equal zero.
SharedCount_c::DecStatus_t SharedCount_c::DecStrong()
{
    OS_LockMutex(&mSharedCountLock);

    SE_ASSERT(mUseCount > 0);
    --mUseCount;
    if (mLog != NULL)
    {
        mLog->Log(ReferenceCountLog_c::DEC, mUseCount);
    }

    bool DeletePointee = (mUseCount == 0);
    bool DeleteThis = DeletePointee && (mWeakCount == 0);

    OS_UnLockMutex(&mSharedCountLock);

    DecStatus_t Status = MORE_REFS;
    if (DeletePointee)
    {
        mPtr = NULL;
        Status = ALL_REFS_GONE;
    }

    if (DeleteThis)
    {
        delete this;
    }

    return Status;
}

void SharedCount_c::IncWeak()
{
    OS_LockMutex(&mSharedCountLock);
    mWeakCount++;
    OS_UnLockMutex(&mSharedCountLock);
}

// Decrement weak reference count.
// Delete this object if both strong and weak counts equal zero.
void SharedCount_c::DecWeak()
{
    OS_LockMutex(&mSharedCountLock);

    SE_ASSERT(mWeakCount > 0);
    --mWeakCount;
    bool DeleteThis = (mUseCount == 0) && (mWeakCount == 0);

    OS_UnLockMutex(&mSharedCountLock);

    if (DeleteThis)
    {
        delete this;
    }
}

int SharedCount_c::StrongCount() const
{
    OS_LockMutex(&mSharedCountLock);
    int Count = mUseCount;
    OS_UnLockMutex(&mSharedCountLock);

    return Count;
}

int SharedCount_c::WeakCount() const
{
    OS_LockMutex(&mSharedCountLock);
    int Count = mWeakCount;
    OS_UnLockMutex(&mSharedCountLock);

    return Count;
}

OS_Status_t SharedCount_c::CreateLog()
{
    OS_Status_t Status = OS_ERROR;

    OS_LockMutex(&mSharedCountLock);
    if (mLog == NULL)
    {
        ReferenceCountStoreLog_c *Log = new ReferenceCountStoreLog_c;
        if (Log != NULL)
        {
            Log->Log(ReferenceCountLog_c::SET, mUseCount);
            mLog = Log;
            Status = OS_NO_ERROR;
        }
        else
        {
            SE_ERROR("out of memory when creating log\n");
        }
    }
    OS_UnLockMutex(&mSharedCountLock);

    return Status;
}

void SharedCount_c::Dump() const
{
    OS_LockMutex(&mSharedCountLock);
    if (mLog != NULL)
    {
        mLog->Dump("shared object", this);
    }
    OS_UnLockMutex(&mSharedCountLock);
}
