/************************************************************************
Copyright (C) 2003-2014 STMicroelectronics. All Rights Reserved.

This file is part of the Streaming Engine.

Streaming Engine is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License version 2 as
published by the Free Software Foundation.

Streaming Engine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with Streaming Engine; see the file COPYING.  If not, write to the Free
Software Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307,
USA.

The Streaming Engine Library may alternatively be licensed under a
proprietary license from ST.
************************************************************************/

#ifndef SHARED_COUNT_H
#define SHARED_COUNT_H

#include "osinline.h"

class ReferenceCountStoreLog_c;

// Internal control block for sharing reference-counted objects.
//
// This class is an implementation detail of SharedPtr_c and WeakPtr_c and is
// not intended for direct use by other classes.
//
// A control block keeps track of the number of strong and weak references to
// a shared object.
//
// A control block is alive as long as there is at least one strong or one weak
// reference to the shared object.  The shared object is deleted when the strong
// reference count drops to zero.  The control block may therefore outlives the
// shared object.  This design avoids back-pointers from control block to all
// weak pointers to invalidate them on shared object destruction.
//
// This class is not a template to reduce code footprint.
class SharedCount_c
{
public:
    enum DecStatus_t { ALL_REFS_GONE, MORE_REFS };

    explicit SharedCount_c(void *Ptr);
    ~SharedCount_c();

    int StrongCount() const;
    int WeakCount() const;

    void IncStrong();
    void *IncStrongIfPositive();
    DecStatus_t DecStrong();
    void IncWeak();
    void DecWeak();

    OS_Status_t CreateLog();
    void Dump() const;

private:
    // Protect access to all members.
    mutable OS_Mutex_t mSharedCountLock;

    // Strong reference count.  When > 0, shared object kept alive.
    int mUseCount;

    // Weak reference count.  Does not prevent shared object destruction.
    int mWeakCount;

    // Pointer to shared object.
    void *mPtr;

    // When not NULL, log changes to mUseCount for debugging leaks.
    ReferenceCountStoreLog_c *mLog;

    void IncStrongLocked();

    DISALLOW_COPY_AND_ASSIGN(SharedCount_c);
};

#endif
