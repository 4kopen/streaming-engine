/************************************************************************
Copyright (C) 2003-2014 STMicroelectronics. All Rights Reserved.

This file is part of the Streaming Engine.

Streaming Engine is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License version 2 as
published by the Free Software Foundation.

Streaming Engine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with Streaming Engine; see the file COPYING.  If not, write to the Free
Software Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307,
USA.

The Streaming Engine Library may alternatively be licensed under a
proprietary license from ST.
************************************************************************/

// Smart pointers inspired from standard C++ for managing reference-counted
// objects.
//
// These smart pointers provide external reference counting: under the hood,
// they allocate a control block per shared object, that notably stores the
// reference count.  This is slightly less efficient than intrusive reference
// counting (embedding counter in shared object) but allows sharing of objects
// without modifying them.
//
// Following standard terminology the (strong) reference count is named use
// count.
//
// These classes diverge from the standard C++ vector in the following ways:
//
// - They implement only a small subset of the standard (implement-on-demand).
//
// - They return errors on out-of-memory rather than raising exceptions.
//
// - They are not exception-safe as SE does not use exceptions.
//
// - Names are similar-enough to allow learning from modern C++ literature but
// have been transmogrified to follow the SE coding style.
//
// For more details, see for example
// http://en.cppreference.com/w/cpp/memory.
//
// Thread-safety guarantees:
//
// - Concurrent access to the pointed shared object from a single or multiple
// shared pointers is not thread-safe.
//
//   Example (BUG):
//      Thread 1: sp->Foo();
//      Thread 2: sp->Bar();
//
// - Concurrent access to the intrisic state of a smart pointer (i.e. what it
// points to) is not thread-safe.
//
//   Example (BUG):
//      Thread 1: sp1.Reset(...);
//      Thread 2: sp2 = sp1;
//
// - Concurrent access to the internal control block (reference count
// manipulations mainly) is thread-safe.
//
//   Example (OK):
//      Thread 1: sp2 = sp1; sp3 = sp2;
//      Thread 2: sp4 = sp1; sp5 = sp4;

#ifndef H_SHARED_PTR
#define H_SHARED_PTR

#include "osinline.h"
#include "utility.h"
#include "reference_count_store_log.h"
#include "shared_count.h"

template <typename T> class WeakPtr_c;
template <typename T> class SharedPtr_c;

//////////////////////////////////////////////////////////////////////////////
// Class definitions

// Smart pointer keeping strong reference to shared object inspired from
// std::shared_ptr.
//
// A shared pointer keeps the pointed object alive as long as the pointer
// exists.  Several shared pointers can share the same object.  When the last
// shared pointer to an object goes out of scope, the pointed object is deleted.
//
// A shared pointer can not point to C arrays.  For sharing arrays, embed them
// in a struct or share a Vector_c.
//
// See file top comment for further details.
//
// Crash course:
//
//  void Function()
//  {
//      // Create NULL pointer.
//      SharedPtr_c<Foo> sp1;
//
//      // Preferred way for creating shared object with new Foo(42) and tying
//      // it to sp1.
//      OS_Status_t status = MakeShared(42, &sp1);
//      if (status != OS_NO_ERROR)
//      {
//          ...
//      }
//      SE_ASSERT(sp1.UseCount() == 1);
//
//      // Dereference pointer
//      sp1->Bar();
//
//      // Create second reference to same object.
//      SharedPtr_c<Foo> sp2(sp1);
//      SE_ASSERT(&*sp1 == &*sp2);
//      SE_ASSERT(sp2.UseCount() == 2);
//
//      // Drop one reference to shared object.  Object still alive as
//      // still referenced by sp2.
//      sp1.Reset();
//      SE_ASSERT(sp2.UseCount() == 1);
//
//  } // sp2 destructed, last reference to Foo object gone => Foo object destructed
template <typename T>
class SharedPtr_c
{
public:
    SharedPtr_c();
    // Not explicit to allow SharedPtr_c<Foo> sp = wp.Lock().
    SharedPtr_c(const SharedPtr_c<T> &Other);
    SharedPtr_c<T> &operator=(const SharedPtr_c<T> &Other);
    ~SharedPtr_c();

    T *operator->() const   { return mPtr; }
    T &operator*() const    { return *mPtr; }
    T *Get() const          { return mPtr; }
    int UseCount() const;
    int WeakCount() const;

    void Swap(SharedPtr_c<T> &Other);
    OS_Status_t Reset(T *Ptr = NULL);
    OS_Status_t ResetIfNotNull(T *Ptr);
    OS_Status_t CreateLog();
    void DumpLog() const;

private:
    T *mPtr;
    SharedCount_c *mSharedCount;

    OS_Status_t Set(T *Ptr);

    friend class WeakPtr_c<T>;
};

// Smart pointer keeping weak reference to shared object inspired from
// std::weak_ptr.
//
// A weak pointer to an object does not prevent the object from being deleted
// when not referenced by any SharedPtr_c any more.
//
// Weak pointers are useful for:
//
// - Breaking cyclic dependencies between shared pointers.  Such cycles cause
// leaks.
//
// - Providing cache-like behaviour: Keep pointer to objects but do not prevent
// their destruction.
//
// See file top comment for further details.
//
// Crash course:
//
//      // Create NULL weak pointer.
//      WeakPtr_c<Foo> wp;
//
//      // Attach weak pointer to shared pointer.
//      SharedPtr_c<Foo> sp1 = ...;
//      wp = sp1;
//      SE_ASSERT(!wp.Expired());
//
//      // Access object through weak pointer.
//      SharedPtr_c<Foo> sp2 = wp.Lock();
//      sp2->Bar();
//      sp2.Reset();
//
//      // Destroying last strong reference invalidates weak pointer.
//      sp1.Reset();
//      SE_ASSERT(wp.Expired());
template<class T>
class WeakPtr_c
{
public:
    WeakPtr_c();
    explicit WeakPtr_c(const WeakPtr_c<T> &Other);
    explicit WeakPtr_c(const SharedPtr_c<T> &Other);
    WeakPtr_c &operator=(const SharedPtr_c<T> &Other);
    WeakPtr_c &operator=(const WeakPtr_c<T> &Other);
    ~WeakPtr_c();

    void Swap(WeakPtr_c<T> &Other);
    SharedPtr_c<T> Lock() const;

    int UseCount() const;
    bool Expired() const    { return UseCount() == 0; }

private:
    SharedCount_c *mSharedCount;

    void Copy(SharedCount_c *SharedCount);

    friend class SharedPtr_c<T>;
};

//////////////////////////////////////////////////////////////////////////////
// SharedPtr_c methods

// Construct NULL shared pointer.
template <typename T>
SharedPtr_c<T>::SharedPtr_c()
    : mPtr(NULL)
    , mSharedCount(NULL)
{
}

// Construct shared pointer pointing to same object as specified shared pointer.
// This increments the use count of the shared object.
template <typename T>
SharedPtr_c<T>::SharedPtr_c(const SharedPtr_c<T> &Other)
    : mPtr(Other.mPtr),
      mSharedCount(Other.mSharedCount)
{
    if (mSharedCount != NULL)
    {
        mSharedCount->IncStrong();
    }
}

// Make this pointer point to same object as specified shared pointer.  This
// increments the use count of the shared object.  This decrements the use count
// of the object (if any) previously pointed to by this pointer.
template <typename T>
SharedPtr_c<T> &SharedPtr_c<T>::operator=(const SharedPtr_c<T> &Other)
{
    SharedPtr_c<T> Tmp(Other);
    Swap(Tmp);
    return *this;
}

// Destruct shared pointer.  This deletes the shared object if this was the
// last reference to it.
template <typename T>
SharedPtr_c<T>::~SharedPtr_c()
{
    // Report error if this function is instantiated when T has only been
    // forward-declared.  Failing this, delete operates on a void * and does not
    // call T destructor, thus potentially causing leaks.  This is a belt and
    // suspenders approach as gcc with the current settings also complains.
    COMPILE_ASSERT(sizeof(T) > 0);

    if (mSharedCount != NULL)
    {
        if (mSharedCount->DecStrong() == SharedCount_c::ALL_REFS_GONE)
        {
            delete mPtr;
        }
    }
}

// Change shared object this pointer points to.
//
// If Ptr is NULL, this pointer becomes a NULL pointer.
//
// If Ptr is not NULL, it must point to an hitherto unshared object and this
// pointer becomes the initial reference to it.
//
// Decrements use count of the object (if any) previously pointed to by
// this pointer.
//
// Return an error if construction of the internal control block fails.
template <typename T>
OS_Status_t SharedPtr_c<T>::Reset(T *Ptr)
{
    SharedPtr_c<T> Tmp;
    OS_Status_t Status = Tmp.Set(Ptr);
    if (Status != OS_NO_ERROR)
    {
        return Status;
    }
    Swap(Tmp);

    return OS_NO_ERROR;
}

// Convenience method behaving like Reset() but also returning an error if
// Ptr is NULL.  Prefer to use MakeShared().
template <typename T>
OS_Status_t SharedPtr_c<T>::ResetIfNotNull(T *Ptr)
{
    if (Ptr == NULL)
    {
        return OS_ERROR;
    }
    return Reset(Ptr);
}

// Interchange this pointer with Other without altering shared object.
template <typename T>
void SharedPtr_c<T>::Swap(SharedPtr_c<T> &Other)
{
    ::Swap(mPtr, Other.mPtr);
    ::Swap(mSharedCount, Other.mSharedCount);
}

// Return number of strong references to this object.  Intended for testing and
// debugging.
template <typename T>
int SharedPtr_c<T>::UseCount() const
{
    return (mSharedCount != NULL) ? mSharedCount->StrongCount() : 0;
}

// Return number of weak references to this object.  Intended for testing and
// debugging.
template <typename T>
int SharedPtr_c<T>::WeakCount() const
{
    return (mSharedCount != NULL) ? mSharedCount->WeakCount() : 0;
}

// Enable reference count logging for this object.
template <typename T>
OS_Status_t SharedPtr_c<T>::CreateLog()
{
    OS_Status_t Status = OS_ERROR;
    if (mSharedCount != NULL)
    {
        Status = mSharedCount->CreateLog();
    }
    return Status;
}

// Write reference count log to kernel log.
template <typename T>
void SharedPtr_c<T>::DumpLog() const
{
    if (mSharedCount != NULL)
    {
        mSharedCount->Dump();
    }
}

// Make NULL pointer point to specified object.
template <typename T>
OS_Status_t SharedPtr_c<T>::Set(T *Ptr)
{
    SE_ASSERT(mPtr == NULL);
    SE_ASSERT(mSharedCount == NULL);

    if (Ptr != NULL)
    {
        mSharedCount = new SharedCount_c(Ptr);
        if (mSharedCount == NULL)
        {
            return OS_ERROR;
        }
        mPtr = Ptr;
    }

    return OS_NO_ERROR;
}

//////////////////////////////////////////////////////////////////////////////
// WeakPtr_c methods

// Construct NULL pointer.
template <typename T>
WeakPtr_c<T>::WeakPtr_c()
    : mSharedCount(NULL)
{
}

// Destruct weak pointer.  This has no impact on the shared object lifetime.
template <typename T>
WeakPtr_c<T>::~WeakPtr_c()
{
    if (mSharedCount != NULL)
    {
        mSharedCount->DecWeak();
    }
}

// Interchange this pointer with Other without altering shared object.
template <typename T>
void WeakPtr_c<T>::Swap(WeakPtr_c<T> &Other)
{
    ::Swap(mSharedCount, Other.mSharedCount);
}

// Construct weak pointer pointing the same object as specified weak pointer.
template <typename T>
WeakPtr_c<T>::WeakPtr_c(const WeakPtr_c<T> &Other)
    : mSharedCount(NULL)
{
    Copy(Other.mSharedCount);
}

// Construct weak pointer pointing the same object as specified shared pointer.
template <typename T>
WeakPtr_c<T>::WeakPtr_c(const SharedPtr_c<T> &Other)
    : mSharedCount(NULL)
{
    Copy(Other.mSharedCount);
}

// Copy constructor helper.
template <typename T>
void WeakPtr_c<T>::Copy(SharedCount_c *SharedCount)
{
    if (SharedCount != NULL)
    {
        SharedCount->IncWeak();
        mSharedCount = SharedCount;
    }
}

// Make this pointer point to the same object as specified shared pointer.
template <typename T>
WeakPtr_c<T> &WeakPtr_c<T>::operator=(const SharedPtr_c<T> &Other)
{
    WeakPtr_c<T> Tmp(Other);
    Swap(Tmp);

    return *this;
}

// Make this pointer point to the same object as specified weak pointer.
template <typename T>
WeakPtr_c<T> &WeakPtr_c<T>::operator=(const WeakPtr_c<T> &Other)
{
    WeakPtr_c<T> Tmp(Other);
    Swap(Tmp);

    return *this;
}

// Return a shared pointer pointing to the same object as this weak pointer.
// If the weak pointer has expired, return a NULL shared pointer.
template <typename T>
SharedPtr_c<T> WeakPtr_c<T>::Lock() const
{
    SharedPtr_c<T> sp;
    if (mSharedCount != NULL)
    {
        void *Ptr = mSharedCount->IncStrongIfPositive();
        if (Ptr != NULL)
        {
            sp.mSharedCount = mSharedCount;
            sp.mPtr = static_cast<T *>(Ptr);
        }
    }
    return sp;
}

// Return the strong reference count of the shared object.
template <typename T>
int WeakPtr_c<T>::UseCount() const
{
    return (mSharedCount != NULL) ? mSharedCount->StrongCount() : 0;
}

//////////////////////////////////////////////////////////////////////////////
// Factory functions
//
// Preferred way for creating a shared object and tying it to shared pointer.
//
// Using these functions rather than calling ResetIfNotNull() directly may be
// more efficient in the future thanks to a known optimization (allocate shared
// object and control block together, see std::make_shared()).
//
// Return error if object allocation fails or if pointer internal state creation
// fails.

template <typename T>
OS_Status_t MakeShared(SharedPtr_c<T> *PtrPtr)
{
    return PtrPtr->ResetIfNotNull(new T);
}

template <typename T, typename A1>
OS_Status_t MakeShared(A1 Arg1, SharedPtr_c<T> *PtrPtr)
{
    return PtrPtr->ResetIfNotNull(new T(Arg1));
}

template <typename T, typename A1, typename A2>
OS_Status_t MakeShared(A1 Arg1, A2 Arg2, SharedPtr_c<T> *PtrPtr)
{
    return PtrPtr->ResetIfNotNull(new T(Arg1, Arg2));
}

template <typename T, typename A1, typename A2, typename A3>
OS_Status_t MakeShared(A1 Arg1, A2 Arg2, A3 Arg3, SharedPtr_c<T> *PtrPtr)
{
    return PtrPtr->ResetIfNotNull(new T(Arg1, Arg2, Arg3));
}

template <typename T, typename A1, typename A2, typename A3, typename A4>
OS_Status_t MakeShared(A1 Arg1, A2 Arg2, A3 Arg3, A4 Arg4, SharedPtr_c<T> *PtrPtr)
{
    return PtrPtr->ResetIfNotNull(new T(Arg1, Arg2, Arg3, Arg4));
}

#endif
