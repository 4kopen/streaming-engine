/************************************************************************
Copyright (C) 2003-2014 STMicroelectronics. All Rights Reserved.

This file is part of the Streaming Engine.

Streaming Engine is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License version 2 as
published by the Free Software Foundation.

Streaming Engine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with Streaming Engine; see the file COPYING.  If not, write to the Free
Software Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307,
USA.

The Streaming Engine Library may alternatively be licensed under a
proprietary license from ST.
************************************************************************/

#undef TRACE_TAG
#define TRACE_TAG "StackTraceStore_c"

#include "stack_trace_store.h"

StackTraceStore_c::StackTraceStore_c()
    : mStackTraceStoreLock()
    , mHeaders()
    , mFrames()
{
    OS_InitializeMutex(&mStackTraceStoreLock);
}

StackTraceStore_c::~StackTraceStore_c()
{
    SE_DEBUG(group_metadebug, "headers: size=%u bytes capacity=%u bytes\n",
             mHeaders.Size() * sizeof(mHeaders[0]), mHeaders.Capacity() * sizeof(mHeaders[0]));
    SE_DEBUG(group_metadebug, "frames: size=%u bytes capacity=%u bytes\n",
             mFrames.Size() * sizeof(mFrames[0]), mFrames.Capacity() * sizeof(mFrames[0]));

    OS_TerminateMutex(&mStackTraceStoreLock);
}

// Add stack trace identified by Frames[0..Count) and store opaque value
// identifying it in *Handle.  Return error on out-of-memory.
OS_Status_t StackTraceStore_c::Store(unsigned long *Frames, size_t Count, size_t *Handle)
{
    SE_ASSERT(Count > 0);

    OS_LockMutex(&mStackTraceStoreLock);

    // Return existing stack trace matching new one if any.
    for (size_t i = 0; i < mHeaders.Size(); ++i)
    {
        const Header_s &hdr = mHeaders[i];
        if (hdr.mFrameCount == Count &&
            memcmp(&mFrames[hdr.mFirstFrame], Frames, sizeof(Frames[0]) * Count) == 0)
        {
            OS_UnLockMutex(&mStackTraceStoreLock);
            *Handle = i;
            return OS_NO_ERROR;
        }
    }

    // Pre-allocate memory for storing new stack trace.   Doing it now
    // simplifies error handling.
    if (mHeaders.ReserveAtLeast(mHeaders.Size() + 1) != OS_NO_ERROR ||
        mFrames.ReserveAtLeast(mFrames.Size() + Count) != OS_NO_ERROR)
    {
        OS_UnLockMutex(&mStackTraceStoreLock);
        SE_ERROR("out-of-memory\n");
        return OS_ERROR;
    }

    size_t FirstFrame = mFrames.Size();
    OS_Status_t Status = mFrames.Resize(FirstFrame + Count);
    SE_ASSERT(Status == OS_NO_ERROR); // pre-allocated above
    memcpy(&mFrames[FirstFrame], Frames, Count * sizeof(Frames[0]));

    Header_s hdr;
    hdr.mFirstFrame = FirstFrame;
    hdr.mFrameCount = Count;
    Status = mHeaders.PushBack(hdr);
    SE_ASSERT(Status == OS_NO_ERROR); // pre-allocated above

    OS_UnLockMutex(&mStackTraceStoreLock);

    *Handle = mHeaders.Size() - 1;
    return OS_NO_ERROR;
}

// Return index of first frame of stack trace identified by Handle.
// Use FrameAt() for mapping index to frame value.
size_t StackTraceStore_c::FirstFrame(size_t Handle) const
{
    OS_LockMutex(&mStackTraceStoreLock);
    size_t ret = mHeaders[Handle].mFirstFrame;
    OS_UnLockMutex(&mStackTraceStoreLock);
    return ret;
}

// Return number of  frames in stack trace identified by Handle.
size_t StackTraceStore_c::FrameCount(size_t Handle) const
{
    OS_LockMutex(&mStackTraceStoreLock);
    size_t ret = mHeaders[Handle].mFrameCount;
    OS_UnLockMutex(&mStackTraceStoreLock);
    return ret;
}

// Return frame at given index.
// Usage:
// size_t FirstFrame = Store.FirstFrame(Handle);
// for (i = 0; i < Store.FrameCount(Handle); ++i)
// {
//      unsigned long Frame = Store.FrameAt(FirstFrame + i);
//      ...
// }
unsigned long StackTraceStore_c::FrameAt(size_t Idx) const
{
    OS_LockMutex(&mStackTraceStoreLock);
    unsigned long ret = mFrames[Idx];
    OS_UnLockMutex(&mStackTraceStoreLock);
    return ret;
}
