/************************************************************************
Copyright (C) 2003-2014 STMicroelectronics. All Rights Reserved.

This file is part of the Streaming Engine.

Streaming Engine is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License version 2 as
published by the Free Software Foundation.

Streaming Engine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with Streaming Engine; see the file COPYING.  If not, write to the Free
Software Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307,
USA.

The Streaming Engine Library may alternatively be licensed under a
proprietary license from ST.
************************************************************************/

#ifndef H_STACK_TRACE_STORE
#define H_STACK_TRACE_STORE

#include "osinline.h"
#include "vector.h"

// Store set of stack traces in space-efficient way.
//
// Stack traces is very useful for debugging but direct logging to kernel log is
// often not practical (too verbose, too much impact on performance).
//
// Storing call stacks in memory for later selective logging, for example
// printing out history of leaked buffers only, is a better option.  However,
// simply accumulating stack traces in memory can quickly consume too much space
// and is wasteful because stack traces are often identical.
//
// For reducing memory footprint, this class stores once only identical stack
// traces.  Furthermore, an object of this class can be shared by several
// loggers for further reducing footprint.
//
// Thread-safe.
class StackTraceStore_c
{
public:
    StackTraceStore_c();
    ~StackTraceStore_c();

    OS_Status_t Store(unsigned long *Frames, size_t Count, size_t *Handle);

    size_t FirstFrame(size_t Handle) const;
    size_t FrameCount(size_t Handle) const;
    unsigned long FrameAt(size_t Idx) const;

private:
    // Describe one stack trace.
    struct Header_s
    {
        uint16_t mFirstFrame;
        uint16_t mFrameCount;
    };

    // Protect members in this class.
    mutable OS_Mutex_t mStackTraceStoreLock;

    // All stack traces accumulated so far.
    Vector_c<Header_s> mHeaders;

    // Store the actual stack frames.
    Vector_c<unsigned long> mFrames;

    DISALLOW_COPY_AND_ASSIGN(StackTraceStore_c);
};

#endif
