/************************************************************************
Copyright (C) 2003-2014 STMicroelectronics. All Rights Reserved.

This file is part of the Streaming Engine.

Streaming Engine is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License version 2 as
published by the Free Software Foundation.

Streaming Engine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with Streaming Engine; see the file COPYING.  If not, write to the Free
Software Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307,
USA.

The Streaming Engine Library may alternatively be licensed under a
proprietary license from ST.
************************************************************************/

#include "timestamp_extrapolator.h"

#define PTS_TOLERANCE_IN_USEC 300ll
#define MAX_INTERPOLATOR_ACCUMULATED_DELTA 50000ll // max 50ms

TimeStampExtrapolator_c::TimeStampExtrapolator_c()
    : mReferenceTimeStamp()
    , mExtrapolation(0)
    , mLastTimeStamp()
    , mAccumulatedDelta(0)
    , mThreshold(PTS_TOLERANCE_IN_USEC)
    , mLastCorrectionDelta(INVALID_TIME)
    , mAlwaysConfirmJump(false)
{
    ResetExtrapolator();
}

void TimeStampExtrapolator_c::SetTimeStampAsReference(TimeStamp_c NewReference)
{
    mReferenceTimeStamp     = NewReference;
    mExtrapolation          = 0;
    mLastCorrectionDelta    = INVALID_TIME;
    mAccumulatedDelta       = 0;
}

void TimeStampExtrapolator_c::ResetExtrapolator()
{
    SE_DEBUG(group_timestamps, "\n");

    mLastTimeStamp = TimeStamp_c();
    SetTimeStampAsReference(mLastTimeStamp);
}

void TimeStampExtrapolator_c::SetJitterThreshold(int64_t NewThreshold)
{
    if (!ValidTime(NewThreshold))
    {
        SE_ERROR("New threshold is not valid\n");
    }
    else
    {
        mThreshold = NewThreshold;
    }
}

bool TimeStampExtrapolator_c::JumpConfirmed(int64_t DeltaDelta)
{
    if (mAlwaysConfirmJump) { return true; }

    if (!ValidTime(mLastCorrectionDelta)) { return false; }

    if (abs64(DeltaDelta - mLastCorrectionDelta) <= mThreshold)
    {
        if (DeltaDelta > 0)
        {
            // Forward jump => always confirmed
            return true;
        }
        else if (DeltaDelta < -PTS_BACKWARD_JUMP_THRESHOLD)
        {
            // backward jump => confirmed only if jump is greater than backward jump threshold
            return true;
        }
    }

    return false;
}

TimeStamp_c TimeStampExtrapolator_c::GetTimeStamp(TimeStamp_c InTimeStamp, uint32_t Nominator, uint32_t Denominator)
{
    bool UseSynthetic = false;
    TimeStamp_c OutTimeStamp;
    TimeStamp_c Extrapolated;

    SE_EXTRAVERB(group_timestamps
                 , "InTimeStamp=%lld (%d) Ref=%lld (%d) Last=%lld (%d) LastCorrectionDelta=%lld\n"
                 , InTimeStamp.NativeValue(), InTimeStamp.TimeFormat()
                 , mReferenceTimeStamp.NativeValue(), mReferenceTimeStamp.TimeFormat()
                 , mLastTimeStamp.NativeValue(), mLastTimeStamp.TimeFormat()
                 , mLastCorrectionDelta
                );

    if (!mLastTimeStamp.IsValid() && !mReferenceTimeStamp.IsValid())
    {
        mLastTimeStamp = InTimeStamp;
        SetTimeStampAsReference(InTimeStamp);
        SE_DEBUG(group_timestamps, "Cannot extrapolate yet. timestamp left unchanged : %lld\n",
                 InTimeStamp.NativeValue());
        UpdateNextExtrapolatedTime(Nominator, Denominator);
        return InTimeStamp;
    }

    if (mReferenceTimeStamp.IsValid())
    {
        // Extrapolated: adds accumulated extrapolation to Reference, in same units as reference
        Extrapolated = TimeStamp_c::AddUsec(mReferenceTimeStamp, mExtrapolation.LongLongIntegerPart());
    }

    if (!InTimeStamp.IsValid())
    {
        UseSynthetic = true;
    }
    else
    {
        // Even if Input is valid extra checks:
        if (mLastTimeStamp.IsValid() && Extrapolated.IsValid())
        {
            int64_t RealDelta      = TimeStamp_c::DeltaUsec(InTimeStamp, mLastTimeStamp);
            int64_t SyntheticDelta = TimeStamp_c::DeltaUsec(Extrapolated, mLastTimeStamp);
            int64_t DeltaDelta     = RealDelta - SyntheticDelta;

            SE_EXTRAVERB(group_timestamps, "RealDelta %lld SyntheticDelta %lld DeltaDelta %lld AccumulatedDelta %lld \n", RealDelta, SyntheticDelta, DeltaDelta, mAccumulatedDelta);

            // Check that the predicted and actual times deviate by no more than the threshold
            if (abs64(DeltaDelta) > mThreshold)
            {
                // - use synthetic PTS until jump is confirmed
                // - also manage 0-PTS case: bug13276
                // In that case, PTS is marked as invalid and predicted timestamp is used instead.
                // also dont cumulate too big delta
                if (abs64(mAccumulatedDelta) > MAX_INTERPOLATOR_ACCUMULATED_DELTA && (0 != InTimeStamp.NativeValue()))
                {
                    SE_DEBUG2(group_timestamps, group_se_pipeline, "Accumulated too large delta %lld in interpolator : now using real playback time %lld\n",
                              mAccumulatedDelta, InTimeStamp.NativeValue());

                }
                else if (JumpConfirmed(DeltaDelta) && (0 != InTimeStamp.NativeValue()))
                {
                    SE_WARNING("Unexpected change in playback time. Expected %lld, got %lld (deltas: exp. %lld us got %lld us)\n",
                               Extrapolated.NativeValue(), InTimeStamp.NativeValue(), SyntheticDelta, RealDelta);
                }
                else
                {
                    UseSynthetic = true;
                    mLastCorrectionDelta = RealDelta - SyntheticDelta;
                    SE_DEBUG(group_timestamps, "Wrong PTS detected %lld: Fixing Using synthetic PTS %lld\n",
                             InTimeStamp.NativeValue(), Extrapolated.NativeValue());
                }
            }
        }
    }

    if (UseSynthetic && Extrapolated.IsValid())
    {
        OutTimeStamp = Extrapolated;

        if (InTimeStamp.IsValid())
        {
            SE_DEBUG(group_timestamps, "Using corrected PTS for frame : %lld (delta to previous %lld us, delta to original %lld us)\n",
                     OutTimeStamp.NativeValue(), TimeStamp_c::DeltaUsec(OutTimeStamp, mLastTimeStamp), TimeStamp_c::DeltaUsec(OutTimeStamp, InTimeStamp));
            mAccumulatedDelta = TimeStamp_c::DeltaUsec(OutTimeStamp, InTimeStamp);
        }
        else
        {
            SE_DEBUG(group_timestamps, "Using synthetic PTS for frame : %lld (delta to previous %lld us)\n",
                     OutTimeStamp.NativeValue(), TimeStamp_c::DeltaUsec(OutTimeStamp, mLastTimeStamp));
        }
    }
    else
    {
        // Save the input TimeStamp as a new reference and reset deltas
        SetTimeStampAsReference(InTimeStamp);
        OutTimeStamp = InTimeStamp;

        SE_DEBUG(group_timestamps, "Using original PTS for frame : %lld (delta to previous %lld us)\n",
                 OutTimeStamp.NativeValue(), TimeStamp_c::DeltaUsec(OutTimeStamp, mLastTimeStamp));
    }

    mLastTimeStamp = OutTimeStamp;

    UpdateNextExtrapolatedTime(Nominator, Denominator);

    return OutTimeStamp;
}

void TimeStampExtrapolator_c::UpdateNextExtrapolatedTime(uint32_t Numerator, uint32_t Denominator)
{
    SE_ASSERT(Denominator != 0);

    if (0 == Numerator)
    {
        SE_DEBUG(group_timestamps, "Numerator is 0\n");
        //Nothing to do
        return;
    }

    if (!mLastTimeStamp.IsValid())
    {
        // reset the extrapolation.
        mExtrapolation = 0;
        return;
    }

    Rational_t FrameDuration =  Rational_t(Numerator * 1000000ull, Denominator);
    mExtrapolation = mExtrapolation + FrameDuration;
}

#undef TS_EXTRAPOLATOR_REDUCE_BY_10N
