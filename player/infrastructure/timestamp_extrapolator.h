/************************************************************************
Copyright (C) 2003-2014 STMicroelectronics. All Rights Reserved.

This file is part of the Streaming Engine.

Streaming Engine is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License version 2 as
published by the Free Software Foundation.

Streaming Engine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with Streaming Engine; see the file COPYING.  If not, write to the Free
Software Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307,
USA.

The Streaming Engine Library may alternatively be licensed under a
proprietary license from ST.
************************************************************************/
#ifndef TIMESTAMP_EXTRAPOLATOR_H
#define TIMESTAMP_EXTRAPOLATOR_H

#include "timestamps.h"
/**
 * A class to handle timestamp linear 1 order extrapolation. It should be
 * generic enough to handle audio and video.
 *
 * It has a Jitter mThreshold property: above this threshold it considers the
 * time line to be discontinuous
 *
 * Known Limitations:
 *
 * 1/ Internal storage of the extrapolated times is through a single uint64_t:
 * user needs to take care of deciding the time format for the extrapolator to
 * strike the right balance between range ans precision (default time format is
 * microseconds).
 *
 * 2/ Because it manipulates time stamps: so far implementation only
 * extrapolates time forward, real-time: there is no support yet of trick-modes,
 * or backward time.
 */
class TimeStampExtrapolator_c
{
public:

    TimeStampExtrapolator_c();

    /**
     * Resets extrapolation mechanism to initial state.
     */
    void ResetExtrapolator();

    /**
     * Sets the jitter threshold
     *
     *
     * @param NewThreshold the tolerance in usec
     */
    void SetJitterThreshold(int64_t NewThreshold);

    /**
     * Sets the jump mode
     *
     *
     * @param AlwaysConfirmJump boolean flag to confirm all PTS jumps
     */
    void SetJumpMode(bool AlwaysConfirmJump) { mAlwaysConfirmJump = AlwaysConfirmJump; }

    /**
     * If the input timestamp is not valid compared to extrapolator state, it
     * returns an extrapolated timestamp based on extrapolator state, else
     * returns the InputTimeStamp.
     *
     * Also updates the internal state for the next time stamp extrapolation,
     * with the duration of the current frame expressed in seconds by (Nominator
     * / Denominator).
     *
     * @param InputTimeStamp timestamp we want to check against extrapolation
     *               context
     * @param Nominator  nominator of the duration of the current frame to
     *           extrapolate the timestamp of the next frame (e.g. Number
     *           of samples)
     * @param Denominator denominator of the duration of the current frame to
     *            extrapolate the timestamp of the next frame (e.g.
     *            Sampling Frequency)
     *
     * @return TimeStamp_c the best timestamp, in the same format at InputTimeStamp
     */
    TimeStamp_c GetTimeStamp(TimeStamp_c InputTimeStamp, uint32_t Nominator, uint32_t Denominator);

    /**
     * Gets the whether there is a valid extrapolated value
     *
     *
     * @return boolean
     */
    bool IsValid() const
    {return mReferenceTimeStamp.IsValid();}

private:
    /// Last Injected Valid TimeStamp, in same units as injected
    TimeStamp_c mReferenceTimeStamp;

    /// Accumulated mExtrapolation in usec
    Rational_t mExtrapolation;

    ///
    TimeStamp_c mLastTimeStamp;

    ///
    uint64_t mAccumulatedDelta;

    /// The maximum value in usec by which the actual TimeStamp is permitted to differ from the synthesised timestamp
    /// (use to identify bad streams or poor predictions).
    int64_t mThreshold;

    ///
    int64_t mLastCorrectionDelta;

    /// mode to confirm all jumps
    bool mAlwaysConfirmJump;

    /// Sets the new reference, resets extrapolation to 0, resets errortracker
    void SetTimeStampAsReference(TimeStamp_c NewReference);

    /**
     * Updates the expected extrapolated timestamp of the next frame with a
     * time equal to: (Nominator/Denomitor seconds)
     *
     * This will be used to synthesise a timestamp if this is missing from the
     * subsequent frame.
     *
     * @param Nominator  (e.g. Number of samples)
     * @param Denominator (e.g. Sampling Frequency)
     */
    void UpdateNextExtrapolatedTime(uint32_t Nominator, uint32_t Denominator);
    bool CheckCorrectionRange(int64_t DeltaUsec);
    bool JumpConfirmed(int64_t DeltaDelta);
};

#endif // TIMESTAMP_EXTRAPOLATOR_H
