/************************************************************************
Copyright (C) 2003-2014 STMicroelectronics. All Rights Reserved.

This file is part of the Streaming Engine.

Streaming Engine is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License version 2 as
published by the Free Software Foundation.

Streaming Engine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with Streaming Engine; see the file COPYING.  If not, write to the Free
Software Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307,
USA.

The Streaming Engine Library may alternatively be licensed under a
proprietary license from ST.
************************************************************************/

#include "timestamps.h"
#include "timestamp_extrapolator.h"

TimeStamp_c::TimeStamp_c()
    : mNativeValue(INVALID_TIME)
    , mNativeFormat(TIME_FORMAT_US)
{}

TimeStamp_c::TimeStamp_c(int64_t ValueIn, stm_se_time_format_t FormatIn)
    : mNativeValue(ValueIn)
    , mNativeFormat(FormatIn)
{
    if (!IsNativeTimeFormatCoherent(mNativeFormat, 0))
    {
        SE_ERROR("invalid format provided:%d - forcing USEC format\n", mNativeFormat);
        mNativeFormat = TIME_FORMAT_US;
    }
}

int64_t TimeStamp_c::Value(stm_se_time_format_t FormatOut) const
{
    switch (FormatOut)
    {
    case TIME_FORMAT_US    : return uSecValue();
    case TIME_FORMAT_PTS   : return PtsValue();
    case TIME_FORMAT_27MHz : return VidValue();
    default                :
        SE_FATAL("Invalid time format: %d\n", mNativeFormat);
        return INVALID_TIME;
    }
}

void TimeStamp_c::Normalize()
{
    switch (mNativeFormat)
    {
    case TIME_FORMAT_US:
        break;
    case TIME_FORMAT_PTS:
        mNativeValue = mNativeValue & TIMESTAMP_MASK_FORMAT_PTS;
        break;
    case TIME_FORMAT_27MHz:
        mNativeValue = mNativeValue & TIMESTAMP_MASK_FORMAT_27MHZ;
        break;
    default:
        SE_FATAL("Invalid time format: %d\n", mNativeFormat);
        break;
    }
}

int64_t TimeStamp_c::uSecValue() const
{
    //No arithmetic for invalid time stamps, 0
    if ((NotValidTime(mNativeValue))
        || (0 == mNativeValue))
    {
        return mNativeValue;
    }

    switch (mNativeFormat)
    {
    case TIME_FORMAT_US :
        return (mNativeValue);

    case TIME_FORMAT_PTS :
        // T*1000/90 = T*200/18 => (T*200+9)/18
        return (((mNativeValue * 200LL) + 9LL) / 18LL);

    case TIME_FORMAT_27MHz :
        // T/27
        return ((mNativeValue + (27LL / 2)) / 27LL);

    default:
        SE_FATAL("Invalid time format: %d\n", mNativeFormat);
        return mNativeValue;
    }
}

int64_t TimeStamp_c::mSecValue() const
{
    //No arithmetic for invalid time stamps, 0
    if ((NotValidTime(mNativeValue))
        || (0 == mNativeValue))
    {
        return mNativeValue;
    }

    switch (mNativeFormat)
    {
    case TIME_FORMAT_US:
        // T/1000 => (T+500)/1000
        return ((mNativeValue + 500LL) / 1000LL);

    case TIME_FORMAT_PTS :
        // T/90 => (T+45)/90
        return ((mNativeValue + 45LL) / 90LL);

    case TIME_FORMAT_27MHz:
        // T/27000 => (T + 27000/2) / 27000;
        return ((mNativeValue + 13500LL) / 27000LL);

    default:
        SE_FATAL("Invalid time format: %d\n", mNativeFormat);
        return mNativeValue;
    }
}

int64_t TimeStamp_c::PtsValue() const
{
    //No arithmetic for invalid time stamps, 0
    if ((NotValidTime(mNativeValue))
        || (0 == mNativeValue))
    {
        return mNativeValue;
    }

    switch (mNativeFormat)
    {
    case TIME_FORMAT_US   :
        // T*90/1000 => (T*90 + 500)/1000 => (T*9 + 50)/100
        return (((mNativeValue * 9LL) + 50LL) / 100LL) & TIMESTAMP_MASK_FORMAT_PTS;

    case TIME_FORMAT_PTS:
        return (mNativeValue) & TIMESTAMP_MASK_FORMAT_PTS;

    case TIME_FORMAT_27MHz:
        // T/300 => (T+150)/300
        return ((mNativeValue + 150LL) / 300LL) & TIMESTAMP_MASK_FORMAT_PTS;

    default:
        SE_FATAL("Invalid time format: %d\n", mNativeFormat);
        return mNativeValue;
    }
}

int64_t TimeStamp_c::VidValue() const
{
    //No arithmetic for invalid time stamps, 0
    if ((NotValidTime(mNativeValue))
        || (0 == mNativeValue))
    {
        return mNativeValue;
    }

    switch (mNativeFormat)
    {
    case TIME_FORMAT_US   : return (mNativeValue * 27LL) & TIMESTAMP_MASK_FORMAT_27MHZ;
    case TIME_FORMAT_PTS  : return (mNativeValue * 300LL) & TIMESTAMP_MASK_FORMAT_27MHZ;
    case TIME_FORMAT_27MHz: return (mNativeValue) & TIMESTAMP_MASK_FORMAT_27MHZ;
    default:
        SE_FATAL("Invalid time format: %d\n", mNativeFormat);
        return mNativeValue;
    }
}

TimeStamp_c  TimeStamp_c::TimeFormat(stm_se_time_format_t TimeFormatOut) const
{
    return TimeStamp_c(this->Value(TimeFormatOut), TimeFormatOut);
}

int64_t TimeStamp_c::DeltaUsec(const TimeStamp_c &left, const TimeStamp_c &right)
{
    // output Errors if working on invalid or incompatible values,
    // TODO(pht) shall be changed to Fatals eventually
    if (!left.IsValid())
    {
        SE_ERROR("left is invalid\n");
    }
    if (!right.IsValid())
    {
        SE_ERROR("right is invalid\n");
    }
    if (left.mNativeFormat != right.mNativeFormat)
    {
        SE_ERROR("left.mNativeFormat (%d) != right.mNativeFormat (%d)\n", left.mNativeFormat, right.mNativeFormat);
    }

    switch (left.mNativeFormat)
    {
    case TIME_FORMAT_US:
        return left.mNativeValue - right.mNativeValue;

    case TIME_FORMAT_PTS:
    {
        int64_t delta = ((left.mNativeValue << 31) >> 31) - ((right.mNativeValue << 31) >> 31);
        delta = (delta << 31) >> 31;
        int sign = delta > 0 ? 1 : -1;
        return (((delta * 200LL) + sign * 9LL) / 18LL);
    }

    case TIME_FORMAT_27MHz:
    {
        int64_t delta = ((left.mNativeValue << 32) >> 32) - ((right.mNativeValue << 32) >> 32);
        delta = (delta << 32) >> 32;
        int sign = delta > 0 ? 1 : -1;
        return ((delta + sign * (27LL / 2)) / 27LL);
    }

    default:
        SE_FATAL("Invalid time format: %d\n", left.mNativeFormat);
        return 0;
    }
}

TimeStamp_c TimeStamp_c::AddUsec(const TimeStamp_c &left, int64_t uSecValue)
{
    if (!left.IsValid()) { return left; }

    int64_t nativeValue;
    int sign = uSecValue > 0 ? 1 : -1;

    switch (left.mNativeFormat)
    {
    case TIME_FORMAT_US:
        nativeValue = uSecValue;
        break;

    case TIME_FORMAT_PTS:
        nativeValue = (((uSecValue * 9LL) + sign * 50LL) / 100LL);
        nativeValue = nativeValue & TIMESTAMP_MASK_FORMAT_PTS;
        break;

    case TIME_FORMAT_27MHz:
        nativeValue = uSecValue * 27LL;
        nativeValue = nativeValue & TIMESTAMP_MASK_FORMAT_27MHZ;
        break;

    default:
        SE_ERROR("Invalid time format: %d)\n", left.mNativeFormat);
        return TimeStamp_c();
    }

    TimeStamp_c Result;

    Result.mNativeValue     = left.mNativeValue + nativeValue;
    Result.mNativeFormat    = left.mNativeFormat;

    Result.Normalize();

    return Result;
}

TimeStamp_c TimeStamp_c::AddNativeOffset(const TimeStamp_c &left, int64_t offsetValue)
{
    if (!left.IsValid()) { return left; }

    TimeStamp_c Result;

    Result.mNativeValue     = left.mNativeValue + offsetValue;
    Result.mNativeFormat    = left.mNativeFormat;

    Result.Normalize();

    return Result;
}

int operator<(const TimeStamp_c &left, const TimeStamp_c &right)
{
    return TimeStamp_c::DeltaUsec(left, right) < 0;
}

int operator>(const TimeStamp_c &left, const TimeStamp_c &right)
{
    return TimeStamp_c::DeltaUsec(left, right) > 0;
}

int operator>=(const TimeStamp_c &left, const TimeStamp_c &right)
{
    return TimeStamp_c::DeltaUsec(left, right) >= 0;
}

int operator!=(const TimeStamp_c &left, const TimeStamp_c &right)
{
    return TimeStamp_c::DeltaUsec(left, right) != 0;
}

bool TimeStamp_c::IsNativeTimeFormatCoherent(stm_se_time_format_t NativeTimeFormat, uint64_t NativeTime)
{
    switch (NativeTimeFormat)
    {
    case TIME_FORMAT_US:
        break;
    case TIME_FORMAT_PTS:
        if (NotValidPtsTime(NativeTime))
        {
            SE_ERROR("Unconsistent native time %llu for PTS\n", NativeTime);
            return false;
        }
        break;
    case TIME_FORMAT_27MHz:
        break;
    default:
        SE_ERROR("Invalid native time format %u\n", NativeTimeFormat);
        return false;
    }

    return true;
}
