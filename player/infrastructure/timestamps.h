/************************************************************************
Copyright (C) 2003-2014 STMicroelectronics. All Rights Reserved.

This file is part of the Streaming Engine.

Streaming Engine is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License version 2 as
published by the Free Software Foundation.

Streaming Engine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with Streaming Engine; see the file COPYING.  If not, write to the Free
Software Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307,
USA.

The Streaming Engine Library may alternatively be licensed under a
proprietary license from ST.
************************************************************************/
#ifndef TIMESTAMPS_H
#define TIMESTAMPS_H

#include "player_types.h"

#undef TRACE_TAG
#define TRACE_TAG   "TimeStamp_c"

// @brief A class providing interface to manipulate timestamps in
// different units of time.
//
//
// The timestamp storage unit is decided upon setting the value: the class
// makes no decision about storage format: the responsability of
// maintainging resolution and range is with caller.
//
// This decision also explains why there is no overload of "operator=" :
// the result of "=" is always exactly the right value, without dependency
// on the left value.
//
// This also mean that arithmetic operations between TimeStamp_c objects
// of different storage format return an InvalidTime stamp, as the class
// does not decide.
//
// The internal storage is a unsigned 64 bit integer:
//
// at the highest precision this provides a range of 4+ days in 1/27Mhz
// units.

#define TIMESTAMP_MASK_FORMAT_27MHZ 0xFFFFFFFFLL
#define TIMESTAMP_MASK_FORMAT_PTS   0x1FFFFFFFFLL

class TimeStamp_c
{
public:
    // An initialising constructor.
    // @param TimestampIn the value, in TimeFormatIn
    // @param TimeFormatIn the format of TimestampIn, will be used for storage
    TimeStamp_c(int64_t TimestampIn, stm_se_time_format_t TimeFormatIn);

    // Default Contructor: Will set the mNativeValue to INVALID_TIME
    // and format to TIME_FORMAT_US
    TimeStamp_c();

    bool IsValid() const
    {
        return (ValidTime((uint64_t)mNativeValue));
    }

    bool IsUnspecified() const
    {
        return (mNativeValue == UNSPECIFIED_TIME) ? true : false;
    }

    //-------------------------------
    // Getters
    //-------------------------------

    // returns the value in a format, performing conversion if required
    // @param OutTimeFormat the format we want to get the value in
    // @return TimeStamp_c
    int64_t Value(stm_se_time_format_t OutTimeFormat) const;

    // returns the value in its native stored format
    int64_t NativeValue() const
    {
        return mNativeValue;
    }

    // Gets the native Stored Format
    stm_se_time_format_t TimeFormat() const
    {
        return mNativeFormat;
    }

    // Get value in micro seconds
    int64_t uSecValue()  const;

    // Get value in milli seconds
    int64_t mSecValue()  const;

    // Get value in 1/90kHz, coded on 33 bits
    int64_t PtsValue()  const;

    // Get value in 1/27MHz, coded on 32 bits
    int64_t VidValue()  const;

    //-------------------------------
    // Setters
    //-------------------------------

    // returns a TimeStamp_c converted to requested format
    // e.g Offset = Offset.TimeFormat( InputTimeStamp.TimeFormat() );
    // @param TimeFormatOut the format we want the new object in
    // @return TimeStamp_c a copy of the object, converted to TimeFormatOut
    TimeStamp_c TimeFormat(stm_se_time_format_t TimeFormatOut) const;

    static int64_t DeltaUsec(const TimeStamp_c &left, const TimeStamp_c &right);

    static TimeStamp_c AddUsec(const TimeStamp_c &left, int64_t UsecValue);

    static TimeStamp_c AddNativeOffset(const TimeStamp_c &left, int64_t offsetValue);

    //-------------------------------
    // Operators
    //-------------------------------

    friend int operator<(const TimeStamp_c &left, const TimeStamp_c &right);
    friend int operator>(const TimeStamp_c &left, const TimeStamp_c &right);
    friend int operator>=(const TimeStamp_c &left, const TimeStamp_c &right);
    friend int operator!=(const TimeStamp_c &left, const TimeStamp_c &right);

    // static checker
    static bool IsNativeTimeFormatCoherent(stm_se_time_format_t NativeTimeFormat, uint64_t NativeTime);

private:
    int64_t              mNativeValue;
    stm_se_time_format_t mNativeFormat;

    // Normalize TimeStamp Value according to native format
    void Normalize();
};

#endif // TIMESTAMPS_H
