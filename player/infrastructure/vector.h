/************************************************************************
Copyright (C) 2003-2014 STMicroelectronics. All Rights Reserved.

This file is part of the Streaming Engine.

Streaming Engine is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License version 2 as
published by the Free Software Foundation.

Streaming Engine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with Streaming Engine; see the file COPYING.  If not, write to the Free
Software Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307,
USA.

The Streaming Engine Library may alternatively be licensed under a
proprietary license from ST.
************************************************************************/

#ifndef H_VECTOR
#define H_VECTOR

#include "utility.h"
#include "osinline.h"

#include "new.h"

// A growing array inspired from std::vector.
//
// This class diverges from the standard C++ vector in the following ways:
//
// - It implements only a small subset of the standard (implement-on-demand).
//
// - It returns errors on out-of-memory rather than raising exceptions.
//
// - It is not exception-safe as SE does not use exceptions.
//
// - Names are similar-enough to allow learning from modern C++ literature but
// have been transmogrified to follow the SE coding style.
//
// Crash course:
//
//      // Create an empty vector and append items to it.
//      // The underlying array is periodically reallocated and the old content
//      // copied into the new array.
//      Vector_c<Foo> v;
//      while (SomeCondition())
//      {
//          OS_Status_t status = v.PushBack(SomeFoo());
//          if (status != OS_NO_ERROR)
//          {
//              ...
//          }
//      }
//
//      // Iterate over vector , reading and writing its content.
//      for (size_t size = v.Size(), size_t i = 0; i < size; ++i)
//      {
//          Process(v[i]);         // or Process(v.At(i));
//          v[i] = Produce();      // or v.At(i) = Produce();
//      }
//
//      // Preallocate to avoid repeated reallocations and copies.
//      Vector_c<Foo> v;
//      OS_Status_t status = v.Reserve(MAX_ITEMS);
//      if (status != OS_NO_ERROR)
//      {
//          ...
//      }
//
//      // Pass vector to function expecting C array.
//      // WARNING: The vector content may be invalidated when calling functions
//      // potentially re-allocating the vector (PushBack(), Reserve()...).
//      extern void Process(const Foo array[], size_t size);
//      Vector_c<Foo> v;
//      ...
//      Process(&v[0], v.Size());
//
// For more details, see for example
// http://en.cppreference.com/w/cpp/container/vector.
//
// This class is not thread-safe.  Clients must provide their own locking if
// needed.
template <typename T>
class Vector_c
{
public:
    Vector_c();
    ~Vector_c();
    void Swap(Vector_c<T> &other);
    OS_Status_t CopyFrom(const Vector_c<T> &other);

    size_t Size() const         { return mSize; }
    size_t Capacity() const     { return mCapacity; }
    bool Empty() const          { return mSize == 0; }

    const T &At(size_t i) const;
    T &At(size_t i);
    const T &operator[](size_t i) const;
    T &operator[](size_t i);
    const T &Front() const      { return operator[](0); }
    T &Front()                  { return operator[](0); }
    const T &Back() const       { return operator[](mSize - 1); }
    T &Back()                   { return operator[](mSize - 1); }

    OS_Status_t PushBack(const T &item);
    void PopBack();

    OS_Status_t Reserve(size_t newCapacity);
    OS_Status_t ReserveAtLeast(size_t desiredCapacity);
    OS_Status_t ReserveNoMoreThan(size_t requiredCapacity);
    OS_Status_t Resize(size_t newSize);

private:
    // Small enough for not wasting memory and big enough for minimizing
    // reallocations for simple uses.
    static const size_t INITIAL_CAPACITY = 16;

    // Popular implementations use this multiplicative factor on reallocation.
    static const int GROW_FACTOR = 2;

    // Dynamic array containing constructed objects in [0, mSize) range and raw
    // memory in [mSize, mCapacity) range.
    T *mItems;

    // Actual number of objects in mItems.
    size_t mSize;

    // Number of objects mItems can store without reallocation.
    size_t mCapacity;

    OS_Status_t ReallocateAndCopy(const Vector_c<T> &other, size_t capacity);

    static T *AllocateRawArray(size_t size);
    static void DeallocateRawArray(T *p);
    static void DefaultConstructItem(T *p);
    static void CopyConstructItem(T *p, const T &from);
    static void DestructItem(T *p);

    // See CopyFrom().
    DISALLOW_COPY_AND_ASSIGN(Vector_c);
};

template <typename T>
Vector_c<T>::Vector_c()
    : mItems(NULL)
    , mSize(0)
    , mCapacity(0)
{
}

template <typename T>
Vector_c<T>::~Vector_c()
{
    // Check invariant.
    SE_ASSERT(mSize <= mCapacity);
    SE_ASSERT((mItems == NULL && mSize == 0) || (mItems != NULL && mCapacity > 0));

    for (size_t i = 0; i < mSize; ++i)
    {
        DestructItem(&mItems[i]);
    }
    DeallocateRawArray(mItems);
}

// Duplicate content of other vector into this vector.  The existing items in
// this vector, if any, are destructed.  Return error on out-of-memory.
//
// This method replaces the copy constructor and assignment operator as they can
// not report an error.
template <typename T>
OS_Status_t Vector_c<T>::CopyFrom(const Vector_c<T> &other)
{
    return ReallocateAndCopy(other, other.mSize);
}

// Interchange content of this vector and other vector without copying the
// items.
template <typename T>
void Vector_c<T>::Swap(Vector_c<T> &other)
{
    ::Swap(mSize, other.mSize);
    ::Swap(mCapacity, other.mCapacity);
    ::Swap(mItems, other.mItems);
}

template <typename T>
const T &Vector_c<T>::At(size_t i) const
{
    SE_ASSERT(i < mSize);
    return mItems[i];
}

template <typename T>
T &Vector_c<T>::At(size_t i)
{
    SE_ASSERT(i < mSize);
    return mItems[i];
}

template <typename T>
const T &Vector_c<T>::operator[](size_t i) const
{
    return At(i);
}

template <typename T>
T &Vector_c<T>::operator[](size_t i)
{
    return At(i);
}

// Grow this vector to at least desired capacity.  Do nothing if already big
// enough.  The grow pattern used by this function is an implementation detail;
// use non-standard ReserveNoMoreThan() and ReserveAtLeast() to control grow
// pattern explicitly.  Return an error on out-of-memory.
template <typename T>
OS_Status_t Vector_c<T>::Reserve(size_t desiredCapacity)
{
    return ReserveNoMoreThan(desiredCapacity);
}

// Grow this vector to required capacity and no more.  Do nothing if already big
// enough.  Use this function for controlling precisely the capacity, for
// example for minimizing memory footprint.  Return an error on out-of-memory.
template <typename T>
OS_Status_t Vector_c<T>::ReserveNoMoreThan(size_t newCapacity)
{
    return (newCapacity > mCapacity) ? ReallocateAndCopy(*this, newCapacity) : OS_NO_ERROR;
}

// Grow this vector to desired capacity or more using same grow pattern as
// PushBack().  Do nothing if already big enough.  Use this function, for
// example, to keep amortized O(1) insertion time when pre-allocating
// repeatedly. Return an error on out-of-memory.
template <typename T>
OS_Status_t Vector_c<T>::ReserveAtLeast(size_t desiredCapacity)
{
    if (desiredCapacity <= mCapacity)
    {
        return OS_NO_ERROR;
    }

    size_t actualCapacity = (mCapacity == 0) ? INITIAL_CAPACITY : mCapacity;
    while (actualCapacity < desiredCapacity)
    {
        actualCapacity *= GROW_FACTOR;
    }

    return ReallocateAndCopy(*this, actualCapacity);
}

// Change this vector size to new size.
// If new size greater than current size, push back additional
// default-initialized items.  If new size less than current size, pop back
// excess items.  Return an error and leave vector unchanged on out-of-memory.
template <typename T>
OS_Status_t Vector_c<T>::Resize(size_t newSize)
{
    if (newSize < mSize)
    {
        while (newSize < mSize)
        {
            DestructItem(&mItems[mSize - 1]);
            --mSize;
        }
    }
    else if (newSize > mSize)
    {
        OS_Status_t status = ReserveAtLeast(newSize);
        if (status != OS_NO_ERROR)
        {
            return status;
        }

        while (newSize > mSize)
        {
            DefaultConstructItem(&mItems[mSize]);
            ++mSize;
        }
    }

    return OS_NO_ERROR;
}

// Append a new item at the end of this vector growing the vector if required.
// The provided item is copy-constructed into the vector.
template <typename T>
OS_Status_t Vector_c<T>::PushBack(const T &item)
{
    if (mItems == NULL || mSize == mCapacity)
    {
        size_t newCapacity = (mItems == NULL) ? INITIAL_CAPACITY : mCapacity * GROW_FACTOR;
        OS_Status_t status = ReallocateAndCopy(*this, newCapacity);
        if (status != OS_NO_ERROR)
        {
            return status;
        }
    }

    CopyConstructItem(&mItems[mSize++], item);
    return OS_NO_ERROR;
}

// Remove the item at the end of this vector.  The removed item is destructed.
template <typename T>
void Vector_c<T>::PopBack()
{
    SE_ASSERT(mSize > 0);
    mSize--;
    DestructItem(&mItems[mSize]);
}

// Helper for growing vector and copying items from other vector.  Return an
// error on out-of-memory.
template <typename T>
OS_Status_t Vector_c<T>::ReallocateAndCopy(const Vector_c<T> &other, size_t capacity)
{
    SE_ASSERT(capacity >= other.mSize);

    // Do all changes on the side for easy rollback on error.
    Vector_c<T> tmp;

    if (capacity > 0)
    {
        tmp.mItems = AllocateRawArray(capacity);
        if (tmp.mItems == NULL)
        {
            return OS_ERROR;
        }
        tmp.mCapacity = capacity;
    }

    for (size_t i = 0; i < other.mSize; ++i)
    {
        CopyConstructItem(&tmp.mItems[i], other.mItems[i]);
    }
    tmp.mSize = other.mSize;

    // Everything went fine.  Use the swap trick for committing new vector and
    // destructing the old one on block exit.
    Swap(tmp);

    return OS_NO_ERROR;
}

template <typename T>
T *Vector_c<T>::AllocateRawArray(size_t size)
{
    // Use 'operator new' rather than 'new' to allocate raw memory rather than
    // constructing objects.
    // Assume OS-level allocator always return memory aligned to the most
    // constrained type.
    return static_cast<T *>(operator new(size * sizeof(T)));
}

template <typename T>
void Vector_c<T>::DeallocateRawArray(T *p)
{
    // Use 'operator delete' rather than 'delete' to free raw memory rather than
    // deleting objects.
    operator delete(p);
}

// Default-construct object in already allocated memory.
template <typename T>
void Vector_c<T>::DefaultConstructItem(T *p)
{
    new(p) T;
}

// Copy-construct object in already allocated memory.
template <typename T>
void Vector_c<T>::CopyConstructItem(T *p, const T &from)
{
    new(p) T(from);
}

// Destruct object without freeing underlying memory.
template <typename T>
void Vector_c<T>::DestructItem(T *item)
{
    item->~T();
}

#endif
