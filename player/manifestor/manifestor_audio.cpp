/************************************************************************
Copyright (C) 2003-2014 STMicroelectronics. All Rights Reserved.

This file is part of the Streaming Engine.

Streaming Engine is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License version 2 as
published by the Free Software Foundation.

Streaming Engine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with Streaming Engine; see the file COPYING.  If not, write to the Free
Software Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307,
USA.

The Streaming Engine Library may alternatively be licensed under a
proprietary license from ST.
************************************************************************/

#include "manifestor_audio.h"
#include "player_generic.h"
#include "player_playback.h"
#include "player_stream.h"
#include "auto_lock_mutex.h"
#include "st_relayfs_se.h"

#undef TRACE_TAG
#define TRACE_TAG   "Manifestor_Audio_c"

//////////////////////////////////////////////////////////////////////////////////////////
//
// Manifestor Audio internal buffer life cycle:
// --------------------------------------------
//
//
//                              ReleaseBuf()
//   --------------------<--------------------<----------------<----------------------
//   |                              (3)                                               |
//   |                                                                                |
//   |  -------------                      ------------                 ------------  |
//   |->|           | QueueDecodeBuffer()  |          | DequeueBuffer() |          |->|
//      | EmptyBuf  | -------------------> | QueuedBuf| ------------->  |Processing|
//      |   Ring    |        (1)           |   Ring   |      (2)        |   Ring   |
//      |           |                      |          |                 |          |
//      -------------                      ------------                 ------------
//
//
//
// This diagram describes the steps of Manifestor Audio internal buffer lifecycle.
// Manifestor_Audio_c uses an internal buffer structure (AudioStreamBuffer_t) to attach
// various information required to each buffer.
//
// (0) On Manifestor_Audio_c::Connect the 3 rings are allocated with same number of slots.
//     EmptyStreamBufRing is filled with AudioStreamBuffer_t allocated structures.
//
// (1) When QueueDecodeBuffer is called:
//     - we extract a empty buffer from EmptyStreamBufRing
//     - we store DecodedBuffer to queue into extracted empty buffer
//     - we insert this buffer into QueuedBufRing
//
// (2) When DequeueBuffer/DequeueBufferUnderLock is called:
//     - we extract buffer from QueuedBufRing
//     - we insert buffer into ProcessingBufRing to keep track of buffer that are processed
//
// (3) When ReleaseBuf is called i.e. processing is finished
//     - we extract the buffer from ProcessingBufRing
//     - we release the DecodedBuffer (insert it into OutputPort owned by Manifestor coordinator)
//     - we insert the buffer into EmptyStreamBufRing.
//
//////////////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////
///
/// constructor
///
Manifestor_Audio_c::Manifestor_Audio_c()
    : DisplayUpdatePending(false)
    , SurfaceDescriptor()
    , ProcessingBufRing(NULL)
    , PtsToDisplay(INVALID_TIME)
    , ForcedUnblock(false)
    , NumberOfEnqueuedBuffers(0)
    , NumberOfReleasedBuffers(0)
    , QueuedBufRing(NULL)
    , EmptyStreamBufRing(NULL)
    , EmptyStreamBufRingLock()
    , BufferQueueLock()
    , BufferQueueUpdated()
    , RelayfsIndex(0)
{
    if (InitializationStatus != ManifestorNoError)
    {
        SE_ERROR("Initialization status not valid - aborting init\n");
        return;
    }
    OS_InitializeMutex(&EmptyStreamBufRingLock);
    OS_InitializeMutex(&BufferQueueLock);
    OS_InitializeEvent(&BufferQueueUpdated);

    Configuration.ManifestorName = "Audio";
    Configuration.StreamType     = StreamTypeAudio;

    SurfaceDescriptor.StreamType = StreamTypeAudio;
    SurfaceDescriptor.ClockPullingAvailable = true;
    SurfaceDescriptor.MaxClockAdjustment = 1000;    // +/-1000 ppm
    SurfaceDescriptor.MaxClockAdjustmentStep = 5;  // +- 5 ppm in one step
    SurfaceDescriptor.PercussiveCapable = true;
    SurfaceDescriptor.MasterCapable = true;
    SurfaceDescriptor.BitsPerSample = 32;
    SurfaceDescriptor.ChannelCount = 8;
    SurfaceDescriptor.SampleRateHz = 0;
    SurfaceDescriptor.FrameRate = 1; // rational

    RelayfsIndex = st_relayfs_getindex_fortype_se(ST_RELAY_TYPE_DECODED_AUDIO_BUFFER);
}


////////////////////////////////////////////////////////////////////////////
///
///
///
Manifestor_Audio_c::~Manifestor_Audio_c()
{
    Manifestor_Audio_c::Halt();
    st_relayfs_freeindex_fortype_se(ST_RELAY_TYPE_DECODED_AUDIO_BUFFER, RelayfsIndex);

    OS_TerminateMutex(&EmptyStreamBufRingLock);
    OS_TerminateMutex(&BufferQueueLock);
    OS_TerminateEvent(&BufferQueueUpdated);
}

////////////////////////////////////////////////////////////////////////////
///
///
///
ManifestorStatus_t      Manifestor_Audio_c::Halt()
{
    SE_DEBUG(GetGroupTrace(), "\n");
    //
    // It is unlikely that we have any decode buffers queued at this point but if we do we
    // deperately need to jettison them (otherwise the reference these buffers hold to the
    // coded data can never be undone).
    //
    FreeAllBufRings();
    return Manifestor_Base_c::Halt();
}

////////////////////////////////////////////////////////////////////////////
///
///
///
void Manifestor_Audio_c::FreeQueuedBufRing()
{
    SE_DEBUG(GetGroupTrace(), "\n");

    if (QueuedBufRing != NULL)
    {
        ReleaseQueuedDecodeBuffers(false); // no flush with copy
        delete QueuedBufRing;
        QueuedBufRing = NULL;
    }
}

////////////////////////////////////////////////////////////////////////////
///
///
///
void Manifestor_Audio_c::FreeProcessingBufRing()
{
    SE_DEBUG(GetGroupTrace(), "\n");

    if (ProcessingBufRing != NULL)
    {
        ReleaseProcessingBuffers();
        delete ProcessingBufRing;
        ProcessingBufRing = NULL;
    }
}

////////////////////////////////////////////////////////////////////////////
///
///
///
void Manifestor_Audio_c::FreeEmptyStreamBufRing()
{
    SE_DEBUG(GetGroupTrace(), "\n");

    if (EmptyStreamBufRing != NULL)
    {
        ReleaseEmptyStreamBuffers();
        delete EmptyStreamBufRing;
        EmptyStreamBufRing = NULL;
    }
}

void Manifestor_Audio_c::FreeAllBufRings()
{
    SE_DEBUG(GetGroupTrace(), "\n");

    FreeQueuedBufRing();
    FreeProcessingBufRing();
    FreeEmptyStreamBufRing();
}

unsigned int Manifestor_Audio_c::GetTotalBuffersInQueue()
{
    AutoLockMutex AutoLock(&BufferQueueLock);
    if (QueuedBufRing == NULL)
    {
        SE_DEBUG(GetGroupTrace(), "QueuedBufRing is NULL\n");
        return 0;
    }
    return QueuedBufRing->NbOfEntries();
}

////////////////////////////////////////////////////////////////////////////
///
///
///
void Manifestor_Audio_c::ReleaseBuf(AudioStreamBuffer_t *StreamBufPtr)
{
    if (mOutputPort == NULL)
    {
        SE_ERROR("mOutputPort is NULL; cannot release StreamBufPtr:%p\n", StreamBufPtr);
        return;
    }

    mOutputPort->Insert((uintptr_t) StreamBufPtr->Buffer);
    OS_LockMutex(&EmptyStreamBufRingLock);
    EmptyStreamBufRing->Insert((uintptr_t) StreamBufPtr);
    NumberOfReleasedBuffers++;

    SE_DEBUG(GetGroupTrace(), "StreamBufPtr:%p NumberOfEnqueuedBuffers=%06d, NumberOfReleasedBuffers=%06d delta:%d\n",
             StreamBufPtr,
             NumberOfEnqueuedBuffers, NumberOfReleasedBuffers,
             NumberOfEnqueuedBuffers - NumberOfReleasedBuffers);
    OS_UnLockMutex(&EmptyStreamBufRingLock);

    return;
}

////////////////////////////////////////////////////////////////////////////
///
///
///
void Manifestor_Audio_c::ExtractAndReleaseProcessingBuf()
{
    SE_VERBOSE(GetGroupTrace(), ">\n");
    AudioStreamBuffer_t *StreamBufPtr;

    if (ProcessingBufRing == NULL)
    {
        SE_ERROR("ProcessingBufRing is NULL; cannot extract Buffer\n");
        return;
    }

    if (ProcessingBufRing->Extract((uintptr_t *) &StreamBufPtr, RING_NONE_BLOCKING) != RingNoError)
    {
        SE_ERROR("Cannot extract Buffer from ProcessingBufRing\n");
        return;
    }
    SE_VERBOSE(GetGroupTrace(), "< Extracted from ProcessingBufRing[%02d]\n", StreamBufPtr->BufferIndex);
    return ReleaseBuf(StreamBufPtr);
}

////////////////////////////////////////////////////////////////////////////
///
/// Connect the output port, and initialize our decode buffer associated context
///
/// \param Port         Port to connect to.
/// \return             Succes or failure
///
ManifestorStatus_t      Manifestor_Audio_c::Connect(Port_c *Port)
{
    ManifestorStatus_t          Status;
    BufferPool_t                Pool;
    unsigned int                MaxBufferCount;

    SE_DEBUG(GetGroupTrace(), "\n");
    // Get the number of buffers in the decode buffer pool.
    Pool = Stream->GetDecodeBufferManager()->GetDecodeBufferPool();

    if (Pool == NULL)
    {
        return ManifestorError;
    }

    Pool->GetPoolUsage(&MaxBufferCount);

    FreeAllBufRings();

    SE_DEBUG(GetGroupTrace(), "new QueuedBufRing ProcessingBufRing (BufferCount:%d)\n", MaxBufferCount);
    QueuedBufRing = RingGeneric_c::New(MaxBufferCount, "Manifestor_Audio_c::QueuedBufRing");
    if (QueuedBufRing == NULL)
    {
        SE_ERROR("Failed to allocate QueuedBufRing\n");
        FreeAllBufRings();
        return ManifestorError;
    }

    ProcessingBufRing = RingGeneric_c::New(MaxBufferCount, "Manifestor_Audio_c::ProcessingBufRing");
    if (ProcessingBufRing == NULL)
    {
        SE_ERROR("Failed to allocate ProcessingBufRing\n");
        FreeAllBufRings();
        return ManifestorError;
    }

    Status = AllocateEmptyStreamBufRing(MaxBufferCount);
    if (Status != ManifestorNoError)
    {
        SE_ERROR("Cannot allocate EmptyStreamBufRing\n");
        FreeAllBufRings();
        return ManifestorError;
    }

    // Connect to the port
    Status = Manifestor_Base_c::Connect(Port);

    if (Status != ManifestorNoError)
    {
        FreeAllBufRings();
        return Status;
    }

    // Let outside world know we are up and running
    SetComponentState(ComponentRunning);
    return ManifestorNoError;
}


////////////////////////////////////////////////////////////////////////////
/// Fill in private structure with timing details of display surface.
///
/// \param      SurfaceParameters pointer to structure to complete
///
ManifestorStatus_t      Manifestor_Audio_c::GetSurfaceParameters(OutputSurfaceDescriptor_t   **SurfaceParameters, unsigned int *NumSurfaces)
{
    *SurfaceParameters  = &SurfaceDescriptor;
    *NumSurfaces        = 1;
    return ManifestorNoError;
}

////////////////////////////////////////////////////////////////////////////
///
/// AllocateEmptyStreamBufRing
///
/// Private method to create EmptyStreamBufRing RingGeneric_c object
/// Allocate BufCount AudioStreamBuffer_t objects and insert their pointers
/// into EmptyStreamBufRing.
///
/// Callers are responsible for cleaning up on error by calling FreeAllBufRings().
///
/// \param      BufCount Size of the Ring, Number of AudioStreamBuffer_t to allocate
///
ManifestorStatus_t Manifestor_Audio_c::AllocateEmptyStreamBufRing(int BufCount)
{
    OS_LockMutex(&EmptyStreamBufRingLock);

    EmptyStreamBufRing = RingGeneric_c::New(BufCount, "Manifestor_Audio_c::EmptyStreamBufRing");
    if (EmptyStreamBufRing == NULL)
    {
        SE_ERROR("Cannot allocate empty stream buffer ring\n");
        OS_UnLockMutex(&EmptyStreamBufRingLock);
        return ManifestorError;
    }

    /* Allocate required StreamBuffers and insert them into EmptyStreamBufRing */
    AudioStreamBuffer_t *StreamBufPtr;
    for (int i = 0; i < BufCount; i++)
    {
        StreamBufPtr = (AudioStreamBuffer_t *) OS_Malloc(sizeof(AudioStreamBuffer_t));
        if (!StreamBufPtr)
        {
            SE_ERROR("Cannot allocate AudioStreamBuffer_t[%d]\n", i);
            OS_UnLockMutex(&EmptyStreamBufRingLock);
            // Cleanup done in caller.
            return ManifestorError;
        }
        EmptyStreamBufRing->Insert((uintptr_t) StreamBufPtr);
    }
    OS_UnLockMutex(&EmptyStreamBufRingLock);
    return ManifestorNoError;
}

////////////////////////////////////////////////////////////////////////////
///
/// Release any buffers queued within the StreamBuffer structure.
///
/// Pass any buffer held on the Manifestor_Audio_c::StreamBuffer structure
/// onto the output port.
///
/// The super-class will tidy up an queued events.
///
ManifestorStatus_t      Manifestor_Audio_c::ReleaseQueuedDecodeBuffers(bool ReleaseAllBuffers)
{
    SE_DEBUG(GetGroupTrace(), "\n");

    OS_LockMutex(&BufferQueueLock);
    AudioStreamBuffer_t *StreamBufPtr;

    //
    // Emit all the queued buffers
    //

    if (QueuedBufRing == NULL)
    {
        SE_ERROR("QueuedBufRing is NULL; Cannot extract Buffer\n");
        OS_UnLockMutex(&BufferQueueLock);
        return ManifestorError;
    }
    while (QueuedBufRing->Extract((uintptr_t *) &StreamBufPtr, RING_NONE_BLOCKING) == RingNoError)
    {
        ReleaseBuf(StreamBufPtr);
    }

    OS_UnLockMutex(&BufferQueueLock);

    WaitForProcessingBufRingRelease();

    return Manifestor_Base_c::ReleaseQueuedDecodeBuffers(ReleaseAllBuffers);
}



////////////////////////////////////////////////////////////////////////////
///
/// Release any non queued buffers within the StreamBuffer structure and
/// pass the buffer onto the output port.
///
/// CAUTION: this function has been designed to be called in case of
/// non recoverable error in the playback thread.
///
void Manifestor_Audio_c::ReleaseProcessingBuffers()
{
    SE_DEBUG(GetGroupTrace(), ">\n");
    OS_LockMutex(&BufferQueueLock);
    AudioStreamBuffer_t *StreamBufPtr;

    if (ProcessingBufRing == NULL)
    {
        SE_ERROR("ProcessingBufRing is NULL; Cannot extract Buffer\n");
        OS_UnLockMutex(&BufferQueueLock);
        return;
    }
    while (ProcessingBufRing->Extract((uintptr_t *) &StreamBufPtr, RING_NONE_BLOCKING) == RingNoError)
    {
        SE_DEBUG(GetGroupTrace(), "Released ProcessingBuf:%p\n", StreamBufPtr);
        ReleaseBuf(StreamBufPtr);
    }
    OS_UnLockMutex(&BufferQueueLock);
    SE_DEBUG(GetGroupTrace(), "<\n");
}

////////////////////////////////////////////////////////////////////////////
///
/// Free all buffers available in EmptyStreamBufRing
///
void Manifestor_Audio_c::ReleaseEmptyStreamBuffers()
{
    SE_DEBUG(GetGroupTrace(), ">\n");
    OS_LockMutex(&EmptyStreamBufRingLock);
    AudioStreamBuffer_t *StreamBufPtr;

    if (EmptyStreamBufRing == NULL)
    {
        SE_WARNING("EmptyStreamBufRing is NULL; Cannot extract Buffer\n");
        OS_UnLockMutex(&EmptyStreamBufRingLock);
        return;
    }
    while (EmptyStreamBufRing->Extract((uintptr_t *) &StreamBufPtr, RING_NONE_BLOCKING) == RingNoError)
    {
        SE_DEBUG(GetGroupTrace(), "Free EmptyStreamBuf\n");
        OS_Free(StreamBufPtr);
    }
    OS_UnLockMutex(&EmptyStreamBufRingLock);
    SE_DEBUG(GetGroupTrace(), "<\n");
}

////////////////////////////////////////////////////////////////////////////
///
///
///
ManifestorStatus_t      Manifestor_Audio_c::QueueDecodeBuffer(class Buffer_c *Buffer,
                                                              ManifestationOutputTiming_t **TimingPointerArray,
                                                              unsigned int *NumTimes)
{
    ManifestorStatus_t                  Status;
    RingStatus_t                        RingStatus;
    unsigned int                        BufferIndex;
    ParsedFrameParameters_t            *ParsedFrameParameters;
    AudioStreamBuffer_t                *StreamBufPtr;
    SE_VERBOSE(GetGroupTrace(), "\n");
    AssertComponentState(ComponentRunning);

    //
    // Obtain the index for the buffer and populate the parameter data.
    //
    Buffer->GetIndex(&BufferIndex);
    if (BufferIndex >= MAX_DECODE_BUFFERS)
    {
        SE_ERROR("invalid buffer index %d\n", BufferIndex);
        return ManifestorError;
    }

    OS_LockMutex(&EmptyStreamBufRingLock);
    if (EmptyStreamBufRing->Extract((uintptr_t *) &StreamBufPtr) != RingNoError)
    {
        SE_ERROR("Cannot get a empty StreamBufPtr to fill with DecodeBuffer\n");
        OS_UnLockMutex(&EmptyStreamBufRingLock);
        return ManifestorWouldBlock;
    }
    NumberOfEnqueuedBuffers++;

    SE_DEBUG(GetGroupTrace(), "StreamBufPtr:%p NumberOfEnqueuedBuffers=%06d, NumberOfReleasedBuffers=%06d delta:%d\n",
             StreamBufPtr,
             NumberOfEnqueuedBuffers, NumberOfReleasedBuffers,
             NumberOfEnqueuedBuffers - NumberOfReleasedBuffers);
    OS_UnLockMutex(&EmptyStreamBufRingLock);

    StreamBufPtr->Buffer = Buffer;
    StreamBufPtr->BufferIndex = BufferIndex;

    StreamBufPtr->BufferState = AudioBufferStateQueued;
    Buffer->ObtainMetaDataReference(Player->MetaDataParsedFrameParametersReferenceType,
                                    (void **) &StreamBufPtr->FrameParameters);
    SE_ASSERT(StreamBufPtr->FrameParameters != NULL);


    Buffer->ObtainMetaDataReference(Player->MetaDataParsedAudioParametersType,
                                    (void **) &StreamBufPtr->AudioParameters);
    SE_ASSERT(StreamBufPtr->FrameParameters != NULL);

    Buffer->ObtainMetaDataReference(Player->MetaDataParsedFrameParametersReferenceType,
                                    (void **)(&ParsedFrameParameters));
    SE_ASSERT(ParsedFrameParameters != NULL);

    if (*NumTimes > 1)
    {
        SE_ERROR("Implementation error - can't handle more than one OutputTiming\n");
    }

    StreamBufPtr->AudioOutputTiming = *TimingPointerArray;
    StreamBufPtr->QueueAsCodedData  = true;

    //Copying ADMetaData to AudioParameter struct
    if ((!StreamBufPtr->AudioParameters->MixingMetadata.IsMixingMetadataPresent) && (ParsedFrameParameters->ADMetaData.ADInfoAvailable))
    {
        memcpy(&(StreamBufPtr->AudioParameters->MixingMetadata.ADPESMetaData),
               &(ParsedFrameParameters->ADMetaData),
               sizeof(ADMetaData_t));
        StreamBufPtr->AudioParameters->MixingMetadata.IsMixingMetadataPresent = true;
    }

    //
    // Dump the entire buffer via relay (if enabled)
    //
    {
        unsigned char *Data = (unsigned char *)Stream->GetDecodeBufferManager()->ComponentBaseAddress(StreamBufPtr->Buffer,
                                                                                                      PrimaryManifestationComponent,
                                                                                                      CachedAddress);
        unsigned int  Len = StreamBufPtr->AudioParameters->SampleCount *
                            StreamBufPtr->AudioParameters->Source.ChannelCount *
                            (StreamBufPtr->AudioParameters->Source.BitsPerSample / 8);
        st_relayfs_write_se(ST_RELAY_TYPE_DECODED_AUDIO_BUFFER + RelayfsIndex, ST_RELAY_SOURCE_SE, Data, Len, false);
    }
    //
    // Allow the sub-class to have a peek at the buffer before we queue it for display
    //
    Status = UpdateForQueueBuffer(StreamBufPtr);
    if (Status != ManifestorNoError)
    {
        if (Status != ManifestorRenderComplete) // transcoding error status is not an error !
        {
            SE_ERROR("Unable to queue buffer %x\n", Status);
        }

        goto freeOnError;
    }

    //
    // Enqueue the buffer for display within the playback thread
    //
    OS_LockMutex(&BufferQueueLock);

    RingStatus = QueuedBufRing->Insert((uintptr_t) StreamBufPtr);
    if (RingStatus != RingNoError)
    {
        SE_ERROR("Failed to insert in QueuedBufRing (RingStatus:%d)\n", RingStatus);
        OS_UnLockMutex(&BufferQueueLock);
        Status = ManifestorError;
        goto freeOnError;
    }

    OS_UnLockMutex(&BufferQueueLock);
    OS_SetEvent(&BufferQueueUpdated);
    SE_VERBOSE(GetGroupTrace(), "Inserted in QueuedBufRing\n");
    return ManifestorNoError;

freeOnError:
    OS_LockMutex(&EmptyStreamBufRingLock);
    EmptyStreamBufRing->Insert((uintptr_t) StreamBufPtr);
    NumberOfReleasedBuffers++;
    SE_DEBUG(GetGroupTrace(), "StreamBufPtr:%p NumberOfEnqueuedBuffers=%06d, NumberOfReleasedBuffers=%06d delta:%d\n",
             StreamBufPtr,
             NumberOfEnqueuedBuffers, NumberOfReleasedBuffers,
             NumberOfEnqueuedBuffers - NumberOfReleasedBuffers);
    OS_UnLockMutex(&EmptyStreamBufRingLock);
    return Status;
}


////////////////////////////////////////////////////////////////////////////
///
/// Get original PTS of currently visible buffer
///
/// \param Pts          Pointer to Pts variable
///
/// \return             Success if Pts value is available
///
ManifestorStatus_t Manifestor_Audio_c::GetNativeTimeOfCurrentlyManifestedFrame(unsigned long long *Time)
{
    struct ParsedFrameParameters_s     *FrameParameters;
    *Time      = INVALID_TIME;
    AudioStreamBuffer_t *StreamBufPtr;
    AutoLockMutex AutoLock(&BufferQueueLock);

    if (QueuedBufRing == NULL)
    {
        SE_DEBUG(GetGroupTrace(), "QueuedBufRing is NULL\n");
        return ManifestorError;
    }
    if (QueuedBufRing->NonEmpty() == false)
    {
        SE_DEBUG(GetGroupTrace(),  "No buffer on display\n");

        if (NotValidTime(PtsToDisplay))
        {
            SE_DEBUG(GetGroupTrace(),  "No previous buffer displayed too\n");
            return ManifestorError;
        }

        // We may be in pause and we had a previous buffer displayed
        // So this is the PTS of the frame to be displayed when we resume
        *Time = PtsToDisplay;
        SE_DEBUG(GetGroupTrace(), "PTS of frame to be displayed %llu\n", *Time);
        return ManifestorNoError;
    }

    if (QueuedBufRing->Peek((uintptr_t *) &StreamBufPtr) != RingNoError)
    {
        SE_ERROR("Unable to extract Buffer from QueuedBufRing\n");
        return ManifestorError;
    }

    StreamBufPtr->Buffer->ObtainMetaDataReference(
        Player->MetaDataParsedFrameParametersReferenceType, (void **)&FrameParameters);
    SE_ASSERT(FrameParameters != NULL);

    if (!FrameParameters->PTS.IsValid())
    {
        SE_ERROR("Buffer on display does not have a valid native playback time\n");
        return ManifestorError;
    }

    TimeStamp_c ComputedPts = TimeStamp_c::AddUsec(FrameParameters->PTS, FrameParameters->PtsOffset);
    *Time = ComputedPts.PtsValue();
    SE_DEBUG(GetGroupTrace(), "PlaybackTime %llu New playback time %llu offset %lld\n", FrameParameters->PTS.PtsValue(), *Time, FrameParameters->PtsOffset);
    return ManifestorNoError;
}

void Manifestor_Audio_c::LockBuffferQueue()
{
    SE_EXTRAVERB(GetGroupTrace(), ">\n");
    OS_LockMutex(&BufferQueueLock);
    SE_EXTRAVERB(GetGroupTrace(), "<\n");
}

void Manifestor_Audio_c::UnLockBuffferQueue()
{
    SE_EXTRAVERB(GetGroupTrace(), ">\n");
    OS_UnLockMutex(&BufferQueueLock);
    SE_EXTRAVERB(GetGroupTrace(), "<\n");
}

////////////////////////////////////////////////////////////////////////////
///
/// Dequeue a buffer from the queue of buffers ready for presentation and
/// release it straightaway because it presentation time is in the past
///
/// \return Manifestor status code, ManifestorNoError indicates success.
///
ManifestorStatus_t Manifestor_Audio_c::DropNextQueuedBufferUnderLock()
{
    RingStatus_t ringStatus;
    SE_DEBUG(GetGroupTrace(), "\n");
    //
    // Dequeue the buffer
    //
    SE_ASSERT(OS_MutexIsLocked(&BufferQueueLock));

    if (QueuedBufRing == NULL)
    {
        SE_ERROR("QueuedBufRing is NULL; Cannot Deuqueue Buffer\n");
        return ManifestorError;
    }

    AudioStreamBuffer_t *StreamBufPtr;
    if ((ringStatus = QueuedBufRing->Extract((uintptr_t *) &StreamBufPtr, RING_NONE_BLOCKING)) != RingNoError)
    {
        SE_INFO(GetGroupTrace(), "Failed to extract Buffer from QueuedBufRing (RingStatus:%d)\n", ringStatus);
        return ManifestorWouldBlock;
    }
    else
    {
        ReleaseBuf(StreamBufPtr);
    }

    return ManifestorNoError;
}


////////////////////////////////////////////////////////////////////////////
///
/// Dequeue a buffer from the queue of buffers ready for presentation.
///
/// Must be called only from the mixer playback thread with Manifestor Locked (see Lock/UnLockBuffferQueue)
///
/// \param Pointer to AudioStreamBuffer_t pointer that will be filled with extracted buffer pointer.
/// \return Manifestor status code, ManifestorNoError indicates success.
///
ManifestorStatus_t Manifestor_Audio_c::DequeueBufferUnderLock(AudioStreamBuffer_t **StreamBufPtr)
{
    RingStatus_t ringStatus;
    //
    // Dequeue the buffer
    //
    SE_ASSERT(OS_MutexIsLocked(&BufferQueueLock));

    if (QueuedBufRing == NULL || ProcessingBufRing == NULL)
    {
        SE_ERROR("QueuedBufRing(%p) or ProcessingBufRing(%p) NULL; Cannot Deuqueue Buffer\n",
                 QueuedBufRing, ProcessingBufRing);
        return ManifestorError;
    }
    if ((ringStatus = QueuedBufRing->Extract((uintptr_t *) StreamBufPtr, RING_NONE_BLOCKING)) != RingNoError)
    {
        SE_INFO(GetGroupTrace(), "Failed to extract Buffer from QueuedBufRing (RingStatus:%d)\n", ringStatus);
        return ManifestorWouldBlock;
    }

    if ((ringStatus = ProcessingBufRing->Insert((uintptr_t) * StreamBufPtr)) != RingNoError)
    {
        SE_ERROR("Failed to insert Buffer into ProcessingBufRing (RingStatus:%d)\n", ringStatus);
        return ManifestorError;
    }

    SE_DEBUG(GetGroupTrace(), "StreamBufPtr:%p\n", StreamBufPtr);

    return ManifestorNoError;
}

////////////////////////////////////////////////////////////////////////////
///
/// Peek a buffer from QueuedBufRing (buffers ready for presentation).
///
/// Must be called only from the mixer playback thread with Manifestor Locked (see Lock/UnLockBuffferQueue)
///
/// \param Pointer to AudioStreamBuffer_t pointer. Valid as long as Manifestor is locked.
/// \return ManifestorStatus_t ManifestorNoError on Sucess, ManifestorWouldBlock otherwise
///
ManifestorStatus_t Manifestor_Audio_c::PeekBufferUnderLock(AudioStreamBuffer_t **StreamBufPtr)
{
    SE_ASSERT(OS_MutexIsLocked(&BufferQueueLock));
    RingStatus_t RingStatus = QueuedBufRing->Peek((uintptr_t *) StreamBufPtr);

    return (RingStatus == RingNoError) ? ManifestorNoError : ManifestorWouldBlock;
}

////////////////////////////////////////////////////////////////////////////
///
/// Allow to manage all Marker frames pushed by the coordinator
///
///
ManifestorStatus_t  Manifestor_Audio_c::HandleMarkerFrame(Buffer_t MarkerFrameBuffer)
{
    PlayerSequenceNumber_t *SequenceNumberStructure = GetSequenceNumberStructure(MarkerFrameBuffer);
    MarkerFrame_t           MarkerFrame = SequenceNumberStructure->mMarkerFrame;

    SE_DEBUG(GetGroupTrace(), "Stream 0x%p this 0x%p received Marker (type %d) #%lld\n", Stream, this, MarkerFrame.mMarkerType, MarkerFrame.mSequenceNumber);

    mOutputPort->Insert((uintptr_t) MarkerFrameBuffer);

    return ManifestorNoError;
}

////////////////////////////////////////////////////////////////////////////
///
/// Function to be called once new frame have been manifested.
/// Generate FRAME_RENDERED event and update associated counter/statitics
///
///
void  Manifestor_Audio_c::NewFrameManifested()
{
    PlayerEventRecord_t FrameRenderedEvent;
    FrameRenderedEvent.Code           = EventNewFrameDisplayed;
    FrameRenderedEvent.Playback       = Playback;
    FrameRenderedEvent.Stream         = Stream;
    PlayerStatus_t StatusEvt = Stream->SignalEvent(&FrameRenderedEvent);
    if (StatusEvt != PlayerNoError)
    {
        SE_ERROR("Stream 0x%p Failed to send rendered event\n", Stream);
    }

    //Increment statistic counter for manifested frames as
    // frame (Decoded Buffer) corresponding to scatter page is consumed
    FrameCountManifested++;
    Stream->Statistics().FrameCountManifested++;
}

