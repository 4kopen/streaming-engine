/************************************************************************
Copyright (C) 2003-2014 STMicroelectronics. All Rights Reserved.

This file is part of the Streaming Engine.

Streaming Engine is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License version 2 as
published by the Free Software Foundation.

Streaming Engine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with Streaming Engine; see the file COPYING.  If not, write to the Free
Software Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307,
USA.

The Streaming Engine Library may alternatively be licensed under a
proprietary license from ST.
************************************************************************/
#include "player_stream.h"
#include "manifestor_base.h"

#undef TRACE_TAG
#define TRACE_TAG   "Manifestor_Base_c"

// audio CRC tuneable
unsigned int volatile Manifestor_c::EnableAudioCRC = 0;
// video SW CRC tuneable
unsigned int volatile Manifestor_c::EnableSoftwareCRC = 0;
// Certification Support tuneable
unsigned int volatile Manifestor_c::SamplesNeededForFadeOutAfterResampling = 128;

Manifestor_Base_c::Manifestor_Base_c()
    : Configuration()
    , mIsMaster(false)
    , mOutputPort(NULL)
    , FrameCountManifested(0)
{
    Configuration.ManifestorName                = "Noname";
    Configuration.StreamType                    = StreamTypeNone;
}

Manifestor_Base_c::~Manifestor_Base_c()
{
    Halt();
}

// /////////////////////////////////////////////////////////////////////////
//
//      Halt :
//      Action  : Terminate access to any registered resources
//      Input   :
//      Output  :
//      Result  :
//

ManifestorStatus_t      Manifestor_Base_c::Halt()
{
    return BaseComponentClass_c::Halt();
}

// /////////////////////////////////////////////////////////////////////////
//
//      Connect :
//      Action  : Save details of port on which to place manifested buffers
//      Input   : Pointer to port to use for finished decode frames
//      Output  :
//      Results :
//

ManifestorStatus_t      Manifestor_Base_c::Connect(Port_c *Port)
{
    SE_DEBUG(GetGroupTrace(), "\n");
    if (Port == NULL)
    {
        SE_ERROR("Incorrect parameter\n");
        return ManifestorError;
    }
    if (mOutputPort != NULL)
    {
        SE_WARNING("Port already connected\n");
    }

    mOutputPort  = Port;
    return ManifestorNoError;
}

// /////////////////////////////////////////////////////////////////////////
//
//      GetSurfaceParameters :
//      Action  : Fill in private structure with details about display surface
//      Input   : Opaque pointer to structure to complete
//      Output  : Filled in structure
//      Results :
//

ManifestorStatus_t      Manifestor_Base_c::GetSurfaceParameters(OutputSurfaceDescriptor_t   **SurfaceParameters, unsigned int *NumSurfaces)
{
    (void)SurfaceParameters; // warning removal
    *NumSurfaces = 0;
    return ManifestorNoError;
}

unsigned long long Manifestor_Base_c::GetManifestationLatency(void *ParsedAudioVideoDataParameters)
{
    (void)ParsedAudioVideoDataParameters; // warning removal
    return 0;
}

//{{{  ReleaseQueuedDecodeBuffers
//{{{  doxynote
/// \brief      Passes onto the output ring any decode buffers that are currently queued,
///             but not in the process of being manifested.
/// \param      ReleaseAllBuffers Indicates whether all buffers must be released
/// \return     Buffer index of last buffer sent for display
//}}}
ManifestorStatus_t      Manifestor_Base_c::ReleaseQueuedDecodeBuffers(bool ReleaseAllBuffers)
{
    SE_DEBUG(GetGroupTrace(), "\n");
    FlushDisplayQueue(ReleaseAllBuffers);

    return ManifestorNoError;
}
//}}}

ManifestorStatus_t  Manifestor_Base_c::GetCapabilities(unsigned int  *Capabilities)
{
    *Capabilities       = Configuration.Capabilities;
    return ManifestorNoError;
}

//}}}
//{{{  GetFrameCount
//{{{  doxynote
/// \brief Get number of frames appeared on the display
/// \param Framecount   Pointer to FrameCount variable
/// \return             Success
//}}}
ManifestorStatus_t Manifestor_Base_c::GetFrameCount(unsigned long long *FrameCount)
{
    *FrameCount         = this->FrameCountManifested;
    return ManifestorNoError;
}

ManifestorStatus_t Manifestor_Base_c::GetNumberOfTimings(unsigned int *NumTimes)
{
    *NumTimes         = 1;
    return ManifestorNoError;
}

ManifestorStatus_t Manifestor_Base_c::WaitForFrozenSurface()
{
    return ManifestorNoError;
}
