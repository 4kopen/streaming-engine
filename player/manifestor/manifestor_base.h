/************************************************************************
Copyright (C) 2003-2014 STMicroelectronics. All Rights Reserved.

This file is part of the Streaming Engine.

Streaming Engine is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License version 2 as
published by the Free Software Foundation.

Streaming Engine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with Streaming Engine; see the file COPYING.  If not, write to the Free
Software Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307,
USA.

The Streaming Engine Library may alternatively be licensed under a
proprietary license from ST.
************************************************************************/

#ifndef H_MANIFESTOR_BASE
#define H_MANIFESTOR_BASE

#include "player.h"
#include "allocinline.h"
#include "player_generic.h"
#include "player_stream.h"

#undef TRACE_TAG
#define TRACE_TAG   "Manifestor_Base_c"

//
// The internal configuration for the manifestor base
//

#define MANIFESTOR_CAPABILITY_GRAB              0x01
#define MANIFESTOR_CAPABILITY_DISPLAY           0x02
#define MANIFESTOR_CAPABILITY_CRC               0x04
#define MANIFESTOR_CAPABILITY_SOURCE            0x08
#define MANIFESTOR_CAPABILITY_ENCODE            0x10
#define MANIFESTOR_CAPABILITY_PUSH_RELEASE      0x20

typedef struct ManifestorConfiguration_s
{
    const char                   *ManifestorName;
    PlayerStreamType_t            StreamType;

    unsigned int                  Capabilities;
} ManifestorConfiguration_t;

struct EventRecord_s
{
    unsigned int                Id;
    struct PlayerEventRecord_s  Event;
    EventRecord_s() : Id(0), Event() {}
};

/// Framework for implementing manifestors.
class Manifestor_Base_c : public Manifestor_c
{
public:
    // Constructor/Destructor methods
    Manifestor_Base_c();
    ~Manifestor_Base_c();

    // Overrides for component base class functions
    ManifestorStatus_t   Halt();

    // Manifestor class functions
    ManifestorStatus_t   Connect(Port_c *Port);

    ManifestorStatus_t   GetSurfaceParameters(OutputSurfaceDescriptor_t   **SurfaceParameters, unsigned int *NumSurfaces);
    ManifestorStatus_t   GetNumberOfTimings(unsigned int           *NumTimes);
    unsigned long long   GetManifestationLatency(void *ParsedAudioVideoDataParameters);
    ManifestorStatus_t   GetFrameCount(unsigned long long    *FrameCount);

    ManifestorStatus_t   ReleaseQueuedDecodeBuffers(bool ReleaseAllBuffers);

    ManifestorStatus_t   HandleMarkerFrame(Buffer_t MarkerFrameBuffer)
    {
        (void)MarkerFrameBuffer; // warning removal
        return ManifestorNoError;
    }
    void                 ResetOnStreamSwitch() {}

    ManifestorStatus_t   GetCapabilities(unsigned int             *Capabilities);

    ManifestorStatus_t   WaitForFrozenSurface();

    void                 SetMaster(bool Master) { mIsMaster = Master;}

    bool                 IsMaster() { return mIsMaster;}

    // Support functions for derived classes

    virtual ManifestorStatus_t  RegisterCaptureDevice(class HavanaCapture_c *CaptureDevice)
    {
        (void)CaptureDevice; // warning removal
        return PlayerNotSupported;
    }
    virtual void                ReleaseCaptureBuffer(unsigned int            BufferIndex)
    {
        (void)BufferIndex; // warning removal
    }

    virtual uint32_t            GetExternalManifestationLatency() { return 0; }

protected:
    ManifestorConfiguration_t   Configuration;

    bool                        mIsMaster;

    //  Output port for used buffers
    Port_c                     *mOutputPort;

    unsigned long long          FrameCountManifested;

    ManifestorStatus_t   FlushEventQueue();

    virtual ManifestorStatus_t FlushDisplayQueue(bool ReleaseAllBuffers) = 0;

    PlayerSequenceNumber_t   *GetSequenceNumberStructure(Buffer_t Buffer)
    {
        Buffer_t OriginalCodedFrameBuffer;
        Buffer->ObtainAttachedBufferReference(Stream->GetCodedFrameBufferType(), &OriginalCodedFrameBuffer);
        SE_ASSERT(OriginalCodedFrameBuffer != NULL);

        PlayerSequenceNumber_t   *SequenceNumberStructure;
        OriginalCodedFrameBuffer->ObtainMetaDataReference(Stream->GetPlayer()->MetaDataSequenceNumberType, (void **) &SequenceNumberStructure);
        SE_ASSERT(SequenceNumberStructure != NULL);

        return SequenceNumberStructure;
    }

private:
    DISALLOW_COPY_AND_ASSIGN(Manifestor_Base_c);
};

#endif
