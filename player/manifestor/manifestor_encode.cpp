/************************************************************************
Copyright (C) 2003-2014 STMicroelectronics. All Rights Reserved.

This file is part of the Streaming Engine.

Streaming Engine is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License version 2 as
published by the Free Software Foundation.

Streaming Engine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with Streaming Engine; see the file COPYING.  If not, write to the Free
Software Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307,
USA.

The Streaming Engine Library may alternatively be licensed under a
proprietary license from ST.
************************************************************************/

#include "manifestor_encode.h"

#undef TRACE_TAG
#define TRACE_TAG   "Manifestor_Encoder_c"

//{{{  Constructor
//{{{  doxynote
/// \brief                      Initial state
/// \return                     InitializationStatus should be checked following construct
//}}}
Manifestor_Encoder_c::Manifestor_Encoder_c()
    : Encoder(NULL)
    , EncodeStream(NULL)
    , Encoder_BufferTypes(NULL)
    , SurfaceDescriptor()
    , ConnectBufferLock()
    , Connected(false)
    , Interrupted(false)
    , EncodeBufferArray()
    , mEncodeStreamPort(NULL)
    , mLastEncodedPts(INVALID_TIME)
    , mCurrentQueuedBuffers(0)
    , mQueuedBufferLock()
{
    if (InitializationStatus != ManifestorNoError)
    {
        SE_ERROR("Initialization status not valid - aborting init\n");
        return;
    }

    Configuration.Capabilities = MANIFESTOR_CAPABILITY_ENCODE;
    OS_InitializeMutex(&ConnectBufferLock);
    OS_InitializeMutex(&mQueuedBufferLock);
}
//}}}

//{{{  Destructor
//{{{  doxynote
/// \brief                      Halt to disconnect and then purge any remaining frames on the queue
/// \return                     No return value
//}}}
Manifestor_Encoder_c::~Manifestor_Encoder_c()
{
    Halt();

    OS_TerminateMutex(&ConnectBufferLock);
    OS_TerminateMutex(&mQueuedBufferLock);
}
//}}}

//{{{  Halt
//{{{  doxynote
/// \brief                      Release all external resources/handles.
///                             In this instance, the EncodeStream
/// \return                     Halt is a best effort task. It cannot fail.
//}}}
ManifestorStatus_t Manifestor_Encoder_c::Halt()
{
    SE_DEBUG(GetGroupTrace(), "Stream 0x%p %s\n", Stream, Configuration.ManifestorName);

    // Perform Halt Actions for the Base Class.
    Manifestor_Base_c::Halt();

    // Disconnect encode stream
    Disconnect(EncodeStream);

    return ManifestorNoError;
}
//}}}

//{{{  QueueDecodeBuffer
//{{{  doxynote
/// \brief                      Queue a decoded buffer from the Player to the Encoder
///
///  This function is called to receive a decode buffer from the Decode
///  Pipeline. The buffer is inserted in the EncodeStream port
///
/// \param Buffer               A Decode buffer from the Decode Pipeline
/// \param TimingArray          Timing array from the Manifestation Coordinator
/// \param NumTimes             The number of times this frame should be presented.
/// \return                     Success or fail based on if the buffer is queued or not.
//}}}
ManifestorStatus_t  Manifestor_Encoder_c::QueueDecodeBuffer(Buffer_t                      Buffer,
                                                            ManifestationOutputTiming_t **TimingArray,
                                                            unsigned int                 *NumTimes)
{
    unsigned int                  EncodeBufferIndex = 0;
    Buffer_t                      EncodeBuffer;
    AssertComponentState(ComponentRunning);

    (void)TimingArray; // warning removal

    //
    // Check availability of queues
    //

    if (!mOutputPort)
    {
        SE_ERROR("Stream 0x%p %s - mOutputPort not available\n", Stream, Configuration.ManifestorName);
        return ManifestorError;
    }

    // Prevent from parallel disconnection
    OS_LockMutex(&ConnectBufferLock);

    //
    // No action if not connected yet
    //

    if (!Connected)
    {
        SE_ERROR("Stream 0x%p %s Not connected\n", Stream, Configuration.ManifestorName);
        OS_UnLockMutex(&ConnectBufferLock);
        return ManifestorError;
    }

    //
    // We will only ever queue one buffer once for encode ... (At least at the moment)
    //

    if (*NumTimes > 1)
    {
        SE_WARNING("Stream 0x%p %s - Can only handle one output timing\n", Stream, Configuration.ManifestorName);
    }

    //
    // Now that we have a buffer containing a frame,
    // its time to Encode it...
    //
    SE_VERBOSE(GetGroupTrace(), "Stream 0x%p %s - extract %p\n", Stream, Configuration.ManifestorName, Buffer);

    ManifestorStatus_t Status = PrepareEncodeMetaData(Buffer, &EncodeBuffer);
    if (Status != ManifestorNoError)
    {
        SE_ERROR("Stream 0x%p Unable to obtain EncodeBuffer - Frame not pushed to encoder\n", Stream);
        OS_UnLockMutex(&ConnectBufferLock);
        return ManifestorError;
    }

    //
    // Save decoded buffer reference in the corresponding Encode Input Buffer array index
    //
    EncodeBuffer->GetIndex(&EncodeBufferIndex);
    if (EncodeBufferIndex >= ENCODER_MAX_INPUT_BUFFERS)
    {
        SE_ERROR("Stream 0x%p Unable to obtain EncodeBuffer index\n", Stream);
        EncodeBuffer->DecrementReferenceCount(IdentifierGetInjectBuffer);
        OS_UnLockMutex(&ConnectBufferLock);
        return ManifestorError;
    }

    OS_LockMutex(&mQueuedBufferLock);
    EncodeBufferArray[EncodeBufferIndex] = Buffer;
    mCurrentQueuedBuffers++;
    OS_UnLockMutex(&mQueuedBufferLock);

    //
    // Push the Encode Input Buffer to Encoder subsystem.
    // Buffer will be released via EncodeReleaseBuffer API.
    //
    if (mEncodeStreamPort->Insert((uintptr_t)EncodeBuffer) != RingNoError)
    {
        OS_LockMutex(&mQueuedBufferLock);
        mCurrentQueuedBuffers--;
        EncodeBufferArray[EncodeBufferIndex] = NULL;
        OS_UnLockMutex(&mQueuedBufferLock);

        SE_ERROR("Stream 0x%p Unable to send InputBuffer to EncodeStream\n", Stream);
        EncodeBuffer->DecrementReferenceCount(IdentifierGetInjectBuffer);
        OS_UnLockMutex(&ConnectBufferLock);
        return ManifestorError;
    }

    SE_DEBUG(GetGroupTrace(), "Stream 0x%p %s %d buffers after push\n", Stream, Configuration.ManifestorName, mCurrentQueuedBuffers);

    OS_UnLockMutex(&ConnectBufferLock);
    return ManifestorNoError;
}
//}}}

//{{{  GetSurfaceParameters
//{{{  doxynote
/// \brief      Fill in private structure with timing details of display surface
/// \param      SurfaceParameters pointer to structure to complete
/// \param      NumSurfaces Number of surfaces our manifestor has
//}}}
ManifestorStatus_t      Manifestor_Encoder_c::GetSurfaceParameters(OutputSurfaceDescriptor_t   **SurfaceParameters, unsigned int *NumSurfaces)
{
    // Set our only Surface Parameter Set
    SurfaceParameters[0] = &SurfaceDescriptor;
    // We only have 1 Surface Parameter Timing Set
    *NumSurfaces = 1;
    return ManifestorNoError;
}
//}}}

//{{{  GetNextQueuedManifestationTime
//{{{  doxynote
/// \brief      Get the earliest System time at which the next frame to Be queued will be encoded.
///             For more details, please have a look at the doxygen definition of
///             ManifestorStatus_t Manifestor_c::GetNextQueuedManifestationTime(unsigned long long *Time, unsigned int *NumTimes)
/// \param      Time     : estimated worse case time at which the next frame will be encoded.
/// \param      NumTimes : Number of Timings to manifestations which will occur
/// \return     Always no error
//}}}
ManifestorStatus_t      Manifestor_Encoder_c::GetNextQueuedManifestationTime(void *ParsedAudioVideoDataParameters, unsigned long long    *Time, unsigned int *NumTimes)
{
    (void)ParsedAudioVideoDataParameters; // warning removal

    *Time     = OS_GetTimeInMicroSeconds();
    *NumTimes = 1;
    //
    // Take into account the number of queued buffers to be encoded
    // Cumulated time at estimated 40ms per frame
    //
    // uint32_t estimatedDelay = mCurrentQueuedBuffers * 40000; TODO reenable once counter fixed; cf bz77492
    uint32_t estimatedDelay = 25000; // previous implementation: TODO remove
    *Time += estimatedDelay;
    SE_DEBUG(GetGroupTrace(), "Stream 0x%p %s - estimatedDelay:%d  for %d buffers\n",
             Stream, Configuration.ManifestorName,
             estimatedDelay, mCurrentQueuedBuffers);
    return ManifestorNoError;
}
//}}}

//{{{  PurgeEncodeStream
//{{{  doxynote
/// \brief                      Discard all encode buffers
/// \note: "Caller must hold ConnectBufferLock"
/// \return    ManifestorError   : in case at least one encode input buffer is not released
/// \return    ManifestorNoError : otherwise
//}}}
ManifestorStatus_t  Manifestor_Encoder_c::PurgeEncodeStream()
{
    unsigned int index;
    unsigned int nbTries;

    OS_AssertMutexHeld(&ConnectBufferLock);

    SE_INFO(GetGroupTrace(), "Stream 0x%p >\n", Stream);

    //
    // Flush Encode stream port
    //
    if (EncodeStream->FlushStages(true) != EncoderNoError)
    {
        SE_ERROR("Stream 0x%p Unable to flush the EncodeStream port\n", Stream);
        return ManifestorError;
    }

    //
    // Make sure all Encode input buffers pushed to Encoder have been released
    // Manage timeout in case of missing release of encode input buffer
    //
    index = 0;
    nbTries = 0;
    while ((index < ENCODER_MAX_INPUT_BUFFERS) && (nbTries < 100))
    {
        OS_UnLockMutex(&mQueuedBufferLock);
        Buffer_t    b = EncodeBufferArray[index];
        OS_UnLockMutex(&mQueuedBufferLock);
        if (b != NULL)
        {
            OS_SleepMilliSeconds(10);
            nbTries ++;
        }
        else
        {
            index ++;
            nbTries = 0;
        }
    }

    if (index < ENCODER_MAX_INPUT_BUFFERS)
    {
        SE_ERROR("Stream 0x%p Timeout - At least one encode input buffer is not released\n", Stream);
        return ManifestorError;
    }


    SE_DEBUG(GetGroupTrace(), "Stream 0x%p <\n", Stream);

    return ManifestorNoError;
}
//}}}

//{{{  FlushDisplayQueue
//{{{  doxynote
/// \brief      Flushes the display queue so buffers not yet manifested are returned
//}}}
ManifestorStatus_t      Manifestor_Encoder_c::FlushDisplayQueue(bool ReleaseAllBuffers)
{
    (void)ReleaseAllBuffers; // warning removal

    SE_DEBUG(GetGroupTrace(), "Stream 0x%p %s\n", Stream, Configuration.ManifestorName);

    //
    // check if currently connected
    //

    // Prevent flush during disconnection
    OS_LockMutex(&ConnectBufferLock);

    if (!Connected)
    {
        OS_UnLockMutex(&ConnectBufferLock);
        return ManifestorNoError;
    }

    ManifestorStatus_t Status = PurgeEncodeStream();
    OS_UnLockMutex(&ConnectBufferLock);

    return Status;
}
//}}}

//{{{  Connect
//{{{  doxynote
/// \brief                      Connect to memory Sink port
/// \param                      SrcHandle  : HavanaStream object
/// \param                      SinkHandle : EncodeStream object to connect to
/// \return                     Success or fail
//}}}
ManifestorStatus_t Manifestor_Encoder_c::Connect(stm_object_h  SrcHandle, stm_object_h  SinkHandle)
{
    if (Connected)
    {
        SE_ERROR("Stream 0x%p %s - already connected\n", Stream, Configuration.ManifestorName);
        return ManifestorError;
    }

    if (SinkHandle == NULL || SrcHandle == NULL)
    {
        SE_ERROR("Stream 0x%p %s - Invalid attachment parameters\n", Stream, Configuration.ManifestorName);
        return ManifestorError;
    }

    SE_INFO(GetGroupTrace(), "Stream 0x%p %s srchandle:0x%p sinkhandle:0x%p (encodestream)\n",
            Stream, Configuration.ManifestorName, SrcHandle, SinkHandle);
    //
    // Take local references to the EncodeStream object we are connecting to.
    // These will be used by the Leaf Classes to Inject frames into the Encoder
    //
    EncodeStream = (EncodeStream_c *)SinkHandle;
    EncodeStream->GetEncoder(&Encoder);
    Encoder_BufferTypes = Encoder->GetBufferTypes();

    //
    // Now connect to EncoderStream port and ReleaseCallBack to Encoder
    //
    if (EncodeStream->Connect(this, &mEncodeStreamPort) != EncoderNoError)
    {
        SE_ERROR("Stream 0x%p %s - Failed to connect Encoder\n", Stream, Configuration.ManifestorName);
        EncodeStream = NULL;
        return ManifestorError;
    }

    //
    // Make sure InputPort is correctly created
    //
    if (mEncodeStreamPort == NULL)
    {
        SE_ERROR("Stream 0x%p %s - Failed to obtain Encoder Input port\n", Stream, Configuration.ManifestorName);
        EncodeStream->Disconnect();
        EncodeStream = NULL;
        return ManifestorError;
    }

    // A memory barrier to ensure that the Connected Flag is not set before all of the above conditions
    OS_Smp_Mb();
    Connected   = true;

    //
    // We can begin accepting Input Data
    //
    SetComponentState(ComponentRunning);
    return ManifestorNoError;
}
//}}}

//{{{  Disconnect
//{{{  doxynote
/// \brief                      Disconnect from memory Sink port (if connected)
///                             Release pending pull call if any
/// \return                     Success or fail
//}}}
ManifestorStatus_t Manifestor_Encoder_c::Disconnect(stm_object_h  SinkObject)
{
    SE_INFO(GetGroupTrace(), "Stream 0x%p sinkobject:0x%p encodestream:0x%p\n", Stream, SinkObject, EncodeStream);

    //
    // Check that Sink object corresponds to the connected Stream
    //
    if (SinkObject != EncodeStream)
    {
        SE_ERROR("Stream 0x%p %s - invalid object\n", Stream, Configuration.ManifestorName);
        return ManifestorError;
    }

    if (Connected == true)
    {
        // prevent queuing of buffer during disconnection
        OS_LockMutex(&ConnectBufferLock);

        // Close connection for memsink calls
        // and shut down buffer release thread
        SE_INFO(GetGroupTrace(), "Stream 0x%p %s\n", Stream, Configuration.ManifestorName);
        Connected = false;

        // release resources
        PurgeEncodeStream();

        // Now disconnect from EncoderStream port
        if (EncodeStream->Disconnect() != EncoderNoError)
        {
            SE_ERROR("Stream 0x%p %s - Failed to disconnect Encoder\n", Stream, Configuration.ManifestorName);
        }
        mEncodeStreamPort = NULL;

        OS_UnLockMutex(&ConnectBufferLock);
    }

    return ManifestorNoError;
}
//}}}

//{{{  HandleMarkerFrame
//{{{  doxynote
/// \brief                      Handle Marker Frame
///                             Required by the Manifestor Interface
/// \return                     status
//}}}
ManifestorStatus_t  Manifestor_Encoder_c::HandleMarkerFrame(Buffer_t MarkerFrameBuffer)
{
    PlayerSequenceNumber_t *SequenceNumberStructure = GetSequenceNumberStructure(MarkerFrameBuffer);
    MarkerFrame_t           MarkerFrame = SequenceNumberStructure->mMarkerFrame;

    SE_DEBUG(GetGroupTrace(), "Stream 0x%p %s received Marker (type %d) #%lld\n"
             , Stream
             , Configuration.ManifestorName
             , MarkerFrame.mMarkerType
             , MarkerFrame.mSequenceNumber);

    return PushMarkerFrameInputBuffer(MarkerFrameBuffer);
}
//}}}

//{{{  GetNativeTimeOfCurrentlyManifestedFrame
//{{{  doxynote
/// \brief                      Get the Native Time of Current Manifested Frame
///                             Required by the Manifestor Interface
/// \return                     Always no error
//}}}
ManifestorStatus_t  Manifestor_Encoder_c::GetNativeTimeOfCurrentlyManifestedFrame(unsigned long long *Time)
{
    SE_VERBOSE(GetGroupTrace(), "Stream 0x%p %lld\n", Stream, mLastEncodedPts);
    *Time = mLastEncodedPts;
    return ManifestorNoError;
}
//}}}

//{{{  ReleaseBuffer
//{{{  doxynote
/// \brief                      Callback function to release a previously pushed EncodeInputBuffer.
/// \param                      EncodeBuffer  : Encode input buffer to release
/// \return                     PlayerNoError: no error
///                             PlayerError  : fails to release the buffer
//}}}
PlayerStatus_t Manifestor_Encoder_c::ReleaseBuffer(Buffer_t EncodeBuffer)
{
    unsigned int           EncodeBufferIndex = 0;
    Buffer_t               DecodedBuffer;

    //
    // Check if there is a corresponding Decode buffer for this EncodeBuffer
    //
    EncodeBuffer->GetIndex(&EncodeBufferIndex);
    if (EncodeBufferIndex >= ENCODER_MAX_INPUT_BUFFERS)
    {
        SE_ERROR("Stream 0x%p Unable to obtain EncodeBuffer index\n", Stream);
        return PlayerError;
    }

    OS_LockMutex(&mQueuedBufferLock);
    if (EncodeBufferArray[EncodeBufferIndex] == NULL)
    {
        SE_ERROR("Stream 0x%p Trying to release twice an Encode input buffer\n", Stream);
        OS_UnLockMutex(&mQueuedBufferLock);
        return PlayerError;
    }

    DecodedBuffer = EncodeBufferArray[EncodeBufferIndex];
    EncodeBufferArray[EncodeBufferIndex] = NULL;

    //  Release InputBuffer
    EncodeBuffer->DecrementReferenceCount(IdentifierGetInjectBuffer);

    // SE_ASSERT(mCurrentQueuedBuffers != 0); commented out: cf bz77492
    if (mCurrentQueuedBuffers == 0)
    {
        SE_ERROR("Stream 0x%p mCurrentQueuedBuffers is 0 upon release call\n", Stream);
    }
    else
    {
        mCurrentQueuedBuffers--;
    }

    SE_DEBUG(GetGroupTrace(), "Stream 0x%p %s %d buffers after release\n"
             , Stream
             , Configuration.ManifestorName
             , mCurrentQueuedBuffers);

    OS_UnLockMutex(&mQueuedBufferLock);

    PlayerSequenceNumber_t *SequenceNumberStructure = GetSequenceNumberStructure(DecodedBuffer);

    if (SequenceNumberStructure->mIsMarkerFrame == false)
    {
        // update Time to have GetPlayInfo function working properly
        ParsedFrameParameters_t  *FrameParameters;
        DecodedBuffer->ObtainMetaDataReference(Player->MetaDataParsedFrameParametersReferenceType, (void **)&FrameParameters);
        SE_ASSERT(FrameParameters != NULL);
        mLastEncodedPts                 = FrameParameters->PTS.NativeValue();
    }
    // Release the corresponding DecodedBuffer
    mOutputPort->Insert((uintptr_t) DecodedBuffer);

    return PlayerNoError;
}
//}}}

//{{{  PushMarkerFrameInputBuffer
//{{{  doxynote
/// \brief                     Create an Encode input buffer to notify the frame marker and push it to
///                             encoder. This is a specific buffer for which there is
///                             no decode buffer attached.
/// \return     ManifestorError  : fail to get buffer index or issue when inserting buffer to Encoder
/// \return     ManifestorNoError: otherwise
//}}}
ManifestorStatus_t Manifestor_Encoder_c::PushMarkerFrameInputBuffer(Buffer_t MarkerFrameBuffer)
{
    stm_se_uncompressed_frame_metadata_t     *Meta;
    Buffer_t                                  EncodeBuffer;
    unsigned int                              EncodeBufferIndex;

    SE_DEBUG(group_encoder_stream, "Stream 0x%p\n", Stream);

    // Prevent from parallel disconnection
    OS_LockMutex(&ConnectBufferLock);

    if (!Connected)
    {
        SE_WARNING("Stream 0x%p %s - Not connected\n", Stream, Configuration.ManifestorName);
        mOutputPort->Insert((uintptr_t) MarkerFrameBuffer);
        OS_UnLockMutex(&ConnectBufferLock);
        return ManifestorNoError;
    }

    PlayerSequenceNumber_t *SequenceNumberStructure = GetSequenceNumberStructure(MarkerFrameBuffer);
    MarkerFrame_t           MarkerFrame = SequenceNumberStructure->mMarkerFrame;

    SE_DEBUG(GetGroupTrace(), "Stream 0x%p this 0x%p received Marker (type %d) #%lld\n", Stream, this, MarkerFrame.mMarkerType, MarkerFrame.mSequenceNumber);

    // Currently only EOS marker is managed by the encoder and encode coordinator
    if (MarkerFrame.mMarkerType != EosMarker)
    {
        SE_DEBUG(GetGroupTrace(), "Stream 0x%p Discard frame marker (type %d)\n", Stream, MarkerFrame.mMarkerType);
        mOutputPort->Insert((uintptr_t) MarkerFrameBuffer);
        OS_UnLockMutex(&ConnectBufferLock);
        return ManifestorNoError;
    }

    // Get Encoder buffer
    EncoderStatus_t Status = Encoder->GetInputBuffer(&EncodeBuffer);
    if ((Status != EncoderNoError) || (EncodeBuffer == NULL))
    {
        SE_ERROR("Stream 0x%p Failed to get Encoder Input Buffer\n", Stream);
        mOutputPort->Insert((uintptr_t) MarkerFrameBuffer);
        OS_UnLockMutex(&ConnectBufferLock);
        return ManifestorNoError;
    }

    EncodeBuffer->ObtainMetaDataReference(Encoder_BufferTypes->InputMetaDataBufferType, (void **)(&Meta));
    SE_ASSERT(Meta != NULL);

    // Set EOS discontinuity
    Meta->discontinuity = STM_SE_DISCONTINUITY_EOS;

    EncodeBuffer->GetIndex(&EncodeBufferIndex);
    if (EncodeBufferIndex >= ENCODER_MAX_INPUT_BUFFERS)
    {
        SE_ERROR("Stream 0x%p Unable to obtain EncodeBuffer index\n", Stream);
        EncodeBuffer->DecrementReferenceCount(IdentifierGetInjectBuffer);
        mOutputPort->Insert((uintptr_t) MarkerFrameBuffer);
        OS_UnLockMutex(&ConnectBufferLock);
        return ManifestorNoError;
    }

    // The Input Encode Buffer is attached to MarkerFrameBuffer
    OS_LockMutex(&mQueuedBufferLock);
    EncodeBufferArray[EncodeBufferIndex] = MarkerFrameBuffer;
    mCurrentQueuedBuffers++;
    OS_UnLockMutex(&mQueuedBufferLock);

    // Push the Encode Input Buffer for frame marker to Encoder subsystem.
    if (mEncodeStreamPort->Insert((uintptr_t) EncodeBuffer) != RingNoError)
    {
        OS_LockMutex(&mQueuedBufferLock);
        mCurrentQueuedBuffers--;
        EncodeBufferArray[EncodeBufferIndex] = NULL;
        OS_UnLockMutex(&mQueuedBufferLock);

        SE_ERROR("Stream 0x%p Unable to send frame marker to EncodeStream\n", Stream);
        EncodeBuffer->DecrementReferenceCount(IdentifierGetInjectBuffer);
        mOutputPort->Insert((uintptr_t) MarkerFrameBuffer);
        OS_UnLockMutex(&ConnectBufferLock);
        return ManifestorNoError;
    }

    SE_DEBUG(GetGroupTrace(), "Stream 0x%p %s %d buffers after push\n", Stream, Configuration.ManifestorName, mCurrentQueuedBuffers);
    OS_UnLockMutex(&ConnectBufferLock);
    return ManifestorNoError;
}
//}}}

//{{{  StampFrame
//{{{  doxynote
/// \brief                      Fill-in encoder sequence number metadata
///                             with play_stream information which is used by se-pipeline.
/// \return     ManifestorNoError
//}}}
ManifestorStatus_t Manifestor_Encoder_c::StampFrame(Buffer_t Buffer, Buffer_t InputBuffer)
{
    PlayerSequenceNumber_t  *PlayerSequenceNumberStructure = GetSequenceNumberStructure(Buffer);
    if (PlayerSequenceNumberStructure != NULL)
    {
        EncoderSequenceNumber_t  *EncoderSequenceNumberStructure;

        InputBuffer->ObtainMetaDataReference(Encoder_BufferTypes->MetaDataSequenceNumberType, (void **)(&EncoderSequenceNumberStructure));
        SE_ASSERT(EncoderSequenceNumberStructure != NULL);

        ParsedFrameParameters_t  *ParsedFrameParameters;
        Buffer->ObtainMetaDataReference(Player->MetaDataParsedFrameParametersReferenceType, (void **)(&ParsedFrameParameters));
        SE_ASSERT(ParsedFrameParameters != NULL);

        EncoderSequenceNumberStructure->StreamUniqueIdentifier = (unsigned int)PlayerSequenceNumberStructure->StreamUniqueIdentifier;
        EncoderSequenceNumberStructure->StreamTypeIdentifier = PlayerSequenceNumberStructure->StreamTypeIdentifier;
        EncoderSequenceNumberStructure->FrameCounter = PlayerSequenceNumberStructure->Value;
        EncoderSequenceNumberStructure->PTS = ParsedFrameParameters->PTS.NativeValue();

        SE_VERBOSE(group_se_pipeline, "Stream 0x%x - %d - Manifestor 0x%p #%lld PTS=%lld ME=%llu\n",
                   EncoderSequenceNumberStructure->StreamUniqueIdentifier,
                   EncoderSequenceNumberStructure->StreamTypeIdentifier,
                   this,
                   EncoderSequenceNumberStructure->FrameCounter,
                   EncoderSequenceNumberStructure->PTS,
                   OS_GetTimeInMicroSeconds()
                  );
    }
    return  ManifestorNoError;
}
//}}}
