/************************************************************************
Copyright (C) 2003-2014 STMicroelectronics. All Rights Reserved.

This file is part of the Streaming Engine.

Streaming Engine is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License version 2 as
published by the Free Software Foundation.

Streaming Engine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with Streaming Engine; see the file COPYING.  If not, write to the Free
Software Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307,
USA.

The Streaming Engine Library may alternatively be licensed under a
proprietary license from ST.
************************************************************************/

#include "manifestor_video_grab.h"

#undef TRACE_TAG
#define TRACE_TAG   "Manifestor_VideoGrab_c"

struct QueueRecord_s
{
    unsigned int        Flags;
    unsigned int        Count;
    int                 PSIndex;
};

//{{{  Constructor
// /////////////////////////////////////////////////////////////////////////
//
//      Constructor :
//      Action  : Initialise state
//      Input   :
//      Output  :
//      Result  :
//

Manifestor_VideoGrab_c::Manifestor_VideoGrab_c()
    : CaptureDevice(NULL)
    , CaptureBuffers()
    , CaptureBuffersForEOS()
{
    if (InitializationStatus != ManifestorNoError)
    {
        SE_ERROR("Initialization status not valid - aborting init\n");
        return;
    }

    SE_DEBUG(group_manifestor_video_grab, "\n");

    SetGroupTrace(group_manifestor_video_grab);

    Configuration.Capabilities                  = MANIFESTOR_CAPABILITY_GRAB;

    SurfaceDescriptor[0].StreamType             = StreamTypeVideo;
    SurfaceDescriptor[0].ClockPullingAvailable  = false;
    SurfaceDescriptor[0].DisplayWidth           = FRAME_GRAB_DEFAULT_DISPLAY_WIDTH;
    SurfaceDescriptor[0].DisplayHeight          = FRAME_GRAB_DEFAULT_DISPLAY_HEIGHT;
    SurfaceDescriptor[0].Progressive            = true;
    SurfaceDescriptor[0].FrameRate              = FRAME_GRAB_DEFAULT_FRAME_RATE; // rational
    SurfaceDescriptor[0].PercussiveCapable      = true;
    // But request that we set output to match input rates
    SurfaceDescriptor[0].InheritRateAndTypeFromSource = true;
}
//}}}
//{{{  Destructor
// /////////////////////////////////////////////////////////////////////////
//
//      Destructor :
//      Action  : Give up switch off the lights and go home
//      Input   :
//      Output  :
//      Result  :
//

Manifestor_VideoGrab_c::~Manifestor_VideoGrab_c()
{
    SE_DEBUG(group_manifestor_video_grab, "\n");

    CloseOutputSurface();
    Halt();
}
//}}}
//{{{  Halt
//{{{  doxynote
/// \brief              Shutdown, stop presenting and retrieving frames
///                     don't return until complete
//}}}
ManifestorStatus_t      Manifestor_VideoGrab_c::Halt()
{
    SE_DEBUG(group_manifestor_video_grab, "\n");
    PtsOnDisplay    = INVALID_TIME;
    return Manifestor_Video_c::Halt();
}
//}}}
//}}}
//{{{  UpdateOutputSurfaceDescriptor
//{{{  doxynote
/// \brief      Find out information about display plane and output
/// \return     ManifestorNoError if done successfully
//}}}
ManifestorStatus_t Manifestor_VideoGrab_c::UpdateOutputSurfaceDescriptor()
{
    SE_DEBUG(group_manifestor_video_grab, "\n");
    SurfaceDescriptor[0].StreamType                = StreamTypeVideo;
    SurfaceDescriptor[0].ClockPullingAvailable     = false;

    if (StreamDisplayParameters.FrameRate != 0)
    {
        SurfaceDescriptor[0].DisplayWidth          = StreamDisplayParameters.DisplayWidth;
        SurfaceDescriptor[0].DisplayHeight         = StreamDisplayParameters.DisplayHeight;
        SurfaceDescriptor[0].Progressive           = StreamDisplayParameters.Progressive;
        SurfaceDescriptor[0].FrameRate             = StreamDisplayParameters.FrameRate;
    }
    else
    {
        SurfaceDescriptor[0].DisplayWidth          = FRAME_GRAB_DEFAULT_DISPLAY_WIDTH;
        SurfaceDescriptor[0].DisplayHeight         = FRAME_GRAB_DEFAULT_DISPLAY_HEIGHT;
        SurfaceDescriptor[0].Progressive           = true;
        SurfaceDescriptor[0].FrameRate             = FRAME_GRAB_DEFAULT_FRAME_RATE; // rational
    }

    // Request that we set output to match input rates
    SurfaceDescriptor[0].InheritRateAndTypeFromSource = true;
    return ManifestorNoError;
}
//}}}
//{{{  OpenOutputSurface
//{{{  doxynote
/// \brief      Find out information about display plane and output and use it to
///             open handles to the desired display plane
/// \param      Device Handle of the display device.
/// \param      PlaneId Plane identifier (interpreted in the same way as stgfb)
/// \return     ManifestorNoError if plane opened successfully
//}}}
ManifestorStatus_t Manifestor_VideoGrab_c::OpenOutputSurface(void *Device)
{
    (void)Device; // warning removal
    SE_DEBUG(group_manifestor_video_grab, "\n");
    mIsVisible = false;
    return ManifestorNoError;
}
//}}}
//{{{  CloseOutputSurface
//{{{  doxynote
/// \brief              Release all frame buffer resources
//}}}
ManifestorStatus_t Manifestor_VideoGrab_c::CloseOutputSurface()
{
    SE_DEBUG(group_manifestor_video_grab, "\n");
    mIsVisible = false;
    return ManifestorNoError;
}
//}}}
//{{{  PrepareToQueue
//{{{  doxynote
/// \brief              Queue the first field of the first picture to the display
/// \param              Buffer containing frame to be displayed
/// \result             Success if frame displayed, fail if not displayed or a frame has already been displayed
//}}}
ManifestorStatus_t      Manifestor_VideoGrab_c::PrepareToQueue(class Buffer_c              *Buffer,
                                                               struct capture_buffer_s     *CaptureBuffer,
                                                               struct ManifestationOutputTiming_s  **VideoOutputTimingArray)
{
    SE_DEBUG(group_manifestor_video_grab, "\n");

    struct ParsedVideoParameters_s     *VideoParameters;
    Buffer->ObtainMetaDataReference(Player->MetaDataParsedVideoParametersType, (void **)&VideoParameters);
    SE_ASSERT(VideoParameters != NULL);

    struct ParsedFrameParameters_s     *ParsedFrameParameters;
    Buffer->ObtainMetaDataReference(Player->MetaDataParsedFrameParametersReferenceType, (void **)(&ParsedFrameParameters));
    SE_ASSERT(ParsedFrameParameters != NULL);

    memset((void *)CaptureBuffer, 0, sizeof(struct capture_buffer_s));

    //
    // Setup input window - portion of incoming content to copy to output window
    //
    struct Window_s                     InputWindow;
    InputWindow.X                       = 0;
    InputWindow.Y                       = 0;

    //
    // Get the Display resolution
    //
    InputWindow.Width                   = VideoParameters->Content.DisplayWidth;
    InputWindow.Height                  = VideoParameters->Content.DisplayHeight;

    //
    // Get Decimated frame policy
    //
    bool DecimateIfAvailable = false;

    if (Player->PolicyValue(Playback, Stream, PolicyDecimateDecoderOutput) != PolicyValueDecimateDecoderOutputDisabled)
    {
        DecimateIfAvailable             = true;
    }

    //{{{  Set up buffer pointers
    DecodeBufferComponentType_t         ManifestationComponent;
    // Fill in src fields depends on decimation
    if ((!DecimateIfAvailable) || (!Stream->GetDecodeBufferManager()->ComponentPresent(Buffer, DecimatedManifestationComponent)))
    {
        // Check for Mpeg2 deblocked output which comes in decimated buffer
        bool DeblockOn = (PolicyValueDecimateDecoderOutputH1V1 == Player->PolicyValue(Playback, Stream, PolicyDecimateDecoderOutput));

        if (DeblockOn)
        {
            ManifestationComponent = DecimatedManifestationComponent;
        }
        else
        {
            ManifestationComponent = PrimaryManifestationComponent;
        }
        CaptureBuffer->rectangle.x          = InputWindow.X;
        CaptureBuffer->rectangle.y          = InputWindow.Y;
        CaptureBuffer->rectangle.width      = InputWindow.Width;
        CaptureBuffer->rectangle.height     = InputWindow.Height;
    }
    else
    {
        unsigned int    HDecimationFactor   = 0;
        unsigned int    VDecimationFactor   = 0;
        ManifestationComponent              = DecimatedManifestationComponent;
        HDecimationFactor                   = (Stream->GetDecodeBufferManager()->DecimationFactor(Buffer, 0) == 2) ? 1 : 2;
        VDecimationFactor                   = 1;
        CaptureBuffer->rectangle.x          = InputWindow.X      >> HDecimationFactor;
        CaptureBuffer->rectangle.y          = InputWindow.Y      >> VDecimationFactor;
        CaptureBuffer->rectangle.width      = InputWindow.Width  >> HDecimationFactor;
        CaptureBuffer->rectangle.height     = InputWindow.Height >> VDecimationFactor;
    }

    CaptureBuffer->virtual_address          = NULL;
    CaptureBuffer->physical_address         = (unsigned long)Stream->GetDecodeBufferManager()->ComponentBaseAddress(Buffer, ManifestationComponent, PhysicalAddress);
    CaptureBuffer->size                     = Stream->GetDecodeBufferManager()->ComponentSize(Buffer, ManifestationComponent);
    CaptureBuffer->chroma_buffer_offset     = Stream->GetDecodeBufferManager()->Chroma(Buffer, ManifestationComponent) -
                                              Stream->GetDecodeBufferManager()->Luma(Buffer, ManifestationComponent);
    CaptureBuffer->stride                   = Stream->GetDecodeBufferManager()->ComponentStride(Buffer, ManifestationComponent, 0, 0);
    CaptureBuffer->presentation_time        = VideoOutputTimingArray[0]->SystemPlaybackTime;
    CaptureBuffer->native_presentation_time = ParsedFrameParameters->PTS.uSecValue();
    CaptureBuffer->flags                    = 0;
    CaptureBuffer->total_lines              = Stream->GetDecodeBufferManager()->ComponentDimension(Buffer, ManifestationComponent, 1);

    //SE_INFO(group_manifestor_video_grab, "Rect = %dx%d at %d,%d\n", CaptureBuffer->rectangle.width, CaptureBuffer->rectangle.height, CaptureBuffer->rectangle.x, CaptureBuffer->rectangle.y);
    //}}}
    //{{{  Set up buffer details
    switch (Stream->GetDecodeBufferManager()->ComponentDataType(PrimaryManifestationComponent))
    {
    case FormatVideo420_MacroBlock:
        CaptureBuffer->buffer_format        = SURFACE_FORMAT_VIDEO_420_MACROBLOCK;
        CaptureBuffer->pixel_depth          = 8;
        break;

    case FormatVideo420_PairedMacroBlock:
        CaptureBuffer->buffer_format        = SURFACE_FORMAT_VIDEO_420_PAIRED_MACROBLOCK;
        CaptureBuffer->pixel_depth          = 8;
        break;

    case FormatVideo8888_ARGB:
        CaptureBuffer->buffer_format        = SURFACE_FORMAT_VIDEO_8888_ARGB;
        CaptureBuffer->pixel_depth          = 32;
        break;

    case FormatVideo888_RGB:
        CaptureBuffer->buffer_format        = SURFACE_FORMAT_VIDEO_888_RGB;
        CaptureBuffer->pixel_depth          = 24;
        break;

    case FormatVideo565_RGB:
        CaptureBuffer->buffer_format        = SURFACE_FORMAT_VIDEO_565_RGB;
        CaptureBuffer->pixel_depth          = 16;
        break;

    case FormatVideo422_Raster:
        CaptureBuffer->buffer_format        = SURFACE_FORMAT_VIDEO_422_RASTER;
        CaptureBuffer->pixel_depth          = 16;
        break;

    case FormatVideo420_PlanarAligned:
    case FormatVideo420_Planar:
        CaptureBuffer->buffer_format        = SURFACE_FORMAT_VIDEO_420_PLANAR;
        CaptureBuffer->pixel_depth          = 8;
        CaptureBuffer->rectangle.x         += Stream->GetDecodeBufferManager()->ComponentBorder(PrimaryManifestationComponent, 0) * 16;
        CaptureBuffer->rectangle.y         += Stream->GetDecodeBufferManager()->ComponentBorder(PrimaryManifestationComponent, 1) * 16;
        break;

    case FormatVideo422_Planar:
        CaptureBuffer->buffer_format        = SURFACE_FORMAT_VIDEO_422_PLANAR;
        CaptureBuffer->pixel_depth          = 8;
        CaptureBuffer->rectangle.x         += Stream->GetDecodeBufferManager()->ComponentBorder(PrimaryManifestationComponent, 0) * 16;
        CaptureBuffer->rectangle.y         += Stream->GetDecodeBufferManager()->ComponentBorder(PrimaryManifestationComponent, 1) * 16;
        break;

    case FormatVideo422_YUYV:
        CaptureBuffer->buffer_format        = SURFACE_FORMAT_VIDEO_422_YUYV;
        CaptureBuffer->pixel_depth          = 16;
        break;

    case FormatVideo420_Raster2B:
        CaptureBuffer->buffer_format        = SURFACE_FORMAT_VIDEO_420_RASTER2B;
        CaptureBuffer->pixel_depth          = 8;
        break;

    case FormatVideo420_Raster2B_10B:
        CaptureBuffer->buffer_format        = SURFACE_FORMAT_VIDEO_420_RASTER2B_10B;
        CaptureBuffer->pixel_depth          = 10;
        break;

    default:
        SE_ERROR("Unsupported display format (%d)\n", Stream->GetDecodeBufferManager()->ComponentDataType(PrimaryManifestationComponent));
        break;
    }

    //}}}
    //{{{  Add colourspace flags
    switch (VideoParameters->Content.ColourMatrixCoefficients)
    {
    case MatrixCoefficients_ITU_R_BT601:                            // Do nothing, use 601 coefficients
    case MatrixCoefficients_ITU_R_BT470_2_M:
    case MatrixCoefficients_ITU_R_BT470_2_BG:
    case MatrixCoefficients_SMPTE_170M:
    case MatrixCoefficients_FCC:
        break;

    case MatrixCoefficients_ITU_R_BT709:                            // Use 709 coefficients
    case MatrixCoefficients_SMPTE_240M:
        CaptureBuffer->flags               |= CAPTURE_COLOURSPACE_709;
        break;

    case MatrixCoefficients_ITU_R_BT2020:
        CaptureBuffer->flags               |= CAPTURE_COLOURSPACE_2020;
        break;

    case MatrixCoefficients_Undefined:
    default:
        if (VideoParameters->Content.Width > 720)                   // Base coefficients on display size SD=601, HD = 709
        {
            CaptureBuffer->flags           |= CAPTURE_COLOURSPACE_709;
        }

        break;
    }

    return ManifestorNoError;
}
//}}}
//{{{  QueueBuffer
//{{{  doxynote
/// \brief                      Actually put buffer on display
/// \param BufferIndex          Index into array of stream buffers
/// \param FrameParameters      Frame parameters generated by frame parser
/// \param VideoParameters      Display positioning information generated by frame parser
/// \param VideoOutputTiming    Display timing information generated by output timer
/// \return                     Success or fail
//}}}
ManifestorStatus_t Manifestor_VideoGrab_c::QueueBuffer(unsigned int                        BufferIndex,
                                                       struct ParsedFrameParameters_s     *FrameParameters,
                                                       struct ParsedVideoParameters_s     *VideoParameters,
                                                       struct ManifestationOutputTiming_s **VideoOutputTimingArray,
                                                       Buffer_t                            Buffer)
{
    struct StreamBuffer_s      *StreamBuff      = &StreamBuffer[BufferIndex];
    struct capture_buffer_s    *CaptureBuffer   = &CaptureBuffers[BufferIndex];

    (void)FrameParameters; // warning removal
    (void)VideoParameters; // warning removal

    AssertComponentState(ComponentRunning);
    SE_DEBUG(group_manifestor_video_grab, "CaptureDevice %p, Buffer %p, BufferClass %p\n", CaptureDevice, Buffer, StreamBuff->BufferClass);

    if (CaptureDevice == NULL)
    {
        return ManifestorError;
    }

    if ((VideoOutputTimingArray[SOURCE_INDEX]->DisplayCount[0] == 0)
        && (VideoOutputTimingArray[SOURCE_INDEX]->DisplayCount[1] == 0))
    {
        return ManifestorNoError;
    }

    StreamBuff->Manifestor          = this;
    ManifestorStatus_t Status       = PrepareToQueue(Buffer, CaptureBuffer, VideoOutputTimingArray);
    if (Status != ManifestorNoError)
    {
        return Status;
    }

    StreamBuff->QueueCount          = 1;
    CaptureBuffer->user_private     = BufferIndex;
    StreamBuff->BufferState = BufferStateQueued;
    CaptureDevice->QueueBuffer(CaptureBuffer);

    return ManifestorNoError;
}
//}}}
//}}}
//{{{  FlushDisplayQueue
//{{{  doxynote
/// \brief      Flushes the display queue so buffers not yet manifested are returned
//}}}
ManifestorStatus_t      Manifestor_VideoGrab_c::FlushDisplayQueue(bool ReleaseAllBuffers)
{
    (void)ReleaseAllBuffers; // warning removal

    SE_DEBUG(group_manifestor_video_grab, "\n");
    OS_LockMutex(&BufferLock);

    if (CaptureDevice == NULL)
    {
        OS_UnLockMutex(&BufferLock);
        return ManifestorError;
    }

    CaptureDevice->FlushQueue();
    OS_UnLockMutex(&BufferLock);
    return ManifestorNoError;
}
//}}}

//}}}
//{{{  Enable
// ///////////////////////////////////////////////////////////////////////////////////////
//    Enable
//
int Manifestor_VideoGrab_c::Enable()
{
    SE_DEBUG(group_manifestor_video_grab, "\n");
    mIsVisible = true;
    return 0;
}
//}}}
//{{{  Disable
// ///////////////////////////////////////////////////////////////////////////////////////
//    Disable
//
int Manifestor_VideoGrab_c::Disable()
{
    SE_DEBUG(group_manifestor_video_grab, "\n");
    mIsVisible = false;
    return 0;
}
//}}}
//{{{  GetEnable
// ///////////////////////////////////////////////////////////////////////////////////////
//    GetEnable
//
bool Manifestor_VideoGrab_c::GetEnable()
{
    SE_DEBUG(group_manifestor_video_grab, "\n");
    return mIsVisible;
}
//}}}
//{{{  RegisterCaptureDevice
ManifestorStatus_t Manifestor_VideoGrab_c::RegisterCaptureDevice(class HavanaCapture_c          *CaptureDevice)
{
    SE_INFO(group_manifestor_video_grab, "CaptureDevice %p\n", CaptureDevice);
    OS_LockMutex(&BufferLock);
    this->CaptureDevice     = CaptureDevice;
    OS_UnLockMutex(&BufferLock);
    return ManifestorNoError;
}
//}}}
//{{{  ReleaseCaptureBuffer
void Manifestor_VideoGrab_c::ReleaseCaptureBuffer(unsigned int    BufferIndex)
{
    struct StreamBuffer_s      *Buffer;
    SE_DEBUG(group_manifestor_video_grab, "%d\n", BufferIndex);

    if (BufferIndex == INVALID_INDEX)
    {
        SE_ERROR("Buffer with invalid Index\n");
        return;
    }

    Buffer                                              = &StreamBuffer[BufferIndex];
    BufferOnDisplay                                     = Buffer->BufferIndex;
    Buffer->OutputTiming[0]->ActualSystemPlaybackTime   = Buffer->OutputTiming[0]->SystemPlaybackTime;
    TimeStamp_c PlaybackTime(Buffer->NativePlaybackTime, TIME_FORMAT_PTS);
    PtsOnDisplay = TimeStamp_c::AddNativeOffset(PlaybackTime, -Buffer->PtsOffset).NativeValue();
    SE_DEBUG(GetGroupTrace(), "PlaybackTime %llu New playback time %llu offset %lld\n", PlaybackTime.PtsValue(), PtsOnDisplay, Buffer->PtsOffset);
    FrameCountManifested++;

    Buffer->QueueCount--;

    if (Buffer->QueueCount == 0)
    {
        Buffer->BufferState   = BufferStateAvailable;

        SE_VERBOSE(group_manifestor_video_grab, "Release Buffer #%d\n", Buffer->BufferIndex);
        mOutputPort->Insert((uintptr_t) Buffer->BufferClass);
    }
}
//}}}

void Manifestor_VideoGrab_c::HandleEndOfStream()
{
    CaptureBuffersForEOS.virtual_address  = 0;                   // No real buffer attached
    CaptureBuffersForEOS.physical_address = 0;                   // No real buffer attached
    CaptureBuffersForEOS.size             = 0;                   // 0 means EOS End Of Stream
    CaptureBuffersForEOS.user_private     = INVALID_INDEX;       // It's a fake buffer
    CaptureBuffersForEOS.presentation_time = INVALID_TIME;
    CaptureDevice->QueueBuffer(&CaptureBuffersForEOS);
}

ManifestorStatus_t  Manifestor_VideoGrab_c::HandleMarkerFrame(Buffer_t MarkerFrameBuffer)
{
    PlayerSequenceNumber_t *SequenceNumberStructure = GetSequenceNumberStructure(MarkerFrameBuffer);
    MarkerFrame_t           MarkerFrame = SequenceNumberStructure->mMarkerFrame;

    SE_DEBUG(GetGroupTrace(), "Stream 0x%p %s received Marker (type %d) #%lld\n", Stream, Configuration.ManifestorName, MarkerFrame.mMarkerType, MarkerFrame.mSequenceNumber);

    switch (MarkerFrame.mMarkerType)
    {
    case EosMarker:
        SE_INFO(GetGroupTrace(), "Stream 0x%p %s received EOS Marker #%lld\n", Stream, Configuration.ManifestorName, MarkerFrame.mSequenceNumber);
        HandleEndOfStream();
        break;

    default:
        break;
    }

    mOutputPort->Insert((uintptr_t) MarkerFrameBuffer);

    return ManifestorNoError;
}


