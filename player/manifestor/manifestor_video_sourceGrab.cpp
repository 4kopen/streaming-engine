/************************************************************************
Copyright (C) 2003-2014 STMicroelectronics. All Rights Reserved.

This file is part of the Streaming Engine.

Streaming Engine is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License version 2 as
published by the Free Software Foundation.

Streaming Engine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with Streaming Engine; see the file COPYING.  If not, write to the Free
Software Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307,
USA.

The Streaming Engine Library may alternatively be licensed under a
proprietary license from ST.
************************************************************************/

#include "osinline.h"
#include "video_decoder_params.h"
#include "manifestor_video_sourceGrab.h"

#undef TRACE_TAG
#define TRACE_TAG   "Manifestor_VideoSrc_c"

///{{{ Constructor
/// \brief                      Initialise state
/// \return                     Success or fail
Manifestor_VideoSrc_c::Manifestor_VideoSrc_c()
    : InputWindow()
    , VideoMetadataHelper()
    , ManifestationComponent(PrimaryManifestationComponent)
    , BlitterHandle(NULL)
    , BlitterSrce()
    , BlitterDest()

{
    if (InitializationStatus != ManifestorNoError)
    {
        SE_ERROR("Initialization status not valid - aborting init\n");
        return;
    }

    SE_VERBOSE(group_manifestor_video_grab, "\n");

    SetGroupTrace(group_manifestor_video_grab);

    // Supply some defaults
    SurfaceDescriptor.StreamType                = StreamTypeVideo;
    SurfaceDescriptor.ClockPullingAvailable     = false;
    SurfaceDescriptor.DisplayWidth              = FRAME_GRAB_DEFAULT_DISPLAY_WIDTH;
    SurfaceDescriptor.DisplayHeight             = FRAME_GRAB_DEFAULT_DISPLAY_HEIGHT;
    SurfaceDescriptor.Progressive               = true;
    SurfaceDescriptor.FrameRate                 = FRAME_GRAB_DEFAULT_FRAME_RATE; // rational
    SurfaceDescriptor.PercussiveCapable         = true;
    // But request that we set output to match input rates
    SurfaceDescriptor.InheritRateAndTypeFromSource = true;
}
//}}}

///{{{ Destructor
/// \brief                      Release video Source resources
/// \return                     Success or fail
Manifestor_VideoSrc_c::~Manifestor_VideoSrc_c()
{
    SE_VERBOSE(group_manifestor_video_grab, "\n");
}

//}}}
//{{{  PrepareToPull
//{{{  doxynote
/// \brief              Prepare the frame in the capture Buffer and determine the grabbed frame size
/// \param              Buffer containing frame pulled by memorySink
//}}}
void Manifestor_VideoSrc_c::PrepareToPull(class Buffer_c                      *Buffer,
                                          stm_se_capture_buffer_t             *CaptureBuffer,
                                          struct ManifestationOutputTiming_s  *VideoOutputTiming)
{
    struct ParsedVideoParameters_s     *VideoParameters;
    struct ParsedFrameParameters_s     *ParsedFrameParameters;

    SE_VERBOSE(group_manifestor_video_grab, "\n");

    Buffer->ObtainMetaDataReference(Player->MetaDataParsedVideoParametersType, (void **)&VideoParameters);
    SE_ASSERT(VideoParameters != NULL);

    Buffer->ObtainMetaDataReference(Player->MetaDataParsedFrameParametersReferenceType, (void **)(&ParsedFrameParameters));
    SE_ASSERT(ParsedFrameParameters != NULL);

    //
    // Update OutputSurfaceDescriptor
    //
    if (VideoParameters->Content.FrameRate != 0)
    {
        SurfaceDescriptor.DisplayWidth          = VideoParameters->Content.DisplayWidth;
        SurfaceDescriptor.DisplayHeight         = VideoParameters->Content.DisplayHeight;
        SurfaceDescriptor.Progressive           = VideoParameters->Content.Progressive;
        SurfaceDescriptor.FrameRate             = VideoParameters->Content.FrameRate;
    }
    else
    {
        SurfaceDescriptor.DisplayWidth          = FRAME_GRAB_DEFAULT_DISPLAY_WIDTH;
        SurfaceDescriptor.DisplayHeight         = FRAME_GRAB_DEFAULT_DISPLAY_HEIGHT;
        SurfaceDescriptor.Progressive           = true;
        SurfaceDescriptor.FrameRate             = FRAME_GRAB_DEFAULT_FRAME_RATE; // rational
    }

    SelectManifestationComponent(Buffer, VideoParameters);

    //
    // CaptureBuffer structure provides the size, physical address of the destination buffer
    //
    FramePhysicalAddress                    = (void *) Stream->GetDecodeBufferManager()->ComponentBaseAddress(Buffer, ManifestationComponent, PhysicalAddress);

    DetermineFrameSize(Buffer);

    //
    // Meta Data setup
    //
    CaptureBuffer->u.uncompressed.system_time              = VideoOutputTiming->SystemPlaybackTime;
    CaptureBuffer->u.uncompressed.native_time_format       = TIME_FORMAT_US;
    CaptureBuffer->u.uncompressed.native_time              = ParsedFrameParameters->PTS.uSecValue();
    setUncompressedMetadata(Buffer, &CaptureBuffer->u.uncompressed, VideoParameters);
}
//}}}

///{{{ PullFrameRead
/// \brief                      Prepare captureBuffer to be returned to memsink_pull
/// \param captureBufferAddr    Kernel Address of buffer provided by memsink
/// \return                     Number of bytes copied, (positive or null value)
///                             Linux kernel error otherwise (negative value)
int32_t        Manifestor_VideoSrc_c::PullFrameRead(uint8_t *captureBufferAddr)
{
    struct SourceStreamBuffer_s        *StreamBuff      = NULL;
    stm_se_capture_buffer_t            *CaptureBuffer   = (stm_se_capture_buffer_t *)captureBufferAddr;
    uint32_t                            bufferIndex;

    AssertComponentState(ComponentRunning);
    SE_DEBUG(group_manifestor_video_grab, "Input window onto decode buffer  captureBufferAddr %p\n",
             captureBufferAddr);

    // Get StreamBuffer of the CurrentBuffer thanks to its index
    CurrentBuffer->GetIndex(&bufferIndex);

    if (bufferIndex >= MAX_DECODE_BUFFERS)
    {
        SE_ERROR("invalid buffer index %d\n", bufferIndex);
        return -EINVAL;
    }

    // retrieve StreamBuffer via its index
    StreamBuff   = &SourceStreamBuffer[bufferIndex];

    // Prepare frame pulled by memorySink
    PrepareToPull(CurrentBuffer, CaptureBuffer, StreamBuff->OutputTiming);

    // Copy Video buffer into provided buffer
    // First check buffer large enough
    if (CaptureBuffer->buffer_length < FrameSize)
    {
        SE_ERROR("Video Capture buffer too small: expected %d got %d bytes\n", FrameSize, CaptureBuffer->buffer_length);
        return -EINVAL;
    }

    // prepare the source surface
    if (PrepareSrceSurfaceToBlit(CurrentBuffer, CaptureBuffer) != ManifestorNoError)
    {
        return -EINVAL;
    }

    // prepare the destination surface
    if (PrepareDestSurfaceToBlit(CaptureBuffer) != ManifestorNoError)
    {
        return -EINVAL;
    }

    // Apply the blit
    if (ApplyBlit() != ManifestorNoError)
    {
        return -EINVAL;
    }

    // Update the real copied frame size
    CaptureBuffer->payload_length = FrameSize;
    return CaptureBuffer->payload_length;
}
//}}}

///{{{ initialiseConnection
/// \brief                      Perform after connection actions
/// \return                     Success or fail
ManifestorStatus_t  Manifestor_VideoSrc_c::initialiseConnection()
{
    //
    // do specific Video initialisation
    //
    int blitter_id = ModuleParameter_BlitterId();
    BlitterHandle = stm_blitter_get(blitter_id);

    if (BlitterHandle == NULL)
    {
        return ManifestorError;
    }

    //
    // perform general initialisation
    //
    return Manifestor_Source_c::initialiseConnection();
}
///{{{ terminateConnection
/// \brief                      Perform before connection actions
/// \return                     Success or fail
ManifestorStatus_t  Manifestor_VideoSrc_c::terminateConnection()
{
    //
    // do specific Video initialisation
    //
    if (BlitterHandle)
    {
        stm_blitter_put(BlitterHandle);
    }

    BlitterHandle = NULL;

    if (BlitterSrce.surface)
    {
        stm_blitter_surface_put(BlitterSrce.surface);
    }

    BlitterSrce.surface = NULL;

    if (BlitterDest.surface)
    {
        stm_blitter_surface_put(BlitterDest.surface);
    }

    BlitterDest.surface = NULL;
    //
    // perform general termination
    //
    return Manifestor_Source_c::terminateConnection();
}


///{{{ GetSurfaceParameters
/// \brief                      Return Video surface descriptor
/// \return                     Success or fail
ManifestorStatus_t      Manifestor_VideoSrc_c::GetSurfaceParameters(OutputSurfaceDescriptor_t   **SurfaceParameters, unsigned int *NumSurfaces)
{
    SE_VERBOSE(group_manifestor_video_grab, "\n");
    *SurfaceParameters  = &SurfaceDescriptor;
    *NumSurfaces        = 1;
    return ManifestorNoError;
}
//}}}

//}}}
//{{{  PrepareSrceSurfaceToBlit
//{{{  doxynote
/// \brief              Prepare the source surface to blit
/// \param              Video frame captured structure
/// \result             Success if blitter surface allocated
ManifestorStatus_t      Manifestor_VideoSrc_c::PrepareSrceSurfaceToBlit(class Buffer_c          *Buffer,
                                                                        stm_se_capture_buffer_t *capture_buffer)
{
    if (BlitterSrce.surface)
    {
        stm_blitter_surface_put(BlitterSrce.surface);
    }

    BlitterSrce.surface = NULL;
    // the whole picture has to be grabbed.
    BlitterSrce.rect.position.x = 0;
    BlitterSrce.rect.position.y = 0;
    //
    // Will need the chroma buffer offset
    //
    unsigned int chromaBufferOffset = Stream->GetDecodeBufferManager()->Chroma(Buffer, ManifestationComponent) -
                                      Stream->GetDecodeBufferManager()->Luma(Buffer, ManifestationComponent);

    //
    // Determine Buffer_format and pixel_depth of the manifestation component
    //
    switch (Stream->GetDecodeBufferManager()->ComponentDataType(ManifestationComponent))
    {
    case FormatVideo420_MacroBlock:
        BlitterSrce.format = STM_BLITTER_SF_YCBCR420MB;
        BlitterSrce.buffer_add.cbcr_offset = chromaBufferOffset;
        break;

    case FormatVideo420_PairedMacroBlock:
        BlitterSrce.format = STM_BLITTER_SF_YCBCR420MB;
        BlitterSrce.buffer_add.cbcr_offset = chromaBufferOffset;
        break;

    case FormatVideo8888_ARGB:
        BlitterSrce.format = STM_BLITTER_SF_ARGB;
        break;

    case FormatVideo888_RGB:
        BlitterSrce.format = STM_BLITTER_SF_RGB24;
        break;

    case FormatVideo565_RGB:
        BlitterSrce.format = STM_BLITTER_SF_RGB565;
        break;

    case FormatVideo422_Raster:
        BlitterSrce.format = STM_BLITTER_SF_UYVY;
        break;

    case FormatVideo420_PlanarAligned:
    case FormatVideo420_Planar:
        BlitterSrce.format = STM_BLITTER_SF_I420;
        BlitterSrce.buffer_add.cb_offset = chromaBufferOffset;  /* FIXME */
        BlitterSrce.buffer_add.cr_offset = chromaBufferOffset;  /* FIXME */
        break;

    case FormatVideo422_Planar:
        BlitterSrce.format = STM_BLITTER_SF_YV61;
        BlitterSrce.buffer_add.cb_offset = chromaBufferOffset;  /* FIXME */
        BlitterSrce.buffer_add.cr_offset = chromaBufferOffset;  /* FIXME */
        break;

    case FormatVideo422_YUYV:
        BlitterSrce.format = STM_BLITTER_SF_YUY2;
        break;

    case FormatVideo420_Raster2B:
        BlitterSrce.format = STM_BLITTER_SF_NV12;
        BlitterSrce.buffer_add.cbcr_offset = chromaBufferOffset;
        break;

    case FormatVideo420_Raster2B_10B:
        BlitterSrce.format = STM_BLITTER_SF_NV12_10B;
        BlitterSrce.buffer_add.cbcr_offset = chromaBufferOffset;
        break;

    default:
        SE_ERROR("Unsupported display format (%d)\n", Stream->GetDecodeBufferManager()->ComponentDataType(ManifestationComponent));
        return ManifestorError;
        break;
    }

    BlitterSrce.rect.size.w        = capture_buffer->u.uncompressed.video.video_parameters.width;
    BlitterSrce.rect.size.h        = capture_buffer->u.uncompressed.video.video_parameters.height;
    BlitterSrce.buffer_dimension.w = capture_buffer->u.uncompressed.video.video_parameters.width;
    BlitterSrce.buffer_dimension.h = capture_buffer->u.uncompressed.video.video_parameters.height;
    BlitterSrce.pitch              = capture_buffer->u.uncompressed.video.pitch;
    BlitterSrce.buffer_size        = Stream->GetDecodeBufferManager()->ComponentSize(Buffer, ManifestationComponent);
    BlitterSrce.buffer_add.base = (long unsigned int) FramePhysicalAddress;

    //
    // Determine the colour Space
    //

    switch (capture_buffer->u.uncompressed.video.video_parameters.colorspace)
    {
    case STM_SE_COLORSPACE_SMPTE170M:
        BlitterSrce.cspace = STM_BLITTER_SCS_BT601;
        break;

    case STM_SE_COLORSPACE_SMPTE240M:
        BlitterSrce.cspace = STM_BLITTER_SCS_BT709;
        break;

    case STM_SE_COLORSPACE_BT709:
        BlitterSrce.cspace = STM_BLITTER_SCS_BT709;
        break;

    case STM_SE_COLORSPACE_BT2020:
        BlitterSrce.cspace = STM_BLITTER_SCS_BT2020;
        break;

    case STM_SE_COLORSPACE_BT470_SYSTEM_M:
        BlitterSrce.cspace = STM_BLITTER_SCS_BT601;
        break;

    case STM_SE_COLORSPACE_BT470_SYSTEM_BG:
        BlitterSrce.cspace = STM_BLITTER_SCS_BT601;
        break;

    case STM_SE_COLORSPACE_SRGB:
        BlitterSrce.cspace = STM_BLITTER_SCS_RGB;
        break;

    default:
        BlitterSrce.cspace = (BlitterSrce.format & STM_BLITTER_SF_YCBCR) ?
                             (capture_buffer->u.uncompressed.video.video_parameters.width > SD_WIDTH ?
                              STM_BLITTER_SCS_BT601 : STM_BLITTER_SCS_BT709) : STM_BLITTER_SCS_RGB;
        break;
    }


    if ((BlitterSrce.format != STM_BLITTER_SF_NV12_10B))
    {
        if (BlitterSrce.format == STM_BLITTER_SF_YCBCR420MB)
        {
            BlitterSrce.format                 = STM_BLITTER_SF_A8;
            BlitterSrce.cspace                 = STM_BLITTER_SCS_RGB;
            // 4:2:0 buffer size is aligned to 512. Let's choose 512 as optimized rectangle height.
            BlitterSrce.buffer_dimension.h     = 512;
            BlitterSrce.buffer_dimension.w     = BlitterSrce.buffer_size / BlitterSrce.buffer_dimension.h;
            BlitterSrce.pitch                  = BlitterSrce.buffer_dimension.w;
            BlitterSrce.rect.size.w            = BlitterSrce.buffer_dimension.w;
            BlitterSrce.rect.size.h            = BlitterSrce.buffer_dimension.h;
        }
        else
        {
            BlitterSrce.format                 = STM_BLITTER_SF_ARGB;
            BlitterSrce.cspace                 = STM_BLITTER_SCS_RGB;
            BlitterSrce.buffer_dimension.w     = BlitterSrce.buffer_size / (BlitterSrce.buffer_dimension.h * ARGB8888_BYTES_PER_PIXEL);
            BlitterSrce.pitch                  = BlitterSrce.buffer_dimension.w * ARGB8888_BYTES_PER_PIXEL;
            BlitterSrce.rect.size.w            = BlitterSrce.buffer_dimension.w;
            BlitterSrce.rect.size.h            = BlitterSrce.buffer_dimension.h;
        }
    }

    BlitterSrce.surface = stm_blitter_surface_new_preallocated(
                              BlitterSrce.format,
                              BlitterSrce.cspace,
                              &BlitterSrce.buffer_add,
                              BlitterSrce.buffer_size,
                              &BlitterSrce.buffer_dimension,
                              BlitterSrce.pitch);

    if (IS_ERR(BlitterSrce.surface))
    {
        SE_ERROR("couldn't create source surface: %ld\n", PTR_ERR(BlitterSrce.surface));
        BlitterSrce.surface = NULL;
        return ManifestorError;
    }

    return ManifestorNoError;
}
//}}}

//{{{  PrepareDestSurfaceToBlit
//{{{  doxynote
/// \brief              Prepare the destination surface to bit
/// \param
/// \result             Success if frame displayed, fail if not displayed or a frame has already been displayed
ManifestorStatus_t      Manifestor_VideoSrc_c::PrepareDestSurfaceToBlit(stm_se_capture_buffer_t   *capture_buffer)
{
    if (BlitterDest.surface)
    {
        stm_blitter_surface_put(BlitterDest.surface);
    }

    BlitterDest.surface                = NULL;
    BlitterDest.buffer_add             = BlitterSrce.buffer_add;
    BlitterDest.buffer_add.cbcr_offset = BlitterSrce.buffer_add.cbcr_offset;
    BlitterDest.buffer_add.cb_offset   = BlitterSrce.buffer_add.cb_offset;
    BlitterDest.buffer_add.cr_offset   = BlitterSrce.buffer_add.cr_offset;
    BlitterDest.format                 = BlitterSrce.format;
    BlitterDest.cspace                 = BlitterSrce.cspace;
    BlitterDest.buffer_dimension       = BlitterSrce.buffer_dimension;
    BlitterDest.pitch                  = BlitterSrce.pitch;
    BlitterDest.rect                   = BlitterSrce.rect;

    Specific10bitsConvertion(capture_buffer);

    //
    // Destination buffer address is specified in the captureBuffer struct
    //
    BlitterDest.buffer_add.base      = (unsigned long)(capture_buffer->physical_address);
    BlitterDest.buffer_size          = static_cast<unsigned long>(capture_buffer->buffer_length);
    BlitterDest.surface = stm_blitter_surface_new_preallocated(
                              BlitterDest.format,
                              BlitterDest.cspace,
                              &BlitterDest.buffer_add,
                              BlitterDest.buffer_size,
                              &BlitterDest.buffer_dimension,
                              BlitterDest.pitch);

    if (IS_ERR(BlitterDest.surface))
    {
        SE_ERROR("couldn't create destination surface: %ld\n",
                 PTR_ERR(BlitterDest.surface));
        BlitterDest.surface = NULL;
        return ManifestorError;
    }

    return ManifestorNoError;
}
//{{{  ApplyBlit
//{{{  doxynote
/// \brief              Blit the Source surface to destination surface
/// \result             Success if blit done
ManifestorStatus_t      Manifestor_VideoSrc_c::ApplyBlit()
{
    ManifestorStatus_t  status = ManifestorNoError;
    int res;
    stm_blitter_surface_add_fence(BlitterDest.surface);

    /* set blit flags - the source is _always_ in fixed point */
    int blitStatus = stm_blitter_surface_set_blitflags(BlitterDest.surface,
                                                       (stm_blitter_surface_blitflags_t)(STM_BLITTER_SBF_NONE));
    if (blitStatus < 0)
    {
        SE_ERROR("Stream 0x%p Failed to set blit flags (%d)\n", Stream, blitStatus);
        stm_blitter_surface_put(BlitterDest.surface);
        BlitterDest.surface = NULL;
        return ManifestorError;
    }

    // stm_blitter_surface_set_porter_duff(BlitterDest.surface, STM_BLITTER_PD_SOURCE);
    stm_blitter_point_t dst_point = { 0, 0 };
    res = stm_blitter_surface_blit(
              BlitterHandle,
              BlitterSrce.surface,
              &BlitterSrce.rect,
              BlitterDest.surface,
              &dst_point, 1);

    if (!res)
    {
        // success - wait for operation to finish
        stm_blitter_serial_t serial;
        stm_blitter_surface_get_serial(BlitterDest.surface,  &serial);
        stm_blitter_wait(BlitterHandle, STM_BLITTER_WAIT_FENCE, serial);
    }
    else
    {
        status = ManifestorError;
        SE_ERROR("Error during blitter operation (%d)\n", res);
    }

    stm_blitter_surface_put(BlitterDest.surface);
    BlitterDest.surface = NULL;
    return status;
}
//}}}

//{{{  SelectManifestationComponent
//{{{  doxynote
/// \brief              Depending if size of the frame is greater than HD size,
///                     select the decimated or primary Manifestation Component
/// \param              Buffer containing frame pulled by memorySink
/// \param              VideoParameters of the frame
void      Manifestor_VideoSrc_c::SelectManifestationComponent(class Buffer_c  *Buffer, struct ParsedVideoParameters_s     *VideoParameters)
{
    bool DeblockOn = (PolicyValueDecimateDecoderOutputH1V1 == Player->PolicyValue(Playback, Stream, PolicyDecimateDecoderOutput));

    // Switch to decimated frame if exists and if resolution is > HD or mpeg2 deblocked output is requested.
    if ((((VideoParameters->Content.DisplayWidth > MAXIMUM_HD_PICTURE_WIDTH) || (VideoParameters->Content.DisplayHeight > MAXIMUM_HD_PICTURE_WIDTH)) &&
         (Stream->GetDecodeBufferManager()->ComponentPresent(Buffer, DecimatedManifestationComponent))) || DeblockOn)
    {
        ManifestationComponent = DecimatedManifestationComponent;
    }
    else
    {
        ManifestationComponent = PrimaryManifestationComponent;
    }
}
//}}}

//{{{  Specific10bitsConvertion
//{{{  doxynote
/// \brief Due to BDISP restrictions, BT2020 10 bit grabbing use case
///        is initially planned to be forbidden in Cannes2.5
///        The BT2020 10bit source is pre-converted into 8bitBT709
/// \param   capture_buffer structure to be updated in case of BT202010bits
void      Manifestor_VideoSrc_c::Specific10bitsConvertion(stm_se_capture_buffer_t   *capture_buffer)
{
    // In case src format is 10bits, change it to 8bits
    // Only NV12_10B format is supported
    if (BlitterSrce.format == STM_BLITTER_SF_NV12_10B)
    {
        BlitterDest.format = STM_BLITTER_SF_NV12;
        BlitterDest.pitch = (BlitterDest.pitch * 8) / 10;
        BlitterDest.buffer_size = (BlitterDest.buffer_size * 8) / 10;
        BlitterDest.buffer_add.cbcr_offset = (BlitterDest.buffer_add.cbcr_offset * 8) / 10;

        //
        // Update capture structure accordingly
        //
        capture_buffer->u.uncompressed.video.surface_format = SURFACE_FORMAT_VIDEO_420_RASTER2B;
        capture_buffer->u.uncompressed.video.pitch          = BlitterDest.pitch;
    }

    // In case src colorspace is BT2020 change it to BT709
    // Use of the approximated BT2020->709 gamut conversion performed by BDSIP
    if (BlitterSrce.cspace == STM_BLITTER_SCS_BT2020)
    {
        BlitterDest.cspace = STM_BLITTER_SCS_BT709;
        //
        // Update capture structure accordingly
        //
        capture_buffer->u.uncompressed.video.video_parameters.colorspace = STM_SE_COLORSPACE_BT709;
    }
}
//}}}

//{{{  DetermineFrameSize
//{{{  doxynote
/// \brief In case decoded frame is in 10bits format, grabbed frame will be converted to 8 bits
///        and its size must be re-computed. Otherwise, the FrameSize if the ComponentSize.
/// \param              Buffer containing decoded frame
void      Manifestor_VideoSrc_c::DetermineFrameSize(class Buffer_c     *Buffer)
{
    FrameSize = Stream->GetDecodeBufferManager()->ComponentSize(Buffer, ManifestationComponent);

    if (Stream->GetDecodeBufferManager()->ComponentDataType(ManifestationComponent) == FormatVideo420_Raster2B_10B)
    {
        // Grabbed framesize will be on 8bits instead of 10bits
        FrameSize = (FrameSize * 8) / 10;
    }
}
//}}}

