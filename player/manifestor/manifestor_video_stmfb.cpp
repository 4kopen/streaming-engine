/************************************************************************
Copyright (C) 2003-2014 STMicroelectronics. All Rights Reserved.

This file is part of the Streaming Engine.

Streaming Engine is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License version 2 as
published by the Free Software Foundation.

Streaming Engine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with Streaming Engine; see the file COPYING.  If not, write to the Free
Software Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307,
USA.

The Streaming Engine Library may alternatively be licensed under a
proprietary license from ST.
************************************************************************/

#include "osinline.h"
#include "player_generic.h"
#include "player_playback.h"
#include "player_stream.h"
#include "manifestor_video_stmfb.h"
#include "post_manifest_edge.h"
#include "timestamps.h"
#include "vector.h"

#undef TRACE_TAG
#define TRACE_TAG   "Manifestor_VideoStmfb_c"

#define PIXEL_DEPTH                     16
#define PROPAGATION_TIME_US           1000

extern "C" int snprintf(char *buf, size_t size, const char *fmt, ...) __attribute__((format(printf, 3, 4))) ;

struct QueueRecord_s
{
    unsigned int        Flags;
    unsigned int        Count;
};

static void source_queue_listener(uint32_t event, void *Stream);

static void display_callback(void           *Buffer,
                             stm_time64_t    VsyncTime,
                             uint16_t        output_change,
                             uint16_t        nb_output,
                             stm_display_latency_params_t *display_latency_params);

static void done_callback(void           *Buffer,
                          const stm_buffer_presentation_stats_t *Data);

Manifestor_VideoStmfb_c::Manifestor_VideoStmfb_c()
    : DisplayDevice(NULL)
    , Source(NULL)
    , QueueInterface(NULL)
    , DisplayPlaneMutex()
    , SurfaceChangedMutex()
    , NumPlanes(0)
    , DisplayPlanes()
    , mMarkerFrameVectorMutex()
    , TopologyChanged(true)
    , DisplayModeChanged(true)
    , mDisplayDiscontinuity(true)
    , LastSourceFrameRate(0)
    , DisplayBuffer()
    , ClockRateAdjustment()
    , mDisplayCallbackEvent()
{
    SE_DEBUG(group_manifestor_video_stmfb, "Stream 0x%p this 0x%p\n", Stream, this);

    if (InitializationStatus != ManifestorNoError)
    {
        SE_ERROR("Stream 0x%p this 0x%p Initialization status not valid - aborting init\n", Stream, this);
        return;
    }

    SetGroupTrace(group_manifestor_video_stmfb);

    Configuration.Capabilities  = MANIFESTOR_CAPABILITY_DISPLAY;

    for (int i = 0; i < MAXIMUM_NUMBER_OF_TIMINGS_PER_MANIFESTATION; i++)
    {
        SurfaceDescriptor[i].StreamType                   = StreamTypeVideo;
        SurfaceDescriptor[i].ClockPullingAvailable        = true;
        SurfaceDescriptor[i].MasterCapable                = true;
        SurfaceDescriptor[i].InheritRateAndTypeFromSource = false;
        SurfaceDescriptor[i].PercussiveCapable            = true;
        SurfaceDescriptor[i].OutputType                   = SURFACE_OUTPUT_TYPE_VIDEO_NO_OUTPUT;
        SurfaceDescriptor[i].IsSlavedSurface              = false;
        SurfaceDescriptor[i].MasterSurface                = NULL;
        SurfaceDescriptor[i].FrameRate                    = 0;
    }

    OS_InitializeMutex(&mMarkerFrameVectorMutex);
    OS_InitializeMutex(&DisplayPlaneMutex);
    OS_InitializeMutex(&SurfaceChangedMutex);
    OS_InitializeEvent(&mDisplayCallbackEvent);

    NumberOfTimings = 0;  // TODO(S.L.D) fixme can this be removed: belong to base class (init with 1)
}

Manifestor_VideoStmfb_c::~Manifestor_VideoStmfb_c()
{
    SE_DEBUG(group_manifestor_video_stmfb, ">Stream 0x%p this 0x%p\n", Stream, this);

    if (TestComponentState(ComponentRunning))
    {
        Halt();
    }

    if (Source != NULL)
    {
        CloseOutputSurface();
    }

    OS_TerminateMutex(&mMarkerFrameVectorMutex);
    OS_TerminateMutex(&DisplayPlaneMutex);
    OS_TerminateMutex(&SurfaceChangedMutex);
    OS_TerminateEvent(&mDisplayCallbackEvent);

    SE_DEBUG(group_manifestor_video_stmfb, "<Stream 0x%p this 0x%p\n", Stream, this);
}

void Manifestor_VideoStmfb_c::CheckAllBuffersReturned()
{
    for (int i = 0; i < MAX_DECODE_BUFFERS; i++)
    {
        if (StreamBuffer[i].BufferState != BufferStateAvailable)
        {
            SE_ERROR("Stream 0x%p this 0x%p Buffer #%d (state %d, QueueCount %d) not returned by Vibe!\n",
                     Stream, this, i, StreamBuffer[i].BufferState, StreamBuffer[i].QueueCount);
        }
    }
}

//  Halt
//  doxynote
/// \brief              Shutdown, stop presenting and retrieving frames
///                     don't return until complete
//
ManifestorStatus_t      Manifestor_VideoStmfb_c::Halt()
{
    SE_DEBUG(group_manifestor_video_stmfb, "Stream 0x%p this 0x%p\n", Stream, this);

    if (QueueInterface != NULL)
    {
        SE_DEBUG(group_manifestor_video_stmfb, "Stream 0x%p this 0x%p >stm_display_source_queue_flush\n", Stream, this);
        if (stm_display_source_queue_flush(QueueInterface, true) < 0)
        {
            SE_ERROR("Stream 0x%p this 0x%p Not able to flush display source queue\n", Stream, this);
        }

        SE_DEBUG(group_manifestor_video_stmfb, "Stream 0x%p this 0x%p <stm_display_source_queue_flush\n", Stream, this);

        CheckAllBuffersReturned();

        // all buffers returned so no more frame on display
        PtsOnDisplay    = INVALID_TIME;
    }

    return Manifestor_Video_c::Halt();
}

ManifestorStatus_t Manifestor_VideoStmfb_c::UpdateDisplayMode(OutputSurfaceDescriptor_t *SurfaceDescriptorPtr, unsigned int PlaneIndex)
{
    int Result = stm_display_output_get_current_display_mode(DisplayPlanes[PlaneIndex].OutputHandle, &DisplayPlanes[PlaneIndex].CurrentMode);
    if (Result != 0)
    {
        SE_ERROR("Stream 0x%p this 0x%p Failed to get display mode (%d)\n", Stream, this, Result);
        return ManifestorError;
    }

    SurfaceDescriptorPtr->DisplayWidth = DisplayPlanes[PlaneIndex].CurrentMode.mode_params.active_area_width;

    // The active area for 480p is actually 483 lines, but by default
    // we don't want to scale up 480 content to that odd number.
    if (DisplayPlanes[PlaneIndex].CurrentMode.mode_params.active_area_height == 483)
    {
        SurfaceDescriptorPtr->DisplayHeight       = 480;
    }
    else
    {
        SurfaceDescriptorPtr->DisplayHeight       = DisplayPlanes[PlaneIndex].CurrentMode.mode_params.active_area_height;
    }

    if (!SurfaceDescriptorPtr->InheritRateAndTypeFromSource || SurfaceDescriptorPtr->FrameRate == 0)
    {
        SurfaceDescriptorPtr->FrameRate     = Rational_c::ConvertDisplayFramerateToRational(DisplayPlanes[PlaneIndex].CurrentMode.mode_params.vertical_refresh_rate);
        SurfaceDescriptorPtr->Progressive   = (DisplayPlanes[PlaneIndex].CurrentMode.mode_params.scan_type == STM_PROGRESSIVE_SCAN);
    }

    return ManifestorNoError;
}

bool Manifestor_VideoStmfb_c::isMainPlane(int PlaneIndex)
{
    stm_plane_capabilities_t    caps;
    int Result = stm_display_plane_get_capabilities(DisplayPlanes[PlaneIndex].PlaneHandle, &caps);
    SE_ASSERT(Result == 0);
    return ((caps & kMainPlaneCapabilities) == kMainPlaneCapabilities);
}

bool Manifestor_VideoStmfb_c::isPipPlane(int PlaneIndex)
{
    stm_plane_capabilities_t    caps;
    int Result = stm_display_plane_get_capabilities(DisplayPlanes[PlaneIndex].PlaneHandle, &caps);
    SE_ASSERT(Result == 0);
    return (caps == kPipPlaneCapabilities);
}

bool Manifestor_VideoStmfb_c::isAuxPlane(int PlaneIndex)
{
    stm_plane_capabilities_t    caps;
    int Result = stm_display_plane_get_capabilities(DisplayPlanes[PlaneIndex].PlaneHandle, &caps);
    SE_ASSERT(Result == 0);
    return ((caps & kAuxPlaneCapabilities) == kAuxPlaneCapabilities);
}

void Manifestor_VideoStmfb_c::PrintSurfaceDescriptors()
{
    for (unsigned int i = 0; i < NumberOfTimings; i++)
    {
        SE_INFO(group_manifestor_video_stmfb, "Stream 0x%p this 0x%p SurfaceDescriptor #%d: Width %d, height %d, FrameRate %d.%06d, Progressive %d"
                " OutputType=%d PercussiveCapable=%d InheritFrameRate=%d ClockPulling=%d\n",
                Stream, this, i, SurfaceDescriptor[i].DisplayWidth, SurfaceDescriptor[i].DisplayHeight,
                SurfaceDescriptor[i].FrameRate.IntegerPart(), SurfaceDescriptor[i].FrameRate.RemainderDecimal(),
                SurfaceDescriptor[i].Progressive, SurfaceDescriptor[i].OutputType, SurfaceDescriptor[i].PercussiveCapable,
                SurfaceDescriptor[i].InheritRateAndTypeFromSource, SurfaceDescriptor[i].ClockPullingAvailable);
    }
}

//--------------------------------------------------------------------------------------------------------------------------------
//                          Cannes Case: Single clock
//--------------------------------------------------------------------------------------------------------------------------------

ManifestorStatus_t Manifestor_VideoStmfb_c::UpdatePlaneSurfaceDescriptor(int PlaneIndex)
{
    ManifestorStatus_t Status = UpdateDisplayMode(&SurfaceDescriptor[PlaneIndex + 1], PlaneIndex);
    if (Status != ManifestorNoError) { return Status; }

    OutputSurfaceDescriptor_t *PlaneSurface         = &SurfaceDescriptor[PlaneIndex + 1];

    PlaneSurface->ClockPullingAvailable             = true;
    PlaneSurface->InheritRateAndTypeFromSource      = false;
    PlaneSurface->IsSlavedSurface                   = true;
    PlaneSurface->MasterSurface                     = &SurfaceDescriptor[SOURCE_INDEX];
    PlaneSurface->PercussiveCapable                 = true;

    if (isMainPlane(PlaneIndex) == true)
    {
        PlaneSurface->OutputType             = SURFACE_OUTPUT_TYPE_VIDEO_MAIN;
        PlaneSurface->MaxClockAdjustment     = 5000;    // +/- 5000 ppm on HDMI as specified in CEA-861-F p 18
        PlaneSurface->MaxClockAdjustmentStep = 5;      // +/- 5 ppm in one step (conservative value)
    }
    else if (isPipPlane(PlaneIndex) == true)
    {
        PlaneSurface->ClockPullingAvailable  = false;   // no adjustment on PiP as already adjusted by Main
        PlaneSurface->OutputType             = SURFACE_OUTPUT_TYPE_VIDEO_PIP;
    }
    else if (isAuxPlane(PlaneIndex) == true)
    {
        PlaneSurface->OutputType             = SURFACE_OUTPUT_TYPE_VIDEO_AUX;
        PlaneSurface->MaxClockAdjustment     = 20;      // on AUX we support only +/-20 ppm
        PlaneSurface->MaxClockAdjustmentStep = 5;
    }
    else
    {
        PlaneSurface->OutputType             = SURFACE_OUTPUT_TYPE_VIDEO_NO_OUTPUT;
    }
    SE_INFO(group_manifestor_video_stmfb, "Stream 0x%p this 0x%p PlaneSurface %p Discovered surface with OutputType %d\n",
            Stream, this, PlaneSurface, PlaneSurface->OutputType);

    return ManifestorNoError;
}

void Manifestor_VideoStmfb_c::UpdateDisplaySourceSurfaceDescriptor()
{
    SurfaceDescriptor[SOURCE_INDEX].ClockPullingAvailable           = false;
    SurfaceDescriptor[SOURCE_INDEX].MasterCapable                   = false;
    SurfaceDescriptor[SOURCE_INDEX].InheritRateAndTypeFromSource    = false;
    SurfaceDescriptor[SOURCE_INDEX].IsSlavedSurface                 = false;
    SurfaceDescriptor[SOURCE_INDEX].PercussiveCapable               = false;
    SurfaceDescriptor[SOURCE_INDEX].OutputType                      = SURFACE_OUTPUT_TYPE_VIDEO_NO_OUTPUT;
}

ManifestorStatus_t Manifestor_VideoStmfb_c::FillDisplayPlaneSurfaceDescriptors()
{
    for (int i = 0; i < NumPlanes; i++)
    {
        UpdatePlaneSurfaceDescriptor(i);
    }

    NumberOfTimings = NumPlanes + 1;
    SE_ASSERT(NumberOfTimings <= MAXIMUM_NUMBER_OF_TIMINGS_PER_MANIFESTATION);

    return ManifestorNoError;
}

ManifestorStatus_t Manifestor_VideoStmfb_c::FillDisplaySourceSurfaceDescriptor()
{
    UpdateDisplaySourceSurfaceDescriptor();

    if (NumPlanes > 0)
    {
        ManifestorStatus_t Status = UpdateDisplayMode(&SurfaceDescriptor[SOURCE_INDEX], 0);
        if (Status != ManifestorNoError) { return Status; }

        SurfaceDescriptor[SOURCE_INDEX].InheritRateAndTypeFromSource = false;
    }
    else
    {
        // No display frame rate so only solution is to disable FRC
        SurfaceDescriptor[SOURCE_INDEX].InheritRateAndTypeFromSource = true;
    }

    return ManifestorNoError;
}

ManifestorStatus_t Manifestor_VideoStmfb_c::FillDisplaySurfaceDescriptors()
{
    ManifestorStatus_t Status = FillDisplayPlaneSurfaceDescriptors();
    if (Status != ManifestorNoError) { return Status; }

    Status = FillDisplaySourceSurfaceDescriptor();
    if (Status != ManifestorNoError) { return Status; }

    return ManifestorNoError;
}

//--------------------------------------------------------------------------------------------------------------------------------

//
//  UpdateOutputSurfaceDescriptor
// This method is called before queuing each buffer.
// When it is called, the timing records have already been filled by the output_timer
//  doxynote
/// \brief      Find out information about display plane and output
/// \return     ManifestorNoError if done successfully
//
ManifestorStatus_t Manifestor_VideoStmfb_c::UpdateOutputSurfaceDescriptor()
{
    bool topologyChanged, displayModeChanged, sourceFrameRateChanged;
    OS_LockMutex(&SurfaceChangedMutex);
    topologyChanged     = TopologyChanged;
    displayModeChanged  = DisplayModeChanged;
    TopologyChanged     = false;
    DisplayModeChanged  = false;
    OS_UnLockMutex(&SurfaceChangedMutex);
    sourceFrameRateChanged  = SurfaceDescriptor[SOURCE_INDEX].FrameRate != LastSourceFrameRate;
    LastSourceFrameRate     = SurfaceDescriptor[SOURCE_INDEX].FrameRate;

    if (topologyChanged)
    {
        SE_INFO(group_manifestor_video_stmfb, "Stream 0x%p this 0x%p Display Topology changed!\n", Stream, this);
    }

    if (sourceFrameRateChanged)
    {
        SE_INFO(group_manifestor_video_stmfb, "Stream 0x%p this 0x%p Source Frame Rate changed!\n", Stream, this);
    }

    if (topologyChanged)
    {
        OS_LockMutex(&DisplayPlaneMutex);

        CloseDisplayPlaneHandles();

        ManifestorStatus_t Status = OpenDisplayPlaneHandles();
        if (Status != ManifestorNoError)
        {
            OS_UnLockMutex(&DisplayPlaneMutex);
            return Status;
        }

        OS_UnLockMutex(&DisplayPlaneMutex);
    }

    if (topologyChanged || displayModeChanged || sourceFrameRateChanged)
    {
        FillDisplaySurfaceDescriptors();

        if (displayModeChanged)
        {
            char rate[16];
            char rate_decimalpart[4];

            snprintf(rate_decimalpart, sizeof(rate_decimalpart), ".%02d", SurfaceDescriptor[SOURCE_INDEX].FrameRate.RemainderDecimal() * 10000);
            snprintf(rate, sizeof(rate), "%d%s"
                     , SurfaceDescriptor[SOURCE_INDEX].FrameRate.IntegerPart()
                     , SurfaceDescriptor[SOURCE_INDEX].FrameRate.RemainderDecimal() != 0 ? rate_decimalpart : "");

            SE_INFO2(group_manifestor_video_stmfb, group_se_pipeline, "Stream 0x%p this 0x%p Display Mode changed : %dx%d-%s%s\n"
                     , Stream
                     , this
                     , SurfaceDescriptor[SOURCE_INDEX].DisplayWidth
                     , SurfaceDescriptor[SOURCE_INDEX].DisplayHeight
                     , rate
                     , SurfaceDescriptor[SOURCE_INDEX].Progressive ? "" : "i");
        }

        PrintSurfaceDescriptors();
    }

    return ManifestorNoError;
}

ManifestorStatus_t Manifestor_VideoStmfb_c::OpenDisplaySourceDevice(stm_object_h    DisplayHandle)
{
    int Result;
    stm_display_source_h                    source;
    stm_display_source_interface_params_t   InterfaceParams;
    unsigned int                            DeviceId;
    SE_DEBUG(group_manifestor_video_stmfb, "\n");
    source = (stm_display_source_h)DisplayHandle;
    InterfaceParams.interface_type  = STM_SOURCE_QUEUE_IFACE;

    Result = stm_display_source_get_interface(source, InterfaceParams, (void **)&QueueInterface);
    if (Result != 0)
    {
        SE_ERROR("Stream 0x%p this 0x%p Failed to get queue interface from source 0x%p (%d)\n", Stream, this, source, Result);
        return ManifestorError;
    }

    Result = stm_display_source_get_device_id(source, &DeviceId);
    if (Result != 0)
    {
        SE_ERROR("Stream 0x%p this 0x%p Failed to get device id (%d)\n", Stream, this, Result);
        return ManifestorError;
    }

    Result = stm_display_open_device(DeviceId, &DisplayDevice);
    if (Result != 0)
    {
        SE_ERROR("Stream 0x%p this 0x%p Failed to open device (%d)\n", Stream, this, Result);
        return ManifestorError;
    }

    // Lock this queue for our exclusive usage
    Result = stm_display_source_queue_lock(QueueInterface);
    if (Result != 0)
    {
        SE_ERROR("Stream 0x%p this 0x%p Queue interface could not be locked (%d)\n", Stream, this, Result);
        return ManifestorError;
    }

    // register listener callback for DE
    Result = stm_display_source_queue_set_listener(QueueInterface, &source_queue_listener, this);
    if (Result != 0)
    {
        SE_ERROR("Stream 0x%p this 0x%p Queue interface could not register a listener (%d)\n", Stream, this, Result);
        return ManifestorError;
    }

    Source = source;
    return ManifestorNoError;
}

void Manifestor_VideoStmfb_c::CloseDisplaySourceDevice()
{
    SE_DEBUG(group_manifestor_video_stmfb, "Stream 0x%p this 0x%p Source 0x%p, QueueInterface 0x%p\n", Stream, this, Source, QueueInterface);

    if (QueueInterface != NULL)
    {
        int Result = stm_display_source_queue_unlock(QueueInterface);
        if (Result != 0)
        {
            SE_ERROR("Stream 0x%p this 0x%p Cannot unlock queue interface from source (%d)\n", Stream, this, Result);
        }

        Result = stm_display_source_queue_release(QueueInterface);
        if (Result != 0)
        {
            SE_ERROR("Stream 0x%p this 0x%p Source is not connected to queue interface (%d)\n", Stream, this, Result);
        }

        QueueInterface = NULL;
    }

    if (DisplayDevice != NULL)
    {
        stm_display_device_close(DisplayDevice);
        DisplayDevice = NULL;
    }

    Source = NULL;
}

//
//  OpenOutputSurface
//  doxynote
/// \brief      Use opaque handle to access queue interface
/// \param      DisplayHandle Handle of the stmfb source object.
/// \return     ManifestorNoError if queue interface available
//
ManifestorStatus_t Manifestor_VideoStmfb_c::OpenOutputSurface(stm_object_h    DisplayHandle)
{
    SE_DEBUG(group_manifestor_video_stmfb, "Stream 0x%p this 0x%p\n", Stream, this);

    ManifestorStatus_t Status = OpenDisplaySourceDevice(DisplayHandle);
    if (Status != ManifestorNoError) { return Status; }

    mIsVisible = false;

    Status = UpdateOutputSurfaceDescriptor();
    if (Status != ManifestorNoError)
    {
        SE_ERROR("Stream 0x%p this 0x%p Not able to update OutputSurface\n", Stream, this);
        return Status;
    }

    // reset variables so that the display planes
    // are re-opened on first buffer
    TopologyChanged = true;
    DisplayModeChanged = true;

    return ManifestorNoError;
}

//
//  CloseOutputSurface
//  doxynote
/// \brief              Release all frame buffer resources
//
ManifestorStatus_t Manifestor_VideoStmfb_c::CloseOutputSurface()
{
    SE_DEBUG(group_manifestor_video_stmfb, "Stream 0x%p this 0x%p\n", Stream, this);
    CloseDisplayPlaneHandles();
    CloseDisplaySourceDevice();
    mIsVisible = false;
    return ManifestorNoError;
}

ManifestorStatus_t Manifestor_VideoStmfb_c::OpenDisplayPlaneHandles()
{
    int Count, Result;
    unsigned int PlaneIds[PLANE_MAX];

    SE_DEBUG(group_manifestor_video_stmfb, "Stream 0x%p this 0x%p\n", Stream, this);

    // we assume a 1:1 mapping of DisplayPlanes[].PlaneHandle to DisplayPlanes[].OutputHandle which is currently true
    NumPlanes = stm_display_source_get_connected_plane_id(Source, PlaneIds, PLANE_MAX);
    if (NumPlanes < 0)
    {
        SE_ERROR("Stream 0x%p this 0x%p Failed to get connected plane id (%d)\n", Stream, this, NumPlanes);
        return ManifestorError;
    }

    SE_ASSERT(NumPlanes <= PLANE_MAX);
    SE_INFO(group_manifestor_video_stmfb, "Stream 0x%p this 0x%p %d Planes connected to source 0x%p\n", Stream, this, NumPlanes, Source);

    for (int i = 0; i < NumPlanes; i++)
    {
        DisplayPlanes[i].PlaneId = PlaneIds[i];
        Result = stm_display_device_open_plane(DisplayDevice, DisplayPlanes[i].PlaneId, &DisplayPlanes[i].PlaneHandle);
        if (Result != 0)
        {
            SE_ERROR("Stream 0x%p this 0x%p Failed to get plane %x (%d)\n", Stream, this, DisplayPlanes[i].PlaneId, Result);
            goto open_display_planes_error;
        }

        Count = stm_display_plane_get_connected_output_id(DisplayPlanes[i].PlaneHandle, &DisplayPlanes[i].OutputId, 1);
        if (Count != 1)
        {
            SE_ERROR("Stream 0x%p this 0x%p Failed to get connected output or too many outputs on plane (%d)\n", Stream, this, Count);
            goto open_display_planes_error;
        }

        Result = stm_display_device_open_output(DisplayDevice, DisplayPlanes[i].OutputId, &DisplayPlanes[i].OutputHandle);
        if (Result != 0)
        {
            SE_ERROR("Stream 0x%p this 0x%p Failed to open OutputId[%d] (%d)\n", Stream, this, i, Result);
            goto open_display_planes_error;
        }

        SE_INFO(group_manifestor_video_stmfb, "Stream 0x%p this 0x%p Source 0x%p DisplayDevice 0x%p PlaneId %d Plane 0x%p OutputId %d Output 0x%p\n",
                Stream, this, Source, DisplayDevice, DisplayPlanes[i].PlaneId, DisplayPlanes[i].PlaneHandle, DisplayPlanes[i].OutputId, DisplayPlanes[i].OutputHandle);
    }

    return ManifestorNoError;

open_display_planes_error:
    CloseDisplayPlaneHandles();
    return ManifestorError;
}

void Manifestor_VideoStmfb_c::CloseDisplayPlaneHandles()
{
    SE_DEBUG(group_manifestor_video_stmfb, "Stream 0x%p this 0x%p %d Planes\n", Stream, this, NumPlanes);

    for (int i = 0; i < NumPlanes; i++)
    {
        if (DisplayPlanes[i].PlaneHandle != NULL)
        {
            stm_display_plane_close(DisplayPlanes[i].PlaneHandle);
            DisplayPlanes[i].PlaneHandle = NULL;
        }

        if (DisplayPlanes[i].OutputHandle != NULL)
        {
            stm_display_output_close(DisplayPlanes[i].OutputHandle);
            DisplayPlanes[i].OutputHandle = NULL;
        }
    }

    NumPlanes = 0;
}

//  Some shared functions used for queuing buffers
void Manifestor_VideoStmfb_c::SelectDisplaySource(Buffer_t                    Buffer,
                                                  stm_display_buffer_t       *DisplayBuff)
{
    /////////// Temporary code to retrieve the full picture width /////////////
    struct ParsedVideoParameters_s *VideoParameters;
    Buffer->ObtainMetaDataReference(Player->MetaDataParsedVideoParametersType, (void **)&VideoParameters);
    SE_ASSERT(VideoParameters != NULL);

    DisplayBuff->src.visible_area.x = VideoParameters->Content.DisplayX;
    DisplayBuff->src.visible_area.y = VideoParameters->Content.DisplayY;
    DisplayBuff->src.visible_area.width = VideoParameters->Content.DisplayWidth;
    DisplayBuff->src.visible_area.height = VideoParameters->Content.DisplayHeight;

    // Set HDR properties
    DisplayBuff->src.hdr_fmt = VideoParameters->Content.VideoHdrInfo;

    // Need for DisplayBuff->src.pan_and_scan_rect calculation based on Width/DisplayWidth & Height/DisplayHeight to set
    // area of interest for aspect ratio conversion performed by STMFB
    //////////////////////////////////////////////////////////////////////////
    DisplayBuff->src.primary_picture.width = Stream->GetDecodeBufferManager()->ComponentDimension(Buffer, PrimaryManifestationComponent, 0);
    DisplayBuff->src.primary_picture.height = Stream->GetDecodeBufferManager()->ComponentDimension(Buffer, PrimaryManifestationComponent, 1);

    // Use decimated frame for deblocked output from mpeg2 decode
    bool DeblockOn = (PolicyValueDecimateDecoderOutputH1V1 == Player->PolicyValue(Playback, Stream, PolicyDecimateDecoderOutput));
    DecodeBufferComponentType_t DisplayComponent;

    if (DeblockOn)
    {
        DisplayComponent = DecimatedManifestationComponent;
    }
    else
    {
        DisplayComponent = PrimaryManifestationComponent;
    }

    switch (Stream->GetDecodeBufferManager()->ComponentDataType(DisplayComponent))
    {
    case FormatVideo420_PairedMacroBlock:
    case FormatVideo420_MacroBlock:
        DisplayBuff->src.primary_picture.color_fmt         = SURF_YCBCR420MB;
        DisplayBuff->src.primary_picture.pixel_depth       = 8;
        break;

    case FormatVideo420_Raster2B:
        DisplayBuff->src.primary_picture.color_fmt         = SURF_YCbCr420R2B;
        DisplayBuff->src.primary_picture.pixel_depth       = 8;
        break;

    case FormatVideo8888_ARGB:
        DisplayBuff->src.primary_picture.color_fmt         = SURF_ARGB8888;
        DisplayBuff->src.primary_picture.pixel_depth       = 32;
        DisplayBuff->src.flags           |= STM_BUFFER_SRC_LIMITED_RANGE_ALPHA;
        break;

    case FormatVideo888_RGB:
        DisplayBuff->src.primary_picture.color_fmt         = SURF_RGB888;
        DisplayBuff->src.primary_picture.pixel_depth       = 24;
        break;

    case FormatVideo565_RGB:
        DisplayBuff->src.primary_picture.color_fmt         = SURF_RGB565;
        DisplayBuff->src.primary_picture.pixel_depth       = 16;
        break;

    case FormatVideo422_Raster:
        DisplayBuff->src.primary_picture.color_fmt         = SURF_YCBCR422R;
        DisplayBuff->src.primary_picture.pixel_depth       = 16;
        break;

    case FormatVideo420_PlanarAligned:
    case FormatVideo420_Planar:
        DisplayBuff->src.primary_picture.color_fmt         = SURF_YUV420;
        DisplayBuff->src.primary_picture.pixel_depth       = 8;
        break;

    case FormatVideo422_Planar:
        DisplayBuff->src.primary_picture.color_fmt         = SURF_YUV422P;
        DisplayBuff->src.primary_picture.pixel_depth       = 8;
        break;

    case FormatVideo422_YUYV:
        DisplayBuff->src.primary_picture.color_fmt         = SURF_YUYV;
        DisplayBuff->src.primary_picture.pixel_depth       = 16;
        break;

    case FormatVideo422_Raster2B:
        DisplayBuff->src.primary_picture.color_fmt         = SURF_YCbCr422R2B;
        DisplayBuff->src.primary_picture.pixel_depth       = 8;
        break;

    case FormatVideo444_Raster:
        DisplayBuff->src.primary_picture.color_fmt         = SURF_CRYCB888;
        DisplayBuff->src.primary_picture.pixel_depth       = 24;
        break;

    case FormatVideo420_Raster2B_10B:
        DisplayBuff->src.primary_picture.color_fmt         = SURF_YCbCr420R2B10;
        DisplayBuff->src.primary_picture.pixel_depth       = 10;
        break;

    default:
        SE_ERROR("Stream 0x%p this 0x%p Unsupported display format (%d)\n",
                 Stream, this, Stream->GetDecodeBufferManager()->ComponentDataType(PrimaryManifestationComponent));
        break;
    }

    if (Stream->GetDecodeBufferManager()->ComponentPresent(Buffer, DecimatedManifestationComponent))
    {
        DisplayBuff->src.secondary_picture.width = Stream->GetDecodeBufferManager()->ComponentDimension(Buffer, DecimatedManifestationComponent, 0);
        DisplayBuff->src.secondary_picture.height = Stream->GetDecodeBufferManager()->ComponentDimension(Buffer, DecimatedManifestationComponent, 1);

        // Default: decimated component is YUV420 Raster2B for 8/10 depths whatever PrimaryManifestationComponent
        DisplayBuff->src.secondary_picture.color_fmt   = SURF_YCbCr420R2B;
        DisplayBuff->src.secondary_picture.pixel_depth = 8;
        //For Cannes Wifi/L2A, decimated output is kept to 8 bits (no support of 10 bit display for decimated output)
#ifndef HEVC_HADES_CANNESWIFI
        if (DisplayBuff->src.primary_picture.pixel_depth == 10)
        {
            DisplayBuff->src.secondary_picture.color_fmt   = SURF_YCbCr420R2B10;
            DisplayBuff->src.secondary_picture.pixel_depth = 10;
        }
#endif
    }
}

static int SelectColourMatrixCoefficients(struct ParsedVideoParameters_s *VideoParameters)
{
    unsigned int Flags = 0;

    switch (VideoParameters->Content.ColourMatrixCoefficients)
    {
    case MatrixCoefficients_ITU_R_BT601:
    case MatrixCoefficients_ITU_R_BT470_2_M:
    case MatrixCoefficients_ITU_R_BT470_2_BG:
    case MatrixCoefficients_SMPTE_170M:
    case MatrixCoefficients_FCC:
        // Do nothing, use 601 coefficients
        break;

    case MatrixCoefficients_ITU_R_BT709:
    case MatrixCoefficients_SMPTE_240M:
        // Use 709 coefficients
        Flags                          |= STM_BUFFER_SRC_COLORSPACE_709;
        break;

    case MatrixCoefficients_ITU_R_BT2020:
        // Use 2020 coefficients
        Flags                          |= STM_BUFFER_SRC_COLORSPACE_BT2020;
        break;

    case MatrixCoefficients_Undefined:
    default:

        // Base coefficients on display size SD=601, HD = 709
        if (VideoParameters->Content.Width > 720)
        {
            Flags                      |= STM_BUFFER_SRC_COLORSPACE_709;
        }

        break;
    }

    return Flags;
}

void Manifestor_VideoStmfb_c::SelectDisplayBufferPointers(Buffer_t                Buffer,
                                                          stm_display_buffer_t   *DisplayBuff)
{
    bool DeblockOn = (PolicyValueDecimateDecoderOutputH1V1 == Player->PolicyValue(Playback, Stream, PolicyDecimateDecoderOutput));
    DecodeBufferComponentType_t DisplayComponent;

    if (DeblockOn)
    {
        DisplayComponent = DecimatedManifestationComponent;
    }
    else
    {
        DisplayComponent = PrimaryManifestationComponent;
    }

    DisplayBuff->src.primary_picture.video_buffer_addr      = (uint32_t)Stream->GetDecodeBufferManager()->ComponentBaseAddress(Buffer, DisplayComponent, PhysicalAddress);
    DisplayBuff->src.primary_picture.video_buffer_size      = Stream->GetDecodeBufferManager()->ComponentSize(Buffer, DisplayComponent);
    DisplayBuff->src.primary_picture.chroma_buffer_offset   = Stream->GetDecodeBufferManager()->Chroma(Buffer, DisplayComponent) -
                                                              Stream->GetDecodeBufferManager()->Luma(Buffer, DisplayComponent);
    DisplayBuff->src.primary_picture.pitch                  = Stream->GetDecodeBufferManager()->ComponentStride(Buffer, DisplayComponent, 0, 0);
    DisplayBuff->src.horizontal_decimation_factor           = STM_NO_DECIMATION;
    DisplayBuff->src.vertical_decimation_factor             = STM_NO_DECIMATION;

    if (Stream->GetDecodeBufferManager()->ComponentPresent(Buffer, DecimatedManifestationComponent))
    {
        DisplayBuff->src.secondary_picture.video_buffer_addr      = (uint32_t)Stream->GetDecodeBufferManager()->ComponentBaseAddress(Buffer, DecimatedManifestationComponent, PhysicalAddress);
        DisplayBuff->src.secondary_picture.video_buffer_size      = Stream->GetDecodeBufferManager()->ComponentSize(Buffer, DecimatedManifestationComponent);
        DisplayBuff->src.secondary_picture.chroma_buffer_offset   = Stream->GetDecodeBufferManager()->Chroma(Buffer, DecimatedManifestationComponent) -
                                                                    Stream->GetDecodeBufferManager()->Luma(Buffer, DecimatedManifestationComponent);
        DisplayBuff->src.secondary_picture.pitch                  = Stream->GetDecodeBufferManager()->ComponentStride(Buffer, DecimatedManifestationComponent, 0, 0);

        //Set horizontal and vertical decimation factors for DisplayBuff
        SetDecimationFactorsForDisplay(Buffer, DisplayBuff);
    }
}

void Manifestor_VideoStmfb_c::ApplyPixelAspectRatioCorrection(stm_display_buffer_t       *DisplayBuff,
                                                              struct ParsedVideoParameters_s *VideoParameters)
{
    // get 32bits restricted numerator and denominator values
    VideoParameters->Content.PixelAspectRatio.GetRu32NumeratorDenominator(
        (uint32_t *)&DisplayBuff->src.pixel_aspect_ratio.numerator,
        (uint32_t *)&DisplayBuff->src.pixel_aspect_ratio.denominator,
        Rational_t::MATCH_ASPECTRATIO);

    DisplayBuff->src.linear_center_percentage       = 0;
}

ManifestorStatus_t Manifestor_VideoStmfb_c::SetOutputRateAdjustment(unsigned int TimingIndex, struct StreamBuffer_s *StreamBuff)
{
    int Adjustment, Status = 0;
    SE_PRECONDITION(StreamBuff != NULL);

    if (!SurfaceDescriptor[TimingIndex].ClockPullingAvailable) { return ManifestorNoError; }

    Adjustment = (StreamBuff->OutputTiming[TimingIndex]->OutputRateAdjustment * 1000000).IntegerPart() - 1000000;

    if (Adjustment != ClockRateAdjustment[TimingIndex])
    {
        if (TimingIndex == SOURCE_INDEX)
        {
            SE_DEBUG(group_manifestor_video_stmfb, "Stream 0x%p this 0x%p Setting DisplaySource clock adjustment to %d parts per million\n", Stream, this, Adjustment);
            Status = stm_display_source_set_control(Source, SOURCE_CTRL_CLOCK_ADJUSTMENT, Adjustment);
        }
        else
        {
            SE_DEBUG(group_manifestor_video_stmfb, "Stream 0x%p this 0x%p Setting DisplayOutput #%d clock adjustment to %d parts per million\n", Stream, this, TimingIndex - 1, Adjustment);
            Status = stm_display_output_set_control(DisplayPlanes[TimingIndex - 1].OutputHandle, OUTPUT_CTRL_CLOCK_ADJUSTMENT, Adjustment);
        }

        if (Status == 0)
        {
            ClockRateAdjustment[TimingIndex] = Adjustment;
        }
        else
        {
            SE_ERROR("Stream 0x%p this 0x%p Cannot set output rate adjustment (%d)\n", Stream, this, Status);
            SE_ERROR("Stream 0x%p this 0x%p Disabling ClockPulling for surface #%d\n", Stream, this, TimingIndex);
            SurfaceDescriptor[TimingIndex].ClockPullingAvailable = false;
        }
    }

    return ManifestorNoError;
}

void Manifestor_VideoStmfb_c::FillInDisplayBufferSrcFields(
    Buffer_t Buffer,
    struct ParsedVideoParameters_s *VideoParameters,
    stm_display_buffer_t *DisplayBuff)
{
    SelectDisplayBufferPointers(Buffer, DisplayBuff);

    ApplyPixelAspectRatioCorrection(DisplayBuff, VideoParameters);

    SelectDisplaySource(Buffer, DisplayBuff);

    // get 32bits restricted numerator and denominator values
    VideoParameters->Content.FrameRate.GetRu32NumeratorDenominator(
        (uint32_t *)&DisplayBuff->src.src_frame_rate.numerator,
        (uint32_t *)&DisplayBuff->src.src_frame_rate.denominator,
        Rational_t::MATCH_FRAMERATE);

    DisplayBuff->src.ulConstAlpha               = 0xff;

#ifdef PROPAGATE_3D_INFO_TO_DISPLAY
    // It has been commented out to avoid to pass 3D information to display engine
    // otherwise the display engine performs 3D to 2D conversion.
    // Ignoring 3D info, will let the display engine present the video 'as-it-is'
    // (SbS, TaB) then the TV set will do the conversion to 3D as per user
    // intervention.
    switch (VideoParameters->Content.Output3DVideoProperty.Stream3DFormat)
    {
    case Stream3DFormatFrameSeq :
        DisplayBuff->src.config_3D.formats = FORMAT_3D_FRAME_SEQ;
        DisplayBuff->src.config_3D.parameters.frame_seq = (VideoParameters->Content.Output3DVideoProperty.Frame0IsLeft) ?
                                                          FORMAT_3D_FRAME_SEQ_LEFT_FRAME : FORMAT_3D_FRAME_SEQ_RIGHT_FRAME;
        break;

    case Stream3DFormatStackedHalf :
        DisplayBuff->src.config_3D.formats = FORMAT_3D_STACKED_HALF;
        DisplayBuff->src.config_3D.parameters.frame_packed.is_left_right_format = VideoParameters->Content.Output3DVideoProperty.Frame0IsLeft;
        break;

    case Stream3DFormatSidebysideHalf :
        DisplayBuff->src.config_3D.formats = FORMAT_3D_SBS_HALF;
        DisplayBuff->src.config_3D.parameters.sbs.is_left_right_format = VideoParameters->Content.Output3DVideoProperty.Frame0IsLeft;
        break;

    default :
        DisplayBuff->src.config_3D.formats = FORMAT_3D_NONE;
        break;
    }
#else
    DisplayBuff->src.config_3D.formats = FORMAT_3D_NONE;
#endif

    DisplayBuff->src.flags  = STM_BUFFER_SRC_CONST_ALPHA;
    DisplayBuff->src.flags |= SelectColourMatrixCoefficients(VideoParameters);

    if (VideoParameters->InterlacedFrame)
    {
        DisplayBuff->src.flags |= STM_BUFFER_SRC_INTERLACED;
    }

    Rational_t          Speed;
    PlayDirection_t     Direction;
    Playback->GetSpeed(&Speed, &Direction);

    if (Speed < 1)
    {
        DisplayBuff->src.flags |= STM_BUFFER_SRC_INTERPOLATE_FIELDS;
    }

    // Display Discontinuity is propagated using Markers in multiple cases:
    // a new stream creation, on a stream switch (channel change use case)
    // on inject discontinuity (seek use case), drain, EOS, on some markers
    // received downstream.
    if (mDisplayDiscontinuity)
    {
        SE_DEBUG(group_manifestor_video_stmfb, "Stream 0x%p this 0x%p DisplayDiscontinuity\n", Stream, this);
        DisplayBuff->src.flags |= STM_BUFFER_SRC_TEMPORAL_DISCONTINUITY;
        mDisplayDiscontinuity = false;
    }

    // Inform vibe that there is a display discontinuity when we are in I only or IP decode mode.
    if (Player->PolicyValue(Playback, Stream, PolicyTrickModeDomain) == PolicyValueTrickModeDecodeKeyFrames
        || Player->PolicyValue(Playback, Stream, PolicyTrickModeDomain) == PolicyValueTrickModeDiscardNonReferenceFrames)
    {
        DisplayBuff->src.flags |= STM_BUFFER_SRC_TEMPORAL_DISCONTINUITY;
    }
}

void Manifestor_VideoStmfb_c::FillInDisplayBufferInfoFields(
    struct StreamBuffer_s *StreamBuff,
    struct ManifestationOutputTiming_s **VideoOutputTimingArray,
    stm_display_buffer_t *DisplayBuff)
{
    OS_Smp_Mb(); // TODO review all memory barriers in manifestor_stmfb

    DisplayBuff->info.ulFlags                   = STM_BUFFER_PRESENTATION_PERSISTENT;
    DisplayBuff->info.puser_data                = StreamBuff;
    DisplayBuff->info.PTS                       = StreamBuff->NativePlaybackTime;
    DisplayBuff->info.display_callback          = &display_callback;
    DisplayBuff->info.completed_callback        = &done_callback;
}

void Manifestor_VideoStmfb_c::FillInDisplayBuffer(
    Buffer_t Buffer,
    struct StreamBuffer_s *StreamBuff,
    struct ParsedVideoParameters_s *VideoParameters,
    struct ManifestationOutputTiming_s **VideoOutputTimingArray,
    stm_display_buffer_t *DisplayBuff)
{
    // Clear the DisplayBuff record
    memset((void *)DisplayBuff, 0, sizeof(stm_display_buffer_t));

    FillInDisplayBufferSrcFields(Buffer, VideoParameters, DisplayBuff);
    FillInDisplayBufferInfoFields(StreamBuff, VideoOutputTimingArray, DisplayBuff);
}

void Manifestor_VideoStmfb_c::ReportQueueBufferFailedEvent()
{
    struct PlayerEventRecord_s DisplayEvent;

    DisplayEvent.Code                           = EventFailedToQueueBufferToDisplay;
    DisplayEvent.Playback                       = Playback;
    DisplayEvent.Stream                         = Stream;
    DisplayEvent.PlaybackTime                   = OS_GetTimeInMicroSeconds();

    PlayerStatus_t Status = Stream->SignalEvent(&DisplayEvent);

    if (Status != PlayerNoError)
    {
        SE_ERROR("Stream 0x%p this 0x%p Failed to signal event\n", Stream, this);
    }
}

//  QueueBuffer
//  doxynote
/// \brief                      Actually put buffer on display
/// \param BufferIndex          Index into array of stream buffers
/// \param FrameParameters      Frame parameters generated by frame parser
/// \param VideoParameters      Display positioning information generated by frame parser
/// \param VideoOutputTiming    Display timing information generated by output timer
/// \return                     Success or fail
//
ManifestorStatus_t Manifestor_VideoStmfb_c::QueueBuffer(unsigned int                    BufferIndex,
                                                        struct ParsedFrameParameters_s *FrameParameters,
                                                        struct ParsedVideoParameters_s *VideoParameters,
                                                        struct ManifestationOutputTiming_s **VideoOutputTimingArray,
                                                        Buffer_t                        Buffer)
{
    struct StreamBuffer_s      *StreamBuff          = &StreamBuffer[BufferIndex];
    stm_display_buffer_t       *DisplayBuff         = &DisplayBuffer[BufferIndex];
    unsigned int                FieldFlags[2]       = {0, 0};
    bool                        is_first_frame      = false;
    ParsedFrameParameters_t    *ParsedFrameParameters;

    Rational_t          Speed;
    PlayDirection_t     Direction;
    Playback->GetSpeed(&Speed, &Direction);

    if (Speed > 1
        && (VideoOutputTimingArray[SOURCE_INDEX]->DisplayCount[0] == 0)
        && (VideoOutputTimingArray[SOURCE_INDEX]->DisplayCount[1] == 0))
    {
        return ManifestorNoError;
    }

    if (mNextQueuedFrameIsFirstFrame)
    {
        // mNextQueuedFrameIsFirstFrame is propagated using Markers in multiple cases:
        // on playstream attachment to a display, on a stream switch (channel change use case)
        // on drain(flush/playout) and EOS,
        mNextQueuedFrameIsFirstFrame = false;
        mFirstFrameOnDisplay         = true;
        is_first_frame               = true;
    }

    // Check if it is the first frame and
    // the frame is not dropped (at least one field to be displayed)
    if (is_first_frame &&
        (VideoOutputTimingArray[SOURCE_INDEX]->DisplayCount[0] + VideoOutputTimingArray[SOURCE_INDEX]->DisplayCount[1]))
    {
        SE_INFO(group_manifestor_video_stmfb, "Stream 0x%p Manifesting First Frame Early\n", Stream);

        // Force frame display early if policy PolicyManifestFirstFrameEarly enable
        if (Player->PolicyValue(Playback, Stream, PolicyManifestFirstFrameEarly) == PolicyValueApply)
        {
            // display only the top field as the frame will stay on display
            if (VideoOutputTimingArray[SOURCE_INDEX]->PictureStructure == StructureFrame)
            {
                if (VideoOutputTimingArray[SOURCE_INDEX]->TopFieldFirst)
                {
                    VideoOutputTimingArray[SOURCE_INDEX]->DisplayCount[1]     = 0;
                }
                else
                {
                    VideoOutputTimingArray[SOURCE_INDEX]->DisplayCount[0]     = 0;
                }
            }
            else
            {
                // if only one field present, display that field
                VideoOutputTimingArray[SOURCE_INDEX]->DisplayCount[1]     = 0;
            }
            // set expected presentation time to unspecified so that it is displayed asap
            for (unsigned int i = 0; i < NumberOfTimings; i++)
            {
                VideoOutputTimingArray[i]->SystemPlaybackTime  = UNSPECIFIED_TIME;
            }
        }
    }

    for (unsigned int i = 0; i < NumberOfTimings; i++)
    {
        ManifestorStatus_t Result = SetOutputRateAdjustment(i, StreamBuff);
        if (Result != ManifestorNoError) { return Result; }
    }

    StreamBuff->Manifestor = this;

    if (VideoOutputTimingArray[SOURCE_INDEX]->Interlaced)
    {
        if (VideoOutputTimingArray[SOURCE_INDEX]->PictureStructure == StructureFrame)
        {
            if (VideoOutputTimingArray[SOURCE_INDEX]->TopFieldFirst)
            {
                FieldFlags[0]    |= STM_BUFFER_SRC_TOP_FIELD_ONLY;
                FieldFlags[1]    |= STM_BUFFER_SRC_BOTTOM_FIELD_ONLY;
            }
            else
            {
                FieldFlags[0]    |= STM_BUFFER_SRC_BOTTOM_FIELD_ONLY;
                FieldFlags[1]    |= STM_BUFFER_SRC_TOP_FIELD_ONLY;
            }
        }
        else
        {
            // if only one field present, update the Field Flags for that field
            if (VideoOutputTimingArray[SOURCE_INDEX]->PictureStructure == StructureTopField)
            {
                FieldFlags[0]    |= STM_BUFFER_SRC_TOP_FIELD_ONLY;
            }
            else
            {
                FieldFlags[0]    |= STM_BUFFER_SRC_BOTTOM_FIELD_ONLY;
            }
        }
    }

    // Check if we must respect PICTURE polarity
    if (VideoOutputTimingArray[SOURCE_INDEX]->RespectPolarity)
    {
        // always set RESPECT_POLARITY flag on first field
        FieldFlags[0] |= STM_BUFFER_SRC_RESPECT_PICTURE_POLARITY;

        // set RESPECT_POLARITY flag on second field only if first field repeat count is odd
        if ((VideoOutputTimingArray[SOURCE_INDEX]->DisplayCount[0] % 2) == 1)
        {
            FieldFlags[1] |= STM_BUFFER_SRC_RESPECT_PICTURE_POLARITY;
        }
    }

    if (SE_IS_DEBUG_ON(group_se_pipeline) || SE_IS_DEBUG_ON(group_frc))
    {
        FRCPatternTrace(FrameParameters->DisplayFrameIndex
                        , VideoOutputTimingArray[SOURCE_INDEX]->DisplayCount[0]
                        , VideoOutputTimingArray[SOURCE_INDEX]->DisplayCount[1]
                        , VideoOutputTimingArray[SOURCE_INDEX]->TopFieldFirst
                       );
    }

    FillInDisplayBuffer(Buffer, StreamBuff, VideoParameters, VideoOutputTimingArray, DisplayBuff);

    long long Now = OS_GetTimeInMicroSeconds();

    if (DisplayBuff->info.presentation_time && DisplayBuff->info.presentation_time <= Now)
    {
        SE_WARNING("Stream 0x%p this 0x%p Queueing late buffer to Display #%d (DecodeFrameIndex %d DisplayFrameIndex %d PTS=%lld presentationTime %lld)\n",
                   Stream, this, BufferIndex, FrameParameters->DecodeFrameIndex, FrameParameters->DisplayFrameIndex,
                   StreamBuff->NativePlaybackTime, DisplayBuff->info.presentation_time);
    }

    unsigned int CommonFlags        = DisplayBuff->src.flags;
    unsigned int LocalQueueCount    = StreamBuff->QueueCount;

    // We shall not use DisplayCount but instead apply a proper timing to both fields in interlaced
    for (int i = 0; i < 2; i++)
    {
        // For speeds <= 2, skipped fields are sent to the display so that it can use them for the DEI
        // But do not send skipped field for Partial Frame (i.e. with only one field)
        if ((VideoOutputTimingArray[SOURCE_INDEX]->DisplayCount[i])
            || (Speed <= 2 && VideoOutputTimingArray[SOURCE_INDEX]->Interlaced && (VideoOutputTimingArray[SOURCE_INDEX]->PictureStructure == StructureFrame)))
        {
            DisplayBuff->src.flags      = CommonFlags | FieldFlags[i];

            DisplayBuff->info.presentation_time = ValidTime(VideoOutputTimingArray[SOURCE_INDEX]->SystemPlaybackTime) ?
                                                  VideoOutputTimingArray[SOURCE_INDEX]->SystemPlaybackTime : 0;

            if (Player->PolicyValue(Playback, Stream, PolicyAVDSynchronization) == PolicyValueApply)
            {
                if (VideoOutputTimingArray[SOURCE_INDEX]->DisplayCount[i] != 0)
                {
                    // repeat count will depend only on the presentation time of next frame/field
                    DisplayBuff->info.nfields   = 1;

                    if (VideoOutputTimingArray[SOURCE_INDEX]->Interlaced && i == 1)
                    {
                        // When the second field is to be displayed too far in the future
                        // (ie when  deltanow > MAXIMUM_SECOND_FIELD_LATENCY_US), the field count
                        // is reset to enhance the reactivity of speed changes.

                        long long FieldDurationTime = SurfaceDescriptor[SOURCE_INDEX].FrameRate != 0 ?
                                                      Rational_t(1000000 / SurfaceDescriptor[SOURCE_INDEX].FrameRate).LongLongIntegerPart() : 0;

                        SE_EXTRAVERB2(group_manifestor_video_stmfb, group_se_pipeline, "FieldDuration=%lld DisplayCount[0]=%d Window=%d\n"
                                      , FieldDurationTime, VideoOutputTimingArray[SOURCE_INDEX]->DisplayCount[0], MAXIMUM_SECOND_FIELD_LATENCY_US);

                        if (FieldDurationTime * VideoOutputTimingArray[SOURCE_INDEX]->DisplayCount[0] > MAXIMUM_SECOND_FIELD_LATENCY_US)
                        {
                            SE_VERBOSE2(group_manifestor_video_stmfb, group_se_pipeline, "NOT Displaying second field\n");
                            DisplayBuff->info.nfields   = 0;
                            DisplayBuff->info.presentation_time = 0;
                        }
                        else
                        {
                            DisplayBuff->info.presentation_time += FieldDurationTime * VideoOutputTimingArray[SOURCE_INDEX]->DisplayCount[0];
                        }
                    }
                }
                else
                {
                    DisplayBuff->info.nfields   = 0;
                }
            }
            else
            {
                DisplayBuff->info.nfields   =  VideoOutputTimingArray[SOURCE_INDEX]->DisplayCount[i];
            }

            // Save the Display Frame Index to get it in the display callback
            StreamBuff->BufferClass->ObtainMetaDataReference(
                Stream->GetPlayer()->MetaDataParsedFrameParametersReferenceType,
                (void **)(&ParsedFrameParameters));
            SE_ASSERT(ParsedFrameParameters != NULL);

            StreamBuff->DisplayFrameIndex = ParsedFrameParameters->DisplayFrameIndex;

            SE_DEBUG2(group_manifestor_video_stmfb, group_se_pipeline,
                      "Stream 0x%p this 0x%p Queueing buffer #%d to Vibe (DecodeFrameIndex %d DisplayFrameIndex %d flags 0x%x nfields %d PTS=%lld presentationTime %lld deltaNow %lld)\n",
                      Stream, this, BufferIndex, FrameParameters->DecodeFrameIndex, FrameParameters->DisplayFrameIndex, DisplayBuff->src.flags, DisplayBuff->info.nfields,
                      StreamBuff->NativePlaybackTime, DisplayBuff->info.presentation_time,
                      DisplayBuff->info.presentation_time ? DisplayBuff->info.presentation_time - Now : 0);

            StreamBuff->QueueCount++;

            if (DisplayBuff->info.nfields != 0)
            {
                StreamBuff->BufferState = BufferStateQueuedForDisplay;
            }
            else if (StreamBuff->BufferState == BufferStateNotQueued)
            {
                StreamBuff->BufferState = BufferStateQueued;
            }

            int Status = stm_display_source_queue_buffer(QueueInterface, DisplayBuff);

            if (Status != 0)
            {
                SE_ERROR("Stream 0x%p this 0x%p Failed to queue buffer %d to display\n", Stream, this, BufferIndex);
                StreamBuff->QueueCount = LocalQueueCount;
                ReportQueueBufferFailedEvent();
                return ManifestorUnplayable;
            }
        }

        if (DisplayBuff->info.nfields != 0)
        {
            DisplayBuff->info.display_callback      = NULL;
        }
    }

    return ManifestorNoError;
}

//  FlushDisplayQueue
//  doxynote
/// \brief      Flushes the display queue so buffers not yet manifested are returned
/// \param      ReleaseAllBuffers Indicates whether all buffers must be released
//
ManifestorStatus_t      Manifestor_VideoStmfb_c::FlushDisplayQueue(bool ReleaseAllBuffers)
{
    int status = 0;
    int LastFrameBehaviourPolicy = Player->PolicyValue(Playback, Stream, PolicyVideoLastFrameBehaviour);

    SE_DEBUG(group_manifestor_video_stmfb, "Stream 0x%p this 0x%p > %s ReleaseAllBuffers %d\n",
             Stream, this,
             (LastFrameBehaviourPolicy == PolicyValueLeaveLastFrameOnScreen) ? "LeaveLastFrameOnScreen" : "BlankScreen",
             ReleaseAllBuffers);

    Rational_t          Speed;
    PlayDirection_t     Direction;
    Playback->GetSpeed(&Speed, &Direction);

    if (ReleaseAllBuffers)
    {
        if (LastFrameBehaviourPolicy == PolicyValueLeaveLastFrameOnScreen)
        {
            // need to call flush_with_copy so that all frames are released while keeping last frame on screen
            status = stm_display_source_queue_flush_with_copy(QueueInterface);
            if (status < 0)
            {
                SE_ERROR("Stream 0x%p this 0x%p stm_display_source_queue_flush_with_copy() status %d\n", Stream, this, status);
            }
        }
        else
        {
            status = stm_display_source_queue_flush(QueueInterface, true);
            if (status < 0)
            {
                SE_ERROR("Stream 0x%p this 0x%p stm_display_source_queue_flush(true) status %d\n", Stream, this, status);
            }

            // all buffers returned so no more frame on display
            PtsOnDisplay = INVALID_TIME;
        }

        CheckAllBuffersReturned();
    }
    else
    {
        // Display is authorized to keep some referenced buffers
        status = stm_display_source_queue_flush(QueueInterface, false);
        if (status < 0)
        {
            SE_ERROR("Stream 0x%p this 0x%p stm_display_source_queue_flush(false) status %d\n", Stream, this, status);
        }
        // set mLastQueuedBufferIndex to invalid to avoid attaching marker frame
        // to a field that will never get displayed, leading to deadlock in drain
        mLastQueuedBufferIndex = INVALID_INDEX;
    }

    SE_DEBUG(group_manifestor_video_stmfb, "Stream 0x%p this 0x%p <\n", Stream, this);

    return ManifestorNoError;
}

int Manifestor_VideoStmfb_c::Enable()
{
    SE_DEBUG(group_manifestor_video_stmfb, "Stream 0x%p this 0x%p NumPlanes = %d\n", Stream, this, NumPlanes);

    int ret = 0;

    OS_LockMutex(&DisplayPlaneMutex);

    for (int i = 0; i < NumPlanes; i++)
    {
        // possible returned status: -EINVAL, -EAGAIN, -EDEADLK (obsolete) or -EALREADY
        // but -EDEADLK/-EALREADY status is set if did not changed state (eg requested same state)
        // so ignore it
        int planeret = stm_display_plane_show(DisplayPlanes[i].PlaneHandle);
        if (planeret != 0 && !(planeret == -EDEADLK || planeret == -EALREADY))
        {
            SE_DEBUG(group_manifestor_video_stmfb,
                     "stm_display_plane_show for plane[%d] %p failed with status:%d\n",
                     i, DisplayPlanes[i].PlaneHandle, planeret);
            ret = planeret;
        }
    }

    OS_UnLockMutex(&DisplayPlaneMutex);

    // mark visible if all planes show ok
    if (NumPlanes != 0 && ret == 0)
    {
        mIsVisible = true;
    }

    return ret;
}

int Manifestor_VideoStmfb_c::Disable()
{
    SE_DEBUG(group_manifestor_video_stmfb, "Stream 0x%p this 0x%p NumPlanes = %d\n", Stream, this, NumPlanes);

    int ret = 0;

    OS_LockMutex(&DisplayPlaneMutex);

    for (int i = 0; i < NumPlanes; i++)
    {
        // same comments as for plane_show
        int planeret = stm_display_plane_hide(DisplayPlanes[i].PlaneHandle);
        if (planeret != 0 && !(planeret == -EDEADLK || planeret == -EALREADY))
        {
            SE_DEBUG(group_manifestor_video_stmfb,
                     "stm_display_plane_hide for plane[%d] %p failed with status:%d\n",
                     i, DisplayPlanes[i].PlaneHandle, planeret);
            ret = planeret;
        }
    }

    OS_UnLockMutex(&DisplayPlaneMutex);

    // shall mark not visible if all planes hide ok
    if (NumPlanes != 0 && ret == 0)
    {
        mIsVisible = false;
    }

    return ret;
}

bool Manifestor_VideoStmfb_c::GetEnable(void)
{
    SE_EXTRAVERB(group_manifestor_video_stmfb, "\n");

    // don't rely on stm_display_plane_get_status, but use internal one
    // cf bz70338
    return mIsVisible;
}

//  GetNextManifestationTime
//  doxynote
//  \brief              Return the next earliest time at which next frame will be manifested
//  \param              void pointer to video Data parameter
long long   Manifestor_VideoStmfb_c::GetNextManifestationTime(void *ParsedAudioVideoDataParameters)
{
    uint32_t    DisplayTimingEvt = STM_TIMING_EVENT_NONE;
    long long   Now = OS_GetTimeInMicroSeconds();
    long long   Latency = GetManifestationLatency(ParsedAudioVideoDataParameters);
    long long   NextManifestationTime = Now + Latency;

    if (NumPlanes > 0)
    {
        long long   LastVsyncTime;
        stm_display_output_get_last_timing_event(DisplayPlanes[0].OutputHandle, &DisplayTimingEvt, &LastVsyncTime);

        int AlignOnTop = 0;

        if (DisplayTimingEvt != STM_TIMING_EVENT_NONE)
        {
            if (DisplayTimingEvt & STM_TIMING_EVENT_BOTTOM_FIELD)
            {
                // As the next top field is elected on a bottom vsync, if the
                // previous event was a bottom vsync, we are going to have to delay
                // the next manifestation time
                AlignOnTop = 1;
            }
            Rational_t OutputFrameRate = Rational_c::ConvertDisplayFramerateToRational(DisplayPlanes[0].CurrentMode.mode_params.vertical_refresh_rate);
            long long PeriodUsec       = Rational_t(1000000 / OutputFrameRate).RoundedLongLongIntegerPart();
            long long NextElectionVsyncTime    = LastVsyncTime + (1 + AlignOnTop) * PeriodUsec;
            // make sure that we will have the time to send our frame to DE before the next election vsync
            int Margin        = 0;
            // Will there be time to send this frame before next election vsync ?
            if (Now + PROPAGATION_TIME_US > NextElectionVsyncTime)
            {
                // ah, snap ! we are going to miss the next election vsync
                if (Now > NextElectionVsyncTime)
                {
                    if (Now < NextElectionVsyncTime + 1000)
                    {
                        // We are not lucky : we just retrieved next vsync time, it was VERY soon, and it's already too late...
                        SE_WARNING("Next election vsync time is just a few microseconds in the past ! Now=%lld NextElectionVsyncTime=%lld\n", Now, NextElectionVsyncTime);
                    }
                    else
                    {
                        // Should not happen !
                        SE_ERROR("Next election vsync time is in the past ! Now=%lld NextElectionVsyncTime=%lld\n", Now, NextElectionVsyncTime);
                    }
                }
                // we need to add 2 more vsync to meet the deadline
                Margin += 2;
            }

            // Next manifestation time is the date of (next election date) + 1 vsync (+ margin if any)
            NextManifestationTime      = NextElectionVsyncTime + PeriodUsec + Margin * PeriodUsec;

            SE_VERBOSE(group_se_pipeline, "DisplayTimingEvt=%d NextElectionVsyncTime=%lld deltaNow=%lld Margin=%d PeriodUsec=%lld\n", DisplayTimingEvt, NextElectionVsyncTime, NextElectionVsyncTime - Now, Margin,
                       PeriodUsec);
            if (NextManifestationTime  < Now + Latency)
            {
                SE_ERROR("Not enough margin when using NextManifestationTime=%lld Now=%lld Latency=%lld\n", NextManifestationTime, Now, Latency);
            }
        }
    }

    SE_DEBUG2(group_manifestor_video_stmfb, group_se_pipeline, "NextManifestationTime=%lld deltaNow=%lld\n"
              , NextManifestationTime, NextManifestationTime - Now);

    return NextManifestationTime;
}

unsigned long long Manifestor_VideoStmfb_c::GetManifestationLatency(void *ParsedAudioVideoDataParameters)
{
    (void)ParsedAudioVideoDataParameters; // warning removal
    if (NumPlanes > 0)
    {
        Rational_t OutputFrameRate = Rational_c::ConvertDisplayFramerateToRational(DisplayPlanes[0].CurrentMode.mode_params.vertical_refresh_rate);
        return Rational_t(1000000 / OutputFrameRate).RoundedLongLongIntegerPart();
    }
    else
    {
        // No plane connected yet so we need to assume worst case
        return Rational_t(1000000, 24).RoundedLongLongIntegerPart();
    }
}

int Manifestor_VideoStmfb_c::GetPlaneIndex(unsigned int PlaneId)
{
    for (int i = 0; i < NumPlanes; i++)
    {
        if (DisplayPlanes[i].PlaneId == PlaneId) { return i; }
    }

    return -1;
}

bool Manifestor_VideoStmfb_c::BuffersToDisplay()
{
    for (int i = 0; i < MAX_DECODE_BUFFERS; i++)
    {
        if (StreamBuffer[i].BufferState == BufferStateQueuedForDisplay)
        {
            SE_DEBUG(group_manifestor_video_stmfb, "Stream 0x%p this 0x%p Buffer %d queued to display\n", Stream, this, i);
            return true;
        }
    }

    return false;
}

ManifestorStatus_t Manifestor_VideoStmfb_c::WaitForFrozenSurface()
{
    SE_DEBUG(group_manifestor_video_stmfb, "Stream 0x%p this 0x%p\n", Stream, this);

    while (1)
    {
        OS_ResetEvent(&mDisplayCallbackEvent);

        if (BuffersToDisplay())
        {
            SE_DEBUG(group_manifestor_video_stmfb, "Stream 0x%p this 0x%p About to wait for display callback event\n", Stream, this);
            OS_Status_t Status = OS_WaitForEventAuto(&mDisplayCallbackEvent, PLAYER_LIMITED_EARLY_MANIFESTATION_WINDOW / 1000);
            if (Status == OS_TIMED_OUT)
            {
                SE_ERROR("Waiting for DisplayCallbackEvent timed out\n");
                return ManifestorError;
            }
        }
        else
        {
            return ManifestorNoError;
        }
    }
}

void Manifestor_VideoStmfb_c::ReportFirstFrameOnDisplayEvent()
{
    if (mFirstFrameOnDisplay)
    {
        stm_event_t streamEvtMngt;

        streamEvtMngt.event_id  = STM_SE_PLAY_STREAM_EVENT_FIRST_FRAME_ON_DISPLAY;
        streamEvtMngt.object    = (stm_object_h)Stream->GetHavanaStream();

        SE_INFO(group_manifestor_video_stmfb, "Stream 0x%p this 0x%p First frame on display event\n" , Stream, this);

        // TODO:vgi any reason why it is not using normal SignalEvent path ?
        int err = stm_event_signal(&streamEvtMngt);
        if (err) { SE_ERROR("Stream 0x%p this 0x%p stm_event_signal failed (%d)\n", Stream, this, err); }

        mFirstFrameOnDisplay = false;
    }
}

void Manifestor_VideoStmfb_c::ReportFrameRenderedEvent()
{
    stm_event_t streamEvtMngt;

    streamEvtMngt.event_id  = STM_SE_PLAY_STREAM_EVENT_FRAME_RENDERED;
    streamEvtMngt.object    = (stm_object_h)Stream->GetHavanaStream();

    SE_VERBOSE(group_player, "Signaling STM_SE_PLAY_STREAM_EVENT_FRAME_RENDERED\n");

    // TODO:vgi any reason why it is not using normal SignalEvent path ?
    int err = stm_event_signal(&streamEvtMngt);
    if (err) { SE_ERROR("Stream 0x%p this 0x%p stm_event_signal failed (%d)\n", Stream, this, err); }
}

void Manifestor_VideoStmfb_c::ReportDisplayEvents()
{
    ReportFirstFrameOnDisplayEvent();
    ReportFrameRenderedEvent();
}

void Manifestor_VideoStmfb_c::DisplayCallback(struct StreamBuffer_s  *Buffer,
                                              stm_time64_t            VsyncTime,
                                              uint16_t                OutputChangeFlags,
                                              uint16_t                NbOutput,
                                              stm_display_latency_params_t *DisplayLatency)
{
    // If the manifestor has been halted and/or reset then various state has been destroyed
    if (! TestComponentState(ComponentRunning) || (IsHalting == true))
    {
        return;
    }

    SE_ASSERT(Buffer != NULL);
    SE_ASSERT(Buffer->OutputTiming[SOURCE_INDEX] != NULL);

    Buffer->OutputTiming[SOURCE_INDEX]->ActualSystemPlaybackTime = VsyncTime;
    Buffer->BufferState = BufferStateDisplayed;

    for (int i = 0; i < (int) NbOutput; i++)
    {
        int PlaneIndex = GetPlaneIndex(DisplayLatency[i].plane_id);

        if (PlaneIndex < 0 || PlaneIndex + 1 >= (int) Buffer->NumberOfTimings) { continue; }

        if (DisplayLatency[i].output_latency_in_us != 0)
        {
            SE_ERROR("Stream 0x%p this 0x%p DisplayLatency[%d].output_latency_in_us is not 0: %u\n",
                     Stream, this, i, DisplayLatency[i].output_latency_in_us);
        }
        SE_ASSERT(Buffer->OutputTiming[PlaneIndex + 1] != NULL);
        Buffer->OutputTiming[PlaneIndex + 1]->ActualSystemPlaybackTime = VsyncTime;

        if ((PlaneIndex == 0) && ValidTime(Buffer->OutputTiming[1]->SystemPlaybackTime))
        {
            Stream->Statistics().SyncError = Buffer->OutputTiming[1]->SystemPlaybackTime - Buffer->OutputTiming[1]->ActualSystemPlaybackTime;
        }
    }

    Buffer->NbOutput             = NbOutput;
    BufferOnDisplay              = Buffer->BufferIndex;
    TimeStamp_c PlaybackTime(Buffer->NativePlaybackTime, TIME_FORMAT_PTS);
    PtsOnDisplay = TimeStamp_c::AddNativeOffset(PlaybackTime, -Buffer->PtsOffset).NativeValue();
    SE_DEBUG(group_manifestor_video_stmfb, "PlaybackTime %llu New playback time %llu offset %lld\n", PlaybackTime.PtsValue(), PtsOnDisplay, Buffer->PtsOffset);
    FrameCountManifested++;

    SE_DEBUG2(group_manifestor_video_stmfb, group_se_pipeline, "Stream 0x%p this 0x%p idx=%d PTS=%llu VsyncTime=%lld FrameCountManifested=%llu\n",
              Stream, this, BufferOnDisplay, PtsOnDisplay, VsyncTime, FrameCountManifested);

    // update playback info statistics
    Stream->Statistics().FrameCountManifested++;
    Stream->Statistics().Pts              = Buffer->NativePlaybackTime;
    Stream->Statistics().PresentationTime = VsyncTime;
    Stream->Statistics().SystemTime       = OS_GetTimeInMicroSeconds();

    if (IsMaster())
    {
        ReportDisplayEvents();
    }

    OS_SetEvent(&mDisplayCallbackEvent);

    PropagatePendingMarkerFrames(Buffer);
}

void Manifestor_VideoStmfb_c::SourceQueueListener(uint32_t event)
{
    OS_LockMutex(&SurfaceChangedMutex);

    TopologyChanged     = TopologyChanged    || (event & OUTPUT_CONNECTION_CHANGE);
    DisplayModeChanged  = DisplayModeChanged || (event & OUTPUT_DISPLAY_MODE_CHANGE);

    OS_UnLockMutex(&SurfaceChangedMutex);

    SE_DEBUG(group_manifestor_video_stmfb, "Stream 0x%p this 0x%p event %d TopologyChanged %d DisplayModeChanged %d\n", Stream, this, event, TopologyChanged, DisplayModeChanged);
}

static void source_queue_listener(uint32_t event, void *CalledBackManifestor)
{
    class Manifestor_VideoStmfb_c   *Manifestor = (Manifestor_VideoStmfb_c *)CalledBackManifestor;
    SE_ASSERT(Manifestor != NULL);

    Manifestor->SourceQueueListener(event);
}

static void display_callback(void           *Buffer,
                             stm_time64_t    VsyncTime,
                             uint16_t        output_change_flags,
                             uint16_t        nb_planes,
                             stm_display_latency_params_t *display_latency_params)
{
    struct StreamBuffer_s              *StreamBuffer    = (struct StreamBuffer_s *)Buffer;
    SE_ASSERT((StreamBuffer != NULL) && (display_latency_params != NULL));
    class Manifestor_VideoStmfb_c      *Manifestor      = (Manifestor_VideoStmfb_c *)StreamBuffer->Manifestor;

    if (StreamBuffer->BufferState != BufferStateQueuedForDisplay)
    {
        SE_ERROR("this 0x%p Wrong BufferState %d for Buffer #%d\n", Manifestor, StreamBuffer->BufferState, StreamBuffer->BufferIndex);
        return;
    }

    SE_ASSERT(Manifestor != NULL);
    Manifestor->DisplayCallback(StreamBuffer, VsyncTime, output_change_flags, nb_planes, display_latency_params);
}

void Manifestor_VideoStmfb_c::DoneCallback(struct StreamBuffer_s  *Buffer,
                                           unsigned int            Status)
{
    // If the manifestor has been halted and/or reset then various state has been destroyed
    if (! TestComponentState(ComponentRunning) || (IsHalting == true))
    {
        return;
    }

    // in case of drain, display callback may not be called
    PropagatePendingMarkerFrames(Buffer);

    if ((Status & STM_STATUS_BUF_HW_ERROR) != 0)
    {
        SE_ERROR("Stream 0x%p this 0x%p FatalHardwareError\n", Stream, this);
    }

    Buffer->QueueCount--;

    if (Buffer->QueueCount == 0)
    {
        Buffer->BufferState   = BufferStateAvailable;

        SE_VERBOSE2(group_manifestor_video_stmfb, group_se_pipeline, "Stream 0x%p this 0x%p Release Buffer #%d\n", Stream, this, Buffer->BufferIndex);
        mOutputPort->Insert((uintptr_t) Buffer->BufferClass);
    }
}

static void done_callback(void   *Buffer, const stm_buffer_presentation_stats_t *Data)
{
    struct StreamBuffer_s              *StreamBuffer    = (struct StreamBuffer_s *)Buffer;
    SE_ASSERT(StreamBuffer != NULL);
    class Manifestor_VideoStmfb_c      *Manifestor      = (Manifestor_VideoStmfb_c *)StreamBuffer->Manifestor;

    SE_VERBOSE(group_manifestor_video_stmfb, "this 0x%p #%d\n", Manifestor, StreamBuffer->BufferIndex);

    if (StreamBuffer->BufferState != BufferStateQueued && StreamBuffer->BufferState != BufferStateDisplayed && StreamBuffer->BufferState != BufferStateQueuedForDisplay)
    {
        SE_ERROR("this 0x%p Wrong BufferState %d for Buffer #%d\n", Manifestor, StreamBuffer->BufferState, StreamBuffer->BufferIndex);
        return;
    }

    SE_ASSERT(Manifestor != NULL);
    Manifestor->DoneCallback(StreamBuffer, Data ? (unsigned int)Data->status : 0);
}

ManifestorStatus_t Manifestor_VideoStmfb_c::GetNumberOfTimings(unsigned int *NumTimes)
{
    SE_VERBOSE(group_manifestor_video_stmfb, "Stream 0x%p this 0x%p NumTimes = %d\n", Stream, this, NumberOfTimings);
    *NumTimes = NumberOfTimings;
    return ManifestorNoError;
}

ManifestorStatus_t  Manifestor_VideoStmfb_c::HandleMarkerFrame(Buffer_t MarkerFrameBuffer)
{
    PlayerSequenceNumber_t *SequenceNumberStructure = GetSequenceNumberStructure(MarkerFrameBuffer);
    MarkerFrame_t           MarkerFrame = SequenceNumberStructure->mMarkerFrame;

    SE_DEBUG(group_manifestor_video_stmfb, "Stream 0x%p this 0x%p received Marker (type %d) #%lld\n", Stream, this, MarkerFrame.mMarkerType, MarkerFrame.mSequenceNumber);

    switch (MarkerFrame.mMarkerType)
    {
    case EosMarker:
        SE_INFO(group_manifestor_video_stmfb, "Stream 0x%p this 0x%p received EOSMarker #%lld\n", Stream, this, MarkerFrame.mSequenceNumber);
        mDisplayDiscontinuity = true;
        mNextQueuedFrameIsFirstFrame           = true;
        break;

    case DrainDiscardMarker:
        SE_INFO(group_manifestor_video_stmfb, "Stream 0x%p this 0x%p received DrainDiscardMarker #%lld\n", Stream, this, MarkerFrame.mSequenceNumber);
        mDisplayDiscontinuity = true;
        mNextQueuedFrameIsFirstFrame           = true;
        break;

    case DrainPlayOutMarker:
        SE_INFO(group_manifestor_video_stmfb, "Stream 0x%p this 0x%p received DrainPlayOutMarker #%lld\n", Stream, this, MarkerFrame.mSequenceNumber);
        mDisplayDiscontinuity = true;
        mNextQueuedFrameIsFirstFrame           = true;
        break;

    case DiscontinuityMarker:
        SE_DEBUG(group_manifestor_video_stmfb, "Stream 0x%p this 0x%p received DiscontinuityMarker #%lld\n", Stream, this, MarkerFrame.mSequenceNumber);
        mDisplayDiscontinuity = true;
        break;

    default:
        break;
    }

    HandleMarkerFramePropagation(MarkerFrameBuffer);

    return ManifestorNoError;
}

void Manifestor_VideoStmfb_c::PropagatePendingMarkerFrames(struct StreamBuffer_s  *StreamBufPtr)
{
    OS_LockMutex(&mMarkerFrameVectorMutex);

    for (size_t i = 0; i < StreamBufPtr->MarkerBufferVector.Size(); i++)
    {
        SE_DEBUG(group_manifestor_video_stmfb, "Propagating marker 0x%p\n", StreamBufPtr->MarkerBufferVector[i]);
        mOutputPort->Insert((uintptr_t)StreamBufPtr->MarkerBufferVector[i]);
    }

    StreamBufPtr->MarkerBufferVector.Resize(0);

    OS_UnLockMutex(&mMarkerFrameVectorMutex);
}

void Manifestor_VideoStmfb_c::HandleMarkerFramePropagation(Buffer_t MarkerFrameBuffer)
{
    bool PropagateNow = true;

    OS_LockMutex(&mMarkerFrameVectorMutex);

    // Check if at least one frame is pushed to display and not yet rendered
    if ((mLastQueuedBufferIndex != INVALID_INDEX)
        && (StreamBuffer[mLastQueuedBufferIndex].BufferState == BufferStateQueuedForDisplay))
    {
        // Attach MarkerFrameBuffer to last queued buffer
        // It will be propagated when we get the display callback (or done callback in case of flush)
        if (StreamBuffer[mLastQueuedBufferIndex].MarkerBufferVector.PushBack(MarkerFrameBuffer) == OS_NO_ERROR)
        {
            SE_DEBUG(group_manifestor_video_stmfb, "Marker 0x%p attached to buffer %u\n", MarkerFrameBuffer, mLastQueuedBufferIndex);
            PropagateNow = false;
        }
        else
        {
            SE_ERROR("Stream 0x%p this 0x%p Failed to insert MarkerFrame\n", Stream, this);
        }
    }

    OS_UnLockMutex(&mMarkerFrameVectorMutex);

    if (PropagateNow)
    {
        mOutputPort->Insert((uintptr_t) MarkerFrameBuffer);
    }
}


void Manifestor_VideoStmfb_c::FRCPatternTrace(unsigned int DisplayFrameIndex
                                              , unsigned int DisplayCount_0
                                              , unsigned int DisplayCount_1
                                              , bool TopFieldFirst)
{
    int i;

    char first[16 + 1]; // +1 for adding \0
    for (i = 0; i < Min(16U, DisplayCount_0); i++)
    {
        if (TopFieldFirst)
        {
            first[i] = 'T';
        }
        else
        {
            first[i] = 'B';
        }
    }
    first[i] = '\0';

    char second[16 + 1]; // +1 for adding \0
    for (i = 0; i < Min(16U, DisplayCount_1); i++)
    {
        if (TopFieldFirst)
        {
            second[i] = 'B';
        }
        else
        {
            second[i] = 'T';
        }
    }
    second[i] = '\0';

    SE_DEBUG2(group_se_pipeline, group_frc, "Frame %d  Top first : %d DisplayCounts:%d,%d FRC pattern : %s%s\n"
              , DisplayFrameIndex
              , TopFieldFirst
              , DisplayCount_0
              , DisplayCount_1
              , first
              , second
             );
}

void Manifestor_VideoStmfb_c::SetDecimationFactorsForDisplay(Buffer_t Buffer, stm_display_buffer_t *DisplayBuff)
{
    if (!Buffer || !DisplayBuff)
    {
        SE_ERROR("Stream %p: Cannot set decimation factors from buffer(%p) on DisplayBuf(%p)\n", Stream, DisplayBuff, Stream);
        return;
    }

    int  horizontal_decimation_factor = Stream->GetDecodeBufferManager()->DecimationFactor(Buffer, 0);
    switch (horizontal_decimation_factor)
    {
    case 1:
        horizontal_decimation_factor = STM_NO_DECIMATION;
        break;
    case 2:
        horizontal_decimation_factor = STM_DECIMATION_BY_TWO;
        break;
    case 4:
        horizontal_decimation_factor = STM_DECIMATION_BY_FOUR;
        break;
    case 8:
        horizontal_decimation_factor = STM_DECIMATION_BY_EIGHT;
        break;
    default:
        //It would not come here, unless codec sets unsupported decimation factor. For failsafe default it to H4V2
        SE_ERROR("Unsupported horizontal_decimation_factor %d. Forcing to H4V2\n", horizontal_decimation_factor);
        horizontal_decimation_factor = STM_DECIMATION_BY_FOUR;
    }

    int  vertical_decimation_factor =  Stream->GetDecodeBufferManager()->DecimationFactor(Buffer, 1);
    switch (vertical_decimation_factor)
    {
    case 1:
        vertical_decimation_factor = STM_NO_DECIMATION;
        break;
    case 2:
        vertical_decimation_factor = STM_DECIMATION_BY_TWO;
        break;
    case 4:
        vertical_decimation_factor = STM_DECIMATION_BY_FOUR;
        break;
    case 8:
        vertical_decimation_factor = STM_DECIMATION_BY_EIGHT;
        break;
    default:
        //It would not come here, unless codec sets unsupported decimation factor. For failsafe default it to H4V2
        SE_ERROR("Unsupported vertical_decimation_factor %d. Forcing to H4V2\n", vertical_decimation_factor);
        vertical_decimation_factor = STM_DECIMATION_BY_TWO;
    }

    DisplayBuff->src.horizontal_decimation_factor             = (stm_decimation_factor_t)horizontal_decimation_factor;
    DisplayBuff->src.vertical_decimation_factor               = (stm_decimation_factor_t)vertical_decimation_factor;
}
