/************************************************************************
Copyright (C) 2003-2014 STMicroelectronics. All Rights Reserved.

This file is part of the Streaming Engine.

Streaming Engine is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License version 2 as
published by the Free Software Foundation.

Streaming Engine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with Streaming Engine; see the file COPYING.  If not, write to the Free
Software Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307,
USA.

The Streaming Engine Library may alternatively be licensed under a
proprietary license from ST.
************************************************************************/

#ifndef MANIFESTOR_VIDEO_STMFB_H
#define MANIFESTOR_VIDEO_STMFB_H

#include <stm_registry.h>
#include "osinline.h"
#include <stm_display.h>

#include "allocinline.h"
#include "manifestor_video.h"

#undef TRACE_TAG
#define TRACE_TAG   "Manifestor_VideoStmfb_c"

#define PLANE_MAX           MAXIMUM_NUMBER_OF_TIMINGS_PER_MANIFESTATION - 1

// To avoid long latencies due to the fact that sending two interlaced fields
// is non interruptible, we want that those two fields are not too far apart
// This make the speed changes more reactive in very low speeds
#define MAXIMUM_SECOND_FIELD_LATENCY_US 500000

struct DisplayPlaneInfo
{
    stm_display_mode_t                  CurrentMode;
    stm_display_plane_h                 PlaneHandle;
    unsigned int                        PlaneId;
    stm_display_output_h                OutputHandle;
    unsigned int                        OutputId;
};

/// Video manifestor based on the stgfb core driver API.
class Manifestor_VideoStmfb_c : public Manifestor_Video_c
{
public:
    /* Constructor / Destructor */
    Manifestor_VideoStmfb_c();
    ~Manifestor_VideoStmfb_c();

    /* Overrides for component base class functions */
    ManifestorStatus_t   Halt();

    ManifestorStatus_t   WaitForFrozenSurface();

    /* Manifestor video class functions */
    ManifestorStatus_t  OpenOutputSurface(stm_object_h DisplayHandle);
    ManifestorStatus_t  CloseOutputSurface();
    ManifestorStatus_t  UpdateOutputSurfaceDescriptor();

    ManifestorStatus_t  GetNumberOfTimings(unsigned int *NumTimes);

    ManifestorStatus_t  QueueBuffer(unsigned int                         BufferIndex,
                                    struct ParsedFrameParameters_s      *FrameParameters,
                                    struct ParsedVideoParameters_s      *VideoParameters,
                                    struct ManifestationOutputTiming_s **VideoOutputTimingArray,
                                    Buffer_t                             Buffer);
    ManifestorStatus_t  FlushDisplayQueue(bool ReleaseAllBuffers);
    ManifestorStatus_t  HandleMarkerFrame(Buffer_t MarkerFrameBuffer);

    void FRCPatternTrace(unsigned int DisplayFrameIndex
                         , unsigned int DisplayCount_0
                         , unsigned int DisplayCount_1
                         , bool TopFieldFirst);

    int  Enable();
    int  Disable();
    bool GetEnable();

    long long           GetNextManifestationTime(void *ParsedAudioVideoDataParameters);

    unsigned long long  GetManifestationLatency(void *ParsedAudioVideoDataParameters);

    /* The following functions are public because they are accessed via C stub functions */
    void                DisplayCallback(struct StreamBuffer_s        *Buffer,
                                        stm_time64_t                  VsyncTime,
                                        uint16_t                      output_change_flags,
                                        uint16_t                      nb_output,
                                        stm_display_latency_params_t *display_latency_params);

    void                DoneCallback(struct StreamBuffer_s  *Buffer,
                                     unsigned int            Status);

    void                SourceQueueListener(uint32_t event);

private:
    stm_display_device_h        DisplayDevice;
    stm_display_source_h        Source;
    stm_display_source_queue_h  QueueInterface;

    OS_Mutex_t                  DisplayPlaneMutex;
    OS_Mutex_t                  SurfaceChangedMutex;
    int                         NumPlanes;
    DisplayPlaneInfo            DisplayPlanes[PLANE_MAX];

    OS_Mutex_t                  mMarkerFrameVectorMutex;

    bool                        TopologyChanged;
    bool                        DisplayModeChanged;
    bool                        mDisplayDiscontinuity;

    Rational_t                  LastSourceFrameRate;

    stm_display_buffer_t        DisplayBuffer[MAX_DECODE_BUFFERS];

    int                         ClockRateAdjustment[MAXIMUM_NUMBER_OF_TIMINGS_PER_MANIFESTATION];

    OS_Event_t                  mDisplayCallbackEvent;

    DISALLOW_COPY_AND_ASSIGN(Manifestor_VideoStmfb_c);

    void                FillInDisplayBufferSrcFields(Buffer_t                         Buffer,
                                                     struct ParsedVideoParameters_s  *VideoParameters,
                                                     stm_display_buffer_t            *DisplayBuff);
    void                FillInDisplayBufferInfoFields(struct StreamBuffer_s                *StreamBuff,
                                                      struct ManifestationOutputTiming_s  **VideoOutputTimingArray,
                                                      stm_display_buffer_t                 *DisplayBuff);

    void                FillInDisplayBuffer(Buffer_t                    Buffer,
                                            struct StreamBuffer_s      *StreamBuff,
                                            struct ParsedVideoParameters_s *VideoParameters,
                                            struct ManifestationOutputTiming_s **VideoOutputTimingArray,
                                            stm_display_buffer_t       *DisplayBuff);

    void                SelectDisplaySource(Buffer_t                    Buffer,
                                            stm_display_buffer_t       *DisplayBuff);

    void                SelectDisplayBufferPointers(Buffer_t                    Buffer,
                                                    stm_display_buffer_t       *DisplayBuff);

    void                ApplyPixelAspectRatioCorrection(stm_display_buffer_t       *DisplayBuff,
                                                        struct ParsedVideoParameters_s *VideoParameters);

    ManifestorStatus_t  SetOutputRateAdjustment(unsigned int TimingIndex,
                                                struct StreamBuffer_s      *StreamBuff);

    ManifestorStatus_t  OpenDisplayPlaneHandles();
    void                CloseDisplayPlaneHandles();

    ManifestorStatus_t  OpenDisplaySourceDevice(stm_object_h    DisplayHandle);
    void                CloseDisplaySourceDevice();

    ManifestorStatus_t  UpdateDisplayMode(OutputSurfaceDescriptor_t *SurfaceDesciptor, unsigned int OutputIndex);

    int                 GetPlaneIndex(unsigned int OutputId);

    void                PrintSurfaceDescriptors();

    ManifestorStatus_t  UpdatePlaneSurfaceDescriptor(int i);
    void                UpdateDisplaySourceSurfaceDescriptor();
    ManifestorStatus_t  FillDisplayPlaneSurfaceDescriptors();
    ManifestorStatus_t  FillDisplaySourceSurfaceDescriptor();
    ManifestorStatus_t  FillDisplaySurfaceDescriptors();

    bool                isMainPlane(int PlaneIndex);
    bool                isAuxPlane(int PlaneIndex);
    bool                isPipPlane(int PlaneIndex);
    void                SetDecimationFactorsForDisplay(Buffer_t Buffer, stm_display_buffer_t *DisplayBuff);
    void                ReportFirstFrameOnDisplayEvent();
    void                ReportFrameRenderedEvent();
    void                ReportQueueBufferFailedEvent();
    void                ReportDisplayEvents();
    bool                BuffersToDisplay();

    void                HandleMarkerFramePropagation(Buffer_t MarkerFrameBuffer);
    void                PropagatePendingMarkerFrames(struct StreamBuffer_s  *StreamBufPtr);

    static const int    kMainPlaneCapabilities  = PLANE_CAPS_VIDEO | PLANE_CAPS_PRIMARY_OUTPUT | PLANE_CAPS_VIDEO_BEST_QUALITY | PLANE_CAPS_PRIMARY_PLANE;
    static const int    kAuxPlaneCapabilities   = PLANE_CAPS_SECONDARY_OUTPUT;
    static const int    kPipPlaneCapabilities   = PLANE_CAPS_VIDEO | PLANE_CAPS_PRIMARY_OUTPUT;

    void CheckAllBuffersReturned();
};


#endif
