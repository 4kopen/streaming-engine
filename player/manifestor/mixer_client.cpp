/************************************************************************
Copyright (C) 2003-2016 STMicroelectronics. All Rights Reserved.

This file is part of the Streaming Engine.

Streaming Engine is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License version 2 as
published by the Free Software Foundation.

Streaming Engine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with Streaming Engine; see the file COPYING.  If not, write to the Free
Software Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307,
USA.

The Streaming Engine Library may alternatively be licensed under a
proprietary license from ST.
************************************************************************/

#include "mixer_client.h"

#undef TRACE_TAG
#define TRACE_TAG   "Mixer_Client_c"

Mixer_Client_c::Mixer_Client_c()
    : State(DISCONNECTED)
    , Manifestor(NULL)
    , Stream(NULL)
    , Parameters()
    , Muted(false)
    , mMixerClientLock()
    , LockNestingLevel(0)
{
    OS_InitializeMutex(&mMixerClientLock);
    SE_DEBUG(group_mixer, "<: This:%p Manifestor:%p Stream:%p\n", this, Manifestor, Stream);
}

Mixer_Client_c::~Mixer_Client_c()
{
    // Expected to be in DISCONNECTED state
    SE_ASSERT(State == DISCONNECTED);
    OS_TerminateMutex(&mMixerClientLock);
    SE_DEBUG(group_mixer, "<: This:%p Manifestor:%p Stream:%p\n", this, Manifestor, Stream);
}

bool Mixer_Client_c::IsMyManifestor(const Manifestor_AudioKsound_c *Manifestor) const
{
    bool Result;

    if (NULL == Mixer_Client_c::Manifestor)
    {
        Result = false;
    }
    else
    {
        Result = (Mixer_Client_c::Manifestor == Manifestor);
    }

    SE_EXTRAVERB(group_mixer, "Returning %s\n", Result ? "true" : "false");
    return Result;
}

bool Mixer_Client_c::IsMyStream(const HavanaStream_c *Stream) const
{
    bool Result;

    if (NULL == Mixer_Client_c::Stream)
    {
        Result = false;
    }
    else
    {
        Result = (Mixer_Client_c::Stream == Stream);
    }

    SE_EXTRAVERB(group_mixer, "Returning %s\n", Result ? "true" : "false");
    return Result;
}

void Mixer_Client_c::RegisterManifestor(Manifestor_AudioKsound_c *Manifestor, HavanaStream_c *Stream)
{
    SE_DEBUG(group_mixer, ">: This:%p Manifestor:%p Stream:%p\n", this, Manifestor, Stream);
    // Expected to be in DISCONNECTED state
    SE_ASSERT(State == DISCONNECTED);
    SetState(STOPPED);
    Mixer_Client_c::Manifestor = Manifestor;
    Mixer_Client_c::Stream = Stream;
    memset(&Parameters, 0, sizeof(Parameters));
    Muted = false;
    SE_DEBUG(group_mixer, "<: %s\n", Mixer_c::LookupInputState(State));
}

void Mixer_Client_c::DeRegisterManifestor()
{
    SE_DEBUG(group_mixer, "This:%p Manifestor:%p Stream:%p\n", this, Manifestor, Stream);
    // Expected to be in STOPPED state
    SetState(DISCONNECTED);
    Manifestor = NULL;
    Stream = NULL;
    memset(&Parameters, 0, sizeof(Parameters));
    Muted = false;
}

PlayerStatus_t Mixer_Client_c::UpdateManifestorModuleParameters(const MixerClientParameters &params)
{
    SE_DEBUG(group_mixer, ">: This:%p Manifestor:%p Stream:%p\n", this, Manifestor, Stream);
    // Expected NOT to be in DISCONNECTED state
    SE_ASSERT(!(State == DISCONNECTED));
    ManifestorStatus_t         Status;
    Manifestor_AudioKsound_c *Manifestor = GetManifestor();
    // This client is correctly stated, and its manifestor is well known.
    Status = Manifestor->SetModuleParameters(sizeof(params.ParameterBlockOT), (void *) &params.ParameterBlockOT);

    if (ManifestorNoError != Status)
    {
        return PlayerError;
    }

    Status = Manifestor->SetModuleParameters(sizeof(params.ParameterBlockDownmix), (void *) &params.ParameterBlockDownmix);

    if (ManifestorNoError != Status)
    {
        return PlayerError;
    }

    return PlayerNoError;
}

bool Mixer_Client_c::ManagedByThread() const
{
    SE_EXTRAVERB(group_mixer, ">: This:%p Manifestor:%p Stream:%p %s\n",
                 this, Manifestor, Stream, Mixer_c::LookupInputState(State));

    switch (State)
    {
    case DISCONNECTED:
        return false;

    case STOPPED:
        return false;

    case UNCONFIGURED:
        return false;

    // In the following states this client internal members are relevant and allow operation mixer thread side.
    case STARTING:
        return true;

    case STARTED:
        return true;

    //case STOPPING:     return true;  TODO:  Should check this
    case STOPPING:
        return false;

    default:
        SE_ERROR("<: Unknown input state %d. Implementation error\n", State);
        return false;
    }
}
