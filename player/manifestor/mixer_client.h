/************************************************************************
Copyright (C) 2003-2014 STMicroelectronics. All Rights Reserved.

This file is part of the Streaming Engine.

Streaming Engine is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License version 2 as
published by the Free Software Foundation.

Streaming Engine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with Streaming Engine; see the file COPYING.  If not, write to the Free
Software Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307,
USA.

The Streaming Engine Library may alternatively be licensed under a
proprietary license from ST.
************************************************************************/

#ifndef H_MIXER_CLIENT_CLASS
#define H_MIXER_CLIENT_CLASS

#include "output_timer_audio.h"
#include "audio_player.h"
#include "player_types.h"
#include "manifestor_audio_ksound.h"
#include "mixer.h"
#include "dtshd.h"
#include "aac_audio.h"
#include "havana_stream.h"

#undef TRACE_TAG
#define TRACE_TAG   "Mixer_Client_c"

class Mixer_Client_c : public Mixer_c
{
public:
    /// Control Parameters exchanged with Input Clients
    struct MixerClientParameters
    {
        OutputTimerParameterBlock_t ParameterBlockOT;
        CodecParameterBlock_t       ParameterBlockDownmix;
    };

    Mixer_Client_c();
    ~Mixer_Client_c();

    inline void LockTake()
    {
        SE_EXTRAVERB(group_mixer, ">: This:%p Manifestor:%p Stream:%p Nesting:%d\n", this, Manifestor,
                     Stream, LockNestingLevel);
        OS_LockMutexNested(&mMixerClientLock, LockNestingLevel);
        SE_EXTRAVERB(group_mixer, "<: %p\n", this);
    }

    inline void LockRelease()
    {
        SE_EXTRAVERB(group_mixer, ">: This:%p Manifestor:%p Stream:%p\n", this, Manifestor, Stream);
        OS_UnLockMutex(&mMixerClientLock);
        SE_EXTRAVERB(group_mixer, "<:\n");
    }

    bool IsMyManifestor(const Manifestor_AudioKsound_c *Manifestor) const;
    bool IsMyStream(const HavanaStream_c *Stream) const;

    inline void SetState(Mixer_c::InputState_t State)
    {
        SE_DEBUG(group_mixer, "This:%p Manifestor:%p Stream:%p (%s) -> %s\n",
                 this, Manifestor, Stream, Mixer_c::LookupInputState(Mixer_Client_c::State), Mixer_c::LookupInputState(State));
        Mixer_Client_c::State = State;
    }

    inline Mixer_c::InputState_t GetState() const
    {
        //Synchronize a possible change ongoing.
        SE_EXTRAVERB(group_mixer, "This:%p Manifestor:%p Stream:%p %s\n",
                     this, Manifestor, Stream, Mixer_c::LookupInputState(State));
        return State;
    }

    void RegisterManifestor(Manifestor_AudioKsound_c *Manifestor, HavanaStream_c *Stream);

    inline Manifestor_AudioKsound_c *GetManifestor() const
    {
        SE_ASSERT(Manifestor);
        SE_EXTRAVERB(group_mixer, "returning: %p\n", Manifestor);
        return Manifestor;
    }

    void DeRegisterManifestor();
    PlayerStatus_t UpdateManifestorModuleParameters(const MixerClientParameters &params);
    bool ManagedByThread() const;

    inline void UpdateParameters(const ParsedAudioParameters_t *ParsedAudioParameters)
    {
        SE_DEBUG(group_mixer, "This:%p Manifestor:%p Stream:%p\n", this, Manifestor, Stream);
        Parameters = *ParsedAudioParameters;
    }

    // Caller must check verify that state is not DISCONNECTED
    inline ParsedAudioParameters_t GetParameters() const
    {
        SE_EXTRAVERB(group_mixer, ">: This:%p Manifestor:%p Stream:%p\n", this, Manifestor, Stream);
        // Expected NOT to be in DISCONNECTED state
        SE_ASSERT(!(State == DISCONNECTED));
        return Parameters;
    }

    // Caller must check verify that state is not DISCONNECTED
    AudioSurfaceParameters_t GetSourceSurfaceParameter() const
    {
        SE_EXTRAVERB(group_mixer, ">: This:%p Manifestor:%p Stream:%p\n", this, Manifestor, Stream);
        SE_ASSERT(!(State == DISCONNECTED));
        SE_VERBOSE(group_mixer, "@: BitsPerSample:%u Channels:%u Rate:%uHz\n",
                   Parameters.Source.BitsPerSample,
                   Parameters.Source.ChannelCount,
                   Parameters.Source.SampleRateHz);
        return Parameters.Source;
    }

    // Caller must check verify that state is not DISCONNECTED
    unsigned int GetSampleCountParameter() const
    {
        SE_EXTRAVERB(group_mixer, ">: This:%p Manifestor:%p Stream:%p\n", this, Manifestor, Stream);
        SE_ASSERT(!(State == DISCONNECTED));
        SE_VERBOSE(group_mixer, "@: SampleCount:%u\n", Parameters.SampleCount);
        return Parameters.SampleCount;
    }

    // Caller must check verify that state is not DISCONNECTED
    int GetOrganisationParameter() const
    {
        SE_EXTRAVERB(group_mixer, ">: This:%p Manifestor:%p Stream:%p\n", this, Manifestor, Stream);
        SE_ASSERT(!(State == DISCONNECTED));
        SE_VERBOSE(group_mixer, "@: Organisation:%u\n", Parameters.Organisation);
        return Parameters.Organisation;
    }

    // Caller must check verify that state is not DISCONNECTED
    AudioOriginalEncoding_t GetOriginalEncodingParameter() const
    {
        SE_EXTRAVERB(group_mixer, ">: This:%p Manifestor:%p Stream:%p\n", this, Manifestor, Stream);
        SE_ASSERT(!(State == DISCONNECTED));
        SE_VERBOSE(group_mixer, "@: OriginalEncoding:%d\n", Parameters.OriginalEncoding);
        return Parameters.OriginalEncoding;
    }

    // Caller must check verify that state is not DISCONNECTED
    SpdifInProperties_t GetSpdifInProperties() const
    {
        SE_EXTRAVERB(group_mixer, ">: This:%p Manifestor:%p Stream:%p\n", this, Manifestor, Stream);
        SE_ASSERT(!(State == DISCONNECTED));
        return Parameters.SpdifInProperties;
    }

    // Caller must check verify that state is not DISCONNECTED
    BackwardCompatibleProperties_t GetBackwardCompParameter() const
    {
        SE_EXTRAVERB(group_mixer, ">: This:%p Manifestor:%p Stream:%p\n", this, Manifestor, Stream);
        SE_ASSERT(!(State == DISCONNECTED));
        return Parameters.BackwardCompatibleProperties;
    }

    // Caller must check verify that state is not DISCONNECTED
    MixingMetadata_t GetMixingMetadataParameter() const
    {
        SE_EXTRAVERB(group_mixer, ">: This:%p Manifestor:%p Stream:%p\n", this, Manifestor, Stream);
        SE_ASSERT(!(State == DISCONNECTED));
        return Parameters.MixingMetadata;
    }

    // Caller must check verify that state is not DISCONNECTED
    inline void GetParameters(ParsedAudioParameters_t *Parameters)
    {
        SE_EXTRAVERB(group_mixer, ">: This:%p Manifestor:%p Stream:%p\n", this, Manifestor, Stream);
        SE_ASSERT(!(State == DISCONNECTED));
        *Parameters = Mixer_Client_c::Parameters;
    }

    inline HavanaStream_c *GetStream() const
    {
        SE_EXTRAVERB(group_mixer, ">: This:%p Manifestor:%p Stream:%p\n", this, Manifestor, Stream);
        // CAUTION: Caller is expected to check that Stream ! NULL
        // Expected NOT to be in DISCONNECTED state
        // SE_ASSERT(State != DISCONNECTED);
        return Stream;
    }

    inline bool IsMute() const
    {
        SE_VERBOSE(group_mixer, "<: %p %p %p Returning %s\n",
                   this, Manifestor, Stream, Muted ? "true" : "false");
        return Muted;
    }

    // Must be called once after construction and before first LockTake() call
    // to assign unique nesting level to this object.
    inline void SetLockNestingLevel(int Level)
    {
        SE_ASSERT(Level > 0);
        LockNestingLevel = Level;
    }

private:
    InputState_t State; // The state of this client of audio mixer.
    Manifestor_AudioKsound_c *Manifestor; // Manifestor object as a pointer.
    HavanaStream_c *Stream; // Stream object as a pointer.
    ParsedAudioParameters_t Parameters; // The audio parameters for this client.
    bool Muted; // The mute state for this client.

    // Internal mutex to arbitrate possible concurrent accesses for this client.
    // Some callers need to lock all clients in a row so keep track of nesting
    // level for this client relative to other clients.  This level will be
    // passed to OS_LockMutexNested() and will prevent lockdep from complaining
    // about lock acquisition false positive.
    OS_Mutex_t mMixerClientLock;
    int LockNestingLevel;

    DISALLOW_COPY_AND_ASSIGN(Mixer_Client_c);
};

#endif // H_MIXER_CLIENT_CLASS
