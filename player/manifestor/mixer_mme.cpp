/************************************************************************
Copyright (C) 2003-2014 STMicroelectronics. All Rights Reserved.

This file is part of the Streaming Engine.

Streaming Engine is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License version 2 as
published by the Free Software Foundation.

Streaming Engine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with Streaming Engine; see the file COPYING.  If not, write to the Free
Software Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307,
USA.

The Streaming Engine Library may alternatively be licensed under a
proprietary license from ST.
************************************************************************/

#include "acc_mme.h"

#include "st_relayfs_se.h"
#include "player_threads.h"
#include "audio_mixer_params.h"
#include "se_mme_strings.h"

#include "mixer_mme.h"
#include "audio_mixer_params.h"

#include "mixer_transformer.h"
#include "player_fadepan_mapping.h"

#include "codec_mme_base.h"

#undef TRACE_TAG
#define TRACE_TAG   "Mixer_Mme_c"

#define MIXER_MAX_WAIT_FOR_MME_COMMAND_COMPLETION  100     /* Ms */

#define MIXER_NB_OF_ITERATION_WITH_ERROR_BEFORE_STOPPING 3

// Number of mixer thread loops to wait before entering low power:
// 1. soft mute
// 2. zero all the periods
#define MIXER_NUMBER_OF_THREAD_LOOPS_BEFORE_LOW_POWER_ENTER     (MIXER_NUM_PERIODS + 1)

//#define SPLIT_AUX  ACC_DONT_SPLIT
#define SPLIT_AUX  ACC_SPLIT_AUTO

#define HW_DELAY 0

static const uint32_t MIXER_SAMPLING_FREQUENCY_STKPI_UNSET(0);

static const int32_t MIXER_MME_NO_ACTIVE_PLAYER(-1);

// Must be less than DEFAULT_MIXER_REQUEST_MANAGER_TIMEOUT
static const uint32_t MIXER_THREAD_SURFACE_PARAMETER_UPDATE_EVENT_TIMEOUT(3 * 1000);

static const  struct stm_se_audio_channel_assignment default_speaker_config =
{
    STM_SE_AUDIO_CHANNEL_PAIR_NOT_CONNECTED,
    STM_SE_AUDIO_CHANNEL_PAIR_NOT_CONNECTED,
    STM_SE_AUDIO_CHANNEL_PAIR_NOT_CONNECTED,
    STM_SE_AUDIO_CHANNEL_PAIR_NOT_CONNECTED,
    STM_SE_AUDIO_CHANNEL_PAIR_NOT_CONNECTED,
    0,
    false
};

static void *PlaybackThreadStub(void *Parameter)
{
    Mixer_Mme_c *Mixer = static_cast<Mixer_Mme_c *>(Parameter);
    Mixer->PlaybackThread();
    OS_TerminateThread();
    return NULL;
}

////////////////////////////////////////////////////////////////////////////
///
/// Stub function with C linkage to connect the MME callback to the manifestor.
///
static void MMEFirstMixerCallbackStub(MME_Event_t      Event,
                                      MME_Command_t   *CallbackData,
                                      void            *UserData)
{
    Mixer_Mme_c *Self = static_cast<Mixer_Mme_c *>(UserData);
    bool isFirstMixer = true;
    Self->CallbackFromMME(Event, CallbackData, isFirstMixer);
}
////////////////////////////////////////////////////////////////////////////
///
/// Stub function with C linkage to connect the MME callback to the manifestor.
///
static void MMECallbackStub(MME_Event_t      Event,
                            MME_Command_t   *CallbackData,
                            void            *UserData)
{
    Mixer_Mme_c *Self = static_cast<Mixer_Mme_c *>(UserData);
    bool isFirstMixer = false;
    Self->CallbackFromMME(Event, CallbackData, isFirstMixer);
}

////////////////////////////////////////////////////////////////////////////
///
///
///
Mixer_Mme_c::Mixer_Mme_c(const char *Name, char *TransformerName, size_t TransformerNameSize, stm_se_mixer_spec_t Topology)
    : Name()
    , mStatistics()
    , mTopology(Topology)
    , Client()
    , MixerRequestManager()
    , PlaybackThreadRunning(false)
    , mPlaybackThreadOnOff()
    , IsLowPowerState(false)
    , IsLowPowerMMEInitialized(false)
    , LowPowerEnterEvent()
    , LowPowerExitEvent()
    , LowPowerExitCompletedEvent()
    , LowPowerEnterThreadLoop(0)
    , LowPowerPostMixGainSave(0)
    , InLowPowerState(false)
    , ThreadState(ThreadState_c::Idle)
    , PcmParamsReceivedWhileStarting(false)
    , PcmPlayersStarted(false)
    , mSPDIFCardId(STM_SE_MIXER_NB_MAX_OUTPUTS)
    , mHDMICardId(STM_SE_MIXER_NB_MAX_OUTPUTS)
    , MMETransformerName(TransformerName)
    , MMETransformerNameSize(TransformerNameSize)
    , MMEHandle(MME_INVALID_ARGUMENT)
    , MMEFirstMixerHdl(MME_INVALID_ARGUMENT)
    , LowPowerWakeUpFirstMixer(false)
    , MMEInitParams()
    , FirstMixerMMEInitParams()
    , MMEInitialized(false)
    , MMECallbackSemaphore()
    , MMEFirstMixerCallbackSemaphore()
    , MMEParamCallbackSemaphore()
    , InterMixerFreeBufRing(NULL)
    , InterMixerFillBufRing(NULL)
    , InterMixerFreeMDRing(NULL)
    , InterMixerFillMDRing(NULL)
    , InterMixerBufferAcMod(ACC_MODE_ID)
    , MMECallbackThreadBoosted(false)
    , MMENeedsParameterUpdate(false)
    , InitializedSamplingFrequency(MIXER_SAMPLING_FREQUENCY_STKPI_UNSET)
    , MixerSamplingFrequency(0)
    , NominalOutputSamplingFrequency(0)
    , MasterOutputGrain(ModuleParameter_GetMixerDefaultGrain())
    , MixerGranuleSize(0)
    , MasterClientIndex(0)
    , PrimaryCodedDataType()
    , UpstreamConfiguration(NULL)
    , MixerSettings()
    , NumberOfAudioPlayerAttached(0)
    , NumberOfAudioPlayerInitialized(0)
    , NumberOfAudioGeneratorAttached(0)
    , NumberOfInteractiveAudioAttached(0)
    , FirstActivePlayer(MIXER_MME_NO_ACTIVE_PLAYER)
    , PcmPlayerNeedsParameterUpdate(false)
    , PrimaryClient(0)
    , SecondaryClient(1)
    , MixerParams()
    , AudioConfiguration()
    , FirstMixerCommand()
    , MixerCommand()
    , ParamsCommand()
    , FirstMixerParamsCommand()
    , CurrentOutputPcmParams()
    , MixerPlayer()
    , mPlayerToReset()
    , Generator()
    , InteractiveAudio()
    , FirstMixerSamplingFrequencyHistory(0)
    , MixerSamplingFrequencyHistory(0)
    , FirstMixerSamplingFrequencyUpdated(true)
    , MixerSamplingFrequencyUpdated(true)
    , FirstMixerGranuleSizeHistory(0)
    , MixerGranuleSizeHistory(0)
    , FirstMixerGranuleSizeUpdated(true)
    , MixerGranuleSizeUpdated(true)
    , OutputTopologyUpdated(true)
    , FirstMixerClientAudioParamsUpdated(true)
    , ClientAudioParamsUpdated(true)
    , AudioGeneratorUpdated(true)
    , IAudioUpdated(true)
    , mApplicationType(STM_SE_CTRL_VALUE_AUDIO_APPLICATION_ISO)
    , mApplicationTypeStoredParams()
    , MixCommandStartTime(0)
{
    strncpy(&this->Name[0], Name, sizeof(this->Name));
    this->Name[sizeof(this->Name) - 1] = '\0';

    if (BaseComponentClass_c::InitializationStatus != PlayerNoError)
    {
        SE_ERROR("Some troubles with InitializationStatus\n");
        return;
    }

    OS_InitializeEvent(&mPlaybackThreadOnOff);
    OS_InitializeEvent(&LowPowerEnterEvent);
    OS_InitializeEvent(&LowPowerExitEvent);
    OS_InitializeEvent(&LowPowerExitCompletedEvent);
    OS_SemaphoreInitialize(&MMECallbackSemaphore, 0);
    OS_SemaphoreInitialize(&MMEFirstMixerCallbackSemaphore, 0);
    OS_SemaphoreInitialize(&MMEParamCallbackSemaphore, 1);

    UpdateTransformerId(TransformerName);

    AudioConfiguration.CRC = false;
    ResetMixerSettings();
    ResetOutputConfiguration();

    // Dual Mixer is required for MS12, thus set application type to MS12
    if (mTopology.type == STM_SE_MIXER_DUAL_STAGE_MIXING)
    {
        PlayerStatus_t status = SetApplicationType(STM_SE_CTRL_VALUE_AUDIO_APPLICATION_MS12);
        SE_ASSERT(status == PlayerNoError);
    }

    for (uint32_t CodedIndex(0); CodedIndex < MIXER_MAX_CODED_INPUTS; CodedIndex++)
    {
        PrimaryCodedDataType[CodedIndex] = PcmPlayer_c::OUTPUT_IEC60958;
    }

    // Allow clients to be locked in ascending index order without lockdep
    // complaining about recursive lock acquisition.
    for (int i = 0; i < MIXER_MAX_CLIENTS; ++i)
    {
        Client[i].SetLockNestingLevel(i + 1);
    }

    Reset();  // TODO(pht) ctor shall work without Reset => factorize code for both ctor and Reset usage

    OS_ResetEvent(&mPlaybackThreadOnOff);

    StartPlaybackThread();

    // wait for the thread to come to rest
    OS_WaitForEventAuto(&mPlaybackThreadOnOff, OS_INFINITE);

    // Once this mixer object has been created, MW can start attaching clients / players to it
    // this attachment relies on the read of Mixer_Request_Manager_c::ThreadState
    OS_Smp_Mb(); // Read memory barrier: rmb_for_Mixer_Starting coupled with: wmb_for_Mixer_Starting

    SE_DEBUG(group_mixer,  "Successful construction of %s\n", Name);
}


////////////////////////////////////////////////////////////////////////////
///
///
///
Mixer_Mme_c::~Mixer_Mme_c()
{
    TerminatePlaybackThread();

    OS_TerminateEvent(&mPlaybackThreadOnOff);
    OS_TerminateEvent(&LowPowerEnterEvent);
    OS_TerminateEvent(&LowPowerExitEvent);
    OS_TerminateEvent(&LowPowerExitCompletedEvent);
    OS_SemaphoreTerminate(&MMECallbackSemaphore);
    OS_SemaphoreTerminate(&MMEFirstMixerCallbackSemaphore);
    OS_SemaphoreTerminate(&MMEParamCallbackSemaphore);
}


////////////////////////////////////////////////////////////////////////////
///
///
///
PlayerStatus_t Mixer_Mme_c::Halt()
{
    SE_DEBUG(group_mixer, ">%s\n", Name);

    if (MMEInitialized)
    {
        if (PlayerNoError != TerminateMMETransformers())
        {
            SE_ERROR("Failed to terminated mixer transformer\n");
            // no error recovery possible
        }
    }

    // Free Hw players if some.
    TerminatePcmPlayers();
    return Mixer_c::Halt();
}


////////////////////////////////////////////////////////////////////////////
///
///
///
PlayerStatus_t Mixer_Mme_c::Reset()
{
    SE_DEBUG(group_mixer, ">%s\n", Name);

    // Do not reset MixerPlayer[] because can still be attached at current moment.

    for (uint32_t PlayerIdx(0); PlayerIdx < STM_SE_MIXER_NB_MAX_OUTPUTS; PlayerIdx++)
    {
        MixerPlayer[PlayerIdx].ResetPcmPlayerConfig();
    }

    PcmParamsReceivedWhileStarting = false;
    PcmPlayerNeedsParameterUpdate = false;
    MMEHandle = MME_INVALID_ARGUMENT;
    MMEFirstMixerHdl = MME_INVALID_ARGUMENT;
    LowPowerWakeUpFirstMixer = false;
    MMEInitialized = false;
    InterMixerFreeBufRing = NULL;
    InterMixerFillBufRing = NULL;
    InterMixerFreeMDRing = NULL;
    InterMixerFillMDRing = NULL;
    InterMixerBufferAcMod = ACC_MODE_ID;
    // MMECallbackThreadBoosted is undefined
    MMENeedsParameterUpdate = false;
    MasterClientIndex = 0;
    InitializedSamplingFrequency = MIXER_SAMPLING_FREQUENCY_STKPI_UNSET;
    MixerSamplingFrequency = 0;
    NominalOutputSamplingFrequency = 0;
    MixerGranuleSize = 0;
    //History Variables
    FirstMixerSamplingFrequencyHistory = 0;
    MixerSamplingFrequencyHistory = 0;
    MixerSamplingFrequencyUpdated = true;
    FirstMixerSamplingFrequencyUpdated = true;
    FirstMixerGranuleSizeHistory = 0;
    MixerGranuleSizeHistory = 0;
    MixerGranuleSizeUpdated = true;
    FirstMixerGranuleSizeUpdated = true;
    OutputTopologyUpdated = true;
    FirstMixerClientAudioParamsUpdated = true;
    ClientAudioParamsUpdated = true;
    AudioGeneratorUpdated = true;
    IAudioUpdated = true;

    for (uint32_t CodedIndex(0); CodedIndex < MIXER_MAX_CODED_INPUTS; CodedIndex++)
    {
        PrimaryCodedDataType[CodedIndex] = PcmPlayer_c::OUTPUT_IEC60958;
    }

    ResetMixingMetadata();
    // Do not touch other parts of the OutputConfiguration. These are immune to reset.

    // Pre-configure the mixer command
    //
    memset(&MixerCommand, 0, sizeof(MixerCommand));
    MixerCommand.Command.StructSize = sizeof(MixerCommand.Command);
    MixerCommand.Command.CmdCode = MME_TRANSFORM;
    MixerCommand.Command.CmdEnd = MME_COMMAND_END_RETURN_NOTIFY;
    MixerCommand.Command.DueTime = 0;   // no sorting by time
    MixerCommand.Command.NumberInputBuffers = MIXER_AUDIO_MAX_INPUT_BUFFERS;
    // MixerCommand.Command.NumberOutputBuffers is not constant and must be set later
    MixerCommand.Command.DataBuffers_p = MixerCommand.DataBufferList;
    MixerCommand.Command.CmdStatus.AdditionalInfoSize = sizeof(MME_LxMixerTransformerFrameMix_ExtendedParams_t);
    MixerCommand.Command.CmdStatus.AdditionalInfo_p = &MixerCommand.OutputParams;
    MixerCommand.Command.ParamSize = sizeof(MixerCommand.InputParams);
    MixerCommand.Command.Param_p = &MixerCommand.InputParams;

    for (uint32_t i = 0; i < MIXER_AUDIO_MAX_BUFFERS; i++)
    {
        MixerCommand.DataBufferList[i] = &MixerCommand.DataBuffers[i];
        MixerCommand.DataBuffers[i].StructSize = sizeof(MixerCommand.DataBuffers[i]);
        MixerCommand.DataBuffers[i].UserData_p = &MixerCommand.BufferIndex[i * MIXER_AUDIO_PAGES_PER_BUFFER];
        MixerCommand.DataBuffers[i].ScatterPages_p = &MixerCommand.ScatterPages[i * MIXER_AUDIO_PAGES_PER_BUFFER];
        //MixerCommand.StreamNumber = i;
    }

    for (uint32_t i = 0; i < MIXER_AUDIO_MAX_PAGES; i++)
    {
        MixerCommand.BufferIndex[i] = NULL;
    }

    //
    // Pre-configure the mixer command
    //
    memset(&FirstMixerCommand, 0, sizeof(FirstMixerCommand));
    FirstMixerCommand.Command.StructSize = sizeof(FirstMixerCommand.Command);
    FirstMixerCommand.Command.CmdCode = MME_TRANSFORM;
    FirstMixerCommand.Command.CmdEnd = MME_COMMAND_END_RETURN_NOTIFY;
    FirstMixerCommand.Command.DueTime = 0;   // no sorting by time
    FirstMixerCommand.Command.NumberInputBuffers = FIRST_MIXER_MAX_INPUT_BUFFERS;
    FirstMixerCommand.Command.NumberOutputBuffers = FIRST_MIXER_MAX_OUTPUT_BUFFERS;
    FirstMixerCommand.Command.DataBuffers_p = FirstMixerCommand.DataBufferList;
    FirstMixerCommand.Command.CmdStatus.AdditionalInfoSize = sizeof(MME_LxMixerTransformerFrameMix_ExtendedParams_t);
    FirstMixerCommand.Command.CmdStatus.AdditionalInfo_p = &FirstMixerCommand.OutputParams;
    FirstMixerCommand.Command.ParamSize = sizeof(FirstMixerCommand.InputParams);
    FirstMixerCommand.Command.Param_p = &FirstMixerCommand.InputParams;

    for (uint32_t i = 0; i < FIRST_MIXER_MAX_BUFFERS; i++)
    {
        FirstMixerCommand.DataBufferList[i] = &FirstMixerCommand.DataBuffers[i];
        FirstMixerCommand.DataBuffers[i].StructSize = sizeof(FirstMixerCommand.DataBuffers[i]);
        FirstMixerCommand.DataBuffers[i].UserData_p = &FirstMixerCommand.BufferIndex[i * MIXER_AUDIO_PAGES_PER_BUFFER];
        FirstMixerCommand.DataBuffers[i].ScatterPages_p = &FirstMixerCommand.ScatterPages[i * MIXER_AUDIO_PAGES_PER_BUFFER];
    }

    for (uint32_t i = 0; i < FIRST_MIXER_MAX_PAGES; i++)
    {
        FirstMixerCommand.BufferIndex[i] = NULL;
    }

    //
    // Pre-configure the parameter update command
    //
    memset(&ParamsCommand, 0, sizeof(ParamsCommand));
    ParamsCommand.Command.StructSize = sizeof(ParamsCommand.Command);
    ParamsCommand.Command.CmdCode = MME_SET_GLOBAL_TRANSFORM_PARAMS;
    ParamsCommand.Command.CmdEnd = MME_COMMAND_END_RETURN_NOTIFY;
    ParamsCommand.Command.DueTime = 0;  // no sorting by time
    ParamsCommand.Command.NumberInputBuffers = 0;
    ParamsCommand.Command.NumberOutputBuffers = 0;
    ParamsCommand.Command.DataBuffers_p = NULL;
    ParamsCommand.Command.CmdStatus.State = MME_COMMAND_COMPLETED;
    ParamsCommand.Command.CmdStatus.AdditionalInfoSize = sizeof(ParamsCommand.OutputParams);
    ParamsCommand.Command.CmdStatus.AdditionalInfo_p = &ParamsCommand.OutputParams;
    ParamsCommand.Command.ParamSize = sizeof(ParamsCommand.InputParams);
    ParamsCommand.Command.Param_p = &ParamsCommand.InputParams;

    memset(&FirstMixerParamsCommand, 0, sizeof(FirstMixerParamsCommand));
    FirstMixerParamsCommand.Command.StructSize = sizeof(FirstMixerParamsCommand.Command);
    FirstMixerParamsCommand.Command.CmdCode = MME_SET_GLOBAL_TRANSFORM_PARAMS;
    FirstMixerParamsCommand.Command.CmdEnd = MME_COMMAND_END_RETURN_NOTIFY;
    FirstMixerParamsCommand.Command.DueTime = 0;  // no sorting by time
    FirstMixerParamsCommand.Command.NumberInputBuffers = 0;
    FirstMixerParamsCommand.Command.NumberOutputBuffers = 0;
    FirstMixerParamsCommand.Command.DataBuffers_p = NULL;
    FirstMixerParamsCommand.Command.CmdStatus.State = MME_COMMAND_COMPLETED;
    FirstMixerParamsCommand.Command.CmdStatus.AdditionalInfoSize = sizeof(FirstMixerParamsCommand.OutputParams);
    FirstMixerParamsCommand.Command.CmdStatus.AdditionalInfo_p = &FirstMixerParamsCommand.OutputParams;
    FirstMixerParamsCommand.Command.ParamSize = sizeof(FirstMixerParamsCommand.InputParams);
    FirstMixerParamsCommand.Command.Param_p = &FirstMixerParamsCommand.InputParams;

    return Mixer_c::Reset();
}

////////////////////////////////////////////////////////////////////////////
///
///
///
PlayerStatus_t Mixer_Mme_c::SetModuleParameters(unsigned int ParameterBlockSize, void *ParameterBlock)
{
    SE_DEBUG(group_mixer, ">%s #########################################\n", Name);

    if (ParameterBlockSize == sizeof(snd_pseudo_mixer_settings))
    {
        struct snd_pseudo_mixer_settings *SettingsForMixer = static_cast<struct snd_pseudo_mixer_settings *>(ParameterBlock);

        if (SettingsForMixer->magic == SND_PSEUDO_MIXER_MAGIC)
        {
            UpstreamConfiguration = SettingsForMixer;
            MixerSettings.SetTobeUpdated(*SettingsForMixer);

            // If play-back is not already running, allow output configuration immediately.
            if (PlayerNoError == Mixer_Mme_c::IsIdle())
            {
                PlayerStatus_t Status;
                // Review output configuration.
                MixerSettings.CheckAndUpdate();
                // Review players.
                CheckAndUpdateAllAudioPlayers();
                // Trigger updates to come accordingly (Mixer and PCM Players).
                // Since Idle state of mixer, no thread issue for these triggers.
                PcmPlayerNeedsParameterUpdate = true;
                MMENeedsParameterUpdate = true;
                Status = UpdatePlayerComponentsModuleParameters();

                if (Status != PlayerNoError)
                {
                    SE_ERROR("Failed to update normalized time offset or CodecControls (default value will be used instead)\n");
                }
            }

            return PlayerNoError;
        }
    }

    if (ParameterBlockSize == sizeof(struct snd_pseudo_transformer_name))
    {
        struct snd_pseudo_transformer_name *TransformerName = static_cast<struct snd_pseudo_transformer_name *>(ParameterBlock);

        if (TransformerName->magic == SND_PSEUDO_TRANSFORMER_NAME_MAGIC)
        {
            return UpdateTransformerId(TransformerName->name);
        }
    }

    return Mixer_c::SetModuleParameters(ParameterBlockSize, ParameterBlock);
}

///////////////////////////////////////////////////////////////////////////
///
///
///
PlayerStatus_t Mixer_Mme_c::SetOption(stm_se_ctrl_t ctrl, int value)
{
    SE_DEBUG(group_mixer, ">%s %s value= %d\n", Name, StringifyControl(ctrl), value);
    MixerOptionStruct Options = MixerSettings.GetLastOptions();

    switch (ctrl)
    {
    case STM_SE_CTRL_AUDIO_MIXER_GRAIN:
        if (mApplicationType == STM_SE_CTRL_VALUE_AUDIO_APPLICATION_MS11 ||
            mApplicationType == STM_SE_CTRL_VALUE_AUDIO_APPLICATION_MS12)
        {
            SE_INFO(group_mixer, "Cannot set STM_SE_CTRL_AUDIO_MIXER_GRAIN when using %s\n",
                    LookupPolicyValueAudioApplicationType(mApplicationType));
            return PlayerNoError;
        }
        Options.MasterGrain = value;
        break;

    case STM_SE_CTRL_STREAM_DRIVEN_STEREO:
        Options.Stream_driven_downmix_enable = value;
        break;

    case STM_SE_CTRL_AUDIO_GAIN:
        Options.MasterPlaybackVolume = value;
        break;

    case STM_SE_CTRL_VOLUME_MANAGER_AMOUNT:
        Options.VolumeManagerAmount = value;
        break;

    case STM_SE_CTRL_VIRTUALIZER_AMOUNT:
        Options.VirtualizerAmount = value;
        break;

    case STM_SE_CTRL_UPMIXER_AMOUNT:
        Options.UpmixerAmount = value;
        break;

    case STM_SE_CTRL_DIALOG_ENHANCER_AMOUNT:
        Options.DialogEnhancerAmount = value;
        break;

    case STM_SE_CTRL_BASS_ENHANCER_AMOUNT:
        Options.BassEnhancerAmount = value;
        break;

    case STM_SE_CTRL_EQUALIZER_AMOUNT:
        Options.EqualizerAmount = value;
        break;

    case STM_SE_CTRL_AUDIO_APPLICATION_TYPE:
        return SetApplicationType(value);

    default:
        SE_ERROR("Invalid control\n");
        return PlayerError;
    }

    Options.DebugDump();
    // Trigger update in mixer thread context.
    MixerSettings.SetTobeUpdated(Options);
    return PlayerNoError;
}

////////////////////////////////////////////////////////////////////////////
///
///
///
PlayerStatus_t Mixer_Mme_c::GetOption(stm_se_ctrl_t ctrl, int &value)  /*const*/
{
    const MixerOptionStruct Options = MixerSettings.GetLastOptions();

    switch (ctrl)
    {
    case STM_SE_CTRL_AUDIO_MIXER_GRAIN:
        value = Options.MasterGrain;
        break;

    case STM_SE_CTRL_STREAM_DRIVEN_STEREO:
        value = Options.Stream_driven_downmix_enable;
        break;

    case STM_SE_CTRL_AUDIO_GAIN:
        value = Options.MasterPlaybackVolume;
        break;

    case STM_SE_CTRL_VOLUME_MANAGER_AMOUNT:
        value = Options.VolumeManagerAmount;
        break;

    case STM_SE_CTRL_VIRTUALIZER_AMOUNT:
        value = Options.VirtualizerAmount;
        break;

    case STM_SE_CTRL_UPMIXER_AMOUNT:
        value = Options.UpmixerAmount;
        break;

    case STM_SE_CTRL_DIALOG_ENHANCER_AMOUNT:
        value = Options.DialogEnhancerAmount;
        break;

    case STM_SE_CTRL_BASS_ENHANCER_AMOUNT:
        value = Options.BassEnhancerAmount;
        break;

    case STM_SE_CTRL_EQUALIZER_AMOUNT:
        value = Options.EqualizerAmount;
        break;

    case STM_SE_CTRL_AUDIO_APPLICATION_TYPE:
        value = mApplicationType;
        break;

    default:
        SE_ERROR("Invalid control\n");
        return PlayerError;
    }

    SE_DEBUG(group_mixer, "%s %s value= %d\n", Name, StringifyControl(ctrl), value);

    return PlayerNoError;
}

////////////////////////////////////////////////////////////////////////////
///
///
///
PlayerStatus_t Mixer_Mme_c::SetCompoundOption(stm_se_ctrl_t ctrl, const void *value)
{
    MixerOptionStruct Options = MixerSettings.GetLastOptions();
    uint32_t AudioGeneratorIdx;
    SE_DEBUG(group_mixer, ">%s %s\n", Name, StringifyControl(ctrl));

    switch (ctrl)
    {
    case STM_SE_CTRL_OUTPUT_SAMPLING_FREQUENCY:
    {
        const stm_se_output_frequency_t *output_sfreq = static_cast<const stm_se_output_frequency_t *>(value);
        if (mApplicationType == STM_SE_CTRL_VALUE_AUDIO_APPLICATION_MS11 ||
            mApplicationType == STM_SE_CTRL_VALUE_AUDIO_APPLICATION_MS12)
        {
            SE_INFO(group_mixer, "Cannot set %s when using %s\n",
                    StringifyControl(ctrl), LookupPolicyValueAudioApplicationType(mApplicationType));
            return PlayerNoError;
        }
        Options.OutputSfreq.control   = output_sfreq->control;
        Options.OutputSfreq.frequency = output_sfreq->frequency;
    }
    break;

    case STM_SE_CTRL_SPEAKER_CONFIG:
    {
        const stm_se_audio_channel_assignment_t *speaker_config = static_cast<const stm_se_audio_channel_assignment_t *>(value);
        Options.SpeakerConfig = *speaker_config;
        SE_DEBUG(group_mixer, "%p mixer channel assignment [pair0:%d]-[pair1:%d]-[pair2:%d]-[pair3:%d]-[pair4:%d] malleable=%d\n",
                 this,
                 Options.SpeakerConfig.pair0,
                 Options.SpeakerConfig.pair1,
                 Options.SpeakerConfig.pair2,
                 Options.SpeakerConfig.pair3,
                 Options.SpeakerConfig.pair4,
                 Options.SpeakerConfig.malleable);
    }
    break;

    case STM_SE_CTRL_AUDIO_MIXER_INPUT_COEFFICIENT:
    {
        const stm_se_q3dot13_input_gain_t *input_gain = static_cast<const stm_se_q3dot13_input_gain_t *>(value);
        int port = LookupClientFromStream((class HavanaStream_c *) input_gain->input);

        /* Note :
           until STLinuxTV is modified to expose the handle of the input object via the input port , we'll
           consider that the port is a basic index */

        switch (port)
        {
        case STM_SE_CTRL_VALUE_AUDIO_MIXER_ROLE_PRIMARY:
        case STM_SE_CTRL_VALUE_AUDIO_MIXER_ROLE_IN3:
        case STM_SE_CTRL_VALUE_AUDIO_MIXER_ROLE_IN4:
            for (uint32_t ChannelIdx(0); ChannelIdx < SND_PSEUDO_MIXER_CHANNELS; ChannelIdx++)
            {
                Options.Gain[PrimaryClient + port][ChannelIdx] = input_gain->gain[remap_channels[ChannelIdx]];
                Options.Pan[PrimaryClient + port][ChannelIdx] = input_gain->panning[remap_channels[ChannelIdx]];
            }
            // Update flags
            Options.GainUpdated = true;
            Options.PanUpdated = true;

            break;

        case STM_SE_CTRL_VALUE_AUDIO_MIXER_ROLE_SECONDARY:
            for (uint32_t ChannelIdx(0); ChannelIdx < SND_PSEUDO_MIXER_CHANNELS; ChannelIdx++)
            {
                Options.Gain[SecondaryClient][ChannelIdx] = input_gain->gain[remap_channels[ChannelIdx]];
                Options.Pan[SecondaryClient][ChannelIdx] = input_gain->panning[remap_channels[ChannelIdx]];
            }
            // Update flags
            Options.GainUpdated = true;
            Options.PanUpdated = true;

            break;

        case STM_SE_CTRL_VALUE_MIXER_ROLE_AUDIO_GENERATOR_0:
        case STM_SE_CTRL_VALUE_MIXER_ROLE_AUDIO_GENERATOR_1:
        case STM_SE_CTRL_VALUE_MIXER_ROLE_AUDIO_GENERATOR_2:
        case STM_SE_CTRL_VALUE_MIXER_ROLE_AUDIO_GENERATOR_3:
        case STM_SE_CTRL_VALUE_MIXER_ROLE_AUDIO_GENERATOR_4:
        case STM_SE_CTRL_VALUE_MIXER_ROLE_AUDIO_GENERATOR_5:
        case STM_SE_CTRL_VALUE_MIXER_ROLE_AUDIO_GENERATOR_6:
        case STM_SE_CTRL_VALUE_MIXER_ROLE_AUDIO_GENERATOR_7:

            AudioGeneratorIdx = port - STM_SE_CTRL_VALUE_MIXER_ROLE_AUDIO_GENERATOR_0;

            for (uint32_t ChannelIdx(0); ChannelIdx < SND_PSEUDO_MIXER_CHANNELS; ChannelIdx++)
            {
                Options.AudioGeneratorGain[AudioGeneratorIdx][ChannelIdx] = input_gain->gain[remap_channels[ChannelIdx]];
                Options.AudioGeneratorPan[AudioGeneratorIdx][ChannelIdx] = input_gain->panning[remap_channels[ChannelIdx]];
            }
            // Update the flag
            Options.AudioGeneratorGainUpdated = true;
            Options.PanUpdated = true;

            break;

        case STM_SE_CTRL_VALUE_AUDIO_MIXER_ROLE_SFX0:
        case STM_SE_CTRL_VALUE_AUDIO_MIXER_ROLE_SFX1:
        case STM_SE_CTRL_VALUE_AUDIO_MIXER_ROLE_SFX2:
        case STM_SE_CTRL_VALUE_AUDIO_MIXER_ROLE_SFX3:
        case STM_SE_CTRL_VALUE_AUDIO_MIXER_ROLE_SFX4:
        case STM_SE_CTRL_VALUE_AUDIO_MIXER_ROLE_SFX5:
        case STM_SE_CTRL_VALUE_AUDIO_MIXER_ROLE_SFX6:
        case STM_SE_CTRL_VALUE_AUDIO_MIXER_ROLE_SFX7:
            port -= STM_SE_CTRL_VALUE_AUDIO_MIXER_ROLE_SFX0; // Substract interactive port offset so can index 0-7
            Options.InteractiveGain[port] = input_gain->gain[0];
            // Update the flag
            Options.InteractiveGainUpdated = true;

            for (uint32_t ChannelIdx(0); ChannelIdx < SND_PSEUDO_MIXER_CHANNELS; ChannelIdx++)
            {
                Options.InteractivePan[port][ChannelIdx] = (int)input_gain->panning[remap_channels[ChannelIdx]];
            }

            // Update the flag
            Options.InteractivePanUpdated = true;
            break;

        default:
            SE_ERROR("Invalid port,  %d\n", port);
            return PlayerError;
            break;
        }
    }
    break;

    case STM_SE_CTRL_AUDIO_MIXER_INPUT_TYPE:
    {
        const stm_se_input_mixing_type_t *mixType = static_cast<const stm_se_input_mixing_type_t *>(value);
        switch (mixType->input)
        {
        case STM_SE_CTRL_VALUE_MIXER_ROLE_AUDIO_GENERATOR_0:
        case STM_SE_CTRL_VALUE_MIXER_ROLE_AUDIO_GENERATOR_1:
        case STM_SE_CTRL_VALUE_MIXER_ROLE_AUDIO_GENERATOR_2:
        case STM_SE_CTRL_VALUE_MIXER_ROLE_AUDIO_GENERATOR_3:
        case STM_SE_CTRL_VALUE_MIXER_ROLE_AUDIO_GENERATOR_4:
        case STM_SE_CTRL_VALUE_MIXER_ROLE_AUDIO_GENERATOR_5:
        case STM_SE_CTRL_VALUE_MIXER_ROLE_AUDIO_GENERATOR_6:
        case STM_SE_CTRL_VALUE_MIXER_ROLE_AUDIO_GENERATOR_7:
        {
            AudioGeneratorIdx = mixType->input - STM_SE_CTRL_VALUE_MIXER_ROLE_AUDIO_GENERATOR_0;
            Options.AudioGeneratorMixing[AudioGeneratorIdx] = mixType->input_type;
            SE_DEBUG(group_mixer, "<: %s AudioGenerator[%d]: %d\n",
                     StringifyControl(ctrl), AudioGeneratorIdx, mixType->input_type);
            break;
        }

        default:
            SE_WARNING("%s is only supported for AudioGenerator inputs\n", StringifyControl(ctrl));
            return PlayerError;
            break;
        }
        break;
    }

    case STM_SE_CTRL_AUDIO_MIXER_OUTPUT_GAIN:
    {
        const stm_se_q3dot13_output_gain_t *output_gain = static_cast<const stm_se_q3dot13_output_gain_t *>(value);
        Options.PostMixGain = (int) output_gain->gain[0];
        // Update the flag
        Options.PostMixGainUpdated = true;
        break;
    }

    case STM_SE_CTRL_AUDIO_DYNAMIC_RANGE_COMPRESSION:
    {
        const stm_se_drc_t drc  = *(static_cast<const stm_se_drc_t *>(value));
        Options.Drc = drc;
        break;
    }

    case STM_SE_CTRL_VOLUME_MANAGER:
    {
        if (CheckMixerTuningProfileValidity((struct MME_PcmProcParamsHeader_s *) value, STM_SE_CTRL_VOLUME_MANAGER) != 0)
        {
            return PlayerError;
        }
        SetMixerTuningProfile((struct MME_PcmProcParamsHeader_s *) value, &Options.VolumeManagerParams);
        break;
    }

    case STM_SE_CTRL_VIRTUALIZER:
    {
        if (CheckMixerTuningProfileValidity((struct MME_PcmProcParamsHeader_s *) value, STM_SE_CTRL_VIRTUALIZER) != 0)
        {
            return PlayerError;
        }
        SetMixerTuningProfile((struct MME_PcmProcParamsHeader_s *) value, &Options.VirtualizerParams);
        break;
    }

    case STM_SE_CTRL_UPMIXER:
    {
        if (CheckMixerTuningProfileValidity((struct MME_PcmProcParamsHeader_s *) value, STM_SE_CTRL_UPMIXER) != 0)
        {
            return PlayerError;
        }
        SetMixerTuningProfile((struct MME_PcmProcParamsHeader_s *) value, &Options.UpmixerParams);
        break;
    }

    case STM_SE_CTRL_DIALOG_ENHANCER:
    {
        if (CheckMixerTuningProfileValidity((struct MME_PcmProcParamsHeader_s *) value, STM_SE_CTRL_DIALOG_ENHANCER) != 0)
        {
            return PlayerError;
        }
        SetMixerTuningProfile((struct MME_PcmProcParamsHeader_s *) value, &Options.DialogEnhancerParams);
        break;
    }

    case STM_SE_CTRL_EQUALIZER:
    {
        if (CheckMixerTuningProfileValidity((struct MME_PcmProcParamsHeader_s *) value, STM_SE_CTRL_EQUALIZER) != 0)
        {
            return PlayerError;
        }
        SetMixerTuningProfile((struct MME_PcmProcParamsHeader_s *) value, &Options.EqualizerParams);
        break;
    }

    case STM_SE_CTRL_BASS_ENHANCER:
    {
        if (CheckMixerTuningProfileValidity((struct MME_PcmProcParamsHeader_s *) value, STM_SE_CTRL_BASS_ENHANCER) != 0)
        {
            return PlayerError;
        }
        SetMixerTuningProfile((struct MME_PcmProcParamsHeader_s *) value, &Options.BassEnhancerParams);
        break;
    }

    default:
        SE_ERROR("Invalid control\n");
        return PlayerError;
    }

    Options.DebugDump();
    // Trigger update in mixer thread context.
    MixerSettings.SetTobeUpdated(Options);
    return PlayerNoError;
}

////////////////////////////////////////////////////////////////////////////
///
///
///
PlayerStatus_t Mixer_Mme_c::GetCompoundOption(stm_se_ctrl_t ctrl, void *value) /*const*/
{
    const MixerOptionStruct MixerOptions = MixerSettings.GetLastOptions();
    SE_VERBOSE(group_mixer, ">%s %s\n", Name, StringifyControl(ctrl));

    switch (ctrl)
    {
    case STM_SE_CTRL_OUTPUT_SAMPLING_FREQUENCY:
    {
        stm_se_output_frequency_t *output_sfreq = static_cast<stm_se_output_frequency_t *>(value);
        output_sfreq->control   = MixerOptions.OutputSfreq.control;
        output_sfreq->frequency = MixerOptions.OutputSfreq.frequency;
        SE_DEBUG(group_mixer, "<: STM_SE_CTRL_OUTPUT_SAMPLING_FREQUENCY freq:%d\n",
                 output_sfreq->frequency);
        break;
    }

    case STM_SE_CTRL_SPEAKER_CONFIG:
    {
        stm_se_audio_channel_assignment_t *speaker_config = static_cast<stm_se_audio_channel_assignment_t *>(value);
        *speaker_config = MixerOptions.SpeakerConfig;
        SE_DEBUG(group_mixer, "<: STM_SE_CTRL_SPEAKER_CONFIG\n");
        break;
    }

    case STM_SE_CTRL_AUDIO_MIXER_INPUT_COEFFICIENT:
    {
        stm_se_q3dot13_input_gain_t *input_gain = static_cast<stm_se_q3dot13_input_gain_t *>(value);
        int port = LookupClientFromStream((class HavanaStream_c *) input_gain->input);
        SE_DEBUG(group_mixer, "<: STM_SE_CTRL_AUDIO_MIXER_INPUT_COEFFICIENT\n");

        switch (port)
        {
        case STM_SE_CTRL_VALUE_AUDIO_MIXER_ROLE_PRIMARY:
        case STM_SE_CTRL_VALUE_AUDIO_MIXER_ROLE_IN3:
        case STM_SE_CTRL_VALUE_AUDIO_MIXER_ROLE_IN4:
            for (int i = 0; i < SND_PSEUDO_MIXER_CHANNELS; i++)
            {
                input_gain->gain   [remap_channels[i]] = MixerOptions.Gain[PrimaryClient + port][i];
                input_gain->panning[remap_channels[i]] = MixerOptions.Pan [PrimaryClient + port][i];
            }

            break;

        case STM_SE_CTRL_VALUE_AUDIO_MIXER_ROLE_SECONDARY:
            for (int i = 0; i < SND_PSEUDO_MIXER_CHANNELS; i++)
            {
                input_gain->gain   [remap_channels[i]] = MixerOptions.Gain[SecondaryClient][i];
                input_gain->panning[remap_channels[i]] = MixerOptions.Pan [SecondaryClient][i];
            }

            break;

        case STM_SE_CTRL_VALUE_MIXER_ROLE_AUDIO_GENERATOR_0:
        case STM_SE_CTRL_VALUE_MIXER_ROLE_AUDIO_GENERATOR_1:
        case STM_SE_CTRL_VALUE_MIXER_ROLE_AUDIO_GENERATOR_2:
        case STM_SE_CTRL_VALUE_MIXER_ROLE_AUDIO_GENERATOR_3:
        case STM_SE_CTRL_VALUE_MIXER_ROLE_AUDIO_GENERATOR_4:
        case STM_SE_CTRL_VALUE_MIXER_ROLE_AUDIO_GENERATOR_5:
        case STM_SE_CTRL_VALUE_MIXER_ROLE_AUDIO_GENERATOR_6:
        case STM_SE_CTRL_VALUE_MIXER_ROLE_AUDIO_GENERATOR_7:

            uint32_t AudioGeneratorIdx;
            AudioGeneratorIdx = port - STM_SE_CTRL_VALUE_MIXER_ROLE_AUDIO_GENERATOR_0;

            for (uint32_t ChannelIdx(0); ChannelIdx < SND_PSEUDO_MIXER_CHANNELS; ChannelIdx++)
            {
                input_gain->gain[remap_channels[ChannelIdx]] = MixerOptions.AudioGeneratorGain[AudioGeneratorIdx][ChannelIdx];
                input_gain->panning[remap_channels[ChannelIdx]] = MixerOptions.AudioGeneratorPan[AudioGeneratorIdx][ChannelIdx];
            }

            break;

        default: // STM_SE_CTRL_VALUE_AUDIO_MIXER_ROLE_SFX#
            port -= STM_SE_CTRL_VALUE_AUDIO_MIXER_ROLE_SFX0;

            for (int i = 0; i < SND_PSEUDO_MIXER_CHANNELS; i++)
            {
                input_gain->gain   [remap_channels[i]] = MixerOptions.InteractiveGain[port];
                input_gain->panning[remap_channels[i]] = MixerOptions.InteractivePan[port][i];
            }
        }

        break;
    }

    case STM_SE_CTRL_AUDIO_MIXER_INPUT_TYPE:
    {
        stm_se_input_mixing_type_t *mixType = static_cast<stm_se_input_mixing_type_t *>(value);

        switch (mixType->input)
        {
        case STM_SE_CTRL_VALUE_MIXER_ROLE_AUDIO_GENERATOR_0:
        case STM_SE_CTRL_VALUE_MIXER_ROLE_AUDIO_GENERATOR_1:
        case STM_SE_CTRL_VALUE_MIXER_ROLE_AUDIO_GENERATOR_2:
        case STM_SE_CTRL_VALUE_MIXER_ROLE_AUDIO_GENERATOR_3:
        case STM_SE_CTRL_VALUE_MIXER_ROLE_AUDIO_GENERATOR_4:
        case STM_SE_CTRL_VALUE_MIXER_ROLE_AUDIO_GENERATOR_5:
        case STM_SE_CTRL_VALUE_MIXER_ROLE_AUDIO_GENERATOR_6:
        case STM_SE_CTRL_VALUE_MIXER_ROLE_AUDIO_GENERATOR_7:
        {
            uint32_t AudioGeneratorIdx = mixType->input - STM_SE_CTRL_VALUE_MIXER_ROLE_AUDIO_GENERATOR_0;
            mixType->input_type = MixerOptions.AudioGeneratorMixing[AudioGeneratorIdx];
            SE_DEBUG(group_mixer, "<: STM_SE_CTRL_AUDIO_MIXER_INPUT_TYPE AudioGenerator[%d]: %d\n",
                     AudioGeneratorIdx, mixType->input_type);
            break;
        }

        default:
            SE_WARNING("STM_SE_CTRL_AUDIO_MIXER_INPUT_TYPE is only supported for AudioGenerator inputs\n");
            return PlayerError;
            break;
        }
        break;
    }

    case STM_SE_CTRL_AUDIO_MIXER_OUTPUT_GAIN:
    {
        stm_se_q3dot13_output_gain_t *output_gain = static_cast<stm_se_q3dot13_output_gain_t *>(value);
        output_gain->gain[0] = MixerOptions.PostMixGain;
        SE_DEBUG(group_mixer, "<: STM_SE_CTRL_AUDIO_MIXER_OUTPUT_GAIN PostMixGain:%d\n",
                 MixerOptions.PostMixGain);
        break;
    }

    case STM_SE_CTRL_AUDIO_DYNAMIC_RANGE_COMPRESSION:
    {
        stm_se_drc_t *drc = static_cast<stm_se_drc_t *>(value);
        SE_DEBUG(group_mixer, "<: STM_SE_CTRL_AUDIO_DYNAMIC_RANGE_COMPRESSION\n");

        *drc = MixerOptions.Drc;
    }
    break;

    default:
        SE_ERROR("Invalid control\n");
        return PlayerError;
    }

    return PlayerNoError;
}

////////////////////////////////////////////////////////////////////////////
///
///
/// This is a synchronous function.
/// This function locks, sets event and waits event so be sure that you
/// call it in a thread state that allows that: in particular DO NOT CALL IT
/// in a spin locked code.
///
PlayerStatus_t Mixer_Mme_c::SendSetPcmParamsReceivedWhileStarting()
{
    if (ThreadState.GetState() != ThreadState_c::Starting || PcmParamsReceivedWhileStarting == true)
    {
        SE_DEBUG(group_mixer, "Mixer is not in starting state. Don't send request\n");
        return PlayerNoError;
    }

    MixerRequest_c request;

    request.SetFunctionToManageTheRequest(&Mixer_Mme_c::SetPcmParamsReceivedWhileStarting);

    PlayerStatus_t status = MixerRequestManager.SendRequest(request);

    if (status != PlayerNoError)
    {
        SE_ERROR("SendRequest failed status:%d\n", status);
    }

    return status;
}

////////////////////////////////////////////////////////////////////////////
///
/// A wrapper to the Mixer_Mme_c::RegisterManifestor( Manifestor_AudioKsound_c * Manifestor , class HavanaStream_c * Stream)
/// method.
///
PlayerStatus_t Mixer_Mme_c::SetPcmParamsReceivedWhileStarting(MixerRequest_c &aMixerRequest_c)
{
    (void)aMixerRequest_c; // warning removal
    SE_DEBUG(group_mixer, "@\n");
    PcmParamsReceivedWhileStarting = true;
    return PlayerNoError;
}


////////////////////////////////////////////////////////////////////////////
///
///
/// This is a synchronous function.
/// This function locks, sets event and waits event so be sure that you
/// call it in a thread state that allows that: in particular DO NOT CALL IT
/// in a spin locked code.
///
PlayerStatus_t Mixer_Mme_c::SendRegisterManifestorRequest(Manifestor_AudioKsound_c *Manifestor,
                                                          class HavanaStream_c *Stream,
                                                          stm_se_sink_input_port_t input_port)

{
    MixerRequest_c request;

    request.SetHavanaStream(Stream);
    request.SetManifestor(Manifestor);
    request.SetInputPort(input_port);
    request.SetFunctionToManageTheRequest(&Mixer_Mme_c::RegisterManifestor);

    PlayerStatus_t status = MixerRequestManager.SendRequest(request);

    if (status != PlayerNoError)
    {
        SE_ERROR("SendRequest failed status:%d\n", status);
    }

    return status;
}
////////////////////////////////////////////////////////////////////////////
///
/// A wrapper to the Mixer_Mme_c::RegisterManifestor( Manifestor_AudioKsound_c * Manifestor , class HavanaStream_c * Stream)
/// method.
///
PlayerStatus_t Mixer_Mme_c::RegisterManifestor(MixerRequest_c &aMixerRequest_c)
{
    PlayerStatus_t Status;
    Manifestor_AudioKsound_c *Manifestor = aMixerRequest_c.GetManifestor();
    class HavanaStream_c *Stream = aMixerRequest_c.GetHavanaStream();
    stm_se_sink_input_port_t input_port = aMixerRequest_c.GetInputPort();
    Status = RegisterManifestor(Manifestor, Stream, input_port);
    return Status;
}


////////////////////////////////////////////////////////////////////////////
///
///
///
PlayerStatus_t Mixer_Mme_c::RegisterManifestor(Manifestor_AudioKsound_c *Manifestor , class HavanaStream_c *Stream, stm_se_sink_input_port_t input_port)
{
    SE_DEBUG(group_mixer, ">%s Manifestor:%p Stream:%p input_port:%d\n", Name, Manifestor, Stream, input_port);

    if (input_port >= mTopology.nb_max_decoded_audio)
    {
        SE_ERROR("Invalid port index:%d\n", input_port);
        return PlayerNotSupported;
    }

    Client[input_port].LockTake();

    if (Client[input_port].IsMyManifestor(Manifestor))
    {
        // Manifestor is already registered for this client.
        Client[input_port].LockRelease();
        SE_ERROR("<: Manifestor %p-%d is already registered\n", Manifestor, input_port);
        return PlayerError;
    }
    if (Client[input_port].GetState() != DISCONNECTED)
    {
        // This client does not own this manifestor, but is already taken.
        Client[input_port].LockRelease();
        SE_ERROR("<: Cannot register Manifestor%p input_port%d is already connected\n", Manifestor, input_port);
        return PlayerTooMany;
    }

    // This client is free, so take it.
    Client[input_port].RegisterManifestor(Manifestor, Stream);
    Client[input_port].LockRelease();
    PlayerStatus_t status;
    status = CheckClientsState();

    if (status != PlayerNoError)
    {
        // Undo previous register operation.
        Client[input_port].DeRegisterManifestor();
        SE_ERROR("Error returned by CheckClientsState\n");
        return PlayerError;
    }

    return PlayerNoError;
}


////////////////////////////////////////////////////////////////////////////
///
/// This is a synchronous function.
///
/// This function should be used to start the manifestor but
/// the manifestor parameters MUST have been provided first
/// thanks to Mixer_Mme_c::UpdateManifestorParameters method
///
PlayerStatus_t Mixer_Mme_c::SendStartManifestorRequest(const Manifestor_AudioKsound_c *Manifestor)
{
    SE_DEBUG(group_mixer, ">\n");
    if (Manifestor == NULL)
    {
        SE_ERROR("< NULL Manifestor\n");
        return PlayerError;
    }

    if (MMEFirstMixerHdl != MME_INVALID_ARGUMENT)
    {
        SE_ASSERT(FirstMixerClientAudioParamsUpdated != false);
    }
    else
    {
        SE_ASSERT(ClientAudioParamsUpdated != false);
    }

    uint32_t ClientIdx;

    for (ClientIdx = 0; ClientIdx < MIXER_MAX_CLIENTS; ClientIdx++)
    {
        if (Client[ClientIdx].IsMyManifestor(Manifestor))
        {
            Client[ClientIdx].SetState(STARTING);

            // This will wakeup MixerPlaybackThread
            PlayerStatus_t status = SendSetPcmParamsReceivedWhileStarting();
            if (status != PlayerNoError)
            {
                SE_ERROR("< SendSetPcmParamsReceivedWhileStarting failed status:%d\n", status);
                return status;
            }
            break;
        }
    }

    if (ClientIdx == MIXER_MAX_CLIENTS)
    {
        SE_ERROR("< Invalid Manifestor 0x%p\n", Manifestor);
        return PlayerError;
    }

    SE_DEBUG(group_mixer, "<\n");
    return PlayerNoError;
}


////////////////////////////////////////////////////////////////////////////
///
///
/// This is a synchronous function.
/// This function locks, sets event and waits event so be sure that you
/// call it in a thread state that allows that: in particular DO NOT CALL IT
/// in a spin locked code.
///
PlayerStatus_t Mixer_Mme_c::SendGetProcessingRingBufCountRequest(Manifestor_AudioKsound_c *Manifestor, int *bufCountPtr)
{
    PlayerStatus_t status;
    MixerRequest_c request;
    request.SetManifestor(Manifestor);
    request.SetFunctionToManageTheRequest(&Mixer_Mme_c::GetProcessingRingBufCount);
    status = MixerRequestManager.SendRequest(request);

    if (status != PlayerNoError)
    {
        SE_ERROR("SendRequest failed status:%d\n", status);
    }

    *bufCountPtr = request.mIntResult;
    SE_DEBUG(group_mixer, "NbOfProcessingBuf:%d\n", *bufCountPtr);
    return status;
}

PlayerStatus_t Mixer_Mme_c::GetProcessingRingBufCount(MixerRequest_c &aMixerRequest_c)
{
    return GetProcessingRingBufCount(aMixerRequest_c.GetManifestor(), &aMixerRequest_c.mIntResult);
}

PlayerStatus_t Mixer_Mme_c::GetProcessingRingBufCount(Manifestor_AudioKsound_c *Manifestor, int *bufCountPtr)
{
    uint32_t ClientIdx(0);
    for (ClientIdx = 0; ClientIdx < MIXER_MAX_CLIENTS; ClientIdx++)
    {
        Client[ClientIdx].LockTake();

        if (Client[ClientIdx].IsMyManifestor(Manifestor))
        {
            Manifestor->GetProcessingRingBufCount(bufCountPtr);
            SE_DEBUG(group_mixer, "NbOfProcessingBuf:%d\n", *bufCountPtr);
            Client[ClientIdx].LockRelease();
            return PlayerNoError;
        }

        Client[ClientIdx].LockRelease();
    }

    SE_ERROR("Unknown Manifestor:%p\n", Manifestor);
    return PlayerError;
}

////////////////////////////////////////////////////////////////////////////
///
///
/// This is a synchronous function.
/// This function locks, sets event and waits event so be sure that you
/// call it in a thread state that allows that: in particular DO NOT CALL IT
/// in a spin locked code.
///
PlayerStatus_t Mixer_Mme_c::SendDeRegisterManifestorRequest(Manifestor_AudioKsound_c *Manifestor)

{
    PlayerStatus_t status;
    MixerRequest_c request;
    request.SetManifestor(Manifestor);
    request.SetFunctionToManageTheRequest(&Mixer_Mme_c::DeRegisterManifestor);
    status = MixerRequestManager.SendRequest(request);

    if (status != PlayerNoError)
    {
        SE_ERROR("SendRequest failed status:%d\n", status);
    }

    return status;
}
////////////////////////////////////////////////////////////////////////////
///
/// A wrapper to the Mixer_Mme_c::DeRegisterManifestor( Manifestor_AudioKsound_c * Manifestor )
/// method.
///
PlayerStatus_t Mixer_Mme_c::DeRegisterManifestor(MixerRequest_c &aMixerRequest_c)
{
    PlayerStatus_t Status;
    Manifestor_AudioKsound_c *Manifestor = aMixerRequest_c.GetManifestor();
    Status = DeRegisterManifestor(Manifestor);
    return Status;
}
////////////////////////////////////////////////////////////////////////////
///
/// Remove the specified manifestor from the client table.
///
/// \todo This method must re-order the table if a client is disconnected from
///       the middle (due to very simple mappings from clients to buffers elsewhere
///       in this class). Until that happens expect to see odd things for DISCONNECTED
///       clients.
///
PlayerStatus_t Mixer_Mme_c::DeRegisterManifestor(Manifestor_AudioKsound_c *Manifestor)
{
    uint32_t ClientIdx(0);
    SE_DEBUG(group_mixer, ">%s %p\n", Name, Manifestor);

    for (ClientIdx = 0; ClientIdx < MIXER_MAX_CLIENTS; ClientIdx++)
    {
        Client[ClientIdx].LockTake();

        if (Client[ClientIdx].IsMyManifestor(Manifestor))
        {
            if (Client[ClientIdx].GetState() != STOPPED)
            {
                SE_ERROR("<: Cannot deregister an input from state %s\n", LookupInputState(Client[ClientIdx].GetState()));
                Client[ClientIdx].LockRelease();
                // Return now
                return PlayerError;
            }
            else
            {
                Client[ClientIdx].DeRegisterManifestor();
                Client[ClientIdx].LockRelease();
                PlayerStatus_t status;
                status = CheckClientsState();

                if (status != PlayerNoError)
                {
                    SE_ERROR("Error returned by CheckClientsState\n");
                    return PlayerError;
                }

                // Client correctly done.
                break;
            }
        }
        else
        {
            Client[ClientIdx].LockRelease();
        }
    }

    if (ClientIdx >= MIXER_MAX_CLIENTS)
    {
        //No client previously registered for this manifestor.
        SE_ERROR("<: Manifestor %p is not registered\n", Manifestor);
        return PlayerError;
    }

    return PlayerNoError;
}
////////////////////////////////////////////////////////////////////////////
///
///
/// This is a synchronous function.
/// This function locks, sets event and waits event so be sure that you
/// call it in a thread state that allows that: in particular DO NOT CALL IT
/// in a spin locked code.
///
PlayerStatus_t Mixer_Mme_c::SendEnableManifestorRequest(Manifestor_AudioKsound_c *Manifestor)

{
    PlayerStatus_t status;
    MixerRequest_c request;
    SE_DEBUG(group_mixer, ">%s %p\n", Name, Manifestor);
    request.SetManifestor(Manifestor);
    request.SetFunctionToManageTheRequest(&Mixer_Mme_c::EnableManifestor);
    status = MixerRequestManager.SendRequest(request);

    if (status != PlayerNoError)
    {
        SE_ERROR("SendRequest failed status:%d\n", status);
    }

    SE_DEBUG(group_mixer, "<:%p\n", Manifestor);
    return status;
}

////////////////////////////////////////////////////////////////////////////
///
/// A wrapper to the Mixer_Mme_c::EnableManifestor( Manifestor_AudioKsound_c * Manifestor )
/// method.
///
PlayerStatus_t Mixer_Mme_c::EnableManifestor(MixerRequest_c &aMixerRequest_c)
{
    PlayerStatus_t Status;
    Manifestor_AudioKsound_c *Manifestor = aMixerRequest_c.GetManifestor();
    Status = EnableManifestor(Manifestor);
    return Status;
}


////////////////////////////////////////////////////////////////////////////
///
///
///
PlayerStatus_t Mixer_Mme_c::EnableManifestor(Manifestor_AudioKsound_c *Manifestor)
{
    uint32_t ClientIdx(0);

    for (ClientIdx = 0; ClientIdx < MIXER_MAX_CLIENTS; ClientIdx++)
    {
        Client[ClientIdx].LockTake();

        if (Client[ClientIdx].IsMyManifestor(Manifestor))
        {
            // Manifestor is found.
            if (Client[ClientIdx].GetState() != STOPPED)
            {
                SE_ERROR("<: Cannot enable an input from state %s\n", LookupInputState(Client[ClientIdx].GetState()));
                Client[ClientIdx].LockRelease();
                // Return now
                return PlayerError;
            }
            else
            {
                // we must move out of the STOPPED state before calling UpdatePlayerComponentsModuleParameters()
                // otherwise the update will not take place.
                Client[ClientIdx].SetState(UNCONFIGURED);
                PlayerStatus_t Status = UpdatePlayerComponentsModuleParameters();

                if (Status != PlayerNoError)
                {
                    SE_ERROR("Failed to update normalized time offset (a zero value will be used instead)\n");
                    // no error recovery needed
                }

                Client[ClientIdx].LockRelease();
                Status = CheckClientsState();

                if (Status != PlayerNoError)
                {
                    SE_ERROR("Error returned by CheckClientsState\n");
                    return PlayerError;
                }
            }

            // The job is done, so exit now
            break;
        }
        else
        {
            // This client does not own this manifestor.
            Client[ClientIdx].LockRelease();
        }
    }

    if (ClientIdx >= MIXER_MAX_CLIENTS)
    {
        //Not enough client for this manifestor registration.
        SE_ERROR("<: Manifestor %p is not registered\n\n", Manifestor);
        return PlayerError;
    }

    return PlayerNoError;
}
////////////////////////////////////////////////////////////////////////////
///
///
/// This is a synchronous function.
/// This function locks, sets event and waits event so be sure that you
/// call it in a thread state that allows that: in particular DO NOT CALL IT
/// in a spin locked code.
///
PlayerStatus_t Mixer_Mme_c::SendDisableManifestorRequest(Manifestor_AudioKsound_c *Manifestor)

{
    MixerRequest_c request;
    SE_DEBUG(group_mixer, ">%s %p\n", Name, Manifestor);

    request.SetManifestor(Manifestor);
    request.SetFunctionToManageTheRequest(&Mixer_Mme_c::DisableManifestor);
    PlayerStatus_t status = MixerRequestManager.SendRequest(request);

    if (status != PlayerNoError)
    {
        SE_ERROR("SendRequest failed status:%d\n", status);
    }

    if (ThreadState.GetState() == ThreadState_c::Idle)
    {
        SetStoppingClientsToStoppedState();
    }

    // The previous request has just set the state to STOPPING. Now we wait for the
    // playback thread to set it to STOPPED.
    for (uint32_t ClientIdx(0); ClientIdx < MIXER_MAX_CLIENTS; ClientIdx++)
    {
        for (uint32_t LoopCount(0); Client[ClientIdx].GetState() == STOPPING; LoopCount++)
        {
            if (LoopCount > 100)
            {
                SE_ERROR("Time out waiting for manifestor %p-%d to enter STOPPED state\n",
                         Manifestor, ClientIdx);
                return PlayerError;
            }

            OS_SleepMilliSeconds(10);
        }
    }

    return status;
}

////////////////////////////////////////////////////////////////////////////
///
/// A wrapper to the Mixer_Mme_c::DisableManifestor( Manifestor_AudioKsound_c * Manifestor )
/// method.
///
PlayerStatus_t Mixer_Mme_c::DisableManifestor(MixerRequest_c &aMixerRequest_c)
{
    PlayerStatus_t Status;
    Manifestor_AudioKsound_c *Manifestor = aMixerRequest_c.GetManifestor();
    Status = DisableManifestor(Manifestor);
    return Status;
}


////////////////////////////////////////////////////////////////////////////
///
///
///
PlayerStatus_t Mixer_Mme_c::DisableManifestor(Manifestor_AudioKsound_c *Manifestor)
{
    uint32_t ClientIdx(0);

    for (ClientIdx = 0; ClientIdx < MIXER_MAX_CLIENTS; ClientIdx++)
    {
        Client[ClientIdx].LockTake();

        if (Client[ClientIdx].IsMyManifestor(Manifestor))
        {
            // Manifestor is found.
            if (Client[ClientIdx].GetState() == STOPPED || Client[ClientIdx].GetState() == DISCONNECTED)
            {
                SE_ERROR("Cannot disable an input from state %s\n", LookupInputState(Client[ClientIdx].GetState()));
                Client[ClientIdx].LockRelease();
                // Return now
                return PlayerError;
            }
            // Note that whilst the writes we are about to do to the state variable are atomic we are not
            // permitted to enter the STOPPED or STOPPING state until it is safe to do so; all code that
            // makes it unsafe to enter this state owns the mutex.
            else if (Client[ClientIdx].GetState() == UNCONFIGURED)
            {
                // If we are in the UNCONFIGURED state then the mixer main thread has not yet started
                // interacting with the manifestor. this means we can (and, in fact, must) go straight
                // to the STOPPED state without waiting for a handshake from the main thread.
                Client[ClientIdx].SetState(STOPPED);
                Client[ClientIdx].LockRelease();
            }
            else
            {
                // When the playback thread observes a client in the STOPPING state it will finalize the input
                // (request a swan song from the manifestor and then release all resources) before placing the
                // client into the STOPPED state.
                Client[ClientIdx].SetState(STOPPING);
                Client[ClientIdx].LockRelease();
                // We are here in the playback thread and we cannot set the client state to STOPPED right now
                // because we need to loop one more time in the playbac thread loop so as to do a fading and
                // other stuff. So we will wait on the STOPPED state in the client thread before returning from the
                // "disable manifestor request".
            }

            PlayerStatus_t status;
            status = CheckClientsState();

            if (status != PlayerNoError)
            {
                SE_ERROR("Error returned by CheckClientsState\n");
                return PlayerError;
            }

            // The job is done, so exit now
            break;
        }
        else
        {
            // This client does not own this manifestor.
            Client[ClientIdx].LockRelease();
        }
    }

    if (ClientIdx >= MIXER_MAX_CLIENTS)
    {
        //Did not found client for this
        SE_ERROR("<: Manifestor %p is not registered\n\n", Manifestor);
        return PlayerError;
    }

    return PlayerNoError;
}
////////////////////////////////////////////////////////////////////////////
///
///
///
void Mixer_Mme_c::GetDRCParameters(DRCParams_t *DRC)
{
    if (mApplicationType == STM_SE_CTRL_VALUE_AUDIO_APPLICATION_MS12)
    {
        // If DDRE Enabled DRC/Comp handled in Mixer, decoder must always be in Line Mode with no DRC applied
        DRC->DRC_Enable = 0;                        // ON/OFF switch of DRC
        DRC->DRC_Type   = STM_SE_COMP_LINE_OUT;     // Line/RF/Custom mode
        DRC->DRC_HDR    = 0;                        // cut factor (attenuating high dynamic range signal)
        DRC->DRC_LDR    = 0;                        // boost factor (boosting low dynamic range signal)
    }
    else
    {
        DRC->DRC_Enable = MixerSettings.MixerOptions.Drc.mode != STM_SE_NO_COMPRESSION ; // ON/OFF switch of DRC
        DRC->DRC_Type   = MixerSettings.MixerOptions.Drc.mode;     // Line/RF/Custom mode
        DRC->DRC_HDR    = MixerSettings.MixerOptions.Drc.cut;      // cut factor (attenuating high dynamic range signal)
        DRC->DRC_LDR    = MixerSettings.MixerOptions.Drc.boost;    // boost factor (boosting low dynamic range signal)
    }
}


////////////////////////////////////////////////////////////////////////////
///
///
///
PlayerStatus_t Mixer_Mme_c::UpdateManifestorParameters(Manifestor_AudioKsound_c *Manifestor,
                                                       ParsedAudioParameters_t *ParsedAudioParameters,
                                                       bool TakeMixerClientLock)
{
    uint32_t ClientIdx(0);
    SE_DEBUG(group_mixer, ">%s %p %s\n", Name, Manifestor, TakeMixerClientLock ? "true" : "false");

    //
    // Store the new audio parameters
    //

    for (ClientIdx = 0; ClientIdx < MIXER_MAX_CLIENTS; ClientIdx++)
    {
        if (true == TakeMixerClientLock)
        {
            Client[ClientIdx].LockTake();
        }

        if (Client[ClientIdx].IsMyManifestor(Manifestor))
        {
            // Manifestor is found.
            Client[ClientIdx].UpdateParameters(ParsedAudioParameters);
            // Update the flag to signal change in client params
            if (MMEFirstMixerHdl != MME_INVALID_ARGUMENT)
            {
                FirstMixerClientAudioParamsUpdated = true;
            }
            else
            {
                ClientAudioParamsUpdated = true;
            }
            SE_ASSERT(Client[ClientIdx].GetState() != DISCONNECTED);
            SE_ASSERT(Client[ClientIdx].GetState() != STOPPED);

            if (Client[ClientIdx].GetState() == UNCONFIGURED)
            {
                Client[ClientIdx].UpdateManifestorModuleParameters(GetClientParameters());
            }

            if (true == TakeMixerClientLock)
            {
                Client[ClientIdx].LockRelease();
            }

            // Re-configure the mixer
            MMENeedsParameterUpdate = true;
            // Reconfigure the PCM player
            PcmPlayerNeedsParameterUpdate = true;

            // The job is done, so exit now
            break;
        }
        else
        {
            // This client does not own this manifestor.
            if (true == TakeMixerClientLock)
            {
                Client[ClientIdx].LockRelease();
            }
        }
    }

    if (ClientIdx >= MIXER_MAX_CLIENTS)
    {
        //Not enough client for this manifestor registration.
        SE_ERROR("<: Manifestor %p is not registered\n\n", Manifestor);
        return PlayerError;
    }

    SE_DEBUG(group_mixer, "<\n");
    return PlayerNoError;
}

////////////////////////////////////////////////////////////////////////////
///
///
///
PlayerStatus_t Mixer_Mme_c::SetOutputRateAdjustment(int adjust)
{
    SE_VERBOSE(group_mixer, ">><<\n");

    for (uint32_t CountOfAudioPlayerAttached(0), PlayerIdx(0); CountOfAudioPlayerAttached < NumberOfAudioPlayerAttached; PlayerIdx++)
    {
        if (true == MixerPlayer[PlayerIdx].IsPlayerObjectAttached())
        {
            CountOfAudioPlayerAttached++;

            if (true == MixerPlayer[PlayerIdx].HasPcmPlayer())
            {
                // Corresponding PcmPlayer found.
                int AdjustDone;
                MixerPlayer[PlayerIdx].GetPcmPlayer()->SetOutputRateAdjustment(adjust, &AdjustDone);
                SE_VERBOSE(group_mixer, "%s %d %d\n", Name, adjust, AdjustDone);
            }
        }
    }

    return PlayerNoError;
}

////////////////////////////////////////////////////////////////////////////
///
/// Power management functions
///
/// These methods are used to synchronize stop/restart of Mixer thread in low power
/// and to terminate/init MME transformer for CPS mode
///
PlayerStatus_t Mixer_Mme_c::LowPowerEnter()
{
    PlayerStatus_t PlayerStatus = PlayerNoError;
    SE_INFO(group_mixer, ">%s\n", Name);
    // Reset events used for putting mixer thread in "low power"
    OS_ResetEvent(&LowPowerEnterEvent);
    OS_ResetEvent(&LowPowerExitEvent);
    OS_ResetEvent(&LowPowerExitCompletedEvent);

    // Init variables used for putting mixer thread in "low power"
    LowPowerEnterThreadLoop = 0;
    // Save low power state
    IsLowPowerState = true;
    // Trigger mixer thread to speed-up low power enter
    MixerRequest_c request;
    request.SetFunctionToManageTheRequest(&Mixer_Mme_c::CheckClientsState);
    PlayerStatus = MixerRequestManager.SendRequest(request);

    if (PlayerStatus != PlayerNoError)
    {
        SE_ERROR("SendRequest failed status:%d\n", PlayerStatus);
    }
    else
    {
        // Wait for mixer thread to be in safe state (no more MME commands issued)
        OS_Status_t WaitStatus = OS_WaitForEventInterruptible(&LowPowerEnterEvent, OS_INFINITE);
        if (WaitStatus == OS_INTERRUPTED)
        {
            SE_INFO(group_mixer, "wait for LP enter interrupted; LowPowerEnterEvent:%d\n", LowPowerEnterEvent.Valid);
        }

        OS_Smp_Mb(); // Read memory barrier: rmb_for_InLowPowerState coupled with: wmb_for_InLowPowerState
        if (InLowPowerState == false)
        {
            PlayerStatus = PlayerError;
        }
    }

    return PlayerStatus;
}

PlayerStatus_t Mixer_Mme_c::LowPowerExit()
{
    PlayerStatus_t PlayerStatus = PlayerNoError;

    SE_INFO(group_mixer, ">%s\n", Name);

    // Reset low power state
    IsLowPowerState = false;
    // Wake-up mixer thread
    OS_SetEventInterruptible(&LowPowerExitEvent);

    // Wait for mixer thread to be in safe state (no more MME commands issued)
    OS_Status_t WaitStatus = OS_WaitForEventInterruptible(&LowPowerExitCompletedEvent, OS_INFINITE);
    if (WaitStatus == OS_INTERRUPTED)
    {
        SE_INFO(group_mixer, "wait for LP exit completed interrupted; LowPowerExitCompletedEvent:%d\n", LowPowerExitCompletedEvent.Valid);
    }

    OS_Smp_Mb(); // Read memory barrier: rmb_for_InLowPowerState coupled with: wmb_for_InLowPowerState
    if (InLowPowerState == true)
    {
        PlayerStatus = PlayerError;
    }

    return PlayerStatus;
}
////////////////////////////////////////////////////////////////////////////
///
/// A wrapper to the Mixer_Mme_c::CheckClientsState( )
/// method.
/// The aMixerRequest argument is not used but we need this function
/// prototype for the MixerRequest_c::SetFunctionToManageTheRequest()
///
PlayerStatus_t Mixer_Mme_c::CheckClientsState(MixerRequest_c &aMixerRequest)
{
    (void)aMixerRequest; // warning removal
    PlayerStatus_t Status = CheckClientsState();
    return Status;
}
////////////////////////////////////////////////////////////////////////////
///
/// Initialize and terminate the appropriate global resources based the state of the inputs.
///
/// Basically we examine the state of all the inputs (including audio generator
/// and interactive inputs) and determine which global services should be
/// running. Then we make it so.
///
/// - If all the clients (Playstream/AudioGenerator/interactive input) are disconnected then we halt the mixer
///   we reset it and set the thread to the Idle state.
/// - Otherwise if there is at least one client (Playstream/AudioGenerator/interactive input)
///   that is not disconnected then:
///       - we initialize the PCM players and the MME transformer if not already done
///       - if the thread is in Idle state and one PlayStream client in UNCONFIGURED state then
///         we set the thread state to Starting. But if one AudioGenerator/interactive client is
///         in STARTED state then we set the thread state to Playback
///
PlayerStatus_t Mixer_Mme_c::CheckClientsState()
{
    PlayerStatus_t Status = PlayerNoError;
    SE_DEBUG(group_mixer, ">%s\n", Name);

    if (AllClientsAreInState(DISCONNECTED) && AllAudioGenIAudioInputsAreNotStarted())
    {
        Halt(); // No error recovery possible
        Status = Reset(); // No error recovery possible
        ThreadState.SetState(ThreadState_c::Idle, __func__);
    }
    else
    {
        bool MixerSettingsToUpdate(false);
        bool AudioPlayersToUpdate(false);
        // Review output configuration.
        MixerSettingsToUpdate = MixerSettings.CheckAndUpdate();
        // Review players.
        AudioPlayersToUpdate = CheckAndUpdateAllAudioPlayers();

        if ((true == MixerSettingsToUpdate) || (true == AudioPlayersToUpdate))
        {
            Status = UpdatePlayerComponentsModuleParameters();
            if (PlayerNoError != Status)
            {
                SE_ERROR("Failed to update normalized time offset or CodecControls (default value will be used instead)\n");
            }

            // Trigger updates to come accordingly (Mixer and PCM Players).
            PcmPlayerNeedsParameterUpdate = true;
            MMENeedsParameterUpdate = true;
        }

        if (Status == PlayerNoError && !MMEInitialized)
        {
            Status = InitializeMMETransformer(&MMEInitParams, false);
            if (Status != PlayerNoError)
            {
                SE_ERROR("%s Failed to initialize MME transformer %s\n", Name, MMETransformerName);
            }
            if ((mTopology.type == STM_SE_MIXER_DUAL_STAGE_MIXING) && (MMEFirstMixerHdl == MME_INVALID_ARGUMENT))
            {
                Status = InitializeMMETransformer(&FirstMixerMMEInitParams, true);
                if (Status != PlayerNoError)
                {
                    SE_ERROR("%s Failed to initialize First Mixer MME transformer:%s\n", Name, FIRST_MIXER_TRANSFORMER);
                }
            }

            // Force a SetGlobal after Init of Mixer TF to get mixer latency for generating correct timemapping
            Status = UpdateMixerParameters();
            if (Status != PlayerNoError)
            {
                SE_ERROR("Failed to update mixer parameters\n");
            }
        }

        if (FirstActivePlayer != MIXER_MME_NO_ACTIVE_PLAYER)
        {
            Status = InitializePcmPlayers();
        }

        if ((NumberOfAudioPlayerInitialized > 0) && (Status == PlayerNoError) && (ThreadState.GetState() == ThreadState_c::Idle))
        {
            if (ThereIsOneClientInState(UNCONFIGURED))
            {
                ThreadState.SetState(ThreadState_c::Starting, __func__);
                // The players may have been already attached and there may be no update
                // So we ensure that the players are updated and started in the "Starting" state
                // as we may have cleared all the player "SurfaceParameters" at ::Reset()
                PcmPlayerNeedsParameterUpdate = true;
            }

            if (ThereIsOneAudioGenIAudioInputInState(STM_SE_AUDIO_GENERATOR_STARTED))
            {
                ThreadState.SetState(ThreadState_c::Playback, __func__);
                // The players may have been already attached and there may be no update
                // So we ensure that the players are updated and started in the "Playback" state
                // as we may have cleared all the player "SurfaceParameters" at ::Reset()
                PcmPlayerNeedsParameterUpdate = true;
            }
        }
    }

    SE_DEBUG(group_mixer, "<%s %d\n", Name, ThreadState.GetState());
    return Status;
}

////////////////////////////////////////////////////////////////////////////
///
///

void Mixer_Mme_c::SetStoppingClientsToStoppedState()
{
    for (uint32_t ClientIdx(0); ClientIdx < MIXER_MAX_CLIENTS; ClientIdx++)
    {
        if (Client[ClientIdx].GetState() == STOPPING)
        {
            SE_DEBUG(group_mixer,  "@: Starting: Moving manifestor %p-%d from STOPPING to STOPPED\n", Client[ClientIdx].GetManifestor(), ClientIdx);
            Client[ClientIdx].SetState(STOPPED);
            MMENeedsParameterUpdate = true;
            PcmPlayerNeedsParameterUpdate = true;
        }
    }
}

////////////////////////////////////////////////////////////////////////////
///
/// Main mixer processing thread.
///
/// \b WARNING: This method is public only to allow it to be called from a
///             C linkage callback. Do not call it directly.
///
void Mixer_Mme_c::PlaybackThread()
{
    PlayerStatus_t PlaybackStatus = PlayerNoError;
    ThreadState.SetState(ThreadState_c::Idle, __func__);
    if (MixerRequestManager.Start() != PlayerNoError)
    {
        SE_ERROR("Failed to create PlaybackThread\n");
        return;
    }
    long long int PlaybackThreadDuration = 0;

    // Once this mixer object has been created, MW can start attaching clients / players to it
    // this attachment relies on the read of Mixer_Request_Manager_c::ThreadState
    OS_Smp_Mb(); // Write memory barrier: wmb_for_Mixer_Starting coupled with: rmb_for_Mixer_Starting
    OS_SetEvent(&mPlaybackThreadOnOff);

    // Check state of clients
    while (PlaybackThreadRunning)
    {
        uint32_t consecutiveErrorCounter = 0;
        // Whatever the playback thread state is, check and update what can have been changed client side.
        bool MixerSettingsToUpdate(false);
        bool AudioPlayersToUpdate(false);

        // acknowledge any mixer clients that have stopped
        SetStoppingClientsToStoppedState();

        if (SE_IS_VERBOSE_ON(group_mixer))
        {
            if (PlaybackThreadDuration != 0)
            {
                SE_VERBOSE(group_mixer, "%s PlaybackThreadDuration:%6lldus\n",
                           Name, OS_GetTimeInMicroSeconds() - PlaybackThreadDuration);
            }
            PlaybackThreadDuration = OS_GetTimeInMicroSeconds();
        }

        // Review output configuration.
        MixerSettingsToUpdate = MixerSettings.CheckAndUpdate();
        // Review players.
        AudioPlayersToUpdate = CheckAndUpdateAllAudioPlayers();

        if ((true == MixerSettingsToUpdate) || (true == AudioPlayersToUpdate))
        {
            PlayerStatus_t Status = UpdatePlayerComponentsModuleParameters();

            if (PlayerNoError != Status)
            {
                SE_ERROR("Failed to update normalized time offset or CodecControls (default value will be used instead)\n");
            }

            // Trigger updates to come accordingly (Mixer and PCM Players).
            PcmPlayerNeedsParameterUpdate = true;
            MMENeedsParameterUpdate = true;
        }

        MixerRequestManager.ManageTheRequest(*this);

        // If low power state, thread must stay asleep until the low power exit signal
        if (IsLowPowerState)
        {
            // Entering low power state...
            if (PlaybackThreadLowPowerEnter())
            {
                // Signals that mixer thread is now in low power stat
                OS_Smp_Mb(); // Read memory barrier: wmb_for_InLowPowerState coupled with: rmb_for_InLowPowerState
                OS_SetEventInterruptible(&LowPowerEnterEvent);

                // Forever wait for wake-up event
                OS_Status_t WaitStatus = OS_WaitForEventInterruptible(&LowPowerExitEvent, OS_INFINITE);
                if (WaitStatus == OS_INTERRUPTED)
                {
                    SE_INFO(group_mixer, "wait for LP exit interrupted; LowPowerExitEvent:%d\n", LowPowerExitEvent.Valid);
                }

                // Exiting low power state...
                PlaybackThreadLowPowerExit();
                // Reset the playback status because we have reset the players
                PlaybackStatus = true;

                // Signals that mixer thread is now back in running state
                OS_Smp_Mb(); // Read memory barrier: wmb_for_InLowPowerState coupled with: rmb_for_InLowPowerState
                OS_SetEventInterruptible(&LowPowerExitCompletedEvent);
            }
        }

        switch (ThreadState.GetState())
        {
        case ThreadState_c::Idle:
        {
            SE_VERBOSE(group_mixer, "%s: Idle\n", Name);
            PlaybackStatus = PlayerNoError;
            MixerRequestManager.WaitForPendingRequest(5000);
        }
        break;

        case ThreadState_c::Starting:
        {
            //
            // Wait until the PCM player is configured and we can inject the initial lump of silence
            //
            while (0 == MixerPlayer[FirstActivePlayer].GetPcmPlayerConfigSurfaceParameters().PeriodParameters.SampleRateHz &&
                   ThreadState.GetState() == ThreadState_c::Starting)
            {
                SE_DEBUG(group_mixer, "%s Starting: Waiting for PCM player to be initialized\n", Name);

                PlayerStatus_t RequestStatus = MixerRequestManager.WaitForPendingRequest(MIXER_THREAD_SURFACE_PARAMETER_UPDATE_EVENT_TIMEOUT);

                if (RequestStatus != PlayerTimedOut)
                {
                    MixerRequestManager.ManageTheRequest(*this);
                }
                else
                {
                    // Just arise a trace, cause this can happen if application is starting play-back and is meeting
                    // error immediately after. Application then stops play-back, with no surface parameters being updated.
                    // Mixer thread has to exit on timeout cause is still awaiting on this surface parameters update that will never come.
                    SE_INFO(group_mixer, "%s Starting: WaitForPendingRequest TIMEOUT\n", Name);
                }

                SE_DEBUG(group_mixer, "%s Starting: WaitForRequest ended, PcmParamsReceivedWhileStarting:%s\n",
                         Name, PcmParamsReceivedWhileStarting ? "true" : "false");

                // in all cases, upon wakeup acknowledge any mixer clients that have stopped
                SetStoppingClientsToStoppedState();

                if (RequestStatus != PlayerTimedOut && PcmParamsReceivedWhileStarting == false)
                {
                    continue;
                }

                PcmParamsReceivedWhileStarting = false;

                if (PcmPlayerNeedsParameterUpdate)
                {
                    SE_DEBUG(group_mixer, "%s Starting: About to update PCM player parameters\n", Name);
                    OS_Status_t Status = UpdatePcmPlayersParameters();
                    if (Status != PlayerNoError)
                    {
                        SE_ERROR("Starting: Failed to update soundcard parameters\n");
                        continue;
                    }
                }

                if (RequestStatus == PlayerTimedOut)
                {
                    break;
                }
            }

            if (0 != MixerPlayer[FirstActivePlayer].GetPcmPlayerConfigSurfaceParameters().PeriodParameters.SampleRateHz)
            {
                if (PcmPlayerNeedsParameterUpdate)
                {
                    SE_DEBUG(group_mixer, "%s Starting: About to update PCM player parameters\n", Name);
                    OS_Status_t Status = UpdatePcmPlayersParameters();
                    if (Status != PlayerNoError)
                    {
                        SE_ERROR("Starting: Failed to update soundcard parameters\n");
                        continue;
                    }
                }

                ThreadState.SetState(ThreadState_c::Playback, __func__);
            }
        }
        break;

        case ThreadState_c::Playback:
        {
            //
            // When we enter playback, PCM players will be started so we can keep running the main loop
            // (which injects silence when there is nothing to play) until all clients are disconnected.
            //

            //SE_DEBUG(group_mixer, "Mixer cycle %d\n", Cycles);
            if (PlaybackStatus != PlayerNoError)
            {
                consecutiveErrorCounter++;
                SE_ERROR("Playback:  %s is trying to recover the error\n", Name);

                if (consecutiveErrorCounter >= MIXER_NB_OF_ITERATION_WITH_ERROR_BEFORE_STOPPING)
                {
                    SE_INFO(group_mixer, "%s Playback: error is considered as not recoverable for a stopping client, so let's stop any STOPPING client\n", Name);
                    CleanUpOnError();
                }

                // ensure we don't live-lock if the sound system goes mad...
                OS_SleepMilliSeconds(100);
            }
            else
            {
                if (consecutiveErrorCounter > 0)
                {
                    SE_DEBUG(group_mixer, "Playback: error has been recovered\n");
                    consecutiveErrorCounter = 0;
                }
            }

            if (PcmPlayerNeedsParameterUpdate)
            {
                SE_DEBUG(group_mixer,  "Playback: About to update PCM player parameters\n");
                PlaybackStatus = UpdatePcmPlayersParameters();

                if (PlaybackStatus != PlayerNoError)
                {
                    SE_ERROR("Playback: Failed to update soundcard parameters\n");
                    continue;
                }
            }

            if (MMENeedsParameterUpdate)
            {
                SE_DEBUG(group_mixer,  "Playback: About to update parameters\n");
                PlaybackStatus = UpdateMixerParameters();

                if (PlaybackStatus != PlayerNoError)
                {
                    SE_ERROR("Playback: Failed to update mixer parameters\n");
                    continue;
                }

                UpdatePcmPlayersIec60958StatusBits();
            }

            if (PcmPlayersStarted == false)
            {
                StartPcmPlayers();
            }

            PlaybackStatus = FillOutMixCommand();

            if (PlaybackStatus != PlayerNoError)
            {
                SE_ERROR("Playback: Failed to populate mix command\n");
                continue;
            }

            PlaybackStatus = SendMMEMixCommand();

            if (PlaybackStatus != PlayerNoError)
            {
                SE_ERROR("Playback: Unable to issue mix command\n");
                continue;
            }

            PlaybackStatus = WaitForMMECallback();

            if (PlaybackStatus != PlayerNoError)
            {
                // Error already prompted by callee
                // SE_ERROR( "Playback: Waiting for MME callback returned error\n" );
                continue;
            }

        }
        break;

        default:
            SE_ERROR("Invalid PlaybackThread state %d\n", ThreadState.GetState());
        }
    }

    SE_DEBUG(group_mixer,  "Terminating\n");
    OS_Smp_Mb(); // Read memory barrier: rmb_for_Mixer_Terminating coupled with: wmb_for_Mixer_Terminating
    OS_SetEvent(&mPlaybackThreadOnOff);
}

////////////////////////////////////////////////////////////////////////////
///
/// Mixer processing thread actions on low power enter.
///
bool Mixer_Mme_c::PlaybackThreadLowPowerEnter()
{
    bool LowPowerEnterCompleted = false;

    // Check playback thread state
    if (ThreadState.GetState() == ThreadState_c::Idle)
    {
        SE_INFO(group_mixer, "%s: entering low power..\n", Name);
        // The mixer is not started, we can enter low power immedialtely
        LowPowerEnterCompleted = true;
    }
    else
    {
        // The mixer is started
        // We set the output gain to 0 and wait for 3 thread loops, to be sure that the mixer output becomes null, and thus avoid audio glitch
        if (LowPowerEnterThreadLoop == 0)
        {
            SE_INFO(group_mixer, "%s: entering low power..\n", Name);
            // Force the ouput mixer gain to 0 to avoid audio glitch
            LowPowerPostMixGainSave = MixerSettings.MixerOptions.PostMixGain;
            MixerSettings.MixerOptions.PostMixGain = 0;
            // Update the flag
            MixerSettings.MixerOptions.PostMixGainUpdated = true;
        }
        else if (LowPowerEnterThreadLoop >= MIXER_NUMBER_OF_THREAD_LOOPS_BEFORE_LOW_POWER_ENTER)
        {
            // We are going to terminate the mixer. Update input buffer for all Clients and request to ReleaseProcessingDecodedBuffer.
            for (uint32_t ClientIdx(0); ClientIdx < MIXER_MAX_CLIENTS; ClientIdx++)
            {
                UpdateInputBuffer(ClientIdx, true);
            }
            // We are now ready to enter low power state
            LowPowerEnterCompleted = true;

            // Terminate MME transformer if needed
            IsLowPowerMMEInitialized = MMEInitialized;
            InLowPowerState          = true;

            if (IsLowPowerMMEInitialized)
            {
                MME_ERROR MMEStatus = MME_TermTransformer(MMEHandle);
                if (MMEStatus != MME_SUCCESS)
                {
                    SE_ERROR("Error returned by MME_TermTransformer()\n");

                    InLowPowerState = false;
                    return true;
                }

                MMEHandle = 0;

                if (MMEFirstMixerHdl != MME_INVALID_ARGUMENT)
                {
                    MMEStatus = MME_TermTransformer(MMEFirstMixerHdl);

                    if (MMEStatus != MME_SUCCESS)
                    {
                        SE_ERROR("Error returned by MME_TermTransformer() on MMEFirstMixerHdl\n");
                        InLowPowerState = false;
                        return true;
                    }
                    LowPowerWakeUpFirstMixer = true;
                    MMEFirstMixerHdl = MME_INVALID_ARGUMENT;
                }
            }

            // Close the PcmPlayers
            TerminatePcmPlayers();
        }

        LowPowerEnterThreadLoop ++;
    }

    return LowPowerEnterCompleted;
}

////////////////////////////////////////////////////////////////////////////
///
/// Mixer processing thread actions on low power exit.
///
void Mixer_Mme_c::PlaybackThreadLowPowerExit()
{
    SE_INFO(group_mixer, "%s: exiting low power..\n", Name);

    // Re-initialize MME transformer if needed
    if (IsLowPowerMMEInitialized)
    {
        // Recreate the PcmPlayers
        InitializePcmPlayers();

        if (ThreadState.GetState() == ThreadState_c::Playback)
        {
            PlayerStatus_t PlayerStatus = PlayerNoError;

            PlayerStatus = UpdatePcmPlayersParameters();

            if (PlayerStatus != PlayerNoError)
            {
                SE_ERROR("Starting: Failed to update soundcard parameters\n");
            }
            else
            {
                // Restart the PcmPlayers
                StartPcmPlayers();
            }
        }

        if (ThreadState.GetState() != ThreadState_c::Idle)
        {
            // Restore output gain as it was before low power
            MixerSettings.MixerOptions.PostMixGain = LowPowerPostMixGainSave;
            // Update the flag
            MixerSettings.MixerOptions.PostMixGainUpdated = true;
        }

        MME_ERROR MMEStatus;
        MMEStatus = MME_InitTransformer(MMETransformerName, &MMEInitParams, &MMEHandle);

        if (MMEStatus != MME_SUCCESS)
        {
            SE_ERROR("Error returned by MME_InitTransformer()\n");
            InLowPowerState = true;
        }
        else
        {
            InLowPowerState = false;
        }

        if (LowPowerWakeUpFirstMixer)
        {
            LowPowerWakeUpFirstMixer = false;
            MMEStatus = MME_InitTransformer(FIRST_MIXER_TRANSFORMER, &FirstMixerMMEInitParams, &MMEFirstMixerHdl);

            if (MMEStatus != MME_SUCCESS)
            {
                SE_ERROR("Error returned by MME_InitTransformer() FirstMixer\n");
                InLowPowerState = true;
            }
            else
            {
                InLowPowerState = false;
            }
        }
        // [bug31642] Force a SetGlobal after Init of Mixer TF.
        PlayerStatus_t Status = UpdateMixerParameters();
        if (Status != PlayerNoError)
        {
            SE_ERROR("Failed to update mixer parameters during Low Power wakeup\n");
        }
    }
    else
    {
        InLowPowerState = false;
    }
}

////////////////////////////////////////////////////////////////////////////
///
/// Handle a callback from MME.
///
/// In addition to handling the callback this function also alters the
/// priority of the callback thread (created by MME) such that it has
/// the same priority as the mixer collation thread. This ensures that
/// the delivery of the callback will not be obscured by lower priority
/// work.
///
void Mixer_Mme_c::CallbackFromMME(MME_Event_t Event, MME_Command_t *Command, bool isFirstMixer)
{
    SE_EXTRAVERB(group_mixer, ">%s\n", Name);

    SE_VERBOSE(group_mixer, ">%s: %s %s %s\n",
               Name, isFirstMixer ? "FirstMixer" : "",
               StringifyMmeCmdCode(Command->CmdCode),
               StringifyMmeEvent(Event));

    switch (Event)
    {
    case MME_COMMAND_COMPLETED_EVT:
    {
        switch (Command->CmdCode)
        {
        case MME_TRANSFORM:
        {
            if (isFirstMixer)
            {
                OS_SemaphoreSignal(&MMEFirstMixerCallbackSemaphore);
            }
            else
            {
                OS_SemaphoreSignal(&MMECallbackSemaphore);
            }
        }
        break;

        case MME_SET_GLOBAL_TRANSFORM_PARAMS:
        {
            SE_VERBOSE(group_mixer, ">%s: %s MME_SET_GLOBAL_TRANSFORM_PARAMS\n", Name, isFirstMixer ? "FirstMixer" : "");

            if (!isFirstMixer)
            {
                OS_SemaphoreSignal(&MMEParamCallbackSemaphore);
            }
            // boost the callback priority to be the same as the mixer process
            if (!MMECallbackThreadBoosted)
            {
                OS_SetSchedAndAffinity(&player_tasks_desc[SE_TASK_AUDIO_MIXER]);
                MMECallbackThreadBoosted = true;
            }

            /* following trace requires bz78148
            SE_DEBUG(group_mixer, "%s Mixer Algorithm Latency:%u usec\n",
                     isFirstMixer ? "First" : "",
                     isFirstMixer ? FirstMixerParamsCommand.OutputParams.DelayMicroSec : ParamsCommand.OutputParams.DelayMicroSec);
                     */
        }
        break;

        default:
        {
            SE_ERROR("%s Unexpected MME CmdCode (%d)\n", isFirstMixer ? "FirstMixer" : "", Command->CmdCode);
        }
        break;
        }
    }
    break;

    default:
        SE_ERROR("%s Unexpected MME event (%d)\n", isFirstMixer ? "FirstMixer" : "", Event);
        break;
    }
}

////////////////////////////////////////////////////////////////////////////
///
/// Send a request to setup the interactive input the id of which being
/// InteractiveId.
/// This is a synchronous function.
/// This function locks, sets event and waits event so be sure that you
/// call it in a thread state that allows that: in particular DO NOT CALL IT
/// in a spin locked code.
///
PlayerStatus_t Mixer_Mme_c::SendSetupAudioGeneratorRequest(Audio_Generator_c *aGenerator)
{
    MixerRequest_c request;
    request.SetAudioGenerator(aGenerator);
    request.SetFunctionToManageTheRequest(&Mixer_Mme_c::SetupAudioGenerator);
    PlayerStatus_t status = MixerRequestManager.SendRequest(request);
    if (status != PlayerNoError)
    {
        SE_ERROR("SendRequest failed status:%d\n", status);
    }

    SendSetPcmParamsReceivedWhileStarting();
    return status;
}

////////////////////////////////////////////////////////////////////////////
///
/// A wrapper to the Mixer_Mme_c::SetupAudioGenerator( MixerRequest_c
///                                                       &aMixerRequest_c )
/// method.
///
PlayerStatus_t Mixer_Mme_c::SetupAudioGenerator(MixerRequest_c &aMixerRequest_c)
{
    PlayerStatus_t Status;
    Audio_Generator_c *aGenerator = aMixerRequest_c.GetAudioGenerator();

    if (aGenerator->IsInteractiveAudio())
    {
        Status = SetupInteractiveAudio(aGenerator);
    }
    else
    {
        Status = SetupAudioGenerator(aGenerator);
    }

    return Status;
}

////////////////////////////////////////////////////////////////////////////
///
/// Update the configuration of the application input.
///
/// Can only be called while the input is not already configured or if it
/// is stopped (or stopping).
///
PlayerStatus_t Mixer_Mme_c::SetupAudioGenerator(Audio_Generator_c *aGenerator)
{
    uint32_t AudioGeneratorIdx;

    if (mTopology.nb_max_application_audio <= NumberOfAudioGeneratorAttached)
    {
        SE_ERROR("Can't Attach: would exceed Topology.nb_max_application_audio=%d\n", mTopology.nb_max_application_audio);
        return PlayerNotSupported;
    }

    for (AudioGeneratorIdx = 0; AudioGeneratorIdx < MAX_AUDIO_GENERATORS; AudioGeneratorIdx++)
    {
        if (!Generator[AudioGeneratorIdx])
        {
            Generator[AudioGeneratorIdx] = aGenerator;
            NumberOfAudioGeneratorAttached++;
            break;
        }
    }

    if (AudioGeneratorIdx == MAX_AUDIO_GENERATORS)
    {
        SE_ERROR("Can't Attach:Mixer table is full\n");
        return PlayerError;
    }

    return SetAudioGeneratorToStoppedState();
}

////////////////////////////////////////////////////////////////////////////
///
/// Update the configuration of the interactive input.
///
/// Can only be called while the input is not already configured or if it
/// is stopped (or stopping).
///
PlayerStatus_t Mixer_Mme_c::SetupInteractiveAudio(Audio_Generator_c *iAudio)
{
    uint32_t InteractiveAudioIdx;

    if (mTopology.nb_max_interactive_audio <= NumberOfInteractiveAudioAttached)
    {
        SE_ERROR("Can't Attach: would exceed Topology.nb_max_interactive_audio=%d\n", mTopology.nb_max_interactive_audio);
        return PlayerNotSupported;
    }

    for (InteractiveAudioIdx = 0; InteractiveAudioIdx < MAX_INTERACTIVE_AUDIO; InteractiveAudioIdx++)
    {
        if (!InteractiveAudio[InteractiveAudioIdx])
        {
            InteractiveAudio[InteractiveAudioIdx] = iAudio;
            NumberOfInteractiveAudioAttached++;
            break;
        }
    }

    if (InteractiveAudioIdx == MAX_INTERACTIVE_AUDIO)
    {
        SE_ERROR("Can't Attach:Mixer table is full\n");
        return PlayerError;
    }

    return SetAudioGeneratorToStoppedState();
}

PlayerStatus_t Mixer_Mme_c::SetAudioGeneratorToStoppedState()
{
    CheckClientsState();
    PcmPlayerNeedsParameterUpdate = true;
    MMENeedsParameterUpdate = true;
    AudioGeneratorUpdated = true;
    return PlayerNoError;
}

////////////////////////////////////////////////////////////////////////////
///
/// Send a request to free the interactive input the id of which being
/// InteractiveId.
/// This is a synchronous function.
/// This function locks, sets event and waits event so be sure that you
/// call it in a thread state that allows that: in particular DO NOT CALL IT
/// in a spin locked code.
///
PlayerStatus_t Mixer_Mme_c::SendFreeAudioGeneratorRequest(Audio_Generator_c *aGenerator)
{
    MixerRequest_c request;
    request.SetAudioGenerator(aGenerator);
    request.SetFunctionToManageTheRequest(&Mixer_Mme_c::FreeAudioGenerator);
    PlayerStatus_t status = MixerRequestManager.SendRequest(request);

    if (status != PlayerNoError)
    {
        SE_ERROR("SendRequest failed status:%d\n", status);
    }

    return status;
}
////////////////////////////////////////////////////////////////////////////
///
/// A wrapper to the Mixer_Mme_c::FreeAudioGenerator( int InteractiveId )
/// method.
///
PlayerStatus_t Mixer_Mme_c::FreeAudioGenerator(MixerRequest_c &aMixerRequest)
{
    PlayerStatus_t Status;
    Audio_Generator_c *aGenerator = aMixerRequest.GetAudioGenerator();

    if (aGenerator->IsInteractiveAudio())
    {
        Status = FreeInteractiveAudio(aGenerator);
    }
    else
    {
        Status = FreeAudioGenerator(aGenerator);
    }

    return Status;
}
////////////////////////////////////////////////////////////////////////////
///
/// Free an audio generator input identifier and associated resources.
///
/// Set the state to DISCONNECTED.
///
PlayerStatus_t Mixer_Mme_c::FreeAudioGenerator(Audio_Generator_c *aGenerator)
{
    uint32_t AudioGeneratorIdx;

    for (AudioGeneratorIdx = 0; AudioGeneratorIdx < MAX_AUDIO_GENERATORS; AudioGeneratorIdx++)
    {
        if (Generator[AudioGeneratorIdx] == aGenerator)
        {
            break;
        }
    }

    if (AudioGeneratorIdx == MAX_AUDIO_GENERATORS)
    {
        return -EINVAL;
    }
    else
    {
        Generator[AudioGeneratorIdx] = NULL;
        NumberOfAudioGeneratorAttached--;
    }

    Generator[AudioGeneratorIdx] = NULL;

    for (uint32_t ChannelIdx(0); ChannelIdx < SND_PSEUDO_MIXER_CHANNELS; ChannelIdx++)
    {
        MixerSettings.MixerOptions.AudioGeneratorGain[AudioGeneratorIdx][ChannelIdx] = Q3_13_UNITY;
    }

    return CheckClientsState();
}

////////////////////////////////////////////////////////////////////////////
///
/// Free an interative input identifier and associated resources.
///
///
/// Set the state to DISCONNECTED.
///
PlayerStatus_t Mixer_Mme_c::FreeInteractiveAudio(Audio_Generator_c *iAudio)
{
    uint32_t InteractiveAudioIdx;

    for (InteractiveAudioIdx = 0; InteractiveAudioIdx < MAX_INTERACTIVE_AUDIO; InteractiveAudioIdx++)
    {
        if (InteractiveAudio[InteractiveAudioIdx] == iAudio)
        {
            break;
        }
    }

    if (InteractiveAudioIdx == MAX_INTERACTIVE_AUDIO)
    {
        return -EINVAL;
    }
    else
    {
        InteractiveAudio[InteractiveAudioIdx] = NULL;
        NumberOfInteractiveAudioAttached--;
    }

    return CheckClientsState();
}

////////////////////////////////////////////////////////////////////////////
///
/// Called by external thread;
/// Typically called in atomic context (SNDRV_PCM_TRIGGER_START)
///
PlayerStatus_t Mixer_Mme_c::StartAudioGenerator(Audio_Generator_c *aGenerator)
{
    (void)aGenerator; // warning removal

    SE_DEBUG(group_mixer, ">\n");
    MixerRequest_c request;
    request.SetFunctionToManageTheRequest(&Mixer_Mme_c::CheckClientsState);
    PlayerStatus_t status = MixerRequestManager.SendAsyncRequest(request); // Use Async request as we could be in atomic context
    if (status != PlayerNoError)
    {
        SE_ERROR("SendRequest failed status:%d\n", status);
    }

    MMENeedsParameterUpdate = true;
    SE_DEBUG(group_mixer, "<\n");
    return status;
}

////////////////////////////////////////////////////////////////////////////
///
/// Called by external thread;
/// Typically called in atomic context (SNDRV_PCM_TRIGGER_STOP)
///
PlayerStatus_t Mixer_Mme_c::StopAudioGenerator()
{
    SE_DEBUG(group_mixer, ">\n");
    MixerRequest_c request;
    request.SetFunctionToManageTheRequest(&Mixer_Mme_c::CheckClientsState);
    PlayerStatus_t status = MixerRequestManager.SendAsyncRequest(request); // Use Async request as we could be in atomic context
    if (status != PlayerNoError)
    {
        SE_ERROR("SendRequest failed status:%d\n", status);
    }

    MMENeedsParameterUpdate = true;
    SE_DEBUG(group_mixer, "<\n");
    return status;
}

////////////////////////////////////////////////////////////////////////////
///
/// Query Channel Configuration respresented as an eAccAcMode
//
/// If a channelAssinement has been set on mixer, the associated eAccAcMode is returned
//  else if all players shared the same ChannelAssignment, the associated eAccAcMode is returned
//  Undefined eAccAcMode (i.e. ACC_MODE_ID) is returned otherwise
//
PlayerStatus_t Mixer_Mme_c::GetChannelConfiguration(enum eAccAcMode *AcMode)
{
    *AcMode = ACC_MODE_ID;
    // Take the lock because player can be attached / detached from other thread
    for (uint32_t ClientIdx(0); ClientIdx < MIXER_MAX_CLIENTS; ClientIdx++)
    {
        Client[ClientIdx].LockTake();
    }

    if (true == MixerPlayer[FirstActivePlayer].IsPlayerObjectAttached())
    {
        eAccAcMode MixerAcMode = TranslateChannelAssignmentToAudioMode(MixerSettings.MixerOptions.SpeakerConfig);

        if (MixerAcMode != ACC_MODE_ID)
        {
            *AcMode = MixerAcMode;
            SE_VERBOSE(group_mixer, "Mixer:%s Use Mixer's SpeakerConfig:%s\n", Name, LookupAudioMode(MixerAcMode));
        }
        else
        {
            PcmPlayer_c::OutputEncoding OutputEncoding;
            PlayerStatus_t status = LookupPlayerFinalOutputEncodingAndFreq(FirstActivePlayer, &OutputEncoding);
            if (status != PlayerNoError)
            {
                for (uint32_t ClientIdx(0); ClientIdx < MIXER_MAX_CLIENTS; ClientIdx++)
                {
                    Client[ClientIdx].LockRelease();
                }
                SE_ERROR("Failed to retrieve Player OutputEncoding\n");
                return status;
            }

            eAccAcMode FirstPlayerAcMode = TranslateDownstreamCardToMainAudioMode(MixerPlayer[FirstActivePlayer].GetCard(), OutputEncoding);
            eAccAcMode PlayerAcMode      = ACC_MODE_ID;

            // Check global promotion (all cards issue the same downmix)
            for (uint32_t CountOfAudioPlayerAttached(0), PlayerIdx(0);
                 CountOfAudioPlayerAttached < NumberOfAudioPlayerAttached;
                 PlayerIdx++)
            {
                if (true == MixerPlayer[PlayerIdx].IsPlayerObjectAttached())
                {
                    CountOfAudioPlayerAttached++;
                    status = LookupPlayerFinalOutputEncodingAndFreq(PlayerIdx, &OutputEncoding);
                    if (status != PlayerNoError)
                    {
                        for (uint32_t ClientIdx(0); ClientIdx < MIXER_MAX_CLIENTS; ClientIdx++)
                        {
                            Client[ClientIdx].LockRelease();
                        }
                        SE_ERROR("Failed to retrieve Player OutputEncoding for PlayerIdx:%d\n", PlayerIdx);
                        return status;
                    }

                    PlayerAcMode = TranslateDownstreamCardToMainAudioMode(MixerPlayer[PlayerIdx].GetCard(), OutputEncoding);
                    SE_VERBOSE(group_mixer, "Mixer:%s Player:%s AccMode:%s\n",
                               Name, MixerPlayer[PlayerIdx].GetCardName(), LookupAudioMode(PlayerAcMode));

                    if (PlayerAcMode != FirstPlayerAcMode)
                    {
                        SE_VERBOSE(group_mixer,  "Mixer:%s players use different SpeakerConfig (%s versus %s)\n",
                                   Name, LookupAudioMode(FirstPlayerAcMode), LookupAudioMode(PlayerAcMode));
                        break;
                    }
                }
            }

            if (PlayerAcMode == FirstPlayerAcMode)
            {
                SE_VERBOSE(group_mixer,  "Mixer:%s All players share the same SpeakerConfig:%s\n",
                           Name, LookupAudioMode(FirstPlayerAcMode));
                *AcMode = FirstPlayerAcMode;
            }
        }
    }

    for (uint32_t ClientIdx(0); ClientIdx < MIXER_MAX_CLIENTS; ClientIdx++)
    {
        Client[ClientIdx].LockRelease();
    }

    return PlayerNoError;
}


PlayerStatus_t Mixer_Mme_c::UpdateTransformerId(const char *transformerName)
{
    MME_ERROR MMEStatus;
    MME_TransformerCapability_t Capability = { 0 };
    MME_LxMixerTransformerInfo_t MixerInfo = { 0 };
    SE_DEBUG(group_mixer, ">%s transformerName:%s\n", Name, transformerName);

    Capability.StructSize = sizeof(Capability);
    Capability.TransformerInfo_p = &MixerInfo;
    Capability.TransformerInfoSize = sizeof(MixerInfo);

    MMEStatus = MME_GetTransformerCapability(transformerName, &Capability);
    if (MMEStatus != MME_SUCCESS)
    {
        SE_INFO(group_mixer, "Unable to read capabilities from:%s (err:%d)\n", transformerName, MMEStatus);
        SE_INFO(group_mixer, "%s Fallback to %s\n", Name, AUDIOMIXER_MT_NAME);

        //Fallback to default mixer Transformer name
        strncpy(MMETransformerName, AUDIOMIXER_MT_NAME, MMETransformerNameSize); // keep '\0'

        return PlayerMatchNotFound;
    }
    else
    {
        strncpy(MMETransformerName, transformerName, MMETransformerNameSize); // keep '\0'
    }

    return PlayerNoError;
}

////////////////////////////////////////////////////////////////////////////
///
/// Get Maximum External latency among all the player in uSec
///
/// if multiple player are attached to the mixer then this API will return
/// maximum latency among all the players
///
/// @return MaxExternal latency in uSec
///
uint32_t  Mixer_Mme_c::GetMaxPlayerExternalLatency()
{
    uint32_t MaxExternalLatency = 0;
    for (uint32_t ClientIdx(0); ClientIdx < MIXER_MAX_CLIENTS; ClientIdx++)
    {
        Client[ClientIdx].LockTake();
    }

    for (uint32_t CountOfAudioPlayerAttached(0), PlayerIdx(0); CountOfAudioPlayerAttached < NumberOfAudioPlayerAttached; PlayerIdx++)
    {
        if (true == MixerPlayer[PlayerIdx].IsPlayerObjectAttached())
        {
            CountOfAudioPlayerAttached++;

            uint32_t ExternalLatency;
            MixerPlayer[PlayerIdx].GetOption(STM_SE_CTRL_EXTERNAL_LATENCY, (int &) ExternalLatency);
            if (MaxExternalLatency < ExternalLatency)
            {
                MaxExternalLatency = ExternalLatency;
            }
        }
    }

    for (uint32_t ClientIdx(0); ClientIdx < MIXER_MAX_CLIENTS; ClientIdx++)
    {
        Client[ClientIdx].LockRelease();
    }

    SE_EXTRAVERB(group_mixer, "MaxExternalLatency %d mSec\n", MaxExternalLatency);
    return (MaxExternalLatency * 1000);
}

////////////////////////////////////////////////////////////////////////////
///
/// Return the runtime latency in usec for a given mixer input.
/// Mixer internal latency includes :
/// - Latency introduced by Mixer algorithm latency
/// - Intermixer buffer latency for FirstStage Mixer clients
///
unsigned long long Mixer_Mme_c::GetMixerLatency(unsigned int ClientIdx) const
{
    unsigned long long LatencyInUs = 0;

    if (mTopology.type == STM_SE_MIXER_DUAL_STAGE_MIXING && ClientIdx <= FIRST_MIXER_MAX_CLIENTS)
    {
        // In case of dual stage mixer, latency should include intermixer latency
        // for clients connected to First Stage Mixer
        LatencyInUs = (INTERMIXER_SAMPLE_COUNT * 1000000ull) / LookupMixerSamplingFrequency();

        LatencyInUs += FirstMixerParamsCommand.OutputParams.DelayMicroSec;
    }

    LatencyInUs += ParamsCommand.OutputParams.DelayMicroSec;
    LatencyInUs += HW_DELAY;

    SE_VERBOSE(group_mixer, "%sStageMixer ClientIdx:%u LatencyInUs:%llu\n",
               mTopology.type == STM_SE_MIXER_DUAL_STAGE_MIXING ? "Dual" : "Single",
               ClientIdx, LatencyInUs);

    return LatencyInUs;
}

////////////////////////////////////////////////////////////////////////////
///
/// Return the latency in usec for a given manifestor in order to establish AV sync TimeMapping.
/// It includes :
/// 1 - ALSA startup/cold latency (Time to consume ALSA prefilled buffers)
/// 2 - Mixer internal latency (see GetMixerLatency for details)
/// 3 - An extra startup latency required to initialize driver and FW
///
unsigned long long Mixer_Mme_c::GetMixerLatencyForManifestorClient(ParsedAudioParameters_t        *ParsedAudioParameters,
                                                                   const Manifestor_AudioKsound_c *Manifestor)
{
    for (uint32_t ClientIdx(0); ClientIdx < MIXER_MAX_CLIENTS; ClientIdx++)
    {
        Client[ClientIdx].LockTake();
    }

    const Manifestor_AudioKsound_c *ForManifestor = NULL;
    unsigned int samplerate_hz = 0;  // needed only if ForManifestor set

    if (ThreadState.GetState() != ThreadState_c::Playback)
    {
        // provide manifestor and associated sample rate frequency to be used to evaluate latency
        // only if state is not playback
        // TODO(pht) audiofw shall be queried also for associated PCM latency
        ForManifestor = Manifestor;
        if (ParsedAudioParameters != NULL)
        {
            SE_ASSERT(ParsedAudioParameters->Base.StreamType == StreamTypeAudio);
            SE_ASSERT(ParsedAudioParameters->Source.SampleRateHz >= UNSUPPORTED_SAMPLE_RATE);
            samplerate_hz = ParsedAudioParameters->Source.SampleRateHz;
        }
        else
        {
            // ParsedAudioParameters maybe be NULL in which case we consider default sample rate
            samplerate_hz = STM_SE_DEFAULT_AUDIO_SAMPLE_RATE_HZ;
        }
    }

    uint32_t SamplingFrequency = LookupMixerSamplingFrequency(ForManifestor, samplerate_hz);
    uint32_t MixerGranuleSize  = LookupMixerGranuleSize(SamplingFrequency);

    for (uint32_t ClientIdx(0); ClientIdx < MIXER_MAX_CLIENTS; ClientIdx++)
    {
        Client[ClientIdx].LockRelease();
    }

    // 1 - ALSA startup/cold latency (Time to consume ALSA prefilled buffers)
    unsigned int LatencyInSamples = MIXER_NUM_PERIODS * MixerGranuleSize;

    unsigned long long LatencyInUs = (LatencyInSamples * 1000000ull) / SamplingFrequency;

    // The overall latency calculated below by adding the mixer latency as well as the driver + FW startup latency
    // of 10ms will increase the latency for HDMI RX usecase beyond the 7.5ms delay which is already handled while
    // generating time mapping for this case. Hence avoid this overhead and return only the ALSA startup latency
    // for this case.
    int CaptureProfile   = PolicyValueCaptureProfileDisabled;

    Client[PrimaryClient].LockTake();
    if (Client[PrimaryClient].GetState() != DISCONNECTED)
    {
        HavanaStream_c *Stream = Client[PrimaryClient].GetStream();
        if (Stream != NULL)
        {
            Stream->GetOption(PolicyCaptureProfile, &CaptureProfile);
        }
    }
    Client[PrimaryClient].LockRelease();

    if (CaptureProfile != PolicyValueCaptureProfileDisabled)
    {
        return LatencyInUs;
    }

    // 2 - Mixer internal latency
    for (uint32_t ClientIdx = 0; ClientIdx < MIXER_MAX_CLIENTS; ClientIdx++)
    {
        Client[ClientIdx].LockTake();
        if (Client[ClientIdx].IsMyManifestor(Manifestor))
        {
            LatencyInUs += GetMixerLatency(ClientIdx);
            Client[ClientIdx].LockRelease();
            break;
        }
        Client[ClientIdx].LockRelease();
    }

    // 3 - An extra startup latency required to initialize driver and FW
    LatencyInUs += STARTUP_LATENCY_US;

    SE_VERBOSE(group_mixer, "LatencyInUs:%llu MixerGranuleSize %u SamplingFrequency %u\n",
               LatencyInUs, MixerGranuleSize, SamplingFrequency);

    return LatencyInUs;
}

////////////////////////////////////////////////////////////////////////////
///
/// Update MME mixer parameters.
///
/// If during the course of an update we determine that there new clients then
/// we must reconfigure ourselves (and potentially initialize MME and the
/// playback thread).
///
PlayerStatus_t Mixer_Mme_c::UpdateMixerParameters()
{
    MME_ERROR ErrorCode;
    SE_DEBUG(group_mixer, ">%s Waiting for parameter semaphore\n", Name);
    OS_SemaphoreWaitAuto(&MMEParamCallbackSemaphore);
    SE_DEBUG(group_mixer, "@%s Got the parameter semaphore\n", Name);
    MMENeedsParameterUpdate = false;
    // TODO: if the SamplingFrequency has changed we ought to terminate and re-init the transformer
    {
        for (uint32_t ClientIdx(0); ClientIdx < MIXER_MAX_CLIENTS; ClientIdx++)
        {
            Client[ClientIdx].LockTake();
        }

        if (MMEFirstMixerHdl != MME_INVALID_ARGUMENT)
        {
            FillOutFirstMixerGlobalParameters(&FirstMixerParamsCommand.InputParams);
        }
        FillOutTransformerGlobalParameters(&ParamsCommand.InputParams);

        for (uint32_t ClientIdx(0); ClientIdx < MIXER_MAX_CLIENTS; ClientIdx++)
        {
            Client[ClientIdx].LockRelease();
        }
    }

    if (MMEFirstMixerHdl != MME_INVALID_ARGUMENT)
    {
        ErrorCode = MME_SendCommand(MMEFirstMixerHdl, &FirstMixerParamsCommand.Command);
        if (ErrorCode != MME_SUCCESS)
        {
            SE_ERROR("Could not issue First Mixer parameter update command (%d)\n", ErrorCode);
            return PlayerError;
        }
    }

    ErrorCode = MME_SendCommand(MMEHandle, &ParamsCommand.Command);
    if (ErrorCode != MME_SUCCESS)
    {
        SE_ERROR("Could not issue parameter update command (%d)\n", ErrorCode);
        return PlayerError;
    }

    CheckAndStartClients();
    SE_DEBUG(group_mixer, "<%s\n", Name);
    return PlayerNoError;
}
////////////////////////////////////////////////////////////////////////////
///
/// Check and START clients that are in STARTING state
///
void Mixer_Mme_c::CheckAndStartClients()
{
    for (uint32_t ClientIdx(0); ClientIdx < MIXER_MAX_CLIENTS; ClientIdx++)
    {
        //Client[ClientIdx].LockTake(); Not Mandatory to be taken
        if (Client[ClientIdx].GetState() == STARTING)
        {
            Client[ClientIdx].SetState(STARTED);
            //Client[ClientIdx].LockRelease();
            // place the buffer descriptor (especially NumberOfScatterPages) to the 'silent' state.
            if (MMEFirstMixerHdl != MME_INVALID_ARGUMENT)
            {
                FillOutSilentBuffer(FirstMixerCommand.Command.DataBuffers_p[ClientIdx],
                                    &FirstMixerCommand.InputParams.InputParam[ClientIdx]);
                FirstMixerCommand.Command.DataBuffers_p[ClientIdx]->Flags = BUFFER_TYPE_AUDIO_IO | ClientIdx;
            }
            else
            {
                FillOutSilentBuffer(MixerCommand.Command.DataBuffers_p[ClientIdx],
                                    &MixerCommand.InputParams.InputParam[ClientIdx]);
                MixerCommand.Command.DataBuffers_p[ClientIdx]->Flags = BUFFER_TYPE_AUDIO_IO | ClientIdx;
            }
        }
    }
}

////////////////////////////////////////////////////////////////////////////
///
/// Lookup the index into Mixer_Mme_c::Clients of the supplied play-stream object
///
int Mixer_Mme_c::LookupClientFromStream(class HavanaStream_c *Stream) const
{
    int32_t ClientIdx;

    if (Stream == NULL)
    {
        return 0;    // This shortcut will only be valid until stlinuxtv provides proper Stream pointers.
    }

    for (ClientIdx = 0; (ClientIdx < MIXER_MAX_CLIENTS) && (false == Client[ClientIdx].IsMyStream(Stream)); ClientIdx++)
    {
        ;           // do nothing
    }

    if (ClientIdx >= MIXER_MAX_CLIENTS)
    {
        /* In case the abstraction layer still doesn't provide client objects as
           control params but direct indexes, then return the client as the index */
        int idx = (int) Stream;
        /* check for primary and secondary , then check for interactive audio indexes */
        ClientIdx = ((idx == STM_SE_CTRL_VALUE_AUDIO_MIXER_ROLE_PRIMARY)   ||
                     (idx == STM_SE_CTRL_VALUE_AUDIO_MIXER_ROLE_SECONDARY) ||
                     (idx == STM_SE_CTRL_VALUE_AUDIO_MIXER_ROLE_IN3)       ||
                     (idx == STM_SE_CTRL_VALUE_AUDIO_MIXER_ROLE_IN4))      ? idx : -1;

        ClientIdx = ((idx >= STM_SE_CTRL_VALUE_MIXER_ROLE_AUDIO_GENERATOR_0) && (idx <= STM_SE_CTRL_VALUE_MIXER_ROLE_AUDIO_GENERATOR_7)) ? idx : ClientIdx;
        ClientIdx = ((idx >= STM_SE_CTRL_VALUE_AUDIO_MIXER_ROLE_SFX0) && (idx <= STM_SE_CTRL_VALUE_AUDIO_MIXER_ROLE_SFX7)) ? idx : ClientIdx;
    }

    return ClientIdx;
}

////////////////////////////////////////////////////////////////////////////
///
/// Examine the downstream topology and determine the maximum frequency we might be asked to
/// support.
///
uint32_t Mixer_Mme_c::LookupPcmPlayersMaxSamplingFrequency() const
{
    uint32_t MaxFreq(Mixer_Player_c::MIN_FREQUENCY);

    for (uint32_t CountOfAudioPlayerAttached(0), PlayerIdx(0); CountOfAudioPlayerAttached < NumberOfAudioPlayerAttached; PlayerIdx++)
    {
        if (true == MixerPlayer[PlayerIdx].IsPlayerObjectAttached())
        {
            CountOfAudioPlayerAttached++;

            if (MixerPlayer[PlayerIdx].GetCardMaxFreq() > MaxFreq)
            {
                MaxFreq = MixerPlayer[PlayerIdx].GetCardMaxFreq();
            }
        }
    }

    return MaxFreq;
}

////////////////////////////////////////////////////////////////////////////
///
/// Examine the clients attached to the mixer and determine what sampling frequency to use.
/// - if manifestor set, use provided sample rate freq and not current sample rate freq
///
uint32_t Mixer_Mme_c::LookupMixerSamplingFrequency(const Manifestor_AudioKsound_c *ForManifestor,
                                                   unsigned int manif_samplerate_hz) const
{
    uint32_t SFreq = 0;
    uint32_t ClientIdx;

    // Check if mixer sampling frequency was previously forced by STKPI control, i.e. STM_SE_FIXED_OUTPUT_FREQUENCY.
    if (MIXER_SAMPLING_FREQUENCY_STKPI_UNSET != InitializedSamplingFrequency)
    {
        SE_DEBUG(group_mixer, "<%s Mixer frequency %d (due to forcibly initialized frequency)\n",
                 Name,
                 InitializedSamplingFrequency);
        return InitializedSamplingFrequency;
    }


    // Check clients, that are registered and well known, so as to get their source sample frequency to use.
    for (ClientIdx = 0; ClientIdx < MIXER_MAX_CLIENTS; ClientIdx++)
    {
        // Client Lock already taken by caller.
        if (Client[ClientIdx].GetState() != DISCONNECTED)
        {
            if ((ForManifestor == NULL) || (Client[ClientIdx].IsMyManifestor(ForManifestor) == false))
            {
                SFreq = Client[ClientIdx].GetSourceSurfaceParameter().SampleRateHz;
            }
            else
            {
                SFreq = manif_samplerate_hz;
            }
            SE_DEBUG(group_mixer, "@%s Mixer frequency %d (due to manifestor client %d)\n", Name, SFreq, ClientIdx);
            break;
        }
    }

    if (!SFreq)
    {
        if (!MixerSamplingFrequency)
        {
            for (uint32_t AudioGeneratorIdx(0); AudioGeneratorIdx < MAX_AUDIO_GENERATORS; AudioGeneratorIdx++)
            {
                if (Generator[AudioGeneratorIdx] && Generator[AudioGeneratorIdx]->GetState() == STM_SE_AUDIO_GENERATOR_STARTED)
                {
                    SFreq = Generator[AudioGeneratorIdx]->GetSamplingFrequency();
                    SE_DEBUG(group_mixer, "@%s Mixer frequency %d (due to audio generator %d)\n", Name, SFreq, AudioGeneratorIdx);
                    break;
                }
            }
        }
        else
        {
            SE_DEBUG(group_mixer, "No input dictates mixer frequency (falling back to previous value of %d)\n",
                     MixerSamplingFrequency);
            return MixerSamplingFrequency;
        }
        // If audio generator has not set sampling frequency.
        if (!SFreq)
        {
            SE_DEBUG(group_mixer,  "No input dictates mixer frequency (falling back to default)\n");
            return STM_SE_DEFAULT_AUDIO_SAMPLE_RATE_HZ;
        }
    }

    uint32_t Funrefined = SFreq;
    // Determine the highest frequency the downstream output supports
    uint32_t PcmPlayersMaxSamplingFrequency = LookupPcmPlayersMaxSamplingFrequency();

    // cap the sampling frequency to the largest of the downstream max. output frequencies.
    // this reduces the complexity of the mix and (if we are lucky) of the subsequent PCM
    // post-processing. this translates to (milli)watts...
    while (SFreq > PcmPlayersMaxSamplingFrequency)
    {
        uint32_t Fprime = SFreq / 2;

        if (Fprime * 2 != SFreq)
        {
            SE_ASSERT(0);
            break;
        }

        SFreq = Fprime;
    }

    // boost low frequencies until they exceed the system minimum (if we are mixing we
    // don't want to drag the sample rate too low and harm the other sources).
    while (SFreq < Mixer_Player_c::MIN_FREQUENCY)
    {
        SFreq *= 2;
    }

    if (Funrefined != SFreq)
    {
        SE_DEBUG(group_mixer, "Refined mixer frequency to %d due to topological constraints\n", SFreq);
    }

    SE_DEBUG(group_mixer, "<%s Returning %d\n", Name, SFreq);
    return SFreq;
}


////////////////////////////////////////////////////////////////////////////
///
/// Examine the clients attached to the mixer and determine what audio mode the mixer will adopt
///
enum eAccAcMode Mixer_Mme_c::LookupMixerAudioMode() const
{
    // Client Lock already taken by caller.
    if (Client[PrimaryClient].ManagedByThread())
    {
        // This client is correctly stated, and its parameters are well known.
        return (enum eAccAcMode)Client[PrimaryClient].GetOrganisationParameter();
    }

    for (uint32_t ClientIdx(0); ClientIdx < MIXER_MAX_CLIENTS; ClientIdx++)
    {
        // Client Lock already taken by caller.
        if (Client[ClientIdx].ManagedByThread())
        {
            // This client is correctly stated, and its parameters are well known.
            return (enum eAccAcMode) Client[ClientIdx].GetOrganisationParameter();
        }
    }

    return ACC_MODE20;
}


////////////////////////////////////////////////////////////////////////////
///
/// Calculate, from the specified sampling frequency, the size of the mixer granule.
///
uint32_t Mixer_Mme_c::LookupMixerGranuleSize(uint32_t Frequency) const
{
    uint32_t Granule = MasterOutputGrain;
    SE_VERBOSE(group_mixer, ">: MasterOutputGrain: %d\n", MasterOutputGrain);

    // Check HDMI mode from PrimaryClient if any
    int CaptureProfile   = PolicyValueCaptureProfileDisabled;

    if (Client[PrimaryClient].GetState() != DISCONNECTED)
    {
        HavanaStream_c *Stream = Client[PrimaryClient].GetStream();
        if (Stream)
        {
            Stream->GetOption(PolicyCaptureProfile, &CaptureProfile);
        }
    }

    // For hdmirx mode force the grain to the minimal one in order to meet the lowest latency.
    if (CaptureProfile != PolicyValueCaptureProfileDisabled)
    {
        // Adjust the grain to the SamplingFrequency and round to upper 128 samples
        Granule = LookupMixerHDMIRxGranule(Frequency);
    }
    else
    {
        if (Frequency > 48000)
        {
            Granule *= 2;
        }

        if (Frequency > 96000)
        {
            Granule *= 2;
        }
    }

    SE_DEBUG(group_mixer, "<%s Returning %d for %d Hz\n", Name, Granule, Frequency);
    return Granule;
}


PlayerStatus_t Mixer_Mme_c::IsDisconnected() const
{
    uint32_t ClientIdx;
    SE_DEBUG(group_mixer, ">><<\n");

    //
    // If everything is disconnected and no resource allocated then return true
    //

    for (ClientIdx = 0; ClientIdx < MIXER_MAX_CLIENTS && Client[ClientIdx].GetState() == DISCONNECTED; ClientIdx++)
        ; // do nothing

    if (ClientIdx < MIXER_MAX_CLIENTS)
    {
        //Something is connected
        return PlayerError;
    }

    // check for connected players || Transformer initialized || Pseudo mixer
    if (((FirstActivePlayer != MIXER_MME_NO_ACTIVE_PLAYER) && (true == MixerPlayer[FirstActivePlayer].IsPlayerObjectAttached()) && (true == MixerPlayer[FirstActivePlayer].HasPcmPlayer()))
        || (true == MMEInitialized))
    {
        SE_DEBUG(group_mixer, "<%s PlayerError\n", Name);
        return PlayerError;
    }

    return PlayerNoError;
}


////////////////////////////////////////////////////////////////////////////
///
/// Check mixer is idle or not
///
PlayerStatus_t Mixer_Mme_c::IsIdle() const
{
    if (PlaybackThreadRunning && (ThreadState.GetState() == ThreadState_c::Idle))
    {
        SE_DEBUG(group_mixer, "<%s PlayerNoError\n", Name);
        return PlayerNoError;
    }

    SE_DEBUG(group_mixer, "<%s PlayerError\n", Name);
    return PlayerError;
}

////////////////////////////////////////////////////////////////////////////
///
/// SetApplicationType
///
/// Private Method to set mApplicationType private member
///
/// If MS11 or MS12 Application Type is selected:
/// - Mixer Output Frequency is forced to 48kHz
/// - Mixer Grain is forced to 1536 samples
/// - Previous values are stored in order to restore it
///   when coming back to non MS11/MS12 application type.
///
PlayerStatus_t Mixer_Mme_c::SetApplicationType(int requestedAppType)
{
    if (requestedAppType < 0 || requestedAppType > STM_SE_CTRL_VALUE_LAST_AUDIO_APPLICATION_TYPE)
    {
        SE_ERROR("Out of bound value set for STM_SE_CTRL_AUDIO_APPLICATION_TYPE:%d\n", requestedAppType);
        return PlayerError;
    }

    SE_DEBUG(group_mixer, "Changed from %s to %s\n",
             LookupPolicyValueAudioApplicationType(mApplicationType),
             LookupPolicyValueAudioApplicationType(requestedAppType));

    if (mApplicationType == requestedAppType)
    {
        return PlayerNoError;
    }

    /* Set MS11/MS12 forced parameters values */
    if (requestedAppType == STM_SE_CTRL_VALUE_AUDIO_APPLICATION_MS11 ||
        requestedAppType == STM_SE_CTRL_VALUE_AUDIO_APPLICATION_MS12)
    {
        if (mApplicationType != STM_SE_CTRL_VALUE_AUDIO_APPLICATION_MS11 &&
            mApplicationType != STM_SE_CTRL_VALUE_AUDIO_APPLICATION_MS12)
        {
            MixerOptionStruct Options = MixerSettings.GetLastOptions();

            /* Store MS11/MS12 forced current values */
            mApplicationTypeStoredParams.MasterGrain           = Options.MasterGrain;
            mApplicationTypeStoredParams.OutputSfreq.control   = Options.OutputSfreq.control;
            mApplicationTypeStoredParams.OutputSfreq.frequency = Options.OutputSfreq.frequency;

            Options.MasterGrain           = 1536;
            Options.OutputSfreq.control   = STM_SE_FIXED_OUTPUT_FREQUENCY;
            Options.OutputSfreq.frequency = 48000;

            MixerSettings.SetTobeUpdated(Options);
        }
    }
    else
    {
        if (mApplicationType == STM_SE_CTRL_VALUE_AUDIO_APPLICATION_MS11 ||
            mApplicationType == STM_SE_CTRL_VALUE_AUDIO_APPLICATION_MS12)
        {
            MixerOptionStruct Options = MixerSettings.GetLastOptions();

            /* Restore ISO/DVB/DVD stored values */
            Options.MasterGrain           = mApplicationTypeStoredParams.MasterGrain;
            Options.OutputSfreq.control   = mApplicationTypeStoredParams.OutputSfreq.control;
            Options.OutputSfreq.frequency = mApplicationTypeStoredParams.OutputSfreq.frequency;

            MixerSettings.SetTobeUpdated(Options);
        }
    }

    SE_VERBOSE(group_mixer, "ApplicationTypeStored Stored Params: Grain:%dsamples Freq:%dHz%s (%s)\n",
               mApplicationTypeStoredParams.MasterGrain,
               mApplicationTypeStoredParams.OutputSfreq.frequency,
               mApplicationTypeStoredParams.OutputSfreq.frequency == 0 ? "[Master]" : "",
               mApplicationTypeStoredParams.OutputSfreq.control == STM_SE_FIXED_OUTPUT_FREQUENCY ? "Fixed" : "Max");
    mApplicationType = requestedAppType;

    return PlayerNoError;
}

////////////////////////////////////////////////////////////////////////////
///
/// Ensure other player components are notified of any applicable mixer settings.
///
/// At present the output timer must be informed of the master A/V sync offset (used
/// to equalize the audio and video latency) and the codec must be informed of the
/// currently desired dynamic range control.
///
/// Each of these commands is proxied via the manifestor. Strictly speaking
/// the mixer, as a sub-class of ::BaseComponentClass_c, is able to reach in directly
/// and poke at these settings but we prefer to give the manifestor visibility of this
/// activity (in part because it might choose to block the requests if the system state
/// in inappropriate.
///
/// Warning: This method is called by userspace threads that have updated the
///          mixer controls. There is therefore absolutely no predictability about
///          when in the startup/shutdown sequences is will be called.
///
PlayerStatus_t Mixer_Mme_c::UpdatePlayerComponentsModuleParameters()
{
    PlayerStatus_t         Result = PlayerNoError;
    SE_VERBOSE(group_mixer, ">:\n");

    // Send the prepared commands to each applicable client
    for (uint32_t ClientIdx(0); ClientIdx < MIXER_MAX_CLIENTS; ClientIdx++)
    {
        // There is no point in updating a STOPPED client since these parameters will be resent as soon as
        // the manifestor is enabled. In fact, updating STOPPED clients invites races during startup and
        // shutdown since this method is called, at arbitrary times, from userspace threads. It would be
        // safe to update STOPPING clients but only it we knew the remained in the STOPPING state after
        // making this check. The mixer thread doesn't take the mutex to transition between STOPPING and
        // STOPPED (deliberately - we don't want the real time mixer thread to be blocked waiting for a
        // userspace thread to be scheduled)) so this mean we must avoid updating STOPPING clients as well.
        // Client Lock already taken by caller.
        // bug 25004 : we can apply an SetModuleParameters() on clients if they are in UNCONFIGURED state
        //             as the client is already allocated
        if (Client[ClientIdx].ManagedByThread() || (Client[ClientIdx].GetState() == UNCONFIGURED))
        {
            Mixer_Client_c::MixerClientParameters ClientParameters = GetClientParameters();
            Result = Client[ClientIdx].UpdateManifestorModuleParameters(ClientParameters);

            if (Result == PlayerError)
            {
                break;
            }
        }
    }

    SE_VERBOSE(group_mixer, "<:\n");
    return Result;
}


////////////////////////////////////////////////////////////////////////////
///
/// Update our PCM player's IEC 60958 status bits.
///
/// This is called when the PCM players are initialized and whenever the
/// mixer values change.
///
void Mixer_Mme_c::UpdatePcmPlayersIec60958StatusBits()
{
    for (uint32_t CountOfAudioPlayerAttached(0), PlayerIdx(0); CountOfAudioPlayerAttached < NumberOfAudioPlayerAttached; PlayerIdx++)
    {
        if (true == MixerPlayer[PlayerIdx].IsPlayerObjectAttached())
        {
            CountOfAudioPlayerAttached++;

            if (true == MixerPlayer[PlayerIdx].HasPcmPlayer())
            {
                enum eIecValidity IecValidity = IEC_VALID_SAMPLES;
                stm_se_aes_iec958_t channel_metadata;
                if (MixerPlayer[PlayerIdx].GetCompoundOption(STM_SE_CTRL_AUDIO_PLAYER_AES_IEC958_METADATA, &channel_metadata) != PlayerNoError)
                {
                    SE_ERROR("<%s> GetCompoundOption Error\n", Name);
                    break;
                }

                PcmPlayer_c::OutputEncoding OutPutEncoding = MixerPlayer[PlayerIdx].LookupOutputEncoding(PrimaryCodedDataType[MIXER_CODED_DATA_INPUT_SPDIF_IDX],
                                                                                                         PrimaryCodedDataType[MIXER_CODED_DATA_INPUT_HDMI_IDX]);

                // it is required to specify "NonLinearPCM" channel status bits  when output is bypassed
                // Bypass also possible for SPDIFIN_PCM. Exclude that for channel status update
                if ((PcmPlayer_c::IsOutputBypassed(OutPutEncoding) || PcmPlayer_c::IsOutputEncoded(OutPutEncoding)) && (OutPutEncoding != PcmPlayer_c::BYPASS_SPDIFIN_PCM))
                {
                    SE_DEBUG(group_mixer, "%s: is NonLinearPCM - SetIec61937StatusBits\n", MixerPlayer[PlayerIdx].GetCardName());
                    IecValidity = IEC_INVALID_SAMPLES;
                    bool HBRA = PcmPlayer_c::IsHBRA(OutPutEncoding);
                    MixerPlayer[PlayerIdx].GetPcmPlayer()->SetIec61937StatusBits(&channel_metadata, HBRA);
                }
                else
                {
                    MixerPlayer[PlayerIdx].GetPcmPlayer()->SetIec60958StatusBits(&channel_metadata);
                }

                (void)MixerPlayer[PlayerIdx].GetPcmPlayer()->SetIec60958ValidityBits(IecValidity);
            }
        }
    }
}


////////////////////////////////////////////////////////////////////////////
///
///
///
PlayerStatus_t Mixer_Mme_c::StartPlaybackThread()
{
    OS_Thread_t Thread;
    int         ThreadType;

    switch (mTopology.type)
    {
    case STM_SE_MIXER_DUAL_STAGE_MIXING:
        ThreadType = SE_TASK_AUDIO_BCAST_MIXER;
        break;

    case STM_SE_MIXER_BYPASS:
        ThreadType = SE_TASK_AUDIO_BYPASS_MIXER;
        break;

    case STM_SE_MIXER_SINGLE_STAGE_MIXING:
    default:
        ThreadType = SE_TASK_AUDIO_MIXER;
        break;
    }

    if (PlaybackThreadRunning)
    {
        SE_ERROR("Playback thread is already running\n");
        return PlayerError;
    }

    PlaybackThreadRunning = true;

    if (OS_CreateThread(&Thread, PlaybackThreadStub, this, &player_tasks_desc[ThreadType]) != OS_NO_ERROR)
    {
        SE_ERROR("Unable to create mixer playback thread\n");
        PlaybackThreadRunning = false;
        return PlayerError;
    }

    return PlayerNoError;
}


////////////////////////////////////////////////////////////////////////////
///
///
///
void Mixer_Mme_c::TerminatePlaybackThread()
{
    if (PlaybackThreadRunning)
    {
        SE_DEBUG(group_mixer, ">%s\n", Name);

        // set any events the thread may be blocked waiting for
        SendSetPcmParamsReceivedWhileStarting();

        // Go out of idle mode
        MixerRequestManager.Stop();

        // Ask thread to terminate
        OS_ResetEvent(&mPlaybackThreadOnOff);
        OS_Smp_Mb(); // Write memory barrier: wmb_for_Mixer_Terminating coupled with: rmb_for_Mixer_Terminating
        PlaybackThreadRunning = false;

        // wait for the thread to come to rest
        OS_WaitForEventAuto(&mPlaybackThreadOnOff, OS_INFINITE);
    }

    SE_ASSERT(!PlaybackThreadRunning);
}

////////////////////////////////////////////////////////////////////////////
///
/// Open the PCM player (or players) ready for them to be configured.
///
PlayerStatus_t Mixer_Mme_c::InitializePcmPlayers()
{
    SE_DEBUG(group_mixer,  ">%s\n", Name);
    PlayerStatus_t Status = PlayerNoError;

    // Instantiate based on the latched topology
    for (uint32_t PlayerIdx(0); PlayerIdx < STM_SE_MIXER_NB_MAX_OUTPUTS; PlayerIdx++)
    {
        Status |= InitializePcmPlayer(PlayerIdx);
    }

    if (PlayerNoError != Status)
    {
        Status = PlayerError;
    }

    SE_DEBUG(group_mixer,  "<%s NumberOfAudioPlayerInitialized:%d\n", Name, NumberOfAudioPlayerInitialized);
    return Status;
}

////////////////////////////////////////////////////////////////////////////
///
/// Open the PCM player ready for them to be configured.
///
PlayerStatus_t Mixer_Mme_c::InitializePcmPlayer(uint32_t PlayerIdx)
{
    SE_VERBOSE(group_mixer,  ">%s\n", Name);
    PlayerStatus_t Status = PlayerNoError;

    if (0 == NumberOfAudioPlayerAttached)
    {
        SE_ERROR("No player available\n");
        return PlayerError;
    }

    if ((true == MixerPlayer[PlayerIdx].IsPlayerObjectAttached()) && (false == MixerPlayer[PlayerIdx].HasPcmPlayer()))
    {
        Status = MixerPlayer[PlayerIdx].CreatePcmPlayer(
                     PrimaryCodedDataType[MIXER_CODED_DATA_INPUT_SPDIF_IDX],
                     PrimaryCodedDataType[MIXER_CODED_DATA_INPUT_HDMI_IDX]);

        if (PlayerNoError != Status)
        {
            SE_ERROR("Failed to manufacture PCM player[%d]\n", PlayerIdx);
        }
        else
        {
            NumberOfAudioPlayerInitialized++;
        }
    }

    SE_VERBOSE(group_mixer,  "<%s\n", Name);
    return Status;
}


////////////////////////////////////////////////////////////////////////////
///
/// Close the PCM player (or players).
///
void Mixer_Mme_c::TerminatePcmPlayers()
{
    SE_DEBUG(group_mixer, ">%s\n", Name);

    for (uint32_t CountOfAudioPlayerAttached(0), PlayerIdx(0); CountOfAudioPlayerAttached < NumberOfAudioPlayerAttached; PlayerIdx++)
    {
        if (true == MixerPlayer[PlayerIdx].IsPlayerObjectAttached())
        {
            CountOfAudioPlayerAttached++;

            if (true == MixerPlayer[PlayerIdx].HasPcmPlayer())
            {
                MixerPlayer[PlayerIdx].DeletePcmPlayer();
                NumberOfAudioPlayerInitialized--;
            }
        }
    }

    PcmPlayersStarted = false;
    SE_DEBUG(group_mixer,  "<\n");
}

////////////////////////////////////////////////////////////////////////////
///
/// Restart all the PCM player (or players) as at least of them failed
/// to map or commit samples
///
void Mixer_Mme_c::DeployPcmPlayersUnderrunRecovery()
{
    SE_DEBUG(group_mixer,  ">%s\n", Name);

    mStatistics.mixer_number_of_underrun++;

    for (uint32_t CountOfAudioPlayerAttached(0), PlayerIdx(0);
         CountOfAudioPlayerAttached < NumberOfAudioPlayerAttached;
         PlayerIdx++)
    {
        if (true == MixerPlayer[PlayerIdx].IsPlayerObjectAttached())
        {
            CountOfAudioPlayerAttached++;

            if (true == MixerPlayer[PlayerIdx].HasPcmPlayer())
            {
                // No need to record the status of this method as the method will
                // itself print an error message if it fails.
                (void)MixerPlayer[PlayerIdx].GetPcmPlayer()->DeployUnderrunRecovery();
            }
        }
    }
}


////////////////////////////////////////////////////////////////////////////
///
///
///
PlayerStatus_t Mixer_Mme_c::MapPcmPlayersSamples(uint32_t SampleCount, bool NonBlock)
{
    for (uint32_t CountOfAudioPlayerAttached(0), PlayerIdx(0); CountOfAudioPlayerAttached < NumberOfAudioPlayerAttached; PlayerIdx++)
    {
        if (true == MixerPlayer[PlayerIdx].IsPlayerObjectAttached())
        {
            CountOfAudioPlayerAttached++;

            if (true == MixerPlayer[PlayerIdx].HasPcmPlayer())
            {
                PlayerStatus_t Status;
                Status = MixerPlayer[PlayerIdx].MapPcmPlayerSamples(SampleCount, NonBlock);

                if (PlayerNoError != Status)
                {
                    SE_ERROR("PcmPlayer %d refused to map %d samples\n", PlayerIdx, SampleCount);
                    return PlayerError;
                }
            }
        }
    }

    return PlayerNoError;
}


////////////////////////////////////////////////////////////////////////////
///
/// Commit any outstanding samples to the PCM player buffers.
///
PlayerStatus_t Mixer_Mme_c::CommitPcmPlayersMappedSamples()
{
    PlayerStatus_t Status;

    for (uint32_t CountOfAudioPlayerAttached(0), PlayerIdx(0); CountOfAudioPlayerAttached < NumberOfAudioPlayerAttached; PlayerIdx++)
    {
        MME_DataBuffer_t *OutputDataBuffer = MixerCommand.Command.DataBuffers_p[MixerCommand.Command.NumberInputBuffers + CountOfAudioPlayerAttached];

        if (true == MixerPlayer[PlayerIdx].IsPlayerObjectAttached())
        {
            CountOfAudioPlayerAttached++;

            if (true == MixerPlayer[PlayerIdx].HasPcmPlayer())
            {
                SE_VERBOSE(group_mixer, "@%s %s\n", Name, MixerPlayer[PlayerIdx].GetCardName());

                uPcmBufferFlags flags;
                flags.u32 = OutputDataBuffer->ScatterPages_p->FlagsOut;

                // extract the characteristics of the output buffer.
                enum eAccFsCode discrete_freq = (enum eAccFsCode) flags.bits.SamplingFreq;
                int32_t         iso_freq      = StmSeTranslateDiscreteSamplingFrequencyToInteger(discrete_freq);
                uint32_t        sample_freq   = (iso_freq > 0) ? (uint32_t) iso_freq : 0;
                enum eAccAcMode audio_mode    = (enum eAccAcMode) flags.bits.AudioMode;
                uDowmixInfoBF downmix_info;
                downmix_info.u8 = 0;


                // Despite the fact that we are in the context of the MixerThread
                // We can't use the cloneForMixer of the AudioPlayer because we aim at
                // updating the object and the cloneForMixer can only be updated through
                // CheckAndUpdatePlayerObject() method (it is a clone).
                // It is however safe to modify directly the player-object because the AudioInfoFrame
                // are only written from the mixer-thread (not read or written from the
                // middleware that controls the audio-player object).

                PcmPlayer_c::OutputEncoding OutputEncoding = MixerPlayer[PlayerIdx].LookupOutputEncoding(PrimaryCodedDataType[MIXER_CODED_DATA_INPUT_SPDIF_IDX],
                                                                                                         PrimaryCodedDataType[MIXER_CODED_DATA_INPUT_HDMI_IDX]);
                if (Client[PrimaryClient].ManagedByThread() && (OutputEncoding == PcmPlayer_c::BYPASS_SPDIFIN_PCM))
                {
                    // This client is correctly stated, and its parameters are well known.
                    SpdifInProperties_t PrimaryClientSpdifInProperties = Client[PrimaryClient].GetSpdifInProperties();

                    downmix_info.bits.LFELevel       = PrimaryClientSpdifInProperties.LfePlaybackLevel;
                    downmix_info.bits.LevelShift     = PrimaryClientSpdifInProperties.LevelShiftValue;
                    downmix_info.bits.DownmixInhibit = PrimaryClientSpdifInProperties.DownMixInhibit;
                }

                bool IsLinearPcm = (OutputEncoding == PcmPlayer_c::OUTPUT_PCM
                                    || OutputEncoding == PcmPlayer_c::OUTPUT_IEC60958
                                    || OutputEncoding == PcmPlayer_c::BYPASS_SPDIFIN_PCM);
                Status = MixerPlayer[PlayerIdx].UpdateAudioInfoFrame(sample_freq, audio_mode, downmix_info.u8, IsLinearPcm);
                if (Status != PlayerNoError)
                {
                    SE_WARNING("Unable to update AudioInfoFrame for HDMI\n");
                }

                Status = MixerPlayer[PlayerIdx].CommitPcmPlayerMappedSamples();


                if (Status != PlayerNoError)
                {
                    SE_ERROR("PcmPlayer failed to commit mapped samples\n");
                    SE_ERROR("MME Mix Command round-trip duration:%6lldus FW-processingTime:%uus\n",
                             OS_GetTimeInMicroSeconds() - MixCommandStartTime,
                             MixerCommand.OutputParams.MixStatus.ElapsedTime / 2); // ST231 timer counter is bugged by a factor 2
                    DeployPcmPlayersUnderrunRecovery();
                    return PlayerError;
                }
            }
        }
    }

    return PlayerNoError;
}

////////////////////////////////////////////////////////////////////////////
///
/// Call Dump method on each running PCM player.
///
void Mixer_Mme_c::DumpPcmPlayersCommitedSamples()
{
    for (uint32_t CountOfAudioPlayerAttached(0), PlayerIdx(0);
         CountOfAudioPlayerAttached < NumberOfAudioPlayerAttached; PlayerIdx++)
    {
        if (true == MixerPlayer[PlayerIdx].IsPlayerObjectAttached())
        {
            CountOfAudioPlayerAttached++;

            if (true == MixerPlayer[PlayerIdx].HasPcmPlayer())
            {
                MixerPlayer[PlayerIdx].DumpLastCommitedSamples();
            }
        }
    }
}

////////////////////////////////////////////////////////////////////////////
///
/// Get and update the estimated display times.
///
PlayerStatus_t Mixer_Mme_c::GetAndUpdateDisplayTimeOfNextCommit()
{
    unsigned long long DisplayTimeOfNextCommit;
    PlayerStatus_t Status;
    SE_VERBOSE(group_mixer, ">\n");

    //
    // Calculate when the next commit will be displayed and appriase the manifestors of the
    // situation. Just use the first PCM player for this.
    //
    if (true == MixerPlayer[FirstActivePlayer].HasPcmPlayer())
    {
        Status = MixerPlayer[FirstActivePlayer].GetPcmPlayer()->GetTimeOfNextCommit(&DisplayTimeOfNextCommit);

        if (Status != PlayerNoError)
        {
            SE_ERROR("PCM player won't tell us its commit latency\n");
            return Status;
        }
    }
    else
    {
        SE_ERROR("PCM player cannot be found\n");
        return PlayerError;
    }

    for (uint32_t ClientIdx(0); ClientIdx < MIXER_MAX_CLIENTS; ClientIdx++)
    {
        Client[ClientIdx].LockTake();

        if (Client[ClientIdx].ManagedByThread() || (Client[ClientIdx].GetState() == STOPPING))
        {
            // This client is correctly stated, and its manifestor is well known.
            // Add Mixer internal/runtime Latency for clean A/V Sync
            Client[ClientIdx].GetManifestor()->UpdateDisplayTimeOfNextCommit(DisplayTimeOfNextCommit + GetMixerLatency(ClientIdx));
        }

        Client[ClientIdx].LockRelease();
    }

    SE_VERBOSE(group_mixer, "<\n");
    return PlayerNoError;
}


////////////////////////////////////////////////////////////////////////////
///
/// Use Hi-Resolution timers to wait until it is right time to start PCM players
/// aligned with TimeMapping expcted by Manifestor_AudioKsound_c
///
/// The purpose of this wait is to make sure we do not drop/inject samples
/// to respect TimeMapping on first buffer presentation.
///
void Mixer_Mme_c::WaitToStartInSyncWithManifestors()
{
    // Iterate other clients to find the PlaybackTime of the first buffer to be presented
    unsigned long long FirstBufferPlaybackTime = INVALID_TIME;

    // Store index of client that will first present a buffer to compute associated latency
    unsigned int EarliestClientIndex = 0;

    for (uint32_t ClientIdx(0); ClientIdx < MIXER_MAX_CLIENTS; ClientIdx++)
    {
        Client[ClientIdx].LockTake();

        if (Client[ClientIdx].ManagedByThread())
        {
            unsigned long long SystemPlaybackTime;
            ManifestorStatus_t status = Client[ClientIdx].GetManifestor()->GetNextBufferSystemPlaybackTime(&SystemPlaybackTime);
            if (status != ManifestorNoError)
            {
                Client[ClientIdx].LockRelease();
                continue;
            }
            if (FirstBufferPlaybackTime == INVALID_TIME)
            {
                FirstBufferPlaybackTime = SystemPlaybackTime;
            }
            else if (SystemPlaybackTime < FirstBufferPlaybackTime)
            {
                FirstBufferPlaybackTime = SystemPlaybackTime;
                EarliestClientIndex = ClientIdx;
            }
            SE_DEBUG(group_mixer, "Client[%d] SystemPlaybackTime:%lluus FirstBufferPlaybackTime:%lluus\n",
                     ClientIdx, SystemPlaybackTime, FirstBufferPlaybackTime);
        }
        Client[ClientIdx].LockRelease();
    }

    // Do not wait if we did not receive any Manifestor Buffers
    if (FirstBufferPlaybackTime == INVALID_TIME)
    {
        return;
    }

    unsigned int SampleRate = MixerPlayer[FirstActivePlayer].GetPcmPlayerConfigSurfaceParameters().PeriodParameters.SampleRateHz ?
                              MixerPlayer[FirstActivePlayer].GetPcmPlayerConfigSurfaceParameters().PeriodParameters.SampleRateHz :
                              STM_SE_DEFAULT_AUDIO_SAMPLE_RATE_HZ;

    unsigned long long MixerAndHwLatencyInUs = (MixerGranuleSize * MIXER_NUM_PERIODS * 1000000ull) / SampleRate;

    MixerAndHwLatencyInUs += GetMixerLatency(EarliestClientIndex);

    unsigned long long now = OS_GetTimeInMicroSeconds();
    SE_DEBUG(group_mixer, "First Buffer should be presented at:%lluus Now:%lluus (delta:%lldus) MixerAndHwLatency:%lluus\n",
             FirstBufferPlaybackTime, now, FirstBufferPlaybackTime - now, MixerAndHwLatencyInUs);

    // Compute the time we should wait before Starting the players
    long long TimeToSleepInUs = FirstBufferPlaybackTime - (now + MixerAndHwLatencyInUs);

    unsigned long SleepRangeInUs = (5 * AVSYNC_TOLERANCE_IN_USEC) / 100; // 5% of AVSync tolerance
    if (TimeToSleepInUs - SleepRangeInUs > 0)
    {
        SE_DEBUG(group_mixer, "Will wait for %lluus %luus\n", TimeToSleepInUs, SleepRangeInUs);
        OS_SleepMicroSecondsHrTimer(TimeToSleepInUs - SleepRangeInUs, TimeToSleepInUs + SleepRangeInUs);

        if (SE_IS_DEBUG_ON(group_mixer))
        {
            long long WaitTimeError = ((long long)OS_GetTimeInMicroSeconds() - now) - TimeToSleepInUs;
            SE_DEBUG(group_mixer, "Waited for %lluus (delta:%lldus)\n",
                     OS_GetTimeInMicroSeconds() - now, WaitTimeError);
        }
    }
}

////////////////////////////////////////////////////////////////////////////
///
/// Populate the PCM player buffers to ensure clean startup.
///
/// Clean startup means startup without immediate starvation. If the PCM
/// player buffers are not seeded with a starting value they rapidly consume
/// the data in the decode buffers. When the decode buffers are consumed the
/// mixer enters the starved state and mutes itself.
///
/// If the PCM player buffers have a start threshold of their own buffer
/// length then this method can simply mix in advance consuming samples from
/// the buffer queue. Alternatively, if the PCM player buffer is 'short' then
/// the buffer can simply be filled with silence.
///
PlayerStatus_t Mixer_Mme_c::StartPcmPlayers()
{
    SE_DEBUG(group_mixer, ">%s\n", Name);
    uint32_t BufferSize = MixerPlayer[FirstActivePlayer].GetPcmPlayerConfigSamplesAreaSize();

    if (PcmPlayersStarted)
    {
        SE_ERROR("%s trying to start pcm players not terminated!\n", Name);
    }

    PlayerStatus_t Status = MapPcmPlayersSamples(BufferSize, true); // non-blocking attempt to map
    if (Status != PlayerNoError)
    {
        SE_ERROR("Failed to map all the PCM buffer samples (%u)\n", BufferSize);
        return Status;
    }

    SE_DEBUG(group_mixer, "Injecting %d muted samples\n", BufferSize);

    for (uint32_t CountOfAudioPlayerAttached(0), PlayerIdx(0); CountOfAudioPlayerAttached < NumberOfAudioPlayerAttached; PlayerIdx++)
    {
        if (true == MixerPlayer[PlayerIdx].IsPlayerObjectAttached())
        {
            CountOfAudioPlayerAttached++;

            if (true == MixerPlayer[PlayerIdx].HasPcmPlayer())
            {
                MixerPlayer[PlayerIdx].ResetPcmPlayerMappedSamplesArea();
            }
        }
    }

    WaitToStartInSyncWithManifestors();

    Status = CommitPcmPlayersMappedSamples();
    if (Status != PlayerNoError)
    {
        SE_ERROR("Failed to commit samples to the player\n");
        return Status;
    }

    DumpPcmPlayersCommitedSamples();

    PcmPlayersStarted = true;
    return PlayerNoError;
}


////////////////////////////////////////////////////////////////////////////
///
/// Update sound card parameters.
///
PlayerStatus_t Mixer_Mme_c::UpdatePcmPlayersParameters()
{
    SE_DEBUG(group_mixer, ">\n");
    PlayerStatus_t Status;
    PcmPlayerSurfaceParameters_t NewSurfaceParameters[MIXER_AUDIO_MAX_PCM_OUT_BUFFERS] = {{{0}}};
    bool RedundantUpdate = true;
    PcmPlayerNeedsParameterUpdate = false;
    // during an update the MixerSamplingFrequency becomes stale (it is updated after we have finished
    // updating the manifestor state). this method is called between these points and therefore must
    // perform a fresh lookup.
    NominalOutputSamplingFrequency = LookupMixerSamplingFrequency();
    uint32_t NominalMixerGranuleSize = LockClientAndLookupMixerGranuleSize(NominalOutputSamplingFrequency);
    SE_DEBUG(group_mixer, "@%s %u %u %u\n", Name, MixerSamplingFrequency, NominalOutputSamplingFrequency, InitializedSamplingFrequency);

    // record the parameters we need to set and check for redundancy
    for (uint32_t CountOfAudioPlayerAttached(0), PlayerIdx(0); CountOfAudioPlayerAttached < NumberOfAudioPlayerAttached; PlayerIdx++)
    {
        if (true == MixerPlayer[PlayerIdx].IsPlayerObjectAttached())
        {
            CountOfAudioPlayerAttached++;
            PcmPlayer_c::OutputEncoding OutputEncoding = MixerPlayer[PlayerIdx].LookupOutputEncoding(PrimaryCodedDataType[MIXER_CODED_DATA_INPUT_SPDIF_IDX],
                                                                                                     PrimaryCodedDataType[MIXER_CODED_DATA_INPUT_HDMI_IDX]);
            NewSurfaceParameters[PlayerIdx].PeriodParameters.BitsPerSample = 32;
            NewSurfaceParameters[PlayerIdx].PeriodParameters.ChannelCount = MixerPlayer[PlayerIdx].LookupOutputNumberOfChannels(PrimaryCodedDataType[MIXER_CODED_DATA_INPUT_SPDIF_IDX],
                                                                                                                                PrimaryCodedDataType[MIXER_CODED_DATA_INPUT_HDMI_IDX]);
            if ((true == MixerPlayer[PlayerIdx].IsBypass()) && Client[PrimaryClient].ManagedByThread())
            {
                // This client is correctly stated, and its parameters are well known.
                AudioOriginalEncoding_t OriginalEncoding = Client[PrimaryClient].GetOriginalEncodingParameter();
                if ((OriginalEncoding == AudioOriginalEncodingSPDIFIn_Compressed) || (OriginalEncoding == AudioOriginalEncodingSPDIFIn_Pcm))
                {
                    SpdifInProperties_t PrimaryClientSpdifInProperties = Client[PrimaryClient].GetSpdifInProperties();
                    NewSurfaceParameters[PlayerIdx].PeriodParameters.ChannelCount = PrimaryClientSpdifInProperties.ChannelCount;
                }
            }

            NewSurfaceParameters[PlayerIdx].PeriodParameters.SampleRateHz = NominalOutputSamplingFrequency;
            NewSurfaceParameters[PlayerIdx].ActualSampleRateHz = LookupPcmPlayerOutputSamplingFrequency(OutputEncoding, PlayerIdx);
            NewSurfaceParameters[PlayerIdx].PeriodSize = NominalMixerGranuleSize;
            NewSurfaceParameters[PlayerIdx].NumPeriods = MIXER_NUM_PERIODS;
            SE_VERBOSE(group_mixer, "Card %d: %s PeriodSize: %d ActualSampleRateHz:%d ChannelCount:%d\n",
                       PlayerIdx,
                       MixerPlayer[PlayerIdx].GetCardName(),
                       NewSurfaceParameters[PlayerIdx].PeriodSize,
                       NewSurfaceParameters[PlayerIdx].ActualSampleRateHz,
                       NewSurfaceParameters[PlayerIdx].PeriodParameters.ChannelCount);

            // Check whether this is any different to the existing configuration.
            if (0 != memcmp(&NewSurfaceParameters[PlayerIdx], &MixerPlayer[PlayerIdx].GetPcmPlayerConfigSurfaceParameters(), sizeof(NewSurfaceParameters[PlayerIdx])))
            {
                SE_DEBUG(group_mixer, "%s Mismatch for #%u: %u-vs-%u  %u-vs-%u  %u-vs-%u  %u-vs-%u  %u-vs-%u  %u-vs-%u\n",
                         Name,
                         PlayerIdx,
                         NewSurfaceParameters[PlayerIdx].PeriodParameters.BitsPerSample, MixerPlayer[PlayerIdx].GetPcmPlayerConfigSurfaceParameters().PeriodParameters.BitsPerSample,
                         NewSurfaceParameters[PlayerIdx].PeriodParameters.ChannelCount, MixerPlayer[PlayerIdx].GetPcmPlayerConfigSurfaceParameters().PeriodParameters.ChannelCount,
                         NewSurfaceParameters[PlayerIdx].PeriodParameters.SampleRateHz, MixerPlayer[PlayerIdx].GetPcmPlayerConfigSurfaceParameters().PeriodParameters.SampleRateHz,
                         NewSurfaceParameters[PlayerIdx].ActualSampleRateHz, MixerPlayer[PlayerIdx].GetPcmPlayerConfigSurfaceParameters().ActualSampleRateHz,
                         NewSurfaceParameters[PlayerIdx].PeriodSize, MixerPlayer[PlayerIdx].GetPcmPlayerConfigSurfaceParameters().PeriodSize,
                         NewSurfaceParameters[PlayerIdx].NumPeriods, MixerPlayer[PlayerIdx].GetPcmPlayerConfigSurfaceParameters().NumPeriods);
                RedundantUpdate = false;
            }
        }
    }

    if (RedundantUpdate)
    {
        SE_DEBUG(group_mixer,  "Ignoring redundant attempt to update the audio parameters\n");
        return PlayerNoError;
    }

    // we need to update all the PCM players (a parameter update involves stopping
    // and restarting a player so we need to make sure all players are treated
    // equally)

    for (uint32_t CountOfAudioPlayerAttached(0), PlayerIdx(0); CountOfAudioPlayerAttached < NumberOfAudioPlayerAttached; PlayerIdx++)
    {
        if (true == MixerPlayer[PlayerIdx].IsPlayerObjectAttached())
        {
            CountOfAudioPlayerAttached++;

            if (true == MixerPlayer[PlayerIdx].HasPcmPlayer())
            {
                Status = MixerPlayer[PlayerIdx].GetPcmPlayer()->SetParameters(&NewSurfaceParameters[PlayerIdx]);

                if (Status != PlayerNoError)
                {
                    if (NULL != MixerPlayer[PlayerIdx].GetPcmPlayerMappedSamplesArea())
                    {
                        PcmPlayerNeedsParameterUpdate = true;
                        SE_INFO(group_mixer, "%s Unable to modify pcm player settings at that time\n", Name);
                        return PlayerNoError;
                    }
                    else
                    {
                        SE_ERROR("Fatal error attempting to reconfigure the PCM player\n");

                        // Restore the original settings. It is required for class consistency that all the attached
                        // PCM players remain in a playable state (e.g. so we can safely call ksnd_pcm_wait() on them)

                        for (uint32_t CountOfAudioPlayerAttachedInner(0), PlayerIdxInner(0);
                             CountOfAudioPlayerAttachedInner < NumberOfAudioPlayerAttached;
                             PlayerIdxInner++)
                        {
                            if (true == MixerPlayer[PlayerIdxInner].IsPlayerObjectAttached())
                            {
                                CountOfAudioPlayerAttachedInner++;

                                if (true == MixerPlayer[PlayerIdxInner].HasPcmPlayer())
                                {
                                    NewSurfaceParameters[PlayerIdxInner] = MixerPlayer[PlayerIdxInner].GetPcmPlayerConfigSurfaceParameters();
                                    Status = MixerPlayer[PlayerIdxInner].GetPcmPlayer()->SetParameters(&NewSurfaceParameters[PlayerIdxInner]);
                                    if (Status != PlayerNoError)
                                    {
                                        SE_ERROR("Failed to restore Pcm Players Paramaters\n");
                                    }
                                }
                            }
                        }

                        return PlayerError;
                    }
                }
                else
                {
                    OutputTopologyUpdated = true;
                }
            }
        }
    }

    // successfully deployed the update (to all players) so record the new surface settings
    for (uint32_t CountOfAudioPlayerAttached(0), PlayerIdx(0); CountOfAudioPlayerAttached < NumberOfAudioPlayerAttached; PlayerIdx++)
    {
        if (true == MixerPlayer[PlayerIdx].IsPlayerObjectAttached())
        {
            CountOfAudioPlayerAttached++;

            if (true == MixerPlayer[PlayerIdx].HasPcmPlayer())
            {
                MixerPlayer[PlayerIdx].SetPcmPlayerConfigSurfaceParameters(NewSurfaceParameters[PlayerIdx]);
            }
        }
    }

    // Check all player configurations as far period size is concerned.
    for (uint32_t CountOfAudioPlayerAttached(0), PlayerIdx(0); CountOfAudioPlayerAttached < NumberOfAudioPlayerAttached; PlayerIdx++)
    {
        if (true == MixerPlayer[PlayerIdx].IsPlayerObjectAttached())
        {
            CountOfAudioPlayerAttached++;

            if (MixerPlayer[PlayerIdx].GetPcmPlayerConfigSurfaceParameters().PeriodSize != NominalMixerGranuleSize)
            {
                SE_ERROR("Invalid PCM player period size for %u!\n", PlayerIdx);
                return PlayerError;
            }
            else
            {
                SE_DEBUG(group_mixer, "PcmPlayer %u parameters - NumPeriods %u PeriodSize %u\n",
                         PlayerIdx,
                         MixerPlayer[PlayerIdx].GetPcmPlayerConfigSurfaceParameters().NumPeriods,
                         MixerPlayer[PlayerIdx].GetPcmPlayerConfigSurfaceParameters().PeriodSize);
            }
        }
    }

    UpdatePcmPlayersIec60958StatusBits();
    return PlayerNoError;
}

////////////////////////////////////////////////////////////////////////////
///
/// AllocateInterMixerRings
///
/// This method allocate buffers and RingBuffers required for MS12 dual Mixer setup
///
///*Creates 2 RingBuffer to hold PCM buffers:
///     + InterMixerFreeBufRing: store allocated unused PCM buffers
///     + InterMixerFillBufRing: store allocated used/filled PCM buffers
/// *Creates 2 RingBuffer to hold Metadata buffers:
///     + InterMixerFreeMDRing: store allocated unused Metadata buffers
///     + InterMixerFillMDRing: store allocated used/filled Metadata buffers
/// *Allocates MME buffers
/// *Stores allocated MME buffers within InterMixerFreeBufRing & InterMixerFreeMDRing
///
PlayerStatus_t Mixer_Mme_c::AllocateInterMixerRings(unsigned int numberOfBuffer, unsigned int bufferSize)
{
    MME_DataBuffer_t *MMEBuf;

    SE_DEBUG(group_mixer, ">\n");

    /* Ensure Rings are freed before overriding references */
    FreeInterMixerRings();

    InterMixerFreeBufRing = RingGeneric_c::New(numberOfBuffer, "Mixer_Mme_c::InterMixerFreeBufRing");
    InterMixerFillBufRing = RingGeneric_c::New(numberOfBuffer, "Mixer_Mme_c::InterMixerFillBufRing");

    InterMixerFreeMDRing  = RingGeneric_c::New(numberOfBuffer, "Mixer_Mme_c::InterMixerFreeMDRing");
    InterMixerFillMDRing  = RingGeneric_c::New(numberOfBuffer, "Mixer_Mme_c::InterMixerFillMDRing");

    if (InterMixerFreeBufRing == NULL ||
        InterMixerFillBufRing == NULL ||
        InterMixerFreeMDRing  == NULL ||
        InterMixerFillMDRing  == NULL)
    {
        SE_ERROR("Failed to allocate Rings\n");
        FreeInterMixerRings();
        return PlayerError;
    }

    for (int i = 0; i < numberOfBuffer; i++)
    {
        if (MME_AllocDataBuffer(MMEHandle, bufferSize, MME_ALLOCATION_PHYSICAL, &MMEBuf) != MME_SUCCESS)
        {
            SE_ERROR("Failed to allocate interMixer MME PCM Buffer bufIdx:%d\n", i);
            FreeInterMixerRings();
            return PlayerError;
        }
        if (InterMixerFreeBufRing->Insert((uintptr_t) MMEBuf) != RingNoError)
        {
            SE_ERROR("Failed to insert into InterMixerFreeBufRing\n");
            MME_FreeDataBuffer(MMEBuf);
            FreeInterMixerRings();
            return PlayerError;
        }
        if (MME_AllocDataBuffer(MMEHandle, sizeof(tMetadata), MME_ALLOCATION_PHYSICAL, &MMEBuf) != MME_SUCCESS)
        {
            SE_ERROR("Failed to allocate interMixer MME MD Buffer bufIdx:%d\n", i);
            FreeInterMixerRings();
            return PlayerError;
        }
        if (InterMixerFreeMDRing->Insert((uintptr_t) MMEBuf) != RingNoError)
        {
            SE_ERROR("Failed to insert into InterMixerFreeMDRing\n");
            MME_FreeDataBuffer(MMEBuf);
            FreeInterMixerRings();
            return PlayerError;
        }
    }
    return PlayerNoError;
}

void Mixer_Mme_c::FreeInterMixerRing(RingGeneric_c *Ring)
{
    MME_DataBuffer_t *MMEBuf;

    if (Ring != NULL)
    {
        while (Ring->Extract((uintptr_t *) &MMEBuf, RING_NONE_BLOCKING) == RingNoError)
        {
            MME_FreeDataBuffer(MMEBuf);
        }
        delete Ring;
    }
}

void Mixer_Mme_c::FreeInterMixerRings()
{
    SE_DEBUG(group_mixer, ">\n");
    FreeInterMixerRing(InterMixerFreeBufRing);
    FreeInterMixerRing(InterMixerFillBufRing);
    FreeInterMixerRing(InterMixerFreeMDRing);
    FreeInterMixerRing(InterMixerFillMDRing);
}

////////////////////////////////////////////////////////////////////////////
///
/// MMETransformerSanityCheck
///
/// This method queries the capabilities of the mixer transformer Name input argument
/// and makes sure the mixer supports sufficient features to be usefully employed.
///
PlayerStatus_t Mixer_Mme_c::MMETransformerSanityCheck(const char *transformerName)
{
    MME_ERROR MMEStatus;
    MME_TransformerCapability_t Capability = { 0 };
    MME_LxMixerTransformerInfo_t MixerInfo = { 0 };

    // Obtain the capabilities of the mixer
    Capability.StructSize = sizeof(Capability);
    Capability.TransformerInfo_p = &MixerInfo;
    Capability.TransformerInfoSize = sizeof(MixerInfo);
    MMEStatus = MME_GetTransformerCapability(transformerName, &Capability);

    if (MMEStatus != MME_SUCCESS)
    {
        SE_ERROR("%s - Unable to read capabilities (%d)\n", transformerName, MMEStatus);
        return PlayerError;
    }

    SE_INFO(group_mixer, "%s Found %s transformer (version %x)\n", Name, transformerName, Capability.Version);
    SE_VERBOSE(group_mixer, "MixerCapabilityFlags = %08x\n", MixerInfo.MixerCapabilityFlags);
    SE_VERBOSE(group_mixer, "PcmProcessorCapabilityFlags[0..1] = %08x %08x\n",
               MixerInfo.PcmProcessorCapabilityFlags[0], MixerInfo.PcmProcessorCapabilityFlags[1]);

    // Verify that is it fit for purpose
    if (MixerInfo.StructSize != sizeof(MixerInfo))
    {
        SE_ERROR("Detected structure size skew between firmware and driver\n");
        return PlayerError;
    }
    return PlayerNoError;
}

////////////////////////////////////////////////////////////////////////////
///
/// Initialize the mixer transformer.
///
/// This method queries the capabilities of the mixer and after making sure
/// the mixer support sufficient features to be usefully employed initialized
/// a transformer and configures it.
///
/// Typically it will be configured with a 'holding pattern' since at this
/// stage it is unlikely that the proper configuration is known.
///

PlayerStatus_t Mixer_Mme_c::InitializeMMETransformer(MME_TransformerInitParams_t *initParams, bool isFirstMixer)
{
    MME_ERROR MMEStatus;
    SE_DEBUG(group_mixer, ">%s\n", Name);

    if (initParams == NULL)
    {
        SE_ERROR("initParams pointer is NULL\n");
        return PlayerError;
    }

    // Check FW driver compatibility
    if (MMETransformerSanityCheck(MMETransformerName) != PlayerNoError)
    {
        return PlayerError;
    }

    //
    // Initialize the transformer
    //
    *initParams = (MME_TransformerInitParams_t) {0}; // Ensure that we clear down the MMEInitParams
    initParams->StructSize = sizeof(*initParams);
    // Set priority to above normal (above the decoders priority).
    // Because mixer has to produce data for players which runs at the real time.
    // We can't take risk of de-scheduling (or block) mixer's thread by decoder's thread that may leads to under-flow.
    // But when SpdifIn codec runs in the capture mode its priority should be same as mixer's thread priority.
    initParams->Priority = MME_PRIORITY_ABOVE_NORMAL;
    initParams->Callback = isFirstMixer ? MMEFirstMixerCallbackStub : MMECallbackStub;
    initParams->CallbackUserData = static_cast<void *>(this);
    initParams->TransformerInitParamsSize = sizeof(MixerParams);
    initParams->TransformerInitParams_p = &MixerParams;

    MixerParams = (MME_LxMixerTransformerInitBDParams_Extended_t) {0}; // Ensure that we clear down the MixerParams
    MixerParams.StructSize = sizeof(MixerParams);
    MixerParams.CacheFlush = ACC_MME_ENABLED;
    {
        uint32_t PrimaryClientEnableAudioCRC(0);
        Client[PrimaryClient].LockTake();

        if (true == Client[PrimaryClient].ManagedByThread())
        {
            // This client is correctly stated, and its manifestor is well known.
            PrimaryClientEnableAudioCRC = Client[PrimaryClient].GetManifestor()->EnableAudioCRC;
        }

        Client[PrimaryClient].LockRelease();
        MixerParams.MemConfig.Crc.CRC_Main = AudioConfiguration.CRC = PrimaryClientEnableAudioCRC;
        MixerParams.MemConfig.Crc.CRC_ALL = (PrimaryClientEnableAudioCRC == 2);
    }
    MixerParams.NbInput          = isFirstMixer ? FIRST_MIXER_MAX_CLIENTS : ACC_MIXER_MAX_NB_INPUT;
    MixerParams.DisableMixing    = 0;
    MixerParams.EnableSFreqGroup = isFirstMixer ? 1 : 0;
    MixerParams.BlockWise        = 0; // transform will be processed in subblocks of 256 ; (64 * (2^Blockwise));
    MixerParams.Index            = 0;

    // the large this value to more memory we'll need on the co-processor (if we didn't consider the topology
    // here we'ld blow our memory budget on STi710x)
    if ((MixerSettings.MixerOptions.MasterGrain) && (MixerSettings.MixerOptions.MasterGrain <= ModuleParameter_GetMixerMaxGrain()))
    {
        MasterOutputGrain = SND_PSEUDO_MIXER_ADJUST_GRAIN(MixerSettings.MixerOptions.MasterGrain);
    }
    else
    {
        MasterOutputGrain = ModuleParameter_GetMixerDefaultGrain();
    }

    MixerParams.MaxNbOutputSamplesPerTransform = LockClientAndLookupMixerGranuleSize(LookupPcmPlayersMaxSamplingFrequency());
    // The interleaving of the output buffer that the host will send to the mixer at each transform.
    // Note that we are using the structure in a 'backwards compatible' mode. If we need to be more
    // expressive we must look at MME_OutChan_t.
    MixerParams.OutputNbChannels.Card = (FirstActivePlayer != MIXER_MME_NO_ACTIVE_PLAYER) ? MixerPlayer[FirstActivePlayer].GetCardNumberOfChannels() : 0;

    if ((true == MixerPlayer[0].IsPlayerObjectAttached()) && MixerPlayer[0].GetCard().alsaname[0] != '\0')
    {
        MixerParams.OutputNbChannels.SubCard.NbChanMain = MixerPlayer[0].GetCardNumberOfChannels();
        MixerParams.OutputNbChannels.SubCard.StrideMain = MixerParams.OutputNbChannels.SubCard.NbChanMain;
    }

    if ((true == MixerPlayer[1].IsPlayerObjectAttached()) && MixerPlayer[1].GetCardNumberOfChannels() != '\0')
    {
        MixerParams.OutputNbChannels.SubCard.NbChanAux = MixerPlayer[1].GetCardNumberOfChannels();
        MixerParams.OutputNbChannels.SubCard.StrideAux = MixerParams.OutputNbChannels.SubCard.NbChanAux;
    }

    if ((true == MixerPlayer[2].IsPlayerObjectAttached()) && MixerPlayer[2].GetCard().alsaname[0] != '\0')
    {
        MixerParams.OutputNbChannels.SubCard.NbChanDig = MixerPlayer[2].GetCardNumberOfChannels();
        MixerParams.OutputNbChannels.SubCard.StrideDig = MixerParams.OutputNbChannels.SubCard.NbChanDig;
    }

    if ((true == MixerPlayer[3].IsPlayerObjectAttached()) && MixerPlayer[3].GetCard().alsaname[0] != '\0')
    {
        MixerParams.OutputNbChannels.SubCard.NbChanHdmi = MixerPlayer[3].GetCardNumberOfChannels();
        MixerParams.OutputNbChannels.SubCard.StrideHdmi = MixerParams.OutputNbChannels.SubCard.NbChanHdmi;
    }

    SE_INFO(group_mixer,  "%s MixerParams.OutputNbChannels = 0x%08x\n", Name, MixerParams.OutputNbChannels.Card);

    // choose the output mode (either the sampling frequency of the mixer or a 'floating' value that can
    // be specified later)

    // Check if mixer sampling frequency possible forced by STKPI control, i.e. STM_SE_FIXED_OUTPUT_FREQUENCY
    if (STM_SE_FIXED_OUTPUT_FREQUENCY == MixerSettings.MixerOptions.OutputSfreq.control)
    {
        MixerParams.OutputSamplingFreq = StmSeTranslateIntegerSamplingFrequencyToDiscrete(MixerSettings.MixerOptions.OutputSfreq.frequency);
        InitializedSamplingFrequency = StmSeTranslateDiscreteSamplingFrequencyToInteger(MixerParams.OutputSamplingFreq);
        SE_INFO(group_mixer, "%s Mixer sampling frequency currently initialized to %d\n", Name, InitializedSamplingFrequency);
    }
    else
    {
        MixerParams.OutputSamplingFreq = (eAccFsCode)AUDIOMIXER_OVERRIDE_OUTFS;
        InitializedSamplingFrequency = MIXER_SAMPLING_FREQUENCY_STKPI_UNSET;
    }

    {
        for (uint32_t ClientIdx(0); ClientIdx < MIXER_MAX_CLIENTS; ClientIdx++)
        {
            Client[ClientIdx].LockTake();
        }

        // provide sane default values (to ensure they are never zero when consumed)
        MixerSamplingFrequency = NominalOutputSamplingFrequency = LookupMixerSamplingFrequency();
        if (isFirstMixer)
        {
            // Force update of MixerSettings to get a fully initilialized GlobalParams struct
            MixerSettings.MixerOptions.GainUpdated = true;
            MixerSettings.MixerOptions.PanUpdated = true;
            MixerSettings.MixerOptions.PostMixGainUpdated = true;

            FillOutFirstMixerGlobalParameters(&MixerParams.GlobalParams);

            if (AllocateInterMixerRings(INTERMIXER_BUFFER_COUNT, MAX_INTER_BUFFER_SIZE) != PlayerNoError)
            {
                SE_ERROR("Failed to allocate InterMixerRings\n");
                for (uint32_t ClientIdx(0); ClientIdx < MIXER_MAX_CLIENTS; ClientIdx++)
                {
                    Client[ClientIdx].LockRelease();
                }
                return PlayerError;
            }
        }
        else
        {
            MMENeedsParameterUpdate = false;
            FillOutTransformerGlobalParameters(&MixerParams.GlobalParams);
        }

        for (uint32_t ClientIdx(0); ClientIdx < MIXER_MAX_CLIENTS; ClientIdx++)
        {
            Client[ClientIdx].LockRelease();
        }
    }

    //As dynamic, we need to fixup the sizes we've declared
    MixerParams.StructSize -= (sizeof(MixerParams.GlobalParams) - MixerParams.GlobalParams.StructSize);
    initParams->TransformerInitParamsSize = MixerParams.StructSize;
    if (isFirstMixer)
    {
        MMEStatus = MME_InitTransformer(FIRST_MIXER_TRANSFORMER, initParams, &MMEFirstMixerHdl);
    }
    else
    {
        MMEStatus = MME_InitTransformer(MMETransformerName, initParams, &MMEHandle);
    }

    if (MMEStatus != MME_SUCCESS)
    {
        SE_ERROR("%s Failed to initialize %s (%08x)\n",
                 isFirstMixer ? "FirstMixer" : "", MMETransformerName, MMEStatus);
        return PlayerError;
    }

    if (!isFirstMixer)
    {
        MMEInitialized = true;
        MMECallbackThreadBoosted = false;
    }
    CheckAndStartClients();
    SE_DEBUG(group_mixer, "<\n\n");

    return PlayerNoError;
}


////////////////////////////////////////////////////////////////////////////
///
/// Attempt to terminate the MME mix transformer.
///
/// Keep attempting to terminate the mix transformer until it returns an
/// status code other than MME_COMMAND_STILL_EXECUTING or it a timeout
/// occurs.
///
PlayerStatus_t Mixer_Mme_c::TerminateMMETransformer(MME_TransformerHandle_t MMEHdl)
{
    MME_ERROR Status;
    SE_DEBUG(group_mixer, ">%s\n", Name);

    if (MMEInitialized && (MMEHdl != MME_INVALID_ARGUMENT))
    {
        //
        // Wait a reasonable time for all mme transactions to terminate
        //
        // CAUTION about signed int !!
        int32_t TimeToWait(MIXER_MAX_WAIT_FOR_MME_COMMAND_COMPLETION);

        for (;;)
        {
            Status = MME_TermTransformer(MMEHdl);

            if (Status != MME_COMMAND_STILL_EXECUTING)
            {
                break;
            }
            else
            {
                if (TimeToWait > 0)
                {
                    TimeToWait -= 10;
                    OS_SleepMilliSeconds(10);
                }
                else
                {
                    break;
                }
            }
        }

        if (Status == MME_SUCCESS)
        {
            MMEHdl = MME_INVALID_ARGUMENT;
        }
        else
        {
            return PlayerError;
        }
    }

    return PlayerNoError;
}

PlayerStatus_t Mixer_Mme_c::TerminateMMETransformers()
{
    PlayerStatus_t Status = PlayerNoError;
    PlayerStatus_t Status1, Status2;

    Status1 = TerminateMMETransformer(MMEHandle);
    Status2 = TerminateMMETransformer(MMEFirstMixerHdl);
    if (Status1 != PlayerNoError)
    {
        SE_ERROR("Cannot Terminate MME Transformer\n");
        Status = Status1;
    }

    if (Status2 != PlayerNoError)
    {
        SE_ERROR("Cannot Terminate First MME Transformer\n");
        Status = Status2;
    }
    MMEInitialized = false;
    FreeInterMixerRings();

    return Status;
}

////////////////////////////////////////////////////////////////////////////
///
/// Issue the (already populated) command to the mixer transformer.
///
PlayerStatus_t Mixer_Mme_c::SendMMEMixCommand()
{
    MME_ERROR ErrorCode;

#define MIXER_INPUT_TRACE(mixCommand, name, inputId)                                                                      \
do{                                                                                                                       \
    SE_VERBOSE2(group_mixer, group_se_pipeline,                                                                           \
            "> %s [%p]"name": %15s (%d ScatterPages; BufferFlags:0x%X; StartOffset:%d)\n",                                \
            Name, &mixCommand.InputParams.InputParam[inputId],                                                            \
            LookupMixerCommand(mixCommand.InputParams.InputParam[inputId].Command),                                       \
            mixCommand.DataBuffers[inputId].NumberOfScatterPages,                                                         \
            mixCommand.DataBuffers[inputId].Flags,                                                                        \
            mixCommand.InputParams.InputParam[inputId].StartOffset);                                                      \
                                                                                                                          \
    if (inputId < MIXER_MAX_CLIENTS)                                                                                      \
    {                                                                                                                     \
        for (int i = 0; i < mixCommand.DataBuffers[inputId].NumberOfScatterPages; i++)                                    \
        {                                                                                                                 \
            AudioStreamBuffer_t *StreamBufPtr = ((AudioStreamBuffer_t **) mixCommand.DataBuffers[inputId].UserData_p)[i]; \
                AudioSurfaceParameters_t *SurfaceParams = &StreamBufPtr->AudioParameters->Source;                         \
                SE_EXTRAVERB(group_mixer, "Page[%d] Freq:%6d SamplesCount:%6d  bytes:%6d (OriginalSamplesCount:%6d)\n",   \
                        i, SurfaceParams->SampleRateHz,                                                                   \
                        mixCommand.DataBuffers[inputId].ScatterPages_p[i].Size /                                          \
                        (SurfaceParams->ChannelCount * (SurfaceParams->BitsPerSample / 8)),                               \
                        mixCommand.DataBuffers[inputId].ScatterPages_p[i].Size,                                           \
                        StreamBufPtr->AudioParameters->SampleCount);                                                      \
        }                                                                                                                 \
    }                                                                                                                     \
} while (0);

    if (SE_IS_VERBOSE_ON(group_mixer))
    {
        MIXER_INPUT_TRACE(MixerCommand, "input0          ", 0)
        MIXER_INPUT_TRACE(MixerCommand, "input1          ", 1)
        MIXER_INPUT_TRACE(MixerCommand, "input2          ", 2)
        MIXER_INPUT_TRACE(MixerCommand, "input3          ", 3)
        MIXER_INPUT_TRACE(MixerCommand, "SPDIF CodedInput", MIXER_CODED_DATA_INPUT + MIXER_CODED_DATA_INPUT_SPDIF_IDX)
        MIXER_INPUT_TRACE(MixerCommand, "HDMI  CodedInput", MIXER_CODED_DATA_INPUT + MIXER_CODED_DATA_INPUT_HDMI_IDX)
        MIXER_INPUT_TRACE(MixerCommand, "AudioGenerator0 ", MIXER_AUDIOGEN_DATA_INPUT)
        MIXER_INPUT_TRACE(MixerCommand, "AudioGenerator1 ", MIXER_AUDIOGEN_DATA_INPUT + 1)
        MIXER_INPUT_TRACE(MixerCommand, "Interactive     ", MIXER_INTERACTIVE_INPUT)
    }

    MixCommandStartTime = OS_GetTimeInMicroSeconds();
    ErrorCode = MME_SendCommand(MMEHandle, &MixerCommand.Command);
    if (ErrorCode != MME_SUCCESS)
    {
        SE_ERROR("Could not issue mixer command (%d)\n", ErrorCode);
        return PlayerError;
    }

    if (MMEFirstMixerHdl != MME_INVALID_ARGUMENT)
    {
        if (SE_IS_VERBOSE_ON(group_mixer))
        {
            MIXER_INPUT_TRACE(FirstMixerCommand, "FirstMixer input0", 0)
            MIXER_INPUT_TRACE(FirstMixerCommand, "FirstMixer input1", 1)
        }
        ErrorCode = MME_SendCommand(MMEFirstMixerHdl, &FirstMixerCommand.Command);
        if (ErrorCode != MME_SUCCESS)
        {
            SE_ERROR("Could not issue First mixer command (%d)\n", ErrorCode);
            return PlayerError;
        }
    }

#undef MIXER_INPUT_TRACE

    return PlayerNoError;
}

////////////////////////////////////////////////////////////////////////////
///
///
///
void Mixer_Mme_c::ReleaseNonQueuedBuffersOfStoppingClients()
{
    for (uint32_t clientIndex = 0; clientIndex < MIXER_MAX_CLIENTS; clientIndex++)
    {
        if (Client[clientIndex].GetState() == STOPPING)
        {
            Manifestor_AudioKsound_c *clientManifestor = Client[clientIndex].GetManifestor();

            if (clientManifestor != NULL)
            {
                clientManifestor->ReleaseProcessingBuffers();
            }
            else
            {
                SE_ERROR("Client[clientIndex].GetManifestor() is NULL\n");
            }
        }
    }
}

////////////////////////////////////////////////////////////////////////////
///
///
///
void Mixer_Mme_c::CleanUpOnError()
{
    SE_DEBUG(group_mixer, ">\n");
    ReleaseNonQueuedBuffersOfStoppingClients();
    SetStoppingClientsToStoppedState();
}

static inline const char *LookupMixerState(enum eMixerState state)
{
    switch (state)
    {
#define E(x) case x: return #x
        E(MIXER_INPUT_NOT_RUNNING);
        E(MIXER_INPUT_RUNNING);
        E(MIXER_INPUT_FADING_OUT);
        E(MIXER_INPUT_MUTED);
        E(MIXER_INPUT_PAUSED);
        E(MIXER_INPUT_FADING_IN);
#undef E
    default:
        return "INVALID";
    }
}

////////////////////////////////////////////////////////////////////////////
///
/// Block until the MME delivers its callback and update the client structures.
///
/// \todo Review this method for unmuted stops (I think we need an extra STOPPING state so we are
///       aware whether or not soft mute has been applied yet).
///
PlayerStatus_t Mixer_Mme_c::WaitForMMECallback()
{
    PlayerStatus_t Status;
    SE_VERBOSE(group_mixer, ">%s\n", Name);
#define MIXER_INPUT_STATUS_TRACE(MixCommand, inputname, Id)                                                  \
    SE_VERBOSE(group_mixer, "%20s: State:%25s BytesUsed:%8d NbInSplNextTransform:%6d MetadataStatus:0x%X\n", \
            inputname,                                                                                       \
            LookupMixerState(MixCommand.OutputParams.MixStatus.InputStreamStatus[Id].State),                 \
            MixCommand.OutputParams.MixStatus.InputStreamStatus[Id].BytesUsed,                               \
            MixCommand.OutputParams.MixStatus.InputStreamStatus[Id].NbInSplNextTransform,                    \
            MixCommand.OutputParams.MixStatus.InputStreamStatus[Id].MetadataStatus);

    OS_SemaphoreWaitAuto(&MMECallbackSemaphore);
    if (SE_IS_VERBOSE_ON(group_mixer))
    {
        SE_VERBOSE(group_mixer, "%s: MME Mix Command round-trip duration:%6lldus FW-processingTime:%uus\n",
                   Name, OS_GetTimeInMicroSeconds() - MixCommandStartTime,
                   MixerCommand.OutputParams.MixStatus.ElapsedTime / 2); // ST231 timer counter is bugged by a factor 2

        MIXER_INPUT_STATUS_TRACE(MixerCommand, "Client 0",           0);
        MIXER_INPUT_STATUS_TRACE(MixerCommand, "Client 1",           1);
        MIXER_INPUT_STATUS_TRACE(MixerCommand, "CODED_DATA_INPUT 0", MIXER_CODED_DATA_INPUT);
        MIXER_INPUT_STATUS_TRACE(MixerCommand, "CODED_DATA_INPUT 1", MIXER_CODED_DATA_INPUT + 1);
    }
    if (MixerCommand.Command.CmdStatus.State != MME_COMMAND_COMPLETED)
    {
        SE_ERROR("MME command failed (State %d Error %d)\n",
                 MixerCommand.Command.CmdStatus.State, MixerCommand.Command.CmdStatus.Error);
    }
    if (MixerCommand.OutputParams.MixStatus.OutputMetadataStatus != METADATA_STATUS_OK)
    {
        SE_WARNING("%s failed to output metadata err:0x%X\n",
                   Name, MixerCommand.OutputParams.MixStatus.OutputMetadataStatus);
    }

    Status = UpdateOutputBuffers();

    if (MMEFirstMixerHdl != MME_INVALID_ARGUMENT)
    {
        OS_SemaphoreWaitAuto(&MMEFirstMixerCallbackSemaphore);
        if (SE_IS_VERBOSE_ON(group_mixer))
        {
            SE_VERBOSE(group_mixer, "%s: FirstMixer MME Mix Command round-trip duration:%6lldus FW-processingTime:%uus\n",
                       Name, OS_GetTimeInMicroSeconds() - MixCommandStartTime,
                       FirstMixerCommand.OutputParams.MixStatus.ElapsedTime / 2); // ST231 timer counter is bugged by a factor 2

            MIXER_INPUT_STATUS_TRACE(FirstMixerCommand, "FIRST MIXER Client 0", 0);
            MIXER_INPUT_STATUS_TRACE(FirstMixerCommand, "FIRST MIXER Client 1", 1);
        }

        if (FirstMixerCommand.OutputParams.MixStatus.OutputMetadataStatus != METADATA_STATUS_OK)
        {
            SE_WARNING("%s failed to output metadata err:0x%X\n",
                       Name, FirstMixerCommand.OutputParams.MixStatus.OutputMetadataStatus);
        }

        Status = UpdateFirstMixerOutputBuffers();
        UpdateSecondMixerInputBuffer();

        if (FirstMixerCommand.OutputParams.MixStatus.AudioMode != InterMixerBufferAcMod)
        {
            SE_DEBUG(group_mixer, "FirstMixer output AcMod Changed: %s => %s\n",
                     LookupAudioMode(InterMixerBufferAcMod),
                     LookupAudioMode(FirstMixerCommand.OutputParams.MixStatus.AudioMode));

            InterMixerBufferAcMod    = FirstMixerCommand.OutputParams.MixStatus.AudioMode;
            MMENeedsParameterUpdate  = true;
            ClientAudioParamsUpdated = true;
        }

        CascadeADMixerStatusParamsToSystemMixer();
    }
#undef MIXER_INPUT_STATUS_TRACE

    // Whatever the status of OutputBuffer updation generally because of overrun at player stage,
    // we should update input buffers as
    // these data have been processed and if we don't update the same data will be resent to
    // mixer on next transform leading to "inconsistent" output.
    for (uint32_t ClientIdx(0); ClientIdx < MIXER_MAX_CLIENTS; ClientIdx++)
    {
        UpdateInputBuffer(ClientIdx);
    }

    for (uint32_t AudioGeneratorIdx = 0; AudioGeneratorIdx < MAX_AUDIO_GENERATORS; AudioGeneratorIdx++)
    {
        UpdateAudioGenBuffer(AudioGeneratorIdx);
    }

    for (uint32_t InteractiveAudioIdx = 0; InteractiveAudioIdx < MAX_INTERACTIVE_AUDIO; InteractiveAudioIdx++)
    {
        UpdateInteractiveAudioBuffer(InteractiveAudioIdx);
    }

    if (Status != PlayerNoError)
    {
        SE_ERROR("Failed to update the output buffer\n");
        return PlayerError;
    }

    if (AudioConfiguration.CRC == true)
    {
        union
        {
            MME_MixerFrameExtStatus_t *s;
            MME_MixerStatusTemplate_t *t;
            MME_CrcStatus_t            *crc;
            char                       *p;
        } status_u;
        int bytes_used, ssize;
        status_u.p = &((char *) &MixerCommand.OutputParams)[MixerCommand.OutputParams.MixStatus.StructSize];
        bytes_used = status_u.s->BytesUsed;
        status_u.t = & status_u.s->MixExtStatus;

        while (bytes_used >= (int)sizeof(MME_MixerStatusTemplate_t))
        {
            ssize = status_u.t->StructSize;

            if ((status_u.t->Id == ACC_CRC_ID) && (bytes_used >= ssize))
            {
                st_relayfs_write_se(ST_RELAY_TYPE_AUDIO_MIXER_CRC, ST_RELAY_SOURCE_SE,
                                    (unsigned char *) &status_u.crc->Crc[0], sizeof(status_u.crc->Crc), false);
            }

            bytes_used -= ssize;
            status_u.p += ssize;
        }
    }

    return PlayerNoError;
}


////////////////////////////////////////////////////////////////////////////
///
/// Provide sensible defaults for the output configuration.
///
void Mixer_Mme_c::ResetOutputConfiguration()
{
    SE_DEBUG(group_mixer, "\n");
    NumberOfAudioPlayerAttached = 0;
    FirstActivePlayer = MIXER_MME_NO_ACTIVE_PLAYER;
    MixerSettings.Reset();
}


////////////////////////////////////////////////////////////////////////////
///
/// Reset the 'meta data' gain/pan configuration.
///
/// If the mixer is configured to honour secondary stream meta data then
/// reset the associated values.
///
void Mixer_Mme_c::ResetMixingMetadata()
{
    SE_DEBUG(group_mixer, ">%s\n", Name);

    if (MixerSettings.OutputConfiguration.metadata_update != SND_PSEUDO_MIXER_METADATA_UPDATE_NEVER)
    {
        for (uint32_t ChannelIdx(0); ChannelIdx < SND_PSEUDO_MIXER_CHANNELS; ChannelIdx++)
        {
            MixerSettings.MixerOptions.Gain[PrimaryClient][ChannelIdx] = Q3_13_UNITY;
        }

        for (uint32_t ChannelIdx(0); ChannelIdx < SND_PSEUDO_MIXER_CHANNELS; ChannelIdx++)
        {
            MixerSettings.MixerOptions.Gain[SecondaryClient][ChannelIdx] = Q3_13_UNITY;
            MixerSettings.MixerOptions.Pan[SecondaryClient][ChannelIdx] = Q3_13_UNITY;
        }

        if (MixerSettings.OutputConfiguration.metadata_update == SND_PSEUDO_MIXER_METADATA_UPDATE_ALWAYS)
        {
            MixerSettings.MixerOptions.PostMixGain = Q3_13_UNITY;
        }
    }

    // Populate the flags with value "true" during reset so that structures dependent on the below params
    // in function Mixer_Mme_c::FillOutTransformerGlobalParameters() are filled
    MixerSettings.MixerOptions.GainUpdated = true;
    MixerSettings.MixerOptions.PanUpdated = true;
    MixerSettings.MixerOptions.PostMixGainUpdated = true;
    MixerSettings.MixerOptions.AudioGeneratorGainUpdated = true;
    MixerSettings.MixerOptions.InteractiveGainUpdated = true;
    MixerSettings.MixerOptions.InteractivePanUpdated = true;
    SE_DEBUG(group_mixer, "<%s\n", Name);
}

////////////////////////////////////////////////////////////////////////////
//
// FillOutGlobalPcmInputMixerConfig
//
void Mixer_Mme_c::FillOutGlobalPcmInputMixerConfig(MME_LxMixerInConfig_t *InConfig, bool isFirstMixer)
{
    MME_MixerInputConfig_t *clientConfig;

    uint32_t nb_clients = isFirstMixer ? FIRST_MIXER_MAX_CLIENTS : MIXER_MAX_CLIENTS;
    for (uint32_t ClientIdx(0);
         ClientIdx < min(ACC_MIXER_MAX_NB_INPUT, nb_clients);
         ClientIdx++)
    {
        clientConfig = &InConfig->Config[ClientIdx];

        // b[7..4] :: Input Type | b[3..0] :: Input Number
        clientConfig->InputId = (ACC_MIXER_LINEARISER << 4) + ClientIdx;
        clientConfig->Alpha = ACC_ALPHA_DONT_CARE;  // Mixing coefficients are given "Per Channel" through the BD Gain Set of coefficient
        clientConfig->Mono2Stereo = ACC_MME_TRUE;  // [enum eAccBoolean] Mono 2 Stereo upmix of a mono input

        if (mApplicationType == STM_SE_CTRL_VALUE_AUDIO_APPLICATION_MS11 ||
            mApplicationType == STM_SE_CTRL_VALUE_AUDIO_APPLICATION_MS12)
        {
            clientConfig->SfcFilterSelect = isFirstMixer ? RESAMPLING_QUALITY_DOLBY_MS1x : RESAMPLING_QUALITY_DEFAULT;
        }
        else
        {
            clientConfig->SfcFilterSelect = RESAMPLING_QUALITY_DEFAULT;
        }

        clientConfig->Lpcm.WordSize = ACC_WS32;     // Input Lpcm.WordSize : ACC_WS32 / ACC_WS16
        // To which output channel is mixer the 1st channel of this input stream
        clientConfig->FirstOutputChan = ACC_MAIN_LEFT;
        clientConfig->AutoFade = ACC_MME_TRUE;
        clientConfig->Config = 0;
        clientConfig->SystemSound     = STM_SE_CTRL_VALUE_APPLICATION_INPUT_TYPE;
        // Client Lock already taken by caller.
        SE_ASSERT(ClientIdx < MIXER_MAX_CLIENTS);
        if (Client[ClientIdx].ManagedByThread())
        {
            // This client is correctly stated, and its parameters are well known.
            clientConfig->AudioMode = (eAccAcMode) Client[ClientIdx].GetOrganisationParameter(); //  Channel Configuration

            if (MMEFirstMixerHdl != MME_INVALID_ARGUMENT && !isFirstMixer)
            {
                // In Dual Mixer setup, we force 2nd Mixer PCM Input parameter to match with 1st Mixer output parameters
                clientConfig->NbChannels = 8;
                clientConfig->SamplingFreq = StmSeTranslateIntegerSamplingFrequencyToDiscrete(MixerSamplingFrequency);
                clientConfig->AudioMode = InterMixerBufferAcMod;
            }
            else
            {
                AudioSurfaceParameters_t SourceParam = Client[ClientIdx].GetSourceSurfaceParameter();
                clientConfig->NbChannels = SourceParam.ChannelCount; // Interleaving of the input pcm buffers
                clientConfig->SamplingFreq = StmSeTranslateIntegerSamplingFrequencyToDiscrete(SourceParam.SampleRateHz);
            }
        }
        else
        {
            // workaround a bug in the mixer (it crashes on transition from 0 channels to 8 channels)
            clientConfig->NbChannels = 8;
            clientConfig->AudioMode = ACC_MODE20;
            clientConfig->SamplingFreq = StmSeTranslateIntegerSamplingFrequencyToDiscrete(MixerSamplingFrequency);
        }
        SE_DEBUG(group_mixer,  "Input %d: AudioMode %s (%d) SamplingFreq %s\n",
                 ClientIdx,
                 LookupAudioMode(clientConfig->AudioMode),
                 clientConfig->AudioMode,
                 LookupDiscreteSamplingFrequency(clientConfig->SamplingFreq));
    }
}

////////////////////////////////////////////////////////////////////////////
//
// FillOutGlobalCodedInputMixerConfig
//
void Mixer_Mme_c::FillOutGlobalCodedInputMixerConfig(MME_LxMixerInConfig_t *InConfig, bool *IsOutputBypassed)
{
    // Describe inputs corresponding to bypass coded data.
    for (uint32_t CodedIndex = 0; CodedIndex < MIXER_MAX_CODED_INPUTS; CodedIndex++)
    {
        MME_MixerInputConfig_t &CodedDataInConfig = InConfig->Config[MIXER_CODED_DATA_INPUT + CodedIndex];
        CodedDataInConfig.InputId         = MIXER_CODED_DATA_INPUT + CodedIndex;
        CodedDataInConfig.NbChannels      = 2;
        CodedDataInConfig.Alpha           = 0; // N/A
        CodedDataInConfig.Mono2Stereo     = ACC_MME_FALSE; // N/A
        CodedDataInConfig.Lpcm.WordSize   = ACC_WS32;
        CodedDataInConfig.AudioMode       = ACC_MODE20;
        CodedDataInConfig.SamplingFreq    = StmSeTranslateIntegerSamplingFrequencyToDiscrete(MixerSamplingFrequency);
        CodedDataInConfig.FirstOutputChan = ACC_MAIN_LEFT; // N/A
        CodedDataInConfig.AutoFade        = ACC_MME_FALSE; // N/A
        CodedDataInConfig.Config          = 0;
        CodedDataInConfig.SfcFilterSelect = RESAMPLING_QUALITY_DEFAULT;
        CodedDataInConfig.SystemSound     = STM_SE_CTRL_VALUE_APPLICATION_INPUT_TYPE;
    }

    // Reset SPDIF Idx and HDMI PLayerIdx as it will be reevaluated in the following loop.
    mSPDIFCardId = STM_SE_MIXER_NB_MAX_OUTPUTS;
    mHDMICardId  = STM_SE_MIXER_NB_MAX_OUTPUTS;

    // Look for all possible coded inputs of mixer according cards and players
    for (uint32_t CountOfAudioPlayerAttached(0), PlayerIdx(0); CountOfAudioPlayerAttached < NumberOfAudioPlayerAttached; PlayerIdx++)
    {
        if (true == MixerPlayer[PlayerIdx].IsPlayerObjectAttached())
        {
            CountOfAudioPlayerAttached++;
            bool IsConnectedToHdmi = MixerPlayer[PlayerIdx].IsConnectedToHdmi();
            bool IsConnectedToSpdifPlayer = MixerPlayer[PlayerIdx].IsConnectedToSpdif();
            PcmPlayer_c::OutputEncoding PlayerEncoding = MixerPlayer[PlayerIdx].LookupOutputEncoding(
                                                             PrimaryCodedDataType[MIXER_CODED_DATA_INPUT_SPDIF_IDX],
                                                             PrimaryCodedDataType[MIXER_CODED_DATA_INPUT_HDMI_IDX]);

            // Connected to SPDIF
            if (IsConnectedToSpdifPlayer)
            {
                mSPDIFCardId = PlayerIdx;
                PcmPlayer_c::OutputEncoding SPDIFEncoding(PlayerEncoding);

                // Is there some SPDIF bypass
                if (PcmPlayer_c::IsOutputBypassed(SPDIFEncoding))
                {
                    MME_MixerInputConfig_t &CodedDataInConfig = InConfig->Config[MIXER_CODED_DATA_INPUT + MIXER_CODED_DATA_INPUT_SPDIF_IDX];

                    // Client Lock already taken by caller.
                    if (Client[PrimaryClient].ManagedByThread() && (SPDIFEncoding == PcmPlayer_c::BYPASS_SPDIFIN_COMPRESSED_SD))
                    {
                        // This client is correctly stated, and its parameters are well known.
                        SpdifInProperties_t PrimaryClientSpdifInProperties = Client[PrimaryClient].GetSpdifInProperties();
                        CodedDataInConfig.NbChannels = PrimaryClientSpdifInProperties.ChannelCount;
                        CodedDataInConfig.AudioMode  = (eAccAcMode) PrimaryClientSpdifInProperties.Organisation;
                    }
                    CodedDataInConfig.InputId += ACC_MIXER_COMPRESSED_DATA << 4;
                    CodedDataInConfig.Config = mSPDIFCardId;
                    CodedDataInConfig.SamplingFreq = StmSeTranslateIntegerSamplingFrequencyToDiscrete(LookupIec60958FrameRateForBypass(SPDIFEncoding));
                    SE_INFO(group_mixer, "%s Applying coded data bypass to PCM chain %d, (%s) , Config 0x%x\n",
                            Name,
                            mSPDIFCardId,
                            PcmPlayer_c::LookupOutputEncoding(SPDIFEncoding),
                            CodedDataInConfig.Config);
                    *IsOutputBypassed = true;
                }
            }

            // Connected to HDMI Only.
            if (IsConnectedToHdmi && !IsConnectedToSpdifPlayer)
            {
                mHDMICardId = PlayerIdx;
                PcmPlayer_c::OutputEncoding HDMIEncoding(PlayerEncoding);

                // Is there some HDMI bypass
                if (PcmPlayer_c::IsOutputBypassed(HDMIEncoding))
                {
                    MME_MixerInputConfig_t &CodedDataInConfig = InConfig->Config[MIXER_CODED_DATA_INPUT + MIXER_CODED_DATA_INPUT_HDMI_IDX];
                    // Client Lock already taken by caller.
                    if (Client[PrimaryClient].ManagedByThread() && ((HDMIEncoding == PcmPlayer_c::BYPASS_SPDIFIN_PCM  || (HDMIEncoding == PcmPlayer_c::BYPASS_SPDIFIN_COMPRESSED))))
                    {
                        // This client is correctly stated, and its parameters are well known.
                        SpdifInProperties_t PrimaryClientSpdifInProperties = Client[PrimaryClient].GetSpdifInProperties();
                        CodedDataInConfig.NbChannels = PrimaryClientSpdifInProperties.ChannelCount;
                        CodedDataInConfig.AudioMode  = (eAccAcMode) PrimaryClientSpdifInProperties.Organisation;
                    }


                    CodedDataInConfig.InputId += ACC_MIXER_COMPRESSED_DATA << 4;
                    CodedDataInConfig.Config = mHDMICardId;
                    CodedDataInConfig.SamplingFreq = StmSeTranslateIntegerSamplingFrequencyToDiscrete(LookupIec60958FrameRateForBypass(HDMIEncoding));
                    SE_INFO(group_mixer, "%s Applying coded data bypass to PCM chain %d, (%s) , Config 0x%x\n",
                            Name,
                            mHDMICardId,
                            PcmPlayer_c::LookupOutputEncoding(HDMIEncoding),
                            CodedDataInConfig.Config);
                    *IsOutputBypassed = true;
                }
            }
        }
    }
}

////////////////////////////////////////////////////////////////////////////
//
// FillOutGlobalInteractiveInputMixerConfig
//
void Mixer_Mme_c::FillOutGlobalInteractiveInputMixerConfig(MME_LxMixerInConfig_t *InConfig)
{
    // Describe inputs corresponding to interactive audio clients.
    MME_MixerInputConfig_t &IAudioInConfig = InConfig->Config[MIXER_INTERACTIVE_INPUT];
    IAudioInConfig.InputId = (ACC_MIXER_IAUDIO << 4) + MIXER_INTERACTIVE_INPUT;
    IAudioInConfig.NbChannels = InConfig->Config[PrimaryClient].NbChannels; // same as primary audio
    IAudioInConfig.Alpha = MIXER_0dB_uQ0_16;
    IAudioInConfig.Mono2Stereo = ACC_MME_FALSE;
    IAudioInConfig.Lpcm.WordSize = ACC_WS16;
    IAudioInConfig.AudioMode = ACC_MODE_ID;
    {
        uint32_t InterractiveClientIdx;

        for (InterractiveClientIdx = 0; (InterractiveClientIdx < MAX_INTERACTIVE_AUDIO); InterractiveClientIdx++)
            if (InteractiveAudio[InterractiveClientIdx])
            {
                break;
            }

        if (InterractiveClientIdx != MAX_INTERACTIVE_AUDIO)
            IAudioInConfig.SamplingFreq = StmSeTranslateIntegerSamplingFrequencyToDiscrete(
                                              InteractiveAudio[InterractiveClientIdx]->GetSamplingFrequency() ?
                                              InteractiveAudio[InterractiveClientIdx]->GetSamplingFrequency() :
                                              MixerSamplingFrequency);
        else
            IAudioInConfig.SamplingFreq = StmSeTranslateIntegerSamplingFrequencyToDiscrete(
                                              MixerSamplingFrequency);
    }
    IAudioInConfig.FirstOutputChan = ACC_MAIN_LEFT;
    IAudioInConfig.AutoFade = ACC_MME_FALSE; // no point when it plays all the time
    IAudioInConfig.Config = 0;
    IAudioInConfig.SfcFilterSelect = RESAMPLING_QUALITY_DEFAULT;
    IAudioInConfig.SystemSound = STM_SE_CTRL_VALUE_APPLICATION_INPUT_TYPE;
}


////////////////////////////////////////////////////////////////////////////
//
// FillOutGlobalGeneratorInputMixerConfig
//
void Mixer_Mme_c::FillOutGlobalGeneratorInputMixerConfig(MME_LxMixerInConfig_t *InConfig)
{
    MME_MixerInputConfig_t *GeneratorCfg;
    for (uint32_t AudioGeneratorIdx = 0; AudioGeneratorIdx < MAX_AUDIO_GENERATORS; AudioGeneratorIdx++)
    {
        if (!Generator[AudioGeneratorIdx])
        {
            continue;
        }
        GeneratorCfg = &InConfig->Config[AudioGeneratorIdx + MIXER_AUDIOGEN_DATA_INPUT];

        stm_se_audio_generator_buffer_t audiogenerator_buffer;
        // b[7..4] :: Input Type | b[3..0] :: Input Number
        GeneratorCfg->InputId         = (ACC_MIXER_LINEARISER << 4) + (AudioGeneratorIdx + MIXER_AUDIOGEN_DATA_INPUT);
        GeneratorCfg->NbChannels      = Generator[AudioGeneratorIdx]->GetNumChannels(); // same as primary audio
        GeneratorCfg->Alpha           = ACC_ALPHA_DONT_CARE;
        GeneratorCfg->Mono2Stereo     = ACC_MME_FALSE;
        GeneratorCfg->AudioMode       = Generator[AudioGeneratorIdx]->GetAudioMode();

        if (Generator[AudioGeneratorIdx]->GetCompoundOption(STM_SE_CTRL_AUDIO_GENERATOR_BUFFER, &audiogenerator_buffer))
        {
            continue;
        }

        if (StmSeAudioGetLpcmWsFromLPcmFormat((enum eAccLpcmWs *) &GeneratorCfg->Lpcm, audiogenerator_buffer.format) != 0
            || GeneratorCfg->Lpcm.WordSize == ACC_LPCM_WS_UNDEFINED)
        {
            SE_WARNING("%s Failed to get Lpcm Format for AudioGenerator[%d] => Fallback to ACC_LPCM_WS32\n",
                       Name, AudioGeneratorIdx);
            GeneratorCfg->Lpcm.WordSize = ACC_LPCM_WS32;
        }
        GeneratorCfg->Lpcm.Definition = 1; // Required to specify that we are providing value formatted as an enum eAccLpcmWs

        GeneratorCfg->SamplingFreq = StmSeTranslateIntegerSamplingFrequencyToDiscrete(
                                         Generator[AudioGeneratorIdx]->GetSamplingFrequency() ?
                                         Generator[AudioGeneratorIdx]->GetSamplingFrequency() :
                                         MixerSamplingFrequency);

        stm_se_input_mixing_type_t mixType;
        mixType.input = (enum stm_se_input_mixer_role_e)(AudioGeneratorIdx + STM_SE_CTRL_VALUE_MIXER_ROLE_AUDIO_GENERATOR_0);

        if (GetCompoundOption(STM_SE_CTRL_AUDIO_MIXER_INPUT_TYPE, &mixType) != PlayerNoError)
        {
            GeneratorCfg->SystemSound = STM_SE_CTRL_VALUE_APPLICATION_INPUT_TYPE;
            SE_ERROR("Failed to get audiogenerator mixer input Type\n");
        }
        else
        {
            GeneratorCfg->SystemSound = mixType.input_type;
        }

        // To which output channel is mixer the 1st channel of
        // this input stream
        GeneratorCfg->FirstOutputChan = ACC_MAIN_LEFT;
        GeneratorCfg->AutoFade = ACC_MME_FALSE;
        GeneratorCfg->Config = 0;
        GeneratorCfg->SfcFilterSelect = RESAMPLING_QUALITY_DEFAULT;
    }
}

////////////////////////////////////////////////////////////////////////////
//
// FillOutGlobalInputGainConfig
//
void Mixer_Mme_c::FillOutGlobalInputGainConfig(MME_LxMixerGainSet_t *InGainConfig)
{
    SE_VERBOSE(group_mixer, "\n");
    for (uint32_t ClientIdx(0); ClientIdx < ACC_MIXER_MAX_NB_INPUT; ClientIdx++)
    {
        InGainConfig->GainSet[ClientIdx].InputId = ClientIdx;

        for (uint32_t ChannelIdx(0); ChannelIdx < SND_PSEUDO_MIXER_CHANNELS; ChannelIdx++)
        {
            U16 *AlphaPerChannel = &InGainConfig->GainSet[ClientIdx].AlphaPerChannel[ChannelIdx];

            if (mTopology.type == STM_SE_MIXER_DUAL_STAGE_MIXING && ClientIdx < FIRST_MIXER_MAX_CLIENTS)
            {
                // On dual stage mixer, input gains are applied on FirstStageMixer
                *AlphaPerChannel = Q3_13_UNITY;
            }
            else if (ClientIdx < MIXER_MAX_CLIENTS)
            {
                *AlphaPerChannel = MixerSettings.MixerOptions.Gain[ClientIdx][ChannelIdx];
            }
            else if ((ClientIdx >= MIXER_AUDIOGEN_DATA_INPUT && ClientIdx < MIXER_AUDIOGEN_DATA_INPUT + MIXER_MAX_AUDIOGEN_INPUTS))
            {
                int AudioGenIdx = ClientIdx - MIXER_AUDIOGEN_DATA_INPUT;
                SE_ASSERT(AudioGenIdx < MIXER_MAX_AUDIOGEN_INPUTS);
                *AlphaPerChannel = MixerSettings.MixerOptions.AudioGeneratorGain[AudioGenIdx][ChannelIdx];
            }
            else if (ClientIdx == MIXER_INTERACTIVE_INPUT)
            {
                *AlphaPerChannel = MixerSettings.MixerOptions.InteractiveGain[ChannelIdx];
            }
            else
            {
                *AlphaPerChannel = Q3_13_UNITY;
            }
        }
    }
}

////////////////////////////////////////////////////////////////////////////
//
// FillOutGlobalInputPanningConfig
//
void Mixer_Mme_c::FillOutGlobalInputPanningConfig(MME_LxMixerPanningSet_t *InPanningConfig)
{
    for (uint32_t ClientIdx(0); ClientIdx < ACC_MIXER_MAX_NB_INPUT; ClientIdx++)
    {
        InPanningConfig->PanningSet[ClientIdx].InputId = ClientIdx;

        for (uint32_t ChannelIdx(0); ChannelIdx < SND_PSEUDO_MIXER_CHANNELS; ChannelIdx++)
        {
            U16 *ChannelPanning = &InPanningConfig->PanningSet[ClientIdx].Panning[ChannelIdx];

            if ((ClientIdx == PrimaryClient) || (ClientIdx == MIXER_INTERACTIVE_INPUT))
            {
                *ChannelPanning = Q3_13_UNITY;
            }
            else if (ClientIdx == SecondaryClient)
            {
                *ChannelPanning = MixerSettings.MixerOptions.Pan[SecondaryClient][ChannelIdx];
            }
            else if ((ClientIdx >= MIXER_AUDIOGEN_DATA_INPUT && ClientIdx < MIXER_AUDIOGEN_DATA_INPUT + MIXER_MAX_AUDIOGEN_INPUTS))
            {
                int AudioGenIdx = ClientIdx - MIXER_AUDIOGEN_DATA_INPUT;
                SE_ASSERT(AudioGenIdx < MIXER_MAX_AUDIOGEN_INPUTS);
                *ChannelPanning = MixerSettings.MixerOptions.AudioGeneratorPan[AudioGenIdx][ChannelIdx];
            }
            else
            {
                *ChannelPanning = Q3_13_UNITY;
            }
        }
    }
}

////////////////////////////////////////////////////////////////////////////
//
// FillOutGlobalInteractiveAudioConfig
//
void Mixer_Mme_c::FillOutGlobalInteractiveAudioConfig(MME_LxMixerInIAudioConfig_t *InIaudioConfig)
{
    MME_MixerIAudioInputConfig_t *clientConfig;
    for (uint32_t InterractiveClientIdx(0); InterractiveClientIdx < MAX_INTERACTIVE_AUDIO; InterractiveClientIdx++)
    {
        clientConfig = &InIaudioConfig->ConfigIAudio[InterractiveClientIdx];
        clientConfig->InputId = InterractiveClientIdx;

        if (InteractiveAudio[InterractiveClientIdx])
        {
            clientConfig->NbChannels = InteractiveAudio[InterractiveClientIdx]->GetNumChannels();
            clientConfig->Alpha = MixerSettings.MixerOptions.InteractiveGain[InterractiveClientIdx];

            for (uint32_t ChannelIdx(0); ChannelIdx < SND_PSEUDO_MIXER_CHANNELS; ChannelIdx++)
            {
                clientConfig->Panning[ChannelIdx] = MixerSettings.MixerOptions.InteractivePan[InterractiveClientIdx][ChannelIdx];
            }

            clientConfig->Play = ACC_MME_TRUE;
        }
        else
        {
            // we set the gain of unused inputs to zero in order to get a de-pop during startup
            // due to gain smoothing
            clientConfig->NbChannels = 1;
            clientConfig->Alpha = 0;

            for (uint32_t ChannelIdx(0); ChannelIdx < SND_PSEUDO_MIXER_CHANNELS; ChannelIdx++)
            {
                clientConfig->Panning[ChannelIdx] = 0;
            }

            clientConfig->Play = ACC_MME_FALSE;
        }
    }
}

////////////////////////////////////////////////////////////////////////////
//
// FillOutGlobalOutputTopology
//
void Mixer_Mme_c::FillOutGlobalOutputTopology(MME_LxMixerOutTopology_t *OutTopology)
{
    if (FirstActivePlayer != MIXER_MME_NO_ACTIVE_PLAYER)
    {
        PcmPlayer_c::OutputEncoding SpdifEncoding = PrimaryCodedDataType[MIXER_CODED_DATA_INPUT_SPDIF_IDX];
        PcmPlayer_c::OutputEncoding HdmiEncoding  = PrimaryCodedDataType[MIXER_CODED_DATA_INPUT_HDMI_IDX];

        uint32_t nbChannels = MixerPlayer[FirstActivePlayer].LookupOutputNumberOfChannels(SpdifEncoding, HdmiEncoding);
        OutTopology->Topology.NbChannels = nbChannels;

        if ((true == MixerPlayer[0].IsPlayerObjectAttached()) && MixerPlayer[0].GetCard().alsaname[0] != '\0')
        {
            nbChannels = MixerPlayer[0].LookupOutputNumberOfChannels(SpdifEncoding, HdmiEncoding);
            OutTopology->Topology.Output.NbChanMain = nbChannels;
            OutTopology->Topology.Output.StrideMain = nbChannels;
        }

        if ((true == MixerPlayer[1].IsPlayerObjectAttached()) && MixerPlayer[1].GetCard().alsaname[0] != '\0')
        {
            nbChannels = MixerPlayer[1].LookupOutputNumberOfChannels(SpdifEncoding, HdmiEncoding);
            OutTopology->Topology.Output.NbChanAux = nbChannels;
            OutTopology->Topology.Output.StrideAux = nbChannels;
        }

        if ((true == MixerPlayer[2].IsPlayerObjectAttached()) && MixerPlayer[2].GetCard().alsaname[0] != '\0')
        {
            nbChannels = MixerPlayer[2].LookupOutputNumberOfChannels(SpdifEncoding, HdmiEncoding);
            OutTopology->Topology.Output.NbChanDig = nbChannels;
            OutTopology->Topology.Output.StrideDig = nbChannels;
        }

        if ((true == MixerPlayer[3].IsPlayerObjectAttached()) && MixerPlayer[3].GetCard().alsaname[0] != '\0')
        {
            nbChannels = MixerPlayer[3].LookupOutputNumberOfChannels(SpdifEncoding, HdmiEncoding);
            OutTopology->Topology.Output.NbChanHdmi = nbChannels;
            OutTopology->Topology.Output.StrideHdmi = nbChannels;
        }
    }
    else
    {
        OutTopology->Topology.NbChannels = 0;
    }

    SE_DEBUG(group_mixer, "LxMixerOutTopology: NbChanMain:%u/%u NbChanAux:%u/%u NbChanDig:%u/%u NbChanHdmi:%u/%u\n",
             OutTopology->Topology.Output.NbChanMain, OutTopology->Topology.Output.StrideMain,
             OutTopology->Topology.Output.NbChanAux,  OutTopology->Topology.Output.StrideAux,
             OutTopology->Topology.Output.NbChanDig,  OutTopology->Topology.Output.StrideDig,
             OutTopology->Topology.Output.NbChanHdmi, OutTopology->Topology.Output.StrideHdmi);
}

////////////////////////////////////////////////////////////////////////////
//
// UpdateOutputTopology
//
void Mixer_Mme_c::UpdateOutputTopology(MME_LxMixerOutTopology_t *OutTopology, int BypassPlayerId, int BypassChannelCount)
{
    switch (BypassPlayerId)
    {
    case 0:
        OutTopology->Topology.Output.NbChanMain = BypassChannelCount;
        OutTopology->Topology.Output.StrideMain = BypassChannelCount;
        break;

    case 1:
        OutTopology->Topology.Output.NbChanAux = BypassChannelCount;
        OutTopology->Topology.Output.StrideAux = BypassChannelCount;
        break;

    case 2:
        OutTopology->Topology.Output.NbChanDig = BypassChannelCount;
        OutTopology->Topology.Output.StrideDig = BypassChannelCount;
        break;

    case 3:
        OutTopology->Topology.Output.NbChanHdmi = BypassChannelCount;
        OutTopology->Topology.Output.StrideHdmi = BypassChannelCount;
        break;

    default:
        SE_DEBUG(group_mixer, "@: Card %d: is not valid\n", BypassPlayerId);
    }
}

////////////////////////////////////////////////////////////////////////////
///
/// Populate a pointer to the mixer's global parameters structure.
///
void Mixer_Mme_c::FillOutTransformerGlobalParameters(MME_LxMixerBDTransformerGlobalParams_Extended_t *GlobalParams)
{
    SE_DEBUG(group_mixer, ">%s\n", Name);
    uint8_t *ptr = reinterpret_cast<uint8_t *>(GlobalParams);
    int RegionType(PolicyValueRegionDVB);
    // Set the global reference for the actual mixer frequency and granule size.
    MixerSamplingFrequency = LookupMixerSamplingFrequency();
    MixerGranuleSize = LookupMixerGranuleSize(MixerSamplingFrequency);
    UpdateMixingMetadata();

    MixerSettings.MixerOptions.DebugDump();

    // Get PrimaryClient RegionType for later use
    if (Client[PrimaryClient].ManagedByThread())
    {
        Client[PrimaryClient].GetStream()->GetOption(PolicyRegionType, &RegionType);
    }

    if (MixerSamplingFrequencyHistory != MixerSamplingFrequency)
    {
        MixerSamplingFrequencyHistory = MixerSamplingFrequency;
        MixerSamplingFrequencyUpdated = true;
    }

    if (MixerGranuleSizeHistory != MixerGranuleSize)
    {
        MixerGranuleSizeHistory = MixerGranuleSize;
        MixerGranuleSizeUpdated = true;
    }

    memset(GlobalParams, 0, sizeof(*GlobalParams));
    GlobalParams->StructSize = sizeof(GlobalParams->StructSize);
    // Update the pointer corresponding to the StructSize variable of the global param structure
    ptr = &ptr[sizeof(GlobalParams->StructSize)];
    {
        //Fill out ACC_RENDERER_MIXER_ID parameters for mixer inputs
        MME_LxMixerInConfig_t InConfig; // C.FENARD: Should take care about size of this object in stack.
        InConfig.Id         = ACC_RENDERER_MIXER_ID;
        InConfig.StructSize = sizeof(InConfig);
        memset(&InConfig.Config, 0, sizeof(InConfig.Config));

        //Flag to signal that outputs are bypassed, used below for global structure update decision
        bool IsOutputBypassed = false;
        bool isFirstMixer = false;

        FillOutGlobalPcmInputMixerConfig(&InConfig, isFirstMixer);
        FillOutGlobalCodedInputMixerConfig(&InConfig, &IsOutputBypassed);
        FillOutGlobalInteractiveInputMixerConfig(&InConfig);
        FillOutGlobalGeneratorInputMixerConfig(&InConfig);

        if (ClientAudioParamsUpdated || MixerSamplingFrequencyUpdated || IsOutputBypassed
            || AudioGeneratorUpdated || IAudioUpdated)
        {
            memcpy(ptr, &InConfig, InConfig.StructSize);
            GlobalParams->StructSize += InConfig.StructSize;
            ptr = &ptr[InConfig.StructSize];
            // Reset the flags here after the global structure is populated.
            // IAudioUpdated flag is set to true due to ongoing
            // work on IAudio code so the structures depending on this are filled always at present.
            // Code will be updated once the work is done and to be covered in separate bug.
            ClientAudioParamsUpdated = false;
            AudioGeneratorUpdated = false;
        }
    }

    if (MixerSettings.MixerOptions.GainUpdated || MixerSettings.MixerOptions.InteractiveGainUpdated ||
        MixerSettings.MixerOptions.AudioGeneratorGainUpdated)
    {
        // Fill out ACC_RENDERER_MIXER_BD_GAIN_ID parameters
        MME_LxMixerGainSet_t *InGainConfig = reinterpret_cast<MME_LxMixerGainSet_t *>(ptr);
        InGainConfig->Id = ACC_RENDERER_MIXER_BD_GAIN_ID;
        InGainConfig->StructSize = sizeof(*InGainConfig);
        memset(&InGainConfig->GainSet[0], 0, sizeof(InGainConfig->GainSet));

        FillOutGlobalInputGainConfig(InGainConfig);

        GlobalParams->StructSize += InGainConfig->StructSize;
        ptr = &ptr[InGainConfig->StructSize];
        // Reset the Gain flag here. InteractiveGain and AudioGeneratorGain flags are reset below
        // as other structures are also dependent on this flag.
        MixerSettings.MixerOptions.GainUpdated = false;
    }

    if (MixerSettings.MixerOptions.PanUpdated)
    {
        // Fill out ACC_RENDERER_MIXER_BD_PANNING_ID parameters
        MME_LxMixerPanningSet_t *InPanningConfig = reinterpret_cast<MME_LxMixerPanningSet_t *>(ptr);
        InPanningConfig->Id = ACC_RENDERER_MIXER_BD_PANNING_ID;
        InPanningConfig->StructSize = sizeof(*InPanningConfig);
        memset(&InPanningConfig->PanningSet[0], 0, sizeof(InPanningConfig->PanningSet));

        FillOutGlobalInputPanningConfig(InPanningConfig);

        GlobalParams->StructSize += InPanningConfig->StructSize;
        ptr = &ptr[InPanningConfig->StructSize];
        //Reset the flag
        MixerSettings.MixerOptions.PanUpdated = false;
    }


    if (MixerSettings.MixerOptions.InteractiveGainUpdated || MixerSettings.MixerOptions.InteractivePanUpdated || IAudioUpdated)
    {
        // Fill out ACC_RENDERER_MIXER_BD_IAUDIO_ID parameters
        MME_LxMixerInIAudioConfig_t *InIaudioConfig = reinterpret_cast<MME_LxMixerInIAudioConfig_t *>(ptr);
        InIaudioConfig->Id = ACC_RENDERER_MIXER_BD_IAUDIO_ID;
        InIaudioConfig->StructSize = sizeof(*InIaudioConfig);
        InIaudioConfig->NbInteractiveAudioInput = MAX_INTERACTIVE_AUDIO;
        memset(&InIaudioConfig->ConfigIAudio[0], 0, sizeof(InIaudioConfig->ConfigIAudio));

        FillOutGlobalInteractiveAudioConfig(InIaudioConfig);

        GlobalParams->StructSize += InIaudioConfig->StructSize;
        ptr = &ptr[InIaudioConfig->StructSize];
        //Reset the flags
        MixerSettings.MixerOptions.InteractivePanUpdated = false;
    }

    // InteractiveGainUpdated is reset here after all the structures
    // dependent on this flag have been filled up
    MixerSettings.MixerOptions.InteractiveGainUpdated = false;
    MixerSettings.MixerOptions.AudioGeneratorGainUpdated = false;

    if (MixerSettings.MixerOptions.PostMixGainUpdated)
    {
        // Fill Out ACC_RENDERER_MIXER_BD_GENERAL_ID parameters
        MME_LxMixerBDGeneral_t &InBDGenConfig = *(reinterpret_cast<MME_LxMixerBDGeneral_t *>(ptr));
        InBDGenConfig.Id = ACC_RENDERER_MIXER_BD_GENERAL_ID;
        InBDGenConfig.StructSize = sizeof(InBDGenConfig);
        InBDGenConfig.PostMixGain = MixerSettings.MixerOptions.PostMixGain;
        InBDGenConfig.GainSmoothEnable = ACC_MME_TRUE;

        if (mApplicationType == PolicyValueAudioApplicationMS12)
        {
            InBDGenConfig.OutputLimiter       = DOLBY_LIMITER;
            InBDGenConfig.ChannelMatchingMode = CHANNEL_MATCHING_LENIENT;
        }
        else
        {
            InBDGenConfig.OutputLimiter       = LIMITER_DISABLED;
            InBDGenConfig.ChannelMatchingMode = CHANNEL_MATCHING_LEGACY;
        }

        GlobalParams->StructSize += InBDGenConfig.StructSize;
        ptr = &ptr[InBDGenConfig.StructSize];
        //Reset the flags
        MixerSettings.MixerOptions.PostMixGainUpdated = false;
    }

    if ((MixerSamplingFrequencyUpdated) || (MixerGranuleSizeUpdated))
    {
        //Fill out ACC_RENDERER_MIXER_OUTPUT_CONFIG_ID parameters
        MME_LxMixerOutConfig_t &OutConfig = *(reinterpret_cast<MME_LxMixerOutConfig_t *>(ptr));
        OutConfig.Id = ACC_RENDERER_MIXER_OUTPUT_CONFIG_ID;
        OutConfig.StructSize = sizeof(OutConfig);
        OutConfig.NbOutputSamplesPerTransform = MixerGranuleSize;
        OutConfig.MainFS = StmSeTranslateIntegerSamplingFrequencyToDiscrete(MixerSamplingFrequency);
        GlobalParams->StructSize += OutConfig.StructSize;
        ptr = &ptr[OutConfig.StructSize];
    }

    MixerSamplingFrequencyUpdated = false;
    MixerGranuleSizeUpdated = false;

    bool UpdateOutputTopologyForPCMBypass = false;
    int  BypassPlarerId = 0;
    int  BypassChannelCount = 0;

    for (uint32_t CountOfAudioPlayerAttached(0), PlayerIdx(0); CountOfAudioPlayerAttached < NumberOfAudioPlayerAttached; PlayerIdx++)
    {
        if (true == MixerPlayer[PlayerIdx].IsPlayerObjectAttached())
        {
            CountOfAudioPlayerAttached++;

            if (true == MixerPlayer[PlayerIdx].HasPcmPlayer())
            {
                if (Client[PrimaryClient].ManagedByThread() && (true == MixerPlayer[PlayerIdx].IsBypass()))
                {
                    // This client is correctly stated, and its parameters are well known.
                    SpdifInProperties_t PrimaryClientSpdifInProperties = Client[PrimaryClient].GetSpdifInProperties();
                    AudioOriginalEncoding_t OriginalEncoding = Client[PrimaryClient].GetOriginalEncodingParameter();
                    if (((OriginalEncoding == AudioOriginalEncodingSPDIFIn_Compressed) || (OriginalEncoding == AudioOriginalEncodingSPDIFIn_Pcm))
                        && (PrimaryClientSpdifInProperties.ChannelCount != MixerPlayer[PlayerIdx].GetCardNumberOfChannels()))
                    {
                        UpdateOutputTopologyForPCMBypass = true;
                        BypassPlarerId = PlayerIdx;
                        BypassChannelCount = PrimaryClientSpdifInProperties.ChannelCount;
                        SE_DEBUG(group_mixer, "@: Card %d: %s is bypassed InputChannelCount %d CardChannelCount %d\n",
                                 PlayerIdx, MixerPlayer[PlayerIdx].GetCardName(), BypassChannelCount,
                                 MixerPlayer[PlayerIdx].GetCardNumberOfChannels());
                    }
                }
            }
        }
    }

    if ((OutputTopologyUpdated) || (UpdateOutputTopologyForPCMBypass))
    {
        //Fill out ACC_RENDERER_MIXER_OUTPUT_TOPOLOGY_ID parameters
        MME_LxMixerOutTopology_t *OutTopology = reinterpret_cast<MME_LxMixerOutTopology_t *>(ptr);
        OutTopology->Id         = ACC_RENDERER_MIXER_OUTPUT_TOPOLOGY_ID;
        OutTopology->StructSize = sizeof(*OutTopology);
        FillOutGlobalOutputTopology(OutTopology);
        if (UpdateOutputTopologyForPCMBypass)
        {
            UpdateOutputTopology(OutTopology, BypassPlarerId, BypassChannelCount);
        }

        GlobalParams->StructSize += OutTopology->StructSize;
        ptr = &ptr[OutTopology->StructSize];
        OutputTopologyUpdated = false;
    }

    {
        uint32_t HeaderSize;
        // Point the output pcm parameters.
        MME_LxPcmPostProcessingGlobalParameters_Frozen_t *GlobalOutputPcmParams = reinterpret_cast<MME_LxPcmPostProcessingGlobalParameters_Frozen_t *>(ptr);
        // Calculate the header size and pointer to the first parameter structure
        HeaderSize = offsetof(MME_LxPcmPostProcessingGlobalParameters_Frozen_t, AuxSplit) + sizeof(MME_LxPcmPostProcessingGlobalParameters_Frozen_t::AuxSplit);
        SE_DEBUG(group_mixer, "@%s HeaderSize: %d\n", Name, HeaderSize);
        // Populate the header for the pcm parameters to fill according to the outputs in presence.
        GlobalOutputPcmParams->Id = ACC_RENDERER_MIXER_POSTPROCESSING_ID;
        GlobalOutputPcmParams->StructSize = HeaderSize;
        GlobalOutputPcmParams->NbPcmProcessings = 0; // no longer used
        GlobalOutputPcmParams->AuxSplit = SPLIT_AUX; // split automatically
        GlobalParams->StructSize += HeaderSize;
        ptr = &ptr[HeaderSize];
        // Fill in the common chain PCM Parameters
        {
            uint32_t SizeOfParametersInBytes = 0;
            FillCommonChainPcmParameters(ptr, SizeOfParametersInBytes);

            if (mTopology.type == STM_SE_MIXER_SINGLE_STAGE_MIXING)
            {
                FillCommonTuningParameters(ptr, &SizeOfParametersInBytes);
            }

            GlobalOutputPcmParams->StructSize += SizeOfParametersInBytes;
            GlobalParams->StructSize += SizeOfParametersInBytes;
            ptr = &ptr[SizeOfParametersInBytes];
        }
        for (uint32_t CountOfAudioPlayerAttached(0), PlayerIdx(0); PlayerIdx < STM_SE_MIXER_NB_MAX_OUTPUTS; PlayerIdx++)
        {
            bool NeedDeviceParamCopy = false;

            if (true == MixerPlayer[PlayerIdx].IsPlayerObjectAttached())
            {
                CountOfAudioPlayerAttached++;
                MixerPlayer[PlayerIdx].DebugDump();
                // Compute each output corresponding to the player.
                FillOutDevicePcmParameters(CurrentOutputPcmParams, PlayerIdx, mApplicationType);
                NeedDeviceParamCopy = true;
            }
            else if (mPlayerToReset[PlayerIdx] == true)
            {
                mPlayerToReset[PlayerIdx] = false;
                PcmProc::ResetDevicePcmParameters(&CurrentOutputPcmParams, PlayerIdx);
                NeedDeviceParamCopy = true;
            }

            if (NeedDeviceParamCopy)
            {
                uint8_t *OriP = reinterpret_cast<uint8_t *>(&CurrentOutputPcmParams);
                // Point just after header of the given CurrentOutputPcmParams element.
                OriP = &OriP[HeaderSize];
                // Compute corresponding size of data that follow this header of this CurrentOutputPcmParams element.
                uint32_t SizeOfParametersInBytes = CurrentOutputPcmParams.StructSize - HeaderSize;
                SE_DEBUG(group_mixer, "@%s SizeOfParametersInBytes: %d\n", Name, SizeOfParametersInBytes);
                memcpy(ptr, OriP, SizeOfParametersInBytes);
                GlobalOutputPcmParams->StructSize += SizeOfParametersInBytes;
                GlobalParams->StructSize += SizeOfParametersInBytes;
                ptr = &ptr[SizeOfParametersInBytes];
            }
        }

        PcmProc::DumpMixerPostProcStruct(GlobalOutputPcmParams, HeaderSize);
    }

    SE_DEBUG(group_mixer, "@: InConfig.Config[MIXER_CODED_DATA_INPUT+0] InputId: 0x%08x Config: 0x%08x SamplingFreq: %s\n",
             GlobalParams->InConfig.Config[MIXER_CODED_DATA_INPUT].InputId,
             GlobalParams->InConfig.Config[MIXER_CODED_DATA_INPUT].Config,
             LookupDiscreteSamplingFrequency(GlobalParams->InConfig.Config[MIXER_CODED_DATA_INPUT].SamplingFreq));
    SE_DEBUG(group_mixer, "@: InConfig.Config[MIXER_CODED_DATA_INPUT+1] InputId: 0x%08x Config: 0x%08x SamplingFreq: %s\n",
             GlobalParams->InConfig.Config[MIXER_CODED_DATA_INPUT + 1].InputId,
             GlobalParams->InConfig.Config[MIXER_CODED_DATA_INPUT + 1].Config,
             LookupDiscreteSamplingFrequency(GlobalParams->InConfig.Config[MIXER_CODED_DATA_INPUT + 1].SamplingFreq));
    SE_DEBUG(group_mixer, "@: PcmParams[0].FatPipeOrSpdifOut.Config.Layout: 0x%08x\n",
             GlobalParams->PcmParams[0].FatPipeOrSpdifOut.Config.Layout);
    SE_DEBUG(group_mixer, "@: PcmParams[1].FatPipeOrSpdifOut.Config.Layout: 0x%08x\n",
             GlobalParams->PcmParams[1].FatPipeOrSpdifOut.Config.Layout);
    SE_DEBUG(group_mixer, "@: PcmParams[2].FatPipeOrSpdifOut.Config.Layout: 0x%08x\n",
             GlobalParams->PcmParams[2].FatPipeOrSpdifOut.Config.Layout);
    SE_DEBUG(group_mixer, "@: PcmParams[3].FatPipeOrSpdifOut.Config.Layout: 0x%08x\n",
             GlobalParams->PcmParams[3].FatPipeOrSpdifOut.Config.Layout);
    SE_DEBUG(group_mixer, "<\n");
}

////////////////////////////////////////////////////////////////////////////
///
/// Populate a pointer to the mixer's global parameters structure.
///
void Mixer_Mme_c::FillOutFirstMixerGlobalParameters(MME_LxMixerBDTransformerGlobalParams_Extended_t *GlobalParams)
{
    SE_DEBUG(group_mixer, ">%s\n", Name);
    uint8_t *ptr = reinterpret_cast<uint8_t *>(GlobalParams);

    // Set the global reference for the actual mixer frequency and granule size.
    MixerSamplingFrequency = LookupMixerSamplingFrequency();
    MixerGranuleSize = LookupMixerGranuleSize(MixerSamplingFrequency);
    UpdateMixingMetadata();

    if (FirstMixerSamplingFrequencyHistory != MixerSamplingFrequency)
    {
        FirstMixerSamplingFrequencyHistory = MixerSamplingFrequency;
        FirstMixerSamplingFrequencyUpdated = true;
    }

    if (FirstMixerGranuleSizeHistory != MixerGranuleSize)
    {
        FirstMixerGranuleSizeHistory = MixerGranuleSize;
        FirstMixerGranuleSizeUpdated = true;
    }

    memset(GlobalParams, 0, sizeof(*GlobalParams));
    GlobalParams->StructSize = sizeof(GlobalParams->StructSize);
    // Update the pointer corresponding to the StructSize variable of the global param structure
    ptr = &ptr[sizeof(GlobalParams->StructSize)];
    // Fill out parameters for inputs of mixer.
    {
        MME_LxMixerInConfig_t InConfig; // C.FENARD: Should take care about size of this object in stack.
        InConfig.Id         = ACC_RENDERER_MIXER_ID;
        InConfig.StructSize = sizeof(InConfig);
        memset(&InConfig.Config, 0, sizeof(InConfig.Config));
        bool isFirstMixer = true;

        FillOutGlobalPcmInputMixerConfig(&InConfig, isFirstMixer);

        //Check if Inconfig Update is required if either of the flags are set
        if (FirstMixerClientAudioParamsUpdated || FirstMixerSamplingFrequencyUpdated)
        {
            memcpy(ptr, &InConfig, InConfig.StructSize);
            GlobalParams->StructSize += InConfig.StructSize;
            ptr = &ptr[InConfig.StructSize];
            // Reset the flags here after the global structure is populated.
            FirstMixerClientAudioParamsUpdated = false;
        }
    }

    if (MixerSettings.MixerOptions.GainUpdated)
    {
        MME_LxMixerGainSet_t &InGainConfig = *(reinterpret_cast<MME_LxMixerGainSet_t *>(ptr));
        InGainConfig.Id = ACC_RENDERER_MIXER_BD_GAIN_ID;
        InGainConfig.StructSize = sizeof(InGainConfig);
        memset(&InGainConfig.GainSet[0], 0, sizeof(InGainConfig.GainSet));

        for (uint32_t ClientIdx(0); ClientIdx < ACC_MIXER_MAX_NB_INPUT; ClientIdx++)
        {
            InGainConfig.GainSet[ClientIdx].InputId =   ClientIdx;

            for (uint32_t ChannelIdx(0); ChannelIdx < SND_PSEUDO_MIXER_CHANNELS; ChannelIdx++)
            {
                if (ClientIdx < MIXER_MAX_CLIENTS)
                {
                    InGainConfig.GainSet[ClientIdx].AlphaPerChannel[ChannelIdx] =
                        MixerSettings.MixerOptions.Gain[ClientIdx][ChannelIdx];
                }
                else
                {
                    InGainConfig.GainSet[ClientIdx].AlphaPerChannel[ChannelIdx] =    Q3_13_UNITY;
                }
            }
        }

        GlobalParams->StructSize += InGainConfig.StructSize;
        ptr = &ptr[InGainConfig.StructSize];
        MixerSettings.MixerOptions.GainUpdated = false;
    }

    if (MixerSettings.MixerOptions.PanUpdated)
    {
        // Fill out ACC_RENDERER_MIXER_BD_PANNING_ID parameters
        MME_LxMixerPanningSet_t *InPanningConfig = reinterpret_cast<MME_LxMixerPanningSet_t *>(ptr);
        InPanningConfig->Id = ACC_RENDERER_MIXER_BD_PANNING_ID;
        InPanningConfig->StructSize = sizeof(*InPanningConfig);
        memset(&InPanningConfig->PanningSet[0], 0, sizeof(InPanningConfig->PanningSet));

        FillOutGlobalInputPanningConfig(InPanningConfig);

        GlobalParams->StructSize += InPanningConfig->StructSize;
        ptr = &ptr[InPanningConfig->StructSize];
        //Reset the flag
        MixerSettings.MixerOptions.PanUpdated = false;
    }

    if (MixerSettings.MixerOptions.PostMixGainUpdated)
    {
        MME_LxMixerBDGeneral_t &InBDGenConfig = *(reinterpret_cast<MME_LxMixerBDGeneral_t *>(ptr));
        InBDGenConfig.Id = ACC_RENDERER_MIXER_BD_GENERAL_ID;
        InBDGenConfig.StructSize = sizeof(InBDGenConfig);
        InBDGenConfig.PostMixGain = MixerSettings.MixerOptions.PostMixGain;
        InBDGenConfig.GainSmoothEnable = ACC_MME_TRUE;

        if (mApplicationType == PolicyValueAudioApplicationMS12)
        {
            InBDGenConfig.OutputLimiter       = DOLBY_LIMITER;
            InBDGenConfig.ChannelMatchingMode = CHANNEL_MATCHING_STRINGENT;
        }
        else
        {
            InBDGenConfig.OutputLimiter       = LIMITER_DISABLED;
            InBDGenConfig.ChannelMatchingMode = CHANNEL_MATCHING_LEGACY;
        }

        GlobalParams->StructSize += InBDGenConfig.StructSize;
        ptr = &ptr[InBDGenConfig.StructSize];
        //Reset the flags
        MixerSettings.MixerOptions.PostMixGainUpdated = false;
    }

    if ((FirstMixerSamplingFrequencyUpdated) || (FirstMixerGranuleSizeUpdated))
    {
        MME_LxMixerOutConfig_t &OutConfig = *(reinterpret_cast<MME_LxMixerOutConfig_t *>(ptr));
        OutConfig.Id = ACC_RENDERER_MIXER_OUTPUT_CONFIG_ID;
        OutConfig.StructSize = sizeof(OutConfig);
        OutConfig.NbOutputSamplesPerTransform = MixerGranuleSize;
        OutConfig.MainFS = StmSeTranslateIntegerSamplingFrequencyToDiscrete(MixerSamplingFrequency);
        GlobalParams->StructSize += OutConfig.StructSize;
        ptr = &ptr[OutConfig.StructSize];
    }

    FirstMixerSamplingFrequencyUpdated = false;
    FirstMixerGranuleSizeUpdated = false;

    MME_LxMixerOutTopology_t &OutTopology = *(reinterpret_cast<MME_LxMixerOutTopology_t *>(ptr));
    OutTopology.Id         = ACC_RENDERER_MIXER_OUTPUT_TOPOLOGY_ID;
    OutTopology.StructSize = sizeof(OutTopology);

    if (FirstActivePlayer != MIXER_MME_NO_ACTIVE_PLAYER)
    {
        OutTopology.Topology.Output.NbChanMain = 8;
        OutTopology.Topology.Output.StrideMain = 8;
    }
    else
    {
        OutTopology.Topology.NbChannels = 0;
    }

    GlobalParams->StructSize += OutTopology.StructSize;
    ptr = &ptr[OutTopology.StructSize];


    uint32_t HeaderSize;
    // Point the output pcm parameters.
    MME_LxPcmPostProcessingGlobalParameters_Frozen_t *GlobalOutputPcmParams =
        reinterpret_cast<MME_LxPcmPostProcessingGlobalParameters_Frozen_t *>(ptr);
    // Calculate the header size and pointer to the first parameter structure
    HeaderSize = offsetof(MME_LxPcmPostProcessingGlobalParameters_Frozen_t, AuxSplit)
                 + sizeof(MME_LxPcmPostProcessingGlobalParameters_Frozen_t::AuxSplit);

    SE_DEBUG(group_mixer, "@%s HeaderSize: %d\n", Name, HeaderSize);
    // Populate the header for the pcm parameters to fill according to the outputs in presence.
    GlobalOutputPcmParams->Id = ACC_RENDERER_MIXER_POSTPROCESSING_ID;
    GlobalOutputPcmParams->StructSize = HeaderSize;
    GlobalOutputPcmParams->NbPcmProcessings = 0; // no longer used
    GlobalOutputPcmParams->AuxSplit = SPLIT_AUX; // split automatically
    GlobalParams->StructSize += HeaderSize;
    ptr = &ptr[HeaderSize];

    unsigned int size = 0;

    FillCommonTuningParameters(ptr, &size);

    GlobalOutputPcmParams->StructSize += size;
    GlobalParams->StructSize += size;
    ptr = &ptr[size];

    PcmProc::DumpMixerPostProcStruct(GlobalOutputPcmParams, HeaderSize);

    SE_DEBUG(group_mixer, "<\n");
}

////////////////////////////////////////////////////////////////////////////
///
/// Determine the output sampling frequency to use for the specified PcmPlayer.
///
/// This method relies on LookupPostProcTargetSampleRate but takes care
/// of specific encodings that can requires specific PcmPlayer frequency setting.
///
/// \param in  OutputEncoding    Encoding requested for this PcmPlayer
/// \param in  MixerPlayerIndex  Index to retrieve MixerPlayer_c object from Mixer_Mme_c::MixerPlayer array
///
/// \return    Frequency to set on PcmPlayer (i.e. ALSA device)
///
uint32_t Mixer_Mme_c::LookupPcmPlayerOutputSamplingFrequency(PcmPlayer_c::OutputEncoding OutputEncoding,
                                                             uint32_t MixerPlayerIndex) const
{
    PcmPlayer_c::OutputEncoding RefineEncoding;
    uint32_t Freq = LookupPostProcTargetSampleRate(OutputEncoding, MixerPlayerIndex, &RefineEncoding);

    /* DDPLUS encoder requires ALSA device running at 4 times the PCM processing output frequency */
    if (RefineEncoding == PcmPlayer_c::OUTPUT_DDPLUS)
    {
        Freq *= 4;
    }

    SE_DEBUG(group_mixer, "<%s %d\n", Name, Freq);
    return Freq;
}

////////////////////////////////////////////////////////////////////////////
///
/// Determine the output sampling frequency to use for PCM Proc Output
/// Also determine OutputEncoding based on PcmPlayer configuration as
/// restrictions can be applied. For instance :
///  - Encoders do not support all SampleRates
///  - Bypass cannot be applied if Mixer Frequency is different from
///    primary output frequency
///
/// \param in  TargetEncoding    Encoding requested for this PcmPlayer
/// \param in  MixerPlayerIndex  Index to retrieve MixerPlayer_c object from Mixer_Mme_c::MixerPlayer array
/// \param out pEncodingSet      Pointer on Encoding selected after considering restrictions mentionned above.
///
/// \return    Frequency to set on PcmPlayer (i.e. ALSA device)
///
/// \todo Strictly speaking there is a cyclic dependency between
///       Mixer_Mme_c::LookupOutputSamplingFrequency() and
///       Mixer_Mme_c::LookupOutputEncoding(). If the output has to
///       fallback to LPCM there is no need to clamp the frequency
///       below 48khz.
///
uint32_t Mixer_Mme_c::LookupPostProcTargetSampleRate(PcmPlayer_c::OutputEncoding TargetEncoding,
                                                     uint32_t MixerPlayerIndex,
                                                     PcmPlayer_c::OutputEncoding *pEncodingSet) const
{
    SE_DEBUG(group_mixer, ">\n");
    SE_ASSERT(NominalOutputSamplingFrequency);
    SE_ASSERT(pEncodingSet != NULL);

    uint32_t Iec60958FrameRate = 0;
    if (PcmPlayer_c::IsOutputBypassed(TargetEncoding))
    {
        Iec60958FrameRate = LookupIec60958FrameRateForBypass(TargetEncoding);
    }

    uint32_t Freq = MixerPlayer[MixerPlayerIndex].LookupOutputSamplingFrequency(TargetEncoding,
                                                                                NominalOutputSamplingFrequency,
                                                                                Iec60958FrameRate);

    // we must send the sample rate here since we need the conditional disabling of the encoders...
    // So refine the output encoding with previously computed PcmProcTargetSampleRate.
    *pEncodingSet = MixerPlayer[MixerPlayerIndex].LookupOutputEncoding(PrimaryCodedDataType[MIXER_CODED_DATA_INPUT_SPDIF_IDX],
                                                                       PrimaryCodedDataType[MIXER_CODED_DATA_INPUT_HDMI_IDX],
                                                                       Freq);

    SE_DEBUG(group_mixer, "<%s %dHz %s\n", Name, Freq, PcmPlayer_c::LookupOutputEncoding(*pEncodingSet));
    return Freq;
}

////////////////////////////////////////////////////////////////////////////
///
/// Determine the output sampling frequency to use for PCM Proc Output
/// Also determine OutputEncoding based on PcmPlayer configuration as
/// restrictions can be applied. For instance :
///  - Encoders do not support all SampleRates
///  - Bypass cannot be applied if Mixer Frequency is different from
///    primary output frequency
///
/// \param in  MixerPlayerIndex  Index to retrieve MixerPlayer_c object from Mixer_Mme_c::MixerPlayer array
/// \param out pEncodingSet      Pointer on Encoding selected after considering restrictions mentionned above, valid pointer is mandatory
/// \param out pOutFreq          Pointer on frequency that will be used for this player (optional: Filled only if not NULL)
///
/// \return PlayerNoError on sucess, PlayerError otherwise
///
/// \todo Strictly speaking there is a cyclic dependency between
///       Mixer_Mme_c::LookupOutputSamplingFrequency() and
///       Mixer_Mme_c::LookupOutputEncoding(). If the output has to
///       fallback to LPCM there is no need to clamp the frequency
///       below 48khz.
///
PlayerStatus_t Mixer_Mme_c::LookupPlayerFinalOutputEncodingAndFreq(uint32_t MixerPlayerIndex,
                                                                   PcmPlayer_c::OutputEncoding *pEncodingSet,
                                                                   uint32_t *pOutFreq)
{
    if (MixerPlayerIndex >= STM_SE_MIXER_NB_MAX_OUTPUTS || pEncodingSet == NULL)
    {
        SE_ERROR("Invalid argument\n");
        return PlayerError;
    }

    PcmPlayer_c::OutputEncoding TargetEncoding =
        MixerPlayer[MixerPlayerIndex].LookupOutputEncoding(PrimaryCodedDataType[MIXER_CODED_DATA_INPUT_SPDIF_IDX],
                                                           PrimaryCodedDataType[MIXER_CODED_DATA_INPUT_HDMI_IDX]);

    if (pOutFreq != NULL)
    {
        *pOutFreq = LookupPostProcTargetSampleRate(TargetEncoding, MixerPlayerIndex, pEncodingSet);
    }
    else
    {
        LookupPostProcTargetSampleRate(TargetEncoding, MixerPlayerIndex, pEncodingSet);
    }

    return PlayerNoError;
}


////////////////////////////////////////////////////////////////////////////
///
/// Configure the downmix processing appropriately.
///
/// Contract: The PcmParams structure is zeroed before population. At the time
/// it is handed to this method the CMC settings have already been stored and
/// the downmix settings are untouched (still zero).
///
/// This method is, semantically, part of FillOutDevicePcmParameters() (hence
/// it is inlined) but is quite complex enough to justify splitting it out into
/// a separate method.
///
inline void Mixer_Mme_c::FillOutDeviceDownmixParameters(MME_LxPcmPostProcessingGlobalParameters_Frozen_t &PcmParams,
                                                        uint32_t MixerPlayerIndex,
                                                        bool EnableDMix)
{
    bool CodecDownmixTable = false;
    int DmixTableNo;
    ParsedAudioParameters_t PrimaryClientAudioParameters;
    // put these are the top because their values are very transient - don't expect them to maintain their
    // values between blocks...

    // Initialize the donwmixer structure before population
    // PcmParams already initialized to null.
    MME_DMixGlobalParams_t &DMix   = PcmParams.Dmix;
    DMix.Id                 = PCMPROCESS_SET_ID(ACC_PCM_DMIX_ID, MixerPlayerIndex);
    DMix.Apply              = (EnableDMix ? ACC_MME_ENABLED : ACC_MME_DISABLED);
    DMix.StructSize         = sizeof(DMix);
    DMix.Config[DMIX_USER_DEFINED]   = ACC_MME_FALSE;
    DMix.Config[DMIX_STEREO_UPMIX]   = ACC_MME_FALSE;
    DMix.Config[DMIX_MONO_UPMIX]     = ACC_MME_FALSE;
    DMix.Config[DMIX_MEAN_SURROUND]  = ACC_MME_FALSE;
    DMix.Config[DMIX_SECOND_STEREO]  = ACC_MME_FALSE;
    DMix.Config[DMIX_MIX_LFE]        = ACC_MME_FALSE;
    DMix.Config[DMIX_NORMALIZE]      = DMIX_NORMALIZE_ON;
    DMix.Config[DMIX_NORM_IDX]       = 0;
    DMix.Config[DMIX_DIALOG_ENHANCE] = ACC_MME_FALSE;
    PcmParams.StructSize += sizeof(DMix);

    // if the downmixer isn't enabled we're done

    if (!EnableDMix)
    {
        return;
    }

    // get output and input audio modes
    enum eAccAcMode OutputMode  = (enum eAccAcMode) PcmParams.CMC.OutMode;
    enum eAccAcMode InputMode   = LookupMixerAudioMode();
    SE_DEBUG(group_mixer, "[Player %d] Input mode (or mixer mode)=%s  OutputMode=%s\n", MixerPlayerIndex, LookupAudioMode(InputMode), LookupAudioMode(OutputMode));

    // Needed by Downmix API to check whether to apply external Downmix table
    DMix.Config[DMIX_TABLE_ACMOD] = InputMode;

    // Client Lock already taken by caller.
    if (Client[PrimaryClient].ManagedByThread())
    {
        // This client is correctly stated, and its parameters are well known.
        Client[PrimaryClient].GetParameters(&PrimaryClientAudioParameters);

        /*Check if we have some downmix table return from the codec
          and its outmode/input mode matches with the user out mode / mixer inmode then use that table for downmixing*/
        for (DmixTableNo = 0; DmixTableNo < MAX_SUPPORTED_DMIX_TABLE; DmixTableNo++)
        {
            if ((PrimaryClientAudioParameters.DownMixTable[DmixTableNo].IsTablePresent)
                && (PrimaryClientAudioParameters.DownMixTable[DmixTableNo].OutMode == OutputMode)
                && (PrimaryClientAudioParameters.DownMixTable[DmixTableNo].InMode == InputMode)) // Downmix Table is present and also input and output mode is matching use this table for downmixing
            {
                CodecDownmixTable = true;
                break;
            }
        }
    }

    if (CodecDownmixTable)
    {
        int nch_out = PrimaryClientAudioParameters.DownMixTable[DmixTableNo].NChOut;
        int nch_in  = PrimaryClientAudioParameters.DownMixTable[DmixTableNo].NChIn;
        int i, j;
        DMix.Config[DMIX_USER_DEFINED] = ACC_MME_TRUE;

        for (i = 0; i < nch_out; i++)
        {
            for (j = 0; j < nch_in; j++)
            {
                DMix.MainMixTable[i][j] = PrimaryClientAudioParameters.DownMixTable[DmixTableNo].DownMixTableCoeff[i][j];
            }
        }

        return; // We have configure the downmixing
    }

    /* STEREO INPUT AND ALL SPEAKER ENABLED */

    if ((InputMode == ACC_MODE20) && (MixerSettings.OutputConfiguration.all_speaker_stereo_enable))
    {
        DMix.Config[DMIX_USER_DEFINED] = ACC_MME_TRUE;
        DMix.MainMixTable[0][0] = ACC_UNITY;
        DMix.MainMixTable[1][1] = ACC_UNITY;
        DMix.MainMixTable[2][0] = ACC_UNITY / 2;
        DMix.MainMixTable[2][1] = ACC_UNITY / 2;
        DMix.MainMixTable[4][0] = ACC_UNITY;
        DMix.MainMixTable[5][1] = ACC_UNITY;

        // one single rear central channel

        if ((OutputMode == ACC_MODE21) || (OutputMode == ACC_MODE31) ||
            (OutputMode == ACC_MODE21_LFE) || (OutputMode == ACC_MODE31_LFE))
        {
            DMix.MainMixTable[4][0] = ACC_UNITY / 2;
            DMix.MainMixTable[4][1] = ACC_UNITY / 2;
        }

        SE_DEBUG(group_mixer, "Used custom UpMix table for %s to %s\n",
                 LookupAudioMode(InputMode), LookupAudioMode(OutputMode));

        for (uint32_t i = 0; i < DMIX_NB_IN_CHANNELS; i++)
            SE_DEBUG(group_mixer, "%04x %04x %04x %04x %04x %04x %04x %04x\n",
                     DMix.MainMixTable[i][0], DMix.MainMixTable[i][1],
                     DMix.MainMixTable[i][2], DMix.MainMixTable[i][3],
                     DMix.MainMixTable[i][4], DMix.MainMixTable[i][5],
                     DMix.MainMixTable[i][6], DMix.MainMixTable[i][7]);

        return;
    }
}



////////////////////////////////////////////////////////////////////////////
///
/// Configure the SpdifOut processing appropriately.
///
/// Contract: The PcmParams structure is zeroed before population. At the time
/// it is handed to this method it may already have been partially filled in.
///
/// This method is, semantically, part of FillOutDevicePcmParameters() (hence
/// it is inlined) but is quite complex enough to justify splitting it out into
/// a separate method.
///
/// The primary responsibility of the function is to set dynamically changing
/// fields (such as the sample rate), static fields are provided by userspace.
/// The general strategy taken to merge the two sources is that is that where
/// fields are supplied by the implementation (e.g. sample rate,
/// LPCM/other, emphasis) we ignore the values provided by userspace. Otherwise
/// we make every effort to reflect the userspace values, we make no effort
/// to validate their correctness.
///
inline void Mixer_Mme_c::FillOutDeviceSpdifParameters(MME_LxPcmPostProcessingGlobalParameters_Frozen_t &PcmParams,
                                                      uint32_t MixerPlayerIndex,
                                                      PcmPlayer_c::OutputEncoding OutputEncoding)
{
    SE_DEBUG(group_mixer, "> Card %d: %s is %s\n",
             MixerPlayerIndex,
             MixerPlayer[MixerPlayerIndex].GetCardName(),
             PcmPlayer_c::LookupOutputEncoding(OutputEncoding));

    AudioOriginalEncoding_t OriginalEncoding(AudioOriginalEncodingUnknown);
    // Client Lock already taken by caller.
    if (Client[MasterClientIndex].ManagedByThread())
    {
        // This client is correctly stated, and its parameters are well known.
        OriginalEncoding = Client[MasterClientIndex].GetOriginalEncodingParameter();
    }

    if (OutputEncoding == PcmPlayer_c::OUTPUT_FATPIPE)
    {
        SE_ERROR("OUTPUT_FATPIPE not supported\n");
        return;
    }

    MMESetSpdifConfig(PcmParams, MixerPlayerIndex, OutputEncoding, OriginalEncoding);


    PlayerStatus_t err;

    // Client Lock already taken by caller.
    if (Client[PrimaryClient].ManagedByThread())
    {
        // This client is correctly started, and its parameters are well known.
        SpdifInProperties_t SpdifInProperties = Client[PrimaryClient].GetSpdifInProperties();
        err = MixerPlayer[MixerPlayerIndex].CheckAndConfigureHDMICell(OutputEncoding, &SpdifInProperties);
    }
    else
    {
        err = MixerPlayer[MixerPlayerIndex].CheckAndConfigureHDMICell(OutputEncoding, NULL);
    }

    if (err != PlayerNoError)
    {
        SE_ERROR("failed configuring the hdmi cell\n");
    }
}

inline void Mixer_Mme_c::MMESetSpdifConfig(MME_LxPcmPostProcessingGlobalParameters_Frozen_t &PcmParams,
                                           uint32_t MixerPlayerIndex,
                                           PcmPlayer_c::OutputEncoding OutputEncoding,
                                           AudioOriginalEncoding_t OriginalEncoding)
{
    //
    // Handle, in a unified manner, all the IEC SPDIF formatings
    //
    MME_SpdifOutGlobalParams_t &SpdifOut = *(reinterpret_cast<MME_SpdifOutGlobalParams_t *>(&PcmParams.FatPipeOrSpdifOut));
    SpdifOut.Id = PCMPROCESS_SET_ID(ACC_PCM_SPDIFOUT_ID, MixerPlayerIndex);
    SpdifOut.StructSize = sizeof(MME_FatpipeGlobalParams_t);   // use fatpipe size (to match frozen structure)
    PcmParams.StructSize += sizeof(MME_FatpipeGlobalParams_t);   // use fatpipe size
    int layout = 0;

    // Set Layout
    if ((MixerPlayer[MixerPlayerIndex].GetCardNumberOfChannels() > 2) &&
        (MixerPlayer[MixerPlayerIndex].IsConnectedToHdmi()) &&
        (PcmParams.CMC.OutMode > ACC_MODE20))
    {
        layout = 1;
    }

    SpdifOut.Config.Layout = 0;

    if ((OutputEncoding == PcmPlayer_c::OUTPUT_IEC60958) || (OutputEncoding == PcmPlayer_c::BYPASS_DTS_CDDA))
    {
        SpdifOut.Apply = ACC_MME_ENABLED;
        SpdifOut.Config.Mode = 0; // SPDIF mode
        SpdifOut.Config.UpdateSpdifControl = 1;
        SpdifOut.Config.SpdifCompressed = 0;
        SpdifOut.Config.AddIECPreamble = 0;
        SpdifOut.Config.ForcePC = 0; // useless in this case: for compressed mode only
        SpdifOut.Spdifout.ChannelStatus[0] = 0x00000000; /* consumer, linear PCM, mode 00 */

        if (OutputEncoding == PcmPlayer_c::OUTPUT_IEC60958)
        {
            SpdifOut.Spdifout.ChannelStatus[1] = 0x0b000000;    /* 24-bit word length */
        }
        else
        {
            SpdifOut.Spdifout.ChannelStatus[1] = 0x00000000;    /* word length not indicated */
        }

        if (OutputEncoding == PcmPlayer_c::OUTPUT_IEC60958)
        {
            SpdifOut.Config.Layout = layout;
        }
    }
    else if ((OutputEncoding == PcmPlayer_c::OUTPUT_AC3) ||
             (OutputEncoding == PcmPlayer_c::OUTPUT_DDPLUS) ||
             (OutputEncoding == PcmPlayer_c::BYPASS_AC3) ||
             (OutputEncoding == PcmPlayer_c::BYPASS_DDPLUS) ||
             (OutputEncoding == PcmPlayer_c::BYPASS_AAC))
    {
        SpdifOut.Apply = ACC_MME_ENABLED;
        SpdifOut.Config.Mode = 0; // SPDIF mode
        SpdifOut.Config.UpdateSpdifControl = 1;
        SpdifOut.Config.UpdateMetaData = 1; // this is supposed to be FatPipe only but the examples set it...
        SpdifOut.Config.SpdifCompressed = 1;
        SpdifOut.Config.AddIECPreamble = ((OutputEncoding == PcmPlayer_c::OUTPUT_AC3) || (OutputEncoding == PcmPlayer_c::OUTPUT_DDPLUS)) ? 0 : 1;
        SpdifOut.Config.ForcePC = 1; // compressed mode: firmware will get the stream type from the to Preamble_PC below

        if ((OutputEncoding == PcmPlayer_c::OUTPUT_AC3) ||
            (OutputEncoding == PcmPlayer_c::OUTPUT_DDPLUS) ||
            ((OutputEncoding == PcmPlayer_c::BYPASS_AC3) &&
             ((OriginalEncoding == AudioOriginalEncodingDdplus) || (OriginalEncoding == AudioOriginalEncodingDPulse))))
        {
            // DD and DD+ encoders and DD+toDD converter all issue little endian samples
            SpdifOut.Config.Endianness = 1; // little endian input frames
            SE_DEBUG(group_mixer, "@ %s Endianness changed for SpdifOut\n", PcmPlayer_c::LookupOutputEncoding(OutputEncoding));
        }

        memset(SpdifOut.Spdifout.Validity, 0xff, sizeof(SpdifOut.Spdifout.Validity));
        SpdifOut.Spdifout.ChannelStatus[0] = 0x02000000; /* consumer, not linear PCM, mode 00 */
        SpdifOut.Spdifout.ChannelStatus[1] = 0x02000000; /* 16-bit word length */
        SpdifOut.Spdifout.Preamble_PA = 0xf872;
        SpdifOut.Spdifout.Preamble_PB = 0x4e1f;
        SpdifOut.Spdifout.Preamble_PC = LookupSpdifPreamblePc(OutputEncoding);
    }
    else if ((OutputEncoding == PcmPlayer_c::OUTPUT_DTS) ||
             (OutputEncoding == PcmPlayer_c::BYPASS_DTS_512) ||
             (OutputEncoding == PcmPlayer_c::BYPASS_DTS_1024) ||
             (OutputEncoding == PcmPlayer_c::BYPASS_DTS_2048) ||
             ((OutputEncoding >= PcmPlayer_c::BYPASS_DTSHD_LBR) && (OutputEncoding <= PcmPlayer_c::BYPASS_DTSHD_DTS_8192)) ||
             (OutputEncoding == PcmPlayer_c::BYPASS_DTSHD_MA))
    {
        SpdifOut.Apply = ACC_MME_ENABLED;
        SpdifOut.Config.Mode = 0; // SPDIF mode
        SpdifOut.Config.UpdateSpdifControl = 1;
        SpdifOut.Config.UpdateMetaData = 1; // this is supposed to be FatPipe only but the examples set it...
        // in case of DTS encoding, the preambles are set by the encoder itself
        SpdifOut.Config.SpdifCompressed = 1;
        SpdifOut.Config.AddIECPreamble = (OutputEncoding == PcmPlayer_c::OUTPUT_DTS) ? 0 : 1;
        SpdifOut.Config.ForcePC = 1; // compressed mode: firmware will get the stream type from the to Preamble_PC below

        if (OutputEncoding == PcmPlayer_c::OUTPUT_DTS)
        {
            SpdifOut.Config.Endianness = 1; // little endian input frames
        }

        memset(SpdifOut.Spdifout.Validity, 0xff, sizeof(SpdifOut.Spdifout.Validity));
        SpdifOut.Spdifout.ChannelStatus[0] = 0x02000000; /* consumer, not linear PCM, mode 00 */
        SpdifOut.Spdifout.ChannelStatus[1] = 0x02000000; /* 16-bit word length */
        SpdifOut.Spdifout.Preamble_PA = 0xf872;
        SpdifOut.Spdifout.Preamble_PB = 0x4e1f;
        SpdifOut.Spdifout.Preamble_PC = LookupSpdifPreamblePc(OutputEncoding);
    }
    else if (OutputEncoding == PcmPlayer_c::BYPASS_TRUEHD)
    {
        /// TODO not supported yet by the firmware...
        SE_INFO(group_mixer, "TrueHD bypassing is not supported yet by the firmware\n");
        SpdifOut.Apply = ACC_MME_ENABLED;
        SpdifOut.Config.Mode = 0; // SPDIF mode
        SpdifOut.Config.UpdateSpdifControl = 1;
        SpdifOut.Config.UpdateMetaData = 1; // this is supposed to be FatPipe only but the examples set it...
        SpdifOut.Config.SpdifCompressed = 1;
        SpdifOut.Config.AddIECPreamble = 1;
        SpdifOut.Config.ForcePC = 1; // compressed mode: firmware will get the stream type from the to Preamble_PC below
        //
        SpdifOut.Config.Endianness = 1; // little endian input frames: check with the MAT engine
        memset(SpdifOut.Spdifout.Validity, 0xff, sizeof(SpdifOut.Spdifout.Validity));
        SpdifOut.Spdifout.ChannelStatus[0] = 0x02000000; /* consumer, not linear PCM, mode 00 */
        SpdifOut.Spdifout.ChannelStatus[1] = 0x02000000; /* 16-bit word length */
        SpdifOut.Spdifout.Preamble_PA = 0xf872;
        SpdifOut.Spdifout.Preamble_PB = 0x4e1f;
        SpdifOut.Spdifout.Preamble_PC = LookupSpdifPreamblePc(OutputEncoding);
    }
    else if ((OutputEncoding == PcmPlayer_c::BYPASS_SPDIFIN_COMPRESSED) || (OutputEncoding == PcmPlayer_c::BYPASS_SPDIFIN_COMPRESSED_SD))
    {
        SE_INFO(group_mixer, "Setting bypass for %s\n", PcmPlayer_c::LookupOutputEncoding(OutputEncoding));
        SpdifOut.Apply = ACC_MME_ENABLED;
        SpdifOut.Config.Mode = 0; // SPDIF mode
        SpdifOut.Config.UpdateSpdifControl = 0;
        SpdifOut.Config.SpdifCompressed = 1;
        SpdifOut.Config.AddIECPreamble = 0;
        SpdifOut.Config.ForcePC = 0;
        SpdifOut.Spdifout.ChannelStatus[0] = 0x02000000; /* consumer, linear PCM, mode 00 */
        SpdifOut.Spdifout.ChannelStatus[1] = 0x02000000; /* 16-bit word length */
        SpdifOut.Config.Endianness = 1; // little endian input frames
    }
    else if (OutputEncoding == PcmPlayer_c::BYPASS_SPDIFIN_PCM)
    {
        SE_INFO(group_mixer, "Setting bypass for BYPASS_SPDIFIN_PCM\n");
        SpdifOut.Apply = ACC_MME_ENABLED;
        SpdifOut.Config.Mode = 0; // SPDIF mode
        SpdifOut.Config.UpdateSpdifControl = 0;
        SpdifOut.Config.SpdifCompressed = 0;
        SpdifOut.Config.AddIECPreamble = 0;
        SpdifOut.Config.ForcePC = 0; // useless in this case: for compressed mode only
        SpdifOut.Spdifout.ChannelStatus[0] = 0x00000000; /* consumer, linear PCM, mode 00 */
        SpdifOut.Spdifout.ChannelStatus[1] = 0x00000000;    /* word length not indicated */
        SpdifOut.Config.Endianness = 1; // little endian input frames
        // No need to set layout. FW will takecare of the layout in SPDIFin bypass mode
    }
    else
    {
        SpdifOut.Apply = ACC_MME_DISABLED;
        SE_DEBUG(group_mixer, "@: SpdifOut.Apply->ACC_MME_DISABLED\n");
    }

    // A convenience macro to allow the entries below to be copied from the IEC
    // standards document. The sub-macro chops of the high bits (the ones that
    // select which word within the channel mask we need). The full macro then
    // performs an endian swap since the firmware expects big endian values.
#define B(x) (((_B(x) & 0xff) << 24) | ((_B(x) & 0xff00) << 8) | ((_B(x) >> 8)  & 0xff00) | ((_B(x) >> 24) & 0xff))
#define _B(x) (1 << ((x) % 32))
    const uint32_t use_of_channel_status_block = B(0);
    const uint32_t linear_pcm_identification = B(1);
    //const uint32_t copyright_information = B(2);
    const uint32_t additional_format_information = B(3) | B(4) | B(5);
    const uint32_t mode = B(6) | B(7);
    //const uint32_t category_code = 0x0000ff00;
    //const uint32_t source_number = B(16) | B(17) | B(18) | B(19);
    //const uint32_t channel_number = B(20) | B(21) | B(22) | B(23);
    const uint32_t sampling_frequency = B(24) | B(25) | B(26) | B(27);
    //const uint32_t clock_accuracy = B(28) | B(29);
    const uint32_t word_length = B(32) | B(33) | B(34) | B(35);
    //const uint32_t original_sampling_frequency = B(36) | B(37) | B(38) | B(39);
    //const uint32_t cgms_a = B(40) | B(41);
#undef B
#undef _B
    const uint32_t STSZ = 6;
    uint32_t ChannelStatusMask[STSZ];

    stm_se_aes_iec958_t channel_metadata;
    stm_se_aes_iec958_t channel_mask;
    PlayerStatus_t status = PlayerNoError;

    if (MixerPlayer[MixerPlayerIndex].GetCompoundOption(STM_SE_CTRL_AUDIO_PLAYER_AES_IEC958_METADATA, &channel_metadata) != PlayerNoError)
    {
        SE_ERROR("<%s> GetCompoundOption metadata Error\n", Name);
        status = PlayerError;
    }
    if (MixerPlayer[MixerPlayerIndex].GetCompoundOption(STM_SE_CTRL_AUDIO_PLAYER_AES_IEC958_MASK, &channel_mask) != PlayerNoError)
    {
        SE_ERROR("<%s> GetCompoundOption mask Error\n", Name);
        status = PlayerError;
    }

    if (status == PlayerNoError)
    {
        //
        // Copy the userspace mask for the channel status bits. This consists mostly
        // of set bits.
        //

        for (uint32_t i = 0; i < STSZ; i++)
        {
            ChannelStatusMask[i] = channel_mask.status[i * 4 + 0] << 24 |
                                   channel_mask.status[i * 4 + 1] << 16 |
                                   channel_mask.status[i * 4 + 2] <<  8 |
                                   channel_mask.status[i * 4 + 3] <<  0;
        }

        // these should never be overlaid
        ChannelStatusMask[0] &= ~(use_of_channel_status_block |
                                  linear_pcm_identification |
                                  additional_format_information | /* auto fill in for PCM, 000 for coded data */
                                  mode |
                                  sampling_frequency);  /* auto fill in */
        ChannelStatusMask[1] &= ~(word_length);

        //
        // The mask is now complete so we can overlay the userspace supplied channel
        // status bits.
        //
        for (uint32_t i = 0; i < STSZ; i++)
        {
            SpdifOut.Spdifout.ChannelStatus[i] |= ChannelStatusMask[i] &
                                                  ((channel_metadata.status[i * 4 + 0] << 24) |
                                                   (channel_metadata.status[i * 4 + 1] << 16) |
                                                   (channel_metadata.status[i * 4 + 2] << 8) |
                                                   (channel_metadata.status[i * 4 + 3] << 0));
        }
    }
}


////////////////////////////////////////////////////////////////////////////
///
/// Fill out the PCM post-processing required for the common processing chain.
///
PlayerStatus_t Mixer_Mme_c::FillCommonChainPcmParameters(uint8_t *ptr, uint32_t &size)
{
    size = 0;

    // Setup the DDRE/PcmRender Processing structure
    if (mApplicationType == STM_SE_CTRL_VALUE_AUDIO_APPLICATION_MS11  ||
        mApplicationType == STM_SE_CTRL_VALUE_AUDIO_APPLICATION_MS12)
    {
        MME_METARENCGlobalParams_t *MetaRencode = reinterpret_cast<MME_METARENCGlobalParams_t *>(ptr);

        memset(MetaRencode, 0, sizeof(*MetaRencode));
        MetaRencode->Id                        = PCMPROCESS_SET_ID(ACC_PCM_METARENC_ID, ACC_MIX_COMMON);
        MetaRencode->StructSize                = sizeof(MME_METARENCGlobalParams_t);
        MetaRencode->Apply                     = ACC_MME_ENABLED;
        MetaRencode->MetadataType              = METADATA_TYPE_DOLBY;
        MetaRencode->MetaDataDesc.DDRECfg.StartupDelay   = 0;

        // If DV258 enabled on Common chain before DDRE, set No Compression mode else Film standard
        // TODO(pht) check with audiofw status of following hack
        bool DV258Enabled = false; // Upon DV258 integration update this flag based upon alsa control for Dolby Volume.
        MetaRencode->MetaDataDesc.DDRECfg.CompProfile = DV258Enabled
                                                        ? DDRE_COMPR_NO_COMPRESSION
                                                        : DDRE_COMPR_FILM_STANDARD;
        size += MetaRencode->StructSize;
    }

    return PlayerNoError;
}


////////////////////////////////////////////////////////////////////////////
///
/// Fill out the Tuning post-processing required for the common processing chain.
///
void Mixer_Mme_c::FillCommonTuningParameters(uint8_t *ptr, uint32_t *size)
{
    PrepareMixerPcmProcTuningCommand(&MixerSettings.MixerOptions.VolumeManagerParams,
                                     MixerSettings.MixerOptions.VolumeManagerAmount,
                                     ACC_MIX_COMMON, ptr, size);
    PrepareMixerPcmProcTuningCommand(&MixerSettings.MixerOptions.VirtualizerParams,
                                     MixerSettings.MixerOptions.VirtualizerAmount,
                                     ACC_MIX_COMMON, ptr, size);
    PrepareMixerPcmProcTuningCommand(&MixerSettings.MixerOptions.UpmixerParams,
                                     MixerSettings.MixerOptions.UpmixerAmount,
                                     ACC_MIX_COMMON, ptr, size);
    PrepareMixerPcmProcTuningCommand(&MixerSettings.MixerOptions.DialogEnhancerParams,
                                     MixerSettings.MixerOptions.DialogEnhancerAmount,
                                     ACC_MIX_COMMON, ptr, size);
    PrepareMixerPcmProcTuningCommand(&MixerSettings.MixerOptions.EqualizerParams,
                                     MixerSettings.MixerOptions.EqualizerAmount,
                                     ACC_MIX_COMMON, ptr, size);
    PrepareMixerPcmProcTuningCommand(&MixerSettings.MixerOptions.BassEnhancerParams,
                                     MixerSettings.MixerOptions.BassEnhancerAmount,
                                     ACC_MIX_COMMON, ptr, size);
}

void Mixer_Mme_c::FillOutDeviceTuningParameters(uint8_t *ptr, uint32_t *size, enum eAccMixOutput chain)
{
    int amount;
    struct PcmProcManagerParams_s  *VolumeManager = NULL;
    MixerPlayer[chain].GetCompoundOption(STM_SE_CTRL_VOLUME_MANAGER, (void *)&VolumeManager);
    MixerPlayer[chain].GetOption(STM_SE_CTRL_VOLUME_MANAGER_AMOUNT, amount);
    if (VolumeManager != NULL)
    {
        PrepareMixerPcmProcTuningCommand(VolumeManager, amount, chain, ptr, size);
    }

    struct PcmProcManagerParams_s  *Virtualizer = NULL;
    MixerPlayer[chain].GetCompoundOption(STM_SE_CTRL_VIRTUALIZER, (void *)&Virtualizer);
    MixerPlayer[chain].GetOption(STM_SE_CTRL_VIRTUALIZER_AMOUNT, amount);
    if (Virtualizer != NULL)
    {
        PrepareMixerPcmProcTuningCommand(Virtualizer, amount, chain, ptr, size);
    }

    struct PcmProcManagerParams_s  *Upmixer = NULL;
    MixerPlayer[chain].GetCompoundOption(STM_SE_CTRL_UPMIXER, (void *)&Upmixer);
    MixerPlayer[chain].GetOption(STM_SE_CTRL_UPMIXER_AMOUNT, amount);
    if (Upmixer != NULL && Upmixer->ProfileAddr != NULL)
    {
        PrepareMixerPcmProcTuningCommand(Upmixer, amount, chain, ptr, size);
    }

    struct PcmProcManagerParams_s  *DialogEnhancer = NULL;
    MixerPlayer[chain].GetCompoundOption(STM_SE_CTRL_DIALOG_ENHANCER, (void *)&DialogEnhancer);
    MixerPlayer[chain].GetOption(STM_SE_CTRL_DIALOG_ENHANCER_AMOUNT, amount);
    if (DialogEnhancer != NULL && DialogEnhancer->ProfileAddr != NULL)
    {
        PrepareMixerPcmProcTuningCommand(DialogEnhancer, amount, chain, ptr, size);
    }

    struct PcmProcManagerParams_s  *Equalizer = NULL;
    MixerPlayer[chain].GetCompoundOption(STM_SE_CTRL_EQUALIZER, (void *)&Equalizer);
    MixerPlayer[chain].GetOption(STM_SE_CTRL_EQUALIZER_AMOUNT, amount);
    if (Equalizer != NULL && Equalizer->ProfileAddr != NULL)
    {
        PrepareMixerPcmProcTuningCommand(Equalizer, amount, chain, ptr, size);
    }

    struct PcmProcManagerParams_s  *BassEnhancer = NULL;
    MixerPlayer[chain].GetCompoundOption(STM_SE_CTRL_BASS_ENHANCER, (void *)&BassEnhancer);
    MixerPlayer[chain].GetOption(STM_SE_CTRL_BASS_ENHANCER_AMOUNT, amount);
    if (BassEnhancer != NULL && BassEnhancer->ProfileAddr != NULL)
    {
        PrepareMixerPcmProcTuningCommand(BassEnhancer, amount, chain, ptr, size);
    }
}

////////////////////////////////////////////////////////////////////////////
///
/// Fill out the PCM post-processing required for a single physical output.
///
PlayerStatus_t Mixer_Mme_c::FillOutDevicePcmParameters(MME_LxPcmPostProcessingGlobalParameters_Frozen_t &PcmParams,
                                                       uint32_t MixerPlayerIndex,
                                                       int ApplicationType)
{
    PcmPlayer_c::OutputEncoding OutputEncoding;
    uint32_t                    PcmProcTargetSampleRate;

    PlayerStatus_t status = LookupPlayerFinalOutputEncodingAndFreq(MixerPlayerIndex, &OutputEncoding, &PcmProcTargetSampleRate);
    if (status != PlayerNoError)
    {
        SE_ERROR("Failed to retrieve Player OutputEncoding & Frequency PlayerIndex:%d\n", MixerPlayerIndex);
        return status;
    }

    //
    // Summarize the processing that are enabled
    //
    int EnableDcRemove = false;
    MixerPlayer[MixerPlayerIndex].GetOption(STM_SE_CTRL_DCREMOVE, EnableDcRemove);
    SE_DEBUG(group_mixer, ">%s Card %d: EnableDcRemove=%d\n",
             Name,
             MixerPlayerIndex,
             EnableDcRemove);
    // the encoder choice complex - this is determined later
    bool EnableDMix = (true == CheckDownmixBypass(MixerPlayer[MixerPlayerIndex].GetCard(), OutputEncoding)) ? false : true;
    SE_DEBUG(group_mixer, ">%s Card %d: %s is %s\n",
             Name,
             MixerPlayerIndex,
             MixerPlayer[MixerPlayerIndex].GetCardName(),
             PcmPlayer_c::LookupOutputEncoding(OutputEncoding));
    //
    // Update the enables if we are in a bypassed mode
    //

    if (PcmPlayer_c::IsOutputBypassed(OutputEncoding))
    {
        EnableDcRemove = false;
        EnableDMix = false;
    }

    memset(&PcmParams, 0, sizeof(PcmParams));
    PcmParams.Id = ACC_RENDERER_MIXER_POSTPROCESSING_ID;
    // These next two would be relevant if doing a single card
    PcmParams.NbPcmProcessings = 0; //!< NbPcmProcessings on main[0..3] and aux[4..7]
    PcmParams.AuxSplit = SPLIT_AUX;     //! Point of split between Main output and Aux output
    uint32_t HeaderSize = offsetof(MME_LxPcmPostProcessingGlobalParameters_Frozen_t, AuxSplit)
                          + sizeof(MME_LxPcmPostProcessingGlobalParameters_Frozen_t::AuxSplit);
    SE_DEBUG(group_mixer, "@%s HeaderSize: %d\n", Name, HeaderSize);
    PcmParams.StructSize = HeaderSize;

    {
        // Get bassmgt config from audio-player
        stm_se_bassmgt_t audioplayerBassMgtConfig;
        MixerPlayer[MixerPlayerIndex].GetCompoundOption(STM_SE_CTRL_BASSMGT, (void *) &audioplayerBassMgtConfig);

        PcmParams.StructSize += PcmProc::FillOutBassMgtPPparams(&PcmParams.BassMgt,
                                                                OutputEncoding,
                                                                MixerPlayerIndex,
                                                                audioplayerBassMgtConfig);

        PcmParams.StructSize += PcmProc::FillOutDelayPPparams(&PcmParams.Delay,
                                                              MixerPlayerIndex,
                                                              audioplayerBassMgtConfig);
    }
    {
        MME_EqualizerGlobalParams_t &Equalizer = PcmParams.Equalizer;
        Equalizer.Id = PCMPROCESS_SET_ID(ACC_PCM_EQUALIZER_ID, MixerPlayerIndex);
        Equalizer.StructSize = sizeof(Equalizer);
        PcmParams.StructSize += Equalizer.StructSize;
        Equalizer.Apply = ACC_MME_DISABLED;
    }
    {
        MME_TempoGlobalParams_t &TempoControl = PcmParams.TempoControl;
        TempoControl.Id = PCMPROCESS_SET_ID(ACC_PCM_TEMPO_ID, MixerPlayerIndex);
        TempoControl.StructSize = sizeof(TempoControl);
        PcmParams.StructSize += TempoControl.StructSize;
        TempoControl.Apply = ACC_MME_DISABLED;
    }
    {
        MME_DCRemoveGlobalParams_t &DCRemove = PcmParams.DCRemove;
        DCRemove.Id = PCMPROCESS_SET_ID(ACC_PCM_DCREMOVE_ID, MixerPlayerIndex);
        DCRemove.StructSize = sizeof(DCRemove);
        PcmParams.StructSize += DCRemove.StructSize;
        DCRemove.Apply = (EnableDcRemove ? ACC_MME_ENABLED : ACC_MME_DISABLED);
    }

    {
        eAccAcMode PlayerAcMode = TranslateDownstreamCardToMainAudioMode(MixerPlayer[MixerPlayerIndex].GetCard(), OutputEncoding);

        bool useTranscoder = true;

        PcmParams.StructSize += PcmProc::FillOutEncoderPPparams(&PcmParams.Encoder,
                                                                &PcmParams.Transcoder,
                                                                OutputEncoding,
                                                                MixerPlayerIndex,
                                                                useTranscoder,
                                                                ApplicationType,
                                                                PlayerAcMode);
    }

    //
    {
        MME_SfcPPGlobalParams_t &Sfc = PcmParams.Sfc;
        Sfc.Id = PCMPROCESS_SET_ID(ACC_PCM_SFC_ID, MixerPlayerIndex);
        Sfc.StructSize = sizeof(Sfc);
        PcmParams.StructSize += Sfc.StructSize;
        Sfc.Apply = ACC_MME_DISABLED;
    }

    PcmParams.StructSize += PcmProc::FillOutResamplex2PPparams(&PcmParams.Resamplex2,
                                                               OutputEncoding,
                                                               MixerPlayerIndex,
                                                               MixerSamplingFrequency,
                                                               PcmProcTargetSampleRate);
    {
        // Get BTSC config from audio-player
        stm_se_btsc_t                               audioplayerBtscConfig;
        stm_se_ctrl_audio_player_hardware_mode_t    audioplayerHwMode;
        MixerPlayer[MixerPlayerIndex].GetCompoundOption(STM_SE_CTRL_BTSC, (void *) &audioplayerBtscConfig);
        MixerPlayer[MixerPlayerIndex].GetCompoundOption(STM_SE_CTRL_AUDIO_PLAYER_HARDWARE_MODE, (void *) &audioplayerHwMode);

        PcmParams.StructSize += PcmProc::FillOutBtscPPparams(&PcmParams.Btsc,
                                                             MixerPlayerIndex,
                                                             audioplayerBtscConfig,
                                                             audioplayerHwMode,
                                                             MixerPlayer[MixerPlayerIndex].GetCardMaxFreq());
    }
    //
    {
        stm_se_drc_t audioplayerdrcConfig;
        MixerPlayer[MixerPlayerIndex].GetCompoundOption(STM_SE_CTRL_AUDIO_DYNAMIC_RANGE_COMPRESSION,
                                                        (void *) &audioplayerdrcConfig);
        stm_se_dual_mode_t player_dualmode;
        MixerPlayer[MixerPlayerIndex].GetOption(STM_SE_CTRL_DUALMONO, (int &)player_dualmode);

        int isStreamDrivenDualMono;
        MixerPlayer[MixerPlayerIndex].GetOption(STM_SE_CTRL_STREAM_DRIVEN_DUALMONO, isStreamDrivenDualMono);

        eAccAcMode PlayerAcMode = TranslateDownstreamCardToMainAudioMode(MixerPlayer[MixerPlayerIndex].GetCard(), OutputEncoding);

        PcmParams.StructSize += PcmProc::FillOutCMCparams(&PcmParams.CMC,
                                                          OutputEncoding,
                                                          MixerPlayerIndex,
                                                          audioplayerdrcConfig,
                                                          player_dualmode,
                                                          isStreamDrivenDualMono,
                                                          MixerPlayer[MixerPlayerIndex].GetCard().target_level,
                                                          PlayerAcMode,
                                                          MixerPlayer[MixerPlayerIndex].GetCard().channel_assignment.pair0,
                                                          LookupMixerAudioMode());
    }
    //
    FillOutDeviceDownmixParameters(PcmParams, MixerPlayerIndex, EnableDMix);
    //
    FillOutDeviceSpdifParameters(PcmParams, MixerPlayerIndex, OutputEncoding);
    //
    {
        // Get limiter config from audio-player
        stm_se_limiter_t  audioplayerLimiterConfig;
        int               gain, delay, softmute;
        MixerPlayer[MixerPlayerIndex].GetCompoundOption(STM_SE_CTRL_LIMITER, (void *) &audioplayerLimiterConfig);
        MixerPlayer[MixerPlayerIndex].GetOption(STM_SE_CTRL_AUDIO_GAIN, gain);
        MixerPlayer[MixerPlayerIndex].GetOption(STM_SE_CTRL_AUDIO_DELAY, delay);
        MixerPlayer[MixerPlayerIndex].GetOption(STM_SE_CTRL_AUDIO_SOFTMUTE, softmute);

        bool AtLeastOneClientPlaying = false;
        for (int ClientIdx = 0; ClientIdx < MIXER_MAX_CLIENTS; ClientIdx++)
        {
            if (Client[ClientIdx].GetState() == STARTED || Client[ClientIdx].GetState() == STARTING)
            {
                AtLeastOneClientPlaying = true;
                break;
            }
        }

        if (AtLeastOneClientPlaying == false)
        {
            // None of broadcast stream clients are playing, there is no point to enable Limiter
            // as it will impact other inputs such as AudioGenerators that never provide DialNorm values
            audioplayerLimiterConfig.apply = 0;
        }

        PcmParams.StructSize += PcmProc::FillOutLimiterPPparams(&PcmParams.Limiter,
                                                                OutputEncoding,
                                                                MixerPlayerIndex,
                                                                audioplayerLimiterConfig,
                                                                ApplicationType,
                                                                MixerPlayer[MixerPlayerIndex].GetCard().target_level,
                                                                MixerSettings.MixerOptions.MasterPlaybackVolume + gain,
                                                                delay,
                                                                softmute,
                                                                MixerPlayer[MixerPlayerIndex].GetSinkType(),
                                                                MixerPlayer[MixerPlayerIndex].IsConnectedToHdmi(),
                                                                MixerPlayer[MixerPlayerIndex].IsConnectedToSpdif(),
                                                                !MMEInitialized);
    }
    {
        // DDRE is disabled on all outputs except the common chain
        MME_METARENCGlobalParams_t &MetaRencode = PcmParams.MetaRencode;
        MetaRencode.Id = PCMPROCESS_SET_ID(ACC_PCM_METARENC_ID, MixerPlayerIndex);
        MetaRencode.StructSize = sizeof(MetaRencode);
        PcmParams.StructSize += MetaRencode.StructSize;
        MetaRencode.Apply = ACC_MME_DISABLED;
    }

    FillOutDeviceTuningParameters((uint8_t *) &PcmParams,
                                  (uint32_t *) &PcmParams.StructSize,
                                  (enum eAccMixOutput) MixerPlayerIndex);

    SE_DEBUG(group_mixer, "<%s PcmParams.StructSize: %d\n", Name, PcmParams.StructSize);
    return PlayerNoError;
}

////////////////////////////////////////////////////////////////////////////
///
/// ResetBufferTypeFlags resets all MME Buffer Flags to 0.
///
void Mixer_Mme_c::ResetBufferTypeFlags()
{
    unsigned int nb_buffers = MixerCommand.Command.NumberInputBuffers
                              + MixerCommand.Command.NumberOutputBuffers;

    for (int i = 0; i < nb_buffers; i++)
    {
        MixerCommand.Command.DataBuffers_p[i]->Flags = 0;
    }

    nb_buffers = FirstMixerCommand.Command.NumberInputBuffers + FirstMixerCommand.Command.NumberOutputBuffers;
    for (int i = 0; i < nb_buffers; i++)
    {
        FirstMixerCommand.Command.DataBuffers_p[i]->Flags = 0;
    }
}

////////////////////////////////////////////////////////////////////////////
///
/// Prepare the mixer command obtaining any required working buffers as we go.
///
/// The output buffers are obtained first since the this may involve waiting
/// for the output buffers to be ready. Waiting for output buffers is a blocking
/// operation (obviously).
///
/// The collection of input buffers is most definitely not a blocking operation
/// (since blocking due to starvation would cause a pop on the output).
///
PlayerStatus_t Mixer_Mme_c::FillOutMixCommand()
{
    ManifestorStatus_t Status;
    uint32_t InteractiveAudioIdx;
    bool FillInputWithSilentBuffer = false;
    SE_VERBOSE(group_mixer, ">\n");

    ResetBufferTypeFlags();

    Status = FillOutOutputBuffers();

    if (Status != PlayerNoError)
    {
        SE_ERROR("Failed to obtain/populate output buffer\n");
        return Status;
    }

    if (MMEFirstMixerHdl != MME_INVALID_ARGUMENT)
    {
        Status = FillOutFirstMixerOutputBuffers();
        if (Status != PlayerNoError)
        {
            SE_ERROR("Failed to obtain/populate First Mixer output buffer\n");
            return Status;
        }
    }

    if (MMEFirstMixerHdl != MME_INVALID_ARGUMENT)
    {
        for (uint32_t ClientIdx(0); ClientIdx < FIRST_MIXER_MAX_CLIENTS; ClientIdx++)
        {
            Client[ClientIdx].LockTake();
            FillOutFirstMixerInputBuffer(ClientIdx, FillInputWithSilentBuffer);
            Client[ClientIdx].LockRelease();
        }

        /* Fill 2nd Mixer input[0] with InterMixerBuffer */
        FillOutSecondMixerInputBuffer();

        /* 2nd Mixer handles remaining clients that cannot be connected to 1st Mixer */
        for (uint32_t ClientIdx(FIRST_MIXER_MAX_CLIENTS); ClientIdx < MIXER_MAX_CLIENTS; ClientIdx++)
        {
            Client[ClientIdx].LockTake();
            FillOutInputBuffer(ClientIdx, FillInputWithSilentBuffer);
            Client[ClientIdx].LockRelease();
        }
    }
    else
    {
        for (uint32_t ClientIdx(0); ClientIdx < MIXER_MAX_CLIENTS; ClientIdx++)
        {
            Client[ClientIdx].LockTake();
            FillOutInputBuffer(ClientIdx, FillInputWithSilentBuffer);
            Client[ClientIdx].LockRelease();
        }
    }

    for (uint32_t ClientIdx(0); ClientIdx < MIXER_MAX_CLIENTS; ClientIdx++)
    {
        Client[ClientIdx].LockTake();
        if (((Client[ClientIdx].GetState() == STARTED) || (Client[ClientIdx].GetState() == STOPPING)))
        {
            int OutputRateAdjustment =   Client[ClientIdx].GetManifestor()->GetOutputRateAdjustment();
            SE_VERBOSE(group_mixer, ">%s OutputRateAdjustment[%d] %d\n", Name, ClientIdx, OutputRateAdjustment);
            SetOutputRateAdjustment(OutputRateAdjustment);
            Client[ClientIdx].LockRelease();
            break;
        }
        Client[ClientIdx].LockRelease();
    }

    for (uint32_t AudioGeneratorIdx = 0; AudioGeneratorIdx < MAX_AUDIO_GENERATORS; AudioGeneratorIdx++)
    {
        FillOutAudioGenBuffer(AudioGeneratorIdx);
    }

    for (InteractiveAudioIdx = 0; InteractiveAudioIdx < MAX_INTERACTIVE_AUDIO; InteractiveAudioIdx++)
    {
        FillOutInteractiveAudioBuffer(MixerCommand.Command.DataBuffers_p[MIXER_INTERACTIVE_INPUT + InteractiveAudioIdx], InteractiveAudioIdx);
    }

    SE_VERBOSE(group_mixer, "<\n");
    return PlayerNoError;
}

PlayerStatus_t Mixer_Mme_c::FillOutFirstMixerOutputBuffers()
{
    uint32_t OnePeriodSize = MixerPlayer[FirstActivePlayer].GetPcmPlayerConfigSurfaceParameters().PeriodSize;
    SE_VERBOSE(group_mixer, ">%s OnePeriodSize: %u\n", Name, OnePeriodSize);

    uint32_t OutBufIdx = FirstMixerCommand.Command.NumberInputBuffers;
    MME_DataBuffer_t *OutputDataBuffer;

    if (InterMixerFreeBufRing->Extract((uintptr_t *) &OutputDataBuffer, RING_NONE_BLOCKING) != RingNoError)
    {
        SE_ERROR("Cannot extract Buff from InterMixerFreeBufRing\n");
        return PlayerError;
    }

    FirstMixerCommand.DataBufferList[OutBufIdx++] = OutputDataBuffer;
    OutputDataBuffer->Flags                       = BUFFER_TYPE_AUDIO_IO;
    OutputDataBuffer->ScatterPages_p->FlagsIn     = 0;
    OutputDataBuffer->ScatterPages_p->BytesUsed   = 0;
    // Because UpdateFirstMixerOutputBuffers makes this equal to BytesUsed
    OutputDataBuffer->ScatterPages_p->Size        = MAX_INTER_BUFFER_SIZE;

    if (InterMixerFreeMDRing->Extract((uintptr_t *) &OutputDataBuffer, RING_NONE_BLOCKING) != RingNoError)
    {
        SE_ERROR("Cannot extract Buff from InterMixerFreeMDRing\n");
        return PlayerError;
    }

    FirstMixerCommand.DataBufferList[OutBufIdx]   = OutputDataBuffer;
    OutputDataBuffer->Flags                       = BUFFER_TYPE_META_DATA_IO;
    OutputDataBuffer->ScatterPages_p->FlagsIn     = 0;
    OutputDataBuffer->ScatterPages_p->BytesUsed   = 0;

    return PlayerNoError;
}

////////////////////////////////////////////////////////////////////////////
///
/// Populate a data buffer ready for it to contain output samples.
///
PlayerStatus_t Mixer_Mme_c::FillOutOutputBuffers()
{
    PlayerStatus_t Status;
    uint32_t OnePeriodSize = MixerPlayer[FirstActivePlayer].GetPcmPlayerConfigSurfaceParameters().PeriodSize;
    SE_VERBOSE(group_mixer, ">%s OnePeriodSize: %u\n", Name, OnePeriodSize);
    // This value cannot be pre-configured.
    MixerCommand.Command.NumberOutputBuffers = NumberOfAudioPlayerAttached;
    Status = MapPcmPlayersSamples(OnePeriodSize, false);

    if (Status != PlayerNoError)
    {
        SE_ERROR("Failed to map a period of samples (%u)\n", OnePeriodSize);
        DeployPcmPlayersUnderrunRecovery();
        return PlayerError;
    }

    Status = GetAndUpdateDisplayTimeOfNextCommit();

    if (PlayerNoError != Status)
    {
        SE_ERROR("Failed to update time of next commit\n");
        return Status;
    }

    for (uint32_t CountOfAudioPlayerAttached(0), PlayerIdx(0); CountOfAudioPlayerAttached < NumberOfAudioPlayerAttached; PlayerIdx++)
    {
        uint32_t OutBufIdx = MixerCommand.Command.NumberInputBuffers + CountOfAudioPlayerAttached;
        MixerCommand.DataBufferList[OutBufIdx] = &MixerCommand.DataBuffers[OutBufIdx];
        MME_DataBuffer_t *OutputDataBuffer = MixerCommand.Command.DataBuffers_p[OutBufIdx];

        //By default, will be overwritten if needed.
        OutputDataBuffer->NumberOfScatterPages = 0;
        OutputDataBuffer->TotalSize = 0;

        if (true == MixerPlayer[PlayerIdx].IsPlayerObjectAttached())
        {
            CountOfAudioPlayerAttached++;

            if (true == MixerPlayer[PlayerIdx].HasPcmPlayer())
            {
                uPcmBufferFlags *PcmBufferFlags = (uPcmBufferFlags *)& OutputDataBuffer->Flags;
                OutputDataBuffer->ScatterPages_p->Page_p = MixerPlayer[PlayerIdx].GetPcmPlayerMappedSamplesArea();
                OutputDataBuffer->ScatterPages_p->Size = MixerPlayer[PlayerIdx].GetPcmPlayer()->SamplesToBytes(OnePeriodSize);
                OutputDataBuffer->ScatterPages_p->FlagsIn = 0;
                OutputDataBuffer->ScatterPages_p->BytesUsed = 0;
                OutputDataBuffer->NumberOfScatterPages = 1;
                OutputDataBuffer->TotalSize = OutputDataBuffer->ScatterPages_p->Size;
                /* Default Output buffer order is  MAIN(Analog0), AUX(Analog1), DIGITALOUT(SPDIF), HDMIOUT(HDMI)
                   AudioFW can except any buffer order provided the Flag of the DataBuffer is set with the order of the buffer as above
                   Type is 8 bit long to represent type of the data [0xCD ==> Compressed Data] or [ 0xAn ==> Audio Data [n]] */
                PcmBufferFlags->bits.Type = BUFFER_TYPE_AUDIO_IO | PlayerIdx;
                SE_VERBOSE(group_mixer, "@:%s %s OutputDataBuffer: %p ->TotalSize=%u\n",
                           Name, MixerPlayer[PlayerIdx].GetCardName(), OutputDataBuffer, OutputDataBuffer->TotalSize);
            }
        }
    }

    return PlayerNoError;
}


void Mixer_Mme_c::SetMixerCodedInputStopCommand(bool updateRequired)
{
    for (uint32_t CodedIndex = 0; CodedIndex < MIXER_MAX_CODED_INPUTS; CodedIndex++)
    {
        uint32_t Idx(MIXER_CODED_DATA_INPUT + CodedIndex);
        MixerCommand.InputParams.InputParam[Idx].Command = MIXER_STOP;
        MixerCommand.InputParams.InputParam[Idx].StartOffset = 0;
        MixerCommand.InputParams.InputParam[Idx].PTSflags.U32 = 0;
        MixerCommand.InputParams.InputParam[Idx].PTS = 0;

        if (MIXER_CODED_DATA_INPUT_SPDIF_IDX == CodedIndex ||
            MIXER_CODED_DATA_INPUT_HDMI_IDX  == CodedIndex)
        {
            if (PrimaryCodedDataType[CodedIndex] != PcmPlayer_c::OUTPUT_IEC60958)
            {
                PrimaryCodedDataType[CodedIndex] = PcmPlayer_c::OUTPUT_IEC60958;
                // Moving from bypass to PCM mode update the PcmPlayerParameters
                if (updateRequired)
                {
                    PcmPlayerNeedsParameterUpdate = true;
                    MMENeedsParameterUpdate = true;
                    OutputTopologyUpdated   = true;
                }
            }
        }
    }
}

void Mixer_Mme_c::DumpMixerCodedInput()
{
    if (SE_IS_VERBOSE_ON(group_mixer))
    {
        SE_VERBOSE(group_mixer, "@: PrimaryCodedDataType[]: -> SPDIF:%s HDMI:%s\n",
                   PcmPlayer_c::LookupOutputEncoding(PrimaryCodedDataType[MIXER_CODED_DATA_INPUT_SPDIF_IDX]),
                   PcmPlayer_c::LookupOutputEncoding(PrimaryCodedDataType[MIXER_CODED_DATA_INPUT_HDMI_IDX]));
    }
}


void Mixer_Mme_c::FillOutFirstMixerInputBuffer(uint32_t Id, bool ForceSilentBuffer)
{
    PlayerStatus_t Status(PlayerNoError);
    SE_VERBOSE(group_mixer, ">: Client[%d].State: %s\n", Id, Mixer_c::LookupInputState(Client[Id].GetState()));

    if (((Client[Id].GetState() == STARTED) || (Client[Id].GetState() == STOPPING)) && !ForceSilentBuffer)
    {
        bool Muted = (Client[Id].IsMute() == true);
        //
        // Generate a ratio used by the manifestor to work out how many samples are required
        // for normal play and how many samples should be reserved for de-pop.
        //
        // both numbers should be divisible by 25
        // This client is correctly stated, and its parameters are well known.
        int OutputFrequency = MixerSamplingFrequency;

        if (0 == OutputFrequency)
        {
            SE_INFO(group_mixer, "I-MixerSamplingFrequency 0; forcing default\n");
            OutputFrequency = STM_SE_DEFAULT_AUDIO_SAMPLE_RATE_HZ;
        }

        // In MS12 mode, AD mixer must drop SecondInput if its sampling frequency is not equal to Main Input freq.
        if (mApplicationType == STM_SE_CTRL_VALUE_AUDIO_APPLICATION_MS12)
        {
            if (Id != PrimaryClient && Client[PrimaryClient].ManagedByThread() &&
                (Client[PrimaryClient].GetSourceSurfaceParameter().SampleRateHz != Client[Id].GetSourceSurfaceParameter().SampleRateHz))
            {
                SE_VERBOSE(group_mixer, "Mixer inputs have different Sampling Frequencies -> MUTE Input:%d\n", Id);
                Muted = true;
            }
        }

        // Primary client, no bypass.
        Status = Client[Id].GetManifestor()->FillOutInputBuffer(MixerGranuleSize,
                                                                OutputFrequency,
                                                                Client[Id].GetState() == STOPPING,
                                                                FirstMixerCommand.Command.DataBuffers_p[Id],
                                                                FirstMixerCommand.Command.DataBuffers_p[FIRST_MIXER_INPUT_METADATA_INDEX + Id],
                                                                &FirstMixerCommand.InputParams.InputParam[Id],
                                                                Muted, mApplicationType);
        FirstMixerCommand.Command.DataBuffers_p[Id]->Flags = BUFFER_TYPE_AUDIO_IO | Id;
        FirstMixerCommand.Command.DataBuffers_p[FIRST_MIXER_INPUT_METADATA_INDEX + Id]->Flags = BUFFER_TYPE_META_DATA_IO | Id;

        if (PlayerNoError == Status)
        {
            // All is correct, can return now.
            SE_VERBOSE(group_mixer, "<\n");
            return;
        }
    }
    else
    {
        // This client is not running or a request to force the silence buffer so fill out silence.
        // (STARTING, ...)
        FillOutSilentBuffer(FirstMixerCommand.Command.DataBuffers_p[Id],
                            &FirstMixerCommand.InputParams.InputParam[Id]);
        ClearMetadataBuffer(FirstMixerCommand.Command.DataBuffers_p[FIRST_MIXER_MAX_CLIENTS + Id]);
        // Let return normally
    }

    if (PlayerNoError != Status)
    {
        SE_ERROR("Manifestor failed to populate its input buffer\n");
        // error recovery is provided by falling through and filling out a silent buffer
        FillOutSilentBuffer(FirstMixerCommand.Command.DataBuffers_p[Id],
                            &FirstMixerCommand.InputParams.InputParam[Id]);
        ClearMetadataBuffer(FirstMixerCommand.Command.DataBuffers_p[FIRST_MIXER_MAX_CLIENTS + Id]);
    }

    SE_VERBOSE(group_mixer, "<\n");
}

void Mixer_Mme_c::FillOutSecondMixerInputBuffer()
{
    if (InterMixerFillBufRing->NonEmpty())
    {
        RingStatus_t err = InterMixerFillBufRing->Extract((uintptr_t *) &MixerCommand.DataBufferList[0], RING_NONE_BLOCKING);
        if (err != RingNoError)
        {
            SE_ERROR("Failed to extract from InterMixerFillBufRing (err:%d)\n", err);
            return;
        }
        SE_VERBOSE(group_mixer, "> Extract buf from InterMixerFillBufRing:%p [size:%d]\n",
                   MixerCommand.DataBufferList[0],
                   MixerCommand.DataBufferList[0]->ScatterPages_p[0].Size);
        MixerCommand.DataBufferList[0]->Flags =
            MixerCommand.DataBufferList[0]->ScatterPages_p[0].Page_p ? BUFFER_TYPE_AUDIO_IO : 0;

        MixerCommand.InputParams.InputParam[0].Command = MIXER_PLAY;
        MixerCommand.InputParams.InputParam[0].StartOffset = 0;

        st_relayfs_write_se(ST_RELAY_TYPE_AUDIO_INTERMIXER_PCM, ST_RELAY_SOURCE_SE,
                            (unsigned char *) MixerCommand.DataBufferList[0]->ScatterPages_p[0].Page_p,
                            MixerCommand.DataBufferList[0]->ScatterPages_p[0].Size, false);

        err = InterMixerFillMDRing->Extract((uintptr_t *) &MixerCommand.DataBufferList[MIXER_METADATA_INPUT],
                                            RING_NONE_BLOCKING);
        if (err != RingNoError)
        {
            SE_ERROR("Failed to extract from InterMixerFillMDRing (err:%d)\n", err);
            return;
        }
        MixerCommand.DataBufferList[MIXER_METADATA_INPUT]->Flags =
            MixerCommand.DataBufferList[MIXER_METADATA_INPUT]->ScatterPages_p[0].Page_p ? BUFFER_TYPE_META_DATA_IO : 0;

        tMetadata *pMetadata = (tMetadata *) &MixerCommand.DataBufferList[MIXER_METADATA_INPUT]->ScatterPages_p[0].Page_p;
        if (SE_IS_VERBOSE_ON(group_mixer) && pMetadata != NULL)
        {
            SE_VERBOSE(group_mixer, "MetadataBuffer BytesUsed:%d TotalSize:%d\n",
                       MixerCommand.DataBufferList[MIXER_METADATA_INPUT]->ScatterPages_p[0].BytesUsed,
                       MixerCommand.DataBufferList[MIXER_METADATA_INPUT]->TotalSize);
        }
        st_relayfs_write_se(ST_RELAY_TYPE_AUDIO_INTERMIXER_METADATA, ST_RELAY_SOURCE_SE,
                            (unsigned char *) pMetadata, sizeof(tMetadata), false);
    }
    else
    {
        // This client is not running or a request to force the silence buffer so fill out silence.
        // (STARTING, ...)
        SE_VERBOSE(group_mixer, "> InterMixerFillBufRing is empty => FillOutSilentBuffer\n");
        FillOutSilentBuffer(MixerCommand.Command.DataBuffers_p[0],
                            &MixerCommand.InputParams.InputParam[0]);
    }
}

////////////////////////////////////////////////////////////////////////////
///
/// Populate the data buffer associated with a specific manifestor.
///
void Mixer_Mme_c::FillOutInputBuffer(uint32_t Id, bool ForceSilentBuffer)
{
    PlayerStatus_t Status(PlayerNoError);
    SE_VERBOSE(group_mixer, ">: Client[%d].State: %s\n", Id, Mixer_c::LookupInputState(Client[Id].GetState()));

    if (((Client[Id].GetState() == STARTED) || (Client[Id].GetState() == STOPPING)) && !ForceSilentBuffer)
    {
        bool Muted = (Client[Id].IsMute() == true);
        //
        // Generate a ratio used by the manifestor to work out how many samples are required
        // for normal play and how many samples should be reserved for de-pop.
        //
        // both numbers should be divisible by 25
        // This client is correctly stated, and its parameters are well known.
        int OutputFrequency = MixerSamplingFrequency;

        if (0 == OutputFrequency)
        {
            SE_INFO(group_mixer, "I-MixerSamplingFrequency 0; forcing default\n");
            OutputFrequency = STM_SE_DEFAULT_AUDIO_SAMPLE_RATE_HZ;
        }

        if (PrimaryClient == Id)
        {
            bool BypassSPDIF(false);
            bool BypassHDMI(false);
            bool BypassSDSPDIF(false);
            bool BypassSDHDMI(false);
            bool Bypassed = IsThisClientRequestedToBeBypassed(Id, BypassSPDIF, BypassHDMI, BypassSDSPDIF, BypassSDHDMI);
            SE_VERBOSE(group_mixer, "SPDIF=>Bypass:%s BypassSD:%s HDMI=>Bypass:%s BypassSD:%s\n",
                       BypassSPDIF ? "true" : "false", BypassSDSPDIF ? "true" : "false",
                       BypassHDMI  ? "true" : "false", BypassSDHDMI  ? "true" : "false");

            int ClientSamplingFrequency = Client[Id].GetSourceSurfaceParameter().SampleRateHz;
            if ((Bypassed == true) && (ClientSamplingFrequency != OutputFrequency))
            {
                SE_DEBUG(group_mixer, "Bypass not possible due to mismatch in player frequency[%d] / client frequency[%d] moving back to PCM mode\n",
                         OutputFrequency, ClientSamplingFrequency);
                Bypassed = false;
            }

            if (Bypassed)
            {
                PcmPlayer_c::OutputEncoding OutputEncodingSPDIF(PcmPlayer_c::OUTPUT_IEC60958);
                PcmPlayer_c::OutputEncoding OutputEncodingHDMI(PcmPlayer_c::OUTPUT_IEC60958);

                // Check if bypassed output should be muted
                int softmute;
                if (mSPDIFCardId < STM_SE_MIXER_NB_MAX_OUTPUTS)
                {
                    MixerPlayer[mSPDIFCardId].GetOption(STM_SE_CTRL_AUDIO_SOFTMUTE, softmute);
                    if (softmute)
                    {
                        OutputEncodingSPDIF = PcmPlayer_c::BYPASS_MUTE;
                    }
                }

                if (mHDMICardId < STM_SE_MIXER_NB_MAX_OUTPUTS)
                {
                    MixerPlayer[mHDMICardId].GetOption(STM_SE_CTRL_AUDIO_SOFTMUTE, softmute);
                    if (softmute)
                    {
                        OutputEncodingHDMI = PcmPlayer_c::BYPASS_MUTE;
                    }
                }

                // Primary client, bypass.
                Status = Client[Id].GetManifestor()->FillOutInputBuffer(MixerGranuleSize,
                                                                        OutputFrequency,
                                                                        Client[Id].GetState() == STOPPING,
                                                                        MixerCommand.Command.DataBuffers_p[Id],
                                                                        MixerCommand.Command.DataBuffers_p[MIXER_METADATA_INPUT + Id],
                                                                        &MixerCommand.InputParams.InputParam[Id],
                                                                        Muted,
                                                                        mApplicationType,
                                                                        MixerCommand.Command.DataBuffers_p[MIXER_CODED_DATA_INPUT],
                                                                        &MixerCommand.InputParams.InputParam[MIXER_CODED_DATA_INPUT],
                                                                        BypassSPDIF,
                                                                        BypassHDMI,
                                                                        BypassSDSPDIF,
                                                                        BypassSDHDMI,
                                                                        &OutputEncodingSPDIF,
                                                                        &OutputEncodingHDMI);

                MixerCommand.Command.DataBuffers_p[Id]->Flags = BUFFER_TYPE_AUDIO_IO | Id;
                MixerCommand.Command.DataBuffers_p[MIXER_METADATA_INPUT + Id]->Flags = BUFFER_TYPE_META_DATA_IO | Id;

                if (PlayerNoError == Status)
                {
                    if (OutputEncodingSPDIF != PrimaryCodedDataType[Mixer_Mme_c::MIXER_CODED_DATA_INPUT_SPDIF_IDX])
                    {
                        uint32_t IdxSpdif(MIXER_CODED_DATA_INPUT + MIXER_CODED_DATA_INPUT_SPDIF_IDX);
                        // Output encoding for SPDIF bypass is actually known, update this from mixer sub-system point of view.
                        PrimaryCodedDataType[MIXER_CODED_DATA_INPUT_SPDIF_IDX] = OutputEncodingSPDIF;
                        // encoding has changed, so the sample rate and the number of samples of the
                        // bypassed output may have also...
                        PcmPlayerNeedsParameterUpdate = true;
                        MMENeedsParameterUpdate = true;
                        // deliberately stop the compressed data input, since it is not configured yet as compressed data in the mixer...
                        MixerCommand.InputParams.InputParam[IdxSpdif].Command = MIXER_STOP;
                        MixerCommand.InputParams.InputParam[IdxSpdif].StartOffset = 0;
                    }

                    if (OutputEncodingHDMI != PrimaryCodedDataType[MIXER_CODED_DATA_INPUT_HDMI_IDX])
                    {
                        uint32_t IdxHdmi(MIXER_CODED_DATA_INPUT + MIXER_CODED_DATA_INPUT_HDMI_IDX);
                        // Output encoding for HDMI bypass is actually known, update this from mixer sub-system point of view.
                        PrimaryCodedDataType[MIXER_CODED_DATA_INPUT_HDMI_IDX] = OutputEncodingHDMI;
                        // encoding has changed, so the sample rate and the number of samples of the
                        // bypassed output may have also...
                        PcmPlayerNeedsParameterUpdate = true;
                        MMENeedsParameterUpdate = true;
                        // deliberately stop the compressed data input, since it is not configured yet as compressed data in the mixer...
                        MixerCommand.InputParams.InputParam[IdxHdmi].Command = MIXER_STOP;
                        MixerCommand.InputParams.InputParam[IdxHdmi].StartOffset = 0;
                    }

                    DumpMixerCodedInput();

                    // All is correct, can return now.
                    SE_VERBOSE(group_mixer, "<\n");
                    return;
                }
                else
                {
                    // There was an error.
                    // Stop input for coded data that correspond to the primary mixer input.
                    SetMixerCodedInputStopCommand(false);
                }
                // Let return normally: let fill out silence.
            }
            else
            {
                // Primary client, no bypass.
                Status = Client[Id].GetManifestor()->FillOutInputBuffer(MixerGranuleSize,
                                                                        OutputFrequency,
                                                                        Client[Id].GetState() == STOPPING,
                                                                        MixerCommand.Command.DataBuffers_p[Id],
                                                                        MixerCommand.Command.DataBuffers_p[MIXER_METADATA_INPUT + Id],
                                                                        &MixerCommand.InputParams.InputParam[Id],
                                                                        Muted,
                                                                        mApplicationType,
                                                                        MixerCommand.Command.DataBuffers_p[MIXER_CODED_DATA_INPUT],
                                                                        &MixerCommand.InputParams.InputParam[MIXER_CODED_DATA_INPUT]);

                MixerCommand.Command.DataBuffers_p[Id]->Flags = BUFFER_TYPE_AUDIO_IO | Id;
                MixerCommand.Command.DataBuffers_p[MIXER_METADATA_INPUT + Id]->Flags = BUFFER_TYPE_META_DATA_IO | Id;

                // Stop input for coded data that correspond to the primary mixer input.
                SetMixerCodedInputStopCommand(true);
                DumpMixerCodedInput();

                if (PlayerNoError == Status)
                {
                    // All is correct, can return now.
                    SE_VERBOSE(group_mixer, "<\n");
                    return;
                }

                // Let return normally: let fill out silence.
            }
        }
        else
        {
            if (mApplicationType == STM_SE_CTRL_VALUE_AUDIO_APPLICATION_MS11 ||
                mApplicationType == STM_SE_CTRL_VALUE_AUDIO_APPLICATION_DVD)
            {
                if (Client[PrimaryClient].ManagedByThread() &&
                    (Client[PrimaryClient].GetSourceSurfaceParameter().SampleRateHz != Client[Id].GetSourceSurfaceParameter().SampleRateHz))
                {
                    SE_VERBOSE(group_mixer, "Mixer inputs have different Sampling Frequencies -> MUTE Input:%d\n", Id);
                    Muted = true;
                }
            }
            // This client is NOT the primary one
            Status = Client[Id].GetManifestor()->FillOutInputBuffer(MixerGranuleSize,
                                                                    OutputFrequency,
                                                                    Client[Id].GetState() == STOPPING,
                                                                    MixerCommand.Command.DataBuffers_p[Id],
                                                                    MixerCommand.Command.DataBuffers_p[MIXER_METADATA_INPUT + Id],
                                                                    &MixerCommand.InputParams.InputParam[Id],
                                                                    Muted,
                                                                    mApplicationType);
            MixerCommand.Command.DataBuffers_p[Id]->Flags = BUFFER_TYPE_AUDIO_IO | Id;

            MixerCommand.Command.DataBuffers_p[MIXER_METADATA_INPUT + Id]->Flags = BUFFER_TYPE_META_DATA_IO | Id;

            if (PlayerNoError == Status)
            {
                // All is correct, can return now.
                SE_VERBOSE(group_mixer, "<\n");
                return;
            }
            // Let return normally: let fill out silence.
        }
    }
    else
    {
        // This client is not running or a request to force the silence buffer so fill out silence.
        // (STARTING, ...)
        FillOutSilentBuffer(MixerCommand.Command.DataBuffers_p[Id],
                            &MixerCommand.InputParams.InputParam[Id]);
        MixerCommand.Command.DataBuffers_p[Id]->Flags = BUFFER_TYPE_AUDIO_IO | Id;


        // Should stop also possible coded data being bypassed in case of the primary client.
        if (PrimaryClient == Id)
        {
            SetMixerCodedInputStopCommand(false);
            DumpMixerCodedInput();
        }
        ClearMetadataBuffer(MixerCommand.Command.DataBuffers_p[MIXER_METADATA_INPUT + Id]);
        // Let return normally
    }

    if (PlayerNoError != Status)
    {
        SE_ERROR("Manifestor failed to populate its input buffer\n");
        // error recovery is provided by falling through and filling out a silent buffer
        FillOutSilentBuffer(MixerCommand.Command.DataBuffers_p[Id],
                            &MixerCommand.InputParams.InputParam[Id]);
        MixerCommand.Command.DataBuffers_p[Id]->Flags = BUFFER_TYPE_AUDIO_IO | Id;
        ClearMetadataBuffer(MixerCommand.Command.DataBuffers_p[MIXER_METADATA_INPUT + Id]);
    }

    SE_VERBOSE(group_mixer, "<\n");
}

////////////////////////////////////////////////////////////////////////////
///
/// Populate the data buffer associated with a specific audio generator input.
///
void Mixer_Mme_c::FillOutAudioGenBuffer(uint32_t AudioGeneratorIdx)
{
    MME_DataBuffer_t *DataBuffer = MixerCommand.Command.DataBuffers_p[MIXER_AUDIOGEN_DATA_INPUT + AudioGeneratorIdx];

    if (Generator[AudioGeneratorIdx] &&
        (Generator[AudioGeneratorIdx]->GetState() == STM_SE_AUDIO_GENERATOR_STARTED))
    {
        stm_se_audio_generator_buffer_t audiogenerator_buffer;
        int sampling = MixerSamplingFrequency;

        if (0 == sampling)
        {
            SE_INFO(group_mixer, "O-MixerSamplingFrequency 0; forcing default\n");
            sampling = STM_SE_DEFAULT_AUDIO_SAMPLE_RATE_HZ;
        }

        Rational_t ResamplingFactor(Generator[AudioGeneratorIdx]->GetSamplingFrequency(), sampling);
        Generator[AudioGeneratorIdx]->AudioGeneratorEventUnderflow(MixerGranuleSize, ResamplingFactor);
        DataBuffer->NumberOfScatterPages = 0;
        DataBuffer->TotalSize = 0;
        DataBuffer->StartOffset = 0;
        Generator[AudioGeneratorIdx]->GetCompoundOption(STM_SE_CTRL_AUDIO_GENERATOR_BUFFER,
                                                        &audiogenerator_buffer);
        unsigned char *Buffer         = (unsigned char *) audiogenerator_buffer.audio_buffer;
        uint32_t       BufferSize     = audiogenerator_buffer.audio_buffer_size;
        uint32_t       ReadOffset     = Generator[AudioGeneratorIdx]->GetBufferReadOffset();
        uint32_t       CommittedBytes = Generator[AudioGeneratorIdx]->GetBufferCommittedBytes();

        DataBuffer->TotalSize        = CommittedBytes;
        if (CommittedBytes == 0)
        {
            DataBuffer->NumberOfScatterPages      = 0;
        }
        else
        {
            DataBuffer->ScatterPages_p[0].Page_p  = Buffer + ReadOffset;
            DataBuffer->ScatterPages_p[0].FlagsIn = 0;

            if ((ReadOffset + CommittedBytes) > BufferSize)
            {
                // playable buffer wraps around ==> 2 pages
                uint32_t first_page_sz                = BufferSize - ReadOffset;
                DataBuffer->ScatterPages_p[0].Size    = first_page_sz;

                DataBuffer->ScatterPages_p[1].Page_p  = Buffer;
                DataBuffer->ScatterPages_p[1].FlagsIn = 0;
                DataBuffer->ScatterPages_p[1].Size    = CommittedBytes - first_page_sz;
                DataBuffer->NumberOfScatterPages      = 2;
            }
            else
            {
                DataBuffer->ScatterPages_p[0].Size    = CommittedBytes;
                DataBuffer->NumberOfScatterPages      = 1;
            }
        }

        MixerCommand.InputParams.InputParam[MIXER_AUDIOGEN_DATA_INPUT + AudioGeneratorIdx].Command      = MIXER_PLAY;
        MixerCommand.InputParams.InputParam[MIXER_AUDIOGEN_DATA_INPUT + AudioGeneratorIdx].StartOffset  = 0;
        MixerCommand.InputParams.InputParam[MIXER_AUDIOGEN_DATA_INPUT + AudioGeneratorIdx].PTS          = 0;
        MixerCommand.InputParams.InputParam[MIXER_AUDIOGEN_DATA_INPUT + AudioGeneratorIdx].PTSflags.U32 = 0;
    }
    else
    {
        FillOutSilentBuffer(DataBuffer, &MixerCommand.InputParams.InputParam[MIXER_AUDIOGEN_DATA_INPUT + AudioGeneratorIdx]);
    }

    DataBuffer->Flags = BUFFER_TYPE_AUDIO_GEN_I | AudioGeneratorIdx;
}

////////////////////////////////////////////////////////////////////////////
///
/// Populate the data buffer associated with a specific interactive audio input.
///
void Mixer_Mme_c::FillOutInteractiveAudioBuffer(MME_DataBuffer_t *DataBuffer, int InteractiveAudioIdx)
{
    if (InteractiveAudio[InteractiveAudioIdx] &&
        ((InteractiveAudio[InteractiveAudioIdx])->GetState() == STM_SE_AUDIO_GENERATOR_STARTED))
    {
        stm_se_audio_generator_buffer_t interactiveaudio_buffer;
        DataBuffer->NumberOfScatterPages = 0;
        DataBuffer->TotalSize = 0;
        DataBuffer->StartOffset = 0;
        InteractiveAudio[InteractiveAudioIdx]->GetCompoundOption(STM_SE_CTRL_AUDIO_GENERATOR_BUFFER,
                                                                 &interactiveaudio_buffer);
        uint8_t *Buffer         = static_cast<uint8_t *>(interactiveaudio_buffer.audio_buffer);
        uint32_t BufferSize     = interactiveaudio_buffer.audio_buffer_size;
        uint32_t ReadOffset     = InteractiveAudio[InteractiveAudioIdx]->GetBufferReadOffset();
        uint32_t CommittedBytes = InteractiveAudio[InteractiveAudioIdx]->GetBufferCommittedBytes();

        DataBuffer->TotalSize  = CommittedBytes;
        if (CommittedBytes == 0)
        {
            DataBuffer->NumberOfScatterPages      = 0;
        }
        else
        {
            DataBuffer->ScatterPages_p[0].Page_p  = Buffer + ReadOffset;
            DataBuffer->ScatterPages_p[0].FlagsIn = 0;

            if ((ReadOffset + CommittedBytes) > BufferSize)
            {
                // playable buffer wraps around ==> 2 pages
                uint32_t first_page_sz                = BufferSize - ReadOffset;
                DataBuffer->ScatterPages_p[0].Size    = first_page_sz;

                DataBuffer->ScatterPages_p[1].Page_p  = Buffer;
                DataBuffer->ScatterPages_p[1].FlagsIn = 0;
                DataBuffer->ScatterPages_p[1].Size    = CommittedBytes - first_page_sz;
                DataBuffer->NumberOfScatterPages      = 2;
            }
            else
            {
                DataBuffer->ScatterPages_p[0].Size    = CommittedBytes;
                DataBuffer->NumberOfScatterPages      = 1;
            }
        }

        MixerCommand.InputParams.InputParam[MIXER_INTERACTIVE_INPUT + InteractiveAudioIdx].Command = MIXER_PLAY;
        MixerCommand.InputParams.InputParam[MIXER_INTERACTIVE_INPUT + InteractiveAudioIdx].StartOffset = 0;
        MixerCommand.InputParams.InputParam[MIXER_INTERACTIVE_INPUT + InteractiveAudioIdx].PTS = 0;
        MixerCommand.InputParams.InputParam[MIXER_INTERACTIVE_INPUT + InteractiveAudioIdx].PTSflags.U32 = 0;


        DataBuffer->Flags = (InteractiveAudio[InteractiveAudioIdx]->GetNumChannels() == 1 ?
                             BUFFER_TYPE_IAUDIO_MONO_I :
                             BUFFER_TYPE_IAUDIO_STEREO_I);
    }

    else
    {
        FillOutSilentBuffer(DataBuffer, &MixerCommand.InputParams.InputParam[MIXER_INTERACTIVE_INPUT + InteractiveAudioIdx]);
        DataBuffer->Flags = BUFFER_TYPE_IAUDIO_MONO_I;
    }
    DataBuffer->Flags |= InteractiveAudioIdx;
}
////////////////////////////////////////////////////////////////////////////
///
/// Populate a data buffer such that it will be zero length (and therefore
/// silent).
///
/// If an mixer command structure is provided this will be configured to
/// stop the associated mixer output.
///
void Mixer_Mme_c::FillOutSilentBuffer(MME_DataBuffer_t *DataBuffer,
                                      tMixerFrameParams *MixerFrameParams)
{
    DataBuffer->NumberOfScatterPages = 0;
    DataBuffer->TotalSize = 0;
    DataBuffer->StartOffset = 0;
    DataBuffer->Flags = BUFFER_TYPE_AUDIO_IO;
    // zero-length, null pointer, no flags
    memset(&DataBuffer->ScatterPages_p[0], 0, sizeof(DataBuffer->ScatterPages_p[0]));

    if (MixerFrameParams)
    {
        MixerFrameParams->Command = MIXER_STOP;
        MixerFrameParams->StartOffset = 0;
        MixerFrameParams->PTS = 0;
        MixerFrameParams->PTSflags.U32 = 0;
    }
}

void Mixer_Mme_c::ClearMetadataBuffer(MME_DataBuffer_t *DataBuffer)
{
    DataBuffer->NumberOfScatterPages = 0;
    DataBuffer->TotalSize = 0;
    DataBuffer->StartOffset = 0;
    DataBuffer->Flags =  BUFFER_TYPE_META_DATA_IO;
    // zero-length, null pointer, no flags
    memset(&DataBuffer->ScatterPages_p[0], 0, sizeof(DataBuffer->ScatterPages_p[0]));
}

////////////////////////////////////////////////////////////////////////////
///
///
///
PlayerStatus_t Mixer_Mme_c::UpdateFirstMixerOutputBuffers()
{
    unsigned int OutBufIdx = FirstMixerCommand.Command.NumberInputBuffers;
    RingStatus_t rStatus;

    MME_DataBuffer_t *MMEBuf = FirstMixerCommand.DataBufferList[OutBufIdx++];
    SE_VERBOSE(group_mixer, ">%s insert MMEBuf:%p in InterMixerFillBufRing\n", Name, MMEBuf);

    MMEBuf->ScatterPages_p[0].Size = MMEBuf->ScatterPages_p[0].BytesUsed;
    MMEBuf->ScatterPages_p[0].BytesUsed = 0;

    rStatus = InterMixerFillBufRing->Insert((uintptr_t) MMEBuf);
    if (rStatus != RingNoError)
    {
        SE_ERROR("Cannot insert MMEBuf:%p in InterMixerFillBufRing\n", MMEBuf);
        return PlayerError;
    }

    /* Store associated Metadata Buffer */
    MMEBuf = FirstMixerCommand.DataBufferList[OutBufIdx++];

    MMEBuf->ScatterPages_p[0].Size = MMEBuf->ScatterPages_p[0].BytesUsed;
    MMEBuf->ScatterPages_p[0].BytesUsed = 0;

    rStatus = InterMixerFillMDRing->Insert((uintptr_t) MMEBuf);
    if (rStatus != RingNoError)
    {
        SE_ERROR("Cannot insert MMEBuf:%p in InterMixerFillMDRing\n", MMEBuf);
        return PlayerError;
    }

    return PlayerNoError;
}

////////////////////////////////////////////////////////////////////////////
///
///
///
PlayerStatus_t Mixer_Mme_c::UpdateOutputBuffers()
{
    PlayerStatus_t Status;
    SE_VERBOSE(group_mixer, ">%s\n", Name);
    Status = CommitPcmPlayersMappedSamples();

    if (PlayerNoError != Status)
    {
        SE_ERROR("Failed to commit\n");
        return Status;
    }

    DumpPcmPlayersCommitedSamples();

    for (uint32_t CountOfAudioPlayerAttached(0), PlayerIdx(0); CountOfAudioPlayerAttached < NumberOfAudioPlayerAttached; PlayerIdx++)
    {
        if (true == MixerPlayer[PlayerIdx].IsPlayerObjectAttached())
        {
            CountOfAudioPlayerAttached++;
            MME_DataBuffer_t *OutputDataBuffer = MixerCommand.Command.DataBuffers_p[MixerCommand.Command.NumberInputBuffers + PlayerIdx];
            OutputDataBuffer->TotalSize = 0;
            SE_VERBOSE(group_mixer, "@: %s OutputDataBuffer: %p ->TotalSize=%u\n", MixerPlayer[PlayerIdx].GetCardName(), OutputDataBuffer, OutputDataBuffer->TotalSize);
        }
    }

    return PlayerNoError;
}


////////////////////////////////////////////////////////////////////////////
///
/// Update input buffer for all Clients.
/// Set Client's ReleaseProcessingDecodedBuffer when requested.
///
void Mixer_Mme_c::UpdateInputBuffer(uint32_t Id, bool ReleaseProcessingBuffer)
{
    MME_DataBuffer_t       *PcmBuffer, *CodedBuffer, *MDBuffer;
    MME_MixerInputStatus_t *PcmStatus, *CodedStatus;

    Client[Id].LockTake();

    /* In case of Dual Mixer setup:
     * Client[0] & Client[1] are connected to 1st Mixer
     * Client[2] & Client[3] are connected to 2nd Mixer
     */
    if (MMEFirstMixerHdl != MME_INVALID_ARGUMENT && Id < FIRST_MIXER_MAX_CLIENTS)
    {
        PcmBuffer   =  FirstMixerCommand.Command.DataBuffers_p[Id];
        MDBuffer    =  FirstMixerCommand.Command.DataBuffers_p[FIRST_MIXER_MAX_CLIENTS + Id];
        CodedBuffer =  NULL;
        PcmStatus   = &FirstMixerCommand.OutputParams.MixStatus.InputStreamStatus[Id];
        CodedStatus =  NULL;
    }
    else
    {
        PcmBuffer   =  MixerCommand.Command.DataBuffers_p[Id];
        MDBuffer    =  MixerCommand.Command.DataBuffers_p[MIXER_METADATA_INPUT + Id];
        CodedBuffer =  MixerCommand.Command.DataBuffers_p[MIXER_CODED_DATA_INPUT];
        PcmStatus   = &MixerCommand.OutputParams.MixStatus.InputStreamStatus[Id];
        CodedStatus = &MixerCommand.OutputParams.MixStatus.InputStreamStatus[MIXER_CODED_DATA_INPUT];
    }

    if ((Client[Id].GetState() == STARTED) || (Client[Id].GetState() == STOPPING))
    {
        PlayerStatus_t Status;
        Status = Client[Id].GetManifestor()->UpdateInputBuffer(PcmBuffer, MDBuffer, PcmStatus, CodedBuffer, CodedStatus, ReleaseProcessingBuffer);
        if (Status != ManifestorNoError && Status != ManifestorNullQueued)
        {
            SE_ERROR("Failed to update the input buffer\n");
            // no real error recovery possible
        }
    }

    Client[Id].LockRelease();
}

////////////////////////////////////////////////////////////////////////////
///
///
///
void Mixer_Mme_c::UpdateSecondMixerInputBuffer()
{
    MME_DataBuffer_t *MMEBuf = MixerCommand.DataBufferList[0];
    if (MMEBuf->NumberOfScatterPages != 0)
    {
        RingStatus_t err = InterMixerFreeBufRing->Insert((uintptr_t) MixerCommand.DataBufferList[0]);
        if (err != RingNoError)
        {
            SE_ERROR("Failed to insert into InterMixerFreeBufRing (err:%d)\n", err);
            return;
        }
        err = InterMixerFreeMDRing->Insert((uintptr_t) MixerCommand.DataBufferList[MIXER_METADATA_INPUT]);
        if (err != RingNoError)
        {
            SE_ERROR("Failed to insert into InterMixerFreeMDRing (err:%d)\n", err);
            return;
        }
    }
}

////////////////////////////////////////////////////////////////////////////
///
///
///
void Mixer_Mme_c::CascadeADMixerStatusParamsToSystemMixer()
{
    MixerCommand.InputParams.InputParam[0].PTSflags =  FirstMixerCommand.OutputParams.MixStatus.PTSflag;
    MixerCommand.InputParams.InputParam[0].PTS      =  FirstMixerCommand.OutputParams.MixStatus.PTS;
}

////////////////////////////////////////////////////////////////////////////
///
///
///
void Mixer_Mme_c::UpdateAudioGenBuffer(uint32_t AudioGenId)
{
    if (Generator[AudioGenId] &&
        (Generator[AudioGenId]->GetState() == STM_SE_AUDIO_GENERATOR_STARTED))
    {
        uint32_t BytesConsumed;
        BytesConsumed = MixerCommand.OutputParams.MixStatus.InputStreamStatus[MIXER_AUDIOGEN_DATA_INPUT + AudioGenId].BytesUsed;
        Generator[AudioGenId]->SamplesConsumed(BytesConsumed / Generator[AudioGenId]->GetBytesPerSample());
    }
}

////////////////////////////////////////////////////////////////////////////
///
///
///
void Mixer_Mme_c::UpdateInteractiveAudioBuffer(uint32_t InteractiveAudioId)
{
    if (InteractiveAudio[InteractiveAudioId] &&
        (InteractiveAudio[InteractiveAudioId]->GetState() == STM_SE_AUDIO_GENERATOR_STARTED))
    {
        uint32_t SamplesConsumed;
        SamplesConsumed = MixerCommand.OutputParams.MixStatus.InputStreamStatus[MIXER_INTERACTIVE_INPUT].BytesUsed / (8 * 4);
        InteractiveAudio[InteractiveAudioId]->SamplesConsumed(SamplesConsumed);
    }
}

////////////////////////////////////////////////////////////////////////////
///
/// Check for any existing mixing metadata from the secondary stream, and
/// eventually update them
///
void Mixer_Mme_c::UpdateMixingMetadata()
{
    // Client Lock already taken by caller.
    if (Client[PrimaryClient].ManagedByThread() && Client[SecondaryClient].ManagedByThread())
    {
        // These clients are correctly stated, and their parameters are well known.
        // Read the code carefully here. We use lots of x = y = z statements.
        MixingMetadata_t SecondaryClientMixingMetadata = Client[SecondaryClient].GetMixingMetadataParameter();
        int PrimaryClientAudioMode = Client[PrimaryClient].GetOrganisationParameter();

        if ((SecondaryClientMixingMetadata.IsMixingMetadataPresent) &&
            (MixerSettings.OutputConfiguration.metadata_update != SND_PSEUDO_MIXER_METADATA_UPDATE_NEVER))
        {
            MixingMetadata_t *StreamMetadata = &SecondaryClientMixingMetadata;
            SE_DEBUG(group_mixer, "Mixing metadata found (mix out configs: %d)!\n", StreamMetadata->NbOutMixConfig);
            // It is impossible for UpstreamConfiguration to be non-NULL. This is so because the reset value
            // of OutputConfiguration.metadata_update makes this code unreachable. The only means by which we
            // could end up executing this branch would also set UpstreamConfiguration to a non-NULL value.
            SE_ASSERT(UpstreamConfiguration);

            if (StreamMetadata->ADPESMetaData.ADInfoAvailable)
            {
                unsigned short  PanCoeff[8];
                unsigned char Panbyte = SecondaryClientMixingMetadata.ADPESMetaData.ADPanValue;
                memset(PanCoeff, 0 , sizeof(PanCoeff));
                StreamMetadata->MixOutConfig[0].AudioMode = PrimaryClientAudioMode;
                StreamMetadata->NbOutMixConfig = 1;
                enum eAccAcMode AudioMode = (enum eAccAcMode)PrimaryClientAudioMode;

                unsigned short Gain = Fade_Mapping[(SecondaryClientMixingMetadata.ADPESMetaData.ADFadeValue) & 0xFF];

                switch (AudioMode)
                {
                case ACC_MODE20t:
                case ACC_MODE20:
                    Gen2chPanCoef(Panbyte, PanCoeff);
                    //Applying Fade
                    StreamMetadata->MixOutConfig[0].PrimaryAudioGain[ACC_MAIN_LEFT] = Gain;
                    StreamMetadata->MixOutConfig[0].PrimaryAudioGain[ACC_MAIN_RGHT] = Gain;
                    //Applying pan
                    StreamMetadata->MixOutConfig[0].SecondaryAudioPanCoeff[ACC_MAIN_LEFT] = PanCoeff[ACC_MAIN_LEFT];
                    StreamMetadata->MixOutConfig[0].SecondaryAudioPanCoeff[ACC_MAIN_RGHT] = PanCoeff[ACC_MAIN_RGHT];
                    break;

                case  ACC_MODE30:
                    Gen4chPanCoef(Panbyte, PanCoeff);
                    //Applying Fade
                    StreamMetadata->MixOutConfig[0].PrimaryAudioGain[ACC_MAIN_LEFT] = Gain;
                    StreamMetadata->MixOutConfig[0].PrimaryAudioGain[ACC_MAIN_RGHT] = Gain;
                    StreamMetadata->MixOutConfig[0].PrimaryAudioGain[ACC_MAIN_CNTR] = Gain;
                    //Applying pan
                    StreamMetadata->MixOutConfig[0].SecondaryAudioPanCoeff[ACC_MAIN_LEFT] = PanCoeff[ACC_MAIN_LEFT];
                    StreamMetadata->MixOutConfig[0].SecondaryAudioPanCoeff[ACC_MAIN_RGHT] = PanCoeff[ACC_MAIN_RGHT];
                    StreamMetadata->MixOutConfig[0].SecondaryAudioPanCoeff[ACC_MAIN_CNTR] = PanCoeff[ACC_MAIN_CNTR];
                    break;

                case  ACC_MODE32_LFE:
                    Gen6chPanCoef(Panbyte, PanCoeff);
                    //Applying Fade
                    StreamMetadata->MixOutConfig[0].PrimaryAudioGain[ACC_MAIN_LEFT] = Gain;
                    StreamMetadata->MixOutConfig[0].PrimaryAudioGain[ACC_MAIN_RGHT] = Gain;
                    StreamMetadata->MixOutConfig[0].PrimaryAudioGain[ACC_MAIN_CNTR] = Gain;
                    StreamMetadata->MixOutConfig[0].PrimaryAudioGain[ACC_MAIN_LSUR] = Gain;
                    StreamMetadata->MixOutConfig[0].PrimaryAudioGain[ACC_MAIN_RSUR] = Gain;

                    //Applying pan
                    StreamMetadata->MixOutConfig[0].SecondaryAudioPanCoeff[ACC_MAIN_LEFT] = PanCoeff[ACC_MAIN_LEFT];
                    StreamMetadata->MixOutConfig[0].SecondaryAudioPanCoeff[ACC_MAIN_RGHT] = PanCoeff[ACC_MAIN_RGHT];
                    StreamMetadata->MixOutConfig[0].SecondaryAudioPanCoeff[ACC_MAIN_CNTR] = PanCoeff[ACC_MAIN_CNTR];
                    StreamMetadata->MixOutConfig[0].SecondaryAudioPanCoeff[ACC_MAIN_LSUR] = PanCoeff[ACC_MAIN_LSUR];
                    StreamMetadata->MixOutConfig[0].SecondaryAudioPanCoeff[ACC_MAIN_RSUR] = PanCoeff[ACC_MAIN_RSUR];
                    break;

                default:
                    SE_INFO(group_mixer, "Audio Mode %u not Supported\n", AudioMode);
                    break;
                }
            }

            SE_DEBUG(group_mixer, "WARNING - Selecting first PrimaryAudioGain (which may not be correct)\n");

            for (uint32_t ChannelIdx(0); ChannelIdx < SND_PSEUDO_MIXER_CHANNELS; ChannelIdx++)
            {
                MixerSettings.MixerOptions.Gain[PrimaryClient][ChannelIdx] = StreamMetadata->MixOutConfig[0].PrimaryAudioGain[ChannelIdx];
                // Update the flag
                MixerSettings.MixerOptions.GainUpdated = true;
            }

            // To pan the secondary stream it is necessary that this stream is mono....
            if (Client[SecondaryClient].GetOrganisationParameter() == ACC_MODE10)
            {
                for (uint32_t MixConfig(0); MixConfig < StreamMetadata->NbOutMixConfig; MixConfig++)
                {
                    // check if this mix config is the same as the primary stream one,
                    // if so, the secondary panning coef metadata will be applied
                    if (StreamMetadata->MixOutConfig[MixConfig].AudioMode == PrimaryClientAudioMode)
                    {
                        for (uint32_t ChannelIdx(0); ChannelIdx < SND_PSEUDO_MIXER_CHANNELS; ChannelIdx++)
                        {
                            MixerSettings.MixerOptions.Gain[SecondaryClient][ChannelIdx] = Q3_13_UNITY;
                            MixerSettings.MixerOptions.Pan[SecondaryClient][ChannelIdx] = StreamMetadata->MixOutConfig[MixConfig].SecondaryAudioPanCoeff[ChannelIdx];
                            // Update the flag
                            MixerSettings.MixerOptions.GainUpdated = true;
                            MixerSettings.MixerOptions.PanUpdated = true;
                        }
                    }
                }
            }

            if (MixerSettings.OutputConfiguration.metadata_update == SND_PSEUDO_MIXER_METADATA_UPDATE_ALWAYS)
            {
                MixerSettings.MixerOptions.PostMixGain = StreamMetadata->PostMixGain;
                // Update the flag
                MixerSettings.MixerOptions.PostMixGainUpdated = true;
            }
        }
    }
}

////////////////////////////////////////////////////////////////////////////
///
/// Lookup a discrete sampling frequency and convert it to a string.
///
const char *Mixer_Mme_c::LookupDiscreteSamplingFrequency(enum eAccFsCode DiscreteFrequency)
{
    switch (DiscreteFrequency)
    {
#define E(x) case x: return #x
        E(ACC_FS48k);
        E(ACC_FS44k);
        E(ACC_FS32k);
        E(ACC_FS_reserved_48K);
        /* Range : 2^1  */
        E(ACC_FS96k);
        E(ACC_FS88k);
        E(ACC_FS64k);
        E(ACC_FS_reserved_96K);
        /* Range : 2^2  */
        E(ACC_FS192k);
        E(ACC_FS176k);
        E(ACC_FS128k);
        E(ACC_FS_reserved_192K);
        /* Range : 2^3 */
        E(ACC_FS384k);
        E(ACC_FS352k);
        E(ACC_FS256k);
        E(ACC_FS_reserved_384K);
        /* Range : 2^-2 */
        E(ACC_FS12k);
        E(ACC_FS11k);
        E(ACC_FS8k);
        E(ACC_FS_reserved_12K);
        /* Range : 2^-1 */
        E(ACC_FS24k);
        E(ACC_FS22k);
        E(ACC_FS16k);
        E(ACC_FS_reserved_24K);
        /* Range : 2^-1 */
        E(ACC_FS768k);
        E(ACC_FS705k);
        E(ACC_FS512k);
        E(ACC_FS_reserved_768K);
        /* Range : 2^-3 */
        E(ACC_FS6k);
        E(ACC_FS5k);
        E(ACC_FS4k);
        E(ACC_FS_reserved_6K);
        E(ACC_FS_reserved);  // Undefined
        E(ACC_FS_ID);        // Used by Mixer : if FS_ID then OutSFreq = InSFreq
#undef E

    default:

        /* AUDIOMIXER_OVERRIDE_OUTFS is not a component of enum eAccFsCode */
        if (DiscreteFrequency == AUDIOMIXER_OVERRIDE_OUTFS)
        {
            return "AUDIOMIXER_OVERRIDE_OUTFS";
        }

        return "INVALID";
    }
}

// For verbosity in case of debug.
const char *Mixer_Mme_c::LookupAudioOriginalEncoding(AudioOriginalEncoding_t AudioOriginalEncoding)
{
    switch (AudioOriginalEncoding)
    {
#define E(x) case x: return #x
        E(AudioOriginalEncodingUnknown);
        E(AudioOriginalEncodingAc3);
        E(AudioOriginalEncodingDdplus);
        E(AudioOriginalEncodingDts);
        E(AudioOriginalEncodingDtshd);
        E(AudioOriginalEncodingDtshdMA);
        E(AudioOriginalEncodingDtshdLBR);
        E(AudioOriginalEncodingTrueHD);
        E(AudioOriginalEncodingAAC);
        E(AudioOriginalEncodingHEAAC_960);
        E(AudioOriginalEncodingHEAAC_1024);
        E(AudioOriginalEncodingHEAAC_1920);
        E(AudioOriginalEncodingHEAAC_2048);
        E(AudioOriginalEncodingDPulse);
        E(AudioOriginalEncodingSPDIFIn_Compressed);
        E(AudioOriginalEncodingSPDIFIn_Pcm);
        E(AudioOriginalEncodingMax);
#undef E

    default:
        return "INVALID";
    }
}


uint32_t Mixer_Mme_c::LookupSpdifPreamblePc(PcmPlayer_c::OutputEncoding Encoding)
{
#if 0
    Value   Corresponding frame
    0     NULL data
    1     Ac3(1536 samples)
    3     Pause burst
    4     MPEG - 1 layer - 1
    5     MPEG - 1 layer - 2 or - 3 data or MPEG - 2 without extension
    6     MPEG - 2 data with extension
    7     MPEG - 2 AAC
    8     MPEG - 2, layer - 1 low sampling frequency
    9     MPEG - 2, layer - 2 low sampling frequency
    10     MPEG - 2, layer - 3 low sampling frequency
    11     DTS type I(512 samples)
    12     DTS type II(1024 samples)
    13     DTS type III(2048 samples)
    14     ATRAC(512 samples)
    15     ATRAC 2 / 3(1 024 samples)
    17     DTS Type 4
    21     DDPLUS
    22     Dolby TrueHD
    23     AAC / HE - AAC
#endif

    switch (Encoding)
    {
    case PcmPlayer_c::OUTPUT_AC3:
    case PcmPlayer_c::BYPASS_AC3:
        return 1;

    case PcmPlayer_c::BYPASS_AAC:
        return 7;

    case PcmPlayer_c::OUTPUT_DTS:
    case PcmPlayer_c::BYPASS_DTS_512:
        return 11;

    case PcmPlayer_c::BYPASS_DTS_1024:
        return 12;

    case PcmPlayer_c::BYPASS_DTS_2048:
        return 13;

    case PcmPlayer_c::BYPASS_DTSHD_HR:
        return (17 + MIXER_DTSHD_PC_REP_PERIOD_2048);

    case PcmPlayer_c::BYPASS_DTSHD_LBR:
    case PcmPlayer_c::BYPASS_DTSHD_DTS_4096:
        return (17 + MIXER_DTSHD_PC_REP_PERIOD_4096);

    case PcmPlayer_c::BYPASS_DTSHD_MA:
    case PcmPlayer_c::BYPASS_DTSHD_DTS_8192:
        return (17 + MIXER_DTSHD_PC_REP_PERIOD_8192);

    case PcmPlayer_c::OUTPUT_DDPLUS:
    case PcmPlayer_c::BYPASS_DDPLUS:
        return 21;

    case PcmPlayer_c::BYPASS_TRUEHD:
        return 22;

    case PcmPlayer_c::BYPASS_HEAAC_960:
        return (23 + MIXER_HEAAC_PC_REP_PERIOD_960);

    case PcmPlayer_c::BYPASS_HEAAC_1024:
        return (23 + MIXER_HEAAC_PC_REP_PERIOD_1024);

    case PcmPlayer_c::BYPASS_HEAAC_1920:
        return (23 + MIXER_HEAAC_PC_REP_PERIOD_1920);

    case PcmPlayer_c::BYPASS_HEAAC_2048:
        return (23 + MIXER_HEAAC_PC_REP_PERIOD_2048);

    default:
        return 0;
    }
}

uint32_t Mixer_Mme_c::LookupRepetitionPeriod(PcmPlayer_c::OutputEncoding Encoding)
{
    switch (Encoding)
    {
    case PcmPlayer_c::OUTPUT_AC3:
    case PcmPlayer_c::BYPASS_AC3:
        return 1536;

    case PcmPlayer_c::OUTPUT_DTS:
    case PcmPlayer_c::BYPASS_DTS_512:
        return 512;

    case PcmPlayer_c::BYPASS_DTS_1024:
        return 1024;

    case PcmPlayer_c::BYPASS_DTS_2048:
        return 2048;

    case PcmPlayer_c::BYPASS_DTSHD_HR:
        return 2048;

    case PcmPlayer_c::BYPASS_DTSHD_LBR:
    case PcmPlayer_c::BYPASS_DTSHD_DTS_4096:
        return 4096;

    case PcmPlayer_c::BYPASS_DTSHD_MA:
    case PcmPlayer_c::BYPASS_DTSHD_DTS_8192:
        return 8192;

    case PcmPlayer_c::OUTPUT_DDPLUS:
    case PcmPlayer_c::BYPASS_DDPLUS:
        return 1536 * 4;

    case PcmPlayer_c::BYPASS_TRUEHD:
        return 10 * 1536;

    case PcmPlayer_c::BYPASS_AAC:
        return 1024;

    case PcmPlayer_c::BYPASS_HEAAC_960:
        return 960;

    case PcmPlayer_c::BYPASS_HEAAC_1024:
        return 1024;

    case PcmPlayer_c::BYPASS_HEAAC_1920:
        return 1920;

    case PcmPlayer_c::BYPASS_HEAAC_2048:
        return 2048;

    default:
        return 1;
    }
}

uint32_t Mixer_Mme_c::LookupIec60958FrameRateForBypass(PcmPlayer_c::OutputEncoding Encoding) const
{
    AudioSurfaceParameters_t PrimaryClientSurfaceParam;
    AudioOriginalEncoding_t OriginalEncoding(AudioOriginalEncodingUnknown);
    uint32_t OriginalSamplingFreq;
    unsigned int SampleCount;

    SE_ASSERT(PcmPlayer_c::IsOutputBypassed(Encoding));

    // Client Lock already taken by caller.
    if (Client[PrimaryClient].ManagedByThread())
    {
        // This client is correctly stated, and its parameters are well known.
        PrimaryClientSurfaceParam = Client[PrimaryClient].GetSourceSurfaceParameter();
        OriginalEncoding          = Client[PrimaryClient].GetOriginalEncodingParameter();
        OriginalSamplingFreq      = Client[PrimaryClient].GetBackwardCompParameter().SampleRateHz;
        SampleCount               = Client[PrimaryClient].GetSampleCountParameter();

        if (0 == OriginalSamplingFreq)
        {
            OriginalSamplingFreq = PrimaryClientSurfaceParam.SampleRateHz;
        }
        if ((Encoding == PcmPlayer_c::BYPASS_SPDIFIN_COMPRESSED) || (Encoding == PcmPlayer_c::BYPASS_SPDIFIN_PCM) || (Encoding == PcmPlayer_c::BYPASS_SPDIFIN_COMPRESSED_SD))
        {
            uint32_t SpdifInSamplingFrequency = Client[PrimaryClient].GetSpdifInProperties().SamplingFrequency;
            if ((Encoding == PcmPlayer_c::BYPASS_SPDIFIN_COMPRESSED_SD) && (Client[PrimaryClient].GetSpdifInProperties().SpdifInStreamType == SPDIF_DDPLUS))
            {
                SpdifInSamplingFrequency /= 4;
            }
            SE_DEBUG(group_mixer, "SPDIFin SamplingFrequency %u\n", SpdifInSamplingFrequency);
            return (SpdifInSamplingFrequency == 0) ? STM_SE_DEFAULT_AUDIO_SAMPLE_RATE_HZ : SpdifInSamplingFrequency;
        }
    }
    else
    {
        SE_DEBUG(group_mixer, "<%s Forcing Default\n", Name);
        // The client is de-registered, so return arbitrary value.
        return STM_SE_DEFAULT_AUDIO_SAMPLE_RATE_HZ;
    }

    SE_DEBUG(group_mixer, "@%s %s %u %s\n",
             Name,
             LookupAudioOriginalEncoding(OriginalEncoding),
             OriginalSamplingFreq,
             PcmPlayer_c::LookupOutputEncoding(Encoding));

    uint32_t Iec60958FrameRate = OriginalSamplingFreq;

    if (Encoding == PcmPlayer_c::BYPASS_DDPLUS)
    {
        Iec60958FrameRate = OriginalSamplingFreq * 4;
    }

    else if ((PcmPlayer_c::BYPASS_AC3 == Encoding) && (AudioOriginalEncodingDPulse == OriginalEncoding))
    {
        // bug29797: in case of DolbyPulse transcoded as AC3 is 48 kHz.
        // Take into account what was forced by STM_SE_FIXED_OUTPUT_FREQUENCY control
        SE_INFO(group_mixer, "%s %s FORCING Iec60958FrameRate to %d\n", Name, LookupAudioOriginalEncoding(OriginalEncoding), NominalOutputSamplingFrequency);
        Iec60958FrameRate = NominalOutputSamplingFrequency;
    }
    else if (Encoding <= PcmPlayer_c::BYPASS_DTSHD_LBR)   // C.FENARD: To be reworked cause can have some side effects ...
    {
        Iec60958FrameRate = OriginalSamplingFreq;
    }
    else if (Encoding <= PcmPlayer_c::BYPASS_DTSHD_MA)  // C.FENARD: To be reworked cause can have some side effects ...
    {
        Iec60958FrameRate = (PrimaryClientSurfaceParam.SampleRateHz * LookupRepetitionPeriod(Encoding)) / SampleCount;
    }
    else if (Encoding == PcmPlayer_c::BYPASS_TRUEHD)
    {
        Iec60958FrameRate = 16 * OriginalSamplingFreq;
        // Limit to 768khz because above it not supported by hdmi
        while (Iec60958FrameRate > 768000)
        {
            Iec60958FrameRate >>= 1;
        }
    }

    SE_DEBUG(group_mixer, "<%s %d\n", Name, Iec60958FrameRate);
    return Iec60958FrameRate;
}

////////////////////////////////////////////////////////////////////////////
///
/// Lookup the most appropriate ACC_MODE for the primary output.
///
enum eAccAcMode Mixer_Mme_c::TranslateDownstreamCardToMainAudioMode(const struct snd_pseudo_mixer_downstream_card &DownstreamCard,
                                                                    PcmPlayer_c::OutputEncoding Encoding)
{
    struct stm_se_audio_channel_assignment ChannelAssignment = DownstreamCard.channel_assignment;

    if (Encoding >= PcmPlayer_c::ENCODED_LOWEST)
    {
        /* This player is either re-encoded or Bypassed => ignore card NumberOfChannels */
        return TranslateChannelAssignmentToAudioMode(ChannelAssignment);
    }

    // Disconnect any pair that is deselected by the number of channels
    switch (DownstreamCard.num_channels)
    {
    case 1:
    case 2:
        ChannelAssignment.pair1 = STM_SE_AUDIO_CHANNEL_PAIR_NOT_CONNECTED;

    //FALLTHRU
    case 3:
    case 4:
        ChannelAssignment.pair2 = STM_SE_AUDIO_CHANNEL_PAIR_NOT_CONNECTED;

    //FALLTHRU
    case 5:
    case 6:
        ChannelAssignment.pair3 = STM_SE_AUDIO_CHANNEL_PAIR_NOT_CONNECTED;

    //FALLTHRU
    default:
        // pair4 is unconditionally disconnected because it is only used for auxilliary output
        ChannelAssignment.pair4 = STM_SE_AUDIO_CHANNEL_PAIR_NOT_CONNECTED;
    }

    return TranslateChannelAssignmentToAudioMode(ChannelAssignment);
}

////////////////////////////////////////////////////////////////////////////
///
/// Verify if the downmix bypass is desired
///
bool Mixer_Mme_c::CheckDownmixBypass(const struct snd_pseudo_mixer_downstream_card &DownstreamCard, PcmPlayer_c::OutputEncoding Encoding)
{
    struct stm_se_audio_channel_assignment ChannelAssignment = DownstreamCard.channel_assignment;
    bool bypass = false;

    if (Encoding >= PcmPlayer_c::ENCODED_LOWEST)
    {
        if ((ChannelAssignment.pair0 == STM_SE_AUDIO_CHANNEL_PAIR_PAIR0 || ChannelAssignment.pair0 == STM_SE_AUDIO_CHANNEL_PAIR_NOT_CONNECTED) &&
            (ChannelAssignment.pair1 == STM_SE_AUDIO_CHANNEL_PAIR_PAIR1 || ChannelAssignment.pair1 == STM_SE_AUDIO_CHANNEL_PAIR_NOT_CONNECTED) &&
            (ChannelAssignment.pair2 == STM_SE_AUDIO_CHANNEL_PAIR_PAIR2 || ChannelAssignment.pair2 == STM_SE_AUDIO_CHANNEL_PAIR_NOT_CONNECTED) &&
            (ChannelAssignment.pair3 == STM_SE_AUDIO_CHANNEL_PAIR_PAIR3 || ChannelAssignment.pair3 == STM_SE_AUDIO_CHANNEL_PAIR_NOT_CONNECTED))
        {
            bypass = true;
        }
    }

    switch (DownstreamCard.num_channels)
    {
    case 1:
    case 2:
        if (ChannelAssignment.pair0 == STM_SE_AUDIO_CHANNEL_PAIR_PAIR0 &&
            ChannelAssignment.pair1 == STM_SE_AUDIO_CHANNEL_PAIR_NOT_CONNECTED &&
            ChannelAssignment.pair2 == STM_SE_AUDIO_CHANNEL_PAIR_NOT_CONNECTED &&
            ChannelAssignment.pair3 == STM_SE_AUDIO_CHANNEL_PAIR_NOT_CONNECTED)
        {
            bypass = true;
        }

        break;

    case 3:
    case 4:
        if (ChannelAssignment.pair0 == STM_SE_AUDIO_CHANNEL_PAIR_PAIR0 &&
            ChannelAssignment.pair1 == STM_SE_AUDIO_CHANNEL_PAIR_PAIR1 &&
            ChannelAssignment.pair2 == STM_SE_AUDIO_CHANNEL_PAIR_NOT_CONNECTED &&
            ChannelAssignment.pair3 == STM_SE_AUDIO_CHANNEL_PAIR_NOT_CONNECTED)
        {
            bypass = true;
        }

        break;

    case 5:
    case 6:
        if (ChannelAssignment.pair0 == STM_SE_AUDIO_CHANNEL_PAIR_PAIR0 &&
            ChannelAssignment.pair1 == STM_SE_AUDIO_CHANNEL_PAIR_PAIR1 &&
            ChannelAssignment.pair2 == STM_SE_AUDIO_CHANNEL_PAIR_PAIR2 &&
            ChannelAssignment.pair3 == STM_SE_AUDIO_CHANNEL_PAIR_NOT_CONNECTED)
        {
            bypass = true;
        }

        break;

    default:
        if (ChannelAssignment.pair0 == STM_SE_AUDIO_CHANNEL_PAIR_PAIR0 &&
            ChannelAssignment.pair1 == STM_SE_AUDIO_CHANNEL_PAIR_PAIR1 &&
            ChannelAssignment.pair2 == STM_SE_AUDIO_CHANNEL_PAIR_PAIR2 &&
            ChannelAssignment.pair3 == STM_SE_AUDIO_CHANNEL_PAIR_PAIR3)
        {
            bypass = true;
        }

        break;
    }

    return bypass;
}

////////////////////////////////////////////////////////////////////////////
///
///
/// This is a synchronous function.
/// This function locks, sets event and waits event so be sure that you
/// call it in a thread state that allows that: in particular DO NOT CALL IT
/// in a spin locked code.
///
PlayerStatus_t Mixer_Mme_c::SendAttachPlayer(Audio_Player_c *AudioPlayer)
{
    PlayerStatus_t status;
    MixerRequest_c request;
    request.SetAudioPlayer(AudioPlayer);
    request.SetFunctionToManageTheRequest(&Mixer_Mme_c::AttachPlayer);
    status = MixerRequestManager.SendRequest(request);

    if (status != PlayerNoError)
    {
        SE_ERROR("SendRequest failed status:%d\n", status);
    }

    return status;
}

////////////////////////////////////////////////////////////////////////////
///
/// A wrapper to the Mixer_Mme_c::AttachPlayer(Audio_Player_c* Audio_Player)
/// method.
///
PlayerStatus_t Mixer_Mme_c::AttachPlayer(MixerRequest_c &aMixerRequest_c)
{
    PlayerStatus_t Status;
    Audio_Player_c *AudioPlayer = aMixerRequest_c.GetAudioPlayer();
    Status = AttachPlayer(AudioPlayer);
    return Status;
}

////////////////////////////////////////////////////////////////////////////
///
/// Attach audio player to the mixer.
///
PlayerStatus_t Mixer_Mme_c::AttachPlayer(Audio_Player_c *Audio_Player)
{
    SE_DEBUG(group_mixer, ">%s %p\n", Name, Audio_Player);

    if (mTopology.nb_max_players <= NumberOfAudioPlayerAttached)
    {
        SE_ERROR("Can't Attach: would exceed Topology.nb_max_players = %d\n", mTopology.nb_max_players);
        return PlayerNotSupported;
    }

    // Take the lock because other functions (like GetChannelConfiguration) access player from other thread
    for (uint32_t ClientIdx(0); ClientIdx < MIXER_MAX_CLIENTS; ClientIdx++)
    {
        Client[ClientIdx].LockTake();
    }

    uint32_t PlayerIdx;

    for (PlayerIdx = 0; PlayerIdx < STM_SE_MIXER_NB_MAX_OUTPUTS; PlayerIdx++)
    {
        if (false == MixerPlayer[PlayerIdx].IsPlayerObjectAttached())
        {
            // Register the given audio player.
            MixerPlayer[PlayerIdx].AttachPlayerObject(Audio_Player);
            NumberOfAudioPlayerAttached++;
            break;
        }
    }

    if (PlayerIdx == STM_SE_MIXER_NB_MAX_OUTPUTS)
    {
        SE_ERROR("Can't Attach:Player table is full\n");
        for (uint32_t ClientIdx(0); ClientIdx < MIXER_MAX_CLIENTS; ClientIdx++)
        {
            Client[ClientIdx].LockRelease();
        }
        return PlayerError;
    }

    if ((ThreadState.GetState() == ThreadState_c::Idle) && (true == MMEInitialized))
    {
        if (ThereIsOneClientInState(UNCONFIGURED) || ThereIsOneAudioGenIAudioInputInState(STM_SE_AUDIO_GENERATOR_STOPPED)
            || ThereIsOneClientInState(STARTED)   || ThereIsOneAudioGenIAudioInputInState(STM_SE_AUDIO_GENERATOR_STARTED))
        {
            ThreadState.SetState(ThreadState_c::Starting, __func__);
            // Atleast 1 player is now attached.
            // Clients may be already started or to be started.
            // Ensure that the players are updated and started in the "Starting" state.
        }
    }

    // If the Mixer is Idle, the player shall not be open
    // If the Mixer is Starting, the player will be open in the InitializePcmPlayer() call
    // If the Mixer is in Playback state, the Player shall be started now.
    if ((ThreadState.GetState() == ThreadState_c::Starting) || (ThreadState.GetState() == ThreadState_c::Playback))
    {
        PlayerStatus_t Status = InitializePcmPlayer(PlayerIdx);

        if (Status != PlayerNoError)
        {
            MixerPlayer[PlayerIdx].DetachPlayerObject();
            NumberOfAudioPlayerAttached--;
            for (uint32_t ClientIdx(0); ClientIdx < MIXER_MAX_CLIENTS; ClientIdx++)
            {
                Client[ClientIdx].LockRelease();
            }
            SE_ERROR("Player (%p) was attached but unable to initialize the pcm player so detaching this player\n", Audio_Player);
            return Status;
        }
    }

    FirstActivePlayer = (true == MixerPlayer[0].IsPlayerObjectAttached()) ? 0 : \
                        (true == MixerPlayer[1].IsPlayerObjectAttached()) ? 1 : \
                        (true == MixerPlayer[2].IsPlayerObjectAttached()) ? 2 : \
                        (true == MixerPlayer[3].IsPlayerObjectAttached()) ? 3 : MIXER_MME_NO_ACTIVE_PLAYER;
    PcmPlayerSurfaceParameters_t NewSurfaceParameters = {{0}};
    MixerPlayer[PlayerIdx].SetPcmPlayerConfigSurfaceParameters(NewSurfaceParameters);
    OutputTopologyUpdated         = true;
    // Re-configure the mixer to update the output topology
    MMENeedsParameterUpdate       = true;
    PcmPlayerNeedsParameterUpdate = true;

    SE_DEBUG(group_mixer, "<%s %d\n", Name, NumberOfAudioPlayerAttached);
    for (uint32_t ClientIdx(0); ClientIdx < MIXER_MAX_CLIENTS; ClientIdx++)
    {
        Client[ClientIdx].LockRelease();
    }
    return PlayerNoError;
}

////////////////////////////////////////////////////////////////////////////
///
///
/// This is a synchronous function.
/// This function locks, sets event and waits event so be sure that you
/// call it in a thread state that allows that: in particular DO NOT CALL IT
/// in a spin locked code.
///
PlayerStatus_t Mixer_Mme_c::SendDetachPlayer(Audio_Player_c *AudioPlayer)
{
    PlayerStatus_t status;
    MixerRequest_c request;
    request.SetAudioPlayer(AudioPlayer);
    request.SetFunctionToManageTheRequest(&Mixer_Mme_c::DetachPlayer);
    status = MixerRequestManager.SendRequest(request);

    if (status != PlayerNoError)
    {
        SE_ERROR("SendRequest failed status:%d\n", status);
    }

    return status;
}

////////////////////////////////////////////////////////////////////////////
///
/// A wrapper to the PlayerStatus_t Mixer_Mme_c::DetachPlayer(Audio_Player_c* Audio_Player)
/// method.
///
PlayerStatus_t Mixer_Mme_c::DetachPlayer(MixerRequest_c &aMixerRequest_c)
{
    PlayerStatus_t Status;
    Audio_Player_c *AudioPlayer = aMixerRequest_c.GetAudioPlayer();
    Status = DetachPlayer(AudioPlayer);
    return Status;
}
////////////////////////////////////////////////////////////////////////////
///
/// Detach audio player from the mixer.
///
PlayerStatus_t Mixer_Mme_c::DetachPlayer(Audio_Player_c *Audio_Player)
{
    SE_DEBUG(group_mixer, ">%s %p\n", Name, Audio_Player);
    uint32_t PlayerIdx;

    for (PlayerIdx = 0; PlayerIdx < STM_SE_MIXER_NB_MAX_OUTPUTS; PlayerIdx++)
    {
        if (true == MixerPlayer[PlayerIdx].IsMyPlayerObject(Audio_Player))
        {
            break;
        }
    }

    if (PlayerIdx == STM_SE_MIXER_NB_MAX_OUTPUTS)
    {
        SE_ERROR("Player not existent\n");
        return PlayerError;
    }

    // Take the lock because other functions (like GetChannelConfiguration) access player from other thread
    for (uint32_t ClientIdx(0); ClientIdx < MIXER_MAX_CLIENTS; ClientIdx++)
    {
        Client[ClientIdx].LockTake();
    }
    if (true == MixerPlayer[PlayerIdx].HasPcmPlayer())
    {
        MixerPlayer[PlayerIdx].DeletePcmPlayer();
        NumberOfAudioPlayerInitialized--;
    }

    MixerPlayer[PlayerIdx].DetachPlayerObject();
    NumberOfAudioPlayerAttached--;

    mPlayerToReset[PlayerIdx] = true;

    // No need to reset mSPDIFCardId and mHDMICardId here because
    // it will be re-evaluated while processing the MMENeedsParameterUpdate

    FirstActivePlayer = (true == MixerPlayer[0].IsPlayerObjectAttached()) ? 0 : \
                        (true == MixerPlayer[1].IsPlayerObjectAttached()) ? 1 : \
                        (true == MixerPlayer[2].IsPlayerObjectAttached()) ? 2 : \
                        (true == MixerPlayer[3].IsPlayerObjectAttached()) ? 3 : MIXER_MME_NO_ACTIVE_PLAYER;
    OutputTopologyUpdated         = true;
    // Re-configure the mixer to update the output topology
    MMENeedsParameterUpdate       = true;
    PcmPlayerNeedsParameterUpdate = true;

    // Make sure to release lock before UpdateInputBuffer (because UpdateInputBuffer will take the lock)
    for (uint32_t ClientIdx(0); ClientIdx < MIXER_MAX_CLIENTS; ClientIdx++)
    {
        Client[ClientIdx].LockRelease();
    }

    if ((FirstActivePlayer == MIXER_MME_NO_ACTIVE_PLAYER) && (ThreadState.GetState() != ThreadState_c::Idle))
    {
        // We are moving mixer ThreadState to Idle. Update input buffer for all Clients and request to ReleaseProcessingDecodedBuffer.
        for (uint32_t ClientIdx(0); ClientIdx < MIXER_MAX_CLIENTS; ClientIdx++)
        {
            UpdateInputBuffer(ClientIdx, true);
        }
        // We have no player attached. Can't play samples move the Mixer ThreadState to Idle
        SE_WARNING("No player attached. Can't play samples so moving Mixer ThreadState to Idle\n");
        ThreadState.SetState(ThreadState_c::Idle, __func__);
    }
    SE_DEBUG(group_mixer, "<%s %d\n", Name, NumberOfAudioPlayerAttached);
    return PlayerNoError;
}

bool Mixer_Mme_c::AllAudioGenIAudioInputsAreNotStarted()
{
    bool allClientInTheState = true;

    for (uint32_t AudioGeneratorIdx = 0; AudioGeneratorIdx < MAX_AUDIO_GENERATORS; AudioGeneratorIdx++)
    {
        if ((Generator[AudioGeneratorIdx]) &&
            Generator[AudioGeneratorIdx]->GetState() == STM_SE_AUDIO_GENERATOR_STARTED)
        {
            allClientInTheState = false;
            break;
        }
    }

    for (uint32_t InteractiveAudioIdx = 0; InteractiveAudioIdx < MAX_INTERACTIVE_AUDIO; InteractiveAudioIdx++)
    {
        if ((InteractiveAudio[InteractiveAudioIdx]) &&
            InteractiveAudio[InteractiveAudioIdx]->GetState() == STM_SE_AUDIO_GENERATOR_STARTED)
        {
            allClientInTheState = false;
            break;
        }
    }

    return allClientInTheState;
}

////////////////////////////////////////////////////////////////////////////
///
/// Specification: it returns true if there is at least one audio generator
/// client in the aState state and false otherwise.
///
bool Mixer_Mme_c::ThereIsOneAudioGenIAudioInputInState(stm_se_audio_generator_state_t aState)
{
    bool allClientInTheState = false;

    for (uint32_t AudioGeneratorIdx = 0; AudioGeneratorIdx < MAX_AUDIO_GENERATORS; AudioGeneratorIdx++)
    {
        if (Generator[AudioGeneratorIdx] && Generator[AudioGeneratorIdx]->GetState() == aState)
        {
            allClientInTheState = true;
            break;
        }
    }

    for (int InteractiveAudioIdx = 0; InteractiveAudioIdx < MAX_INTERACTIVE_AUDIO; InteractiveAudioIdx++)
    {
        if (InteractiveAudio[InteractiveAudioIdx] && InteractiveAudio[InteractiveAudioIdx]->GetState() == aState)
        {
            allClientInTheState = true;
            break;
        }
    }

    return allClientInTheState;
}

////////////////////////////////////////////////////////////////////////////
///
/// Provide sensible defaults for the output configuration.
///
void Mixer_Mme_c::ResetMixerSettings()
{
    MixerSettings.MixerOptions.Reset();
    MixerSettings.MixerOptions.SpeakerConfig = default_speaker_config;
}
