/************************************************************************
Copyright (C) 2003-2014 STMicroelectronics. All Rights Reserved.

This file is part of the Streaming Engine.

Streaming Engine is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License version 2 as
published by the Free Software Foundation.

Streaming Engine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with Streaming Engine; see the file COPYING.  If not, write to the Free
Software Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307,
USA.

The Streaming Engine Library may alternatively be licensed under a
proprietary license from ST.
************************************************************************/

#include "mixer_mme.h"
#include "mixer_request_manager.h"

#undef TRACE_TAG
#define TRACE_TAG   "Mixer_Request_Manager_c"

#define MAX_MIXER_REQUESTS_NUMBER             16

Mixer_Request_Manager_c::Mixer_Request_Manager_c()
    : mRtMutex()
    , mPendingRequestEvent()
    , mPendingTerminationEvent()
    , mRequestCompletionEvent()
    , mTimeOut(0)
    , mError(false)
    , mThreadState(Idle)
    , mFilledMixerRequestRing(NULL)
    , mEmptyMixerRequestRing(NULL)
{
    SetTimeOut(DEFAULT_MIXER_REQUEST_MANAGER_TIMEOUT);

    SE_DEBUG(group_mixer, "this = %p\n", this);

    OS_InitializeRtMutex(&mRtMutex);
    OS_InitializeEvent(&mPendingRequestEvent);
    OS_InitializeEvent(&mPendingTerminationEvent);
    OS_InitializeEvent(&mRequestCompletionEvent);
}

Mixer_Request_Manager_c::~Mixer_Request_Manager_c()
{
    Stop();
    OS_TerminateRtMutex(&mRtMutex);
    OS_TerminateEvent(&mPendingRequestEvent);
    OS_TerminateEvent(&mRequestCompletionEvent);
    OS_TerminateEvent(&mPendingTerminationEvent);

    MixerRequest_c *MixerReq;
    if (mFilledMixerRequestRing != NULL)
    {
        while (mFilledMixerRequestRing->Extract((uintptr_t *) &MixerReq, RING_NONE_BLOCKING) == RingNoError)
        {
            SE_DEBUG(group_mixer, "Extracted MixerReq:%p for deletion from mFilledMixerRequestRing\n", MixerReq);
            delete MixerReq;
        }
        SE_DEBUG(group_mixer, "Will delete mFilledMixerRequestRing\n");
        delete mFilledMixerRequestRing;
    }

    if (mEmptyMixerRequestRing != NULL)
    {
        while (mEmptyMixerRequestRing->Extract((uintptr_t *) &MixerReq, RING_NONE_BLOCKING) == RingNoError)
        {
            SE_DEBUG(group_mixer, "Extracted MixerReq:%p for deletion from mEmptyMixerRequestRing\n", MixerReq);
            delete MixerReq;
        }
        SE_DEBUG(group_mixer, "Will delete mEmptyMixerRequestRing\n");
        delete mEmptyMixerRequestRing;
    }
    SE_DEBUG(group_mixer, "this = %p\n", this);
}

PlayerStatus_t Mixer_Request_Manager_c::WaitForPendingRequest(OS_Timeout_t aTimeOut)
{
    PlayerStatus_t returnStatus = PlayerNoError;

    switch (mThreadState)
    {
    case Started:
    {
        if (!(OS_TestEventSet(&mPendingRequestEvent)))
        {
            if (OS_WaitForEventAuto(&mPendingRequestEvent, aTimeOut) == OS_TIMED_OUT)
            {
                returnStatus = PlayerTimedOut;
            }
        }
        break;
    }
    case Terminating:
    {
        // mPendingRequestEvent was set during termination phase to exit the idle state
        // but there is no actual request to process
        SE_DEBUG(group_mixer, "Terminating...\n");
        // The Event mPendingTerminationEvent is set, and to avoid the Playback thread
        // idle state handler to send us back here before termination is taken into account,
        // we sleep a little bit
        OS_SleepMilliSeconds(10);
        break;
    }
    default:
    {
        SE_ERROR("Trying to wait for pending request whereas the request manager is not started\n");
        returnStatus = PlayerError;
        break;
    }
    }

    return returnStatus;
}

void Mixer_Request_Manager_c::Stop()
{
    mThreadState = Terminating;
    OS_SetEvent(&mPendingTerminationEvent);
    OS_SetEvent(&mPendingRequestEvent);
    OS_SetEvent(&mRequestCompletionEvent);
}

PlayerStatus_t Mixer_Request_Manager_c::Start()
{
    SE_ASSERT(mFilledMixerRequestRing == NULL);
    SE_ASSERT(mEmptyMixerRequestRing == NULL);

    mFilledMixerRequestRing = RingGeneric_c::New(MAX_MIXER_REQUESTS_NUMBER, "Mixer_Request_Manager_c::mFilledMixerRequestRing");
    if (mFilledMixerRequestRing == NULL)
    {
        SE_ERROR("Failed to create mFilledMixerRequestRing RingGeneric_c object\n");
        return PlayerInsufficientMemory;
    }

    mEmptyMixerRequestRing = RingGeneric_c::New(MAX_MIXER_REQUESTS_NUMBER, "Mixer_Request_Manager_c::mEmptyMixerRequestRing");
    if (mEmptyMixerRequestRing == NULL)
    {
        SE_ERROR("Failed to create mEmptyMixerRequestRing RingGeneric_c object\n");
        delete mFilledMixerRequestRing;
        mFilledMixerRequestRing = NULL;
        return PlayerInsufficientMemory;
    }

    for (int i = 0; i < MAX_MIXER_REQUESTS_NUMBER; i++)
    {
        MixerRequest_c *MixerReq = new MixerRequest_c();
        if (mEmptyMixerRequestRing->Insert((uintptr_t)MixerReq) != RingNoError)
        {
            SE_ERROR("No more space to insert mixer request\n");
            delete MixerReq;
            break;
        }
    }

    mThreadState = Started;
    return PlayerNoError;
}

PlayerStatus_t Mixer_Request_Manager_c::ManageTheRequest(Mixer_Mme_c &aMixer)
{
    PlayerStatus_t returnStatus = PlayerNoError;

    if (!OS_TestEventSet(&mPendingTerminationEvent)
        && OS_TestEventSet(&mPendingRequestEvent))
    {
        MixerRequest_c *MixerReq;

        int nbRequests = mFilledMixerRequestRing->NbOfEntries();

        SE_DEBUG(group_mixer, "@: %d pending requests (this = %p)\n", nbRequests, this);
        while (mFilledMixerRequestRing->Extract((uintptr_t *) &MixerReq, RING_NONE_BLOCKING) == RingNoError)
        {
            SE_VERBOSE(group_mixer, "@: Executing request %d/%d\n",
                       nbRequests - mFilledMixerRequestRing->NbOfEntries(),
                       nbRequests);
            MixerRequest_c::FunctionToManageTheRequest fctToManageTheRequest = MixerReq->GetFunctionToManageTheRequest();

            if (fctToManageTheRequest == 0)
            {
                SE_ERROR("Unspecified Mixer_Mme_c method to manage the request\n");
                returnStatus = PlayerError;
            }
            else
            {
                returnStatus = (aMixer.*fctToManageTheRequest)(*MixerReq);

                if (returnStatus != PlayerNoError)
                {
                    SetError();
                }
            }

            MixerReq->SetFunctionToManageTheRequest(0);

            if (mEmptyMixerRequestRing->Insert((uintptr_t) MixerReq) != RingNoError)
            {
                SE_ERROR("Failed to insert MixerReq:%p in mEmptyMixerRequestRing while recylcing request\n", MixerReq);
            }
        }
        SE_DEBUG(group_mixer, "@: Set the completion event to signal that the request has been completed (this = %p)\n", this);
        OS_ResetEvent(&mPendingRequestEvent);
        OS_SetEvent(&mRequestCompletionEvent);
    }

    return returnStatus;
}

PlayerStatus_t Mixer_Request_Manager_c::SendRequest(MixerRequest_c &aMixerRequest)
{
    SE_VERBOSE(group_mixer, ">: this = %p\n", this);
    PlayerStatus_t status = PlayerNoError;

    if (mThreadState == Started)
    {
        Lock();

        if (aMixerRequest.GetFunctionToManageTheRequest() == 0)
        {
            SE_ERROR("Trying to send a request with unspecified Mixer_Mme_c method to managed the request\n");
            status = PlayerNoError;
        }
        else
        {
            MixerRequest_c *MixRequestClone;

            if (mEmptyMixerRequestRing->Extract((uintptr_t *) &MixRequestClone, RING_NONE_BLOCKING) != RingNoError)
            {
                SE_ERROR("Failed to extract empty request from mEmptyMixerRequestRing\n");
                Unlock();
                return PlayerError;
            }

            *MixRequestClone = aMixerRequest;

            if (mFilledMixerRequestRing->Insert((uintptr_t) MixRequestClone) != RingNoError)
            {
                SE_ERROR("Failed to insert request in mFilledMixerRequestRing\n");
                mEmptyMixerRequestRing->Insert((uintptr_t) MixRequestClone);
                Unlock();
                return PlayerTooMany;
            }

            SE_DEBUG(group_mixer, "@: sending a request\n");
            OS_ResetEvent(&mRequestCompletionEvent);
            OS_SetEvent(&mPendingRequestEvent);
            SE_VERBOSE(group_mixer, "@: waiting completion of the request\n");
            status = WaitRequestCompletion();
            SE_DEBUG(group_mixer, "@: the request has been completed\n");
        }

        Unlock();
    }
    else
    {
        SE_ERROR("Trying to send a request whereas the request manager is not yet started\n");
        status = PlayerError;
    }

    SE_VERBOSE(group_mixer, "<: this = %p\n", this);
    return status;
}

PlayerStatus_t Mixer_Request_Manager_c::SendAsyncRequest(MixerRequest_c &aMixerRequest)
{
    SE_VERBOSE(group_mixer, ">: this = %p\n", this);
    PlayerStatus_t status = PlayerNoError;

    if (mThreadState == Started)
    {
        if (aMixerRequest.GetFunctionToManageTheRequest() == 0)
        {
            SE_ERROR("Trying to send a request with unspecified Mixer_Mme_c method to managed the request\n");
            status = PlayerNoError;
        }
        else
        {
            MixerRequest_c *MixRequestClone;

            if (mEmptyMixerRequestRing->Extract((uintptr_t *) &MixRequestClone, RING_NONE_BLOCKING) != RingNoError)
            {
                SE_ERROR("Failed to extract empty request from mEmptyMixerRequestRing\n");
                return PlayerError;
            }

            *MixRequestClone = aMixerRequest;

            if (mFilledMixerRequestRing->Insert((uintptr_t) MixRequestClone) != RingNoError)
            {
                SE_ERROR("Failed to insert request in mFilledMixerRequestRing\n");
                mEmptyMixerRequestRing->Insert((uintptr_t) MixRequestClone);
                return PlayerTooMany;
            }

            SE_DEBUG(group_mixer, "@: sending an Asynchronous request\n");
            OS_ResetEvent(&mRequestCompletionEvent);
            OS_SetEvent(&mPendingRequestEvent);
        }
    }
    else
    {
        SE_ERROR("Trying to send a request whereas the request manager is not yet started\n");
        status = PlayerError;
    }

    SE_VERBOSE(group_mixer, "<: this = %p\n", this);
    return status;
}

PlayerStatus_t Mixer_Request_Manager_c::WaitRequestCompletion()
{
    PlayerStatus_t returnStatus = PlayerNoError;
    SE_VERBOSE(group_mixer, ">: this = %p\n", this);

    if (mThreadState == Started)
    {
        returnStatus = PlayerNoError;
        OS_Status_t WaitStatus = OS_WaitForEventAuto(&mRequestCompletionEvent, mTimeOut);
        if (WaitStatus == OS_TIMED_OUT)
        {
            SE_ERROR("wait for request completion timedout\n");
            returnStatus = PlayerError;
        }

        OS_ResetEvent(&mRequestCompletionEvent);

        if (mError)
        {
            returnStatus = PlayerError;
        }

        mError = false;
    }

    SE_VERBOSE(group_mixer, "<: this = %p\n", this);
    return returnStatus;
}

void Mixer_Request_Manager_c::Lock()
{
    SE_VERBOSE(group_mixer, ">: locking this = %p\n", this);
    OS_LockRtMutex(&mRtMutex);
    SE_VERBOSE(group_mixer, "<: this = %p is locked now\n", this);
}

void Mixer_Request_Manager_c::Unlock()
{
    SE_VERBOSE(group_mixer, ">: Unlocking this = %p\n", this);
    OS_UnLockRtMutex(&mRtMutex);
    SE_VERBOSE(group_mixer, "<: this = %p is unlocked now\n", this);
}
