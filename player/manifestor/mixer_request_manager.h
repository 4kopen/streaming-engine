/************************************************************************
Copyright (C) 2003-2014 STMicroelectronics. All Rights Reserved.

This file is part of the Streaming Engine.

Streaming Engine is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License version 2 as
published by the Free Software Foundation.

Streaming Engine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with Streaming Engine; see the file COPYING.  If not, write to the Free
Software Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307,
USA.

The Streaming Engine Library may alternatively be licensed under a
proprietary license from ST.
************************************************************************/

#ifndef H_MIXER_REQUEST_MANAGER_CLASS
#define H_MIXER_REQUEST_MANAGER_CLASS

#include "mixer.h"
#include "mixer_requests.h"

#define DEFAULT_MIXER_REQUEST_MANAGER_TIMEOUT 5000

////////////////////////////////////////////////////////////////////////////////////
///
/// class Mixer_Request_Manager_c:
///
/// (1) This class provides a way for mixer client to send synchronous requests to
/// the playback thread. It has been created to solve bug 22324 and in general to
/// avoid some of the concurrent accesses to the shared resources (client states)
/// between the client and the playback thread.
/// Note that this manager accept only one request at a time (no request queue)
/// and if a thread (a client thread typically) tries to send a request while
/// another thread is waiting for a request to complete, then it will block.
///
///
/// (2) The typical usage is:
/// - in the playback thread at the very beginning: Start()
/// - in the playback thread: regularly call ManageTheRequest() to manage
///   the pending request
/// - a client thread sends a request and blocks until the request has been
///   completed:
///
///     PlayerStatus_t Mixer_Mme_c::SendMyRequest(MyType aMyRequestParameter)
///     {
///         PlayerStatus_t status;
///         MixerRequest_c request;
///
///         request.SetMyRequestParameter(aMyRequestParameter);
///         request.SetFunctionToManageTheRequest(&Mixer_Mme_c::MyRequestManagementFunction);
///
///         status = MixerRequestManager.SendRequest(request);
///         if(status != PlayerNoError)
///         {
///             SE_ERROR(" Error returned by MixerRequestManager.SendRequest()\n");
///         }
///
///         return status;
///     }
/// - before deleting the manager needs to call Stop().
///
///
/// (3) CAUTION: SendRequest(), the method that sends the request to the receiver thread
/// typically the playback thread), is a blocking one that sleeps until
/// the request has been managed by receiver thread (typically the playback thread).
/// So it cannot be used in a spin locked client code.
///
///
class Mixer_Request_Manager_c
{
////////////////////////////////////////////////////////////////////////////////
///
///                              Public API
///
////////////////////////////////////////////////////////////////////////////////
public:
    Mixer_Request_Manager_c();
    ~Mixer_Request_Manager_c();

    ////////////////////////////////////////////////////////////////////////////
    ///
    /// Specification: it waits (with a time out value of aTimeOut) for a
    /// a request to be pending. the timeout is not considered as an error.
    /// It returns an error if something went wrong in waiting.
    ///
    PlayerStatus_t WaitForPendingRequest(OS_Timeout_t aTimeOut);

    ////////////////////////////////////////////////////////////////////////////
    ///
    /// Specification: it stops the manager so that it cannot send request
    /// nor wait for them. It also frees the Ring used to store MixerRequests.
    ///
    /// Implementation comment:
    /// This function sets the completion and pending request events so as to
    /// unblock the thread that could be waiting on these events.
    ///
    void Stop();

    ////////////////////////////////////////////////////////////////////////////
    ///
    /// Specification: it starts the manager so as to make it able to send,
    /// wait for and manage requests. It creates the Ring used to store MixerRequests.
    /// Typically it can be called when the receiver thread is ready to accept requests.
    ///
    PlayerStatus_t Start();

    ////////////////////////////////////////////////////////////////////////////
    ///
    /// Specification: it sets to aTimeOut the time out value for waiting the
    /// request completion.
    ///
    inline void SetTimeOut(OS_Timeout_t aTimeOut)
    {
        mTimeOut = aTimeOut;
    }

    ////////////////////////////////////////////////////////////////////////////
    ///
    /// Specification: it manages the pending requests if Ring is NonEmpty.
    /// That is to say: it calls the Mixer_Mme_c method that has been specified
    /// in the MixerRequest stored in mFilledMixerRequestRing member during previous call to SendRequest/SendAsyncRequest().
    /// It returns a PlayerStatus_t error status.
    ///
    PlayerStatus_t ManageTheRequest(Mixer_Mme_c &aMixer);

    ////////////////////////////////////////////////////////////////////////////
    ///
    /// Specification: it sends the aMixerRequest request to whoever is listening
    /// for request.
    /// It returns an error status.
    /// aMixerRequest: it should be initialized properly in particular the
    /// Mixer_Mme_c method to be called when managing the request must have been
    /// set.
    /// CAUTION: this function locks, sets event and waits for event. So be sure that
    /// the thread that is calling this function is in a state that allows that:
    /// DO NOT USED this function in a spin locked code for instance.
    ///
    /// Implementation comments:
    /// After having checked that the manager is started and that the mixer method
    /// to call has been specified all right:
    /// - we lock the manager
    /// - we reset the request completion event
    /// - we set the pending request event
    /// - we wait for the request completion event
    /// - we unlock the manager.
    ///
    PlayerStatus_t SendRequest(MixerRequest_c &aMixerRequest);

    PlayerStatus_t SendAsyncRequest(MixerRequest_c &aMixerRequest);

private:
    ////////////////////////////////////////////////////////////////////////////
    /// Internal mutex to prevent different threads to set an event in parallel.
    ///
    OS_RtMutex_t mRtMutex;

    ////////////////////////////////////////////////////////////////////////////
    /// Internal event that must be set to tell that a request is pending.
    ///
    OS_Event_t mPendingRequestEvent;

    ////////////////////////////////////////////////////////////////////////////
    /// Internal event that must be set to tell that a termination is requested.
    ///
    OS_Event_t mPendingTerminationEvent;

    ////////////////////////////////////////////////////////////////////////////
    /// Internal event that tells that the request has been completed
    ///
    OS_Event_t mRequestCompletionEvent;

    ////////////////////////////////////////////////////////////////////////////
    /// The time out value for waiting the request completion.
    ///
    ///
    OS_Timeout_t mTimeOut;

    ////////////////////////////////////////////////////////////////////////////
    /// Set to true if there is an error in the request management and false
    /// otherwise.
    ///
    ///
    bool mError;

    enum
    {
        Idle,
        Started,
        Terminating
    } mThreadState;

    ////////////////////////////////////////////////////////////////////////////
    /// The mixer request rings
    ///
    RingGeneric_c *mFilledMixerRequestRing;
    RingGeneric_c *mEmptyMixerRequestRing;

    ////////////////////////////////////////////////////////////////////////////
    ///
    /// Specifications: it waits for the request to be completed.
    ///
    /// Implementation comments:
    ///
    PlayerStatus_t WaitRequestCompletion();

    ////////////////////////////////////////////////////////////////////////////
    ///
    /// Specification: it sets the error to true.
    ///
    inline void SetError()
    {
        mError = true;
    }

    ////////////////////////////////////////////////////////////////////////////
    ///
    /// Specification: this function locks the manager
    ///
    void Lock();

    ////////////////////////////////////////////////////////////////////////////
    ///
    /// Specification: this function unlocks the manager.
    ///
    void Unlock();

    DISALLOW_COPY_AND_ASSIGN(Mixer_Request_Manager_c);
};

#endif // H_MIXER_REQUEST_MANAGER_CLASS
