/************************************************************************
Copyright (C) 2003-2014 STMicroelectronics. All Rights Reserved.

This file is part of the Streaming Engine.

Streaming Engine is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License version 2 as
published by the Free Software Foundation.

Streaming Engine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with Streaming Engine; see the file COPYING.  If not, write to the Free
Software Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307,
USA.

The Streaming Engine Library may alternatively be licensed under a
proprietary license from ST.
************************************************************************/

#include "acc_mme.h"
#include "mixer_mme.h"
#include "pcm_proc.h"

#undef TRACE_TAG
#define TRACE_TAG "PcmProc"

// ToDO : tune TVOuput_GainAdjust, AVROutput_GainAdjust and HEADPHONEOutput_GainAdjust values in table below
//
typedef struct
{
    enum eAccBoolean  AdjustPRL;
    I32               PRL_TV;         // value in mB
    I32               PRL_AVR;        // value in mB
    I32               PRL_HeadPhone;  // value in mB
} ProgramReferenceLevelPreset_t;

// CAUTION: MUST correspond to: PolicyValueAudioApplicationXXX definitions.
#define NB_AUDIO_APPLICATION_TYPES (STM_SE_CTRL_VALUE_LAST_AUDIO_APPLICATION_TYPE + 1)

static const ProgramReferenceLevelPreset_t LimiterGainAdjustLookupTable[NB_AUDIO_APPLICATION_TYPES] =
{
    /*AdjustProgramRefLevel  PRL_TV    PRL_AVR    PRL_HeadPhone */
    { ACC_MME_FALSE,         0,          0,             0 }, // STM_SE_CTRL_VALUE_AUDIO_APPLICATION_ISO
    { ACC_MME_TRUE ,     -3100,      -3100,         -3100 }, // STM_SE_CTRL_VALUE_AUDIO_APPLICATION_DVD
    { ACC_MME_TRUE ,     -2300,      -3100,         -2300 }, // STM_SE_CTRL_VALUE_AUDIO_APPLICATION_DVB
    { ACC_MME_TRUE ,     -2300,      -3100,         -2300 }, // STM_SE_CTRL_VALUE_AUDIO_APPLICATION_MS10
    { ACC_MME_TRUE ,     -2300,      -3100,         -2300 }, // STM_SE_CTRL_VALUE_AUDIO_APPLICATION_MS11
    { ACC_MME_TRUE ,     -2300,      -3100,         -2300 }  // STM_SE_CTRL_VALUE_AUDIO_APPLICATION_MS12
};

////////////////////////////////////////////////////////////////////////////
///
/// Reset all PCM post-processing that could be enabled for a single physical output.
///
void PcmProc::ResetDevicePcmParameters(MME_LxPcmPostProcessingGlobalParameters_Frozen_t *PcmParams,
                                       uint32_t MixerPlayerIndex)
{
    SE_ASSERT(PcmParams != NULL);
    SE_DEBUG(group_mixer, "Reset Pcm Post Proc for MixerPlayer:%d\n", MixerPlayerIndex);

    memset(PcmParams, 0, sizeof(*PcmParams));
    PcmParams->Id               = ACC_RENDERER_MIXER_POSTPROCESSING_ID;
    PcmParams->NbPcmProcessings = 0;
    PcmParams->AuxSplit         = ACC_SPLIT_AUTO;
    PcmParams->StructSize       = offsetof(MME_LxPcmPostProcessingGlobalParameters_Frozen_t, AuxSplit)
                                  + sizeof(MME_LxPcmPostProcessingGlobalParameters_Frozen_t::AuxSplit);

#define RESET_PCM_PROC(pcmproc, ID, STRUCT)                           \
    pcmproc.Id            = PCMPROCESS_SET_ID(ID, MixerPlayerIndex);  \
    pcmproc.StructSize    = sizeof(STRUCT);                           \
    pcmproc.Apply         = ACC_MME_DISABLED;                         \
    PcmParams->StructSize += pcmproc.StructSize;

    RESET_PCM_PROC(PcmParams->BassMgt,              ACC_PCM_BASSMGT_ID,       MME_ExtBassMgtGlobalParams_t)
    RESET_PCM_PROC(PcmParams->Equalizer,            ACC_PCM_EQUALIZER_ID,     MME_EqualizerGlobalParams_t)
    RESET_PCM_PROC(PcmParams->TempoControl,         ACC_PCM_TEMPO_ID,         MME_TempoGlobalParams_t)
    RESET_PCM_PROC(PcmParams->DCRemove,             ACC_PCM_DCREMOVE_ID,      MME_DCRemoveGlobalParams_t)
    RESET_PCM_PROC(PcmParams->Delay,                ACC_PCM_DELAY_ID,         MME_DelayGlobalParams_t)
    RESET_PCM_PROC(PcmParams->Encoder.Generic,      ACC_PCM_ENCODER_ID,       MME_EncoderPPGlobalParams_u)
    RESET_PCM_PROC(PcmParams->Transcoder,           ACC_PCM_TRANSCODER_ID,    MME_TranscodePPGlobalParams_t)
    RESET_PCM_PROC(PcmParams->Sfc,                  ACC_PCM_SFC_ID,           MME_SfcPPGlobalParams_t)
    RESET_PCM_PROC(PcmParams->Resamplex2,           ACC_PCM_RESAMPLE_ID,      MME_Resamplex2GlobalParams_t)
    RESET_PCM_PROC(PcmParams->Dmix,                 ACC_PCM_DMIX_ID,          MME_DMixGlobalParams_t)
    RESET_PCM_PROC(PcmParams->FatPipeOrSpdifOut,    ACC_PCM_SPDIFOUT_ID,      MME_FatpipeGlobalParams_t)
    RESET_PCM_PROC(PcmParams->Limiter,              ACC_PCM_LIMITER_ID,       MME_LimiterGlobalParams_t)
    RESET_PCM_PROC(PcmParams->Btsc,                 ACC_PCMPRO_BTSC_ID,       MME_BTSCGlobalParams_t)
    RESET_PCM_PROC(PcmParams->MetaRencode,          ACC_PCM_METARENC_ID,      MME_METARENCGlobalParams_t)

#undef RESET_PCM_PROC

    PcmParams->CMC.Id           = PCMPROCESS_SET_ID(ACC_PCM_CMC_ID, MixerPlayerIndex);
    PcmParams->CMC.StructSize   = sizeof(MME_CMCGlobalParams_t);
    PcmParams->StructSize      += PcmParams->CMC.StructSize;
}

////////////////////////////////////////////////////////////////////////////
///
/// Static helper function to dump PcmProcessing structure content
///
void PcmProc::DumpMixerPostProcStruct(const MME_LxPcmPostProcessingGlobalParameters_Frozen_t *params,
                                      uint32_t HeaderSize)
{
    SE_DEBUG(group_mixer, "GlobalOutputPcmParams->StructSize=%d\n", params->StructSize);
    if (SE_IS_VERBOSE_ON(group_mixer))
    {
        const uint8_t *dumpPtr  = reinterpret_cast<const uint8_t *>(params);
        const struct MME_PcmProcParamsHeader_s *pcmProc;
        uint32_t index = HeaderSize;
        while (index < params->StructSize)
        {
            pcmProc = reinterpret_cast<const struct MME_PcmProcParamsHeader_s *>(&dumpPtr[index]);
            SE_VERBOSE(group_mixer, "index:%5d PCM PROC Id:%2d %-32s [chain:%2d size:%4d] ->%s\n",
                       index, ACC_PCMPROC_ID(pcmProc->Id),
                       Mixer_Mme_c::LookupPcmProcessingID(ACC_PCMPROC_ID(pcmProc->Id)),
                       ACC_PCMPROC_SUBID(pcmProc->Id),
                       pcmProc->StructSize, pcmProc->Apply ? "Enable" : "Disable");
            index += pcmProc->StructSize;
            SE_ASSERT(pcmProc->StructSize != 0);
        }
    }
}

static void FillOutDDPCEparams(MME_EncoderPPGlobalParams_t *DDPCE_GP,
                               tDdpceConfig                *DDPCE_Spec,
                               int                          ApplicationType,
                               enum eAccAcMode              acmod)
{
    DDPCE_GP->Apply = ACC_MME_ENABLED;
    ACC_ENCODERPP_SET_TYPE(DDPCE_GP, ACC_ENCODERPP_DDPCE);  // Select DDPCE Encoder
    ACC_ENCODERPP_SET_SUBTYPE(DDPCE_GP, 0);                 // No Sub Type defined for DDPCE
    ACC_ENCODERPP_SET_IECENABLE(DDPCE_GP, 1);               // Let the encoder insert preambles

    if (ApplicationType == STM_SE_CTRL_VALUE_AUDIO_APPLICATION_MS12)
    {
        DDPCE_GP->BitRate = ENCPP_BITRATE_AUTO;
    }
    else
    {
        const int datarate[] = {256, 640, 640, 640, 640, 640, 640, 640, 768};

        if (acmod <= 7)
        {
            DDPCE_GP->BitRate = datarate[acmod];
        }
        else
        {
            DDPCE_GP->BitRate = datarate[8];
        }
    }

    DDPCE_Spec->Version          = DDPCE_API_VERSION;
    DDPCE_Spec->EncoderMode      = DDPCE_ENC_MODE_DDP_LC;
    DDPCE_Spec->ContentType      = DDPCE_ENC_CE_CONTENTTYPE_MOVIE;
    DDPCE_Spec->Phase90Filter    = true;
    DDPCE_Spec->Upmixer          = DDPCE_UPMIX_DISABLE;
    DDPCE_Spec->DisableTranscode = false;
    DDPCE_Spec->DisableLfeEncode = false;
    DDPCE_Spec->ReportLoudness   = false;
    DDPCE_Spec->Rsvd             = 0;
}

int PcmProc::FillOutEncoderPPparams(MME_EncoderPPGlobalParams_u   *EncoderPP,
                                    MME_TranscodePPGlobalParams_t *Transcoder,
                                    PcmPlayer_c::OutputEncoding OutputEncoding,
                                    uint32_t MixerPlayerIndex,
                                    bool useTranscoder,
                                    int ApplicationType,
                                    enum eAccAcMode acmod)
{
    SE_ASSERT(EncoderPP != NULL);
    SE_ASSERT(Transcoder != NULL);

    MME_EncoderPPGlobalParams_t *Encoder = &EncoderPP->Generic;

    Encoder->Id         = PCMPROCESS_SET_ID(ACC_PCM_ENCODER_ID, MixerPlayerIndex);
    Encoder->StructSize = sizeof(*EncoderPP);
    Encoder->Apply      = ACC_MME_DISABLED;

    Transcoder->Id         = PCMPROCESS_SET_ID(ACC_PCM_TRANSCODER_ID, MixerPlayerIndex);
    Transcoder->StructSize = sizeof(*Transcoder);
    Transcoder->Apply      = ACC_MME_DISABLED;


    // maybe setup AC3/etc encoding if SPDIF device.
    switch (OutputEncoding)
    {
    case PcmPlayer_c::OUTPUT_PCM:
    case PcmPlayer_c::OUTPUT_IEC60958:
    case PcmPlayer_c::BYPASS_AC3:
    case PcmPlayer_c::BYPASS_DTS_512:
    case PcmPlayer_c::BYPASS_DTS_1024:
    case PcmPlayer_c::BYPASS_DTS_2048:
    case PcmPlayer_c::BYPASS_DTS_CDDA:
    case PcmPlayer_c::BYPASS_DTSHD_LBR:
    case PcmPlayer_c::BYPASS_DTSHD_HR:
    case PcmPlayer_c::BYPASS_DTSHD_DTS_4096:
    case PcmPlayer_c::BYPASS_DTSHD_DTS_8192:
    case PcmPlayer_c::BYPASS_DTSHD_MA:
    case PcmPlayer_c::BYPASS_DDPLUS:
    case PcmPlayer_c::BYPASS_TRUEHD:
    case PcmPlayer_c::BYPASS_AAC:
    case PcmPlayer_c::BYPASS_HEAAC_960:
    case PcmPlayer_c::BYPASS_HEAAC_1024:
    case PcmPlayer_c::BYPASS_HEAAC_1920:
    case PcmPlayer_c::BYPASS_HEAAC_2048:
    case PcmPlayer_c::BYPASS_SPDIFIN_COMPRESSED:
    case PcmPlayer_c::BYPASS_SPDIFIN_COMPRESSED_SD:
    case PcmPlayer_c::BYPASS_SPDIFIN_PCM:
        break;

    case PcmPlayer_c::OUTPUT_AC3:
    {
        if (useTranscoder)
        {
            /* If we use transcoder we need to configure both Transcoder & Encoder */
            Transcoder->Apply     = ACC_MME_ENABLED;
            Transcoder->Type      = ACC_TRANSCODERPP_DDP_TO_DD;
            Transcoder->IecEnable = 1;

            FillOutDDPCEparams(&EncoderPP->DDPce.GP, &EncoderPP->DDPce.SpecificConfig, ApplicationType, acmod);
        }
        else
        {
            Encoder->Apply = ACC_MME_ENABLED;
            Encoder->Type  = ACC_ENCODERPP_DDCE;     // type=bits0-3, subtype=bits4-7
            ACC_ENCODERPP_SET_IECENABLE(Encoder, 1); // let the encoder insert preambles

            if (ApplicationType == STM_SE_CTRL_VALUE_AUDIO_APPLICATION_MS11 ||
                ApplicationType == STM_SE_CTRL_VALUE_AUDIO_APPLICATION_MS12)
            {
                Encoder->BitRate = ENCPP_BITRATE_AUTO;
            }
            else
            {
                Encoder->BitRate = 448; // DDCE:448000bps
            }
        }
    }
    break;

    case PcmPlayer_c::OUTPUT_DDPLUS:
    {
        FillOutDDPCEparams(&EncoderPP->DDPce.GP, &EncoderPP->DDPce.SpecificConfig, ApplicationType, acmod);
    }
    break;

    case PcmPlayer_c::OUTPUT_DTS:
    {
        Encoder->Apply = ACC_MME_ENABLED;
        Encoder->Type = ACC_ENCODERPP_DTSE;      // type=bits0-3, subtype=bits4-7
        ACC_ENCODERPP_SET_IECENABLE(Encoder, 1); // let the encoder insert preambles
        Encoder->BitRate = 1536000;              // DDCE:448000bps DTS:1536000bps
    }
    break;

    default:
        SE_ASSERT(0);
    }

    SE_DEBUG(group_mixer, "@ %s Encoder->Apply=%s Type:%d\n",
             PcmPlayer_c::LookupOutputEncoding(OutputEncoding),
             Encoder->Apply == ACC_MME_ENABLED ? "ENABLED" : "DISABLED",
             ACC_ENCODERPP_GET_TYPE(Encoder));

    return (Encoder->StructSize + Transcoder->StructSize);
}

int PcmProc::FillOutBassMgtPPparams(MME_ExtBassMgtGlobalParams_t *BassMgt,
                                    PcmPlayer_c::OutputEncoding OutputEncoding,
                                    uint32_t MixerPlayerIndex,
                                    const stm_se_bassmgt_t &BassMgtConfig)
{
    SE_ASSERT(BassMgt != NULL);

    BassMgt->Id         = PCMPROCESS_SET_ID(ACC_PCM_BASSMGT_ID, MixerPlayerIndex);
    BassMgt->StructSize = sizeof(*BassMgt);

    if (PcmPlayer_c::IsOutputBypassed(OutputEncoding) || BassMgtConfig.apply == false)
    {
        BassMgt->Apply = ACC_MME_DISABLED;
        return BassMgt->StructSize;
    }

    //const int BASSMGT_ATTENUATION_MUTE = 9600;
    BassMgt->Apply = ACC_MME_ENABLED;

    switch (BassMgtConfig.type)
    {
    case STM_SE_BASSMGT_SPEAKER_BALANCE:
        BassMgt->Type = BASSMGT_VOLUME_CTRL;
        break;

    case STM_SE_BASSMGT_DOLBY_1:
        BassMgt->Type = BASSMGT_DOLBY_1;
        break;

    case STM_SE_BASSMGT_DOLBY_2:
        BassMgt->Type = BASSMGT_DOLBY_2;
        break;

    case STM_SE_BASSMGT_DOLBY_3:
        BassMgt->Type = BASSMGT_DOLBY_3;
        break;

    default:
        SE_ERROR("Unexpected BassMgt type\n");
    }

    // this is rather naughty (the gain values are linear everywhere else but are mapped onto a logarithmic scale here)
    BassMgt->VolumeInmB  = 1; // attenuation in mB

    // Implementation choice: the delay is not applied to ExtMME_BassMgtGlobalParams_t FW component but
    // to MME_DelayGlobalParams_t FW component which is practically implemented in the FW
    // as a BassMgt component dedicated to the delay
    BassMgt->DelayEnable = ACC_MME_FALSE;
    BassMgt->LfeOut      = ACC_MME_TRUE;
    BassMgt->BoostOut    = (true == BassMgtConfig.boost_out) ? ACC_MME_ENABLED : ACC_MME_DISABLED;
    BassMgt->Prologic    = ACC_MME_TRUE;
    BassMgt->WsOut       = BassMgtConfig.ws_out;
    BassMgt->Limiter     = 0;
    BassMgt->InputRoute  = ACC_MIX_MAIN;
    BassMgt->OutputRoute = ACC_MIX_MAIN;

    for (uint32_t ChannelIdx(0); ChannelIdx < VOLUME_NB_CONFIG_ELEMENTS; ChannelIdx++)
    {
        // remap ALSA channels definition to FW channels definition
        // STKPI uses a signed gain value whereas we are setting a positive attenuation
        // (explains why we use a negative value below)
        if (ChannelIdx < SND_PSEUDO_MIXER_CHANNELS)
        {
            BassMgt->Volume[ChannelIdx] = -BassMgtConfig.gain[remap_channels[ChannelIdx]]; // in mB
        }
        else
        {
            BassMgt->Volume[ChannelIdx] = 0;
        }
    }

    // From ACC-BL023-3BD
    BassMgt->CutOffFrequency = BassMgtConfig.cut_off_frequency;
    BassMgt->FilterOrder     = BassMgtConfig.filter_order;
    SE_DEBUG(group_mixer, "BassMgt enabled, Volume in mB (%d)(%d)(%d)(%d)-(%d)(%d)(%d)(%d)\n",
             BassMgt->Volume[0], BassMgt->Volume[1], BassMgt->Volume[2], BassMgt->Volume[3],
             BassMgt->Volume[4], BassMgt->Volume[5], BassMgt->Volume[6], BassMgt->Volume[7]);
    return BassMgt->StructSize;
}

int PcmProc::FillOutDelayPPparams(MME_DelayGlobalParams_t *Delay,
                                  uint32_t MixerPlayerIndex,
                                  const stm_se_bassmgt_t &BassMgtConfig)
{
    SE_ASSERT(Delay != NULL);

    Delay->Id          = PCMPROCESS_SET_ID(ACC_PCM_DELAY_ID, MixerPlayerIndex);
    Delay->StructSize  = sizeof(*Delay);
    Delay->Apply       = ACC_MME_ENABLED;
    Delay->DelayUpdate = ACC_MME_TRUE;

    // Implementation choice: this FW component is always instantiated but, by default, delay is set to 0 for all channels
    memset(&Delay->Delay[0], 0, sizeof(Delay->Delay));
    if (STM_SE_CTRL_VALUE_APPLY == BassMgtConfig.delay_enable)
    {
        for (uint32_t ChannelIdx(0); ChannelIdx < SND_PSEUDO_MIXER_CHANNELS; ChannelIdx++)
        {
            // remap ALSA channels definition to FW channels definition
            Delay->Delay[ChannelIdx] = BassMgtConfig.delay[remap_channels[ChannelIdx]]; // in msec
        }

        SE_DEBUG(group_mixer, ">Delay enabled, delay in msec (%d)(%d)(%d)(%d)-(%d)(%d)(%d)(%d)\n",
                 Delay->Delay[0], Delay->Delay[1], Delay->Delay[2], Delay->Delay[3],
                 Delay->Delay[4], Delay->Delay[5], Delay->Delay[6], Delay->Delay[7]);
    }
    return Delay->StructSize;
}

int PcmProc::FillOutResamplex2PPparams(MME_Resamplex2GlobalParams_t *Resamplex2,
                                       PcmPlayer_c::OutputEncoding OutputEncoding,
                                       uint32_t MixerPlayerIndex,
                                       uint32_t MixerSamplingFrequency,
                                       uint32_t PcmProcTargetSampleRate)
{
    SE_ASSERT(Resamplex2 != NULL);

    Resamplex2->Id = PCMPROCESS_SET_ID(ACC_PCM_RESAMPLE_ID, MixerPlayerIndex);
    Resamplex2->StructSize = sizeof(*Resamplex2);
    SE_DEBUG(group_mixer, "## SampleRateHz %d MixerSamplingFrequency:%d\n", PcmProcTargetSampleRate, MixerSamplingFrequency);

    if (true == PcmPlayer_c::IsOutputBypassed(OutputEncoding))
    {
        Resamplex2->Apply = ACC_MME_DISABLED;
    }
    else if (PcmProcTargetSampleRate == MixerSamplingFrequency)
    {
        Resamplex2->Apply = ACC_MME_DISABLED;
    }
    else
    {
        if ((OutputEncoding == PcmPlayer_c::OUTPUT_AC3)    ||
            (OutputEncoding == PcmPlayer_c::OUTPUT_DDPLUS) ||
            (OutputEncoding == PcmPlayer_c::OUTPUT_DTS))
        {
            // When the outputs are encoded then we have to deploy resampling at the beginning of the PCM
            // chain. Thus if the output rate differs from the mixers native rate then we need to deploy.
            Resamplex2->Apply = ACC_MME_ENABLED;
        }
        else
        {
            // it is important to use _AUTO here (rather than conditionally selecting _ENABLE ourselves)
            // because _AUTO also leaves the firmware free to apply the resampling at the point it believes is
            // best.using _ENABLE causes the processing to unconditionally applied at the start of the chain.
            Resamplex2->Apply = ACC_MME_AUTO;
        }
        Resamplex2->Range = TranslateIntegerSamplingFrequencyToRange(PcmProcTargetSampleRate);
    }

    SE_DEBUG(group_mixer,  "%s post-mix resampling (Range:%s Target:%dHz)\n",
             (Resamplex2->Apply == ACC_MME_ENABLED ? "Enabled" :
              Resamplex2->Apply == ACC_MME_AUTO ? "Automatic" : "Disabled"),
             LookupDiscreteSamplingFrequencyRange((enum eFsRange) Resamplex2->Range),
             PcmProcTargetSampleRate);

    return Resamplex2->StructSize;
}

int PcmProc::FillOutBtscPPparams(MME_BTSCGlobalParams_t *Btsc,
                                 uint32_t MixerPlayerIndex,
                                 const stm_se_btsc_t &audioplayerBtscConfig,
                                 const stm_se_ctrl_audio_player_hardware_mode_t &audioplayerHwMode,
                                 uint32_t CardMaxFreq)
{
    SE_ASSERT(Btsc != NULL);

    Btsc->Id         = PCMPROCESS_SET_ID(ACC_PCMPRO_BTSC_ID, MixerPlayerIndex);
    Btsc->StructSize = sizeof(*Btsc);
    Btsc->Apply      = ACC_MME_DISABLED;
    Btsc->DualSignal = ACC_MME_FALSE;
    Btsc->OutAt192K  = ACC_MME_FALSE;

    if ((STM_SE_PLAYER_I2S == audioplayerHwMode.player_type) && (STM_SE_PLAY_BTSC_OUT == audioplayerHwMode.playback_mode))
    {
        Btsc->Apply = ACC_MME_ENABLED;
    }

    if (audioplayerBtscConfig.dual_signal)
    {
        Btsc->DualSignal = ACC_MME_TRUE;
    }

    Btsc->InputGain = audioplayerBtscConfig.input_gain;
    Btsc->TXGain    = audioplayerBtscConfig.tx_gain;
    Btsc->GainInMB  = ACC_MME_TRUE;

    if (CardMaxFreq >= 192000)
    {
        Btsc->OutAt192K = ACC_MME_TRUE;
    }

    SE_DEBUG(group_mixer, "BTSC Config Apply = %d, DualSignal = %d, InputGain = 0x%x, TxGain = 0x%x\n",
             Btsc->Apply, Btsc->DualSignal, Btsc->InputGain, Btsc->TXGain);

    return Btsc->StructSize;
}

int PcmProc::FillOutCMCparams(MME_CMCGlobalParams_t       *CMC,
                              PcmPlayer_c::OutputEncoding OutputEncoding,
                              uint32_t                    MixerPlayerIndex,
                              const stm_se_drc_t          &audioplayerdrcConfig,
                              stm_se_dual_mode_t          player_dualmode,
                              int                         isStreamDrivenDualMono,
                              int32_t                     CardTargetLevel,
                              enum eAccAcMode             CardAcMode,
                              uint32_t                    CardChannelAssignmentPair0,
                              enum eAccAcMode             DecoderAcMode)
{
    SE_ASSERT(CMC != NULL);

    CMC->Id          = PCMPROCESS_SET_ID(ACC_PCM_CMC_ID, MixerPlayerIndex);
    CMC->StructSize  = sizeof(*CMC);

    CMC->ComprMode   = StmSeAudioGetFwDrcCode(audioplayerdrcConfig.mode);
    if (CardTargetLevel <= -3100)
    {
        CMC->TargetLevel = 31;
    }
    else if (CardTargetLevel >= 0)
    {
        CMC->TargetLevel =  0;
    }
    else
    {
        CMC->TargetLevel = -CardTargetLevel / 100; // audio-player target-level in in mB
    }
    CMC->CutFactor   = audioplayerdrcConfig.cut;   // Cut factor is in uQ8
    CMC->BoostFactor = audioplayerdrcConfig.boost; // Boost factor is in uQ8

    switch (player_dualmode)
    {
    case STM_SE_STEREO_OUT:
        CMC->DualMode = ACC_DUAL_LR;
        break;

    case STM_SE_DUAL_LEFT_OUT:
        CMC->DualMode = ACC_DUAL_LEFT_MONO;
        break;

    case STM_SE_DUAL_RIGHT_OUT:
        CMC->DualMode = ACC_DUAL_RIGHT_MONO;
        break;

    case STM_SE_MONO_OUT:
        CMC->DualMode = ACC_DUAL_MIX_LR_MONO;
        break;

    default:
        SE_ERROR("Invalid Dual Mode from Audio_Player\n");
        break;
    }


    if (isStreamDrivenDualMono)
    {
        CMC->DualMode = CMC->DualMode | ACC_DUAL_1p1_ONLY;
    }

    CMC->PcmDownScaled    = ACC_MME_FALSE;
    CMC->CenterMixCoeff   = ACC_M3DB;
    CMC->SurroundMixCoeff = ACC_M3DB;
    CMC->GlobalGainCoeff  = ACC_UNITY;
    CMC->LfeMixCoeff      = ACC_UNITY;

    switch (OutputEncoding)
    {
    case PcmPlayer_c::OUTPUT_FATPIPE:
    {
        SE_ERROR("OUTPUT_FATPIPE not supported\n");
        CMC->OutMode = ACC_MODE_ID;
    }
    break;

    case PcmPlayer_c::OUTPUT_AC3:
    case PcmPlayer_c::OUTPUT_DTS:
    {
        CMC->OutMode = ACC_MODE_ALL6; /* malleable output up to 5.1 */
    }
    break;

    case PcmPlayer_c::OUTPUT_DDPLUS:
    {
        CMC->OutMode = CardAcMode;
    }
    break;

    default:
    {
        CMC->OutMode = CardAcMode;

#ifndef BUG_4518

        // The downmixer in the current firmware doesn't support 7.1 outputs (can't automatically generate
        // downmix tables for them). We therefore need to fall back to ACC_MODE32(_LFE) unless the decoder
        // is already chucking out samples in the correct mode.
        // However if we have custom downmix matrix then we won't have to do this.
        if ((ACC_MODE34 == CMC->OutMode || ACC_MODE34_LFE == CMC->OutMode))
        {
            if (CMC->OutMode != DecoderAcMode)
            {
                if (ACC_MODE34_LFE == CMC->OutMode)
                {
                    if (ACC_MODE34 == DecoderAcMode)
                    {
                        CMC->OutMode = ACC_MODE34;
                    }
                    else
                    {
                        CMC->OutMode = ACC_MODE32_LFE;
                    }
                }
                else
                {
                    CMC->OutMode = ACC_MODE32;
                }
            }
        }

#endif
        break;
    } // end of default
    } // end of switch

    SE_DEBUG(group_mixer,  "CMC output mode (%d): %s - Dual mode=%d (1p1_only=%d)\n",
             MixerPlayerIndex,
             LookupAudioMode((eAccAcMode) CMC->OutMode),
             (CMC->DualMode & (ACC_DUAL_LEFT_MONO | ACC_DUAL_RIGHT_MONO)),
             ((CMC->DualMode & ACC_DUAL_1p1_ONLY) == ACC_DUAL_1p1_ONLY));

    return CMC->StructSize;
}

int PcmProc::FillOutLimiterPPparams(MME_LimiterGlobalParams_t   *Limiter,
                                    PcmPlayer_c::OutputEncoding OutputEncoding,
                                    uint32_t                    MixerPlayerIndex,
                                    const stm_se_limiter_t      &audioplayerLimiterConfig,
                                    int                         ApplicationType,
                                    int32_t                     CardTargetLevel,
                                    int                         Gain,
                                    int                         delay,
                                    int                         softmute,
                                    stm_se_player_sink_t        playerSinkType,
                                    bool                        IsConnectedToHdmi,
                                    bool                        IsConnectedToSpdif,
                                    bool                        ForceUnitaryHardGain)
{
    SE_ASSERT(Limiter != NULL);

    Limiter->Id         = PCMPROCESS_SET_ID(ACC_PCM_LIMITER_ID, MixerPlayerIndex);
    Limiter->StructSize = sizeof(*Limiter);
    Limiter->Apply      = ACC_MME_ENABLED;

    if (PcmPlayer_c::IsOutputBypassed(OutputEncoding) || audioplayerLimiterConfig.apply == 0)
    {
        Limiter->Apply = ACC_MME_DISABLED;
    }
    Limiter->LimiterEnable   = ACC_MME_TRUE;
    Limiter->SvcEnable       = ACC_MME_TRUE;
    Limiter->SoftMute        = softmute; // soft mute is triggered by a 0 to 1 transition, and unmute by a 1 to 0 transition
    Limiter->GainInMilliBell = 1; //Gain is given in mB

    Limiter->Gain            = Gain;
    SE_DEBUG(group_mixer, ">Card%d:  LimiterGain (%d)\n",
             MixerPlayerIndex,
             Limiter->Gain);
    Limiter->DelayEnable     = 1;  // enable delay engine
    Limiter->MuteDuration    = MIXER_LIMITER_MUTE_RAMP_DOWN_PERIOD;
    Limiter->UnMuteDuration  = MIXER_LIMITER_MUTE_RAMP_UP_PERIOD;
    SE_DEBUG(group_mixer, "< Limiter Gain before target_level modificators= %d >\n", Limiter->Gain);

    if (CardTargetLevel)
    {
        // when (target_level != 0), the target playback level uses the value provided
        // by the control STM_SE_CTRL_AUDIO_PROGRAM_PLAYBACK_LEVEL
        // The actual gain to apply will be adjusted wrt actual audio dialog-level

        Limiter->AdjustProgramRefLevel = ACC_MME_TRUE;
        Limiter->Gain += CardTargetLevel; // Combine User-Requested volume and audio-player target-level
        SE_DEBUG(group_mixer, "card[%d] : target_level=%d, AdjustProgramRefLevel=%d\n",
                 MixerPlayerIndex, CardTargetLevel, Limiter->AdjustProgramRefLevel);
    }
    else
    {
        //when (target_level = 0), the target playback level is driven by AUDIO_APPLICATION_TYPE
        if (ApplicationType < NB_AUDIO_APPLICATION_TYPES)
        {
            I32 gainAdjust = 0;
            Limiter->AdjustProgramRefLevel = LimiterGainAdjustLookupTable[ApplicationType].AdjustPRL;

            switch (playerSinkType)
            {
            case STM_SE_PLAYER_SINK_AUTO:
            {
                if (ApplicationType == STM_SE_CTRL_VALUE_AUDIO_APPLICATION_MS12 &&
                    (OutputEncoding == PcmPlayer_c::OUTPUT_AC3   ||
                     OutputEncoding == PcmPlayer_c::OUTPUT_DTS   ||
                     OutputEncoding == PcmPlayer_c::OUTPUT_DDPLUS))
                {
                    // In case of Post Mixer Encode, we force PRL for AVR even for HDMI
                    gainAdjust = LimiterGainAdjustLookupTable[ApplicationType].PRL_AVR;
                    SE_DEBUG(group_mixer, "COMPRESSED OUTPUT ==> PRL for AVR\n");
                }
                // Connected to HDMI Only: default output = TV
                else if (IsConnectedToHdmi)
                {
                    gainAdjust = LimiterGainAdjustLookupTable[ApplicationType].PRL_TV;
                    SE_DEBUG(group_mixer, "Default HDMI PRL  ==> TV\n");
                }
                // Connected to SPDIF : default output = AVR
                else if (IsConnectedToSpdif)
                {
                    gainAdjust = LimiterGainAdjustLookupTable[ApplicationType].PRL_AVR;
                    SE_DEBUG(group_mixer, "Default SPDIF PRL ==> AVR\n");
                }
                // Connected to Analog: default output = TV
                else
                {
                    // Analog is considered as TVOuput (historical)
                    gainAdjust = LimiterGainAdjustLookupTable[ApplicationType].PRL_TV;
                    SE_DEBUG(group_mixer, "Default Analog PRL ==> TV\n");
                }

                break;
            }

            case STM_SE_PLAYER_SINK_TV:
                // Forced case : connected to TV
                gainAdjust = LimiterGainAdjustLookupTable[ApplicationType].PRL_TV;
                SE_DEBUG(group_mixer, "Apply PRL_TV\n");
                break;

            case STM_SE_PLAYER_SINK_AVR:
                // Forced case : connected to AVR
                gainAdjust = LimiterGainAdjustLookupTable[ApplicationType].PRL_AVR;
                SE_DEBUG(group_mixer, "Apply PRL_AVR\n");
                break;

            case STM_SE_PLAYER_SINK_HEADPHONE:
                // Forced case : connected to HeadPhone
                gainAdjust = LimiterGainAdjustLookupTable[ApplicationType].PRL_HeadPhone;
                SE_DEBUG(group_mixer, "Apply PRL_TV\n");
                break;

            default:
                SE_ERROR("Invalid Player Sink Type: %d\n", playerSinkType);
                break;
            }

            Limiter->Gain += gainAdjust;
            SE_DEBUG(group_mixer, "card[%d]: ApplicationType=%s, AdjustProgramRefLevel=%d, GainAdjust=%d\n",
                     MixerPlayerIndex ,
                     LookupPolicyValueAudioApplicationType(ApplicationType),
                     Limiter->AdjustProgramRefLevel,
                     gainAdjust);
        }

        // Limiter->Gain value should remains between [+3200 ,- 9600] mB
        if (Limiter->Gain < -9600)
        {
            Limiter->Gain = -9600;
        }
        else if (Limiter->Gain > 3200)
        {
            Limiter->Gain = 3200;
        }

        SE_DEBUG(group_mixer, "</card[%d]: Limiter Gain after target_level modificators= %d >\n\n",
                 MixerPlayerIndex, Limiter->Gain);

        // During initialization (i.e. when MMEInitialized is false) we must set HardGain to 1. This
        // prevents the Limiter from setting the initial gain to -96db and applying gain smoothing
        // (which takes ~500ms to reach 0db) during startup.
        // After initialization, the HardGain is driven using the STKPI limiter control.
        Limiter->HardGain = (ForceUnitaryHardGain) ? 1 : (uint32_t) audioplayerLimiterConfig.hard_gain;
        //  Limiter->Threshold = ;
        Limiter->DelayBuffer  = NULL; // delay buffer will be allocated by firmware
        Limiter->DelayBufSize = 0;
        Limiter->Delay        = delay; //in ms
    }
    return Limiter->StructSize;
}
