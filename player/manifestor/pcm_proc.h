/************************************************************************
Copyright (C) 2003-2014 STMicroelectronics. All Rights Reserved.

This file is part of the Streaming Engine.

Streaming Engine is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License version 2 as
published by the Free Software Foundation.

Streaming Engine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with Streaming Engine; see the file COPYING.  If not, write to the Free
Software Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307,
USA.

The Streaming Engine Library may alternatively be licensed under a
proprietary license from ST.
************************************************************************/

#ifndef H_PCMPROC_CLASS
#define H_PCMPROC_CLASS

#include <ACC_Transformers/Pcm_PostProcessingTypes.h>
#include <ACC_Transformers/Pcm_TuningProcessingTypes.h>
#include <ACC_Transformers/AudioMixer_ProcessorTypes.h>
#include <ACC_Transformers/DolbyDigitalPlus_EncoderTypes.h>

#include "pcmplayer.h"

//Freeze this structure as it is now.
//Otherwise, changes to AudioMixer_ProcessorTypes.h that inserted structures
//could cause huge problems at aggregation (because lack of initialized size
//field on substructures means we can't even skip over them)
//Of course, remain vulnerable to changes in higher structures, but I'm
//taking a bet they'll change less
typedef struct
{
    enum eAccMixerId               Id;                //!< Id of the PostProcessing structure.
    U16                            StructSize;        //!< Size of this structure
    U8                             NbPcmProcessings;  //!< NbPcmProcessings on main[0..3] and aux[4..7]
    U8                             AuxSplit;          //!< Point of split between Main output and Aux output
    MME_ExtBassMgtGlobalParams_t   BassMgt;
    MME_EqualizerGlobalParams_t    Equalizer;
    MME_TempoGlobalParams_t        TempoControl;
    MME_DCRemoveGlobalParams_t     DCRemove;
    MME_DelayGlobalParams_t        Delay;
    MME_EncoderPPGlobalParams_u    Encoder;
    MME_TranscodePPGlobalParams_t  Transcoder;
    MME_SfcPPGlobalParams_t        Sfc;
    MME_Resamplex2GlobalParams_t   Resamplex2;
    MME_CMCGlobalParams_t          CMC;
    MME_DMixGlobalParams_t         Dmix;
    MME_FatpipeGlobalParams_t      FatPipeOrSpdifOut;
    MME_LimiterGlobalParams_t      Limiter;
    MME_BTSCGlobalParams_t         Btsc;
    MME_METARENCGlobalParams_t     MetaRencode; // DDRE and DTSMetadata
    // Tuning Profile Parmaters
    MME_VolumeManagersParams_u     VolumeManagerParams;
    MME_VirtualizerParams_u        VirtualizerParams;
    MME_UpmixerParams_u            UpmixerParams;
    MME_DialogEnhancerParams_u     DialogEnhancerParams;
    MME_EqualizerParams_u          EqualizerParams;
    MME_BassEnhancerParams_u       BassEnhancerParams;
    // Tuning Amount parameters
    MME_TuningGlobalParams_t       VolumeManagerGlobalParams;
    MME_TuningGlobalParams_t       VirtualizerGlobalParams;
    MME_TuningGlobalParams_t       UpmixerGlobalParams;
    MME_TuningGlobalParams_t       DialogEnhancerGlobalParams;
    MME_TuningGlobalParams_t       EqualizerGlobalParams;
    MME_TuningGlobalParams_t       BassEnhancerGlobalParams;
} MME_LxPcmPostProcessingGlobalParameters_Frozen_t; //!< PcmPostProcessings Params


/* re-map channels between ALSA and the audio firmware (swap 2,3 with 4,5) */
static const uint32_t remap_channels[SND_PSEUDO_MIXER_CHANNELS] =
{
    ACC_MAIN_LEFT,
    ACC_MAIN_RGHT,
    ACC_MAIN_LSUR,
    ACC_MAIN_RSUR,
    ACC_MAIN_CNTR,
    ACC_MAIN_LFE,
    ACC_MAIN_CSURL,
    ACC_MAIN_CSURR
};

class PcmProc
{
public:
    static void ResetDevicePcmParameters(MME_LxPcmPostProcessingGlobalParameters_Frozen_t *PcmParams,
                                         uint32_t                                         MixerPlayerIndex);

    static void DumpMixerPostProcStruct(const MME_LxPcmPostProcessingGlobalParameters_Frozen_t *params,
                                        uint32_t                                               HeaderSize);

    static int FillOutEncoderPPparams(MME_EncoderPPGlobalParams_u   *EncoderPP,
                                      MME_TranscodePPGlobalParams_t *Transcoder,
                                      PcmPlayer_c::OutputEncoding   OutputEncoding,
                                      uint32_t                      MixerPlayerIndex,
                                      bool                          useTranscoder,
                                      int                           ApplicationType,
                                      enum eAccAcMode               acmod);

    static int FillOutFillOutTranscoderPPparams(MME_TranscodePPGlobalParams_t *Transcoder,
                                                PcmPlayer_c::OutputEncoding   OutputEncoding,
                                                uint32_t                      MixerPlayerIndex);

    static int FillOutBassMgtPPparams(MME_ExtBassMgtGlobalParams_t *BassMgt,
                                      PcmPlayer_c::OutputEncoding  OutputEncoding,
                                      uint32_t                     MixerPlayerIndex,
                                      const stm_se_bassmgt_t       &BassMgtConfig);

    static int FillOutDelayPPparams(MME_DelayGlobalParams_t *Delay,
                                    uint32_t                MixerPlayerIndex,
                                    const stm_se_bassmgt_t  &BassMgtConfig);

    static int FillOutResamplex2PPparams(MME_Resamplex2GlobalParams_t *Resamplex2,
                                         PcmPlayer_c::OutputEncoding  OutputEncoding,
                                         uint32_t                     MixerPlayerIndex,
                                         uint32_t                     MixerSamplingFrequency,
                                         uint32_t                     PcmProcTargetSampleRate);

    static int FillOutBtscPPparams(MME_BTSCGlobalParams_t                         *Btsc,
                                   uint32_t                                       MixerPlayerIndex,
                                   const stm_se_btsc_t                            &audioplayerBtscConfig,
                                   const stm_se_ctrl_audio_player_hardware_mode_t &audioplayerHwMode,
                                   uint32_t                                       CardMaxFreq);

    static int FillOutCMCparams(MME_CMCGlobalParams_t       *CMC,
                                PcmPlayer_c::OutputEncoding OutputEncoding,
                                uint32_t                    MixerPlayerIndex,
                                const stm_se_drc_t          &audioplayerdrcConfig,
                                stm_se_dual_mode_t          player_dualmode,
                                int                         isStreamDrivenDualMono,
                                int32_t                     CardTargetLevel,
                                enum eAccAcMode             CardAcMode,
                                uint32_t                    CardChannelAssignmentPair0,
                                enum eAccAcMode             DecoderAcMode);

    static int FillOutLimiterPPparams(MME_LimiterGlobalParams_t   *Limiter,
                                      PcmPlayer_c::OutputEncoding OutputEncoding,
                                      uint32_t                    MixerPlayerIndex,
                                      const stm_se_limiter_t      &audioplayerLimiterConfig,
                                      int                         ApplicationType,
                                      int32_t                     CardTargetLevel,
                                      int                         Gain,
                                      int                         delay,
                                      int                         softmute,
                                      stm_se_player_sink_t        playerSinkType,
                                      bool                        IsConnectedToHdmi,
                                      bool                        IsConnectedToSpdif,
                                      bool                        ForceUnitaryHardGain);
};
#endif // H_PCMPROC_CLASS
