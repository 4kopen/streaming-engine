/************************************************************************
Copyright (C) 2003-2014 STMicroelectronics. All Rights Reserved.

This file is part of the Streaming Engine.

Streaming Engine is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License version 2 as
published by the Free Software Foundation.

Streaming Engine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with Streaming Engine; see the file COPYING.  If not, write to the Free
Software Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307,
USA.

The Streaming Engine Library may alternatively be licensed under a
proprietary license from ST.
************************************************************************/

#include "output_coordinator_base.h"
#include "player_generic.h"
#include "player_playback.h"
#include "player_stream.h"
#include "st_relayfs_se.h"
#include "timestamps.h"

#undef TRACE_TAG
#define TRACE_TAG           "OutputCoordinator_Base_c"

#ifndef UNITTESTS
#define SYNCHRONIZE_WAIT                                30              // 4 waits of 30ms
#else
#define SYNCHRONIZE_WAIT                                0
#endif

#define MAX_SYNCHRONIZE_WAITS_LIVE                      20              // targeting the reception of a PCR within 600ms
#define MAX_SYNCHRONIZE_WAITS                           10               // wait 10 x 30 ms in case of non live.
#define MAX_SYNCHRONIZE_WAITS_VIDEO_START_IMMEDIATE     30              // time we allow until the first video frame is decoded.
// If this has not happened in time, audio will generate its mapping

#define MAX_MANIFESTOR_LATENCY                          100000
#define NETWORK_DEFAULT_LATENCY                         100000          // Default network latency
#define PROPAGATION_TIME_LATENCY                        30000           // TE accumulates 30ms of data before sending downstream
#define PROPAGATION_TIME_JITTER_MARGIN                  20000           // Jitter margin to avoid starvations
#define MINIMUM_TIME_TO_MANIFEST                        100000          // I am guessing that it will take at least 100 ms to get from synchronize to the manifestor
#define INTERNAL_TIMESTAMP_LATENCY_ESTIMATE             10000           // 10ms

#define REBASE_TIME_TRIGGER_VALUE                       40000000        // we need to rebase the time mapping periodically so that the delta with PTS used for the time mapping
// is never more than half the wrap around period. Since 27MHz PTS wrap around every 159s, we rebase every 40s

#define PCR_MONITORING_THRESHOLD                        500000          // if PCR to STC delta grows to more than 500ms vs initial delta, it will reset the mapping
#define PCR_FORWARD_JUMP_THRESHOLD                      1000000         // 1sec forward PCR jump

#define MINIMUM_SLEEP_TIME_US                           10000

/* With 16 second MINIMUM_INTEGRATION_TIME and 512 MINIMUM_POINTS at max  PPM(72)
And minimum 1 PCR data point every 100ms as per ISO 13818-1 standard (PCR reception is of at least 10 Hz)
Max (72*51  + 54*51  + 36*64 + 18*128 =) 11.059 ms delay after full clock recovery.*/
#define CLOCK_RECOVERY_MINIMUM_POINTS                   16
#define CLOCK_RECOVERY_MINIMUM_DURATION                 16000000 // 16s
#define CLOCK_RECOVERY_MAXIMUM_POINTS                   36000 // 1H at 10Hz
#define CLOCK_RECOVERY_MINIMUM_INTEGRATION_TIME         (  16 * 1000000)
#define CLOCK_RECOVERY_MAXIMUM_INTEGRATION_TIME         (2048 * 1000000)
#define CLOCK_RECOVERY_MAXIMUM_PPM                      72          // A parts per million value controlling the sanity check for clock recovery gradient
#define CLOCK_RECOVERY_PPM_STABILITY_THRESHOLD          1ULL
#define CLOCK_RECOVERY_PPM_STABILITY_THRESHOLD_SQR      (CLOCK_RECOVERY_PPM_STABILITY_THRESHOLD*CLOCK_RECOVERY_PPM_STABILITY_THRESHOLD)
/* PCR mapping request should not be performed more often than once every 3 seconds */
#define PCR_MAPPING_TOO_CLOSE_DELTA_THRESHOLD           3000000
#define PCR_MAPPING_TOO_CLOSE_COUNT_THRESHOLD           2
#define MAX_DELTA_BETWEEN_PCR_US                        1000000  // PCR should not be separated by more than 1s (TS spec says 100ms, actually)
#define MAX_DISCARDED_PCR_ON_BIG_JUMPS                  1 // Maximum number of PCR points to be discarded when encountering a big jump
#define MAX_CONSECUTIVE_PCR_BIG_JUMPS                   1 // Maximum consecutive unconfirmed big jump before mapping reset
#define MAX_TRANSITION_STATE_COUNTER                    250 // Arbitrary value : we don't want to remain in transition forever
#define MAX_INVALID_TRANSITION_REQUESTS                 4 // Transition requests from unstable state finally trigger a drain
#define CLOCK_RECOVERY_DISCARD_COUNT_THRESHOLD          4 // Maximum consecutive datapoint discard until reset of clock recovery
#define CLOCK_ADJUSTMENT_MINIMUM_DURATION               200000 // 200ms
#define CLOCK_ADJUSTMENT_MINIMUM_POINTS                 16
#define CLOCK_ADJUSTMENT_MAXIMUM_POINTS                 36000 // 1H at 10Hz

#define ORA_MAXIMUM_PPM                                 72  // A parts per million value controlling the sanity check for ORA gradient
#define ORA_PPM_STABILITY_THRESHOLD                     1ULL
#define ORA_PPM_STABILITY_THRESHOLD_SQR                 (ORA_PPM_STABILITY_THRESHOLD*ORA_PPM_STABILITY_THRESHOLD)

/* Latency in micro sec to be applied for decode and no decode mode
   Note this need to be fine tuned */
#define HW_DELAY 0
/* Worst case latency for 32kHz(384 Samples) 1 frame in capture(12ms) + 2 frame in mixer grain(24ms) + 0ms HW_DELAY + mixer time */
#define HDMIRX_BASE_LATENCY               (12000 + HW_DELAY + 24000 + 7500)
/* For no decode mode the decoding stage is configured to be minimal , still we assume the decoding of one grain to take 4ms */
#define HDMIRX_NO_DECODE_MODE_LATENCY     ( 4000 + HDMIRX_BASE_LATENCY)
/* For decode mode, the decoding stage will really decode the compressed data received from HDMIRx and to do so, it requires to
 * capture several input grains to be collate complete compressed frames, then the decoding itself being realtime, it will take at best
 * one encoded frame time to decode  : HDMIRx supports AAC, DD and DD+ , and worst case frame time is 6 bufffers of 384 samples at 32kHz so 72ms */
#define HDMIRX_DECODE_MODE_LATENCY        (72000 + HDMIRX_BASE_LATENCY)

#define MODULES_PROPAGATION_LATENCY     1000    // 1 ms latency to pass buffer between the modules

#define MAXIMUM_DECODE_WINDOW_SLEEP     200000  // 200 milliseconds
#define ERRONEOUS_MAPPING_WINDOW_SLEEP  MAX_STREAM_OFFSET_US // requesting as much sleep is definite ly due to a wrong mapping
#define MAX_ERRONEOUS_MAPPING_REQUESTS  1 // 2 consecutive erroneous sleep request is too much

// AUX Latency is at max 85ms : Audio video synchronization is based on a worst case
// video latency (lowest MAIN fps, lowest AUX fps) as a consequence MAIN to AUX system
// latency is 1 x (1 / 24) + 2 x (1 / 50) = 85ms
#define MAX_AUX_LATENCY                    85
#define MAX_WAIT_TIME_MAPPING_SLEEP        120     // 120 milliseconds

#define INTEGRATION_PERIOD         32

#define StreamType() ((Context->StreamType == 1) ? "Audio" : "Video")

static int GradientToPartsPerMillion(Rational_t Gradient)
{
    // report by how many parts per million out argument differs from 1
    Rational_t PartsPerPart = Gradient - 1;
    Rational_t PartsPerMillion = PartsPerPart * 1000000;
    return PartsPerMillion.IntegerPart();
}
static Rational_t GradientToPartsPerMillionRational(Rational_t Gradient)
{
    // report by how many parts per million out argument differs from 1
    Rational_t PartsPerPart = Gradient - 1;
    Rational_t PartsPerMillion = PartsPerPart * 1000000;
    return PartsPerMillion;
}
//
OutputCoordinator_Base_c::OutputCoordinator_Base_c()
    : SynchronizeMayHaveCompleted()
    , StreamCount(0)
    , mRegisteredVideoStreamCount(0)
    , CoordinatedContexts(NULL)
    , StreamsInSynchronize(0)
    , MasterTimeMappingEstablished(false)
    , MasterTimeMappingEstablishedOnType(StreamTypeNone)
    , MasterBaseSystemTime(0)
    , MasterBasePts()
    , mPcrLastMappingDate(0)
    , mPcrCloseMappings(0)
    , mInitialDeltaToPcr(INVALID_TIME)
    , mSplicingOffset(0)
    , UseSystemClockAsMaster(false)
    , SystemClockAdjustmentEstablished(true)
    , SystemClockAdjustment(1)
    , Speed(1)
    , Direction(PlayForward)
    , mClockRecoveryInitialized(false)
    , mClockRecoveryBasePcr()
    , mClockRecoverySourceTimeFormat(TIME_FORMAT_US)
    , mClockRecoveryBaseLocalClock(INVALID_TIME)
    , mClockRecoveryAccumulatedPoints(0)
    , mClockRecoveryDiscardedPoints(0)
    , mClockRecoveryDiscardedPointsOnBigJumps(0)
    , mClockRecoveryConsecutiveUnconfirmedBigJumps(0)
    , mClockRecoveryLastSpuriousDelta(0)
    , mClockRecoveryLeastSquareFit()
    , mClockRecoveryEstablishedGradient(1)
    , mClockRecoveryLastPpm()
    , mClockRecoveryLastPpmIndex(0)
    , mClockRecoveryPCRInterpolator()
    , mClockRecoveryLastPcr()
    , mClockRecoveryLastLocalTime(INVALID_TIME)
    , mClockRecoveryNextPcr()
    , mClockRecoveryNextLocalTime(INVALID_TIME)
    , mClockRecoveryLastValidPcr()
    , mClockRecoveryLastValidLocalTime(INVALID_TIME)
    , mClockRecoveryPreviousPcr()
    , mClockRecoveryPreviousLocalTime(INVALID_TIME)
    , mClockRecoveryConfirmedBigJumpDate(INVALID_TIME)
    , LatencyUsedInCaptureModeDuringTimeMapping(0)
{
    OS_InitializeEvent(&SynchronizeMayHaveCompleted);
}

//
OutputCoordinator_Base_c::~OutputCoordinator_Base_c()
{
    OS_TerminateEvent(&SynchronizeMayHaveCompleted);
}

//
// This function does not fully revert this object to its initial state.  The
// CoordinatedContexts list is not cleaned up because contexts are referenced
// from both this list and OutputTimer_Base_c::mOutputCoordinatorContext and
// deleting them here would turn mOutputCoordinatorContext into a dangling
// pointer and cause double deletion.
// OutputTimer_c::DeRegisterOutputCoordinator() is instead responsible for
// calling DeRegisterStream() for removing its context from this
// list and deleting it.
//
// This function does not call BaseComponentClass_c::Reset() intentionally
// because this would reset the Playback and Player members which must point to
// valid objects for the lifetime of this object to avoid races.
//
// This function shall not take any lock that can be held while calling into
// PlayerPlayback_c.  See PlayerPlayback_c::mStreamListLock.
void OutputCoordinator_Base_c::ResetPrivateState()
{
    //
    // Initialize the system/playback time mapping and clock recovery state
    // machine.
    //
    StreamsInSynchronize                = 0;
    MasterTimeMappingEstablished        = false;
    MasterTimeMappingEstablishedOnType  = StreamTypeNone;
    SystemClockAdjustmentEstablished    = true;
    SystemClockAdjustment               = 1;
    Speed                               = 1;
    Direction                           = PlayForward;
    mClockRecoveryInitialized           = false;
    mClockRecoveryLastValidPcr          = TimeStamp_c();
    mClockRecoveryLastValidLocalTime    = INVALID_TIME;
    mClockRecoveryNextPcr               = TimeStamp_c();
    mClockRecoveryNextLocalTime         = INVALID_TIME;
    mClockRecoveryConfirmedBigJumpDate  = INVALID_TIME;
    mSplicingOffset                     = 0;

    //
    // To be on safe side we signal threads in the process of establishing time
    // mapping as resetting state might impact this algorithm.
    //
    OS_SetEvent(&SynchronizeMayHaveCompleted);
}

// /////////////////////////////////////////////////////////////////////////
//
//      the register a stream function, creates a new Coordinator context
//

OutputCoordinatorStatus_t   OutputCoordinator_Base_c::RegisterStream(PlayerStream_t                    Stream,
                                                                     PlayerStreamType_t                StreamType,
                                                                     OutputCoordinatorContext_t       *Context)
{
    ComponentMonitor(this);
    //
    // Obtain a new context
    //
    *Context    = NULL;

    OutputCoordinatorContext_t NewContext = new struct OutputCoordinatorContext_s(Stream, StreamType);
    if (NewContext == NULL)
    {
        SE_ERROR("Failed to obtain a new Coordinator context\n");
        return PlayerInsufficientMemory;
    }

    // Initialize non trivial fields of context
    NewContext->ManifestorLatency = MINIMUM_TIME_TO_MANIFEST;
    ResetTimingContext(NewContext);

    NewContext->ManifestationCoordinator = Stream->GetManifestationCoordinator();
    if (NewContext->ManifestationCoordinator == NULL)
    {
        SE_ERROR("Not able to retrieve ManifestationCoordinator\n");
        delete NewContext;
        return PlayerNotSupported;
    }

    if (StreamType == StreamTypeVideo)
    {
        mRegisteredVideoStreamCount++;
    }

    //
    // If there is an external time mapping in force,
    // then we copy it into this context.
    //
    int ExternalMapping  = Player->PolicyValue(Playback, NewContext->Stream, PolicyExternalTimeMapping);

    if ((ExternalMapping == PolicyValueApply) && MasterTimeMappingEstablished)
    {
        NewContext->TimeMappingEstablished = MasterTimeMappingEstablished;
    }

    //
    // Insert the context into the list
    //
    // TODO(vgi): No protection on this list so far :(. Need to handle mutual exclusion properly
    NewContext->Next    = CoordinatedContexts;
    CoordinatedContexts = NewContext;
    *Context            = NewContext;
    StreamCount++;

    if (Player->PolicyValue(Playback, PlayerAllStreams, PolicyExternalLatencyBehavior) == PolicyValueExternalLatencyDelayOtherStreams)
    {
        AdjustExternalLatency(0);
    }

    return OutputCoordinatorNoError;
}

// /////////////////////////////////////////////////////////////////////////
//
//      Function to de-register a stream.
//

void OutputCoordinator_Base_c::DeRegisterStream(OutputCoordinatorContext_t Context)
{
    ComponentMonitor(this);

    //
    // Make sure no one is trying to synchronize this stream
    //

    while (Context->InSynchronizeFn)
    {
        ComponentMonitor.UnLock();
        SE_INFO(group_avsync, "In Synchronize (%d)\n", Context->InSynchronizeFn);
        OS_SleepMilliSeconds(5);
        ComponentMonitor.Lock();
    }

    if (Context->StreamType == StreamTypeVideo)
    {
        mRegisteredVideoStreamCount--;
    }
    //
    // Remove the context from the list
    //

    OutputCoordinatorContext_t *PointerToContext;
    for (PointerToContext   = &CoordinatedContexts;
         *PointerToContext != NULL;
         PointerToContext   = &((*PointerToContext)->Next))
        if ((*PointerToContext) == Context)
        {
            *PointerToContext   = Context->Next;
            delete Context;
            //
            // Reduce stream count
            //
            StreamCount--;
            OS_SetEvent(&SynchronizeMayHaveCompleted);
            break;
        }

    //
    // Reverify the master clock
    //
    VerifyMasterClock();
}

// /////////////////////////////////////////////////////////////////////////
//
//      Function to set the speed
//

OutputCoordinatorStatus_t   OutputCoordinator_Base_c::SetPlaybackSpeed(OutputCoordinatorContext_t        Context,
                                                                       Rational_t                        Speed,
                                                                       PlayDirection_t                   Direction)
{
    ComponentMonitor(this);

    if (Context != PlaybackContext)
    {
        SE_ERROR("does not support stream specific speeds\n");
        return PlayerNotSupported;
    }

    //
    // Record the new speed and direction
    //
    this->Speed     = Speed;
    this->Direction = Direction;

    // Invalidate the master mapping
    MasterTimeMappingEstablished = false;

    if (Speed == 0)
    {
        mInitialDeltaToPcr = INVALID_TIME;
    }

    return OutputCoordinatorNoError;
}

// /////////////////////////////////////////////////////////////////////////
//
//      Function to reset the time mapping
//

void OutputCoordinator_Base_c::ResetStreamTimeMapping(OutputCoordinatorContext_t Context)
{
    SE_INFO(group_avsync, "Stream 0x%p - %d\n", Context->Stream, Context->StreamType);

    Context->TimeMappingEstablished = false;
    Context->mLastValidSystemTime = INVALID_TIME;
    Context->mLastValidDeltaSystemTime = 0;
    OS_SetEvent(&Context->AbortPerformEntryIntoDecodeWindowWait);
}

void OutputCoordinator_Base_c::ResetMasterTimeMapping()
{
    SE_INFO(group_avsync, "\n");

    MasterTimeMappingEstablished        = false;
    MasterTimeMappingEstablishedOnType  = StreamTypeNone;
    mSplicingOffset                     = 0;

    // Release anyone waiting to enter a decode window
    for (OutputCoordinatorContext_t ContextLoop = CoordinatedContexts; ContextLoop != NULL; ContextLoop = ContextLoop->Next)
    {
        ResetStreamTimeMapping(ContextLoop);
    }
}

bool OutputCoordinator_Base_c::NoStreamUsesMasterTimeMapping()
{
    for (OutputCoordinatorContext_t ContextLoop = CoordinatedContexts; ContextLoop != NULL; ContextLoop = ContextLoop->Next)
    {
        if (ContextLoop->TimeMappingEstablished == true) { return false; }
    }

    return true;
}

void OutputCoordinator_Base_c::ResetTimeMapping(OutputCoordinatorContext_t Context)
{
    ComponentMonitor(this);
    SE_DEBUG(group_avsync, "\n"); // no need for info level: trace already present in caller most of times,

    if (Context == PlaybackContext)
    {
        ResetMasterTimeMapping();
    }
    else
    {
        ResetStreamTimeMapping(Context);

        if (NoStreamUsesMasterTimeMapping())
        {
            ResetMasterTimeMapping();
        }
    }

    // A live mapping exists already ? If so the last PCR datapoint is reset
    // If not, we keep the most recent PCR
    if (Player->PolicyValue(Playback, PlayerAllStreams, PolicyLivePlayback) == PolicyValueApply
        && MasterBasePts.IsValid())
    {
        mClockRecoveryLastValidPcr                = TimeStamp_c();
        mClockRecoveryLastValidLocalTime          = INVALID_TIME;
    }
}

// /////////////////////////////////////////////////////////////////////////
//
//      Set a specific mapping
//

void OutputCoordinator_Base_c::EstablishTimeMapping(OutputCoordinatorContext_t  Context,
                                                    const TimeStamp_c          &Pts,
                                                    unsigned long long          SystemTime)
{
    ComponentMonitor(this);

    //
    // Set the master mapping as specified
    //
    MasterTimeMappingEstablished = false;
    unsigned long long Now       = OS_GetTimeInMicroSeconds();
    MasterBasePts                = Pts;
    MasterBaseSystemTime         = ValidTime(SystemTime) ? SystemTime : Now;
    MasterTimeMappingEstablished = true;

    if (Context == PlaybackContext)
    {
        MasterTimeMappingEstablishedOnType = StreamTypeNone;
        SE_INFO(group_se_pipeline, "Generated mapping (%lld,%llu)\n", MasterBasePts.NativeValue(), MasterBaseSystemTime);
    }
    else
    {
        MasterTimeMappingEstablishedOnType = Context->StreamType;
        SE_INFO(group_se_pipeline, "Stream 0x%p - %d - Generated mapping (%lld,%llu)\n",
                Context->Stream, Context->StreamType,
                MasterBasePts.NativeValue(), MasterBaseSystemTime);
    }

    //
    // Propagate this to the appropriate coordinated contexts
    //

    for (OutputCoordinatorContext_t ContextLoop = (Context == PlaybackContext) ? CoordinatedContexts : Context;
         ((Context == PlaybackContext) ? (ContextLoop != NULL) : (ContextLoop == Context));
         ContextLoop = ContextLoop->Next)
    {
        ContextLoop->TimeMappingEstablished = true;
        RestartOutputRateIntegration(ContextLoop);
        // Ensure no-one is waiting in an out of date decode window.
        OS_SetEvent(&ContextLoop->AbortPerformEntryIntoDecodeWindowWait);
    }

    //
    // All communal activity completed, free the lock.
    //
    ComponentMonitor.UnLock();

    OS_SetEvent(&SynchronizeMayHaveCompleted);
}

// ///////////////////////////////////////////////////////////////////////////
//
//      rebase the time mapping if needed
//

void OutputCoordinator_Base_c::RebaseTimeMapping(OutputCoordinatorContext_t  Context,
                                                 const TimeStamp_c          &Pts,
                                                 unsigned long long          SystemTime)
{
    ComponentMonitor(this);

    if (!Pts.IsValid() || NotValidTime(SystemTime) || (Context != PlaybackContext && Context->TransitionState != COORDINATOR_STABLE))
    {
        return;
    }

    long long ElapsedPts = TimeStamp_c::DeltaUsec(Pts, MasterBasePts);
    // we rebase if the elapsed times are large
    if (ElapsedPts > REBASE_TIME_TRIGGER_VALUE)
    {
        MasterBasePts        = Pts;
        MasterBaseSystemTime = SystemTime;

        if (Context == PlaybackContext)
        {
            SE_INFO(group_se_pipeline, "Rebased mapping (%lld,%llu)\n", MasterBasePts.NativeValue(), MasterBaseSystemTime);
        }
        else
        {
            SE_INFO(group_se_pipeline, "Stream 0x%p - %d - Rebased mapping (%lld,%llu)\n",
                    Context->Stream, Context->StreamType,
                    MasterBasePts.NativeValue(), MasterBaseSystemTime);
        }
    }
}

// /////////////////////////////////////////////////////////////////////////
//
//      Translate a given playback time to a system time
//

OutputCoordinatorStatus_t   OutputCoordinator_Base_c::TranslatePlaybackTimeToSystem(OutputCoordinatorContext_t  Context,
                                                                                    const TimeStamp_c          &Pts,
                                                                                    unsigned long long         *SystemTime)
{
    // Lock the component to thread-safely use ClockRecovery variables
    ComponentMonitor(this);

    if (!MasterTimeMappingEstablished
        || (Context != PlaybackContext && !Context->TimeMappingEstablished))
    {
        return OutputCoordinatorMappingNotEstablished;
    }
    if (Context != PlaybackContext)
    {
        SE_EXTRAVERB(group_se_pipeline, "Stream 0x%p - %d - Pts=%lld\n", Context->Stream , Context->StreamType, Pts.NativeValue());
    }

    TimeStamp_c BasePts = MasterBasePts;

    uint64_t BaseSystemTime = MasterBaseSystemTime;

    //CheckTransitionState(Context, Pts, &BasePts, &BaseSystemTime);
    unsigned long long Now = OS_GetTimeInMicroSeconds();

    long long ElapsedPts = TimeStamp_c::DeltaUsec(Pts, BasePts);
    long long ElapsedSystemTime = SpeedScale(ElapsedPts);
    ElapsedSystemTime = RoundedLongLongIntegerPart(ElapsedSystemTime / SystemClockAdjustment);

    *SystemTime = BaseSystemTime + ElapsedSystemTime;
    if (Context != PlaybackContext)
    {
        if (Player->PolicyValue(Playback, PlayerAllStreams, PolicyExternalLatencyBehavior) == PolicyValueExternalLatencyDelayOtherStreams)
        {
            *SystemTime = *SystemTime + Context->TimeOffset;
        }
        else
        {
            *SystemTime = *SystemTime - Context->ManifestorMaxExternalLatency;
        }
        // But is the current mapping valid ??
        //
        long long deltaNow = (unsigned long long) * SystemTime - (unsigned long long) Now;
        if (Context != PlaybackContext)
        {
            if ((deltaNow < -MAX_STREAM_OFFSET_US || deltaNow > MAX_STREAM_OFFSET_US))
            {
                SE_DEBUG(group_se_pipeline, "Stream 0x%p - %d - INVALID SYSTEM TIME for Pts=%lld deltaNow=%lld\n", Context->Stream , Context->StreamType, Pts.NativeValue(), deltaNow);
                *SystemTime = INVALID_TIME;
                return OutputCoordinatorInconsistentMapping;
            }
        }
    }

    return OutputCoordinatorNoError;
}

void OutputCoordinator_Base_c::CheckTransitionState(OutputCoordinatorContext_t   Context, TimeStamp_c Pts, TimeStamp_c *BasePts, uint64_t *BaseSystemTime)
{
    if (Context == PlaybackContext || Context->TransitionState == COORDINATOR_STABLE)
    {
        return;
    }
    Context->TransitionStateCounter++;
    bool TransitionEnded = false;
    if (Context->TransitionState != COORDINATOR_UNSYNCED)
    {
        // The UNSYNCED transition never ends after a PTS check, only when transition mapping becomes invalidated
        // (if stream offset is not constant for instance)

        if (StreamIsClockMaster(Context))
        {
            if (Context->TransitionState == COORDINATOR_JUMP_FORWARD)
            {
                if (Pts >= Context->TransitionPts)
                {
                    SE_DEBUG(group_se_pipeline, "Stream 0x%p - %d - PTS %lld > TransitionPts %lld, this concludes the forward jump transition\n"
                             , Context->Stream
                             , Context->StreamType
                             , Pts.NativeValue()
                             , Context->TransitionPts.NativeValue());

                    TransitionEnded = true;
                }
            }
            else   //  JUMP BACKWARD
            {
                if (Pts < Context->TransitionLastPts)
                {
                    SE_DEBUG(group_se_pipeline, "Stream 0x%p - %d - PTS %lld > TransitionLastPts %lld, this concludes the backward jump transition\n"
                             , Context->Stream
                             , Context->StreamType
                             , Pts.NativeValue()
                             , Context->TransitionLastPts.NativeValue());

                    TransitionEnded = true;
                }
            }
        }
        else
        {
            SE_DEBUG(group_se_pipeline, "Stream 0x%p - %d - Transition on-going : master : %d mapping established : %d\n"
                     , Context->Stream
                     , Context->StreamType
                     , StreamIsClockMaster(Context)
                     , MasterTimeMappingEstablished);


        }

        // Aren't we in transition for too long ?
        if (!TransitionEnded && Context->TransitionStateCounter > MAX_TRANSITION_STATE_COUNTER)
        {
            // This is a safety guard : we don't wat to remain in transitionning state forever
            SE_ERROR("Stream 0x%p - %d - Force exit of transition state (%d)\n"
                     , Context->Stream
                     , Context->StreamType
                     , Context->TransitionStateCounter);

            TransitionEnded = true;
        }
    }

    if (TransitionEnded == true)
    {
        // Time to reset transition variables in the context

        SE_DEBUG(group_se_pipeline, "Stream 0x%p - %d - Processing PTS=%lld : Time mapping transition type %d from (%lld,%llu) to (%lld,%llu) ended, mapping now (%lld,%llu)\n"
                 , Context->Stream
                 , Context->StreamType
                 , Pts.NativeValue()
                 , Context->TransitionState
                 , Context->TransitionLastPts.NativeValue()
                 , Context->TransitionLastSystemTime
                 , Context->TransitionPts.NativeValue()
                 , Context->TransitionSystemTime
                 , MasterBasePts.NativeValue()
                 , MasterBaseSystemTime
                );
        ResetTransitionContext(Context);
    }
    else
    {
        // Current strem is in transition, the mapping to use to get a System Playback time is the last mapping, not current one
        SE_VERBOSE(group_se_pipeline, "Stream 0x%p - %d - Processing PTS=%lld during Time mapping transition type %d from (%lld,%llu) to (%lld,%llu)\n"
                   , Context->Stream
                   , Context->StreamType
                   , Pts.NativeValue()
                   , Context->TransitionState
                   , Context->TransitionLastPts.NativeValue()
                   , Context->TransitionLastSystemTime
                   , Context->TransitionPts.NativeValue()
                   , Context->TransitionSystemTime);

        if (StreamIsClockMaster(Context))
        {
            *BasePts = Context->TransitionLastMappingPts;
            *BaseSystemTime = Context->TransitionLastMappingSystemTime;
        }
        else
        {
            *BasePts = Context->TransitionPts;
            *BaseSystemTime = Context->TransitionSystemTime;
        }
        SE_VERBOSE(group_se_pipeline, "Stream 0x%p - %d - Processing PTS=%lld Using BasePts=%lld SystemTime=%lld\n"
                   , Context->Stream
                   , Context->StreamType
                   , Pts.NativeValue()
                   , (*BasePts).NativeValue()
                   , (*BaseSystemTime));

    }
}

void OutputCoordinator_Base_c::ResetTransitionContext(OutputCoordinatorContext_t Context)
{
    if (Context == NULL)
    {
        return;
    }
    Context->TransitionState = COORDINATOR_STABLE;
    Context->TransitionStateCounter = 0;
    Context->TransitionLastPts = TimeStamp_c();
    Context->TransitionPts = TimeStamp_c();
    Context->TransitionLastMappingPts = TimeStamp_c();
    Context->TransitionLastSystemTime = INVALID_TIME;
    Context->TransitionSystemTime = INVALID_TIME;
    Context->TransitionLastMappingSystemTime = INVALID_TIME;
    Context->InvalidTransitionsFromUnstableStateRequests = 0;
}

///////////////////////////////////////////////////////////////////////////
//
//      Apply Max External Latency from all the streams available in the
//      context.
//
void OutputCoordinator_Base_c::AdjustExternalLatency(uint32_t ExternalLatency)
{
    uint32_t MaxLatency = ExternalLatency;

    // Compute Max External latency from all streams
    for (OutputCoordinatorContext_t ContextLoop = CoordinatedContexts;
         (ContextLoop != NULL);
         ContextLoop  = ContextLoop->Next)
    {
        if (ContextLoop->ManifestorMaxExternalLatency > MaxLatency)
        {
            MaxLatency = ContextLoop->ManifestorMaxExternalLatency;
        }
    }

    // Apply Maximum latency on other streams
    for (OutputCoordinatorContext_t ContextLoop = CoordinatedContexts;
         (ContextLoop != NULL);
         ContextLoop  = ContextLoop->Next)
    {
        ContextLoop->TimeOffset = MaxLatency - ContextLoop->ManifestorMaxExternalLatency;
    }
}

///////////////////////////////////////////////////////////////////////////
//
//      Get Max External Latency of the stream. If there is any change
//      in External latency then adjust the latency of all other streams
//      available in the Playback.
//
void OutputCoordinator_Base_c::UpdateExternalLatency(OutputCoordinatorContext_t Context)
{
    ComponentMonitor(this);

    // Get Max External latency
    uint32_t MaxExternalLatency = Context->ManifestationCoordinator->GetMaxExternalManifestationLatency();
    // Adjust TimeOffset for each stream
    SE_EXTRAVERB(group_avsync, "Maxlatency %d\n", MaxExternalLatency);

    if (Context->ManifestorMaxExternalLatency == MaxExternalLatency)
    {
        return;
    }

    Context->ManifestorMaxExternalLatency = MaxExternalLatency;

    if (Player->PolicyValue(Playback, PlayerAllStreams, PolicyExternalLatencyBehavior) == PolicyValueExternalLatencyDelayOtherStreams)
    {
        AdjustExternalLatency(MaxExternalLatency);
        ResetTimeMapping(PlaybackContext);
    }
}

// /////////////////////////////////////////////////////////////////////////
//
//      Translate a given system time to a playback time
//

OutputCoordinatorStatus_t   OutputCoordinator_Base_c::TranslateSystemTimeToPlayback(OutputCoordinatorContext_t        Context,
                                                                                    unsigned long long                SystemTime,
                                                                                    TimeStamp_c                      *Pts)
{
    // Lock the component to thread-safely use ClockRecovery variables
    ComponentMonitor(this);

    if (!MasterTimeMappingEstablished
        || (Context != PlaybackContext && !Context->TimeMappingEstablished))
    {
        return OutputCoordinatorMappingNotEstablished;
    }

    // SE_ASSERT(SystemTime >= MasterBaseSystemTime);
    long long ElapsedSystemTime = SystemTime - MasterBaseSystemTime;
    ElapsedSystemTime = RoundedLongLongIntegerPart(ElapsedSystemTime * SystemClockAdjustment);
    long long ElapsedPts = InverseSpeedScale(ElapsedSystemTime);

    *Pts = TimeStamp_c::AddUsec(MasterBasePts, ElapsedPts);

    return OutputCoordinatorNoError;
}

bool OutputCoordinator_Base_c::ReuseExistingTimeMapping(
    OutputCoordinatorContext_t        Context,
    const TimeStamp_c                &Pts,
    unsigned long long               *SystemTime,
    void                             *ParsedAudioVideoDataParameters)
{
    if (MasterTimeMappingEstablished)
    {
        unsigned long long Now = OS_GetTimeInMicroSeconds();
        unsigned long long MasterMappingNow;
        TranslatePlaybackTimeToSystem(PlaybackContext, Pts, &MasterMappingNow);

        long long DeltaNow = (long long)MasterMappingNow - (long long)Now;
        // if the deltaNow is greater than MAX_STREAM_OFFSET it means the PTSes of the different streams
        // have incoherent PTSes. So dont reuse the mapping.
        bool AlternateMappingIsReasonable;

        if (!StreamIsClockMaster(Context))
        {
            // Current stream is not master, it cannot invalidate the mapping
            AlternateMappingIsReasonable    = true;
        }
        else
        {
            AlternateMappingIsReasonable = inrange(DeltaNow, -MAX_STREAM_OFFSET_US, MAX_STREAM_OFFSET_US);

            if (Player->PolicyValue(Playback, Stream, PolicyCaptureProfile) != PolicyValueCaptureProfileDisabled)
            {
                long long LargestOfMinimumManifestationLatencies = GetLargestOfMinimumManifestationLatencies(ParsedAudioVideoDataParameters);
                if (LatencyUsedInCaptureModeDuringTimeMapping < LargestOfMinimumManifestationLatencies)
                {
                    AlternateMappingIsReasonable    = false;
                    SE_INFO(group_avsync, "Stream 0x%p: Mapping not reasonable %d- Latency used during previous time mapping %lld  Latency required for current time mapping %lld\n",
                            Context->Stream, Context->StreamType, LatencyUsedInCaptureModeDuringTimeMapping, LargestOfMinimumManifestationLatencies);
                }
                else
                {
                    AlternateMappingIsReasonable    = true;
                    SE_INFO(group_avsync, "Stream 0x%p: Mapping reasonable %d- Latency used during previous time mapping %lld  Latency required for current time mapping %lld\n",
                            Context->Stream, Context->StreamType, LatencyUsedInCaptureModeDuringTimeMapping, LargestOfMinimumManifestationLatencies);
                }
            }
            else if (Player->PolicyValue(Playback, PlayerAllStreams, PolicyVideoStartImmediate) == PolicyValueApply
                     && Player->PolicyValue(Playback, PlayerAllStreams, PolicyLivePlayback) != PolicyValueApply)
            {
                if (MasterTimeMappingEstablishedOnType == StreamTypeAudio)
                {
                    // when PolicyVideoStartImmediate is set, video should never reuse a time mapping done by audio
                    AlternateMappingIsReasonable    = false;
                }
            }
        }

        if (AlternateMappingIsReasonable)
        {
            Context->TimeMappingEstablished = true;
            Context->StreamOffset           = DeltaNow;

            TranslatePlaybackTimeToSystem(Context, Pts, SystemTime);
            Context->InSynchronizeFn       = false;

            return true;
        }

        SE_INFO(group_avsync, "Stream 0x%p: Mapping not reasonable - %d - Pts=%llu DeltaNow=%lld ManifestorLatency=%lld\n",
                Context->Stream, Context->StreamType, Pts.NativeValue(), DeltaNow, Context->ManifestorLatency);
    }

    return false;
}

bool OutputCoordinator_Base_c::StreamCanGenerateTimeMapping(OutputCoordinatorContext_t Context, unsigned int WaitCount)
{
    if (Player->PolicyValue(Playback, PlayerAllStreams, PolicyLivePlayback) == PolicyValueApply)
    {
        if (mClockRecoveryLastValidPcr.IsValid() || (WaitCount >= MAX_SYNCHRONIZE_WAITS_LIVE))
        {
            return true;
        }
        return false;
    }
    else if (Player->PolicyValue(Playback, Stream, PolicyCaptureProfile) != PolicyValueCaptureProfileDisabled)
    {
        return true;
    }
    else if (Player->PolicyValue(Playback, PlayerAllStreams, PolicyVideoStartImmediate) == PolicyValueApply
             && Context->Stream->GetStreamType() == StreamTypeVideo)
    {
        return true;
    }
    else if (Player->PolicyValue(Playback, PlayerAllStreams, PolicyVideoStartImmediate) == PolicyValueApply
             && mRegisteredVideoStreamCount > 0
             && Context->Stream->GetStreamType() == StreamTypeAudio)
    {
        if (WaitCount >= MAX_SYNCHRONIZE_WAITS_VIDEO_START_IMMEDIATE)
        {
            SE_INFO(group_avsync, "Stream 0x%p Audio Stream will finally generate the mapping after %d waiting periods\n", Context->Stream, WaitCount);
            return true;
        }
        return false;
    }
    else if (StreamsInSynchronize == StreamCount || WaitCount >= MAX_SYNCHRONIZE_WAITS)
    {
        return true;
    }

    return false;
}

// /////////////////////////////////////////////////////////////////////////
//
//      Check that the time mapping established by another stream is suitable
//      and take recovery actions when it is not
//
OutputCoordinatorStatus_t   OutputCoordinator_Base_c::CheckTimeMapping(OutputCoordinatorContext_t Context,
                                                                       const TimeStamp_c         &Pts,
                                                                       unsigned long long        *SystemTime
                                                                      )
{
    SE_ASSERT(MasterTimeMappingEstablished);

    OutputCoordinatorStatus_t Status = TranslatePlaybackTimeToSystem(Context, Pts, SystemTime);
    if (Status != OutputCoordinatorNoError)
    {
        // Not much we can do
        return OutputCoordinatorNoError;
    }

    long long deltaNow = (long long)(*SystemTime - OS_GetTimeInMicroSeconds());

    if (Player->PolicyValue(Playback, PlayerAllStreams, PolicyLivePlayback) == PolicyValueApply)

    {
        if (deltaNow < (long long) Context->ManifestorLatency)
        {
            SE_WARNING("Stream 0x%p Time Mapping leads to deltaNow (%lld) < ManifestorLatency (%lld)\n",
                       Context->Stream, deltaNow, Context->ManifestorLatency);
        }
    }

    // if deltaNow is way out of range, due to streams withtin playback with PTS not aligned,
    // can not synchronize in such case as either audio or video would get discarded
    // in such case enter dedicated state
    if (!inrange(deltaNow, -MAX_STREAM_OFFSET_US, MAX_STREAM_OFFSET_US))
    {
        SE_WARNING("Stream 0x%p Time Mapping leads to deltaNow %lld way out of range\n",
                   Context->Stream, deltaNow);
        // NOTE : supporting seamless playback is not compatible with UNSYNCED state transition
        // EnterTransitionPeriod(Context, Pts, *SystemTime, COORDINATOR_UNSYNCED);
    }

    return OutputCoordinatorNoError;
}

// /////////////////////////////////////////////////////////////////////////
//
//      Synchronize the streams to establish a time mapping
//

OutputCoordinatorStatus_t   OutputCoordinator_Base_c::SynchronizeStreams(OutputCoordinatorContext_t        Context,
                                                                         const TimeStamp_c                &Pts,
                                                                         const TimeStamp_c                &Dts,
                                                                         unsigned long long               *SystemTime,
                                                                         void                             *ParsedAudioVideoDataParameters)
{
    OutputCoordinatorStatus_t Status;

    ComponentMonitor(this);

    // We do not perform the synchronization if we use an enforced external time mapping
    int ExternalMapping = Player->PolicyValue(Playback, Context->Stream, PolicyExternalTimeMapping);
    if (ExternalMapping == PolicyValueApply)
    {
        SE_INFO(group_avsync, "Function entered when PolicyExternalTimeMapping is set\n");
        *SystemTime = UNSPECIFIED_TIME;
        return OutputCoordinatorNoError;
    }

    //
    // Load in the surface descriptors, and verify the master clock
    //
    VerifyMasterClock();

    SE_INFO2(group_avsync, group_se_pipeline, "Stream 0x%p: Sync In - %d - Pts=%lld Dts=%lld\n",
             Context->Stream, Context->StreamType, Pts.NativeValue(), Dts.NativeValue());

    Context->InSynchronizeFn = true;

    if (ReuseExistingTimeMapping(Context, Pts, SystemTime, ParsedAudioVideoDataParameters))
    {
        SE_INFO2(group_avsync, group_se_pipeline, "Stream 0x%p: Sync Out (Reusing Existing Time Mapping) - %d - Pts=%lld ManifestorLatency=%lld StreamOffset=DeltaNow=%lld\n",
                 Context->Stream, Context->StreamType, Pts.NativeValue(), Context->ManifestorLatency, Context->StreamOffset);

        stm_se_time_mapping_source_t Source;
        if (MasterTimeMappingEstablishedOnType == StreamTypePcr)
        {
            Source = MappingSourcePcr;
        }
        else
        {
            Source = MappingSourceOther;
        }
        GenerateMappingEstablishedEvent(Context, Source);

        // check in case mapping reused is out of bond and need to disable avsync for this stream
        CheckTimeMapping(Context, Pts, SystemTime);

        return OutputCoordinatorNoError;
    }

    //
    // Having reached here, either there is no alternate mapping, or the alternate mapping is
    // not appropriate, in either event there is now no valid master time mapping. We therefore
    // invalidate it before proceding to establish a new one.
    //
    MasterTimeMappingEstablished = false;

    for (OutputCoordinatorContext_t ContextLoop = CoordinatedContexts;
         (ContextLoop != NULL);
         ContextLoop   = ContextLoop->Next)
    {
        ContextLoop->TimeMappingEstablished     = false;
        OS_SetEvent(&ContextLoop->AbortPerformEntryIntoDecodeWindowWait);
    }

    // Now we need to add ourselves to the list of synchronizing streams
    Context->SynchronizingAtPlaybackTime        = Pts;
    StreamsInSynchronize++;

    // Enter the loop where we await synchronization
    unsigned int WaitCount   = 0;

    stm_se_time_mapping_source_t Source = MappingSourceOther; // default

    while (true)
    {
        // Has someone completed the establishment of the mapping
        if (MasterTimeMappingEstablished)
        {
            Context->TimeMappingEstablished = true;
            Status = CheckTimeMapping(Context, Pts, SystemTime);
            break;
        }

        // Can we do the synchronization
        if (StreamCanGenerateTimeMapping(Context, WaitCount))
        {
            Status = GenerateTimeMapping(ParsedAudioVideoDataParameters);
            if (Status != OutputCoordinatorNoError)
            {
                SE_ERROR("Failed to generate a time mapping\n");
            }
            else
            {
                Source = MappingSourceSelf;
            }

            if (Player->PolicyValue(Playback, Stream, PolicyCaptureProfile) != PolicyValueCaptureProfileDisabled)
            {
                GenerateTimeMappingForCapture(Context, ParsedAudioVideoDataParameters);
                Source = MappingSourceVideoCapture;
            }

            // Wake everyone and pass back through to pickup the mapping
            OS_SetEvent(&SynchronizeMayHaveCompleted);
        }
        else
        {
            SE_VERBOSE(group_se_pipeline, "Stream 0x%p - %d - Conditions are not met to generate the mapping\n", Context->Stream, Context->StreamType);
        }

        // Wait until something happens
        if (!MasterTimeMappingEstablished)
        {
            OS_ResetEvent(&SynchronizeMayHaveCompleted);
            ComponentMonitor.UnLock();
            OS_WaitForEventAuto(&SynchronizeMayHaveCompleted, SYNCHRONIZE_WAIT);
            ComponentMonitor.Lock();
            WaitCount++;
        }
    }

    Context->InSynchronizeFn    = false;
    StreamsInSynchronize--;

    if (ValidTime(*SystemTime))
    {
        long long deltaNow = (long long)(*SystemTime - OS_GetTimeInMicroSeconds());

        SE_INFO2(group_avsync, group_se_pipeline, "Stream 0x%p: Sync Out (New Time Mapping) - %d - Pts=%lld SystemTime=%8llu (DeltaNow=%lld) StreamOffset=%lld\n",
                 Context->Stream, Context->StreamType, Pts.NativeValue(), *SystemTime, deltaNow, Context->StreamOffset);
        GenerateMappingEstablishedEvent(Context, Source);
    }
    else
    {
        SE_INFO2(group_avsync, group_se_pipeline, "Stream 0x%p: Sync Out (Invalid Time Mapping) - %d - Pts=%lld StreamOffset=%lld\n",
                 Context->Stream, Context->StreamType, Pts.NativeValue(), Context->StreamOffset);
    }

    return Status;
}

OutputCoordinatorStatus_t   OutputCoordinator_Base_c::GenerateMappingEstablishedEvent(OutputCoordinatorContext_t Context, stm_se_time_mapping_source_t Source)
{
    if (Context->Stream != NULL)
    {
        PlayerEventRecord_t MappingEstablishedEvent;
        MappingEstablishedEvent.Code                 = EventTimeMappingEstablished;
        MappingEstablishedEvent.Playback             = Playback;
        MappingEstablishedEvent.Stream               = Context->Stream;
        MappingEstablishedEvent.Value[0].UnsignedInt = (unsigned int) Source;
        PlayerStatus_t StatusEvt = Context->Stream->SignalEvent(&MappingEstablishedEvent);
        if (StatusEvt != PlayerNoError)
        {
            SE_ERROR("Stream 0x%p Failed to signal event\n", Context->Stream);
            return PlayerError;
        }
        else
        {
            const char *SourceStr;
            switch (MappingEstablishedEvent.Value[0].UnsignedInt)
            {
            case MappingSourcePcr:          SourceStr = "PCR"; break;
            case MappingSourceSelf:         SourceStr = "SELF"; break;
            case MappingSourceOther:        SourceStr = "OTHER"; break;
            case MappingSourceVideoCapture: SourceStr = "VIDEO_IN"; break;
            default:                        SourceStr = "UNKNOWN"; break;
            }
            SE_INFO2(group_avsync, group_se_pipeline, "Stream 0x%p  - %d - EventTimeMappingEstablished generated (source: %s)\n",
                     Context->Stream, Context->StreamType, SourceStr);
            return PlayerNoError;
        }
    }
    return PlayerError;
}

// /////////////////////////////////////////////////////////////////////////
//
//      The function to await entry into the decode window, if the decode
//      time is invalid, or if no time mapping has yet been established,
//      we return immediately.
//

OutputCoordinatorStatus_t   OutputCoordinator_Base_c::PerformEntryIntoDecodeWindowWait(OutputCoordinatorContext_t  Context,
                                                                                       const TimeStamp_c          &Dts,
                                                                                       unsigned long long          DecodeWindowPorch)
{
    //
    // Reset the abort flag
    //
    OS_ResetEvent(&Context->AbortPerformEntryIntoDecodeWindowWait);

    //
    // Get the translation of the decode time, and apply the porch to find the window start
    // If we do not have a mapping here we can use the master mapping.
    //
    unsigned long long SystemTime;
    OutputCoordinatorStatus_t Status = TranslatePlaybackTimeToSystem((Context->TimeMappingEstablished ? Context : PlaybackContext), Dts, &SystemTime);

    long long SleepTime;
    unsigned long long Now = OS_GetTimeInMicroSeconds();

    if (Status == OutputCoordinatorMappingNotEstablished)
    {
        SleepTime = 30000;
        SE_WARNING("Stream 0x%p Mapping not established yet, sleeping for %lldus\n", Stream, SleepTime);
    }
    else
    {
        //
        // Shall we sleep
        //

        if (Now >= SystemTime - DecodeWindowPorch)
        {
            return OutputCoordinatorNoError;
        }

        //
        // How long shall we sleep
        //
        SleepTime                   = ((int64_t)SystemTime - (int64_t)Now) - ((int64_t)DecodeWindowPorch + (int64_t)Context->ManifestorLatency);

        SE_EXTRAVERB(group_avsync, "Stream 0x%p Sleep period defined with SystemTime=%llu Now=%lld DecodeWindowPorch=%lld ManifestorLatency=%lld\n",
                     Stream,
                     SystemTime,
                     Now,
                     DecodeWindowPorch,
                     Context->ManifestorLatency);
        if (SleepTime < 0)
        {
            return OutputCoordinatorNoError;
        }
        else if (SleepTime < MINIMUM_SLEEP_TIME_US)
        {
            SE_EXTRAVERB(group_avsync, "Stream 0x%p Sleep period too short (%lldus)\n", Stream, SleepTime);
            return OutputCoordinatorNoError;
        }
        else if (SleepTime > MAXIMUM_DECODE_WINDOW_SLEEP)
        {
            if (SleepTime > ERRONEOUS_MAPPING_WINDOW_SLEEP)
            {
                Context->ErroneousMappingWaitCount++;
                SE_WARNING("Sleep period erroneous (%lldus > %lld) cnt=%d\n", SleepTime, ERRONEOUS_MAPPING_WINDOW_SLEEP, Context->ErroneousMappingWaitCount);
            }
            if (Context->ErroneousMappingWaitCount > MAX_ERRONEOUS_MAPPING_REQUESTS)
            {
                SE_ERROR("Erroneous sleep period max count exceeded, Draining live playback\n");
                Context->ErroneousMappingWaitCount = 0;
                DrainLivePlayback();
                return OutputCoordinatorNoError;
            }
            else
            {
                SE_DEBUG(group_avsync, "Sleep period too long (%lldus), clamping to %d us\n", SleepTime, MAXIMUM_DECODE_WINDOW_SLEEP);
                SleepTime               = MAXIMUM_DECODE_WINDOW_SLEEP;
            }
        }
        else
        {
            Context->ErroneousMappingWaitCount = 0;
        }
    }

    //
    // Sleep
    //

    SE_DEBUG2(group_avsync, group_se_pipeline, "Stream 0x%p About to sleep for  %lldus (system time : %lluus porch : %lluus now : %llus)\n"
              , Stream
              , SleepTime
              , SystemTime
              , DecodeWindowPorch
              , Now);

    OS_Status_t WaitStatus = OS_WaitForEventAuto(&Context->AbortPerformEntryIntoDecodeWindowWait, (unsigned int)(SleepTime / 1000));

    return (WaitStatus == OS_NO_ERROR) ? OutputCoordinatorNoError : OutputCoordinatorAbandonedWait;
}

// /////////////////////////////////////////////////////////////////////////
//
//      The function to handle deltas in the playback time versus the
//      expected playback time of a frame.
//
//      This function splits into two halves, first checking for any
//      large delta and performing a rebase of the time mapping when one
//      occurs.
//
//      A second half checking that if we did a rebase, then if this stream
//      has not matched the rebase then we invalidate mapping to force a
//      complete re-synchronization.
//

OutputCoordinatorStatus_t   OutputCoordinator_Base_c::HandlePlaybackTimeDeltas(OutputCoordinatorContext_t        Context,
                                                                               const TimeStamp_c                &ExpectedPlaybackTime,
                                                                               const TimeStamp_c                &ActualPlaybackTime)
{
    ComponentMonitor(this);

    if (Context == PlaybackContext)
    {
        SE_ERROR("Deltas only handled for specific streams\n");
        return OutputCoordinatorError;
    }
    if (Context->TransitionState != COORDINATOR_STABLE)
    {
        return OutputCoordinatorNoError;
    }
    if (!MasterTimeMappingEstablished)
    {
        return OutputCoordinatorNoError;
    }

    if (Context != PlaybackContext)
    {
        SE_VERBOSE(group_se_pipeline, "Stream 0x%p - %d - ExpectedPlaybackTime=%lld ActualPlaybackTime=%lld\n", Context->Stream , Context->StreamType, ExpectedPlaybackTime.NativeValue(),
                   ActualPlaybackTime.NativeValue());
    }
    // What are the current value of the jump detector thresholds
    int Threshold = Player->PolicyValue(Playback, Context->Stream, PolicyPtsForwardJumpDetectionThreshold);
    long long PlaybackTimeForwardJumpThreshold    = 1000000 * (1ULL << Threshold);

    // Now evaluate the delta and handle any jump in the playback time
    long long DeltaPlaybackTime = (Direction == PlayBackward) ?
                                  TimeStamp_c::DeltaUsec(ExpectedPlaybackTime, ActualPlaybackTime) :
                                  TimeStamp_c::DeltaUsec(ActualPlaybackTime, ExpectedPlaybackTime);

    if (!inrange(DeltaPlaybackTime, -PTS_BACKWARD_JUMP_THRESHOLD, PlaybackTimeForwardJumpThreshold))
    {
        if (!StreamIsClockMaster(Context))
        {
            SE_ERROR("Stream 0x%p - %d - Spotted a delta(%s) %12lld, but stream is not master. Entering transition period without resetting the mapping\n", Context->Stream
                     , Context->StreamType
                     , StreamType(), DeltaPlaybackTime);
        }
        else
        {
            SE_ERROR("Stream 0x%p - %d - Spotted a delta(%s) %12lld, and stream is master. Entering transition period and resetting the mapping\n", Context->Stream
                     , Context->StreamType
                     , StreamType(), DeltaPlaybackTime);
            ResetTimeMapping(PlaybackContext);
        }
        return OutputCoordinatorLargeDelta;
    }

    return OutputCoordinatorNoError;
}

// /////////////////////////////////////////////////////////////////////////
//
// This function calculates a long term drift corrector that tries to correct the current
// synchronization error
//

Rational_t OutputCoordinator_Base_c::CalculateAvsyncErrorDriftCorrector(OutputCoordinatorContext_t   Context,
                                                                        TimingContext_t             *State,
                                                                        long long int                CurrentError,
                                                                        int                          i)
{
    long long AvsyncErrorCorrection = -CurrentError / 2;
    unsigned long long AvsyncErrorCorrectionPeriod = LongLongIntegerPart(State->LeastSquareFit.Y());

    if (0 == AvsyncErrorCorrectionPeriod)
    {
        SE_WARNING("fixing AvsyncErrorCorrectionPeriod 0->1\n");
        AvsyncErrorCorrectionPeriod = 1;
    }

    Rational_t AvsyncErrorDriftCorrector = Rational_t(AvsyncErrorCorrection, AvsyncErrorCorrectionPeriod);

    SE_DEBUG(group_avsync, "clock (%d-%d) AvsyncErrorCorrection %lld AvsyncErrorCorrectionPeriod %llu us AvsyncErrorDriftCorrector: %d ppm\n",
             Context->StreamType, i, AvsyncErrorCorrection, AvsyncErrorCorrectionPeriod, (AvsyncErrorDriftCorrector * 1000000).IntegerPart());

    // Clamp the adjustment done to correct avsync error to the
    // maximum adjustment step supported by the surface
    // This will avoid oscillations around 0 due to not being able to slow down fast enough
    OutputSurfaceDescriptor_t *Surface = Context->Surfaces[i];

    Rational_t MaxAdjustmentStep = Rational_t(Surface->MaxClockAdjustmentStep, 1000000);
    Rational_t MinAdjustmentStep = Rational_t(-Surface->MaxClockAdjustmentStep, 1000000);

    if (!inrange(AvsyncErrorDriftCorrector, MinAdjustmentStep, MaxAdjustmentStep))
    {
        Clamp(AvsyncErrorDriftCorrector, MinAdjustmentStep, MaxAdjustmentStep);

        SE_DEBUG(group_avsync, "clock (%d-%d) Clamping AvsyncErrorDriftCorrector to %d ppm\n",
                 Context->StreamType, i, (AvsyncErrorDriftCorrector * 1000000).IntegerPart());
    }

    return AvsyncErrorDriftCorrector;
}

// /////////////////////////////////////////////////////////////////////////
//
// Calculate a new adjustment multiplier that matches the measured drift
//

Rational_t OutputCoordinator_Base_c::CalculateOutputDrift(OutputCoordinatorContext_t   Context,
                                                          TimingContext_t             *State,
                                                          int                          i)
{
    Rational_t  OutputDrift = State->LeastSquareFit.Gradient();
    if (OutputDrift == 0)
    {
        SE_WARNING("fixing OutputDrift 0->1\n");
        OutputDrift = 1;
    }

    Context->Stream->Statistics().OutputRateGradient[i] = GradientToPartsPerMillion(OutputDrift);

    SE_DEBUG(group_avsync, "clock (%d-%d) Gradient: %d.%09d (%d ppm)\n",
             Context->StreamType, i, OutputDrift.IntegerPart(), OutputDrift.UnsignedRemainderDecimal(9), GradientToPartsPerMillion(OutputDrift));

    return OutputDrift;
}

void OutputCoordinator_Base_c::CalculateOutputRateAdjustment(OutputCoordinatorContext_t   Context,
                                                             TimingContext_t             *State,
                                                             long long                    CurrentAvsyncError,
                                                             int                          i)
{
    Rational_t ClockAdjustment = CalculateOutputDrift(Context, State, i);

    ClockAdjustment += CalculateAvsyncErrorDriftCorrector(Context, State, CurrentAvsyncError, i);

    OutputSurfaceDescriptor_t *Surface = Context->Surfaces[i];

    if (!inrange(GradientToPartsPerMillion(ClockAdjustment), -Surface->MaxClockAdjustmentStep, Surface->MaxClockAdjustmentStep))
    {
        Rational_t MaxAdjustmentStep = Rational_t(1000000 + Surface->MaxClockAdjustmentStep, 1000000);
        Rational_t MinAdjustmentStep = Rational_t(1000000 - Surface->MaxClockAdjustmentStep, 1000000);

        Clamp(ClockAdjustment, MinAdjustmentStep, MaxAdjustmentStep);

        SE_DEBUG(group_avsync, "clock (%d-%d) Clamping adjustment step to %d ppm\n",
                 Context->StreamType, i, GradientToPartsPerMillion(ClockAdjustment));
    }

    // Now apply this to the clock adjustment
    State->ClockAdjustment  = State->ClockAdjustment * ClockAdjustment;
    State->ClockAdjustmentEstablished   = true;

    SE_DEBUG(group_avsync, "(%d-%d) ClockAdjustment drift added: %d.%09d %d ppm\n",
             Context->StreamType, i, State->ClockAdjustment.IntegerPart(), State->ClockAdjustment.UnsignedRemainderDecimal(9),
             GradientToPartsPerMillion(State->ClockAdjustment));

    Rational_t MaximumClockRateMultiplier      = Rational_t(1000000 + Surface->MaxClockAdjustment, 1000000);
    Rational_t MinimumClockRateMultiplier      = Rational_t(1000000 - Surface->MaxClockAdjustment, 1000000);

    Clamp(State->ClockAdjustment, MinimumClockRateMultiplier, MaximumClockRateMultiplier);
}

// /////////////////////////////////////////////////////////////////////////
//
//      The function to calculate the clock rate adjustment values,
//      and system clock, and for any subsiduary clocks.
//

OutputCoordinatorStatus_t OutputCoordinator_Base_c::CalculateOutputRateAdjustments(OutputCoordinatorContext_t   Context,
                                                                                   OutputTiming_t              *OutputTiming,
                                                                                   Rational_t                 **ParamOutputRateAdjustments,
                                                                                   Rational_t                 **CurrentErrorJitterUs,
                                                                                   Rational_t                  *ParamSystemClockAdjustment)
{
    // Lock the component to thread-safely use ClockRecovery variables
    ComponentMonitor(this);

    if (Context == PlaybackContext)
    {
        SE_ERROR("Attempt to calculate output rate adjustments for 'PlaybackContext'\n");
        return PlayerNotSupported;
    }

    //
    // If called without an output timing this function just returns the current values
    //

    if (OutputTiming == NULL)
    {
        *ParamOutputRateAdjustments     = Context->OutputRateAdjustments;
        *ParamSystemClockAdjustment     = SystemClockAdjustment;
        *CurrentErrorJitterUs           = Context->CurrentErrorJitterUs;
        return OutputCoordinatorNoError;
    }
    //
    // Load in the surface descriptors, and verify the master clock
    //
    VerifyMasterClock();

    // ///////////////////////////////////////////////////////////////////////////////////////
    //
    // Loop processing each individual surface
    //

    for (unsigned int i = 0; i <= Context->MaxSurfaceIndex; i++)
    {
        OutputSurfaceDescriptor_t   *Surface = Context->Surfaces[i];
        TimingContext_t             *State   = &Context->TimingState[i];
        ManifestationOutputTiming_t *Timing = &OutputTiming->ManifestationTimings[i];
        bool ClockMaster = Context->ClockMaster && (i == Context->ClockMasterIndex) && !UseSystemClockAsMaster;
        SE_EXTRAVERB(group_avsync, "ClockMaster %d ClockMasterIndex=%d UseSystemClockAsMaster=%d\n", Context->ClockMaster, Context->ClockMasterIndex, UseSystemClockAsMaster);

        //
        // Do we need to reset the integration after a new data setting
        //

        unsigned long long ExpectedTime        = Timing->SystemPlaybackTime;
        unsigned long long ActualTime      = Timing->ActualSystemPlaybackTime;
        long long CurrentError        = (long long)((long long)ExpectedTime - (long long)ActualTime);


        if (!Timing->TimingValid || NotValidTime(ExpectedTime) || NotValidTime(ActualTime))
        {
            // Current timing not valid
            continue;
        }

        if (NotValidTime(State->LastExpectedTime) || NotValidTime(State->LastActualTime))
        {
            // Last timing not yet valid for first iteration. Next iteration hould be fine
            State->LastExpectedTime = ExpectedTime;
            State->LastActualTime   = ActualTime;
            continue;
        }

        long long ExpectedDuration = (long long)ExpectedTime - (long long)State->LastExpectedTime;
        long long ActualDuration   = (long long)ActualTime - (long long)State->LastActualTime;
        long long Difference       = (long long)(ExpectedDuration - ActualDuration);
        // SE-PIPELINE TRACE
        SE_VERBOSE2(group_avsync, group_se_pipeline, "Stream 0x%p clock(%d-%d) #%d Master=%d ExpectedDuration=%lld ActualDuration=%lld Difference %lld\n",
                    Context->Stream, Context->StreamType, i, State->IntegrationCount, ClockMaster, ExpectedDuration, ActualDuration, Difference);
        SE_VERBOSE2(group_avsync, group_se_pipeline, "Stream 0x%p clock(%d-%d) #%d ExpectedTime = %lld ActualTime = %lld CurrentErr = %lld\n",
                    Context->Stream, Context->StreamType, i, State->IntegrationCount, ExpectedTime, ActualTime, CurrentError);

        State->LastExpectedTime = ExpectedTime;
        State->LastActualTime   = ActualTime;

        if ((Surface == NULL) || (!ClockMaster && !Surface->ClockPullingAvailable))
        {
            SE_VERBOSE(group_avsync, "Stream 0x%p clock(%d-%d) No adjustment possible, master=%d pulling=%d surface=0x%p\n"
                       , Context->Stream, Context->StreamType, i, ClockMaster, (Surface != NULL) ? Surface->ClockPullingAvailable : 0
                       , Surface);
            Context->OutputRateAdjustments[i]   = 1;
            Context->CurrentErrorJitterUs[i]    = 0;
            continue;
        }

        if (!ClockMaster && !SystemClockAdjustmentEstablished)
        {
            SE_VERBOSE(group_avsync, "Stream 0x%p clock(%d-%d) Clock is not master and system clock adjustment not established\n", Context->Stream, Context->StreamType, i);
            continue;
        }

        //
        // Now perform the integration or ignore
        //
        State->IntegrationCount++;

        State->LeastSquareFit.Add(ActualDuration, ExpectedDuration);

        Rational_t Gradient;
        Rational_t Intercept;

        bool GradientStable = false;
        bool ComputeGradient;
        int SkipCount = (State->IntegrationCount / LAST_ORA_GRADIENTS_COUNT) / 8;

        if ((SkipCount == 0 || (State->IntegrationCount % SkipCount == 0)))
        {
            ComputeGradient = true;
        }
        else
        {
            ComputeGradient = false;
        }

        SE_EXTRAVERB(group_avsync, "Stream 0x%p clock(%d-%d) ComputeGradient %d IntegrationCount %d skipcount %d\n", Context->Stream
                     , Context->StreamType, i,
                     ComputeGradient, State->IntegrationCount, SkipCount);
        if (ComputeGradient && State->IntegrationCount >= INTEGRATION_PERIOD)
        {
            Gradient  = State->LeastSquareFit.Gradient();
            Intercept = State->LeastSquareFit.Intercept();
            long long IntegrationDuration = State->LeastSquareFit.X();
            ///////////////////////////////////////////////////////////////////////////
            //  Gradient stability computation
            ///////////////////////////////////////////////////////////////////////////

            Rational_t CurrentPpm = GradientToPartsPerMillionRational(Gradient);

            SE_EXTRAVERB(group_avsync, "Stream 0x%p clock(%d-%d) Gradient[%d] %d.%06d ppm\n", Context->Stream
                         , Context->StreamType, i,
                         State->mORALastPpmIndex, CurrentPpm.IntegerPart(), CurrentPpm.UnsignedRemainderDecimal(6));

            State->mORALastPpm[State->mORALastPpmIndex % LAST_ORA_GRADIENTS_COUNT] = CurrentPpm;
            State->mORALastCurrentError[State->mORALastPpmIndex % LAST_ORA_GRADIENTS_COUNT] = CurrentError;
            State->mORALastPpmIndex++;

            unsigned int PpmIntegrationCount;
            if (State->mORALastPpmIndex < LAST_ORA_GRADIENTS_COUNT)
            {
                PpmIntegrationCount = State->mORALastPpmIndex;
            }
            else
            {
                PpmIntegrationCount = LAST_ORA_GRADIENTS_COUNT;
            }

            if (PpmIntegrationCount >= CLOCK_ADJUSTMENT_MINIMUM_POINTS && IntegrationDuration >= CLOCK_ADJUSTMENT_MINIMUM_DURATION)
            {
                Rational_t PpmSum = 0;
                Rational_t CurrentErrorSum = 0;
                for (int k = 0; k < PpmIntegrationCount; k++)
                {
                    PpmSum += State->mORALastPpm[k];
                    CurrentErrorSum += State->mORALastCurrentError[k];
                }
                Rational_t PpmMean = PpmSum * Rational_t(1, PpmIntegrationCount);
                Rational_t CurrentErrorMean = CurrentErrorSum * Rational_t(1, PpmIntegrationCount);

                Rational_t PpmSumSqrDiff = 0;
                Rational_t CurrenErrorSumSqrDiff = 0;
                for (int k = 0; k < PpmIntegrationCount; k++)
                {
                    Rational_t PpmDiff = (State->mORALastPpm[k] - PpmMean);
                    PpmSumSqrDiff += PpmDiff * PpmDiff;

                    Rational_t CurrentErrorDiff = (State->mORALastCurrentError[k] - CurrentErrorMean);
                    CurrenErrorSumSqrDiff += CurrentErrorDiff * CurrentErrorDiff;
                }
                Rational_t PpmStdDevSqr = PpmSumSqrDiff * Rational_t(1, PpmIntegrationCount);
                Rational_t CurrentErrorStdDevSqr = CurrenErrorSumSqrDiff * Rational_t(1, PpmIntegrationCount);

                // The jitter (in us) is given by multiplying the variace of the error x the integration duration

                if (PpmStdDevSqr.IntegerPart() < ORA_PPM_STABILITY_THRESHOLD_SQR
                    && abs64(PpmMean.IntegerPart()) < CLOCK_RECOVERY_MAXIMUM_PPM
                    && abs64(Rational_t(CurrentPpm - PpmMean).IntegerPart()) < ORA_PPM_STABILITY_THRESHOLD
                    && State->mORALastPpmIndex >= LAST_ORA_GRADIENTS_COUNT)
                {
                    GradientStable = true;
                }
                Context->CurrentErrorJitterUs[i] = CurrentErrorStdDevSqr;

                SE_EXTRAVERB2(group_se_pipeline, group_avsync
                              , "Stream 0x%p clock(%d-%d) Timing 0x%p CurrentPpm=%d.%03d PpmCount=%d PpmMean=%d.%03d PpmStdDevSqr=%d.%03d IntegrationDuration=%lld thresh_sqr=%lld ppm index %d SystemPlaybackTime %lld Actual %lld CurrentErrorMean %d.%03d CurrentErrorJitterUs=%d.%03d GradientStable=%d\n"
                              , Context->Stream
                              , Context->StreamType
                              , i, Timing, CurrentPpm.IntegerPart(), CurrentPpm.UnsignedRemainderDecimal(3), PpmIntegrationCount
                              , PpmMean.IntegerPart(), PpmMean.UnsignedRemainderDecimal(3)
                              , PpmStdDevSqr.IntegerPart(), PpmStdDevSqr.UnsignedRemainderDecimal(3)
                              , IntegrationDuration
                              , ORA_PPM_STABILITY_THRESHOLD_SQR
                              , State->mORALastPpmIndex
                              , Timing->SystemPlaybackTime
                              , Timing->ActualSystemPlaybackTime
                              , CurrentErrorMean.IntegerPart()
                              , CurrentErrorMean.UnsignedRemainderDecimal(3)
                              , Context->CurrentErrorJitterUs[i].IntegerPart()
                              , Context->CurrentErrorJitterUs[i].UnsignedRemainderDecimal(3)
                              , GradientStable);

                ///////////////////////////////////////////////////////////////////////////
                //  Let's now use this gradient or take corrective actions
                ///////////////////////////////////////////////////////////////////////////
                if (!GradientStable)
                {
                    if (State->IntegrationCount >= CLOCK_ADJUSTMENT_MAXIMUM_POINTS)
                    {
                        SE_WARNING("Stream 0x%p clock(%d-%d) Gradient %d.%09d not stable over %d samples, Failed to recover valid clock gradient - retrying\n"
                                   , Context->Stream
                                   , Context->StreamType
                                   , i
                                   , Gradient.IntegerPart(), Gradient.UnsignedRemainderDecimal(9), State->IntegrationCount
                                  );

                        memset(State->mORALastPpm, 0, sizeof(State->mORALastPpm));
                        memset(State->mORALastCurrentError, 0, sizeof(State->mORALastCurrentError));
                        State->mORALastPpmIndex = 0;

                        return OutputCoordinatorNoError;
                    }
                }
                else
                {
                    CalculateOutputRateAdjustment(Context, State, CurrentError, i);

                    // If we are the master then transfer this to the system clock adjustment
                    // but inverted, if we needed to speed up, we do this by slowing the system clock
                    // if we needed to slow down, we do this by speeding up the system clock.

                    if (ClockMaster)
                    {
                        // Adjust time mapping to account for the new slope
                        Rational_t NewSystemClockAdjustment = 1 / State->ClockAdjustment;

                        int CurrentPpm = GradientToPartsPerMillion(SystemClockAdjustment);
                        int NewPpm = GradientToPartsPerMillion(NewSystemClockAdjustment);

                        Rational_t SysChange   = NewSystemClockAdjustment - SystemClockAdjustment;
                        long long SinceTm      = (long long)OS_GetTimeInMicroSeconds() - MasterBaseSystemTime;
                        long long Jerk         = -LongLongIntegerPart(SysChange * Rational_t(SinceTm));

                        if (CurrentPpm != NewPpm)
                        {
                            // If we are the master then transfer this to the system clock adjustment
                            // but inverted, if we needed to speed up, we do this by slowing the system clock
                            // if we needed to slow down, we do this by speeding up the system clock.

                            SE_INFO(group_avsync, "Stream 0x%p clock(%d-%d) clock Output rate adjustment changed : %d ppm -> %d ppm (%d.%09d) on sysclock. SinceTm=%lld SysChange %d.%09d Time mapping adjusted by %lld\n",
                                    Context->Stream, Context->StreamType, i,
                                    CurrentPpm, NewPpm,
                                    NewSystemClockAdjustment.IntegerPart(), NewSystemClockAdjustment.UnsignedRemainderDecimal(9),
                                    SinceTm,
                                    SysChange.IntegerPart(), SysChange.UnsignedRemainderDecimal(9),
                                    Jerk);
                        }
                        else
                        {
                            SE_EXTRAVERB(group_avsync, "Stream 0x%p clock(%d-%d) clock Output rate adjustment virtually unchanged: %d ppm\n",
                                         Context->Stream, Context->StreamType, i, CurrentPpm);
                        }

                        SE_VERBOSE(group_player, "Stream 0x%p clock(%d-%d) Adjusting Time Mapping by %lld (SystemClockAdjustment %d ppm (%d.%09d) MasterBaseSystemTime %llu)\n",
                                   Context->Stream, Context->StreamType, i,
                                   Jerk, CurrentPpm, SystemClockAdjustment.IntegerPart(),
                                   SystemClockAdjustment.UnsignedRemainderDecimal(9), MasterBaseSystemTime);

                        MasterBaseSystemTime   -= Jerk;

                        SystemClockAdjustment               = NewSystemClockAdjustment;
                        SystemClockAdjustmentEstablished    = true;
                        Context->OutputRateAdjustments[i]   = 1;
                        Context->CurrentErrorJitterUs[i]    = 0;
                    }
                    else if (SystemClockAdjustmentEstablished)
                    {
                        // In the case where we are not the master update the value to be applied
                        // on the output clock
                        int CurrentPpm = GradientToPartsPerMillion(Context->OutputRateAdjustments[i]);
                        int NewPpm = GradientToPartsPerMillion(State->ClockAdjustment);
                        if (CurrentPpm != NewPpm)
                        {
                            Context->OutputRateAdjustments[i] = State->ClockAdjustment;

                            SE_INFO(group_avsync, "Output rate adjustment changed (%s,%d): %d ppm -> %d ppm (delta_ppm %d ppm) (%d.%09d)\n",
                                    StreamType(), i, CurrentPpm, NewPpm, NewPpm - CurrentPpm,
                                    State->ClockAdjustment.IntegerPart(), State->ClockAdjustment.UnsignedRemainderDecimal(9));
                        }
                        else
                        {
                            SE_EXTRAVERB(group_avsync, "Output rate adjustment unchanged (%s,%d): %d ppm\n",
                                         StreamType(), i, CurrentPpm);
                        }
                    }
                    else
                    {
                        SE_VERBOSE(group_avsync, "Stream 0x%p clock(%d-%d) system Clock adjustment not compensated\n", Context->Stream, Context->StreamType, i);
                    }

                    //
                    // Finally prepare for next integration period
                    //
                    State->IntegrationCount     = 0;
                    State->LeastSquareFit.Reset();
                    State->mORALastPpmIndex = 0;
                    memset(State->mORALastPpm, 0, sizeof(State->mORALastPpm));
                    memset(State->mORALastCurrentError, 0, sizeof(State->mORALastCurrentError));
                }
            }
        }

        //
        // Update statistics
        //
        Context->Stream->Statistics().OutputRateFramesToIntegrateOver[i] = INTEGRATION_PERIOD;
        Context->Stream->Statistics().OutputRateIntegrationCount[i] = State->IntegrationCount;
        Context->Stream->Statistics().OutputRateClockAdjustment[i] = GradientToPartsPerMillion(State->ClockAdjustment);

        // Enable the capture of clock-data points through strelay to easy debugging of the clock-recovery algorithm
        // It provides a mean to record all the data-points and intermediate gradient computation that can then
        // be reviewed later by loading the data in a spreadsheet and post-process them
        {
            struct data_point_t
            {
                OutputCoordinatorContext_t Context;
                uint32_t                   SurfaceIndex;
                uint64_t                   ExpectedTime;
                uint64_t                   ActualTime;
                int64_t                    ExpectedDuration;
                int64_t                    ActualDuration;
                int                        OutputRateAdjustment_int;
                int                        OutputRateAdjustment_dec;

                int                        OutputRateGradient;
                unsigned int               OutputRateFramesToIntegrateOver;
                unsigned int               OutputRateIntegrationCount;
                int                        OutputRateClockAdjustment;
            } DataPoint =
            {
                Context, i,
                ExpectedTime    , ActualTime,
                ExpectedDuration, ActualDuration,
                State->ClockAdjustment.IntegerPart(),
                State->ClockAdjustment.RemainderDecimal(9),
                Context->Stream->Statistics().OutputRateGradient[i],
                Context->Stream->Statistics().OutputRateFramesToIntegrateOver[i],
                Context->Stream->Statistics().OutputRateIntegrationCount[i],
                Context->Stream->Statistics().OutputRateClockAdjustment[i],
            };

            if (Context->StreamType == StreamTypeAudio)
            {
                st_relayfs_write_se(ST_RELAY_TYPE_AUDIO_CLOCK_DATAPOINT, ST_RELAY_SOURCE_SE,
                                    (unsigned char *)&DataPoint, sizeof(DataPoint), false);
            }
            else
            {
                st_relayfs_write_se(ST_RELAY_TYPE_VIDEO_CLOCK_DATAPOINT, ST_RELAY_SOURCE_SE,
                                    (unsigned char *)&DataPoint, sizeof(DataPoint), false);
            }
        }
    } // end foreach Surface

    //
    // Finally set the return parameters
    //
    *ParamOutputRateAdjustments     = Context->OutputRateAdjustments;
    *CurrentErrorJitterUs           = Context->CurrentErrorJitterUs;
    *ParamSystemClockAdjustment     = SystemClockAdjustment;

    return OutputCoordinatorNoError;
}

// /////////////////////////////////////////////////////////////////////////
//
//      The function to restart the output rate adjustment
//

void OutputCoordinator_Base_c::RestartOutputRateIntegration(OutputCoordinatorContext_t Context,
                                                            bool                       ResetCount,
                                                            unsigned int               TimingIndex)
{
    SE_VERBOSE2(group_se_pipeline, group_avsync, ">\n");
    unsigned int LoopStart = (TimingIndex == INVALID_INDEX) ? 0 : TimingIndex;
    unsigned int LoopEnd   = (TimingIndex == INVALID_INDEX) ? MAXIMUM_MANIFESTATION_TIMING_COUNT : (TimingIndex + 1);

    for (unsigned int i = LoopStart; i < LoopEnd; i++)
    {
        TimingContext_t *State = &Context->TimingState[i];

        State->IntegrationCount             = 0;
        State->LeastSquareFit.Reset();
        memset(State->mORALastPpm, 0, sizeof(State->mORALastPpm));
        memset(State->mORALastCurrentError, 0, sizeof(State->mORALastCurrentError));
        State->mORALastPpmIndex = 0;
        Context->Stream->Statistics().OutputRateFramesToIntegrateOver[i] = INTEGRATION_PERIOD;
        Context->Stream->Statistics().OutputRateIntegrationCount[i] = State->IntegrationCount;
    }
}

// /////////////////////////////////////////////////////////////////////////
//
//      The function to restart the output rate adjustment
//

void OutputCoordinator_Base_c::ResetTimingContext(OutputCoordinatorContext_t  Context,
                                                  unsigned int            TimingIndex)
{
    unsigned int LoopStart = (TimingIndex == INVALID_INDEX) ? 0 : TimingIndex;
    unsigned int LoopEnd   = (TimingIndex == INVALID_INDEX) ? MAXIMUM_MANIFESTATION_TIMING_COUNT : (TimingIndex + 1);

    for (unsigned int i = LoopStart; i < LoopEnd; i++)
    {
        TimingContext_t *State = &Context->TimingState[i];
        State->IntegrationCount         = 0;
        State->LeastSquareFit.Reset();
        memset(State->mORALastPpm, 0, sizeof(State->mORALastPpm));
        memset(State->mORALastCurrentError, 0, sizeof(State->mORALastCurrentError));
        State->mORALastPpmIndex = 0;
        State->LastExpectedTime         = INVALID_TIME;
        State->LastActualTime           = INVALID_TIME;
        State->ClockAdjustmentEstablished   = false;
        State->ClockAdjustment          = 1;
        State->CurrentErrorJitterUs     = 0;

        State->VideoFrameDuration       = 41666;        // Give a 24fps default value

        Context->OutputRateAdjustments[i]   = 1;
        Context->CurrentErrorJitterUs[i]    = 0;
        // reset statistics
        Context->Stream->Statistics().OutputRateFramesToIntegrateOver[i] = INTEGRATION_PERIOD;
        Context->Stream->Statistics().OutputRateIntegrationCount[i] = State->IntegrationCount;
    }
}

// /////////////////////////////////////////////////////////////////////////
//
//      Private - This function trawls the contexts and deduces the next
//      system time at which we can start playing.
//

OutputCoordinatorStatus_t   OutputCoordinator_Base_c::GenerateTimeMapping(void *ParsedAudioVideoDataParameters)
{
    unsigned int              i;
    unsigned long long       *ManifestationTimes;
    unsigned long long       *ManifestationGranularities;
    TimeStamp_c               EarliestPlaybackTime;
    TimeStamp_c               EarliestVideoPlaybackTime;
    TimeStamp_c               LatestPlaybackTime;

    //
    // Calculate the stream offset values
    //

    OutputCoordinatorContext_t LoopContext;
    for (LoopContext     = CoordinatedContexts;
         LoopContext    != NULL;
         LoopContext     = LoopContext->Next)
    {
        if (LoopContext->InSynchronizeFn)
        {
            if (!EarliestPlaybackTime.IsValid() || LoopContext->SynchronizingAtPlaybackTime < EarliestPlaybackTime)
            {
                EarliestPlaybackTime = LoopContext->SynchronizingAtPlaybackTime;
            }

            if ((LoopContext->StreamType == StreamTypeVideo) &&
                (!EarliestVideoPlaybackTime.IsValid() || LoopContext->SynchronizingAtPlaybackTime < EarliestVideoPlaybackTime))
            {
                EarliestVideoPlaybackTime = LoopContext->SynchronizingAtPlaybackTime;
            }

            if (!LatestPlaybackTime.IsValid() || LoopContext->SynchronizingAtPlaybackTime > LatestPlaybackTime)
            {
                LatestPlaybackTime = LoopContext->SynchronizingAtPlaybackTime;
            }
        }
    }

    for (LoopContext     = CoordinatedContexts;
         LoopContext    != NULL;
         LoopContext     = LoopContext->Next)
    {
        if (LoopContext->InSynchronizeFn)
        {
            LoopContext->StreamOffset = TimeStamp_c::DeltaUsec(LoopContext->SynchronizingAtPlaybackTime, EarliestPlaybackTime);
        }
    }

    // Now scan the streams (and recursively the manifestors associated
    // with them), to find the earliest start time for playback.
    //
    unsigned long long Now = OS_GetTimeInMicroSeconds();
    unsigned long long LatestBaseStartTime = Now - TimeStamp_c::DeltaUsec(LatestPlaybackTime, EarliestPlaybackTime);
    unsigned long long LatestBaseVideoStartTime;
    TimeStamp_c LatestBaseStartTimePlaybackTime = EarliestPlaybackTime;

    if (EarliestVideoPlaybackTime.IsValid())
    {
        LatestBaseVideoStartTime = Now - TimeStamp_c::DeltaUsec(LatestPlaybackTime, EarliestVideoPlaybackTime);
    }
    else
    {
        LatestBaseVideoStartTime = 0;
    }

    TimeStamp_c LatestBaseVideoStartTimePlaybackTime = EarliestVideoPlaybackTime;
    unsigned long long LargestGranularity = 1;

    for (LoopContext  = CoordinatedContexts;
         LoopContext != NULL;
         LoopContext  = LoopContext->Next)
    {
        //
        // Get the manifestation times,.
        //
        LoopContext->ManifestorLatency  = MINIMUM_TIME_TO_MANIFEST;
        ParsedAudioVideoParameters_t *parsed_data_parameters = (ParsedAudioVideoParameters_t *)ParsedAudioVideoDataParameters;
        if (parsed_data_parameters != NULL && LoopContext->StreamType != parsed_data_parameters->StreamType)
        {
            // don't pass on parsed data parameters not of same stream type as loopcontext
            SE_DEBUG(group_avsync, "LoopContext StreamType %d != parsed_data_parameters StreamType %d\n",
                     LoopContext->StreamType, parsed_data_parameters->StreamType);
            parsed_data_parameters = NULL;
        }
        LoopContext->ManifestationCoordinator->GetNextQueuedManifestationTimes(parsed_data_parameters, &ManifestationTimes, &ManifestationGranularities);

        if (LoopContext->InSynchronizeFn)
        {
            //
            // Cycle through the manifestation times to find the latest, and the largest granularity
            //
            for (i = 0; i <= LoopContext->MaxSurfaceIndex; i++)
                if (ValidTime(ManifestationTimes[i]))
                {
                    unsigned long long BaseStartTime = ManifestationTimes[i] - LoopContext->StreamOffset;

                    SE_DEBUG(group_avsync, "StreamType=%d LatestBaseStartTime=%llu BaseStartTime=%llu ManifestationTimes[%d]=%llu StreamOffset=%llu\n",
                             LoopContext->StreamType, LatestBaseStartTime, BaseStartTime, i, ManifestationTimes[i], LoopContext->StreamOffset);

                    if (BaseStartTime > LatestBaseStartTime)
                    {
                        LatestBaseStartTime = BaseStartTime;
                        MasterTimeMappingEstablishedOnType = LoopContext->StreamType;
                    }

                    if ((LoopContext->StreamType == StreamTypeVideo) &&
                        (BaseStartTime + TimeStamp_c::DeltaUsec(EarliestVideoPlaybackTime, EarliestPlaybackTime) > LatestBaseVideoStartTime))
                    {
                        LatestBaseVideoStartTime = BaseStartTime + TimeStamp_c::DeltaUsec(EarliestVideoPlaybackTime, EarliestPlaybackTime);
                    }

                    if (ManifestationGranularities[i] > LargestGranularity)
                    {
                        LargestGranularity = ManifestationGranularities[i];
                    }

                    if ((long long)(ManifestationTimes[i] - Now) > (long long)LoopContext->ManifestorLatency)
                    {
                        LoopContext->ManifestorLatency = ManifestationTimes[i] - Now;
                    }
                }
        }
        else
        {
            //
            // Not reached synchronize, just calculate the manifestor latency for this stream
            //
            for (i = 0; i <= LoopContext->MaxSurfaceIndex; i++)
                if (ValidTime(ManifestationTimes[i]))
                {
                    if ((long long)(ManifestationTimes[i] - Now) > (long long)LoopContext->ManifestorLatency)
                    {
                        LoopContext->ManifestorLatency = ManifestationTimes[i] - Now;
                    }
                }
        }

        //
        // Warn if some of these numbers are a tad dubious
        //

        if (LoopContext->ManifestorLatency > 1000000ull)
        {
            SE_WARNING("Long latency %12lldus for stream type %d\n", LoopContext->ManifestorLatency, LoopContext->StreamType);
        }
    }

    //
    // Apply a delay, to compensate for variable latency in the a live input stream.
    //
    int LatencyVariability  = Player->PolicyValue(Playback, PlayerAllStreams, PolicyLivePlaybackLatencyVariabilityMs);
    if ((LatencyVariability != 0) &&
        (Player->PolicyValue(Playback, PlayerAllStreams, PolicyLivePlayback) == PolicyValueApply))
    {
        LatestBaseStartTime += ((unsigned long long)LatencyVariability) * 1000; //making ms to micro second
        SE_INFO(group_avsync, "LivePlaybackLatency:%dms\n", LatencyVariability);
    }

    //
    // Revisit the result for video start immediate
    //
    bool VideoStartImmediate = (Player->PolicyValue(Playback, PlayerAllStreams, PolicyVideoStartImmediate) == PolicyValueApply) &&
                               !(Player->PolicyValue(Playback, PlayerAllStreams, PolicyLivePlayback) == PolicyValueApply);
    if (VideoStartImmediate && EarliestVideoPlaybackTime.IsValid())
    {
        LatestBaseStartTime             = LatestBaseVideoStartTime;
        LatestBaseStartTimePlaybackTime = LatestBaseVideoStartTimePlaybackTime;
        SE_INFO(group_avsync, "VideoStartImmediate is set -> LatestBaseStartTime=%llu LatestBaseStartTimePlaybackTime=%lld\n",
                LatestBaseStartTime, LatestBaseStartTimePlaybackTime.NativeValue());
        MasterTimeMappingEstablishedOnType = StreamTypeVideo;
    }

    //
    // Now generate the mapping
    //
    MasterBasePts                = LatestBaseStartTimePlaybackTime;
    MasterBaseSystemTime         = LatestBaseStartTime;
    MasterTimeMappingEstablished = true;
    mInitialDeltaToPcr           = INVALID_TIME;

    SE_INFO(group_se_pipeline, "Generated mapping (%lld,%llu)\n", MasterBasePts.NativeValue(), MasterBaseSystemTime);

    return OutputCoordinatorNoError;
}

OutputCoordinatorStatus_t   OutputCoordinator_Base_c::GenerateTimeMappingWithPCR()
{
    unsigned long long Now = OS_GetTimeInMicroSeconds();

    // Use the PCR info as the most reliable info.

    unsigned long long LatestBaseStartTime  = mClockRecoveryLastValidLocalTime;
    TimeStamp_c LatestBaseStartTimePlaybackTime = mClockRecoveryLastValidPcr;

    SE_INFO(group_avsync, "Time mapping has been established using PCR  (SourceTime=%llu, SystemTime=%llu)\n",
            mClockRecoveryLastValidPcr.NativeValue(), mClockRecoveryLastValidLocalTime);

    //
    // Apply a Maximum manifestor latency to the mapping derived from the PCR.
    //
    mInitialDeltaToPcr = NETWORK_DEFAULT_LATENCY + MAX_MANIFESTOR_LATENCY + PROPAGATION_TIME_LATENCY + PROPAGATION_TIME_JITTER_MARGIN;
    //
    // Apply a delay, to compensate for variable latency in the a live input stream.
    //
    int LatencyVariability  = Player->PolicyValue(Playback, PlayerAllStreams, PolicyLivePlaybackLatencyVariabilityMs);

    if ((LatencyVariability != 0) &&
        (Player->PolicyValue(Playback, PlayerAllStreams, PolicyLivePlayback) == PolicyValueApply))
    {
        mInitialDeltaToPcr += ((unsigned long long)LatencyVariability) * 1000; // making ms to micro second
        SE_INFO(group_avsync, "LivePlaybackLatency:%dms\n", LatencyVariability);
    }

    LatestBaseStartTime += mInitialDeltaToPcr;

    // Now generate the mapping
    MasterBasePts        = LatestBaseStartTimePlaybackTime;
    MasterBaseSystemTime = LatestBaseStartTime;

    if (mPcrLastMappingDate != 0)
    {
        long long int PcrMappingDelta = Now - mPcrLastMappingDate;
        if (PcrMappingDelta < PCR_MAPPING_TOO_CLOSE_DELTA_THRESHOLD)
        {
            SE_WARNING("PCR mapping request too close (%lld < %d)\n", PcrMappingDelta, PCR_MAPPING_TOO_CLOSE_DELTA_THRESHOLD);
            mPcrCloseMappings++;
            if (mPcrCloseMappings > PCR_MAPPING_TOO_CLOSE_COUNT_THRESHOLD)
            {
                SE_ERROR("Too many (%d > %d) consecutive PCR mapping requests, draining live playback\n"
                         , mPcrCloseMappings, PCR_MAPPING_TOO_CLOSE_COUNT_THRESHOLD);
                DrainLivePlayback();
                mPcrCloseMappings = 0;
            }
        }
        else
        {
            mPcrCloseMappings = 0;
        }
    }
    mPcrLastMappingDate = Now;
    MasterTimeMappingEstablished = true;
    MasterTimeMappingEstablishedOnType = StreamTypePcr;
    SE_INFO(group_se_pipeline, "Generated mapping using PCR (%lld,%llu)\n", MasterBasePts.NativeValue(), MasterBaseSystemTime);

    return OutputCoordinatorNoError;
}

void   OutputCoordinator_Base_c::GenerateTimeMappingForCapture(OutputCoordinatorContext_t Context, void *ParsedAudioVideoDataParameters)
{
    TimeStamp_c EarliestPlaybackTime = Context->SynchronizingAtPlaybackTime;
    LatencyUsedInCaptureModeDuringTimeMapping = GetLargestOfMinimumManifestationLatencies(ParsedAudioVideoDataParameters);
    SE_INFO(group_avsync, "Applying %lld Latency for HdmiRx\n", LatencyUsedInCaptureModeDuringTimeMapping);
    EarliestPlaybackTime  = TimeStamp_c::AddUsec(EarliestPlaybackTime, LatencyUsedInCaptureModeDuringTimeMapping);

    //
    // Now generate the mapping
    //
    MasterBaseSystemTime = EarliestPlaybackTime.uSecValue();
}

long long OutputCoordinator_Base_c::GetLargestOfMinimumManifestationLatencies(void *ParsedAudioVideoDataParameters)
{
    long long MaxVideoManifestorLatency = 0;
    long long MaxVideoInputLatency = 0;

    unsigned long long Now = OS_GetTimeInMicroSeconds();

    for (OutputCoordinatorContext_t LoopContext = CoordinatedContexts;
         LoopContext != NULL;
         LoopContext  = LoopContext->Next)
    {
        if (LoopContext->InSynchronizeFn)
        {
            if (LoopContext->StreamType == StreamTypeVideo)
            {
                ParsedVideoParameters_t *parsed_video_data_parameters = (ParsedVideoParameters_t *)ParsedAudioVideoDataParameters;
                if ((parsed_video_data_parameters != NULL) && parsed_video_data_parameters->Base.StreamType == StreamTypeVideo)
                {
                    SE_ASSERT(parsed_video_data_parameters->Content.FrameRate != 0);
                    long long VideoInputLatency = RoundedLongLongIntegerPart(1000000 / parsed_video_data_parameters->Content.FrameRate);
                    if (VideoInputLatency > MaxVideoInputLatency)
                    {
                        MaxVideoInputLatency = VideoInputLatency;
                    }
                    unsigned long long *ManifestationTimes;
                    unsigned long long *ManifestationGranularities;
                    LoopContext->ManifestationCoordinator->GetNextQueuedManifestationTimes(parsed_video_data_parameters, &ManifestationTimes, &ManifestationGranularities);
                    for (int i = 0; i <= LoopContext->MaxSurfaceIndex; i++)
                    {
                        if (ValidTime(ManifestationTimes[i]))
                        {
                            if ((long long)(ManifestationTimes[i] - Now) > MaxVideoManifestorLatency)
                            {
                                MaxVideoManifestorLatency = ManifestationTimes[i] - Now;
                            }
                        }
                    }
                }
                else
                {
                    SE_ERROR("invalid parsed_video_data_parameters in video context\n");
                }
            }
        }
    }

    long long LargestOfMinimumManifestationLatencies = MaxVideoManifestorLatency + MaxVideoInputLatency + MODULES_PROPAGATION_LATENCY;

    if ((Player->PolicyValue(Playback, Stream, PolicyCaptureProfile) == PolicyValueCaptureProfileHdmiRxNoAudioDecodeNoVideoFrc) ||
        (Player->PolicyValue(Playback, Stream, PolicyCaptureProfile) == PolicyValueCaptureProfileHdmiRxNoAudioDecodeVideoFrc))
    {
        LargestOfMinimumManifestationLatencies = Max(static_cast<long long int>(HDMIRX_NO_DECODE_MODE_LATENCY), LargestOfMinimumManifestationLatencies) ;
    }
    else if ((Player->PolicyValue(Playback, Stream, PolicyCaptureProfile) == PolicyValueCaptureProfileHdmiRxAudioDecodeVideoFrc) ||
             (Player->PolicyValue(Playback, Stream, PolicyCaptureProfile) == PolicyValueCaptureProfileHdmiRxAudioDecodeNoVideoFrc))
    {
        LargestOfMinimumManifestationLatencies = Max(static_cast<long long int>(HDMIRX_DECODE_MODE_LATENCY), LargestOfMinimumManifestationLatencies) ;
    }

    return LargestOfMinimumManifestationLatencies;
}

// /////////////////////////////////////////////////////////////////////////
//
//      The function to verify the master clock, will identify what
//      is the master clock, and ensure that it is still extant.
//

void OutputCoordinator_Base_c::VerifyMasterClock()
{
    //
    // Separate a first scan just for the surface descriptors
    //

    OutputCoordinatorContext_t  ContextLoop;
    for (ContextLoop  = CoordinatedContexts;
         ContextLoop != NULL;
         ContextLoop  = ContextLoop->Next)
    {
        ContextLoop->ManifestationCoordinator->GetSurfaceParameters(&ContextLoop->Surfaces,  &ContextLoop->MaxSurfaceIndex);
    }

    //
    // If it is the system clock, then all is well
    //
    int MasterClock = Player->PolicyValue(Playback, PlayerAllStreams, PolicyMasterClock);
    if (MasterClock == PolicyValueSystemClockMaster)
    {
        UseSystemClockAsMaster  = true;
        return;
    }

    //
    // Scan
    //
    bool GotPrimary = false;
    OutputCoordinatorContext_t PrimaryContext = NULL;
    unsigned int PrimaryIndex = 0;

    bool GotSecondary = false;
    OutputCoordinatorContext_t SecondaryContext = NULL;
    unsigned int  SecondaryIndex = 0;

    for (ContextLoop  = CoordinatedContexts;
         ContextLoop != NULL;
         ContextLoop  = ContextLoop->Next)
    {
        //
        // Found it, and it is OK
        //
        if (ContextLoop->ClockMaster && (ContextLoop->Surfaces[ContextLoop->ClockMasterIndex] != NULL))
        {
            return;
        }

        ContextLoop->ClockMaster = false;    // Just in case it went away

        //
        // Not found it yet, is this a possible
        //
        unsigned int i;

        if (!GotPrimary && (((MasterClock == PolicyValueVideoClockMaster) && (ContextLoop->StreamType == StreamTypeVideo)) ||
                            ((MasterClock == PolicyValueAudioClockMaster) && (ContextLoop->StreamType == StreamTypeAudio))))
        {
            for (i = 0; i <= ContextLoop->MaxSurfaceIndex; i++)
                if ((ContextLoop->Surfaces[i] != NULL) && ContextLoop->Surfaces[i]->MasterCapable)
                {
                    GotPrimary      = true;
                    PrimaryContext  = ContextLoop;
                    PrimaryIndex    = i;
                    break;
                }
        }

        if (!GotPrimary && !GotSecondary)
        {
            for (i = 0; i <= ContextLoop->MaxSurfaceIndex; i++)
                if ((ContextLoop->Surfaces[i] != NULL) && ContextLoop->Surfaces[i]->MasterCapable)
                {
                    GotSecondary     = true;
                    SecondaryContext = ContextLoop;
                    SecondaryIndex   = i;
                    break;
                }
        }
    }

    //
    // The fact that we got here implies that we didn't
    // have a master, or it has gone away.
    //

    if (GotPrimary)
    {
        PrimaryContext->ClockMasterIndex = PrimaryIndex;
        PrimaryContext->ClockMaster      = true;
        UseSystemClockAsMaster           = false;
    }
    else if (GotSecondary)
    {
        SecondaryContext->ClockMasterIndex = SecondaryIndex;
        SecondaryContext->ClockMaster      = true;
        UseSystemClockAsMaster             = false;
    }
    else
    {
        UseSystemClockAsMaster = true;
    }

    //
    // The fact we got here without use system clock as master
    // implies that we have a new master clock
    //
    SystemClockAdjustmentEstablished = false;
}

// /////////////////////////////////////////////////////////////////////////
//
//      The function to restart clock recovery. Used mainly in case of errors
//

void OutputCoordinator_Base_c::ClockRecoveryRestart(bool conservative)
{
    mClockRecoveryBasePcr              = TimeStamp_c();
    mClockRecoveryPreviousPcr          = TimeStamp_c();
    mClockRecoveryBaseLocalClock       = INVALID_TIME;
    mClockRecoveryPreviousLocalTime    = INVALID_TIME;
    //
    // We only reset the system clock here if the system clock is master
    //
    int MasterClock  = Player->PolicyValue(Playback, PlayerAllStreams, PolicyMasterClock);

    if (MasterClock != PolicyValueSystemClockMaster)
    {
        SE_WARNING("Performing clock recovery when system clock is NOT master\n");
        return;
    }

    SystemClockAdjustmentEstablished = true;

    if (!conservative)
    {
        //  we assume that the local clock is 1:1 with the source clock
        mClockRecoveryEstablishedGradient = 1;
        SystemClockAdjustment             = 1;
    }

    mClockRecoveryLastPpmIndex = 0;
    memset(mClockRecoveryLastPpm, 0, sizeof(mClockRecoveryLastPpm));
}

// /////////////////////////////////////////////////////////////////////////
//
//      The function to initialize clock recovery when we have a system
//      clock master, driven by an external broadcast source such as
//      a satelite.
//      We will allow clock recovery when system clock is not master,
//      but we will not adjust the system clock.
//

void OutputCoordinator_Base_c::ClockRecoveryInitialize(stm_se_time_format_t SourceTimeFormat)
{
    SE_INFO(group_avsync, "\n");

    mClockRecoverySourceTimeFormat       = SourceTimeFormat;
    mClockRecoveryInitialized            = true;
    mClockRecoveryLastValidPcr           = TimeStamp_c();
    mClockRecoveryLastValidLocalTime     = INVALID_TIME;

    mClockRecoveryPCRInterpolator.Reset(PCR_FORWARD_JUMP_THRESHOLD);

    ClockRecoveryRestart(false);
    ResetTimeMapping(PlaybackContext);
    for (OutputCoordinatorContext_t ContextLoop = CoordinatedContexts; ContextLoop != NULL; ContextLoop = ContextLoop->Next)
    {
        ResetTransitionContext(ContextLoop);
    }
}

// /////////////////////////////////////////////////////////////////////////
//
//      This function checks if Clock Recovery is initialized
//

void OutputCoordinator_Base_c::ClockRecoveryIsInitialized(bool *Initialized)
{
    *Initialized = mClockRecoveryInitialized;
}

// /////////////////////////////////////////////////////////////////////////
//
//      This function checks whether a given datapoint is significant :
//      0  : keep this datapoint
//      1  : ignore the rest of the processing

int  OutputCoordinator_Base_c::CheckDataPointBurstStatus(TimeStamp_c Pcr,
                                                         unsigned long long LocalTime,
                                                         bool *ComputeGradient)
{
    // When there is jitter in the transmission, only the last PCR datapoint in the burst is meaningful
    // for clock recovery
    // We detect this by discarding datapoint having a very close LocalTime and keep only the last one
    if (mClockRecoveryPreviousPcr.IsValid() && ValidTime(mClockRecoveryPreviousLocalTime))
    {
        long long deltaToLastSourceTime = TimeStamp_c::DeltaUsec(Pcr, mClockRecoveryPreviousPcr);
        long long deltaToLastLocalTime  = LocalTime - mClockRecoveryPreviousLocalTime;

        if (deltaToLastLocalTime * 2 < deltaToLastSourceTime)
        {
            // Not a relevant datapoint, let's not compute gradient for this one
            *ComputeGradient = false;
        }
        // In a normal case, both delta should be similar
        // In a burst effect, localtimes are very close from each other.
        // We arbitrarily chose to discard datapoints with localtime too close to each other
        // as they are probably not meaningful
        // However, we must prevent spurious datapoint to activate this logic by using a threshold on PCR delta
        if (deltaToLastSourceTime < MAX_DELTA_BETWEEN_PCR_US && deltaToLastLocalTime * 100 < deltaToLastSourceTime)
        {
            SE_DEBUG2(group_se_pipeline, group_avsync
                      , "Discarding datapoint, localtime %lld too close to previous %lld delta_us %lld delta_pcr_us %lld\n"
                      , LocalTime, mClockRecoveryPreviousLocalTime, deltaToLastLocalTime, deltaToLastSourceTime);

            mClockRecoveryPreviousLocalTime = LocalTime;
            mClockRecoveryPreviousPcr = Pcr;

            // This is NOT an invalid point so we should not update statistics
            // TODO : or create a BurstDicarded dedicated stat
            //mClockRecoveryDiscardedPoints++;
            //Playback->GetStatistics().ClockRecoveryCummulativeDiscardedPoints++;
            return 1;
        }
    }

    *ComputeGradient = true;
    return 0;
}

// /////////////////////////////////////////////////////////////////////////
//
//      This function checks whether datapoint delta from previous should be
//      discarded or should reset the mapping
//      Return value :
//      0  : keep this datapoint
//      1  : ignore the rest of the processing

int  OutputCoordinator_Base_c::CheckDataPointDeltas(TimeStamp_c Pcr,
                                                    int64_t SystemTime,
                                                    int64_t DeltaPcr,
                                                    int64_t DeltaLocalTime)
{
    // Jitter between points cannot be greater than the maximum playback latency
    // (plus our own timestamping error)
    long MaximumPointToPointJitter = (long)Player->PolicyValue(Playback, PlayerAllStreams, PolicyLivePlaybackLatencyVariabilityMs) * 1000;
    MaximumPointToPointJitter += INTERNAL_TIMESTAMP_LATENCY_ESTIMATE;
    bool MightDiscardPoint = !inrange((long long)(DeltaLocalTime - DeltaPcr), -MaximumPointToPointJitter, MaximumPointToPointJitter);

    bool ShouldDiscard = false;
    bool ShouldReset = false;

    if (MightDiscardPoint)
    {
        ShouldDiscard = true;
        ShouldReset = false;

        // A major objective is to filter spurious PCR (from degraded stream or bad reception)
        //
        // Depending upon the PCR jump we operate this way :
        // 0. We compare the jump against the value given by PCR_FORWARD_JUMP_THRESHOLD
        // 1. If jump is higher than this value, we store the spurious jump value.
        // 2. We compare the jump to the previous jump value and if the jump is not confirmed we reset the jump counter
        // 3. When the jump counter is higher than MAX_DISCARDED_PCR_ON_BIG_JUMPS, we reset the mapping
        // With MAX_DISCARDED_PCR_ON_BIG_JUMPS == 1, this strategy will gracefully handle spurious PCR jumps

        int64_t playback_latency =  Player->PolicyValue(Playback, PlayerAllStreams, PolicyLivePlaybackLatencyVariabilityMs) * 1000ULL;
        int64_t MaximumAllowedForwardPcrJump = PCR_FORWARD_JUMP_THRESHOLD + playback_latency;
        int64_t MaximumAllowedBackwardPcrJump = 0;

        // Management of spurious jumps
        int64_t spurious = abs64(DeltaPcr - DeltaLocalTime);
        if ((DeltaPcr > 0 && spurious > MaximumAllowedForwardPcrJump)
            || (DeltaPcr < 0 && spurious > MaximumAllowedBackwardPcrJump))
        {
            mClockRecoveryDiscardedPointsOnBigJumps ++;

            if (mClockRecoveryLastSpuriousDelta == 0)
            {
                // This is the first big jump (or previous was invalidated)
                SE_WARNING("Big PCR jump detected, (this_delta=%lld) PCR discarded\n", spurious);
                mClockRecoveryLastSpuriousDelta = spurious;

            }
            else
            {
                // In line with previous jump ?
                int64_t threshold = playback_latency + INTERNAL_TIMESTAMP_LATENCY_ESTIMATE;

                if (abs64(spurious - mClockRecoveryLastSpuriousDelta) > threshold)
                {
                    // Big jump not validated
                    SE_WARNING("Big PCR jump not confirmed, reseting counter (this_delta=%lld last_delta=%lld threshold=%lld)\n",
                               spurious,
                               mClockRecoveryLastSpuriousDelta,
                               playback_latency);
                    mClockRecoveryDiscardedPointsOnBigJumps = 0;
                    mClockRecoveryConsecutiveUnconfirmedBigJumps ++;
                    mClockRecoveryLastSpuriousDelta = 0;
                    if (mClockRecoveryConsecutiveUnconfirmedBigJumps > MAX_CONSECUTIVE_PCR_BIG_JUMPS)
                    {
                        SE_WARNING("Too many (%d > %d) unconfirmed Big PCR jumps, resetting the mapping\n",
                                   mClockRecoveryConsecutiveUnconfirmedBigJumps,
                                   MAX_CONSECUTIVE_PCR_BIG_JUMPS);

                        ShouldReset = true;
                    }
                }
            }

            if (mClockRecoveryDiscardedPointsOnBigJumps > MAX_DISCARDED_PCR_ON_BIG_JUMPS)
            {
                SE_WARNING("Big PCR jump confirmed, resetting the mapping due to %d consecutive PCR jumps (delta=%lld)\n",
                           mClockRecoveryDiscardedPointsOnBigJumps,
                           (DeltaLocalTime - DeltaPcr));

                mClockRecoveryDiscardedPointsOnBigJumps = 0;
                mClockRecoveryLastSpuriousDelta = 0;
                ShouldReset = true;

                //EnterTransitionPeriod(NULL, Pcr, SystemTime, (DeltaPcr > 0) ? COORDINATOR_JUMP_FORWARD : COORDINATOR_JUMP_BACKWARD);

            }
        }
        else
        {
            mClockRecoveryDiscardedPointsOnBigJumps = 0;
            mClockRecoveryLastSpuriousDelta = 0;
        }
        // We reset the mapping if we obser a pause on PCR higher than a threshold given by PlaybackResetOnPcrPauseMs +  PlaybackLatency
        if (!ShouldReset && Player->PolicyValue(Playback, PlayerAllStreams, PolicyLivePlaybackResetOnPcrPauseMs))
        {
            long MaximumAllowedPcrPause = Player->PolicyValue(Playback, PlayerAllStreams, PolicyLivePlaybackResetOnPcrPauseMs) * 1000 + MaximumPointToPointJitter;

            if ((int64_t)(DeltaLocalTime - DeltaPcr) > MaximumAllowedPcrPause)
            {
                SE_WARNING("Resetting the mapping due to PCR pause %lld [allowed PCR pause %ld]\n", (DeltaLocalTime - DeltaPcr), MaximumAllowedPcrPause);
                ShouldReset = true;
            }
        }

        if (ShouldDiscard)
        {
            mClockRecoveryDiscardedPoints++;
            Playback->GetStatistics().ClockRecoveryCummulativeDiscardedPoints++;
            if (!ShouldReset && mClockRecoveryDiscardedPoints > CLOCK_RECOVERY_DISCARD_COUNT_THRESHOLD)
            {
                Playback->GetStatistics().ClockRecoveryCummulativeDiscardResets++;
                SE_WARNING("Too many PCR discarded (%d > %d): conservatively resetting integration\n"
                           , mClockRecoveryDiscardedPoints, CLOCK_RECOVERY_DISCARD_COUNT_THRESHOLD);
                ClockRecoveryRestart(true);
            }
        }

        if (ShouldReset)
        {
            ResetTimeMapping(PlaybackContext);
            ClockRecoveryRestart(false);
            // End of the story
            return 1;
        }

        if (ShouldDiscard)
        {
            return 1;
        }
    }
    else
    {
        mClockRecoveryDiscardedPointsOnBigJumps = 0;
        mClockRecoveryLastSpuriousDelta = 0;
        mClockRecoveryConsecutiveUnconfirmedBigJumps = 0;
    }

    return 0;
}

// /////////////////////////////////////////////////////////////////////////
//
//      This function sets all coordinators to transitionning state, where
//      former mapping is going to be used to retrieve a proper playback time
//      until the streams themselves wrap (almost immediately for audio, and
//      after stream offset for video)
void OutputCoordinator_Base_c::EnterTransitionPeriod(OutputCoordinatorContext_t Context, TimeStamp_c Pts, uint64_t SystemTime, TransitionState_e State)
{
    unsigned long long Now     = OS_GetTimeInMicroSeconds();
    if (State == COORDINATOR_UNSYNCED && Context != PlaybackContext)
    {
        unsigned long long Latency =  MINIMUM_TIME_TO_MANIFEST + PROPAGATION_TIME_LATENCY + PROPAGATION_TIME_JITTER_MARGIN;
        Context->TransitionSystemTime = Now + Latency;
        Context->TransitionPts = Pts;
        Context->TransitionLastSystemTime = Now + Latency;
        Context->TransitionLastPts = Pts;
        Context->TransitionLastMappingSystemTime = Now + Latency;
        Context->TransitionLastMappingPts = Pts;

        Context->TransitionState = State;
        SE_DEBUG(group_se_pipeline, "Stream 0x%p - %d - Entering unsynced period (%d) period from (%lld,%lld) to (%lld,%lld)\n"
                 , Context->Stream
                 , Context->StreamType
                 , Context->TransitionState
                 , Context->TransitionLastPts.NativeValue()
                 , Context->TransitionLastSystemTime
                 , Context->TransitionPts.NativeValue()
                 , Context->TransitionSystemTime);
        return;
    }

    for (OutputCoordinatorContext_t ContextLoop         = CoordinatedContexts;
         ContextLoop        != NULL;
         ContextLoop         = ContextLoop->Next)
    {

        if (ContextLoop->TransitionState == COORDINATOR_STABLE && ContextLoop->mLastValidPTS.IsValid())
        {
            TimeStamp_c           PTS;
            //
            // Translate that to playback time
            //
            bool Live = (Player->PolicyValue(Playback, PlayerAllStreams, PolicyLivePlayback) == PolicyValueApply);

            if (!Live || mClockRecoveryLastValidPcr.IsValid())
            {

                // Maybe the system is so unstable that we don't have a previous valid PCR
                //
                ContextLoop->TransitionSystemTime = SystemTime;
                ContextLoop->TransitionPts = Pts;
                ContextLoop->TransitionLastSystemTime = Now;
                if (Live && !ContextLoop->mLastValidPTS.IsValid())
                {
                    ContextLoop->TransitionLastPts = mClockRecoveryLastValidPcr;
                }
                else
                {
                    ContextLoop->TransitionLastPts = ContextLoop->mLastValidPTS;
                }
                ContextLoop->TransitionLastMappingSystemTime = MasterBaseSystemTime;
                ContextLoop->TransitionLastMappingPts = MasterBasePts;

                ContextLoop->TransitionState = State;
                SE_DEBUG(group_se_pipeline, "Stream 0x%p - %d - Entering time mapping transition (%d) period from (%lld,%lld) to (%lld,%lld)\n"
                         , ContextLoop->Stream
                         , ContextLoop->StreamType
                         , ContextLoop->TransitionState
                         , ContextLoop->TransitionLastPts.NativeValue()
                         , ContextLoop->TransitionLastSystemTime
                         , ContextLoop->TransitionPts.NativeValue()
                         , ContextLoop->TransitionSystemTime);
            }
            ContextLoop->InvalidTransitionsFromUnstableStateRequests = 0;
        }
        else
        {
            ContextLoop->InvalidTransitionsFromUnstableStateRequests++;
            SE_DEBUG(group_se_pipeline, "Stream 0x%p - %d - Already in unstable state (%d) (%d/%d) or no previous valid time\n"
                     , ContextLoop->Stream
                     , ContextLoop->StreamType
                     , ContextLoop->TransitionState
                     , ContextLoop->InvalidTransitionsFromUnstableStateRequests
                     , MAX_INVALID_TRANSITION_REQUESTS);
            if (ContextLoop->InvalidTransitionsFromUnstableStateRequests > MAX_INVALID_TRANSITION_REQUESTS)
            {
                ContextLoop->InvalidTransitionsFromUnstableStateRequests = 0;
                DrainLivePlayback();
            }
        }
    }
}

// /////////////////////////////////////////////////////////////////////////
//
//      This function checks the delta to PCR
//      Return value :
//      0  : keep this datapoint
//      1  : ignore the rest of the processing

int OutputCoordinator_Base_c::CheckShiftFromPCR(TimeStamp_c Pcr,
                                                unsigned long long InterpolatedLocalTime)
{
    if (Player->PolicyValue(Playback, PlayerAllStreams, PolicyLivePlayback) == PolicyValueApply)
    {
        unsigned long long PcrSystemPlaybackTime;
        OutputCoordinatorStatus_t Status = TranslatePlaybackTimeToSystem(PlaybackContext, Pcr, &PcrSystemPlaybackTime);

        if (Status == OutputCoordinatorNoError && ValidTime(mInitialDeltaToPcr))
        {
            long long ShiftFromPCR = (long long)PcrSystemPlaybackTime - InterpolatedLocalTime - mInitialDeltaToPcr;
            SE_VERBOSE(group_avsync, "PcrSystemPlaybackTime %lld (PcrSystemPlaybackTime - LocalTime) %lld mInitialDeltaToPcr %lld shift: %lld\n",
                       PcrSystemPlaybackTime, PcrSystemPlaybackTime - InterpolatedLocalTime,  mInitialDeltaToPcr, ShiftFromPCR);

            if (ShiftFromPCR < -PCR_MONITORING_THRESHOLD || ShiftFromPCR > PCR_MONITORING_THRESHOLD)
            {
                SE_WARNING("Delta between PCR and playback time greater than threshold (%lld > %d). Resetting mapping\n", ShiftFromPCR, PCR_MONITORING_THRESHOLD);
                ClockRecoveryRestart(false);
                ResetTimeMapping(PlaybackContext);
                return 1;
            }
        }
    }
    return 0;
}

int OutputCoordinator_Base_c::CheckGradientStability(Rational_t Gradient, Rational_t Intercept)
{
    ///////////////////////////////////////////////////////////////////////////
    //  Gradient stability computation
    ///////////////////////////////////////////////////////////////////////////

    int CurrentPpm = GradientToPartsPerMillion(Gradient);

    mClockRecoveryLastPpm[mClockRecoveryLastPpmIndex % LAST_GRADIENTS_COUNT] = CurrentPpm;
    mClockRecoveryLastPpmIndex++;

    unsigned int PpmIntegrationCount;
    if (mClockRecoveryLastPpmIndex < LAST_GRADIENTS_COUNT)
    {
        PpmIntegrationCount = mClockRecoveryLastPpmIndex;
    }
    else
    {
        PpmIntegrationCount = LAST_GRADIENTS_COUNT;
    }

    bool GradientStable = false;

    if (mClockRecoveryLeastSquareFit.X() >= CLOCK_RECOVERY_MINIMUM_DURATION)
    {
        long long PpmSum = 0;
        for (int k = 0; k < PpmIntegrationCount; k++)
        {
            PpmSum += mClockRecoveryLastPpm[k];
        }
        long long PpmMean = PpmSum / PpmIntegrationCount;
        long long PpmSumSqrDiff = 0;
        for (int k = 0; k < PpmIntegrationCount; k++)
        {
            int PpmDiff = (mClockRecoveryLastPpm[k] - PpmMean);
            PpmSumSqrDiff += PpmDiff * PpmDiff;
        }
        long long PpmStdDevSqr = PpmSumSqrDiff / PpmIntegrationCount;

        if (PpmStdDevSqr < CLOCK_RECOVERY_PPM_STABILITY_THRESHOLD_SQR
            && abs64(PpmMean) < CLOCK_RECOVERY_MAXIMUM_PPM
            && (CurrentPpm - PpmMean) < CLOCK_RECOVERY_PPM_STABILITY_THRESHOLD
            && mClockRecoveryLastPpmIndex >= LAST_GRADIENTS_COUNT)
        {
            GradientStable = true;
        }

        SE_EXTRAVERB2(group_se_pipeline, group_avsync
                      , "CurrentPpm=%d PpmCount=%d PpmMean=%lld PpmStdDevSqr=%lld thresh_sqr=%lld ppm index %d GradientStable=%d\n"
                      , CurrentPpm, PpmIntegrationCount, PpmMean, PpmStdDevSqr, CLOCK_RECOVERY_PPM_STABILITY_THRESHOLD_SQR, mClockRecoveryLastPpmIndex, GradientStable);
    }

    ///////////////////////////////////////////////////////////////////////////
    //  Let's now use this gradient or take corrective actions
    ///////////////////////////////////////////////////////////////////////////
    if (!GradientStable)
    {
        if (mClockRecoveryAccumulatedPoints >= CLOCK_RECOVERY_MAXIMUM_POINTS)
        {
            SE_WARNING("Gradient %d.%09d not stable over %d samples, Failed to recover valid clock - retrying\n"
                       , Gradient.IntegerPart(), Gradient.RemainderDecimal(9), mClockRecoveryAccumulatedPoints
                      );
            ClockRecoveryRestart(true);
            Playback->GetStatistics().ClockRecoveryClockAdjustment = GradientToPartsPerMillion(SystemClockAdjustment);
            Playback->GetStatistics().ClockRecoveryEstablishedGradient = GradientToPartsPerMillion(mClockRecoveryEstablishedGradient);
            return 1;
        }
    }
    else
    {
        // We have a stable gradient

        Playback->GetStatistics().ClockRecoveryActualGradient = CurrentPpm;

        mClockRecoveryEstablishedGradient        = Gradient;

        SE_VERBOSE2(group_avsync, group_se_pipeline, "ClockRecovery - IntegrationTime = %5llds (%5d), Gradient %d.%09d (%d ppm)  Intercept %4d.%09d\n",
                    mClockRecoveryLeastSquareFit.X() / 1000000,
                    mClockRecoveryAccumulatedPoints,
                    mClockRecoveryEstablishedGradient.IntegerPart(), mClockRecoveryEstablishedGradient.RemainderDecimal(9), GradientToPartsPerMillion(mClockRecoveryEstablishedGradient),
                    Intercept.IntegerPart(), Intercept.RemainderDecimal(9));
        //
        // Set us up for a reset
        //
        // We move the base values to the most recent PCR, but rather than use the
        // actual system clock value, we use the value of X that matches our line fit
        //
        mClockRecoveryBasePcr           = TimeStamp_c::AddUsec(mClockRecoveryBasePcr, mClockRecoveryLeastSquareFit.Y());
        mClockRecoveryBaseLocalClock    = mClockRecoveryBaseLocalClock + mClockRecoveryLeastSquareFit.X();
        mClockRecoveryAccumulatedPoints = 0;
        mClockRecoveryLeastSquareFit.Reset();
        // We should reset the gradient history
        mClockRecoveryLastPpmIndex = 0;
        memset(mClockRecoveryLastPpm, 0, sizeof(mClockRecoveryLastPpm));

        //
        // Do we need to adjust the system clock rate
        //
        int MasterClock = Player->PolicyValue(Playback, PlayerAllStreams, PolicyMasterClock);

        if (MasterClock == PolicyValueSystemClockMaster)
        {
            PerformMasterClockAdjustment();
        }
    }
    return 0;
}

void OutputCoordinator_Base_c::PerformMasterClockAdjustment(void)
{
    //
    // Before performing the system clock adjustment, we need to
    // compensate the mapping for the jerk affect this will have
    // on previous time.
    //
    // NOTE we need to know whether or not to cut the integration
    //      period for clock rate adjustment, we gate this on whether or
    //      the change is greater than 1.5ppm.
    //
    Rational_t NewSystemClockAdjustment  = mClockRecoveryEstablishedGradient;
    Rational_t SystemClockChange         = NewSystemClockAdjustment - SystemClockAdjustment;
    long long delta = (long long)(OS_GetTimeInMicroSeconds() - MasterBaseSystemTime);
    long long Jerk  = -LongLongIntegerPart(SystemClockChange * delta);
    bool ResetIntegrationPeriod    = !inrange(SystemClockChange, Rational_t(-3, 2000000), Rational_t(3, 2000000));

    if (!inrange(Jerk, -10000, 10000))
    {
        SE_WARNING("Big Jerk CR - %lld - Old %d.%09d, New %d.%09d, Change %d.%09d - TP %lld\n", Jerk,
                   SystemClockAdjustment.IntegerPart(), SystemClockAdjustment.RemainderDecimal(9),
                   NewSystemClockAdjustment.IntegerPart(), NewSystemClockAdjustment.RemainderDecimal(9),
                   SystemClockChange.IntegerPart(), SystemClockChange.RemainderDecimal(9),
                   delta);
    }

    SystemClockAdjustmentEstablished        = true;
    int CurrentPpm = GradientToPartsPerMillion(SystemClockAdjustment);
    int NewPpm = GradientToPartsPerMillion(NewSystemClockAdjustment);
    if (CurrentPpm != NewPpm)
    {
        SE_INFO2(group_avsync, group_se_pipeline, "SystemClockAdjustment %d ppm -> %d ppm  deltappm %d ppm delta_us %lld Jerk %lld\n", CurrentPpm, NewPpm, NewPpm - CurrentPpm, delta, Jerk);
    }
    else
    {
        SE_EXTRAVERB2(group_avsync, group_se_pipeline, "Unchanged SystemClockAdjustment %d ppm delta %lld Jerk %lld\n", CurrentPpm, delta, Jerk);
    }

    SystemClockAdjustment           = NewSystemClockAdjustment;
    SE_EXTRAVERB2(group_avsync, group_se_pipeline, "SystemClockAdjustment %dppm delta %lld Jerk %lld\n", GradientToPartsPerMillion(SystemClockAdjustment), delta, Jerk);
    MasterBaseSystemTime            -= Jerk;

    for (OutputCoordinatorContext_t ContextLoop         = CoordinatedContexts;
         ContextLoop        != NULL;
         ContextLoop         = ContextLoop->Next)
    {
        RestartOutputRateIntegration(ContextLoop, ResetIntegrationPeriod);
    }
}

// /////////////////////////////////////////////////////////////////////////
//
//      The function to handle clock recovery when we have a system
//      clock master, driven by an external broadcast source such as
//      a satelite.
//

OutputCoordinatorStatus_t   OutputCoordinator_Base_c::ClockRecoveryDataPoint(unsigned long long SourceTime,
                                                                             unsigned long long LocalTime)
{
    // Lock the component to thread-safely use ClockRecovery variables
    ComponentMonitor(this);

    if (!mClockRecoveryInitialized)
    {
        SE_ERROR("Clock recovery not initialized\n");
        return OutputCoordinatorClockRecoveryNotAvail;
    }

    TimeStamp_c Pcr = TimeStamp_c(SourceTime, mClockRecoverySourceTimeFormat);

    long long  InterpolatedLocalTime;
    mClockRecoveryPCRInterpolator.Interpolate(Pcr, LocalTime, &InterpolatedLocalTime);

    SE_DEBUG(group_se_pipeline, "Pcr=%lld LocalTime=%lld InterpolatedLocalTime=%lld\n",
             Pcr.NativeValue(), LocalTime, InterpolatedLocalTime);

    if (!ValidTime(mClockRecoveryPreviousLocalTime))
    {
        mClockRecoveryPreviousLocalTime = LocalTime;
        mClockRecoveryPreviousPcr = Pcr;

        return OutputCoordinatorNoError;
    }

    bool ComputeGradient = true;

    if (CheckDataPointBurstStatus(Pcr, LocalTime, &ComputeGradient) != 0)
    {
        return OutputCoordinatorNoError;
    }

    // Actually, we want to consider previous datapoint (it exists, we made sure before)
    // (we want the last point in a burst hence the one-datapoint delay)
    TimeStamp_c fresh = Pcr;
    long long now = LocalTime;

    Pcr = mClockRecoveryPreviousPcr;
    LocalTime = mClockRecoveryPreviousLocalTime;

    mClockRecoveryPreviousPcr = fresh;
    mClockRecoveryPreviousLocalTime = now;

    // Initialize the accumulated data if this is our first point
    if (!mClockRecoveryBasePcr.IsValid())
    {
        mClockRecoveryBasePcr            = Pcr;
        // We will not use the interpolated value for clock recovery :
        // the gradient measurement would be less accurate on short integrations
        mClockRecoveryBaseLocalClock                 = LocalTime;
        mClockRecoveryAccumulatedPoints              = 0;
        mClockRecoveryDiscardedPoints                = 0;
        mClockRecoveryDiscardedPointsOnBigJumps      = 0;
        mClockRecoveryConsecutiveUnconfirmedBigJumps = 0;
        mClockRecoveryLeastSquareFit.Reset();
        mClockRecoveryPCRInterpolator.Reset(PCR_FORWARD_JUMP_THRESHOLD);
    }

    // Accumulate the data point (converting the times to deltas first)
    // The interpolated value is not used for clock recovery integration, as the statistical
    // properties of the input data is closer to the actual gradient that the linear
    // interpolation, which is more linear but introduces a slight offset
    int64_t DeltaPcr       = TimeStamp_c::DeltaUsec(Pcr, TimeStamp_c::AddUsec(mClockRecoveryBasePcr, mClockRecoveryLeastSquareFit.Y()));
    int64_t DeltaLocalTime = LocalTime - (mClockRecoveryBaseLocalClock + mClockRecoveryLeastSquareFit.X());

    if (CheckDataPointDeltas(Pcr, LocalTime, DeltaPcr, DeltaLocalTime) != 0)
    {
        return OutputCoordinatorNoError;
    }

    if (CheckShiftFromPCR(Pcr, InterpolatedLocalTime) != 0)
    {
        return OutputCoordinatorNoError;
    }

    mClockRecoveryLeastSquareFit.Add(DeltaPcr, DeltaLocalTime);
    mClockRecoveryAccumulatedPoints++;
    mClockRecoveryDiscardedPoints = 0;

    // The last system time we record is the interpolated value as it might be used for time mapping
    mClockRecoveryLastValidPcr = Pcr;
    mClockRecoveryLastValidLocalTime  = LocalTime;

    if (!MasterTimeMappingEstablished && Player->PolicyValue(Playback, PlayerAllStreams, PolicyLivePlayback) == PolicyValueApply
        && mClockRecoveryLastValidPcr.IsValid())
    {

        int MasterClock = Player->PolicyValue(Playback, PlayerAllStreams, PolicyMasterClock);
        if (MasterClock == PolicyValueSystemClockMaster)
        {
            UseSystemClockAsMaster = true;

            int streamCount = 0;

            OutputCoordinatorStatus_t Status = GenerateTimeMappingWithPCR();
            bool reported = false;
            if (Status != OutputCoordinatorNoError)
            {
                SE_ERROR("Failed to generate a time mapping with PCR\n");
            }
            else
            {
                for (OutputCoordinatorContext_t ContextLoop = CoordinatedContexts;
                     (ContextLoop != NULL);
                     ContextLoop  = ContextLoop->Next)
                {
                    streamCount++;
                    ContextLoop->TimeMappingEstablished = true;
                    if (!reported)
                    {
                        GenerateMappingEstablishedEvent(ContextLoop, MappingSourcePcr);
                        reported = true;
                    }
                }
            }
            if (!reported)
            {
                SE_ERROR("EventTimeMappingEstablished NOT reported (stream count : %d)\n", streamCount);
                MasterTimeMappingEstablished = false;
            }
        }
    }

    ///////////////////////////////////////////////////////////////////////////
    //  Gradient stability computation
    ///////////////////////////////////////////////////////////////////////////

    // We'd like to register the gradient to compute its stability over a sliding window. But the size of this window should depend on the total integration lenght, right ?
    // And we don't really want to have a variable size vector to compute this over, so we are going to be smarter than that and skip some Gradient values so that our vector length is
    // a quarter of the overall integration.
    // So that, when we say the Gradient is "stable", it means it's been stable over a 1/8 of the overall integration period, with a minimum sample count of LAST_GRADIENTS_COUNT,
    // which is the size of the stability integration vector

    int SkipCount = (mClockRecoveryAccumulatedPoints / LAST_GRADIENTS_COUNT) / 8;

    if (ComputeGradient == true)
    {
        if ((SkipCount == 0 || (mClockRecoveryAccumulatedPoints % SkipCount == 0))
            && mClockRecoveryAccumulatedPoints >= CLOCK_RECOVERY_MINIMUM_POINTS)
        {
            ComputeGradient = true;
        }
        else
        {
            ComputeGradient = false;
        }
    }

    if (ComputeGradient)
    {
        Rational_t Gradient                = mClockRecoveryLeastSquareFit.Gradient();
        Rational_t Intercept               = mClockRecoveryLeastSquareFit.Intercept();

        TimeStamp_c Estimation = TimeStamp_c::AddUsec(mClockRecoveryBasePcr, (Intercept.IntegerPart() + (Gradient * Rational_t((long long)LocalTime - mClockRecoveryBaseLocalClock)).LongLongIntegerPart()));
        long long Error = TimeStamp_c::DeltaUsec(Pcr, Estimation);

        SE_EXTRAVERB2(group_se_pipeline, group_avsync
                      , "PCR %lld DeltaPcr %lld ts %lld delta_ts %lld AccumulatedPoints %d "
                      "min %d Integrated %lld gradient %d.%09d (%dppm) current %d ppm  "
                      "intercept %d.%09d Estimation %lld Error %lld smoothing_index %d Skip count : %d\n"
                      , Pcr.NativeValue()
                      , DeltaPcr
                      , LocalTime
                      , DeltaLocalTime
                      , mClockRecoveryAccumulatedPoints
                      , CLOCK_RECOVERY_MINIMUM_POINTS
                      , mClockRecoveryLeastSquareFit.X()
                      , Gradient.IntegerPart(), Gradient.RemainderDecimal(9)
                      , GradientToPartsPerMillion(Gradient)
                      , GradientToPartsPerMillion(mClockRecoveryEstablishedGradient)
                      , Intercept.IntegerPart(), Intercept.RemainderDecimal(9)
                      , Estimation.NativeValue()
                      , Error
                      , mClockRecoveryLastPpmIndex
                      , SkipCount
                     );

        if (CheckGradientStability(Gradient, Intercept) != 0)
        {
            return OutputCoordinatorNoError;
        }
    }

    Playback->GetStatistics().ClockRecoveryAccumulatedPoints = mClockRecoveryAccumulatedPoints;
    Playback->GetStatistics().ClockRecoveryClockAdjustment = GradientToPartsPerMillion(SystemClockAdjustment);
    Playback->GetStatistics().ClockRecoveryEstablishedGradient = GradientToPartsPerMillion(mClockRecoveryEstablishedGradient);
    Playback->GetStatistics().ClockRecoveryIntegrationTimeWindow = 0; // TODO: remove this stat
    Playback->GetStatistics().ClockRecoveryIntegrationTimeElapsed = mClockRecoveryLeastSquareFit.X() / 1000000;

    return OutputCoordinatorNoError;
}

// /////////////////////////////////////////////////////////////////////////
//
//      The function to read out an estimate of the recovered clock
//

OutputCoordinatorStatus_t   OutputCoordinator_Base_c::ClockRecoveryEstimate(unsigned long long *SourceTime,
                                                                            unsigned long long *LocalTime)
{
    // Let us initialize first
    if (SourceTime != NULL)
    {
        *SourceTime = INVALID_TIME;
    }

    if (LocalTime != NULL)
    {
        *LocalTime = INVALID_TIME;
    }

    // Is clock recovery live
    if (!mClockRecoveryInitialized)
    {
        SE_ERROR("Clock recovery not initialized\n");
        return OutputCoordinatorClockRecoveryNotAvail;
    }

    if (!mClockRecoveryBasePcr.IsValid())
    {
        // can happen upon call to get_clock_data_point when no PCR arrived yet
        // not a functional error, and can happen a lot => do not trace as error
        SE_DEBUG(group_avsync, "No basis on which to make an estimate\n");
        return OutputCoordinatorClockRecoveryNotAvail;
    }

    // Calculate the value
    unsigned long long Now = OS_GetTimeInMicroSeconds();
    long long delta = (long long)(Now - mClockRecoveryBaseLocalClock);
    unsigned long long ElapsedSourceTime = LongLongIntegerPart(delta / mClockRecoveryEstablishedGradient);
    TimeStamp_c EstimatedSourceTime  = TimeStamp_c::AddUsec(mClockRecoveryBasePcr, ElapsedSourceTime);

    // Subtract Splicing offset
    EstimatedSourceTime = TimeStamp_c::AddNativeOffset(EstimatedSourceTime, -mSplicingOffset);

    // Write the desired outputs
    if (SourceTime != NULL)
    {
        *SourceTime     = EstimatedSourceTime.Value(mClockRecoverySourceTimeFormat);
    }

    if (LocalTime != NULL)
    {
        *LocalTime = Now;
    }

    if (SourceTime != NULL && LocalTime != NULL)
    {
        SE_DEBUG2(group_avsync, group_se_pipeline, "Clock estimate - (%llu, %llu, %llu)\n", *SourceTime, *LocalTime, *SourceTime - *LocalTime);
    }

    return OutputCoordinatorNoError;
}

// /////////////////////////////////////////////////////////////////////////
//
//      Function to get capture to render latency for AUX synchro
//

OutputCoordinatorStatus_t  OutputCoordinator_Base_c::GetCaptureToRenderLatency(int32_t *value)
{
    // Lock the component to thread-safely
    ComponentMonitor(this);

    if (!MasterTimeMappingEstablished)
    {
        ComponentMonitor.UnLock();
        OS_Status_t WaitStatus = OS_WaitForEventAuto(&SynchronizeMayHaveCompleted, MAX_WAIT_TIME_MAPPING_SLEEP);
        ComponentMonitor.Lock();

        if (WaitStatus != OS_NO_ERROR)
        {
            SE_WARNING("Abandon waiting for time mapping establishment\n");
            *value = 0;
            return OutputCoordinatorMappingNotEstablished;
        }
    }

    // The Time mapping will basically determine:
    // PresentationTime = CaptureTime + Latency
    // PresentationTime => MasterBaseSystemTime
    // CaptureTime => MasterBasePts

    int32_t val =  MasterBaseSystemTime - MasterBasePts.uSecValue();

    // Convert to millisecond
    val = val / 1000;

    if (val > MAX_AUX_LATENCY)
    {
        SE_WARNING("AUXLatency = %d ms exceeds the maximum %d ms\n", val, MAX_AUX_LATENCY);
        val = MAX_AUX_LATENCY;
    }

    SE_DEBUG(group_avsync, "AUX Latency Value = %d\n", val);
    *value = val;
    return OutputCoordinatorNoError;
}

// /////////////////////////////////////////////////////////////////////////
//
//      Function to drain a live playback if we need to re-synchronize
//

void    OutputCoordinator_Base_c::DrainLivePlayback()
{
    //
    // If we are in live playback, it would be wise to drain down all streams before continuing
    //
    if (Player->PolicyValue(Playback, PlayerAllStreams, PolicyLivePlayback) == PolicyValueApply)
    {
        SE_INFO(group_avsync, "Performing DrainLivePlayback()\n");
        OS_SetEvent(&Playback->GetDrainSignalEvent());
        ClockRecoveryRestart(false);
    }
    else
    {
        SE_DEBUG(group_avsync, "Resetting time mapping\n");
        ResetTimeMapping(PlaybackContext);
    }
}

// /////////////////////////////////////////////////////////////////////////
//
//      Time functions, coded here since direction
//      of play may change the meaning later.
//

unsigned long long   OutputCoordinator_Base_c::SpeedScale(unsigned long long T)
{
    if (Direction == PlayBackward)
    {
        T = -T;
    }

    if (Speed == 1)
    {
        return T;
    }

    Rational_t Temp = (Speed == 0) ? 0 : T / Speed;
    return Temp.LongLongIntegerPart();
}

unsigned long long   OutputCoordinator_Base_c::InverseSpeedScale(unsigned long long T)
{
    if (Direction == PlayBackward)
    {
        T = -T;
    }

    if (Speed == 1)
    {
        return T;
    }

    Rational_t Temp = Speed * (Rational_t)T;
    return Temp.LongLongIntegerPart();
}

bool OutputCoordinator_Base_c::StreamIsClockMaster(OutputCoordinatorContext_t Context)
{
    int MasterClock = Player->PolicyValue(Playback, Context->Stream, PolicyMasterClock);
    if (
        (Context->StreamType == StreamTypeVideo && MasterClock != STM_SE_CTRL_VALUE_VIDEO_CLOCK_MASTER) ||
        (Context->StreamType == StreamTypeAudio && MasterClock != STM_SE_CTRL_VALUE_AUDIO_CLOCK_MASTER)
    )
    {
        SE_EXTRAVERB(group_se_pipeline, "Stream 0x%p is not clock master\n", Stream);
        return false;
    }
    return true;
}
