/************************************************************************
Copyright (C) 2003-2014 STMicroelectronics. All Rights Reserved.

This file is part of the Streaming Engine.

Streaming Engine is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License version 2 as
published by the Free Software Foundation.

Streaming Engine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with Streaming Engine; see the file COPYING.  If not, write to the Free
Software Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307,
USA.

The Streaming Engine Library may alternatively be licensed under a
proprietary license from ST.
************************************************************************/

#ifndef H_OUTPUT_COORDINATOR_BASE
#define H_OUTPUT_COORDINATOR_BASE

#include "player.h"
#include "least_squares.h"
#include "pcr_interpolation.h"

#undef TRACE_TAG
#define TRACE_TAG           "OutputCoordinator_Base_c"

// We will average this amount of gradient measurment to ensure it's stable
#define LAST_GRADIENTS_COUNT 128
#define LAST_ORA_GRADIENTS_COUNT 32

typedef struct TimingContext_s
{
    //
    // Output rate information
    //

    unsigned int          IntegrationCount;
    LeastSquares_t        LeastSquareFit;

    bool                  ClockAdjustmentEstablished;
    Rational_t            ClockAdjustment;

    unsigned long long    LastExpectedTime;
    unsigned long long    LastActualTime;

    long long int         VideoFrameDuration;

    Rational_t            mORALastPpm[LAST_GRADIENTS_COUNT];
    Rational_t            mORALastCurrentError[LAST_GRADIENTS_COUNT];
    int                   mORALastPpmIndex;

    Rational_t            CurrentErrorJitterUs;

    TimingContext_s()
        : IntegrationCount(0)
        , LeastSquareFit()
        , ClockAdjustmentEstablished(false)
        , ClockAdjustment()
        , LastExpectedTime(0)
        , LastActualTime(0)
        , VideoFrameDuration(0)
        , mORALastPpm()
        , mORALastCurrentError()
        , mORALastPpmIndex(0)
        , CurrentErrorJitterUs(0)
    {}
} TimingContext_t;

struct OutputCoordinatorContext_s
{
    OutputCoordinatorContext_t  Next;

    PlayerStream_t              Stream;
    PlayerStreamType_t          StreamType;

    ManifestationCoordinator_t  ManifestationCoordinator;
    unsigned long long          ManifestorLatency;
    uint32_t                    ManifestorMaxExternalLatency;
    uint32_t                    TimeOffset;

    bool                        InSynchronizeFn;
    TimeStamp_c                 SynchronizingAtPlaybackTime;
    OS_Event_t                  AbortPerformEntryIntoDecodeWindowWait;

    bool                        TimeMappingEstablished;

    bool                        ClockMaster;
    unsigned int                ClockMasterIndex;             // Which surface is it on

    unsigned int                MaxSurfaceIndex;
    OutputSurfaceDescriptor_t **Surfaces;
    TimingContext_t             TimingState[MAXIMUM_MANIFESTATION_TIMING_COUNT];

    Rational_t                  OutputRateAdjustments[MAXIMUM_MANIFESTATION_TIMING_COUNT];
    Rational_t                  CurrentErrorJitterUs[MAXIMUM_MANIFESTATION_TIMING_COUNT];
    long long int               StreamOffset;
    TimeStamp_c                 mLastValidPTS;
    unsigned long long          mLastValidSystemTime;
    unsigned long long          mLastValidDeltaSystemTime;
    int                         mInvalidTimedFrames;

    TransitionState_e           TransitionState;
    int                         TransitionStateCounter;
    unsigned long long          TransitionSystemTime;
    TimeStamp_c                 TransitionPts;
    unsigned long long          TransitionLastSystemTime;
    TimeStamp_c                 TransitionLastPts;
    unsigned long long          TransitionLastMappingSystemTime;
    TimeStamp_c                 TransitionLastMappingPts;
    int                         InvalidTransitionsFromUnstableStateRequests;
    int                         ErroneousMappingWaitCount;

    OutputCoordinatorContext_s(PlayerStream_t stream, PlayerStreamType_t streamtype)
        : Next(NULL)
        , Stream(stream)
        , StreamType(streamtype)
        , ManifestationCoordinator(NULL)
        , ManifestorLatency(0)
        , ManifestorMaxExternalLatency(0)
        , TimeOffset(0)
        , InSynchronizeFn(false)
        , SynchronizingAtPlaybackTime()
        , AbortPerformEntryIntoDecodeWindowWait()
        , TimeMappingEstablished(false)
        , ClockMaster(false)
        , ClockMasterIndex(0)
        , MaxSurfaceIndex(0)
        , Surfaces(NULL)
        , TimingState()
        , OutputRateAdjustments()
        , CurrentErrorJitterUs()
        , StreamOffset((long long)INVALID_TIME)
        , mLastValidPTS()
        , mLastValidSystemTime(INVALID_TIME)
        , mLastValidDeltaSystemTime(0)
        , mInvalidTimedFrames(0)
        , TransitionState(COORDINATOR_STABLE)
        , TransitionStateCounter(0)
        , TransitionSystemTime(INVALID_TIME)
        , TransitionPts()
        , TransitionLastSystemTime(INVALID_TIME)
        , TransitionLastPts()
        , TransitionLastMappingSystemTime(INVALID_TIME)
        , TransitionLastMappingPts()
        , InvalidTransitionsFromUnstableStateRequests(0)
        , ErroneousMappingWaitCount(0)
    {
        OS_InitializeEvent(&AbortPerformEntryIntoDecodeWindowWait);
    }
    ~OutputCoordinatorContext_s()
    {
        OS_TerminateEvent(&AbortPerformEntryIntoDecodeWindowWait);
    }
private:
    DISALLOW_COPY_AND_ASSIGN(OutputCoordinatorContext_s);
};

class OutputCoordinator_Base_c : public OutputCoordinator_c
{
public:
    OutputCoordinator_Base_c();
    ~OutputCoordinator_Base_c();

    //
    // OutputCoordinator class functions
    //

    void ResetPrivateState();

    OutputCoordinatorStatus_t   RegisterStream(PlayerStream_t                Stream,
                                               PlayerStreamType_t            StreamType,
                                               OutputCoordinatorContext_t   *Context);

    void                        DeRegisterStream(OutputCoordinatorContext_t   Context);

    OutputCoordinatorStatus_t   SetPlaybackSpeed(OutputCoordinatorContext_t   Context,
                                                 Rational_t                   Speed,
                                                 PlayDirection_t              Direction);

    void                        ResetTimeMapping(OutputCoordinatorContext_t   Context);

    void                        EstablishTimeMapping(OutputCoordinatorContext_t  Context,
                                                     const TimeStamp_c          &Pts,
                                                     unsigned long long          SystemTime = INVALID_TIME);

    OutputCoordinatorStatus_t   TranslatePlaybackTimeToSystem(OutputCoordinatorContext_t   Context,
                                                              const TimeStamp_c           &Pts,
                                                              unsigned long long          *SystemTime);

    void                        UpdateExternalLatency(OutputCoordinatorContext_t Context);

    void RebaseTimeMapping(OutputCoordinatorContext_t   Context,
                           const TimeStamp_c            &Pts,
                           unsigned long long           SystemTime);

    OutputCoordinatorStatus_t   TranslateSystemTimeToPlayback(OutputCoordinatorContext_t  Context,
                                                              unsigned long long          SystemTime,
                                                              TimeStamp_c                *Pts);

    void SetSplicingOffset(long long PtsOffset)
    {
        if (PtsOffset == mSplicingOffset) { return; }

        mSplicingOffset = PtsOffset;

        SE_VERBOSE(group_avsync, "PtsOffset %lld\n", PtsOffset);
    }

    long long GetSplicingOffset() { return mSplicingOffset; }

    OutputCoordinatorStatus_t   SynchronizeStreams(OutputCoordinatorContext_t     Context,
                                                   const TimeStamp_c             &Pts,
                                                   const TimeStamp_c             &Dts,
                                                   unsigned long long           *SystemTime,
                                                   void                         *ParsedAudioVideoDataParameters);

    OutputCoordinatorStatus_t   PerformEntryIntoDecodeWindowWait(OutputCoordinatorContext_t  Context,
                                                                 const TimeStamp_c           &Dts,
                                                                 unsigned long long           DecodeWindowPorch);

    OutputCoordinatorStatus_t   HandlePlaybackTimeDeltas(OutputCoordinatorContext_t  Context,
                                                         const TimeStamp_c          &ExpectedPlaybackTime,
                                                         const TimeStamp_c          &ActualPlaybackTime);

    OutputCoordinatorStatus_t   CalculateOutputRateAdjustments(OutputCoordinatorContext_t    Context,
                                                               OutputTiming_t               *OutputTiming,
                                                               Rational_t                  **OutputRateAdjustments,
                                                               Rational_t                  **CurrentErrorJitterUs,
                                                               Rational_t                   *SystemClockAdjustment);

    void                        RestartOutputRateIntegration(OutputCoordinatorContext_t  Context,
                                                             bool                        ResetCount = true,
                                                             unsigned int                TimingIndex = INVALID_INDEX);

    void                        ResetTimingContext(OutputCoordinatorContext_t     Context,
                                                   unsigned int                   TimingIndex = INVALID_INDEX);

    void                        ClockRecoveryInitialize(stm_se_time_format_t  SourceTimeFormat);
    void                        ClockRecoveryIsInitialized(bool              *Initialized);

    void                        ClockRecoveryRestart(bool conservative);

    OutputCoordinatorStatus_t   ClockRecoveryDataPoint(unsigned long long  SourceTime,
                                                       unsigned long long  LocalTime);

    OutputCoordinatorStatus_t   ClockRecoveryEstimate(unsigned long long   *SourceTime,
                                                      unsigned long long   *LocalTime);

    OutputCoordinatorStatus_t   GetCaptureToRenderLatency(int32_t *value);

private:
    OS_Event_t                  SynchronizeMayHaveCompleted;

    unsigned int                StreamCount;

    int                         mRegisteredVideoStreamCount;
    OutputCoordinatorContext_t  CoordinatedContexts;
    unsigned int                StreamsInSynchronize;
    bool                        MasterTimeMappingEstablished;
    PlayerStreamType_t          MasterTimeMappingEstablishedOnType;

    unsigned long long          MasterBaseSystemTime;
    TimeStamp_c                 MasterBasePts;

    long long                   mPcrLastMappingDate;
    int                         mPcrCloseMappings;

    long long                   mInitialDeltaToPcr;
    long long                   mSplicingOffset;

    bool                        UseSystemClockAsMaster;

    bool                        SystemClockAdjustmentEstablished;
    Rational_t                  SystemClockAdjustment;

    Rational_t                  Speed;
    PlayDirection_t             Direction;

    bool                        mClockRecoveryInitialized;
    TimeStamp_c                 mClockRecoveryBasePcr;
    stm_se_time_format_t        mClockRecoverySourceTimeFormat;
    unsigned long long          mClockRecoveryBaseLocalClock;
    unsigned int                mClockRecoveryAccumulatedPoints;
    unsigned int                mClockRecoveryDiscardedPoints;
    unsigned int                mClockRecoveryDiscardedPointsOnBigJumps;
    unsigned int                mClockRecoveryConsecutiveUnconfirmedBigJumps;
    unsigned long long          mClockRecoveryLastSpuriousDelta;
    LeastSquares_t              mClockRecoveryLeastSquareFit;
    Rational_t                  mClockRecoveryEstablishedGradient;
    int                         mClockRecoveryLastPpm[LAST_GRADIENTS_COUNT];
    int                         mClockRecoveryLastPpmIndex;

    PCRInterpolator_t           mClockRecoveryPCRInterpolator;

    TimeStamp_c                 mClockRecoveryLastPcr;
    unsigned long long          mClockRecoveryLastLocalTime;

    TimeStamp_c                 mClockRecoveryNextPcr;
    unsigned long long          mClockRecoveryNextLocalTime;

    TimeStamp_c                 mClockRecoveryLastValidPcr;
    unsigned long long          mClockRecoveryLastValidLocalTime;

    TimeStamp_c                 mClockRecoveryPreviousPcr;
    unsigned long long          mClockRecoveryPreviousLocalTime;
    unsigned long long          mClockRecoveryConfirmedBigJumpDate;

    long long                   LatencyUsedInCaptureModeDuringTimeMapping;

    // Clock recovery datapoint helper methods
    int                         CheckDataPointBurstStatus(TimeStamp_c Pcr,
                                                          unsigned long long LocalTime,
                                                          bool *ComputeGradient);
    int                         CheckDataPointDeltas(TimeStamp_c Pcr,
                                                     int64_t SystemTime,
                                                     int64_t DeltaPcr,
                                                     int64_t DeltaLocalTime);

    void                        EnterTransitionPeriod(OutputCoordinatorContext_t Context, TimeStamp_c Pts, uint64_t SystemTime, TransitionState_e State);

    int                         CheckShiftFromPCR(TimeStamp_c Pcr,
                                                  unsigned long long InterpolatedLocalTime);

    int                         CheckGradientStability(Rational_t Gradient,
                                                       Rational_t Intercept);

    void                         PerformMasterClockAdjustment(void);

    void                        CheckTransitionState(OutputCoordinatorContext_t   Context, TimeStamp_c Pts, TimeStamp_c *BasePts, uint64_t *BaseSystemTime);
    void                        ResetTransitionContext(OutputCoordinatorContext_t Context);

    // Time mapping related methods
    void                        DrainLivePlayback();
    void                        VerifyMasterClock();

    bool                        StreamCanGenerateTimeMapping(OutputCoordinatorContext_t Context, unsigned int WaitCount);
    OutputCoordinatorStatus_t   GenerateTimeMapping(void *ParsedAudioVideoDataParameters);
    OutputCoordinatorStatus_t   GenerateTimeMappingWithPCR();
    void                        GenerateTimeMappingForCapture(OutputCoordinatorContext_t    Context,
                                                              void   *ParsedAudioVideoDataParameters);
    bool                        ReuseExistingTimeMapping(OutputCoordinatorContext_t   Context,
                                                         const TimeStamp_c           &Pts,
                                                         unsigned long long          *SystemTime,
                                                         void                        *ParsedAudioVideoDataParameters);
    OutputCoordinatorStatus_t   CheckTimeMapping(OutputCoordinatorContext_t Context, const TimeStamp_c &Pts, unsigned long long  *SystemTime);
    unsigned long long          SpeedScale(unsigned long long   T);
    unsigned long long          InverseSpeedScale(unsigned long long    T);
    bool                        StreamIsClockMaster(OutputCoordinatorContext_t Context);

    Rational_t CalculateAvsyncErrorDriftCorrector(OutputCoordinatorContext_t   Context,
                                                  TimingContext_t             *State,
                                                  long long int                CurrentError,
                                                  int                          i);

    Rational_t CalculateOutputDrift(OutputCoordinatorContext_t   Context,
                                    TimingContext_t             *State,
                                    int                          i);

    void CalculateOutputRateAdjustment(OutputCoordinatorContext_t   Context,
                                       TimingContext_t             *State,
                                       long long                    CurrentAvsyncError,
                                       int                          i);

    void   ResetStreamTimeMapping(OutputCoordinatorContext_t Context);
    void   ResetMasterTimeMapping();
    bool   NoStreamUsesMasterTimeMapping();
    void   AdjustExternalLatency(uint32_t ExternalLatency);

    long long GetLargestOfMinimumManifestationLatencies(void *ParsedAudioVideoDataParameters);

    OutputCoordinatorStatus_t   GenerateMappingEstablishedEvent(OutputCoordinatorContext_t   Context,
                                                                stm_se_time_mapping_source_t Source);
    DISALLOW_COPY_AND_ASSIGN(OutputCoordinator_Base_c);
};
#endif
