/************************************************************************
Copyright (C) 2015 STMicroelectronics. All Rights Reserved.

This file is part of the Streaming Engine.

Streaming Engine is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License version 2 as
published by the Free Software Foundation.

Streaming Engine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with Streaming Engine; see the file COPYING.  If not, write to the Free
Software Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307,
USA.

The Streaming Engine Library may alternatively be licensed under a
proprietary license from ST.
************************************************************************/

#include "pcr_interpolation.h"

#undef TRACE_TAG
#define TRACE_TAG "PCRInterpolator_c"

// We are going to track cumulted error on the slope (ideally being 1) of the
// PCR value versus system time
#define CUMULATED_ERROR_UNITS_ON_SLOPE 5

PCRInterpolator_c::PCRInterpolator_c()
    : mLastPcr() ,
      mLastRawLocalTime(INVALID_TIME),
      mLastInterpolated(INVALID_TIME),
      mAccumulatedAdditionAvg(0),
      mAccumulatedCounter(0),
      mJumpThreshold(INVALID_TIME),
      mInitialized(false),
      mLastDelta(0),
      mGrowingDeltaCounter(0),
      mLastValidLocalTime(INVALID_TIME)
{}

void PCRInterpolator_c::Reset(long long JumpThreshold)
{
    mLastPcr            = TimeStamp_c();
    mLastRawLocalTime   = INVALID_TIME;
    mLastInterpolated   = INVALID_TIME;
    mAccumulatedAdditionAvg             = 0;
    mAccumulatedCounter                 = 0;
    mJumpThreshold                      = JumpThreshold;
    mInitialized                        = false;
    mLastDelta                          = 0;
    mGrowingDeltaCounter                = 0;
    mLastValidLocalTime                 = INVALID_TIME;
}

// -------------------------------------------------------------------------
//  Interpolates the value of the clock data point
//  Returns 0 on success
// -------------------------------------------------------------------------
void   PCRInterpolator_c::Interpolate(
    TimeStamp_c Pcr,
    long long LocalTime,
    long long *InterpolatedLocalTime
)
{
    // The idea of this function is to overcome bursts of PCR by choosing an interpolated
    //  value instead of a raw system time value in case the time difference between
    //  consecutive system times does not match the PCR delta

    bool Interpolated = false;
    if (mInitialized)
    {
        // Then proceed

        // Both these delta should be equal (slope == 1)
        long long PcrDelta = TimeStamp_c::DeltaUsec(Pcr, mLastPcr);
        long long SinceLast = LocalTime - mLastRawLocalTime;
        long long Delta = abs64(SinceLast - PcrDelta);

        // SinceLast and PcrDelta should be close or get closer to each other at some point
        // otherwise : reset

        // First, let's check the Delta
        if (Delta > mLastDelta)
        {
            // our counter is growing big when Delta is significantly higher than PCR delta
            if (PcrDelta == 0)
            {
                mGrowingDeltaCounter ++;
            }
            else
            {
                mGrowingDeltaCounter += Delta / PcrDelta;
            }
            SE_EXTRAVERB(group_interpolation, "Delta (%lld) mGrowingDeltaCounter=%d\n", Delta, mGrowingDeltaCounter);
            if (mGrowingDeltaCounter > CUMULATED_ERROR_UNITS_ON_SLOPE)
            {
                SE_EXTRAVERB(group_interpolation, "RESET : Growing Delta (%lld)\n", Delta);
                Reset(mJumpThreshold);
            }
        }
        else
        {
            mGrowingDeltaCounter = 0;
        }

        if (mInitialized)   // Could have been reset
        {
            if (mLastInterpolated + PcrDelta > LocalTime)
            {
                SE_EXTRAVERB(group_interpolation, "LocalTime is late\n");
                // New LocalTime is behind interpolated value
                if ((abs64(LocalTime - mLastRawLocalTime)) < PcrDelta
                    // System time diff between datapoints < PCR diff
                    || !(mLastInterpolated - LocalTime > LocalTime - mLastValidLocalTime)
                    // System time diff with last interpolated is lesser than the time since last valid datapoint
                    // This second condition prevents going back in time for subtle jumps
                   )
                {
                    SE_EXTRAVERB(group_interpolation, "not_too_far (LocalTime-mLastInterpolated=%lld    LocalTime-mLastValidLocalTime=%lld\n", LocalTime - mLastInterpolated,
                                 LocalTime - mLastValidLocalTime);
                    // New LocalTime is "not too far" from last one (given the max jump threshold)
                    if (!ValidTime(mJumpThreshold) || PcrDelta <= mJumpThreshold)
                    {
                        *InterpolatedLocalTime   = mLastInterpolated + PcrDelta;
                        Interpolated = true;
                        mLastInterpolated += PcrDelta;
                        mAccumulatedCounter ++;
                        SE_EXTRAVERB(group_interpolation, "Interpolated LocalTime=%lld (added %lld)\n"
                                     , *InterpolatedLocalTime, PcrDelta);
                    }
                    else
                    {
                        SE_EXTRAVERB(group_interpolation, "RESET : PcrDelta above threshold (%lld)\n", mJumpThreshold);
                        Reset(mJumpThreshold);
                    }
                }
                else
                {
                    SE_EXTRAVERB(group_interpolation,
                                 "LocalTime_delta_too_big local=%lld last=%lld (%lld > %lld)  (LocalTime-mLastInterpolated=%lld    LocalTime-mLastValidLocalTime=%lld\n",
                                 LocalTime,
                                 mLastRawLocalTime, abs64((long long)((long long)LocalTime - (long long)mLastRawLocalTime)), PcrDelta >> 2, LocalTime - mLastInterpolated,
                                 LocalTime - mLastValidLocalTime);
                }
            }
            else
            {
                SE_EXTRAVERB(group_interpolation, "Above_threshold\n");
                // New LocalTime is after interpolated value : datapoint is late
            }
        }
        mLastDelta = Delta;
    }
    else
    {
        SE_EXTRAVERB(group_interpolation, "\nNOT_Interpolated LocalTime=%lld\n", LocalTime);
    }

    if (!Interpolated)
    {
        *InterpolatedLocalTime = LocalTime;
        mLastInterpolated   = LocalTime;
        mLastValidLocalTime = LocalTime;
    }

    mInitialized = true;
    mLastRawLocalTime   = LocalTime;
    mLastPcr            = Pcr;
}

