/************************************************************************
Copyright (C) 2015 STMicroelectronics. All Rights Reserved.

This file is part of the Streaming Engine.

Streaming Engine is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License version 2 as
published by the Free Software Foundation.

Streaming Engine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with Streaming Engine; see the file COPYING.  If not, write to the Free
Software Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307,
USA.

The Streaming Engine Library may alternatively be licensed under a
proprietary license from ST.
************************************************************************/

#ifndef H_PCR_INTERPOLATION
#define H_PCR_INTERPOLATION
#include "timestamps.h"
#include "player_types.h"

#undef TRACE_TAG
#define TRACE_TAG "PCRInterpolator_c"

class PCRInterpolator_c
{
public:
    PCRInterpolator_c();
    void Reset(long long JumpThreshold);
    void Interpolate(
        TimeStamp_c Pcr,
        long long LocalTime,
        long long *InterpolatedLocalTime
    );
private:
    TimeStamp_c        mLastPcr;
    long long          mLastRawLocalTime;
    long long          mLastInterpolated;
    int                mAccumulatedAdditionAvg;
    int                mAccumulatedCounter;
    long long          mJumpThreshold;
    bool               mInitialized;
    long long          mLastDelta;
    int                mGrowingDeltaCounter;
    long long          mLastValidLocalTime;
};

typedef PCRInterpolator_c PCRInterpolator_t;

#endif // H_PCR_INTERPOLATION

