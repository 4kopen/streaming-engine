/************************************************************************
Copyright (C) 2003-2014 STMicroelectronics. All Rights Reserved.

This file is part of the Streaming Engine.

Streaming Engine is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License version 2 as
published by the Free Software Foundation.

Streaming Engine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with Streaming Engine; see the file COPYING.  If not, write to the Free
Software Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307,
USA.

The Streaming Engine Library may alternatively be licensed under a
proprietary license from ST.
************************************************************************/

#include "player_generic.h"
#include "player_playback.h"
#include "player_stream.h"
#include "output_timer_base.h"

#undef TRACE_TAG
#define TRACE_TAG "OutputTimer_Base_c"

#define DECODE_TIME_JUMP_THRESHOLD           16000000    // 16 second default (as coordinator PLAYBACK_TIME_JUMP_THRESHOLD)

// Integrate "decode in time" checks over 5 frames
#define DECODE_IN_TIME_INTEGRATION_PERIOD                    5

#define REBASE_MARGIN                       50000       // A 50ms margin when we attempt to recover from decode in time failures by rebasing

#define MAX_SYNCHRONIZATION_DIFFERENCE      60000000    // On first try synchronize up to 10 minutes.
// On following tries synchronize up to a minute,
// otherwise setup a new time mapping
//

#define DROP_TEST_TOO_LATE_THRESHOLD                      10000  // start dropping frames when they are late by more than 10 ms
#define DROP_TEST_TOO_LATE_RESET_MAPPING_THRESHOLD        MAX_STREAM_OFFSET_US // drop frames and reset mapping when they are late by more than MAX_STREAM_OFFSET_US

#define MANIFESTATION_WINDOW_WIDTH   300000000ULL   // Drop frame if it's is supposed to be presented more than 5 minutes in the future

OutputTimer_Base_c::OutputTimer_Base_c()
    : mLock()
    , mConfiguration()
    , mOutputSurfaceDescriptors(NULL)
    , mOutputSurfaceDescriptorsHighestIndex(0)
    , mCodedFrameRate(1)
    , mSpeed(1)
    , mDirection(PlayForward)
    , mLastCountMultiplier(1)
    , mTrickmodeDomain(PolicyValueTrickModeDecodeAll)
    , mExpectedTrickmodeDomain(PolicyValueTrickModeDecodeAll)
    , mNextExpectedPlaybackTime()
    , mLastFrameCollationTime(INVALID_TIME)
    , mManifestationTimingState()
    , mSystemClockAdjustment(1)
    , mOutputRateAdjustments(NULL)
    , mCurrentErrorJitterUs(NULL)
    , mOutputCoordinator(NULL)
    , mOutputCoordinatorContext(NULL)
    , mBufferManager(NULL)
    , mDecodeBufferPool(NULL)
    , mCodedFrameBufferPool()
    , mCodedFrameBufferMaximumSize(0)
    , mLastSystemTime(INVALID_TIME)
    , mLastValidDuration(0)
    , mCandidateDuration(0)
    , mLastCandidateDuration(0)
    , mLastPTS()
    , mLastCodedFrameRate(0)
    , mDecodeTimeJumpThreshold(DECODE_TIME_JUMP_THRESHOLD)
    , mTimeOffset(0)
    , mTimeMappingValidForDecodeTiming(false)
    , mTimeMappingInvalidatedAtDecodeIndex(INVALID_INDEX)
    , mLastSeenDecodeTime()
    , mLastFrameDropPreDecodeDecision(OutputTimerNoError)
    , mKeyFramesSinceDiscontinuity(0)
    , mDecodeInTimeState(DecodingInTime)
    , mDecodeInTimeFailures(0)
    , mDecodeInTimeBehind(0)
    , mLastSynchronizationPolicy(0)
    , mSmoothReverseSupport(true)
    , mSmoothReverseSupportChanged(false)
    , mGlobalSetSynchronizationAtStartup(true)
    , mGlobalSynchronizationState(SyncStateNullState)
    , mPresentationIntervalMutex()
    , mPresentationIntervalStart()
    , mPresentationIntervalEnd()
    , mDiscardPtsMutex()
    , mDiscardPts(UNSPECIFIED_TIME, TIME_FORMAT_US)
    , mDroppedPartialFrame(0)
{
    OS_InitializeMutex(&mLock);
    OS_InitializeMutex(&mPresentationIntervalMutex);
    OS_InitializeMutex(&mDiscardPtsMutex);
}

OutputTimer_Base_c::~OutputTimer_Base_c()
{
    Halt();
    UnregisterOutputCoordinator();

    OS_TerminateMutex(&mLock);
    OS_TerminateMutex(&mPresentationIntervalMutex);
    OS_TerminateMutex(&mDiscardPtsMutex);
}

OutputTimerStatus_t   OutputTimer_Base_c::Halt()
{
    if (TestComponentState(ComponentRunning))
    {
        //
        // Move the base state to halted early, to ensure
        // no activity can be queued once we start halting
        //
        BaseComponentClass_c::Halt();
    }

    return OutputTimerNoError;
}

// /////////////////////////////////////////////////////////////////////////
//
//  Override for component base class set module parameters function
//

OutputTimerStatus_t   OutputTimer_Base_c::SetModuleParameters(
    unsigned int      ParameterBlockSize,
    void             *ParameterBlock)
{
    OutputTimerParameterBlock_t  *OutputTimerParameterBlock = (OutputTimerParameterBlock_t *)ParameterBlock;

    if (ParameterBlockSize != sizeof(OutputTimerParameterBlock_t))
    {
        SE_ERROR("Invalid parameter block\n");
        return OutputTimerError;
    }

    switch (OutputTimerParameterBlock->ParameterType)
    {
    case OutputTimerSetTimeOffset:
        //
        // Lazily update the value, and force a rebase of the time mapping if required
        //
        if (mTimeOffset != OutputTimerParameterBlock->Offset.Value)
        {
            mTimeOffset = OutputTimerParameterBlock->Offset.Value;
            int ExternalMappingPolicy = Player->PolicyValue(Playback, Stream, PolicyExternalTimeMapping);
            if (ExternalMappingPolicy != PolicyValueApply)
            {
                ResetTimeMapping();
            }
        }

        break;

    default:
        SE_ERROR("Unrecognised parameter block (%d)\n", OutputTimerParameterBlock->ParameterType);
        return OutputTimerError;
    }

    return  OutputTimerNoError;
}

// /////////////////////////////////////////////////////////////////////////
//
//  the register a stream function, creates a new timer context
//

OutputTimerStatus_t   OutputTimer_Base_c::RegisterOutputCoordinator(
    OutputCoordinator_t   mOutputCoordinator)
{
    //
    // Obtain the class list for this stream then fill in the
    // configuration record
    //
    mBufferManager = Player->GetBufferManager();
    InitializeConfiguration();
    //
    // Read the initial value of the sync policy
    //
    mLastSynchronizationPolicy = Player->PolicyValue(Playback, Stream, PolicyAVDSynchronization);
    //
    // Attach the stream specific (audio|video|data)
    // output timing information to the decode buffer pool.
    //
    mDecodeBufferPool = Stream->GetDecodeBufferManager()->GetDecodeBufferPool();

    if (mDecodeBufferPool == NULL)
    {
        SE_ERROR("Stream 0x%p - %s - Failed to obtain the decode buffer pool\n", Stream , mConfiguration.OutputTimerName);
        return OutputTimerError;
    }

    PlayerStatus_t Status = mDecodeBufferPool->AttachMetaData(Player->MetaDataOutputTimingType);
    if (Status != BufferNoError)
    {
        SE_ERROR("Stream 0x%p - %s - Failed to attach stream specific timing data to all members of the decode buffer pool\n",
                 Stream, mConfiguration.OutputTimerName);
        return Status;
    }

    //
    // Get the coded frame buffer pool, so that we can monitor levels if
    // frames fail to decode in time.
    //
    mCodedFrameBufferPool = Stream->GetCodedFrameBufferPool(&mCodedFrameBufferMaximumSize);

    //
    // Record the output coordinator
    // and register us with it.
    //
    Status  = mOutputCoordinator->RegisterStream(Stream, mConfiguration.StreamType, &mOutputCoordinatorContext);

    if (Status != PlayerNoError)
    {
        SE_ERROR("Stream 0x%p - %s - Failed to register with output coordinator\n", Stream, mConfiguration.OutputTimerName);
        return Status;
    }

    this->mOutputCoordinator = mOutputCoordinator;
    //
    // Initialize state
    //
    mLastFrameDropPreDecodeDecision  = OutputTimerNoError;
    mKeyFramesSinceDiscontinuity     = 0;
    ResetTimingContext();
    // TODO(pht) check status
    mOutputCoordinator->CalculateOutputRateAdjustments(mOutputCoordinatorContext, NULL,
                                                       &mOutputRateAdjustments, &mCurrentErrorJitterUs, &mSystemClockAdjustment);
    //
    // Go live, and return
    //
    SetComponentState(ComponentRunning);

    return OutputTimerNoError;
}

// /////////////////////////////////////////////////////////////////////////
//
//  unregister from output coordinator
//

void OutputTimer_Base_c::UnregisterOutputCoordinator()
{
    // invoked from Player_Generic_c dtor()
    // after each play stream thread is terminated, we release output coordinator
    // context here

    if (mOutputCoordinator != NULL)
    {
        mOutputCoordinator->DeRegisterStream(mOutputCoordinatorContext);
        mOutputCoordinatorContext = NULL;
        mOutputCoordinator        = NULL;
    }
}

// /////////////////////////////////////////////////////////////////////////
//
//  the reset time mapping function
//

void OutputTimer_Base_c::ResetTimeMapping()
{
    //
    // Check that we are running
    //
    AssertComponentStateVoid(ComponentRunning);

    //
    // Reset the mapping
    //
    mTimeMappingValidForDecodeTiming = false;
    mTimeMappingInvalidatedAtDecodeIndex = INVALID_INDEX;
    mLastSeenDecodeTime          = TimeStamp_c();
    mNextExpectedPlaybackTime    = TimeStamp_c();
    mLastSystemTime = INVALID_TIME;
    mLastValidDuration = 0;
    mCandidateDuration = 0;
    mLastCandidateDuration = 0;

    for (unsigned int i = 0; i <= mOutputSurfaceDescriptorsHighestIndex; i++)
    {
        ManifestationTimingState_t     *State   = &mManifestationTimingState[i];
        if (State == NULL || !State->Initialized) { continue; }
        State->AccumulatedError = 0;
    }

    SE_DEBUG2(group_se_pipeline, group_output_timer, "Stream 0x%p\n", Stream);

    // base reset work
    mOutputCoordinator->ResetTimeMapping(mOutputCoordinatorContext);
}

// /////////////////////////////////////////////////////////////////////////
//
//  the reset time mapping function
//

void OutputTimer_Base_c::ResetTimingContext(unsigned int     ManifestationIndex)
{
    unsigned int LoopStart = (ManifestationIndex == INVALID_INDEX) ? 0 : ManifestationIndex;
    unsigned int LoopEnd = (ManifestationIndex == INVALID_INDEX) ? MAXIMUM_MANIFESTATION_TIMING_COUNT : (ManifestationIndex + 1);

    for (unsigned int i = LoopStart; i < LoopEnd; i++)
    {
        mManifestationTimingState[i].SynchronizationState            = SyncStateInSynchronization;
        mManifestationTimingState[i].SynchronizationFrameCount       = 0;
        mManifestationTimingState[i].SynchronizationAtStartup        = true;
        mManifestationTimingState[i].SynchronizationErrorIntegrationCount = 0;
        mManifestationTimingState[i].SynchronizationAccumulatedError = 0;
        mManifestationTimingState[i].SynchronizationError            = 0;
        mManifestationTimingState[i].FrameWorkthroughCount           = 0;
        mManifestationTimingState[i].AccumulatedError                = 0; // rational
        mManifestationTimingState[i].ExtendSamplesForSynchronization = false;
        mManifestationTimingState[i].SynchronizationCorrectionUnits  = 0;
        mManifestationTimingState[i].SynchronizationOneTenthCorrectionUnits  = 0;
        mManifestationTimingState[i].LoseFieldsForSynchronization    = 0;
        mManifestationTimingState[i].PreviousDisplayFrameRate        = 0; // rational
        mManifestationTimingState[i].CountMultiplier                 = 1; // rational
        mManifestationTimingState[i].FieldDurationTime               = 0;
        mManifestationTimingState[i].Initialized                     = true;
    }

    mOutputCoordinator->ResetTimingContext(mOutputCoordinatorContext, ManifestationIndex);
}

// /////////////////////////////////////////////////////////////////////////
//
//  The function to await entry into the decode window, if the decode
//  time is invalid, or if no time mapping has yet been established,
//  we return immediately.
//

OutputTimerStatus_t   OutputTimer_Base_c::AwaitEntryIntoDecodeWindow(Buffer_t         Buffer)
{
    //
    // Check that we are running
    //
    AssertComponentState(ComponentRunning);

    //
    // Check for policies under which we do not wait for entry into the decode window.
    //

    if ((Player->PolicyValue(Playback, Stream, PolicyAVDSynchronization) == PolicyValueDisapply) ||
        (Player->PolicyValue(Playback, Stream, PolicyLivePlayback) == PolicyValueApply) ||
        (Player->PolicyValue(Playback, Stream, PolicyCaptureProfile) != PolicyValueCaptureProfileDisabled))
    {
        return PlayerNoError;
    }

    //
    // For reverse play we use different values, but now still do implement decode windows
    //
    Rational_t      Speed;
    PlayDirection_t Direction;
    Playback->GetSpeed(&Speed, &Direction);

    // Note : Wait to entry into decode window not when speed is 0 and when speed is 1 for video
    // For audio in 1x, we do not want to decode too early as when entering trickmode, all decoded
    // audio will be discarded and we don't want glitches in speed transitions
    if ((Speed == 0) || (mConfiguration.StreamType == StreamTypeVideo && Speed == 1))
    {
        return PlayerNoError;
    }

    bool TrickModeDiscarding = !inrange(Speed, mConfiguration.MinimumSpeedSupported, mConfiguration.MaximumSpeedSupported);

    //
    // Is time mapping usable (Note for discarding a master may be allowed)
    //

    if (!mTimeMappingValidForDecodeTiming && !TrickModeDiscarding)
    {
        return PlayerNoError;
    }

    //
    // Would it be helpful to make up a DTS
    //

    ParsedFrameParameters_t *ParsedFrameParameters = GetParsedFrameParameters(Buffer);

    if (!ParsedFrameParameters->DTS.IsValid() &&
        !ParsedFrameParameters->ReferenceFrame &&
        ParsedFrameParameters->PTS.IsValid())
    {
        ParsedFrameParameters->DTS = TimeStamp_c::AddUsec(ParsedFrameParameters->PTS, -mConfiguration.FrameDecodeTime);
    }

    //
    // Check for large DTS changes, invalidating the mapping if we find one
    // Note we allow negative jumps here of up to the decode porch time, simply
    // because when we make up a DTS we often create a negative jump of 1 or
    // two frame times.
    //

    if (mLastSeenDecodeTime.IsValid() && ParsedFrameParameters->DTS.IsValid())
    {
        long long DeltaDecodeTime     = (Direction == PlayBackward) ?
                                        TimeStamp_c::DeltaUsec(mLastSeenDecodeTime, ParsedFrameParameters->DTS) :
                                        TimeStamp_c::DeltaUsec(ParsedFrameParameters->DTS, mLastSeenDecodeTime);
        long long LowerLimit      = min(-100, -(long long)mConfiguration.EarlyDecodePorch);

        if (!inrange(DeltaDecodeTime, LowerLimit, mDecodeTimeJumpThreshold))
        {
            mTimeMappingValidForDecodeTiming = false;
            mTimeMappingInvalidatedAtDecodeIndex = ParsedFrameParameters->DecodeFrameIndex;
            SE_WARNING("Stream 0x%p - %s - Jump in decode times detected %12lld\n", Stream, mConfiguration.OutputTimerName, DeltaDecodeTime);
        }
    }

    mLastSeenDecodeTime      = ParsedFrameParameters->DTS;

    //
    // Check if we have a valid time mapping, return immediately
    // if we have no time mapping or the decode time is invalid.
    //

    if ((!mTimeMappingValidForDecodeTiming && !TrickModeDiscarding) || !ParsedFrameParameters->DTS.IsValid())
    {
        return OutputTimerNoError;
    }

    //
    // Perform the wait
    //
    TimeStamp_c DTS = TimeStamp_c::AddUsec(ParsedFrameParameters->DTS, mTimeOffset);

    unsigned long long DecodeWindowPorch;
    if (Direction == PlayForward)
    {
        DecodeWindowPorch   = mConfiguration.FrameDecodeTime + mConfiguration.EarlyDecodePorch;
    }
    else
    {
        DecodeWindowPorch   = mConfiguration.ReverseEarlyDecodePorch;
    }

    OutputCoordinatorStatus_t Status = mOutputCoordinator->PerformEntryIntoDecodeWindowWait(mOutputCoordinatorContext, DTS, DecodeWindowPorch);

    return Status;
}

// /////////////////////////////////////////////////////////////////////////
//
//  The function to test for frame drop before or after decode
//

OutputTimerStatus_t   OutputTimer_Base_c::TestForFrameDrop(Buffer_t       Buffer,
                                                           OutputTimerTestPoint_t    TestPoint)
{
    //
    // Check that we are running
    //
    AssertComponentState(ComponentRunning);

    // test for frame drop before decode is allowed even if we are in sync disable
    //
    if (TestPoint != OutputTimerBeforeDecodeWindow)
    {
        int SynchronizationPolicy = Player->PolicyValue(Playback, Stream, PolicyAVDSynchronization);
        if (SynchronizationPolicy == PolicyValueDisapply)
        {
            return PlayerNoError;
        }
    }

    //
    // Split out to the different test points
    //

    OutputTimerStatus_t Status;
    switch (TestPoint)
    {
    case OutputTimerBeforeDecodeWindow:
        Status  = DropTestBeforeDecodeWindow(Buffer);
        break;

    case OutputTimerBeforeDecode:
        Status  = DropTestBeforeDecode(Buffer);
        break;

    case OutputTimerBeforeOutputTiming:
        Status  = DropTestBeforeOutputTiming(Buffer);
        break;

    case OutputTimerBeforeManifestation:
        Status  = DropTestBeforeManifestation(Buffer);
        break;

    default:
        Status  = OutputTimerNoError;
    }

    if (Status != OutputTimerNoError)
    {
        mNextExpectedPlaybackTime = TimeStamp_c();
        IncrementTestFrameDropStatistics(Status, TestPoint);
    }

    return Status;
}

OutputTimerStatus_t OutputTimer_Base_c::DropTestVideoTrickmodeDomain(ParsedFrameParameters_t  *ParsedFrameParameters,
                                                                     bool beforeDecode)
{
    // Trickmode domain applies to video only
    if (mConfiguration.StreamType == StreamTypeAudio)
    {
        return OutputTimerNoError;
    }

    if (Player->PolicyValue(Playback, Stream, PolicyTrickModeDomain) != PolicyValueTrickModeAuto)
    {
        mExpectedTrickmodeDomain = Player->PolicyValue(Playback, Stream, PolicyTrickModeDomain);
    }

    SE_EXTRAVERB(group_output_timer, "TrickmodeDomain %d ExpectedTrickmodeDomain %d\n", mTrickmodeDomain, mExpectedTrickmodeDomain);

    // switching from Ionly to IP or IPB must be done before decode and on a key frame
    if (beforeDecode && mTrickmodeDomain == PolicyValueTrickModeDecodeKeyFrames
        && (mExpectedTrickmodeDomain == PolicyValueTrickModeDecodeAll
            || mExpectedTrickmodeDomain == PolicyValueTrickModeDiscardNonReferenceFrames))
    {
        if (ParsedFrameParameters->KeyFrame)
        {
            SE_VERBOSE(group_player, "Switching trickmode domain from %d to %d\n", mTrickmodeDomain, mExpectedTrickmodeDomain);
            mTrickmodeDomain = mExpectedTrickmodeDomain;
        }
    }
    else if (mTrickmodeDomain != mExpectedTrickmodeDomain)
    {
        SE_VERBOSE(group_player, "Switching trickmode domain from %d to %d\n", mTrickmodeDomain, mExpectedTrickmodeDomain);
        mTrickmodeDomain = mExpectedTrickmodeDomain;
    }

    // If only independent frames should be decoded, then drop all non-independent frames
    // Note that the policy name states "Decode key frames", but in H264 and HEVC,
    // we can decode also non-key independant frames as no reference will be needed when this policy is set
    if (!ParsedFrameParameters->IndependentFrame && mTrickmodeDomain == PolicyValueTrickModeDecodeKeyFrames)
    {
        return OutputTimerDropFrameKeyFramesOnly;
    }

    // If only reference frames should be decoded, then drop all non-reference frames
    if (!ParsedFrameParameters->ReferenceFrame && mTrickmodeDomain == PolicyValueTrickModeDiscardNonReferenceFrames)
    {
        return OutputTimerDropFrameReferenceFramesOnly;
    }

    return OutputTimerNoError;
}

OutputTimerStatus_t OutputTimer_Base_c::DropTestAudioTrickmodeDecodedSpeedMismatch(ParsedFrameParameters_t  *ParsedFrameParameters)
{
    Rational_t      Speed;
    PlayDirection_t Direction;
    Playback->GetSpeed(&Speed, &Direction);

    // For the audio trick mode tempo is applied at the selected speed before decoding. If the speed change after the decoding drop the buffer.
    if ((mConfiguration.StreamType == StreamTypeAudio) && (Speed != ParsedFrameParameters->Speed))
    {
        SE_VERBOSE(group_output_timer, "Stream 0x%p Decoded Speed %d.%06d Current Speed [%d.%06d] dropping decoded buffer as speed not ok for manifestation\n",
                   Stream, ParsedFrameParameters->Speed.IntegerPart(), ParsedFrameParameters->Speed.RemainderDecimal(), Speed.IntegerPart(), Speed.RemainderDecimal());
        return OutputTimerTrickModeDropFrame;
    }
    return OutputTimerNoError;
}

OutputTimerStatus_t OutputTimer_Base_c::DropTestSingleGopPolicy(ParsedFrameParameters_t  *ParsedFrameParameters)
{
    if (ParsedFrameParameters->FirstParsedParametersAfterInputJump)
    {
        mKeyFramesSinceDiscontinuity = 0;
    }

    if (ParsedFrameParameters->KeyFrame)
    {
        mKeyFramesSinceDiscontinuity++;
    }

    int SingleGroupPlayback = Player->PolicyValue(Playback, Stream, PolicyStreamSingleGroupBetweenDiscontinuities);

    if ((SingleGroupPlayback == PolicyValueApply) && (mKeyFramesSinceDiscontinuity >= 2))
    {
        return OutputTimerDropFrameSingleGroupPlayback;
    }

    return OutputTimerNoError;
}

OutputTimerStatus_t OutputTimer_Base_c::DropTestPresentationInterval(const ParsedFrameParameters_t  *ParsedFrameParameters, bool beforeDecode)
{
    if (beforeDecode && ParsedFrameParameters->ReferenceFrame) { return OutputTimerNoError; }

    OS_LockMutex(&mPresentationIntervalMutex);
    TimeStamp_c IntervalStart = mPresentationIntervalStart;
    TimeStamp_c IntervalEnd   = mPresentationIntervalEnd;
    OS_UnLockMutex(&mPresentationIntervalMutex);

    Rational_t               Speed;
    PlayDirection_t          Direction;
    Playback->GetSpeed(&Speed, &Direction);

    if ((Direction == PlayForward && !IsInInterval(ParsedFrameParameters->PTS, IntervalStart, IntervalEnd))
        || (Direction == PlayBackward && !IsInInterval(ParsedFrameParameters->PTS, IntervalEnd, IntervalStart)))
    {
        SE_VERBOSE(group_output_timer, "Stream 0x%p dropping frame outside interval (PTS %lld Direction %d, startI %lld endI %lld)\n",
                   Stream, ParsedFrameParameters->PTS.NativeValue(), Direction, IntervalStart.NativeValue(), IntervalEnd.NativeValue());
        return OutputTimerDropFrameOutsidePresentationInterval;
    }

    SE_EXTRAVERB(group_output_timer, "Stream 0x%p NOT dropping frame outside interval (PTS %lld Direction %d, startI %lld endI %lld)\n",
                 Stream, ParsedFrameParameters->PTS.NativeValue(), Direction, IntervalStart.NativeValue(), IntervalEnd.NativeValue());

    return OutputTimerNoError;
}

OutputTimerStatus_t OutputTimer_Base_c::SetPresentationInterval(
    const TimeStamp_c &IntervalStart, const TimeStamp_c &IntervalEnd)
{
    OS_LockMutex(&mPresentationIntervalMutex);
    mPresentationIntervalStart  = IntervalStart;
    mPresentationIntervalEnd    = IntervalEnd;
    OS_UnLockMutex(&mPresentationIntervalMutex);

    return OutputTimerNoError;
}

OutputTimerStatus_t OutputTimer_Base_c::DropTestDiscardPts(const ParsedFrameParameters_t  *ParsedFrameParameters)
{
    OS_LockMutex(&mDiscardPtsMutex);
    TimeStamp_c DiscardPts = mDiscardPts;
    OS_UnLockMutex(&mDiscardPtsMutex);

    if (mConfiguration.StreamType != StreamTypeAudio || mSpeed != 0 || DiscardPts.IsUnspecified()) { return OutputTimerNoError; }

    if (!DiscardPts.IsValid())
    {
        SE_VERBOSE(group_output_timer, "Stream 0x%p DiscardPTS is invalid => dropping\n", Stream);
        return OutputTimerDropFrameDiscardPts;
    }

    if (!ParsedFrameParameters->PTS.IsValid())
    {
        SE_VERBOSE(group_output_timer, "Stream 0x%p Invalid PTS => dropped\n", Stream);
        return OutputTimerDropFrameDiscardPts;
    }

    if (!inrange(TimeStamp_c::DeltaUsec(DiscardPts, ParsedFrameParameters->PTS), -MAX_STREAM_OFFSET_US, MAX_STREAM_OFFSET_US))
    {
        SE_VERBOSE(group_output_timer, "Stream 0x%p incoherent PTSes (%lld vs %lld) => dropped\n",
                   Stream, DiscardPts.NativeValue(), ParsedFrameParameters->PTS.NativeValue());
        return OutputTimerDropFrameDiscardPts;
    }

    if (ParsedFrameParameters->PTS < DiscardPts)
    {
        SE_VERBOSE(group_output_timer, "Stream 0x%p PTS (%lld) < DiscardPTS (%lld) => dropped\n",
                   Stream, ParsedFrameParameters->PTS.NativeValue(), DiscardPts.NativeValue());
        return OutputTimerDropFrameDiscardPts;
    }
    return OutputTimerNoError;
}

OutputTimerStatus_t OutputTimer_Base_c::DropTestDiscardPartialFrame(
    Buffer_t Buffer, const ParsedFrameParameters_t  *ParsedFrameParameters)
{
    if (mConfiguration.StreamType == StreamTypeAudio)
    {
        return OutputTimerNoError;
    }

    // if time mapping is generated on Partial frames and then subsequent frames
    // are dropped by decoder because of unavailability of reference frame, on display it looks
    // like freeze till we get next decoded frame. To avoid such situation we will not
    // generate time mapping on any partial frames and such partial frames will be discarded.

    if (ParsedFrameParameters->PTS.IsValid())
    {
        unsigned long long SystemTime;
        OutputTimerStatus_t Status = mOutputCoordinator->TranslatePlaybackTimeToSystem(mOutputCoordinatorContext, ParsedFrameParameters->PTS, &SystemTime);
        if (Status != OutputCoordinatorMappingNotEstablished)
        {
            // Time mapping is already established so dont't discard it
            return OutputTimerNoError;
        }
        else
        {
            Rational_t          Speed;
            PlayDirection_t     Direction;

            Playback->GetSpeed(&Speed, &Direction);

            if (Speed != 0 && mTrickmodeDomain != PolicyValueTrickModeDecodeKeyFrames)
            {
                int SynchronizationPolicy = Player->PolicyValue(Playback, Stream, PolicyAVDSynchronization);
                ParsedVideoParameters_s *ParsedVideoPatameters = (ParsedVideoParameters_s *)GetParsedAudioVideoDataParameters(Buffer);

                if ((SynchronizationPolicy == PolicyValueApply) &&
                    (ParsedVideoPatameters->PictureStructure != StructureFrame))
                {
                    // We might want to discard I frames when they are partial to build our time mapping,
                    // but if they are all partial, then after having discarded 2 (T, then B), and if
                    // next one is partial too, let's use it anyway
                    if (mDroppedPartialFrame < 2)
                    {
                        SE_VERBOSE(group_se_pipeline, "Stream 0x%p Discarding partial frame dropped:%d, structure=%d\n",
                                   Stream,
                                   mDroppedPartialFrame,
                                   ParsedVideoPatameters->PictureStructure);

                        // Discard all the partial frames till time mapping not established.
                        mDroppedPartialFrame ++;
                        return OutputTimerDropFramePartialFrame;
                    }
                    else
                    {
                        SE_VERBOSE(group_se_pipeline, "Stream 0x%p don't discard partial frame, structure=%d\n",
                                   Stream,
                                   ParsedVideoPatameters->PictureStructure);

                        // Discarding more than 2 consecutive partial frames means that we should stop with this strategy
                        // maybe the stream only has partial frames
                        mDroppedPartialFrame = 0;
                        return OutputTimerNoError;
                    }
                }
            }
        }
    }
    return OutputTimerNoError;
}

OutputTimerStatus_t OutputTimer_Base_c::SetDiscardPts(
    const TimeStamp_c &DiscardPts)
{
    OS_LockMutex(&mDiscardPtsMutex);
    mDiscardPts    = DiscardPts;
    OS_UnLockMutex(&mDiscardPtsMutex);

    SE_VERBOSE(group_output_timer, "DiscardPts=%lld\n", DiscardPts.NativeValue());

    return OutputTimerNoError;
}

// /////////////////////////////////////////////////////////////////////////
//
//  The function to test for frame drop before decode
//

OutputTimerStatus_t   OutputTimer_Base_c::DropTestBeforeDecodeWindow(Buffer_t Buffer)
{
    ParsedFrameParameters_t  *ParsedFrameParameters = GetParsedFrameParameters(Buffer);

    // If this is not the first decode of a frame, then we repeat our previous decision
    // (i.e we cannot drop just slice, or a single field).
    if (!ParsedFrameParameters->FirstParsedParametersForOutputFrame)
    {
        return mLastFrameDropPreDecodeDecision;
    }

    OutputTimerStatus_t Status = DropTestSingleGopPolicy(ParsedFrameParameters);
    if (Status != OutputTimerNoError) { mLastFrameDropPreDecodeDecision = Status; return Status; }

    Status = DropTestVideoTrickmodeDomain(ParsedFrameParameters, true);
    if (Status != OutputTimerNoError) { mLastFrameDropPreDecodeDecision = Status; return Status; }

    Status = DropTestPresentationInterval(ParsedFrameParameters, true);
    if (Status != OutputTimerNoError) { mLastFrameDropPreDecodeDecision = Status; return Status; }

    Status = DropTestDiscardPts(ParsedFrameParameters);
    if (Status != OutputTimerNoError) { mLastFrameDropPreDecodeDecision = Status; return Status; }

    return OutputTimerNoError;
}

OutputTimerStatus_t   OutputTimer_Base_c::DropTestSupportedSpeed()
{
    Rational_t      Speed;
    PlayDirection_t Direction;
    Playback->GetSpeed(&Speed, &Direction);

    if (((Direction == PlayBackward) && !mConfiguration.ReversePlaySupported) ||
        ((Speed != 0) && !inrange(Speed, mConfiguration.MinimumSpeedSupported, mConfiguration.MaximumSpeedSupported)))
    {
        return OutputTimerTrickModeNotSupportedDropFrame;
    }

    return OutputTimerNoError;
}

OutputTimerStatus_t OutputTimer_Base_c::SetSpeed(Rational_t Speed, PlayDirection_t Direction)
{
    mSpeed      = Speed;
    mDirection  = Direction;

    if (Player->PolicyValue(Playback, Stream, PolicyTrickModeDomain) == PolicyValueTrickModeAuto)
    {
        mExpectedTrickmodeDomain = PolicyValueTrickModeDecodeAll;
    }

    // Reset discard Pts as we are starting a new segment
    SetDiscardPts(TimeStamp_c(UNSPECIFIED_TIME, TIME_FORMAT_US));

    if (Player->PolicyValue(Playback, PlayerAllStreams, PolicyLivePlayback) == PolicyValueDisapply)
    {
        ResetTimeMapping();
    }

    return OutputTimerNoError;
}

// /////////////////////////////////////////////////////////////////////////
//
//  The function to test for frame drop before decode
//

OutputTimerStatus_t   OutputTimer_Base_c::DropTestBeforeDecode(Buffer_t       Buffer)
{
    OutputTimerStatus_t Status = DropTestSupportedSpeed();
    if (Status != OutputTimerNoError) { mLastFrameDropPreDecodeDecision = Status; return Status; }

    ParsedFrameParameters_t  *ParsedFrameParameters = GetParsedFrameParameters(Buffer);

    // If this is not the first decode of a frame, then we repeat our previous decision
    // (i.e we cannot drop just slice, or a single field).
    if (!ParsedFrameParameters->FirstParsedParametersForOutputFrame)
    {
        return mLastFrameDropPreDecodeDecision;
    }

    // We are not going to drop the frame, if we are transitioning from an unsupported trick mode
    // where we would drop all frames, then we haven't been around for a while, and need to
    // reset our sync monitors
    if (mLastFrameDropPreDecodeDecision == OutputTimerTrickModeNotSupportedDropFrame)
    {
        mGlobalSynchronizationState  = SyncStateStartAwaitingCorrectionWorkthrough;
        mDecodeInTimeState           = DecodingInTime;
    }

    // If we have reached this point, then we have not discarded,
    // and we update the last decision variable.
    mLastFrameDropPreDecodeDecision  = Status;

    return Status;
}

// /////////////////////////////////////////////////////////////////////////
//
//  The function to test for frame drop before decode
//

OutputTimerStatus_t   OutputTimer_Base_c::DropTestBeforeOutputTiming(Buffer_t         Buffer)
{
    ParsedFrameParameters_t *ParsedFrameParameters = GetParsedFrameParametersReference(Buffer);

    OutputTimerStatus_t Status = DropTestSupportedSpeed();
    if (Status != OutputTimerNoError) { return Status; }

    Status = DropTestVideoTrickmodeDomain(ParsedFrameParameters, false);
    if (Status != OutputTimerNoError) { return Status; }

    Status = DropTestAudioTrickmodeDecodedSpeedMismatch(ParsedFrameParameters);
    if (Status != OutputTimerNoError) { return Status; }

    Status = DropTestPresentationInterval(ParsedFrameParameters, false);
    if (Status != OutputTimerNoError) { return Status; }

    Status = DropTestDiscardPts(ParsedFrameParameters);
    if (Status != OutputTimerNoError) { return Status; }

    Status = DropTestDiscardPartialFrame(Buffer, ParsedFrameParameters);
    if (Status != OutputTimerNoError) { return Status; }

    return OutputTimerNoError;
}

OutputTimerStatus_t   OutputTimer_Base_c::DropTestReallyTooEarlyForManifestation(OutputTiming_t *OutputTiming)
{
    Rational_t      Speed;
    PlayDirection_t Direction;
    Playback->GetSpeed(&Speed, &Direction);

    unsigned long long NowMaxManifestation = OS_GetTimeInMicroSeconds() + MANIFESTATION_WINDOW_WIDTH;

    // Drop the frame if it is (really) too far in the future
    if ((Speed != 0) && (NowMaxManifestation < OutputTiming->BaseSystemPlaybackTime))
    {
        SE_WARNING("Stream 0x%p - %s - Frame is to be presented too far in the future (window=%llu SystemPlaybackTime=%llu)\n"
                   , Stream
                   , mConfiguration.OutputTimerName
                   , NowMaxManifestation
                   , OutputTiming->BaseSystemPlaybackTime);
        return OutputTimerDropFrameTooEarlyForManifestation;
    }

    return OutputTimerNoError;
}

OutputTimerStatus_t   OutputTimer_Base_c::DropTestTooLateForManifestation(
    Buffer_t Buffer, ParsedFrameParameters_t *ParsedFrameParameters, OutputTiming_t *OutputTiming)
{
    return DecodeInTimeMonitoring(Buffer, ParsedFrameParameters, OutputTiming);
}

OutputTimerStatus_t OutputTimer_Base_c::DropTestUntimedFrame(ParsedFrameParameters_t *ParsedFrameParameters,
                                                             OutputTiming_t *OutputTiming)
{
    Rational_t      Speed;
    PlayDirection_t Direction;
    Playback->GetSpeed(&Speed, &Direction);

    int TrickModePolicy = Player->PolicyValue(Playback, Stream, PolicyTrickModeDomain);

    // Check on valid time is not meaningful in case of I only decode mode.
    // In case of I only decode mode, we can have only I frames injected separated by discontinuity.
    // In case the I frame doesn't have an associated PTS, ParsedFrameParameters->PTS will be
    // invalid because the discontinuity prevents computing it from former PTS.

    if ((!ParsedFrameParameters->PTS.IsValid() &&
         (TrickModePolicy != PolicyValueTrickModeDecodeKeyFrames)) ||
        ((Speed != 0) && (NotValidTime(OutputTiming->BaseSystemPlaybackTime))))
    {
        return OutputTimerUntimedFrame;
    }

    return OutputTimerNoError;
}

// /////////////////////////////////////////////////////////////////////////
//
//  The function to test for frame drop before decode
//

OutputTimerStatus_t   OutputTimer_Base_c::DropTestBeforeManifestation(Buffer_t Buffer)
{
    ParsedFrameParameters_t *ParsedFrameParameters  = GetParsedFrameParametersReference(Buffer);
    OutputTiming_t          *OutputTiming           = GetOutputTiming(Buffer);

    OutputTimerStatus_t Status = DropTestUntimedFrame(ParsedFrameParameters, OutputTiming);
    if (Status != OutputTimerNoError) { return Status; }

    Status = DropTestReallyTooEarlyForManifestation(OutputTiming);
    if (Status != OutputTimerNoError) { return Status; }

    Status = DropTestTooLateForManifestation(Buffer, ParsedFrameParameters, OutputTiming);
    if (Status != OutputTimerNoError) { return Status; }

    return OutputTimerNoError;
}

// /////////////////////////////////////////////////////////////////////////
//
//  The function to actually generate an output timing record
//

OutputTimerStatus_t   OutputTimer_Base_c::GenerateFrameTiming(
    Buffer_t          Buffer)
{
    OutputTimerStatus_t Status;

    AssertComponentState(ComponentRunning);

    OutputTiming_t *OutputTiming = GetOutputTiming(Buffer);

    memset(OutputTiming, 0, sizeof(OutputTiming_t));

    for (int i = 0; i < MAXIMUM_MANIFESTATION_TIMING_COUNT; i++)
    {
        OutputTiming->ManifestationTimings[i].OutputRateAdjustment  = 0; // rational
        OutputTiming->ManifestationTimings[i].SystemClockAdjustment = 1; // rational
    }

    //
    // Read synchronization policy and check for it being turned on
    //
    int SynchronizationPolicy = Player->PolicyValue(Playback, Stream, PolicyAVDSynchronization);

    if ((mLastSynchronizationPolicy == PolicyValueDisapply) &&
        (SynchronizationPolicy == PolicyValueApply))
    {
        SE_DEBUG(group_avsync, "Resetting time mapping due to enabling or disabling avsync\n");
        ResetTimeMapping();
    }

    mLastSynchronizationPolicy = SynchronizationPolicy;
    //
    // Check for large PTS changes, delta the mapping if we find one.
    // but only if this is not a re-timing exercise
    //
    ParsedFrameParameters_t *ParsedFrameParameters = GetParsedFrameParametersReference(Buffer);

    TimeStamp_c PTS = TimeStamp_c::AddUsec(ParsedFrameParameters->PTS, mTimeOffset);

    if (ParsedFrameParameters->PTS.IsValid())
    {
        if (NotValidTime(mLastFrameCollationTime))
        {
            mLastFrameCollationTime = ParsedFrameParameters->CollationTime;
        }

        if (mNextExpectedPlaybackTime.IsValid())
        {
            if (mTrickmodeDomain != PolicyValueTrickModeDecodeKeyFrames)
            {
                Status = mOutputCoordinator->HandlePlaybackTimeDeltas(mOutputCoordinatorContext, mNextExpectedPlaybackTime, PTS);
                if (Status != OutputCoordinatorNoError)
                {
                    if (Status == OutputCoordinatorLargeDelta)
                    {
                        // We are going to need interpolation
                        mLastPTS = TimeStamp_c();
                    }
                    else if (Status == OutputCoordinatorLargeDeltaMappingReset)
                    {
                        mLastPTS = TimeStamp_c();
                        mLastValidDuration = 0;
                        mLastCandidateDuration = 0;
                        mCandidateDuration = 0;
                        mLastSystemTime = INVALID_TIME;
                    }
                    else
                    {
                        SE_ERROR("Stream 0x%p - %s - Unable to handle playback time deltas\n", Stream, mConfiguration.OutputTimerName);
                        return Status;
                    }
                }
            }
            else
            {
                SE_VERBOSE(group_output_timer, "Stream 0x%p Stubbing jump detection in I-only mode for frame with PTS=%lld\n"
                           , Stream
                           , ParsedFrameParameters->PTS.NativeValue());
            }
        }

        mNextExpectedPlaybackTime = PTS;

        if (ParsedFrameParameters->SpecifiedPlaybackTime)
        {
            mLastFrameCollationTime = ParsedFrameParameters->CollationTime;
        }
    }

    mOutputCoordinator->SetSplicingOffset(ParsedFrameParameters->PtsOffset);

    //
    // Now we are applying synchronization, and we have a valid time to play with ...
    //
    Rational_t          Speed;
    PlayDirection_t     Direction;

    Playback->GetSpeed(&Speed, &Direction);

    unsigned long long SystemTime;
    if ((Speed != 0) &&
        (SynchronizationPolicy == PolicyValueApply) &&
        ParsedFrameParameters->PTS.IsValid())
    {
        // Update External Latency
        mOutputCoordinator->UpdateExternalLatency(mOutputCoordinatorContext);

        //
        // Translate the playback time to a system time
        //
        Status = mOutputCoordinator->TranslatePlaybackTimeToSystem(mOutputCoordinatorContext, PTS, &SystemTime);
        if (Status == OutputCoordinatorMappingNotEstablished)
        {
            TimeStamp_c DTS = ParsedFrameParameters->DTS.IsValid() ?
                              TimeStamp_c::AddUsec(ParsedFrameParameters->DTS, mTimeOffset) :
                              TimeStamp_c::AddUsec(PTS, -mConfiguration.FrameDecodeTime);

            Status = mOutputCoordinator->SynchronizeStreams(
                         mOutputCoordinatorContext, PTS, DTS, &SystemTime, GetParsedAudioVideoDataParameters(Buffer));

            mTimeMappingValidForDecodeTiming    = true;
            mGlobalSynchronizationState         = SyncStateInSynchronization;
            mGlobalSetSynchronizationAtStartup  = true;
            mDecodeInTimeState                  = DecodingInTime;
        }

        if (Status != OutputCoordinatorNoError || !ValidTime(SystemTime))
        {
            SE_VERBOSE2(group_se_pipeline, group_output_timer, "Stream 0x%p - %s - Failed to obtain system time for playback\n"
                        , Stream, mConfiguration.OutputTimerName);
            if (ValidTime(mLastSystemTime) && mLastValidDuration != 0)
            {
                // Let's interpolate a system time
                SystemTime = mLastSystemTime + mLastValidDuration;
                SE_VERBOSE(group_se_pipeline, "Stream 0x%p - %s - Interpolated SystemTime=%lld mLastSystemTime=%lld mLastValidDuration=%lld\n"
                           , Stream, mConfiguration.OutputTimerName, SystemTime, mLastSystemTime, mLastValidDuration);
                // TODO [SH] Do not interpolate forever !

            }
            else
            {
                return Status;
            }
        }
        else
        {
            // We might want to update the know the duration of a frame so that it can be used in interpolation of system time
            // when it's not available (during PTS jumps or wraparound)
            if (ValidTime(mLastSystemTime) && mLastPTS.IsValid())
            {
                mCandidateDuration = TimeStamp_c::DeltaUsec(PTS, mLastPTS);
                SE_VERBOSE(group_se_pipeline, "Stream 0x%p - %s - Status %d  mCandidateDuration=%lld with mPts=%lld mLastPTS=%lld\n"
                           , Stream, mConfiguration.OutputTimerName, Status, mCandidateDuration, PTS.NativeValue(), mLastPTS.NativeValue());

                // Is that a stable duration (stable meaning : same as last time, should be good enough to handle possible stream errors)
                if (mLastCandidateDuration != 0 && mCandidateDuration == mLastCandidateDuration)
                {
                    mLastValidDuration = mCandidateDuration;

                    SE_VERBOSE(group_se_pipeline, "Stream 0x%p - %s - Status %d Updating mLastValidDuration=%lld with mLastSystemTime=%lld mLastPTS=%lld\n"
                               , Stream, mConfiguration.OutputTimerName, Status, mLastValidDuration, mLastSystemTime, mLastPTS.NativeValue());
                }
                mLastCandidateDuration = mCandidateDuration;
            }
        }

        mLastSystemTime = SystemTime;
        mLastPTS = PTS;
        SE_VERBOSE(group_se_pipeline, "Stream 0x%p - %s - Status %d Updating mLastSystemTime=%lld mLastPTS=%lld\n"
                   , Stream, mConfiguration.OutputTimerName, Status, mLastSystemTime, mLastPTS.NativeValue());

        mOutputCoordinator->RebaseTimeMapping(mOutputCoordinatorContext, PTS, SystemTime);

        // Do we wish to re-validate the time mapping for the decode window timing
        if (!mTimeMappingValidForDecodeTiming &&
            (!ValidIndex(mTimeMappingInvalidatedAtDecodeIndex) ||
             (ParsedFrameParameters->DecodeFrameIndex > mTimeMappingInvalidatedAtDecodeIndex)))
        {
            mTimeMappingValidForDecodeTiming = true;
        }
    }
    else
    {
        SystemTime = UNSPECIFIED_TIME;
    }

    // Generate the timing record
    Status  = FillOutFrameTimingRecord(SystemTime, GetParsedAudioVideoDataParameters(Buffer), OutputTiming);
    if (Status != OutputTimerNoError)
    {
        SE_ERROR("Stream 0x%p - %s - Failed to fill out timing record\n", Stream, mConfiguration.OutputTimerName);
        return Status;
    }

    return OutputTimerNoError;
}

// /////////////////////////////////////////////////////////////////////////
//
//  The function to record the actual frame timing
//

OutputTimerStatus_t   OutputTimer_Base_c::RecordActualFrameTiming(
    Buffer_t          Buffer)
{
    AssertComponentState(ComponentRunning);

    //
    // Load a new set of output descriptors.
    //
    PlayerStatus_t Status = Stream->GetManifestationCoordinator()->GetSurfaceParameters(&mOutputSurfaceDescriptors, &mOutputSurfaceDescriptorsHighestIndex);
    if (Status != ManifestationCoordinatorNoError)
    {
        SE_ERROR("Stream 0x%p - %s - Failed to obtain the output surface descriptors from the Manifestation Coordinator\n", Stream, mConfiguration.OutputTimerName);
        return OutputCoordinatorError;
    }

    //
    // Calculate the clock adjustment rates to compensate for clock drift
    // NOTE if we have a sudden change in the system clock rate, we need to
    //      hold off on avsync while the output rate adjustments have a time
    //      to complete an integration.
    //

    OutputTiming_t *OutputTiming = GetOutputTiming(Buffer);

    Status = mOutputCoordinator->CalculateOutputRateAdjustments(mOutputCoordinatorContext,
                                                                OutputTiming,
                                                                &mOutputRateAdjustments,
                                                                &mCurrentErrorJitterUs,
                                                                &mSystemClockAdjustment);
    if (Status != PlayerNoError)
    {
        SE_ERROR("Stream 0x%p - %s: Failed to calculate output rate adjustments\n", Stream, mConfiguration.OutputTimerName);
    }

    //
    // Perform the individual AVD synchronizations
    //

    for (unsigned int i = 0; i <= OutputTiming->HighestTimingIndex; i++)
    {
        unsigned long long ExpectedTime = OutputTiming->ManifestationTimings[i].SystemPlaybackTime;
        unsigned long long ActualTime   = OutputTiming->ManifestationTimings[i].ActualSystemPlaybackTime;
        OutputSurfaceDescriptor_t *PercussiveSurface = mOutputSurfaceDescriptors[i];

        //
        // Copy any global sync state setting
        //

        if (mGlobalSynchronizationState != SyncStateNullState)
        {
            mManifestationTimingState[i].SynchronizationState  = mGlobalSynchronizationState;
        }

        if (mGlobalSetSynchronizationAtStartup)
        {
            mManifestationTimingState[i].SynchronizationAtStartup = true;
        }

        //
        // Was the frame manifested on this manifestation
        //

        if (NotValidTime(ExpectedTime) || NotValidTime(ActualTime) || PercussiveSurface == NULL)
        {
            continue;
        }

        //
        // Perform Audio/Video/Data synchronization
        //

        if (PercussiveSurface->PercussiveCapable && Player->PolicyValue(Playback, Stream, PolicyCaptureProfile) == PolicyValueCaptureProfileDisabled)
        {
            if (PercussiveSurface->IsSlavedSurface)
            {
                // if the surface is a slaved surface, the percussive adjustment must be be impacted on the master surface
                int MasterSurfaceTimingIndex = i - (mOutputSurfaceDescriptors[i] - PercussiveSurface->MasterSurface);
                Status = PerformAVDSync(MasterSurfaceTimingIndex, ExpectedTime, ActualTime, OutputTiming);
            }
            else
            {
                Status = PerformAVDSync(i, ExpectedTime, ActualTime, OutputTiming);
            }

            if (Status != PlayerNoError)
            {
                SE_ERROR("Stream 0x%p - %s - Failed to perform synchronization\n", Stream, mConfiguration.OutputTimerName);
                continue;
            }
        }
    }

    //
    // Clear the global synchronization state, and return
    //
    mGlobalSynchronizationState      = SyncStateNullState;
    mGlobalSetSynchronizationAtStartup   = false;

    return OutputTimerNoError;
}

const char *OutputTimer_c::StringifyOutputTimerStatus(OutputTimerStatus_t Status)
{
    switch (Status)
    {
    case OutputTimerDropFrame :
        return "OutputTimerDropFrame";
    case OutputTimerDropFrameSingleGroupPlayback:
        return "OutputTimerDropFrameSingleGroupPlayback";
    case OutputTimerDropFrameKeyFramesOnly:
        return "OutputTimerDropFrameKeyFramesOnly";
    case OutputTimerDropFrameReferenceFramesOnly:
        return "OutputTimerDropFrameReferenceFramesOnly";
    case OutputTimerDropFrameOutsidePresentationInterval:
        return "OutputTimerDropFrameOutsidePresentationInterval";
    case OutputTimerDropFrameDiscardPts:
        return "OutputTimerDropFrameDiscardPts";
    case OutputTimerDropFramePartialFrame:
        return "OutputTimerDropFramePartialFrame";
    case OutputTimerUntimedFrame:
        return "OutputTimerUntimedFrame";

    case OutputTimerTrickModeNotSupportedDropFrame:
        return "OutputTimerTrickModeNotSupportedDropFrame";
    case OutputTimerTrickModeDropFrame:
        return "OutputTimerTrickModeDropFrame";
    case OutputTimerLateDropFrame:
        return "OutputTimerLateDropFrame";

    case OutputTimerDropFrameTooLateForManifestation:
        return "OutputTimerDropFrameTooLateForManifestation";
    case OutputTimerDropFrameTooEarlyForManifestation:
        return "OutputTimerDropFrameTooEarlyForManifestation";
    default:
        return "Unknown error";
    };
}

// /////////////////////////////////////////////////////////////////////////
//
//  The function responsible for managing AVD synchronization
//

OutputTimerStatus_t   OutputTimer_Base_c::PerformAVDSync(
    unsigned int          TimingIndex,
    unsigned long long    ExpectedFrameOutputTime,
    unsigned long long    ActualFrameOutputTime,
    OutputTiming_t       *OutputTiming)
{
    SE_ASSERT(!mOutputSurfaceDescriptors[TimingIndex]->IsSlavedSurface);
    ManifestationTimingState_t *State = &mManifestationTimingState[TimingIndex];
    //
    // Calculate the current error threshold, first using a base threshold,
    // and then adding any legitimate offset relating to this specific manifestation
    //
    State->SynchronizationFrameCount++;
    //
    // Determine whether to use the default error threshold (for when we can freely deploy percussive
    // adjustments) or the quantized error threshold (when we have some limit)
    //
    int MasterClockPolicy = Player->PolicyValue(Playback, Stream, PolicyMasterClock);
    int ExternalMappingPolicy = Player->PolicyValue(Playback, Stream, PolicyExternalTimeMapping);

    long long ErrorThreshhold;
    if ((MasterClockPolicy == PolicyValueAudioClockMaster && mConfiguration.StreamType == StreamTypeAudio) ||
        (ExternalMappingPolicy == PolicyValueApply && mConfiguration.StreamType == StreamTypeVideo))
    {
        ErrorThreshhold = mConfiguration.SynchronizationErrorThreshold;
    }
    else
    {
        ErrorThreshhold = mConfiguration.SynchronizationErrorThresholdForQuantizedSync;
    }

    //
    // Widen the error threshhold when we are not targeting a quantized threshhold. There should
    // be no need to widen a quantized threshhold because the quantum can be assumed to be large.
    //
    // Note this branch is distinct from the section above because for streams without any
    // quanitization effect SynchronizationErrorThreshold == SynchronizationErrorThresholdForQuantizedSync .
    //

    if ((unsigned long long) ErrorThreshhold == mConfiguration.SynchronizationErrorThreshold)
    {
        //
        // When running a stream, we have a loose sync threshold that
        // gradually tightens over 3..5 mins this seems worse than it is,
        // most of the error during this period is due to the error in the
        // clock adjustments being worked out.
        //
        ErrorThreshhold = mConfiguration.SynchronizationErrorThreshold;

        if (State->SynchronizationFrameCount < 2048)
        {
            ErrorThreshhold = max(ErrorThreshhold, (4 * State->SynchronizationFrameCount));
        }
        else if (State->SynchronizationFrameCount < 4096)
        {
            ErrorThreshhold = 4 * ErrorThreshhold;
        }
        else if (State->SynchronizationFrameCount < 6144)
        {
            ErrorThreshhold = 2 * ErrorThreshhold;
        }
        else if (State->SynchronizationFrameCount < 8192)
        {
            ErrorThreshhold = (3 * ErrorThreshhold) / 2;
        }
    }

    long long SynchronizationDifference = (long long)ActualFrameOutputTime - (long long)ExpectedFrameOutputTime;
    bool DifferenceExceedsThreshhold = !inrange(SynchronizationDifference, -ErrorThreshhold, ErrorThreshhold);

    SE_VERBOSE(group_avsync,
               "Stream 0x%p - %d - SynchronizationDifference=%lld Actual=%lld Expected=%lld ErrorThreshold=%lld SynchronizationState=%d SynchronizationErrorIntegrationCount=%d FrameWorkthroughCount=%d\n",
               Stream, mConfiguration.StreamType, SynchronizationDifference, ActualFrameOutputTime, ExpectedFrameOutputTime,
               ErrorThreshhold, State->SynchronizationState, State->SynchronizationErrorIntegrationCount, State->FrameWorkthroughCount);

    //
    // Now a switch statement depending on the state we are in
    //
    OutputTimerStatus_t Status;

    switch (State->SynchronizationState)
    {
    //
    // We believe we are in sync, can transition to suspected error.
    // if we remain in sync, then we reset the startup flag, as any
    // future errors cannot be at startup.
    //
    case SyncStateNullState:
    case SyncStateInSynchronization:
        if (DifferenceExceedsThreshhold)
        {
            State->SynchronizationState            = SyncStateSuspectedSyncError;
            State->SynchronizationAccumulatedError = SynchronizationDifference;
            State->SynchronizationPreviousError    = SynchronizationDifference;
            State->SynchronizationErrorStabilityCounter = 0;
            State->SynchronizationErrorIntegrationCount = 1;
        }

        break;

    //
    // We have a suspected sync error, we integrate over a number of values
    // to eliminate glitch effects. If we confirm the error then we fall through
    // to the next state.
    //

    case SyncStateSuspectedSyncError:
        if (DifferenceExceedsThreshhold && samesign(SynchronizationDifference, State->SynchronizationAccumulatedError))
        {
            State->SynchronizationAccumulatedError      += SynchronizationDifference;
            if (abs64(SynchronizationDifference - State->SynchronizationPreviousError) < abs64(SynchronizationDifference) >> 4)
            {
                // We are observing a stable error
                State->SynchronizationErrorStabilityCounter ++;
            }
            else
            {
                State->SynchronizationErrorStabilityCounter = 0;
            }

            State->SynchronizationPreviousError    = SynchronizationDifference;
            State->SynchronizationErrorIntegrationCount++;

            // Extend threshold  by 10 for first attempt to synchronize, just to be sure, it kicks in only when absolutely necessary
            // So if threshold is exceeded, it is considered as a new stream and time mapping will be re-established, if the threshold is not
            // exceeded, it is considered as the same stream with some disruption, therefore no drain or reset of time mapping.
            if (!inrange(SynchronizationDifference, -(10 * MAX_SYNCHRONIZATION_DIFFERENCE), 10 * MAX_SYNCHRONIZATION_DIFFERENCE))
            {
                SE_WARNING("Stream 0x%p too far to sync %lldus\n", Stream, SynchronizationDifference);
                mOutputCoordinator->DrainLivePlayback();
            }

            if (State->SynchronizationErrorIntegrationCount < mConfiguration.SynchronizationIntegrationCount)
            {
                break;
            }
        }
        else
        {
            State->SynchronizationState         = SyncStateInSynchronization;
            State->SynchronizationAtStartup     = false;
            break;
        }

        // Fallthrough
        State->SynchronizationState             = SyncStateConfirmedSyncError;

    //
    // Confirmed sync error, entered by a fallthrough,
    // We request the stream specific function to correct
    // it, then we fallthrough to start awaiting the
    // correction to work through.
    //

    case SyncStateConfirmedSyncError:
        // Should adjustment be performed on average or based on last value ?
        // if error is not stable over integration period, using average makes no sense
        if (State->SynchronizationErrorStabilityCounter >= State->SynchronizationErrorIntegrationCount
            && State->SynchronizationErrorIntegrationCount != 0)
        {
            // Averaging when error is stable will absorb the jitter
            State->SynchronizationError = State->SynchronizationAccumulatedError / State->SynchronizationErrorIntegrationCount;
        }
        else
        {
            // This is the best we can do : use latest known error
            State->SynchronizationError = State->SynchronizationPreviousError;
        }

        SE_INFO2(group_avsync, group_se_pipeline, "Stream 0x%p AVDsync %s %s - %6lld us (err %s :  %d<%d) ErrThreshold = %lld\n", Stream,
                 ((ExternalMappingPolicy == PolicyValueApply) ? "Err" : "Initiating percussive adjustment"),
                 StringifyStreamType(mConfiguration.StreamType)
                 , State->SynchronizationError
                 , (State->SynchronizationErrorStabilityCounter >= State->SynchronizationErrorIntegrationCount ? "stable" : "unstable")
                 , State->SynchronizationErrorStabilityCounter
                 , State->SynchronizationErrorIntegrationCount
                 , ErrorThreshhold);

        Status = CorrectSynchronizationError(TimingIndex);
        if (Status != OutputTimerNoError) { SE_ERROR("Stream 0x%p Failed to correct error\n", Stream); }

        //
        // Synchronization corrections can often mess up the output rate integration,
        // so we restart it here, but only if this was a correctable thing.
        //

        if (ExternalMappingPolicy == PolicyValueDisapply)
        {
            mOutputCoordinator->RestartOutputRateIntegration(mOutputCoordinatorContext, false, TimingIndex);

            for (unsigned int i = 0; i <= OutputTiming->HighestTimingIndex; i++)
            {
                OutputSurfaceDescriptor_t *SlavedSurface = mOutputSurfaceDescriptors[i];

                if (SlavedSurface == NULL) { continue; }

                // also restart output rate integration of any slaved surface
                if (SlavedSurface->IsSlavedSurface && SlavedSurface->MasterSurface == mOutputSurfaceDescriptors[TimingIndex])
                {
                    mOutputCoordinator->RestartOutputRateIntegration(mOutputCoordinatorContext, false, i);
                }
            }
        }

        // Fallthrough
        State->SynchronizationState         = SyncStateStartAwaitingCorrectionWorkthrough;

    //
    // Here we set up to wait for as correction to work through
    //

    case SyncStateStartAwaitingCorrectionWorkthrough:
        State->SynchronizationState         = SyncStateAwaitingCorrectionWorkthrough;

        break;

    //
    // Here we handle waiting for a correction to workthrough
    //

    case SyncStateAwaitingCorrectionWorkthrough:
        State->FrameWorkthroughCount++;

        if (State->FrameWorkthroughCount >= State->FrameWorkthroughFinishCount)
        {
            State->FrameWorkthroughCount      = 0;
            State->SynchronizationState       = SyncStateInSynchronization;
            State->SynchronizationAtStartup   = false;

            if (!inrange(SynchronizationDifference, -MAX_SYNCHRONIZATION_DIFFERENCE, MAX_SYNCHRONIZATION_DIFFERENCE))
            {
                SE_WARNING("Stream 0x%p Failed to sync %lldus\n", Stream, SynchronizationDifference);
                mOutputCoordinator->DrainLivePlayback();
            }

            //
            // In sync event signalling
            //

            if (!DifferenceExceedsThreshhold)
            {
                PlayerEventRecord_t         DisplayEvent;
                DisplayEvent.Code           = EventStreamInSync;
                DisplayEvent.Playback       = Playback;
                DisplayEvent.Stream         = Stream;
                PlayerStatus_t StatusEvt = Stream->SignalEvent(&DisplayEvent);
                if (StatusEvt != PlayerNoError)
                {
                    SE_ERROR("Stream 0x%p Failed to signal event\n", Stream);
                }
            }
        }

        break;
    }

    return OutputTimerNoError;
}

void OutputTimer_Base_c::UpdateTrickmodeDomain()
{
    PlayerEventRecord_t Event;
    Event.Code          = EventTrickModeDomainChange;
    Event.Playback      = Playback;
    Event.Stream        = Stream;
    Event.PlaybackTime  = TIME_NOT_APPLICABLE;
    Event.UserData      = EventUserData;

    if (Player->PolicyValue(Playback, Stream, PolicyTrickModeDomain) == PolicyValueTrickModeAuto)
    {
        switch (mTrickmodeDomain)
        {
        case PolicyValueTrickModeDecodeAll:
            mExpectedTrickmodeDomain    = PolicyValueTrickModeDiscardNonReferenceFrames;
            Event.Value[0].UnsignedInt  = mExpectedTrickmodeDomain;
            break;

        case PolicyValueTrickModeDiscardNonReferenceFrames:
            mExpectedTrickmodeDomain    = PolicyValueTrickModeDecodeKeyFrames;
            Event.Value[0].UnsignedInt  = mExpectedTrickmodeDomain;
            break;

        default:
            Event.Value[0].UnsignedInt  = PolicyValueTrickModeMaxSpeedReached;
            break;
        }
    }
    else
    {
        Event.Value[0].UnsignedInt  = PolicyValueTrickModeMaxSpeedReached;
    }

    Stream->SignalEvent(&Event);
}

bool OutputTimer_Base_c::ResetTimeMappingOnDecodeInTimeFailure(bool DataDeliveredLate)
{
    int MasterClockPolicy = Player->PolicyValue(Playback, Stream, PolicyMasterClock);

    if (Player->PolicyValue(Playback, PlayerAllStreams, PolicyLivePlayback) == PolicyValueApply)
    {
        // In live we want to reset only if the data was delivered late
        return DataDeliveredLate;
    }
    else if ((MasterClockPolicy == PolicyValueVideoClockMaster && mConfiguration.StreamType == StreamTypeAudio) ||
             (MasterClockPolicy == PolicyValueAudioClockMaster && mConfiguration.StreamType == StreamTypeVideo))
    {
        SE_DEBUG2(group_output_timer, group_se_pipeline, "Time mapping rebase not allowed for this stream type (%d vs master %d)\n"
                  , mConfiguration.StreamType, MasterClockPolicy);
        return false;
    }
    else if (Player->PolicyValue(Playback, Stream, PolicyCaptureProfile) != PolicyValueCaptureProfileDisabled)
    {
        // we should never rebase for capture use cases
        return false;
    }

    return true;
}

void   OutputTimer_Base_c::HandleConfirmedDecodeInTimeFailure(long long FailedBy, bool CollatedAfterDisplayTime)
{
    if (mConfiguration.StreamType == StreamTypeVideo && mSpeed > 1)
    {
        UpdateTrickmodeDomain();
        return;
    }

    PlayerEventIdentifier_t EventCode;

    if (CollatedAfterDisplayTime)
    {
        SE_WARNING("Stream 0x%p - %s - Failed to deliver data to decoder in time\n", Stream, mConfiguration.OutputTimerName);
        EventCode = EventFailedToDeliverDataInTime;
    }
    else
    {
        SE_ERROR("Stream 0x%p - %s - Failed to decode frames in time\n", Stream, mConfiguration.OutputTimerName);
        EventCode = EventFailedToDecodeInTime;
    }

    // raise an event
    PlayerEventRecord_t Event;
    Event.Code      = EventCode;
    Event.Playback  = Playback;
    Event.Stream    = Stream;
    Event.PlaybackTime  = TIME_NOT_APPLICABLE;
    Event.UserData  = EventUserData;

    PlayerStatus_t Status = Stream->SignalEvent(&Event);
    if (Status != PlayerNoError)
    {
        SE_ERROR("Stream 0x%p - %s - Failed to signal event\n", Stream, mConfiguration.OutputTimerName);
    }

    if (ResetTimeMappingOnDecodeInTimeFailure(CollatedAfterDisplayTime))
    {
        ResetTimeMapping();
    }
}

OutputTimerStatus_t OutputTimer_Base_c::DecodeInTimeMonitoring(
    Buffer_t Buffer,
    ParsedFrameParameters_t *ParsedFrameParameters,
    OutputTiming_t *OutputTiming)
{
    if (NotValidTime(OutputTiming->BaseSystemPlaybackTime))
    {
        return OutputTimerNoError;
    }

    unsigned long long MaxManifestationLatency = Stream->GetManifestationCoordinator()->GetMaxManifestationLatency(GetParsedAudioVideoDataParameters(Buffer));
    long long EarliestManifestationTime = OS_GetTimeInMicroSeconds() + MaxManifestationLatency;
    long long Delta = OutputTiming->BaseSystemPlaybackTime - EarliestManifestationTime;

    SE_VERBOSE(group_se_pipeline, "Stream 0x%p CollationTime=%lld EarliestManifestationTime=%lld SystemPlaybackTime=%llu delta=%lld MaxManifestationLatency=%llu State %d\n",
               Stream, ParsedFrameParameters->CollationTime, EarliestManifestationTime, OutputTiming->BaseSystemPlaybackTime, Delta, MaxManifestationLatency, mDecodeInTimeState);

    switch (mDecodeInTimeState)
    {
    case DecodingInTime:
        if (Delta < 0)
        {
            SE_VERBOSE(group_se_pipeline, "Switching to AccumulatingDecodeInTimeFailures state\n");
            mDecodeInTimeFailures   = 1;
            mDecodeInTimeBehind     = Delta;
            mDecodeInTimeState      = AccumulatingDecodeInTimeFailures;
        }
        break;

    case AccumulatingDecodeInTimeFailures:
        if (Delta >= 0)
        {
            SE_VERBOSE(group_se_pipeline, "Switching to DecodingInTime state\n");
            mDecodeInTimeState = DecodingInTime;
            break;
        }
        else if (Delta > mDecodeInTimeBehind)
        {
            mDecodeInTimeBehind = Delta;
            break;
        }
        else
        {
            mDecodeInTimeBehind = Delta;
            mDecodeInTimeFailures++;
            if (mDecodeInTimeFailures < DECODE_IN_TIME_INTEGRATION_PERIOD)
            {
                break;
            }

            SE_VERBOSE(group_se_pipeline, "Switching to AccumulatedDecodeInTimeFailures state\n");
            mDecodeInTimeState = AccumulatedDecodeInTimeFailures;
        }

    // fallthrough
    case AccumulatedDecodeInTimeFailures:
        HandleConfirmedDecodeInTimeFailure(-Delta, OutputTiming->BaseSystemPlaybackTime <
                                           (ParsedFrameParameters->CollationTime + MaxManifestationLatency));
        mDecodeInTimeState   = DecodingInTime;
        break;
    }

    // we dont want to drop frames during video trickmodes as it makes the trickmode less smooth
    if (mConfiguration.StreamType == StreamTypeVideo && mSpeed > 1)
    {
        return OutputTimerNoError;
    }

    if (Delta < -DROP_TEST_TOO_LATE_THRESHOLD)
    {
        if (Delta < -DROP_TEST_TOO_LATE_RESET_MAPPING_THRESHOLD)
        {
            SE_DEBUG(group_se_pipeline, "Stream 0x%p Dropping Frame and resetting mapping (EarliestManifestationTime=%lld SystemPlaybackTime=%llu delta=%lld MaxManifestationLatency=%llu)\n",

                     Stream, EarliestManifestationTime, OutputTiming->BaseSystemPlaybackTime,
                     OutputTiming->BaseSystemPlaybackTime - EarliestManifestationTime, MaxManifestationLatency);
            ResetTimeMapping();
            return OutputTimerDropFrameTooLateForManifestation;
        }
        else
        {
            SE_DEBUG(group_se_pipeline, "Stream 0x%p Dropping Frame (EarliestManifestationTime=%lld SystemPlaybackTime=%llu delta=%lld MaxManifestationLatency=%llu)\n",

                     Stream, EarliestManifestationTime, OutputTiming->BaseSystemPlaybackTime,
                     OutputTiming->BaseSystemPlaybackTime - EarliestManifestationTime, MaxManifestationLatency);

            return OutputTimerDropFrameTooLateForManifestation;
        }
    }

    return OutputTimerNoError;
}

// /////////////////////////////////////////////////////////////////////////
//
//  Time functions
//
//

bool   OutputTimer_Base_c::IsInInterval(
    TimeStamp_c Value,
    TimeStamp_c IntervalStart,
    TimeStamp_c IntervalEnd)
{
    bool InInterval = true;

    if (!Value.IsValid())
    {
        return true;
    }

    if (IntervalStart.IsValid())
    {
        InInterval = (Value.NativeValue() >= IntervalStart.NativeValue());
    }

    if (IntervalEnd.IsValid())
    {
        InInterval = InInterval && (Value.NativeValue() < IntervalEnd.NativeValue());
    }

    return InInterval;
}

// /////////////////////////////////////////////////////////////////////////
//
//      Increment statistic counters for frame dropping on different
//      output timer edges
//

void OutputTimer_Base_c::IncrementTestFrameDropStatistics(OutputTimerStatus_t  Status, OutputTimerTestPoint_t  TestPoint)
{
    SE_DEBUG(group_output_timer, "Status=%d\n", Status);

    switch (TestPoint)
    {
    case OutputTimerBeforeDecodeWindow:
        switch (Status)
        {
        case OutputTimerDropFrameSingleGroupPlayback :
            Stream->Statistics().DroppedBeforeDecodeWindowSingleGroupPlayback++;
            break;

        case OutputTimerDropFrameKeyFramesOnly :
            Stream->Statistics().DroppedBeforeDecodeWindowKeyFramesOnly++;
            break;

        case OutputTimerDropFrameOutsidePresentationInterval:
            Stream->Statistics().DroppedBeforeDecodeWindowOutsidePresentationInterval++;
            break;

        case OutputTimerTrickModeNotSupportedDropFrame:
            Stream->Statistics().DroppedBeforeDecodeWindowTrickModeNotSupported++;
            break;

        case OutputTimerTrickModeDropFrame:
            Stream->Statistics().DroppedBeforeDecodeWindowTrickMode++;
            break;

        default:
            Stream->Statistics().DroppedBeforeDecodeWindowOthers++;
            break;
        }

        Stream->Statistics().DroppedBeforeDecodeWindowTotal++;
        break;

    case OutputTimerBeforeDecode:
        switch (Status)
        {
        case OutputTimerDropFrameSingleGroupPlayback :
            Stream->Statistics().DroppedBeforeDecodeSingleGroupPlayback++;
            break;

        case OutputTimerDropFrameKeyFramesOnly :
            Stream->Statistics().DroppedBeforeDecodeKeyFramesOnly++;
            break;

        case OutputTimerDropFrameOutsidePresentationInterval:
            Stream->Statistics().DroppedBeforeDecodeOutsidePresentationInterval++;
            break;

        case OutputTimerTrickModeNotSupportedDropFrame:
            Stream->Statistics().DroppedBeforeDecodeTrickModeNotSupported++;
            break;

        case OutputTimerTrickModeDropFrame:
            Stream->Statistics().DroppedBeforeDecodeTrickMode++;
            break;

        default:
            Stream->Statistics().DroppedBeforeDecodeOthers++;
            break;
        }

        Stream->Statistics().DroppedBeforeDecodeTotal++;
        break;

    case OutputTimerBeforeOutputTiming:
        switch (Status)
        {
        case OutputTimerDropFrameOutsidePresentationInterval:
            Stream->Statistics().DroppedBeforeOutputTimingOutsidePresentationInterval++;
            break;

        default:
            Stream->Statistics().DroppedBeforeOutputTimingOthers++;
            break;
        }

        Stream->Statistics().DroppedBeforeOutputTimingTotal++;
        break;

    case OutputTimerBeforeManifestation:
        switch (Status)
        {
        case OutputTimerUntimedFrame:
            Stream->Statistics().DroppedBeforeManifestationUntimed++;
            break;

        case OutputTimerDropFrameTooLateForManifestation:
            Stream->Statistics().DroppedBeforeManifestationTooLateForManifestation++;
            break;

        case OutputTimerTrickModeNotSupportedDropFrame:
            Stream->Statistics().DroppedBeforeManifestationTrickModeNotSupported++;
            break;

        default:
            Stream->Statistics().DroppedBeforeManifestationOthers++;
            break;
        }

        Stream->Statistics().DroppedBeforeManifestationTotal++;
        break;
    }
}

// /////////////////////////////////////////////////////////////////////////
//
//      Function to know about the parser ability to support smooth reverse
//
//      This function is updating variables for output timer
//      for it to adapt its dropping policy to the backward decode mode.
//

OutputTimerStatus_t  OutputTimer_Base_c::SetSmoothReverseSupport(bool SmoothReverseSupportFromParser)
{
    if (SmoothReverseSupportFromParser != mSmoothReverseSupport)
    {
        mSmoothReverseSupport = SmoothReverseSupportFromParser;
        mSmoothReverseSupportChanged = true;
    }

    return OutputTimerNoError;
}
