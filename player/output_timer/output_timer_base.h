/************************************************************************
Copyright (C) 2003-2014 STMicroelectronics. All Rights Reserved.

This file is part of the Streaming Engine.

Streaming Engine is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License version 2 as
published by the Free Software Foundation.

Streaming Engine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with Streaming Engine; see the file COPYING.  If not, write to the Free
Software Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307,
USA.

The Streaming Engine Library may alternatively be licensed under a
proprietary license from ST.
************************************************************************/

#ifndef H_OUTPUT_TIMER_BASE
#define H_OUTPUT_TIMER_BASE

#include "player.h"
#include "player_stream.h"
#include "shared_ptr.h"

#undef TRACE_TAG
#define TRACE_TAG "OutputTimer_Base_c"

//
// Enumeration of the possible states of the synchronizer
//

typedef enum
{
    SyncStateNullState              = 0,
    SyncStateInSynchronization      = 1,
    SyncStateSuspectedSyncError,
    SyncStateConfirmedSyncError,
    SyncStateStartAwaitingCorrectionWorkthrough,
    SyncStateAwaitingCorrectionWorkthrough,
} SynchronizationState_t;

//
// Enumeration of the decoding in time states
//

typedef enum
{
    DecodingInTime              = 1,
    AccumulatingDecodeInTimeFailures,
    AccumulatedDecodeInTimeFailures,
} DecodeInTimeState_t;

//
// mConfiguration structure for the base class
//

typedef struct OutputTimerConfiguration_s
{
    const char           *OutputTimerName;

    PlayerStreamType_t    StreamType;

    BufferType_t          AudioVideoDataParsedParametersType;
    unsigned int          SizeOfAudioVideoDataParsedParameters;

    //
    // Values controlling the decode window for a frame,
    // the first of which can be refined over a period as
    // frames are timed.
    //

    unsigned long long    FrameDecodeTime;
    unsigned long long    EarlyDecodePorch;

    unsigned int          MaximumDecodeTimesToWait; // This will be adjusted by the speed factor when used.

    unsigned int          ReverseEarlyDecodePorch;  // reverse is a whole new kettle of fish for decode porches,
    // This is generally hardcoded to less than the maximum manifestation wait.

    //
    // Values controlling the avsync mechanisms
    //

    unsigned long long    SynchronizationErrorThreshold;
    unsigned long long    SynchronizationErrorThresholdForQuantizedSync;
    unsigned int          SynchronizationIntegrationCount;

    //
    // trick mode controls
    //

    bool                  ReversePlaySupported;
    Rational_t            MinimumSpeedSupported;
    Rational_t            MaximumSpeedSupported;

    OutputTimerConfiguration_s()
        : OutputTimerName(NULL)
        , StreamType(StreamTypeNone)
        , AudioVideoDataParsedParametersType(0)
        , SizeOfAudioVideoDataParsedParameters(0)
        , FrameDecodeTime(0)
        , EarlyDecodePorch(0)
        , MaximumDecodeTimesToWait(0)
        , ReverseEarlyDecodePorch(0)
        , SynchronizationErrorThreshold(0)
        , SynchronizationErrorThresholdForQuantizedSync(0)
        , SynchronizationIntegrationCount(0)
        , ReversePlaySupported(false)
        , MinimumSpeedSupported()
        , MaximumSpeedSupported()
    {}
} OutputTimerConfiguration_t;

//
// Parameter block settings
//

typedef enum
{
    OutputTimerSetTimeOffset      = BASE_OUTPUT_TIMER
} OutputTimerParameterBlockType_t;

typedef struct OutputTimerSetTimeOffset_s
{
    long long         Value;
} OutputTimerSetTimeOffset_t;

typedef struct OutputTimerParameterBlock_s
{
    OutputTimerParameterBlockType_t     ParameterType;

    union
    {
        OutputTimerSetTimeOffset_t    Offset; ///< Offset to apply to the stream (in microseconds)
    };
} OutputTimerParameterBlock_t;

//
// Individual timing state - for a specific manifestation
//

typedef struct ManifestationTimingState_s
{
    bool                    Initialized;

    SynchronizationState_t  SynchronizationState;             // Values used in synchronization
    bool                    SynchronizationAtStartup;
    unsigned int            SynchronizationFrameCount;
    unsigned int            SynchronizationErrorIntegrationCount;
    long long               SynchronizationAccumulatedError;
    long long               SynchronizationError;
    long long               SynchronizationPreviousError;
    unsigned int            SynchronizationErrorStabilityCounter;
    unsigned int            FrameWorkthroughCount;
    unsigned int            FrameWorkthroughFinishCount;

    Rational_t              AccumulatedError;             // Generic

    unsigned int            ExtendSamplesForSynchronization;      // Audio specific
    unsigned int            SynchronizationCorrectionUnits;
    unsigned int            SynchronizationOneTenthCorrectionUnits;

    unsigned int            LoseFieldsForSynchronization;

    Rational_t              PreviousDisplayFrameRate;
    bool                    PreviousSurfaceProgressive;
    Rational_t              CountMultiplier;
    unsigned long long      FieldDurationTime;
    bool                    RespectPolarity;

    ManifestationTimingState_s()
        : Initialized(false)
        , SynchronizationState()
        , SynchronizationAtStartup(false)
        , SynchronizationFrameCount(0)
        , SynchronizationErrorIntegrationCount(0)
        , SynchronizationAccumulatedError(0)
        , SynchronizationError(0)
        , SynchronizationPreviousError(0)
        , SynchronizationErrorStabilityCounter(false)
        , FrameWorkthroughCount(0)
        , FrameWorkthroughFinishCount(0)
        , AccumulatedError(0)
        , ExtendSamplesForSynchronization(0)
        , SynchronizationCorrectionUnits(0)
        , SynchronizationOneTenthCorrectionUnits(0)
        , LoseFieldsForSynchronization(0)
        , PreviousDisplayFrameRate()
        , PreviousSurfaceProgressive(false)
        , CountMultiplier()
        , FieldDurationTime(0)
        , RespectPolarity(false)
    {}
} ManifestationTimingState_t;

class OutputTimer_Base_c : public OutputTimer_c
{
public:
    OutputTimer_Base_c();
    ~OutputTimer_Base_c();

    //
    // Overrides for component base class functions
    //

    OutputTimerStatus_t   Halt();

    OutputTimerStatus_t   SetModuleParameters(unsigned int   ParameterBlockSize,
                                              void          *ParameterBlock);

    //
    // OutputTimer class functions
    //
    OutputTimerStatus_t   RegisterOutputCoordinator(OutputCoordinator_t   mOutputCoordinator);
    void                  UnregisterOutputCoordinator();

    void                  ResetTimeMapping();

    void                  ResetTimingContext(unsigned int         ManifestationIndex = INVALID_INDEX);

    OutputTimerStatus_t   AwaitEntryIntoDecodeWindow(Buffer_t         Buffer);

    OutputTimerStatus_t   TestForFrameDrop(Buffer_t       Buffer,
                                           OutputTimerTestPoint_t    TestPoint);

    OutputTimerStatus_t   GenerateFrameTiming(Buffer_t        Buffer);
    OutputTimerStatus_t   RecordActualFrameTiming(Buffer_t        Buffer);

    static const char    *StringifyOutputTimerStatus(OutputTimerStatus_t Status);

    OutputTimerStatus_t   SetSmoothReverseSupport(bool SmoothReverseSupportFromParser);
    OutputTimerStatus_t   SetSpeed(Rational_t Speed, PlayDirection_t Direction);

    OutputTimerStatus_t   SetPresentationInterval(const TimeStamp_c &IntervalStart, const TimeStamp_c &IntervalEnd);
    OutputTimerStatus_t   SetDiscardPts(const TimeStamp_c &DiscardPts);

protected:
    OS_Mutex_t                    mLock;
    OutputTimerConfiguration_t    mConfiguration;

    OutputSurfaceDescriptor_t   **mOutputSurfaceDescriptors;
    unsigned int                  mOutputSurfaceDescriptorsHighestIndex;

    Rational_t                    mCodedFrameRate;
    Rational_t                    mSpeed;
    PlayDirection_t               mDirection;
    Rational_t                    mLastCountMultiplier;

    int                           mTrickmodeDomain;
    int                           mExpectedTrickmodeDomain;

    TimeStamp_c                   mNextExpectedPlaybackTime;         // Value used in check for PTS jumps
    unsigned long long            mLastFrameCollationTime;

    ManifestationTimingState_t    mManifestationTimingState[MAXIMUM_MANIFESTATION_TIMING_COUNT];
    Rational_t                    mSystemClockAdjustment;
    Rational_t                   *mOutputRateAdjustments;
    Rational_t                   *mCurrentErrorJitterUs;

    static bool                   IsInInterval(TimeStamp_c Value, TimeStamp_c IntervalStart, TimeStamp_c IntervalEnd);

    void              SetTrickModeDomainBoundaries();
    void              InitializeGroupStructure();
    void              DecodeInTimeFailure(unsigned long long      FailedBy,
                                          bool              CollatedAfterDisplayTime);

    ParsedFrameParameters_t *GetParsedFrameParameters(Buffer_t Buffer)
    {
        ParsedFrameParameters_t *ParsedFrameParameters;
        Buffer->ObtainMetaDataReference(Player->MetaDataParsedFrameParametersType, (void **) &ParsedFrameParameters);
        SE_ASSERT(ParsedFrameParameters != NULL);

        return ParsedFrameParameters;
    }

    ParsedFrameParameters_t *GetParsedFrameParametersReference(Buffer_t Buffer)
    {
        ParsedFrameParameters_t *ParsedFrameParametersReference;
        Buffer->ObtainMetaDataReference(Player->MetaDataParsedFrameParametersReferenceType, (void **) &ParsedFrameParametersReference);
        SE_ASSERT(ParsedFrameParametersReference != NULL);

        return ParsedFrameParametersReference;
    }

    // TODO(pht) propagate type from void* to ParsedAudioVideoParameters_t* castable in ParsedAudioParameters_t* or ParsedVideoParameters_t*
    void *GetParsedAudioVideoDataParameters(Buffer_t Buffer)
    {
        ParsedAudioVideoParameters_t *ParsedAudioVideoDataParameters = NULL;

        if (Stream->GetStreamType() == StreamTypeVideo)
        {
            Buffer->ObtainMetaDataReference(Player->MetaDataParsedVideoParametersType, (void **) &ParsedAudioVideoDataParameters);
            SE_ASSERT(ParsedAudioVideoDataParameters != NULL);
        }
        else if (Stream->GetStreamType() == StreamTypeAudio)
        {
            Buffer->ObtainMetaDataReference(Player->MetaDataParsedAudioParametersType, (void **) &ParsedAudioVideoDataParameters);
            SE_ASSERT(ParsedAudioVideoDataParameters != NULL);
        }

        if (ParsedAudioVideoDataParameters != NULL)
        {
            ParsedAudioVideoDataParameters->StreamType = Stream->GetStreamType();
        }

        return ParsedAudioVideoDataParameters;
    }

    OutputTiming_t *GetOutputTiming(Buffer_t Buffer)
    {
        OutputTiming_t *OutputTiming;
        Buffer->ObtainMetaDataReference(Player->MetaDataOutputTimingType, (void **) &OutputTiming);
        SE_ASSERT(OutputTiming != NULL);

        return OutputTiming;
    }

    OutputTimerStatus_t       DropTestPresentationInterval(const ParsedFrameParameters_t *ParsedFrameParameters, bool beforeDecode);
    OutputTimerStatus_t       DropTestDiscardPts(const ParsedFrameParameters_t *ParsedFrameParameters);
    OutputTimerStatus_t       DropTestSingleGopPolicy(ParsedFrameParameters_t *ParsedFrameParameters);
    OutputTimerStatus_t       DropTestDiscardPartialFrame(Buffer_t Buffer, const ParsedFrameParameters_t *ParsedFrameParameters);
    OutputTimerStatus_t       DropTestSupportedSpeed();
    OutputTimerStatus_t       DropTestVideoTrickmodeDomain(ParsedFrameParameters_t *ParsedFrameParameters, bool beforeDecode);
    OutputTimerStatus_t       DropTestAudioTrickmodeDecodedSpeedMismatch(ParsedFrameParameters_t *ParsedFrameParameters);
    OutputTimerStatus_t       DropTestReallyTooEarlyForManifestation(OutputTiming_t *OutputTiming);
    OutputTimerStatus_t       DropTestTooLateForManifestation(Buffer_t Buffer, ParsedFrameParameters_t *ParsedFrameParameters, OutputTiming_t *OutputTiming);
    OutputTimerStatus_t       DropTestUntimedFrame(ParsedFrameParameters_t *ParsedFrameParameters, OutputTiming_t *OutputTiming);

    OutputTimerStatus_t       DropTestBeforeDecodeWindow(Buffer_t       Buffer);
    OutputTimerStatus_t       DropTestBeforeDecode(Buffer_t             Buffer);
    OutputTimerStatus_t       DropTestBeforeOutputTiming(Buffer_t       Buffer);
    OutputTimerStatus_t       DropTestBeforeManifestation(Buffer_t      Buffer);

    void                      TrickmodeMonitoring(Buffer_t Buffer, ParsedFrameParameters_t *ParsedFrameParameters, OutputTiming_t *OutputTiming);
    void                      UpdateTrickmodeDomain();
    OutputTimerStatus_t       DecodeInTimeMonitoring(Buffer_t Buffer, ParsedFrameParameters_t *ParsedFrameParameters, OutputTiming_t *OutputTiming);
    bool                      ResetTimeMappingOnDecodeInTimeFailure(bool DataDeliveredLate);
    void                      HandleConfirmedDecodeInTimeFailure(long long FailedBy, bool CollatedAfterDisplayTime);

    void IncrementTestFrameDropStatistics(OutputTimerStatus_t  Status, OutputTimerTestPoint_t  TestPoint);

    //
    // Function to perform AVD synchronization, could be overridden, but not recomended
    //

    virtual OutputTimerStatus_t   PerformAVDSync(unsigned int         TimingIndex,
                                                 unsigned long long   ExpectedFrameOutputTime,
                                                 unsigned long long   ActualFrameOutputTime,
                                                 OutputTiming_t      *OutputTiming);

    //
    // Functions to be provided by the stream specific implementations
    //

    virtual OutputTimerStatus_t   InitializeConfiguration() = 0;

    virtual OutputTimerStatus_t   FillOutFrameTimingRecord(unsigned long long   SystemTime,
                                                           void                *ParsedAudioVideoDataParameters,
                                                           OutputTiming_t      *OutputTiming) = 0;

    virtual OutputTimerStatus_t   CorrectSynchronizationError(unsigned int      TimingIndex) = 0;

private:
    OutputCoordinator_t           mOutputCoordinator;
    OutputCoordinatorContext_t    mOutputCoordinatorContext;

    BufferManager_t               mBufferManager;
    BufferPool_t                  mDecodeBufferPool;
    SharedPtr_c<BufferPool_c>     mCodedFrameBufferPool;
    unsigned int                  mCodedFrameBufferMaximumSize;


    unsigned long long            mLastSystemTime;
    unsigned long long            mLastValidDuration;
    unsigned long long            mCandidateDuration;
    unsigned long long            mLastCandidateDuration;
    TimeStamp_c                   mLastPTS;

    Rational_t                    mLastCodedFrameRate;
    long long                     mDecodeTimeJumpThreshold;
    long long                     mTimeOffset;
    bool                          mTimeMappingValidForDecodeTiming;      // Values used in decode time window checking
    unsigned int                  mTimeMappingInvalidatedAtDecodeIndex;
    TimeStamp_c                   mLastSeenDecodeTime;
    OutputTimerStatus_t           mLastFrameDropPreDecodeDecision;
    unsigned int                  mKeyFramesSinceDiscontinuity;
    DecodeInTimeState_t           mDecodeInTimeState;
    unsigned int                  mDecodeInTimeFailures;
    unsigned long long            mDecodeInTimeBehind;
    int                           mLastSynchronizationPolicy;
    bool                          mSmoothReverseSupport;
    bool                          mSmoothReverseSupportChanged;
    bool                          mGlobalSetSynchronizationAtStartup;
    SynchronizationState_t        mGlobalSynchronizationState;
    OS_Mutex_t                    mPresentationIntervalMutex;
    TimeStamp_c                   mPresentationIntervalStart;
    TimeStamp_c                   mPresentationIntervalEnd;
    OS_Mutex_t                    mDiscardPtsMutex;
    TimeStamp_c                   mDiscardPts;
    int                           mDroppedPartialFrame;

    DISALLOW_COPY_AND_ASSIGN(OutputTimer_Base_c);
};

#endif
