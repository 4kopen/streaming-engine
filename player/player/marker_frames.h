/************************************************************************
Copyright (C) 2003-2014 STMicroelectronics. All Rights Reserved.

This file is part of the Streaming Engine.

Streaming Engine is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License version 2 as
published by the Free Software Foundation.

Streaming Engine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with Streaming Engine; see the file COPYING.  If not, write to the Free
Software Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307,
USA.

The Streaming Engine Library may alternatively be licensed under a
proprietary license from ST.
************************************************************************/
#ifndef H_MARKER_FRAMES
#define H_MARKER_FRAMES

typedef enum
{
    DrainDiscardMarker = 0,
    DrainPlayOutMarker,
    EosMarker,
    SplicingMarker,
    ReqTimeMarker,
    DiscontinuityMarker,
} MarkerType_t;

typedef struct MarkerSplicingData_s
{
    unsigned int        mSplicingFlags;
    long long           mPtsOffset;
} MarkerSplicingData_t;

typedef struct MarkerReqTimeData_s
{
    unsigned long long mPts;
    unsigned int mMarkerId0;
    unsigned int mMarkerId1;
} MarkerReqTimeData_t;

typedef struct
{
    MarkerType_t       mMarkerType;
    unsigned long long mSequenceNumber;
    union
    {
        MarkerSplicingData_t        mSplicingMarkerData;
        MarkerReqTimeData_t         mReqTimeMarkerData;
    };
} MarkerFrame_t;

static inline const char *MarkerFrameTypeString(MarkerType_t markerType)
{
    switch (markerType)
    {
    case DrainDiscardMarker:
        return "DrainDiscardMarker";
    case DrainPlayOutMarker:
        return "DrainPlayOutMarker";
    case EosMarker:
        return "EosMarker";
    case SplicingMarker:
        return "SplicingMarker";
    case ReqTimeMarker:
        return "ReqTimeMarker";
    case DiscontinuityMarker:
        return "DiscontinuityMarker";
    default:
        return "<unknown marker>";
    }
}

#endif // H_MARKER_FRAMES
