/************************************************************************
Copyright (C) 2003-2014 STMicroelectronics. All Rights Reserved.

This file is part of the Streaming Engine.

Streaming Engine is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License version 2 as
published by the Free Software Foundation.

Streaming Engine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with Streaming Engine; see the file COPYING.  If not, write to the Free
Software Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307,
USA.

The Streaming Engine Library may alternatively be licensed under a
proprietary license from ST.
************************************************************************/

#include "player_version.h"
#include "player_generic.h"
#include "player_playback.h"
#include "player_stream.h"

#undef TRACE_TAG
#define TRACE_TAG "Player_Generic_c"

// //////////////////////////////////////////////////////////////////////////////////////////////////
//
//      Constructor function - Initialize our data
//

// The coprocessor is capable of doing the frame-parsing for some codec (e.g AAC) .
// This switch disable this coprocessing by default.

Player_Generic_c::Player_Generic_c()
    : mPlayerLock()
    , ShutdownPlayer(false)
    , BufferManager(NULL)
    , mBufferCodedFrameBufferType()
    , mBufferStartCodeHeadersBufferType()
    , PlayerControlStructurePool(NULL)
    , ListOfPlaybacks(NULL)
    , PlaybackCount(0)
    , PolicyRecord()
    , ControlsRecord()
    , mAudioCodedConfiguration(PLAYER_AUDIO_DEFAULT_CODED_FRAME_COUNT,
                               PLAYER_AUDIO_DEFAULT_CODED_MEMORY_SIZE,
                               PLAYER_AUDIO_DEFAULT_CODED_FRAME_MAXIMUM_SIZE,
                               PLAYER_AUDIO_DEFAULT_CODED_FRAME_PARTITION_NAME)
    , mVideoCodedConfiguration(PLAYER_VIDEO_DEFAULT_CODED_FRAME_COUNT,
                               PLAYER_VIDEO_DEFAULT_HD_CODED_MEMORY_SIZE,
                               PLAYER_VIDEO_DEFAULT_HD_CODED_FRAME_MAXIMUM_SIZE,
                               PLAYER_VIDEO_DEFAULT_CODED_FRAME_PARTITION_NAME)
    , mRwLockBWLimiter()
{
    SE_INFO(group_player, "setting up Player2 - version %s\n", PLAYER_VERSION);

    OS_InitializeMutex(&mPlayerLock);
    OS_InitializeRwLock(&mRwLockBWLimiter);

    //
    SetPolicy(PlayerAllPlaybacks, PlayerAllStreams, (PlayerPolicy_t)PolicyStatisticsOnAudio,  PolicyValueDisapply);
    SetPolicy(PlayerAllPlaybacks, PlayerAllStreams, (PlayerPolicy_t)PolicyStatisticsOnVideo,  PolicyValueDisapply);
    //
    SetPolicy(PlayerAllPlaybacks, PlayerAllStreams, PolicyLivePlayback,        PolicyValueDisapply);
    SetPolicy(PlayerAllPlaybacks, PlayerAllStreams, PolicyAVDSynchronization,  PolicyValueApply);
    // player, playback streams
    SetPolicy(PlayerAllPlaybacks, PlayerAllStreams, PolicyIgnoreStreamUnPlayableCalls, PolicyValueApply);
    SetPolicy(PlayerAllPlaybacks, PlayerAllStreams, PolicyDecodeBuffersUserAllocated,  PolicyValueDisapply);
    SetPolicy(PlayerAllPlaybacks, PlayerAllStreams, PolicyDecodeBufferCopy, PolicyValueDefaultBufferCopyMode);
    // collators
    SetPolicy(PlayerAllPlaybacks, PlayerAllStreams, PolicyLimitInputInjectAhead,               PolicyValueApply);
    SetPolicy(PlayerAllPlaybacks, PlayerAllStreams, PolicyReduceCollatedData,                  PolicyValueDisapply);
    // frameparsers
    SetPolicy(PlayerAllPlaybacks, PlayerAllStreams, PolicyMPEG2DoNotHonourProgressiveFrameFlag,    PolicyValueDisapply);
    SetPolicy(PlayerAllPlaybacks, PlayerAllStreams, PolicyMPEG2ApplicationType,                    PolicyValueMPEG2ApplicationDvb);
    SetPolicy(PlayerAllPlaybacks, PlayerAllStreams, PolicyH264TreatDuplicateDpbValuesAsNonReferenceFrameFirst, PolicyValueApply);
    SetPolicy(PlayerAllPlaybacks, PlayerAllStreams, PolicyH264ForcePicOrderCntIgnoreDpbDisplayFrameOrdering,   PolicyValueApply);
    SetPolicy(PlayerAllPlaybacks, PlayerAllStreams, PolicyH264ValidateDpbValuesAgainstPTSValues,               PolicyValueApply);
    SetPolicy(PlayerAllPlaybacks, PlayerAllStreams, PolicyH264TreatTopBottomPictureStructAsInterlaced,         PolicyValueDisapply);
    SetPolicy(PlayerAllPlaybacks, PlayerAllStreams, PolicyH264AllowNonIDRResynchronization,                    PolicyValueDisapply);
    SetPolicy(PlayerAllPlaybacks, PlayerAllStreams, PolicyBypassReordering,                                    PolicyValueDisapply);
    SetPolicy(PlayerAllPlaybacks, PlayerAllStreams, PolicyFrameRateCalculationPrecedence, PolicyValuePrecedencePtsStreamContainerDefault);
    SetPolicy(PlayerAllPlaybacks, PlayerAllStreams, PolicyContainerFrameRate, 0);
    // codecs
    SetPolicy(PlayerAllPlaybacks, PlayerAllStreams, PolicyAllowBadPreProcessedFrames,   PolicyValueApply);
    SetPolicy(PlayerAllPlaybacks, PlayerAllStreams, PolicyAllowBadHevcPreProcessedFrames,   PolicyValueApply);
    SetPolicy(PlayerAllPlaybacks, PlayerAllStreams, PolicyVideoPlayStreamMemoryProfile, PolicyValueVideoPlayStreamMemoryProfileAuto);
    SetPolicy(PlayerAllPlaybacks, PlayerAllStreams, PolicyErrorDecodingLevel,          PolicyValuePolicyErrorDecodingLevelMaximum);
    SetPolicy(PlayerAllPlaybacks, PlayerAllStreams, PolicyCpuSelectionId,              PolicyValueCpuSelectionDefault);
    SetPolicy(PlayerAllPlaybacks, PlayerAllStreams, PolicyAudioApplicationType,        PolicyValueAudioApplicationIso);
    SetPolicy(PlayerAllPlaybacks, PlayerAllStreams, PolicyAudioServiceType,            PolicyValueAudioServiceMain);
    SetPolicy(PlayerAllPlaybacks, PlayerAllStreams, PolicyRegionType,                  PolicyValueRegionDVB);
    SetPolicy(PlayerAllPlaybacks, PlayerAllStreams, PolicyAudioProgramReferenceLevel,  0);   //Default level is set to 0 to let
    //the fw decide based upon the stream
    SetPolicy(PlayerAllPlaybacks, PlayerAllStreams, PolicyAudioSubstreamId,            0);
    SetPolicy(PlayerAllPlaybacks, PlayerAllStreams, PolicyAllowReferenceFrameSubstitution,         PolicyValueApply);
    SetPolicy(PlayerAllPlaybacks, PlayerAllStreams, PolicyDiscardForReferenceQualityThreshold,     0);
    SetPolicy(PlayerAllPlaybacks, PlayerAllStreams, PolicyDiscardForManifestationQualityThreshold, 0);
    // codecs and manifestors
    SetPolicy(PlayerAllPlaybacks, PlayerAllStreams, PolicyDecimateDecoderOutput, PolicyValueDecimateDecoderOutputHalf);
    // manifestors
    SetPolicy(PlayerAllPlaybacks, PlayerAllStreams, PolicyManifestFirstFrameEarly, PolicyValueApply);
    SetPolicy(PlayerAllPlaybacks, PlayerAllStreams, PolicyVideoLastFrameBehaviour, PolicyValueLeaveLastFrameOnScreen);
    // output timer and coordinator
    SetPolicy(PlayerAllPlaybacks, PlayerAllStreams, PolicyMasterClock,                     PolicyValueVideoClockMaster);
    SetPolicy(PlayerAllPlaybacks, PlayerAllStreams, PolicyExternalTimeMapping,             PolicyValueDisapply);
    SetPolicy(PlayerAllPlaybacks, PlayerAllStreams, PolicyVideoStartImmediate,     PolicyValueApply);
    // output timer
    SetPolicy(PlayerAllPlaybacks, PlayerAllStreams, PolicyStreamSingleGroupBetweenDiscontinuities,  PolicyValueDisapply);
    SetPolicy(PlayerAllPlaybacks, PlayerAllStreams, PolicyTrickModeDomain,         PolicyValueTrickModeAuto);
    // output coordinator
    SetPolicy(PlayerAllPlaybacks, PlayerAllStreams, PolicyPtsForwardJumpDetectionThreshold,        4);
    SetPolicy(PlayerAllPlaybacks, PlayerAllStreams, PolicyLivePlaybackLatencyVariabilityMs,        0);
    SetPolicy(PlayerAllPlaybacks, PlayerAllStreams, PolicyLivePlaybackResetOnPcrPauseMs,           0);
    SetPolicy(PlayerAllPlaybacks, PlayerAllStreams, PolicyExternalLatencyBehavior ,                PolicyValueExternalLatencyPresentFrameEarly);
    SetPolicy(PlayerAllPlaybacks, PlayerAllStreams, PolicyCaptureProfile,                          PolicyValueCaptureProfileDisabled);
    // audio
    SetPolicy(PlayerAllPlaybacks, PlayerAllStreams, PolicyAudioStreamDrivenDualMono,       PolicyValueStreamDrivenDualMono);
    SetPolicy(PlayerAllPlaybacks, PlayerAllStreams, PolicyAudioDualMonoChannelSelection,   PolicyValueDualMonoStereoOut);
    SetPolicy(PlayerAllPlaybacks, PlayerAllStreams, PolicyAudioDeEmphasis,                 PolicyValueDisapply);

    Codec_c::RegisterTuneable();
    Manifestor_c::RegisterTuneable();
    Collator_c::RegisterTuneable();
    register_fatal_error_handler_tuneables();

    //
    // do not add normal initialization after this point
    //
}

// //////////////////////////////////////////////////////////////////////////////////////////////////
//
//      Destructor function - close down
//

Player_Generic_c::~Player_Generic_c()
{
    ShutdownPlayer = true;
    SE_INFO(group_player, "Releasing Player2\n");
    SE_ASSERT(PlaybackCount == 0);

    BufferManager->UnregisterTuneables();
    unregister_fatal_error_handler_tuneables();
    Collator_c::UnregisterTuneable();
    Manifestor_c::UnregisterTuneable();
    Codec_c::UnregisterTuneable();

    OS_TerminateMutex(&mPlayerLock);
    OS_TerminateRwLock(&mRwLockBWLimiter);
}

void Player_Generic_c::GetReadLockBWLimiter()
{
    OS_LockRead(&mRwLockBWLimiter);
}

void Player_Generic_c::ReleaseReadLockBWLimiter()
{
    OS_UnLockRead(&mRwLockBWLimiter);
}

void Player_Generic_c::GetWriteLockBWLimiter()
{
    OS_LockWrite(&mRwLockBWLimiter);
}

void Player_Generic_c::ReleaseWriteLockBWLimiter()
{
    OS_UnLockWrite(&mRwLockBWLimiter);
}

