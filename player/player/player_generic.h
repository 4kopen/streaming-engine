/************************************************************************
Copyright (C) 2003-2014 STMicroelectronics. All Rights Reserved.

This file is part of the Streaming Engine.

Streaming Engine is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License version 2 as
published by the Free Software Foundation.

Streaming Engine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with Streaming Engine; see the file COPYING.  If not, write to the Free
Software Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307,
USA.

The Streaming Engine Library may alternatively be licensed under a
proprietary license from ST.
************************************************************************/

#ifndef H_PLAYER_GENERIC
#define H_PLAYER_GENERIC

#include <stm_event.h>

#include "player.h"
#include "allocinline.h"
#include "marker_frames.h"
#include "coded_configuration.h"

#undef TRACE_TAG
#define TRACE_TAG "Player_Generic_c"

#define PLAYER_STREAM_PROCESS_NB            4   // Player creates/manages 4 threads for each stream (CollateToParse, ParseToDecode, DecodeToManifest, PostManifest)

#define PLAYER_MAX_CONTROL_STRUCTURE_BUFFERS        512 /* 32 */
#define PLAYER_MAX_INPUT_BUFFERS            16

#define PLAYER_MAX_INLINE_PARAMETER_BLOCK_SIZE      64
#define PLAYER_MAX_DISCARDED_FRAMES         64

#define PLAYER_LIMIT_ON_OUT_OF_ORDER_DECODES        13  // Limit on the number of out of order decodes, this is applied
// because some streams (H264) limit the ref frame count, and the standard
// calculations can then leave us with a potential for deadlock I have set
// it to 16 - the number of frames that can be in the manifestor

#define PLAYER_MAX_EVENT_WAIT               50  // Ms
#define PLAYER_NEXT_FRAME_EVENT_WAIT            20  // Ms (lower because it will also scan for state changes)
#define PLAYER_DEFAULT_FRAME_TIME           100 // Ms
#define PLAYER_MAX_DISCARD_DRAIN_TIME           1000    // Ms
#define PLAYER_MAX_MARKER_TIME_THROUGH_CODEC        4000    // Ms (Quite loose this one)
#ifdef CONFIG_HCE
#define PLAYER_MAX_TIME_ON_DISPLAY          1600000 // Ms (time for a single frame)
#else
#define PLAYER_MAX_TIME_ON_DISPLAY          16000   // Ms (time for a single frame)
#endif
#define PLAYER_MAX_PLAYOUT_TIME             60000   // Ms (I am allowing a minute here for slideshows )

#define PLAYER_MAX_TIME_IN_RETIMING         10000  // us
#define PLAYER_RETIMING_WAIT                5   // ms
#define PLAYER_LIMITED_EARLY_MANIFESTATION_WINDOW 150000 // 150 ms

// Common frame sizes
#define PLAYER_4K2K_FRAME_WIDTH             4096
#define PLAYER_4K2K_FRAME_HEIGHT            2400
#define PLAYER_UHD_FRAME_WIDTH              3840
#define PLAYER_UHD_FRAME_HEIGHT             2160
#define PLAYER_UHD_DECODE_FRAME_HEIGHT      2176
#define PLAYER_HD_FRAME_WIDTH               1920
#define PLAYER_HD_FRAME_HEIGHT              1088
#define PLAYER_HD720P_FRAME_WIDTH           1280
#define PLAYER_HD720P_FRAME_HEIGHT          720
#define PLAYER_SD_FRAME_WIDTH               720
#define PLAYER_SD_FRAME_HEIGHT              576

// These default values are derived from the maximum existing values in the specific codecs
//
#define PLAYER_MAX_BUFFER_IN_DECODE_PIPELINE            128
#define PLAYER_AUDIO_DEFAULT_CODED_FRAME_COUNT          1024 // Originally these default values were derived from the maximum existing values in the specific codecs, but some streams need more
#define PLAYER_AUDIO_DEFAULT_CODED_MEMORY_SIZE          262144
#define PLAYER_AUDIO_DEFAULT_CODED_FRAME_MAXIMUM_SIZE   32768
#define PLAYER_AUDIO_DEFAULT_CODED_FRAME_PARTITION_NAME "aud-coded"

#define PLAYER_VIDEO_DEFAULT_CODED_FRAME_COUNT          2048
#define PLAYER_VIDEO_DEFAULT_4K2K_CODED_MEMORY_SIZE       20971520
#define PLAYER_VIDEO_DEFAULT_HD_CODED_MEMORY_SIZE         8388608
#define PLAYER_VIDEO_DEFAULT_720P_CODED_MEMORY_SIZE       4194304
#define PLAYER_VIDEO_DEFAULT_SD_CODED_MEMORY_SIZE         2488320

// From H264 standard, annex A.3.1: "Number of bits of macroblock_layer( ) data for any macroblock is not greater than 3200."
#define PLAYER_VIDEO_DEFAULT_4K2K_CODED_FRAME_MAXIMUM_SIZE  4718592
#define PLAYER_VIDEO_DEFAULT_HD_CODED_FRAME_MAXIMUM_SIZE    1052672
#define PLAYER_VIDEO_DEFAULT_720P_CODED_FRAME_MAXIMUM_SIZE  1052672
#define PLAYER_VIDEO_DEFAULT_SD_CODED_FRAME_MAXIMUM_SIZE    648000
#define PLAYER_VIDEO_DEFAULT_CODED_FRAME_PARTITION_NAME     "vid-coded"

// Audio Config parameters
enum
{
    NEW_DUALMONO_CONFIG           = (1 << 2),
    NEW_STEREO_CONFIG             = (1 << 3),
    NEW_DRC_CONFIG                = (1 << 4),
    NEW_AAC_CONFIG                = (1 << 5),
    NEW_SPEAKER_CONFIG            = (1 << 6),
    NEW_EMPHASIS_CONFIG           = (1 << 7),
    NEW_MUTE_CONFIG               = (1 << 8),
};

#define BASE_EXTENSIONS                                 0x10000

typedef class Player_Generic_c  *Player_Generic_t;
typedef class HavanaStream_c    *HavanaStream_t;

class CollateToParseEdge_c;
class ParseToDecodeEdge_c;
class DecodeToManifestEdge_c;
class PostManifestEdge_c;
class ES_Processor_c;

enum
{
    OSFnSetEventOnPostManifestation = BASE_EXTENSIONS,

    PlayerFnSwitchCollator,
    PlayerFnSwitchFrameParser,
    PlayerFnSwitchCodec,
    PlayerFnSwitchOutputTimer,
    PlayerFnSwitchComplete
};

// /////////////////////////////////////////////////////////////////////////
//
// The definition of the player parameter block (set module parameters calls)
//

typedef enum
{
    PlayerSetCodedFrameBufferParameters     = 0
} PlayerParameterBlockType_t;

typedef struct PlayerSetCodedFrameBufferParameters_s
{
    PlayerStreamType_t        StreamType;
    CodedConfiguration_t      Configuration;

    PlayerSetCodedFrameBufferParameters_s()
        : StreamType()
        , Configuration()
    {
    }

} PlayerSetCodedFrameBufferParameters_t;

typedef struct PlayerParameterBlock_s
{
    PlayerParameterBlockType_t              ParameterType;
    PlayerSetCodedFrameBufferParameters_t   CodedFrame;

    PlayerParameterBlock_s()
        : ParameterType()
        , CodedFrame()
    {
    }
} PlayerParameterBlock_t;


// /////////////////////////////////////////////////////////////////////////
//
// The player control structure buffer type and associated structures
//

typedef enum
{
    ActionInSequenceCall    = 0
} PlayerControlAction_t;

typedef struct PlayerInSequenceParams_s
{
    PlayerComponentFunction_t     Fn;
    unsigned int          UnsignedInt;
    void             *Pointer;
    unsigned char         Block[PLAYER_MAX_INLINE_PARAMETER_BLOCK_SIZE];
    PlayerEventRecord_t       Event;
    stm_event_t               EventMngr;

} PlayerInSequenceParams_t;

typedef struct PlayerControlStructure_s
{
    PlayerControlAction_t     Action;
    PlayerSequenceType_t      SequenceType;
    PlayerSequenceValue_t     SequenceValue;

    PlayerInSequenceParams_t      InSequence;

} PlayerControlStructure_t;

#define BUFFER_PLAYER_CONTROL_STRUCTURE     "PlayerControlStructure"
#define BUFFER_PLAYER_CONTROL_STRUCTURE_TYPE   {BUFFER_PLAYER_CONTROL_STRUCTURE, BufferDataTypeBase, AllocateFromOSMemory, 4, 0, true, true, sizeof(PlayerControlStructure_t)}

#define BUFFER_FAKE     "FakeBuffer"
#define BUFFER_FAKE_TYPE   {BUFFER_FAKE, BufferDataTypeBase, AllocateFromOSMemory, 4, 0, true, true, sizeof(char)}


// /////////////////////////////////////////////////////////////////////////
//
// Input-related buffer type definitions
//

#define BUFFER_CODED_FRAME_BUFFER       "CodedFrameBuffer"
#define BUFFER_CODED_FRAME_BUFFER_TYPE  {BUFFER_CODED_FRAME_BUFFER, BufferDataTypeBase, AllocateFromSuppliedBlock, 4, 64, false, false, 0}

#define BUFFER_STARTCODE_HEADERS_BUFFER       "StartCodeHeadersBuffer"
#define BUFFER_STARTCODE_HEADERS_BUFFER_TYPE  {BUFFER_STARTCODE_HEADERS_BUFFER, BufferDataTypeBase, AllocateFromSuppliedBlock, 4, 64, false, false, 0}

// /////////////////////////////////////////////////////////////////////////
//
// The Buffer sequence number meta data type, allowing the
// attachment of a sequence number to all coded buffers.
//
typedef enum
{
    InitialState,
    QueuedState,    // Frame is pushed to display
    RenderedState,  // Frame came back from display and was rendered
    RetimeState     // Frame come back from display but was not rendered and will be queued again for display
} ManifestedState_t;

typedef struct PlayerSequenceNumber_s
{
    bool                        mIsMarkerFrame;
    unsigned int                StreamUniqueIdentifier;
    unsigned int                StreamTypeIdentifier;
    unsigned long long          Value;
    unsigned long long          PTS;
    unsigned long long          PtsValid;
    MarkerFrame_t               mMarkerFrame;

    // Statistical values for each buffer

    unsigned long long      TimeEntryInPipeline;
    unsigned long long      TimeEntryInProcess0;
    unsigned long long      TimeEntryInProcess1;
    unsigned long long      TimeEntryInProcess2;
    unsigned long long      TimeEntryInProcess3;

    unsigned long long      TimePassToCodec;
    unsigned long long      TimePassToManifestor;
} PlayerSequenceNumber_t;

#define METADATA_SEQUENCE_NUMBER        "SequenceNumber"
#define METADATA_SEQUENCE_NUMBER_TYPE       {METADATA_SEQUENCE_NUMBER, MetaDataTypeBase, AllocateFromOSMemory, 4, 0, true, false, sizeof(PlayerSequenceNumber_t)}

// ---------------------------------------------------------
//  The policy state record
//

typedef enum
{
    PolicyStatisticsOnAudio  = PolicyMaxPolicy,
    PolicyStatisticsOnVideo,

    PolicyMaxExtraPolicy
} PlayerExtraPolicies_t;

#define POLICY_WORDS                (((unsigned int)PolicyMaxExtraPolicy + 31)/32)

typedef struct PlayerPolicyState_s
{
    unsigned int      Specified[POLICY_WORDS];
    int               Value[PolicyMaxExtraPolicy];
} PlayerPolicyState_t;

#define CONTROL_WORDS       (((unsigned int) ControlsRecord_max_value + 31)/32)

// ---------------------------------------------------------
//  The controls storage (for controls which are not a policy)
//

typedef enum
{
    ControlsRecord_drc_main,
    ControlsRecord_StreamDrivenDownmix,
    ControlsRecord_SpeakerConfig,
    ControlsRecord_AacConfig,
    ControlsRecord_mute,
    ControlsRecord_not_implemented,
    ControlsRecord_max_value,
} PlayerControlStorage_t;

typedef struct PlayerControslStorageState_s
{
    unsigned int     Specified[CONTROL_WORDS];
    stm_se_drc_t     Control_drc;
    bool             Control_StreamDrivenDownmix;
    bool             Control_mute;
    stm_se_audio_channel_assignment_t  Control_SpeakerConfig;
    stm_se_mpeg4aac_t                  Control_AacConfig;
} PlayerControslStorageState_t;

// ---------------------------------------------------------
//  The accumulated buffer table type (held during re-ordering after decode)
//

typedef struct PlayerBufferRecord_s
{
    Buffer_t                Buffer;
    unsigned long long      SequenceNumber;

    bool                    ReleasedBuffer;

    union
    {
        ParsedFrameParameters_t     *ParsedFrameParameters;
        PlayerControlStructure_t    *ControlStructure;
        unsigned int                 DisplayFrameIndex;
    };
} PlayerBufferRecord_t;

// /////////////////////////////////////////////////////////////////////////
//
// The class definition
//

class Player_Generic_c : public Player_c
{
public:
    Player_Generic_c();
    ~Player_Generic_c();

    //
    // Mechanisms for registering global items
    //

    PlayerStatus_t   RegisterBufferManager(BufferManager_t       BufferManager);

    //
    // Called when fully initialized for registering tuneables that depend on
    // full initialization.
    //

    void             RegisterLateTuneables();

    //
    // Mechanisms relating to event retrieval
    //

    PlayerStatus_t   SpecifySignalledEvents(PlayerPlayback_t     Playback,
                                            PlayerStream_t       Stream,
                                            PlayerEventMask_t    Events,
                                            void                *UserData  = NULL);

    //
    // Mechanisms for policy management
    //

    PlayerStatus_t   SetPolicy(PlayerPlayback_t   Playback,
                               PlayerStream_t     Stream,
                               PlayerPolicy_t     Policy,
                               int                PolicyValue = PolicyValueApply);

    //
    // Mechanisms for managing playbacks
    //

    PlayerStatus_t   CreatePlayback(OutputCoordinator_t    OutputCoordinator,
                                    PlayerPlayback_t      *Playback);

    PlayerStatus_t   TerminatePlayback(PlayerPlayback_t    Playback);

    int              GetPlaybackCount() const;

    //
    // Mechanisms for inserting module specific parameters
    //

    PlayerStatus_t   SetModuleParameters(PlayerPlayback_t  Playback,
                                         PlayerStream_t    Stream,
                                         unsigned int      ParameterBlockSize,
                                         void             *ParameterBlock);


    //
    // Mechanisms for controls storage management
    //
    PlayerStatus_t      SetControl(PlayerPlayback_t   Playback,
                                   PlayerStream_t     Stream,
                                   stm_se_ctrl_t      Ctrl,
                                   const void        *Value);

    PlayerStatus_t      GetControl(PlayerPlayback_t   Playback,
                                   PlayerStream_t     Stream,
                                   stm_se_ctrl_t      Ctrl,
                                   void              *Value,
                                   bool               ForceReset = false);

    PlayerStatus_t      MapControl(PlayerPlayback_t               Playback,
                                   PlayerStream_t                 Stream,
                                   PlayerControlStorage_t         ControlRecordName,
                                   PlayerControslStorageState_t *&SpecificControlRecord);

    //
    // Support functions for the child classes
    //

    BufferManager_t  GetBufferManager() { SE_ASSERT(BufferManager != NULL); return BufferManager; }

    BufferPool_t     GetControlStructurePool() { return PlayerControlStructurePool; }

    int              PolicyValue(PlayerPlayback_t     Playback,
                                 PlayerStream_t        Stream,
                                 PlayerPolicy_t        Policy);

    //
    // Low power functions
    //

    PlayerStatus_t   LowPowerEnter();
    PlayerStatus_t   LowPowerExit();
    PlayerStatus_t   PlaybackLowPowerEnter(PlayerPlayback_t        Playback);
    PlayerStatus_t   PlaybackLowPowerExit(PlayerPlayback_t        Playback);

    void ProcessDrain(PlayerPlayback_t          Playback);

    BufferType_t      GetCodedFrameBufferType() const { return mBufferCodedFrameBufferType; }
    BufferType_t      GetStartCodeHeadersBufferType() const { return mBufferStartCodeHeadersBufferType; }

    // Protects some members in this class.  See per-member comments.
    //
    // Lock ordering: To avoid deadlocks, a thread shall not acquire this lock
    // while holding a lock owned by a lower level object (e.g.
    // DecodeToManifestEdge_c::mStreamLock).  Acquiring lower level locks (e.g.
    // PlayerPlayback_c::mStreamListLock) while holding this one is fine.
    mutable OS_Mutex_t mPlayerLock;

    void GetReadLockBWLimiter();
    void ReleaseReadLockBWLimiter();
    void GetWriteLockBWLimiter();
    void ReleaseWriteLockBWLimiter();

private:
    friend class CollateToParseEdge_c;
    friend class ParseToDecodeEdge_c;
    friend class DecodeToManifestEdge_c;
    friend class PostManifestEdge_c;

    bool              ShutdownPlayer;

    BufferManager_t   BufferManager;
    BufferType_t      mBufferCodedFrameBufferType;
    BufferType_t      mBufferStartCodeHeadersBufferType;

//    MetaDataType_t      MetaDataSequenceNumberType;   //made public so WMA can make it's private CodedFrameBufferPool properly

    BufferPool_t      PlayerControlStructurePool;

    // Playback linked list.  Protected by mPlayerLock.
    PlayerPlayback_t      ListOfPlaybacks;

    // Number of playbacks in ListOfPlaybacks.  Protected by mPlayerLock.
    int                   PlaybackCount;

    PlayerPolicyState_t   PolicyRecord;

    PlayerControslStorageState_t     ControlsRecord;

    // Template configurations used to initialize new playbacks.
    CodedConfiguration_t mAudioCodedConfiguration;
    CodedConfiguration_t mVideoCodedConfiguration;

    OS_RwLock_t      mRwLockBWLimiter;

    DISALLOW_COPY_AND_ASSIGN(Player_Generic_c);
};

#endif
