/************************************************************************
Copyright (C) 2003-2014 STMicroelectronics. All Rights Reserved.

This file is part of the Streaming Engine.

Streaming Engine is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License version 2 as
published by the Free Software Foundation.

Streaming Engine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with Streaming Engine; see the file COPYING.  If not, write to the Free
Software Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307,
USA.

The Streaming Engine Library may alternatively be licensed under a
proprietary license from ST.
************************************************************************/

#include "player_threads.h"

#include "ring_generic.h"
#include "player_generic.h"
#include "player_playback.h"
#include "player_stream.h"
#include "collate_to_parse_edge.h"
#include "parse_to_decode_edge.h"
#include "decode_to_manifest_edge.h"
#include "post_manifest_edge.h"
#include "es_processor_base.h"

#undef TRACE_TAG
#define TRACE_TAG "Player_Generic_c"

// //////////////////////////////////////////////////////////////////////////////////////////////////
//
//      Create a new playback
//

PlayerStatus_t   Player_Generic_c::CreatePlayback(
    OutputCoordinator_t       OutputCoordinator,
    PlayerPlayback_t         *Playback)
{
    PlayerPlayback_t          NewPlayback;

    *Playback = NULL;

    NewPlayback = PlayerPlayback_c::New(this, OutputCoordinator, mAudioCodedConfiguration, mVideoCodedConfiguration);
    if (NewPlayback == NULL)
    {
        SE_ERROR("Unable to construct playback\n");
        return PlayerInsufficientMemory;
    }

    OS_LockMutex(&mPlayerLock);
    NewPlayback->Next   = ListOfPlaybacks;
    ListOfPlaybacks     = NewPlayback;
    PlaybackCount++;
    SE_DEBUG(group_player, "PlaybackCount %d\n", PlaybackCount);
    OS_UnLockMutex(&mPlayerLock);

    *Playback = NewPlayback;

    return PlayerNoError;
}

// //////////////////////////////////////////////////////////////////////////////////////////////////
//
//      Delete a playback
//

PlayerStatus_t   Player_Generic_c::TerminatePlayback(
    PlayerPlayback_t          Playback)
{
    PlayerEventRecord_t       Event;
    PlayerPlayback_t         *PointerToPlayback;

    OS_LockMutex(&mPlayerLock);

    SE_ASSERT(PlaybackCount > 0);

    for (PointerToPlayback       = &ListOfPlaybacks;
         (*PointerToPlayback)   != NULL;
         PointerToPlayback       = &((*PointerToPlayback)->Next))
    {
        if ((*PointerToPlayback) == Playback)
        {
            (*PointerToPlayback)  = Playback->Next;
            break;
        }
    }

    delete Playback;
    PlaybackCount--;
    SE_DEBUG(group_player, "PlaybackCount %d\n", PlaybackCount);

    OS_UnLockMutex(&mPlayerLock);

    return PlayerNoError;
}

int Player_Generic_c::GetPlaybackCount() const
{
    OS_LockMutex(&mPlayerLock);
    int Count = PlaybackCount;
    OS_UnLockMutex(&mPlayerLock);
    return Count;
}

//
//  Low power enter / exit functions at playback level
//  These methods are used to stop current streams processing (e.g. collator) to speed-up low power enter procedure
//

PlayerStatus_t   Player_Generic_c::PlaybackLowPowerEnter(PlayerPlayback_t Playback)
{
    SE_INFO(group_player, "Playback 0x%p\n", Playback);
    // Save low power state at playback level
    Playback->LowPowerEnter();
    return PlayerNoError;
}

PlayerStatus_t   Player_Generic_c::PlaybackLowPowerExit(PlayerPlayback_t Playback)
{
    SE_INFO(group_player, "Playback 0x%p\n", Playback);
    // Reset low power state at playback level
    Playback->LowPowerExit();
    return PlayerNoError;
}


