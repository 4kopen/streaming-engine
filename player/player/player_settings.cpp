/************************************************************************
Copyright (C) 2003-2014 STMicroelectronics. All Rights Reserved.

This file is part of the Streaming Engine.

Streaming Engine is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License version 2 as
published by the Free Software Foundation.

Streaming Engine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with Streaming Engine; see the file COPYING.  If not, write to the Free
Software Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307,
USA.

The Streaming Engine Library may alternatively be licensed under a
proprietary license from ST.
************************************************************************/

#include <stdarg.h>
#include "player_generic.h"
#include "player_playback.h"
#include "player_stream.h"
#include "collate_to_parse_edge.h"
#include "parse_to_decode_edge.h"
#include "decode_to_manifest_edge.h"
#include "post_manifest_edge.h"

#undef TRACE_TAG
#define TRACE_TAG "Player_Generic_c"

// Used for human readable policy reporting
static const char *LookupPlayerPolicy(PlayerPolicy_t Policy)
{
    switch ((unsigned int)Policy)
    {
#define C(x) case x: return #x
        C(NonCascadedControls);
        C(PolicyLivePlayback);
        C(PolicyAVDSynchronization);
        C(PolicyIgnoreStreamUnPlayableCalls);
        C(PolicyDecodeBuffersUserAllocated);
        C(PolicyDecodeBufferCopy);
        C(PolicyLimitInputInjectAhead);
        C(PolicyReduceCollatedData);
        C(PolicyMPEG2DoNotHonourProgressiveFrameFlag);
        C(PolicyMPEG2ApplicationType);
        C(PolicyH264TreatDuplicateDpbValuesAsNonReferenceFrameFirst);
        C(PolicyH264ForcePicOrderCntIgnoreDpbDisplayFrameOrdering);
        C(PolicyH264ValidateDpbValuesAgainstPTSValues);
        C(PolicyH264TreatTopBottomPictureStructAsInterlaced);
        C(PolicyH264AllowNonIDRResynchronization);
        C(PolicyBypassReordering);
        C(PolicyFrameRateCalculationPrecedence);
        C(PolicyContainerFrameRate);
        C(PolicyAllowBadPreProcessedFrames);
        C(PolicyAllowBadHevcPreProcessedFrames);
        C(PolicyVideoPlayStreamMemoryProfile);
        C(PolicyCompressBufferSize);
        C(PolicyErrorDecodingLevel);
        C(PolicyCpuSelectionId);
        C(PolicyAudioApplicationType);
        C(PolicyAudioServiceType);
        C(PolicyRegionType);
        C(PolicyAudioProgramReferenceLevel);
        C(PolicyAudioSubstreamId);
        C(PolicyAllowReferenceFrameSubstitution);
        C(PolicyDiscardForReferenceQualityThreshold);
        C(PolicyDiscardForManifestationQualityThreshold);
        C(PolicyDecimateDecoderOutput);
        C(PolicyManifestFirstFrameEarly);
        C(PolicyVideoLastFrameBehaviour);
        C(PolicyMasterClock);
        C(PolicyExternalTimeMapping);
        C(PolicyVideoStartImmediate);
        C(PolicyStreamSingleGroupBetweenDiscontinuities);
        C(PolicyTrickModeDomain);
        C(PolicyPtsForwardJumpDetectionThreshold);
        C(PolicyLivePlaybackLatencyVariabilityMs);
        C(PolicyLivePlaybackResetOnPcrPauseMs);
        C(PolicyExternalLatencyBehavior);
        C(PolicyCaptureProfile);
        C(PolicyAudioStreamDrivenDualMono);
        C(PolicyAudioDualMonoChannelSelection);
        C(PolicyAudioDeEmphasis);
        // Private policies
        C(PolicyStatisticsOnAudio);
        C(PolicyStatisticsOnVideo);
        C(PolicyNotImplemented);
#undef C

    default:
        if (Policy < (PlayerPolicy_t) PolicyMaxExtraPolicy)
        {
            SE_ERROR("LookupPlayerPolicy - Lookup failed for policy %d\n", Policy);
            return "UNKNOWN";
        }
        return "UNSPECIFIED";
    }
}

static BufferDataDescriptor_t  MetaDataInputDescriptor          = METADATA_PLAYER_INPUT_DESCRIPTOR_TYPE;
static BufferDataDescriptor_t  MetaDataCodedFrameParameters     = METADATA_CODED_FRAME_PARAMETERS_TYPE;
static BufferDataDescriptor_t  MetaDataStartCodeList            = METADATA_START_CODE_LIST_TYPE;
static BufferDataDescriptor_t  MetaDataParsedFrameParameters    = METADATA_PARSED_FRAME_PARAMETERS_TYPE;
static BufferDataDescriptor_t  MetaDataParsedFrameParametersReference = METADATA_PARSED_FRAME_PARAMETERS_REFERENCE_TYPE;
static BufferDataDescriptor_t  MetaDataParsedVideoParameters    = METADATA_PARSED_VIDEO_PARAMETERS_TYPE;
static BufferDataDescriptor_t  MetaDataParsedAudioParameters    = METADATA_PARSED_AUDIO_PARAMETERS_TYPE;
static BufferDataDescriptor_t  MetaDataOutputTiming             = METADATA_OUTPUT_TIMING_TYPE;

static BufferDataDescriptor_t  BufferFake                       = BUFFER_FAKE_TYPE;
static BufferDataDescriptor_t  BufferPlayerControlStructure     = BUFFER_PLAYER_CONTROL_STRUCTURE_TYPE;
static BufferDataDescriptor_t  MetaDataSequenceNumberDescriptor = METADATA_SEQUENCE_NUMBER_TYPE;
static BufferDataDescriptor_t  MetaDataUserDataDescriptor       = METADATA_USER_DATA_TYPE;

static BufferDataDescriptor_t  BufferCodedFrameBuffer           = BUFFER_CODED_FRAME_BUFFER_TYPE;
static BufferDataDescriptor_t  BufferStartCodeHeadersBuffer     = BUFFER_STARTCODE_HEADERS_BUFFER_TYPE;

// //////////////////////////////////////////////////////////////////////////////////////////////////
//
//      Register the buffer manager to the player
//

PlayerStatus_t   Player_Generic_c::RegisterBufferManager(
    BufferManager_t       BufferManager)
{
    // TODO(pht) rollbacks on error!

    if (this->BufferManager != NULL)
    {
        SE_ERROR("Attempt to change buffer manager, not permitted\n");
        return PlayerError;
    }

    this->BufferManager     = BufferManager;
    //
    // Register the predefined types, and the two buffer types used by the player
    //
    BufferStatus_t Status  = BufferManager->CreateBufferDataType(&MetaDataInputDescriptor,         &MetaDataInputDescriptorType);
    if (Status != BufferNoError)
    {
        return PlayerError;
    }

    Status  = BufferManager->CreateBufferDataType(&MetaDataCodedFrameParameters,        &MetaDataCodedFrameParametersType);
    if (Status != BufferNoError)
    {
        return PlayerError;
    }

    Status  = BufferManager->CreateBufferDataType(&MetaDataStartCodeList,           &MetaDataStartCodeListType);
    if (Status != BufferNoError)
    {
        return PlayerError;
    }

    Status  = BufferManager->CreateBufferDataType(&MetaDataParsedFrameParameters,       &MetaDataParsedFrameParametersType);
    if (Status != BufferNoError)
    {
        return PlayerError;
    }

    Status  = BufferManager->CreateBufferDataType(&MetaDataParsedFrameParametersReference,  &MetaDataParsedFrameParametersReferenceType);
    if (Status != BufferNoError)
    {
        return PlayerError;
    }

    Status  = BufferManager->CreateBufferDataType(&MetaDataParsedVideoParameters,       &MetaDataParsedVideoParametersType);
    if (Status != BufferNoError)
    {
        return PlayerError;
    }

    Status  = BufferManager->CreateBufferDataType(&MetaDataParsedAudioParameters,       &MetaDataParsedAudioParametersType);
    if (Status != BufferNoError)
    {
        return PlayerError;
    }

    Status  = BufferManager->CreateBufferDataType(&MetaDataOutputTiming,            &MetaDataOutputTimingType);
    if (Status != BufferNoError)
    {
        return PlayerError;
    }

    Status  = BufferManager->CreateBufferDataType(&MetaDataSequenceNumberDescriptor,    &MetaDataSequenceNumberType);
    if (Status != BufferNoError)
    {
        return PlayerError;
    }

    Status  = BufferManager->CreateBufferDataType(&BufferFake,        &BufferFakeType);
    if (Status != BufferNoError)
    {
        return PlayerError;
    }

    Status  = BufferManager->CreateBufferDataType(&BufferPlayerControlStructure,        &BufferPlayerControlStructureType);
    if (Status != BufferNoError)
    {
        return PlayerError;
    }

    Status  = BufferManager->CreateBufferDataType(&BufferCodedFrameBuffer,          &mBufferCodedFrameBufferType);
    if (Status != BufferNoError)
    {
        return PlayerError;
    }

    Status  = BufferManager->CreateBufferDataType(&BufferStartCodeHeadersBuffer,   &mBufferStartCodeHeadersBufferType);
    if (Status != BufferNoError)
    {
        return PlayerError;
    }

    Status  = BufferManager->CreateBufferDataType(&MetaDataUserDataDescriptor,     &MetaDataUserDataType);
    if (Status != BufferNoError)
    {
        return PlayerError;
    }

    //
    // Create pools of buffers for input and player control structures
    //
    Status  = BufferManager->CreatePool(&PlayerControlStructurePool, BufferPlayerControlStructureType, PLAYER_MAX_CONTROL_STRUCTURE_BUFFERS);
    if (Status != BufferNoError)
    {
        SE_ERROR("Failed to create pool of player control structures\n");
        return PlayerError;
    }

    return PlayerNoError;
}

void Player_Generic_c::RegisterLateTuneables()
{
    BufferManager->RegisterTuneables();
}

// //////////////////////////////////////////////////////////////////////////////////////////////////
//
//      Set a policy
//

PlayerStatus_t   Player_Generic_c::SetPolicy(PlayerPlayback_t Playback,
                                             PlayerStream_t   Stream,
                                             PlayerPolicy_t   Policy,
                                             int              PolicyValue)
{
    PlayerPolicyState_t *SpecificPolicyRecord;
    SE_INFO(group_player, "Playback 0x%p Stream 0x%p %-45s %d\n", Playback,
            Stream, LookupPlayerPolicy(Policy), PolicyValue);

    // Check Policy parameter validity
    if (Policy >= (PlayerPolicy_t)PolicyMaxExtraPolicy)
    {
        SE_ERROR("Invalid policy %d\n", Policy);
        return PlayerError;
    }

    // Check parameters consistency
    if ((Playback              != PlayerAllPlaybacks) &&
        (Stream                != PlayerAllStreams) &&
        (Stream->GetPlayback() != Playback))
    {
        SE_ERROR("Attempt to set policy on specific stream, and different specific playback\n");
        return PlayerError;
    }

    // Determine at which level the policy should be applied
    if (Stream != PlayerAllStreams)
    {
        SpecificPolicyRecord  = &Stream->PolicyRecord;
    }
    else if (Playback != PlayerAllPlaybacks)
    {
        // Verify the applicable range of the Policy if specified
        switch (Policy)
        {
        case PolicyLivePlaybackLatencyVariabilityMs:
            if (!inrange(PolicyValue, STM_SE_CTRL_VALUE_MIN_PLAYBACK_LATENCY, STM_SE_CTRL_VALUE_MAX_PLAYBACK_LATENCY))
            {
                SE_ERROR("Requested %dms of Playback Latency is out of range [%d, %d]\n",
                         PolicyValue, STM_SE_CTRL_VALUE_MIN_PLAYBACK_LATENCY, STM_SE_CTRL_VALUE_MAX_PLAYBACK_LATENCY);
                return PlayerError;
            }
            break;
        default:
            // no range specified.
            break;
        }
        SpecificPolicyRecord  = &Playback->PolicyRecord;
    }
    else
    {
        SpecificPolicyRecord  = &PolicyRecord;
    }

    // Save new policy settings
    SpecificPolicyRecord->Specified[Policy / 32]  |= 1 << (Policy % 32);
    SpecificPolicyRecord->Value[Policy]      = PolicyValue;
    return PlayerNoError;
}


// //////////////////////////////////////////////////////////////////////////////////////////////////
//
//      Set module parameters
//

PlayerStatus_t   Player_Generic_c::SetModuleParameters(
    PlayerPlayback_t   Playback,
    PlayerStream_t     Stream,
    unsigned int       ParameterBlockSize,
    void              *ParameterBlock)
{
    PlayerParameterBlock_t  *PlayerParameterBlock = (PlayerParameterBlock_t *)ParameterBlock;

    if (ParameterBlockSize != sizeof(PlayerParameterBlock_t))
    {
        SE_ERROR("Invalid parameter block\n");
        return PlayerError;
    }

    switch (PlayerParameterBlock->ParameterType)
    {
    case PlayerSetCodedFrameBufferParameters:

        //
        // Check that this is not stream specific, and update the relevant values.
        //
        if (Stream != PlayerAllStreams)
        {
            SE_ERROR("Coded frame parameters cannot be set after a stream has been created\n");
            return PlayerError;
        }

        switch (PlayerParameterBlock->CodedFrame.StreamType)
        {
        case StreamTypeAudio:
            if (Playback == PlayerAllPlaybacks)
            {
                mAudioCodedConfiguration = PlayerParameterBlock->CodedFrame.Configuration;
            }
            else
            {
                Playback->mAudioCodedConfiguration = PlayerParameterBlock->CodedFrame.Configuration;
            }

            break;

        case StreamTypeVideo:
            if (Playback == PlayerAllPlaybacks)
            {
                mVideoCodedConfiguration = PlayerParameterBlock->CodedFrame.Configuration;
            }
            else
            {
                Playback->mVideoCodedConfiguration = PlayerParameterBlock->CodedFrame.Configuration;
            }

            break;

        default:
            SE_ERROR("Attempt to set Coded frame parameters for StreamTypeNone\n");
            return PlayerError;
            break;
        }

        break;

    default:
        SE_ERROR("Unrecognised parameter block (%d)\n", PlayerParameterBlock->ParameterType);
        return PlayerError;
    }

    return  PlayerNoError;
}

// //////////////////////////////////////////////////////////////////////////////////////////////////
//
//      Discover the status of a policy
//

int     Player_Generic_c::PolicyValue(PlayerPlayback_t    Playback,
                                      PlayerStream_t        Stream,
                                      PlayerPolicy_t        Policy)
{
    int             Value;
    unsigned int        Offset;
    unsigned int        Mask;

    // Check Policy parameter validity
    if (Policy >= (PlayerPolicy_t)PolicyMaxExtraPolicy)
    {
        SE_ERROR("Invalid policy %d\n", Policy);
        return PlayerError;
    }

    // Check parameters consistency
    if ((Playback        != PlayerAllPlaybacks) &&
        (Stream         != PlayerAllStreams) &&
        (Stream->GetPlayback()   != Playback))
    {
        SE_ERROR("Attempt to get policy on specific stream, and differing specific playback\n");
        return PlayerError;
    }

    if (Stream != PlayerAllStreams)
    {
        Playback  = Stream->GetPlayback();
    }

    Offset      = Policy / 32;
    Mask        = (1 << (Policy % 32));
    Value       = 0;

    if ((PolicyRecord.Specified[Offset] & Mask) != 0)
    {
        Value = PolicyRecord.Value[Policy];
    }

    if (Playback != PlayerAllPlaybacks)
    {
        if ((Playback->PolicyRecord.Specified[Offset] & Mask) != 0)
        {
            Value = Playback->PolicyRecord.Value[Policy];
        }

        if (Stream != PlayerAllStreams)
        {
            if ((Stream->PolicyRecord.Specified[Offset] & Mask) != 0)
            {
                Value = Stream->PolicyRecord.Value[Policy];
            }
        }
    }

    return Value;
}

// //////////////////////////////////////////////////////////////////////////////////////////////////
//
//   Store the control's data into the specified object
//

PlayerStatus_t Player_Generic_c::SetControl(PlayerPlayback_t  Playback,
                                            PlayerStream_t    Stream,
                                            stm_se_ctrl_t     Ctrl,
                                            const void       *Value)
{
    SE_DEBUG(group_player, "Playback 0x%p Stream 0x%p Ctrl %d\n", Playback, Stream, Ctrl);

    // controls the consistency of the request
    if ((Playback              != PlayerAllPlaybacks) &&
        (Stream                != PlayerAllStreams) &&
        (Stream->GetPlayback() != Playback))
    {
        SE_ERROR("Attempt to set control on specific stream, and differing specific playback\n");
        return PlayerError;
    }

    // gets the object on which the control is applied (Stream, Playback or Player)
    PlayerControslStorageState_t *SpecificControlRecord;

    if (Stream != PlayerAllStreams)
    {
        SpecificControlRecord   = &Stream->ControlsRecord;
    }
    else if (Playback != PlayerAllPlaybacks)
    {
        SpecificControlRecord   = &Playback->ControlsRecord;
    }
    else
    {
        SpecificControlRecord   = &ControlsRecord;
    }

    PlayerControlStorage_t  ControlRecordName = ControlsRecord_not_implemented;

    switch (Ctrl)
    {
    //
    // Compound Controls
    //
    case STM_SE_CTRL_AUDIO_DYNAMIC_RANGE_COMPRESSION:
    {
        stm_se_drc_t *drc = (stm_se_drc_t *) Value;

        ControlRecordName = ControlsRecord_drc_main;
        SpecificControlRecord->Control_drc = *drc;
        SE_VERBOSE(group_player, "Set DRC mode:%d cut:%d boost:%d\n", drc->mode, drc->cut, drc->boost);
        break;
    }

    case STM_SE_CTRL_SPEAKER_CONFIG:
    {
        stm_se_audio_channel_assignment_t *channelassignment = (stm_se_audio_channel_assignment_t *) Value;

        ControlRecordName = ControlsRecord_SpeakerConfig;
        SpecificControlRecord->Control_SpeakerConfig = *channelassignment;
        SE_VERBOSE(group_player, "Set channel assignment pair:%d.%d.%d.%d.%d mal:%d\n",
                   channelassignment->pair0, channelassignment->pair1, channelassignment->pair2,
                   channelassignment->pair3, channelassignment->pair4,
                   channelassignment->malleable);
        break;
    }

    case STM_SE_CTRL_AAC_DECODER_CONFIG:
    {
        stm_se_mpeg4aac_t *aac_config = (stm_se_mpeg4aac_t *) Value;

        ControlRecordName = ControlsRecord_AacConfig;
        SpecificControlRecord->Control_AacConfig = *aac_config;
        SE_VERBOSE(group_player, "Set AAC config: prof:%d sbr:%d ps:%d\n",
                   aac_config->aac_profile, aac_config->sbr_enable, aac_config->ps_enable);
        break;
    }

    //
    // Simple Controls
    //
    case STM_SE_CTRL_STREAM_DRIVEN_STEREO:
    {
        bool *streamdrivendownmix = (bool *) Value;

        ControlRecordName = ControlsRecord_StreamDrivenDownmix;
        SpecificControlRecord->Control_StreamDrivenDownmix = *streamdrivendownmix;
        SE_VERBOSE(group_player, "Set StreamDrivenDownmix: %d\n", SpecificControlRecord->Control_StreamDrivenDownmix);
        break;
    }

    case STM_SE_CTRL_PLAY_STREAM_MUTE:
    {
        bool *mute = (bool *) Value;

        ControlRecordName = ControlsRecord_mute;
        SpecificControlRecord->Control_mute = (*mute == STM_SE_CTRL_VALUE_APPLY) ? true : false;
        SE_VERBOSE(group_player, "Set mute: %d\n", SpecificControlRecord->Control_mute);
        break;
    }

    default:
        SE_ERROR("Non supported ctrl %d\n", Ctrl);
        return PlayerError;
    }

    // at this point: ControlsRecord_not_implemented != ControlRecordName
    // indicates that the control is now specified for this object
    SpecificControlRecord->Specified[ControlRecordName / 32]  |= 1 << (ControlRecordName % 32);
    return PlayerNoError;
}

// //////////////////////////////////////////////////////////////////////////////////////////////////
//
//   Retrieve the control's datas from the appropriate object
//   (from stream object if available, else from playback object, else from player object)
//
//   in case no match found return PlayerMatchNotFound and still reset value if ForceReset set

PlayerStatus_t Player_Generic_c::GetControl(PlayerPlayback_t  Playback,
                                            PlayerStream_t    Stream,
                                            stm_se_ctrl_t     Ctrl,
                                            void             *Value,
                                            bool              ForceReset)
{
    SE_VERBOSE(group_player, "Playback 0x%p Stream 0x%p Ctrl %d\n", Playback, Stream, Ctrl);

    // controls the consistency of the request
    if ((Playback              != PlayerAllPlaybacks) &&
        (Stream                != PlayerAllStreams) &&
        (Stream->GetPlayback() != Playback))
    {
        SE_ERROR("Attempt to get control on specific stream, and differing specific playback\n");
        return PlayerError;
    }

    if (Stream != PlayerAllStreams)
    {
        Playback = Stream->GetPlayback();
    }

    PlayerStatus_t PlayerStatus = PlayerNoError;
    PlayerControslStorageState_t *SpecificControlRecord = NULL;

    switch (Ctrl)
    {
    //
    // Compound Controls
    //
    case STM_SE_CTRL_AUDIO_DYNAMIC_RANGE_COMPRESSION:
    {
        stm_se_drc_t *drc = (stm_se_drc_t *) Value;

        PlayerStatus = MapControl(Playback, Stream, ControlsRecord_drc_main, SpecificControlRecord);
        if (PlayerStatus == PlayerMatchNotFound)
        {
            if (ForceReset)
            {
                SE_VERBOSE(group_player, "Get reset DRC value\n");
                *drc = stm_se_drc_t();
            }
        }
        else
        {
            *drc = SpecificControlRecord->Control_drc;
            SE_VERBOSE(group_player, "Get DRC mode:%d cut:%d boost:%d\n", drc->mode, drc->cut, drc->boost);
        }
        break;
    }

    case STM_SE_CTRL_SPEAKER_CONFIG:
    {
        stm_se_audio_channel_assignment_t *channelassignment = (stm_se_audio_channel_assignment_t *) Value;

        PlayerStatus = MapControl(Playback, Stream, ControlsRecord_SpeakerConfig, SpecificControlRecord);
        if (PlayerStatus == PlayerMatchNotFound)
        {
            if (ForceReset)
            {
                SE_VERBOSE(group_player, "Get reset channel assignment value\n");
                *channelassignment = stm_se_audio_channel_assignment_t();
            }
        }
        else
        {
            *channelassignment = SpecificControlRecord->Control_SpeakerConfig;
            SE_VERBOSE(group_player, "Get channel assignment pair:%d.%d.%d.%d.%d mal:%d\n",
                       channelassignment->pair0, channelassignment->pair1, channelassignment->pair2,
                       channelassignment->pair3, channelassignment->pair4,
                       channelassignment->malleable);
        }
        break;
    }

    case STM_SE_CTRL_AAC_DECODER_CONFIG:
    {
        stm_se_mpeg4aac_t *aac_config = (stm_se_mpeg4aac_t *) Value;

        PlayerStatus = MapControl(Playback, Stream, ControlsRecord_AacConfig, SpecificControlRecord);
        if (PlayerStatus == PlayerMatchNotFound)
        {
            if (ForceReset)
            {
                SE_VERBOSE(group_player, "Get reset AAC config value\n");
                *aac_config = stm_se_mpeg4aac_t();
            }
        }
        else
        {
            *aac_config = SpecificControlRecord->Control_AacConfig;
            SE_VERBOSE(group_player, "Get AAC config: prof:%d sbr:%d ps:%d\n",
                       aac_config->aac_profile, aac_config->sbr_enable, aac_config->ps_enable);
        }
        break;
    }

    //
    // Simple Controls
    //
    case STM_SE_CTRL_STREAM_DRIVEN_STEREO:
    {
        bool *streamdrivendownmix = (bool *) Value;

        PlayerStatus = MapControl(Playback, Stream, ControlsRecord_StreamDrivenDownmix, SpecificControlRecord);
        if (PlayerStatus == PlayerMatchNotFound)
        {
            if (ForceReset)
            {
                SE_VERBOSE(group_player, "Get reset stream driven stereo value\n");
                *streamdrivendownmix = false;
            }
        }
        else
        {
            *streamdrivendownmix = SpecificControlRecord->Control_StreamDrivenDownmix;
            SE_VERBOSE(group_player, "Get StreamDrivenDownmix: %d\n", *streamdrivendownmix);
        }
        break;
    }

    case STM_SE_CTRL_PLAY_STREAM_MUTE:
    {
        bool *mute = (bool *) Value;

        PlayerStatus = MapControl(Playback, Stream, ControlsRecord_mute, SpecificControlRecord);
        if (PlayerStatus == PlayerMatchNotFound)
        {
            if (ForceReset)
            {
                SE_VERBOSE(group_player, "Get reset mute value\n");
                *mute = false;
            }
        }
        else
        {
            *mute = SpecificControlRecord->Control_mute;
            SE_VERBOSE(group_player, "Get mute: %d\n", *mute);
        }
        break;
    }

    default:
        SE_ERROR("Non supported ctrl %d\n", Ctrl);
        return PlayerError;
    }

    return PlayerStatus;
}

// //////////////////////////////////////////////////////////////////////////////////////////////////
//
//   Find the appropriate object which holds the control's data
//
//   in case not found return PlayerMatchNotFound
//   else return PlayerNoError
//

PlayerStatus_t Player_Generic_c::MapControl(
    PlayerPlayback_t               Playback,
    PlayerStream_t                 Stream,
    PlayerControlStorage_t         ControlRecordName,
    PlayerControslStorageState_t *&SpecificControlRecord
)
{
    SE_VERBOSE(group_player, "Playback 0x%p Stream 0x%p CtrlRecName %d\n", Playback, Stream, ControlRecordName);

    // get the applicable control if exists (from Stream, Playback or Player)
    unsigned int Offset = ControlRecordName / 32;
    unsigned int Mask   = (1 << (ControlRecordName % 32));

    // get the control at Player level if exists
    if ((ControlsRecord.Specified[Offset] & Mask) != 0)
    {
        SpecificControlRecord   = &ControlsRecord;
        SE_VERBOSE(group_player, "data found at Player level for ControlRecordName=%d\n", ControlRecordName);
    }

    // if a specific playback is indicated, get the control at Playback level if exists
    if (Playback != PlayerAllPlaybacks)
    {
        if ((Playback->ControlsRecord.Specified[Offset] & Mask) != 0)
        {
            SpecificControlRecord   = &Playback->ControlsRecord;
            SE_VERBOSE(group_player, "data found at Playback level for ControlRecordName=%d\n", ControlRecordName);
        }

        // if a specific stream is indicated, get the control at Stream level if exists
        if (Stream != PlayerAllStreams)
        {
            if ((Stream->ControlsRecord.Specified[Offset] & Mask) != 0)
            {
                SpecificControlRecord   = &Stream->ControlsRecord;
                SE_VERBOSE(group_player, "data found at Stream level for ControlRecordName=%d\n", ControlRecordName);
            }
        }
    }

    if (NULL == SpecificControlRecord)
    {
        SE_VERBOSE(group_player, "No datas available for ControlRecordName=%d\n", ControlRecordName);
        return PlayerMatchNotFound;
    }

    return PlayerNoError;
}

// //////////////////////////////////////////////////////////////////////////////////////////////////
//
//      Low power enter
//

PlayerStatus_t Player_Generic_c::LowPowerEnter()
{
    // Nothing to do here
    return PlayerNoError;
}

// //////////////////////////////////////////////////////////////////////////////////////////////////
//
//      Low power exit
//

PlayerStatus_t Player_Generic_c::LowPowerExit()
{
    // Nothing to do here
    return PlayerNoError;
}
