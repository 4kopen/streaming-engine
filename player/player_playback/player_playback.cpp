/************************************************************************
Copyright (C) 2003-2014 STMicroelectronics. All Rights Reserved.

This file is part of the Streaming Engine.

Streaming Engine is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License version 2 as
published by the Free Software Foundation.

Streaming Engine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with Streaming Engine; see the file COPYING.  If not, write to the Free
Software Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307,
USA.

The Streaming Engine Library may alternatively be licensed under a
proprietary license from ST.
************************************************************************/

#include "player_playback.h"
#include "player_stream.h"
#include "collate_to_parse_edge.h"
#include "parse_to_decode_edge.h"
#include "decode_to_manifest_edge.h"
#include "post_manifest_edge.h"

#undef TRACE_TAG
#define TRACE_TAG "PlayerPlayback_c"

PlayerPlaybackInterface_c::~PlayerPlaybackInterface_c() {}

PlayerPlayback_c *PlayerPlayback_c::New(Player_Generic_c *Player,
                                        OutputCoordinator_c *OutputCoordinator,
                                        const CodedConfiguration_t &AudioCodedConfiguration,
                                        const CodedConfiguration_t &VideoCodedConfiguration)
{
    PlayerPlayback_c *Playback = new PlayerPlayback_c(Player, OutputCoordinator,
                                                      AudioCodedConfiguration, VideoCodedConfiguration);
    if (Playback != NULL)
    {
        if (Playback->FinalizeInit() != PlayerNoError)
        {
            delete Playback;
            Playback = NULL;
        }
    }

    return Playback;
}

PlayerPlayback_c::PlayerPlayback_c(Player_Generic_c *Player,
                                   OutputCoordinator_c *OutputCoordinator,
                                   const CodedConfiguration_t &AudioCodedConfiguration,
                                   const CodedConfiguration_t &VideoCodedConfiguration)
    : Next(NULL)
    , OutputCoordinator(OutputCoordinator)
    , mSpeed(1)
    , mDirection(PlayForward)
    , PolicyRecord()
    , ControlsRecord()
    , mAudioCodedConfiguration(AudioCodedConfiguration)
    , mVideoCodedConfiguration(VideoCodedConfiguration)
    , mPlayer(Player)
    , mDrainStartStopEvent()
    , mDrainSignalThreadRunning(false)
    , mDrainSignal()
    , mIsLowPowerState(false)
    , mStatistics()
    , mStreamListLock()
    , mListOfStreams()
    , mDataPointReceivedOutOfLiveMode(false)
{
    OS_InitializeRwLock(&mStreamListLock);

    OS_InitializeEvent(&mDrainSignal);
    OS_InitializeEvent(&mDrainStartStopEvent);

    OutputCoordinator->RegisterPlayer(Player, this, PlayerAllStreams);
    ResetStatistics();
}

PlayerStatus_t PlayerPlayback_c::FinalizeInit()
{
    OS_Thread_t               DrainThread;

    // create associated drain thread
    mDrainSignalThreadRunning = true;

    if (OS_CreateThread(&DrainThread, ProcessDrainThread, this, &player_tasks_desc[SE_TASK_PLAYBACK_DRAIN]) != OS_NO_ERROR)
    {
        SE_ERROR("Unable to create drain thread\n");
        mDrainSignalThreadRunning = false;
        return PlayerError;
    }

    OS_Status_t WaitStatus;
    // Wait for drain process to run
    do
    {
        WaitStatus = OS_WaitForEventAuto(&mDrainStartStopEvent, PLAYER_MAX_EVENT_WAIT);
        if (WaitStatus == OS_TIMED_OUT)
        {
            SE_WARNING("Playback 0x%p Still waiting for drain thread to start\n", this);
        }
    }
    while (WaitStatus == OS_TIMED_OUT);
    OS_ResetEvent(&mDrainStartStopEvent);

    SE_INFO(group_player, "Playback 0x%p drain thread created\n", this);

    return PlayerNoError;
}

PlayerPlayback_c::~PlayerPlayback_c()
{
    //
    // First drain all playing streams, playing out the remaining data
    //
    PlayerStatus_t Status = InternalDrain(false, false);
    if (Status != PlayerNoError)
    {
        SE_ERROR("Failed to drain streams\n");
    }

    // Close down Drain thread
    SE_INFO(group_player, "Playback 0x%p Close down drain thread\n", this);

    OS_ResetEvent(&mDrainStartStopEvent);
    OS_Smp_Mb(); // Write memory barrier: wmb_for_Player_Generic_Terminating coupled with: rmb_for_Player_Generic_Terminating

    if (mDrainSignalThreadRunning)
    {
        mDrainSignalThreadRunning = false;
        OS_SetEvent(&mDrainSignal);

        // Wait for drain thread to terminate
        OS_Status_t WaitStatus;
        do
        {
            WaitStatus = OS_WaitForEventAuto(&mDrainStartStopEvent, PLAYER_MAX_EVENT_WAIT);
            if (WaitStatus == OS_TIMED_OUT)
            {
                SE_WARNING("Playback 0x%p Still waiting for drain thread to stop\n", this);
            }
        }
        while (WaitStatus == OS_TIMED_OUT);
    }

    OS_TerminateEvent(&mDrainStartStopEvent);
    OS_TerminateEvent(&mDrainSignal);

    //
    // Clean up the structures
    //
    while (mListOfStreams.IsEmpty() == false)
    {
        PlayerStream_t CurStream = &*mListOfStreams.Begin();
        CurStream->mLink.Unlink();
        delete CurStream;
    }

    OS_TerminateRwLock(&mStreamListLock);
}

PlayerStatus_t PlayerPlayback_c::SetDiscardPts(
    const TimeStamp_c &DiscardPts)
{
    OS_LockRead(&mStreamListLock);

    for (PlayerStreamList_t::Iterator_c Iter = mListOfStreams.Begin();
         Iter != mListOfStreams.End();
         ++Iter)
    {
        PlayerStatus_t Status = Iter->SetDiscardPts(DiscardPts);
        if (Status != PlayerNoError)
        {
            OS_UnLockRead(&mStreamListLock);
            return Status;
        }
    }

    OS_UnLockRead(&mStreamListLock);

    return PlayerNoError;
}

PlayerStatus_t   PlayerPlayback_c::InternalDrain(
    bool                      Discard,
    bool                      ParseAllFrames)
{
    PlayerStatus_t    Status;

    SE_DEBUG(group_player, "Playback 0x%p\n", this);

    OS_LockRead(&mStreamListLock);

    //
    // Drain all playing streams (use non-blocking on individuals).
    //
    PlayerStreamInterface_c::DrainParameters_t   DrainParameters(false, Discard);
    DrainParameters.ParseAllFrames  = ParseAllFrames;

    for (PlayerStreamList_t::Iterator_c Iter = mListOfStreams.Begin();
         Iter != mListOfStreams.End();
         ++Iter)
    {
        PlayerStream_t Stream = &*Iter;
        Status = Stream->Drain(DrainParameters, NULL);
        if (Status != PlayerNoError)
        {
            SE_ERROR("Stream 0x%p - %d - failed to drain\n", Stream, Stream->GetStreamType());
        }
    }

    //
    // Start waiting for the drains to complete, and removing the appropriate data.
    //
    for (PlayerStreamList_t::Iterator_c Iter = mListOfStreams.Begin();
         Iter != mListOfStreams.End();
         ++Iter)
    {
        PlayerStream_t Stream = &*Iter;
        Status = Stream->WaitForDrainCompletion(Discard);
        if (Status != PlayerNoError)
        {
            SE_ERROR("Stream 0x%p - %d - failed to drain within allowed time (%d %d %d %d)\n",
                     Stream, Stream->GetStreamType(),
                     Stream->CollateToParseEdge->IsDiscardingUntilDrainMarkerFrame(), Stream->ParseToDecodeEdge->IsDiscardingUntilDrainMarkerFrame(),
                     Stream->DecodeToManifestEdge->IsDiscardingUntilDrainMarkerFrame(), Stream->PostManifestEdge->IsDiscardingUntilDrainMarkerFrame());
            break;
        }
    }

    OS_UnLockRead(&mStreamListLock);

    SE_DEBUG(group_player, "Playback 0x%p completed\n", this);
    return PlayerNoError;
}

PlayerStatus_t   PlayerPlayback_c::Step(PlayerStream_t StreamToStep)
{
    OS_LockRead(&mStreamListLock);

    StreamToStep->Step();

    for (PlayerStreamList_t::Iterator_c Iter = mListOfStreams.Begin();
         Iter != mListOfStreams.End();
         ++Iter)
    {
        PlayerStream_t Stream = &*Iter;
        if (Stream != StreamToStep)
        {
            Stream->DiscardStep();
        }
    }

    OS_UnLockRead(&mStreamListLock);

    return PlayerNoError;
}

//
// Set the playback speed and direction.
//
// Setting the speed to zero has the effect of pausing the playback, less than 1
// means slow motion, greater than one means fast wind.
//

PlayerStatus_t   PlayerPlayback_c::SetSpeed(int PlaySpeed)
{
    PlayDirection_t     Direction;

    SE_DEBUG(group_player, "Playback 0x%p Setting speed to %d\n", this, PlaySpeed);

    if (PlaySpeed == STM_SE_PLAYBACK_SPEED_REVERSE_STOPPED)
    {
        Direction       = PlayBackward;
        PlaySpeed       = STM_SE_PLAYBACK_SPEED_STOPPED;
    }
    else if (PlaySpeed >= 0)
    {
        Direction       = PlayForward;
    }
    else
    {
        Direction       = PlayBackward;
        PlaySpeed       = -PlaySpeed;
    }

    Rational_t Speed               = Rational_t(PlaySpeed, STM_SE_PLAYBACK_SPEED_NORMAL_PLAY);

    Rational_t MaxSpeed = MAXIMUM_SPEED_SUPPORTED; // rational
    Rational_t MinSpeed = Rational_t(1, FRACTIONAL_MINIMUM_SPEED_SUPPORTED);

    if (Speed > MaxSpeed)
    {
        Speed = MaxSpeed;
    }

    if (PlaySpeed != STM_SE_PLAYBACK_SPEED_STOPPED &&
        Speed < MinSpeed)
    {
        Speed = MinSpeed;
    }

    if ((mSpeed == Speed) && (mDirection == Direction))
    {
        return PlayerNoError;
    }

    SE_INFO(group_player, "Playback 0x%p Speed %d.%06d Direction %d\n",
            this, Speed.IntegerPart(), Speed.RemainderDecimal(), Direction);

    //
    // Are we performing a direction flip?
    //

    if (mDirection != Direction)
    {
        if ((Speed == 0) && (Direction == PlayForward))
        {
            SetSpeed(1);
        }

        if ((Speed == 0) && (Direction == PlayBackward))
        {
            SetSpeed(-1);
        }

        //
        // Drain with prejudice, but ensuring all frames are parsed
        //
        InternalDrain(true, true);
    }

    //
    // Record the new speed and direction
    //
    mSpeed = Speed;
    mDirection = Direction;

    //
    // Specifically inform the output coordinator of the change
    //
    PlayerStatus_t Status  = OutputCoordinator->SetPlaybackSpeed(PlaybackContext, Speed, Direction);
    if (Status != PlayerNoError)
    {
        SE_ERROR("Playback 0x%p failed to inform output coordinator of speed change\n", this);
        return Status;
    }

    OS_LockRead(&mStreamListLock);

    for (PlayerStreamList_t::Iterator_c Iter = mListOfStreams.Begin();
         Iter != mListOfStreams.End();
         ++Iter)
    {
        Iter->SetSpeed(Speed, Direction);
    }

    OS_UnLockRead(&mStreamListLock);

    return PlayerNoError;
}

//
// Get the playback speed.
//

void   PlayerPlayback_c::GetSpeed(int *PlaySpeed)
{
    PlayDirection_t     Direction = mDirection;
    Rational_t          Speed = mSpeed;

    *PlaySpeed          = RoundedIntegerPart(Speed * STM_SE_PLAYBACK_SPEED_NORMAL_PLAY);

    if (((*PlaySpeed) == STM_SE_PLAYBACK_SPEED_STOPPED) && (Direction != PlayForward))
    {
        *PlaySpeed      = STM_SE_PLAYBACK_SPEED_REVERSE_STOPPED;
    }
    else if (Direction != PlayForward)
    {
        *PlaySpeed      = -*PlaySpeed;
    }
}

// //////////////////////////////////////////////////////////////////////////////////////////////////
//
//      Get statistics for a playback
//

void  PlayerPlayback_c::GetStatistics(__stm_se_playback_statistics_s *Statistics)
{
    memcpy(Statistics, &mStatistics, sizeof(__stm_se_playback_statistics_s));
}


// //////////////////////////////////////////////////////////////////////////////////////////////////
//
//      Reset statistics value
//

void   PlayerPlayback_c::ResetStatistics()
{
    memset(&mStatistics, 0, sizeof(__stm_se_playback_statistics_s));
}

// //////////////////////////////////////////////////////////////////////////////////////////////////
//
//      Set the native playback time for a playback.
//

PlayerStatus_t   PlayerPlayback_c::SetNativePlaybackTime(
    unsigned long long    NativeTime,
    unsigned long long    SystemTime)
{
    unsigned long long    Now = OS_GetTimeInMicroSeconds();
    SE_DEBUG(group_player, "%016llx %016llx (%016llx)\n", NativeTime, SystemTime, Now);

    if (NotValidTime(NativeTime))
    {
        //
        // Invalid, reset all time mappings
        //
        SE_DEBUG(group_avsync, "Resetting all time mappings\n");
        OutputCoordinator->ResetTimeMapping(PlaybackContext);
    }
    else
    {
        TimeStamp_c PTS = TimeStamp_c(NativeTime, TIME_FORMAT_PTS);

        OutputCoordinator->EstablishTimeMapping(PlaybackContext, PTS, SystemTime);
    }

    return PlayerNoError;
}

// //////////////////////////////////////////////////////////////////////////////////////////////////
//
//      Retrieve the native playback time for a playback.
//

PlayerStatus_t   PlayerPlayback_c::RetrieveNativePlaybackTime(
    unsigned long long   *NativeTime,
    stm_se_time_format_t  NativeTimeFormat)
{
    TimeStamp_c           PTS;
    //
    // What is the system time
    //
    unsigned long long Now     = OS_GetTimeInMicroSeconds();
    //
    // Translate that to playback time
    //
    PlayerStatus_t Status  = OutputCoordinator->TranslateSystemTimeToPlayback(PlaybackContext, Now, &PTS);
    if (Status != PlayerNoError)
    {
        // fails when mapping not established.
        // In that case we return PlayerBusy to notify that the Time mapping is not established
        SE_DEBUG(group_player, "Playback 0x%p could not translate system time to playback time: mapping not established\n", this);
        return PlayerBusy;
    }

    // Translate to PTS time format if requested
    *NativeTime = PTS.Value(NativeTimeFormat);

    return Status;
}

// //////////////////////////////////////////////////////////////////////////////////////////////////
//
//      Clock recovery functions are entirely handled by the output coordinator
//

PlayerStatus_t   PlayerPlayback_c::ClockRecoveryDataPoint(
    stm_se_time_format_t    TimeFormat,
    bool                    NewSequence,
    unsigned long long    SourceTime,
    unsigned long long    LocalTime)
{
    stm_se_time_format_t        SourceTimeFormat        = TimeFormat;
    bool                        OutputCoordInitialized  = false;
    bool                        Live = mPlayer->PolicyValue(this, PlayerAllStreams, PolicyLivePlayback);

    SE_DEBUG(group_se_pipeline, "Received PCR - (SourceTime=%llu, SystemTime=%llu)\n", SourceTime, LocalTime);

    // ClockRecovery is not initialized when there's no A/V stream attached and it's not a new PCR sequence, so initialize it now.
    // This can happen when zapping between streams with the same PCR PID
    OutputCoordinator->ClockRecoveryIsInitialized(&OutputCoordInitialized);

    if ((mDataPointReceivedOutOfLiveMode && Live == PolicyValueApply) || NewSequence || OutputCoordInitialized == false)
    {
        // no need to check status (always noError)
        OutputCoordinator->ClockRecoveryInitialize(SourceTimeFormat);
    }

    if (!Live)
    {
        SE_VERBOSE(group_se_pipeline, "Receiving PCR with PolicyLivePlayback disabled so ignoring it!\n");
        mDataPointReceivedOutOfLiveMode = true;
        return PlayerNoError;
    }
    else
    {
        mDataPointReceivedOutOfLiveMode = false;
    }

    GetStatistics().PCRSourceTime = SourceTime;
    GetStatistics().PCRSystemTime = LocalTime;
    GetStatistics().PCRCounter++;

    return OutputCoordinator->ClockRecoveryDataPoint(SourceTime, LocalTime);
}

PlayerStatus_t   PlayerPlayback_c::ClockRecoveryEstimate(
    unsigned long long   *SourceTime,
    unsigned long long   *LocalTime)
{
    return OutputCoordinator->ClockRecoveryEstimate(SourceTime, LocalTime);
}

//
// Get the playback speed and direction.
//

void   PlayerPlayback_c::GetSpeed(Rational_t       *Speed, PlayDirection_t      *Direction)
{
    *Speed  = mSpeed;
    *Direction  = mDirection;
}

//
// Return a stream which has its service policy set as SECONDARY.
//

PlayerStream_t PlayerPlayback_c::GetSecondaryStream()
{
    OS_LockRead(&mStreamListLock);

    for (PlayerStreamList_t::Iterator_c Iter = mListOfStreams.Begin();
         Iter != mListOfStreams.End();
         ++Iter)
    {
        PlayerStream_t Stream = &*Iter;

        int AudioServiceType = mPlayer->PolicyValue(this, Stream, PolicyAudioServiceType);

        switch (AudioServiceType)
        {
        case STM_SE_CTRL_VALUE_AUDIO_SERVICE_SECONDARY:
        case STM_SE_CTRL_VALUE_AUDIO_SERVICE_AUDIO_DESCRIPTION:
            SE_VERBOSE(group_player, "Stream 0x%p - %d - is secondary stream\n", Stream, Stream->GetStreamType());
            OS_UnLockRead(&mStreamListLock);
            return Stream;

        default:
            break;
        }
    }

    OS_UnLockRead(&mStreamListLock);

    return NULL;
}


// //////////////////////////////////////////////////////////////////////////////////////////////////
//
//      Add a stream to an existing playback
//

PlayerStatus_t   PlayerPlayback_c::AddStream(HavanaPlayer_t             HavanaPlayer,
                                             PlayerStream_t            *Stream,
                                             PlayerStreamType_t         StreamType,
                                             stm_se_stream_encoding_t   Encoding,
                                             unsigned int               InstanceId,
                                             HavanaStream_t             HavanaStream,
                                             BufferManager_t            BufferManager,
                                             UserDataSource_t           UserDataSender)
{
    *Stream = NULL;

    if (StreamType == StreamTypeNone)
    {
        SE_ERROR("Stream type must be specified (not StreamTypeNone)\n");
        return PlayerError;
    }

    PlayerStream_c *NewStream = new PlayerStream_c(HavanaPlayer, mPlayer, this, HavanaStream,
                                                   StreamType, Encoding, InstanceId, UserDataSender);
    if (NewStream == NULL)
    {
        SE_ERROR("Unable to allocate new stream structure\n");
        return PlayerInsufficientMemory;
    }

    PlayerStatus_t Status = NewStream->FinalizeInit(BufferManager);
    if (Status != PlayerNoError)
    {
        SE_ERROR("Failed to initialize stream\n");
        delete NewStream;
        return Status;
    }

    RegisterStream(NewStream);

    *Stream = NewStream;

    return PlayerNoError;
}


// //////////////////////////////////////////////////////////////////////////////////////////////////
//
//      Remove a stream from an existing playback
//

PlayerStatus_t   PlayerPlayback_c::RemoveStream(PlayerStream_t Stream)
{
    SE_DEBUG(group_player, "Playback 0x%p Stream 0x%p\n", this, Stream);

    //
    // First drain the stream: blocking mode, discarding remaining data
    //
    PlayerStreamInterface_c::DrainParameters_t   DrainParameters(true, true);
    PlayerStatus_t Status = Stream->Drain(DrainParameters, NULL);
    if (Status != PlayerNoError)
    {
        SE_ERROR("Failed to drain stream\n");
    }

    UnregisterStream(Stream);

    SE_DEBUG(group_player, "Playback 0x%p Stream 0x%p delete\n", this, Stream);
    //
    // Since we blocked in the drain, we should now be able to shutdown the stream cleanly.
    //
    delete Stream;

    //
    // If the playback no longer has any streams, we reset the
    // output coordinator refreshing the registration of the player.
    // Please note that if concurrent stream removal occurs the
    // OutputCoordinator clean up might be executed twice with no consequences.
    OS_LockRead(&mStreamListLock);
    if (mListOfStreams.IsEmpty())
    {
        OutputCoordinator->ResetPrivateState();
        SE_DEBUG(group_player, "Playback 0x%p outputcoordinator reset done (no more streams)\n", this);
    }
    OS_UnLockRead(&mStreamListLock);

    return Status;
}

//
// Register a newly created stream.  Must be called once the stream is fully
// initialized as this make it visible to the rest of the system.
//

void PlayerPlayback_c::RegisterStream(PlayerStream_t Stream)
{
    SE_DEBUG(group_player, "Stream 0x%p - %d\n", Stream, Stream->GetStreamType());
    SE_ASSERT(Stream != NULL);

    OS_LockWrite(&mStreamListLock);

    mListOfStreams.PushBack(*Stream);

    OS_UnLockWrite(&mStreamListLock);
}

//
// Unregister a previously registered stream.  Do nothing if stream was not
// registered.
//

void PlayerPlayback_c::UnregisterStream(PlayerStream_t Stream)
{
    SE_DEBUG(group_player, "Stream 0x%p - %d\n", Stream, Stream->GetStreamType());
    SE_ASSERT(Stream != NULL);

    OS_LockWrite(&mStreamListLock);

    // The stream may not be registered if there was a failure during stream
    // construction before registration point.
    Stream->mLink.UnlinkIfLinked();

    OS_UnLockWrite(&mStreamListLock);
}

void *PlayerPlayback_c::ProcessDrainThread(void *Parameter)
{
    PlayerPlayback_t Playback = (PlayerPlayback_t)Parameter;
    Playback->ProcessDrain();
    OS_TerminateThread();
    return NULL;
}

void PlayerPlayback_c::ProcessDrain()
{
    //
    // Signal we have started
    //
    OS_SetEvent(&mDrainStartStopEvent);
    SE_INFO(group_player, "Starting Playback 0x%p\n", this);

    while (mDrainSignalThreadRunning)
    {
        OS_Status_t WaitStatus;
        do
        {
            WaitStatus = OS_WaitForEventAuto(&GetDrainSignalEvent(), 10000);
        }
        while (WaitStatus == OS_TIMED_OUT);

        OS_ResetEvent(&GetDrainSignalEvent());

        // check we weren't awoken due to shutdown
        if (mDrainSignalThreadRunning)
        {
            //
            // Drain all playing streams, discarding all remaining data
            //
            SE_INFO(group_player, "Playback 0x%p Received event - issuing a drain\n", this);
            PlayerStatus_t Status = InternalDrain(true, false);
            if (Status != PlayerNoError)
            {
                SE_ERROR("Playback 0x%p Failed to drain streams\n", this);
            }
        }
    }

    SE_INFO(group_player, "Terminating Playback 0x%p\n", this);

    OS_Smp_Mb(); // Read memory barrier: rmb_for_Player_Generic_Terminating coupled with: wmb_for_Player_Generic_Terminating
    OS_SetEvent(&mDrainStartStopEvent);
}
