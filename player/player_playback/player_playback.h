/************************************************************************
Copyright (C) 2003-2014 STMicroelectronics. All Rights Reserved.

This file is part of the Streaming Engine.

Streaming Engine is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License version 2 as
published by the Free Software Foundation.

Streaming Engine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with Streaming Engine; see the file COPYING.  If not, write to the Free
Software Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307,
USA.

The Streaming Engine Library may alternatively be licensed under a
proprietary license from ST.
************************************************************************/

#ifndef PLAYER_PLAYBACK_H
#define PLAYER_PLAYBACK_H

#include "player_types.h"
#include "player_playback_interface.h"
#include "player_generic.h"
#include "player_stream.h"
#include "list.h"
#include "coded_configuration.h"

typedef List_c<PlayerStream_c, &PlayerStream_c::mLink> PlayerStreamList_t;

class PlayerPlayback_c: public PlayerPlaybackInterface_c
{
public:
    static PlayerPlayback_c *New(Player_Generic_c *Player, OutputCoordinator_c *OutputCoordinator,
                                 const CodedConfiguration_t &AudioCodedConfiguration,
                                 const CodedConfiguration_t &VideoCodedConfiguration);

    ~PlayerPlayback_c();

    // PlayerPlaybackInterface_c methods
    virtual PlayerPlaybackStatistics_t     &GetStatistics() { return mStatistics; }
    virtual OS_Event_t                     &GetDrainSignalEvent() { return mDrainSignal; }
    virtual bool                            IsLowPowerState() { return mIsLowPowerState; }
    virtual void                            LowPowerEnter() { mIsLowPowerState = true; }
    virtual void                            LowPowerExit()  { mIsLowPowerState = false; }

    //

    PlayerStatus_t InternalDrain(bool Discard, bool ParseAllFrames);
    PlayerStatus_t SetDiscardPts(const TimeStamp_c &DiscardPts);

    PlayerStatus_t Step(PlayerStream_t Stream);
    PlayerStatus_t SetSpeed(int Speed);
    void GetSpeed(int *Speed);
    void GetSpeed(Rational_t *Speed,
                  PlayDirection_t *Direction);

    void ResetStatistics();
    virtual void GetStatistics(__stm_se_playback_statistics_s *Statistics);

    //
    // Mechanisms for managing time
    //

    PlayerStatus_t   SetNativePlaybackTime(unsigned long long NativeTime,
                                           unsigned long long SystemTime            = INVALID_TIME);

    PlayerStatus_t   RetrieveNativePlaybackTime(unsigned long long   *NativeTime,
                                                stm_se_time_format_t  NativeTimeFormat  = TIME_FORMAT_PTS);

    PlayerStatus_t ClockRecoveryDataPoint(stm_se_time_format_t    TimeFormat,
                                          bool                    NewSequence,
                                          unsigned long long    SourceTime,
                                          unsigned long long    LocalTime);
    PlayerStatus_t ClockRecoveryEstimate(unsigned long long   *SourceTime,
                                         unsigned long long   *LocalTime);

    PlayerStream_t GetSecondaryStream();

    PlayerStatus_t   AddStream(HavanaPlayer_t              HavanaPlayer,
                               PlayerStream_t             *Stream,
                               PlayerStreamType_t          StreamType,
                               stm_se_stream_encoding_t    Encoding,
                               unsigned int                InstanceId,
                               HavanaStream_t              HavanaStream,
                               BufferManager_t             BufferManager,
                               UserDataSource_t            UserDataSender);

    PlayerStatus_t   RemoveStream(PlayerStream_t           Stream);

    void RegisterStream(PlayerStream_t Stream);
    void UnregisterStream(PlayerStream_t Stream);

    PlayerPlayback_t      Next;

    OutputCoordinator_t   OutputCoordinator;

    Rational_t            mSpeed;
    PlayDirection_t       mDirection;

    PlayerPolicyState_t   PolicyRecord;

    PlayerControslStorageState_t ControlsRecord;

    CodedConfiguration_t  mAudioCodedConfiguration;
    CodedConfiguration_t  mVideoCodedConfiguration;

private:
    Player_Generic_t      mPlayer;

    /* Data shared with drain process */

    OS_Event_t            mDrainStartStopEvent;
    bool                  mDrainSignalThreadRunning;

    OS_Event_t            mDrainSignal;

    bool                            mIsLowPowerState; // HPS/CPS: indicates when SE is in low power state
    PlayerPlaybackStatistics_t      mStatistics;

    // Protect mListOfStreams.
    //
    // This lock is deadlock-prone because it is held while iterating the list
    // and calling into other SE modules which call back into this class.  The
    // write lock is taken when adding or removing streams only.  The read lock
    // is taken when iterating over all streams.  To avoid deadlocks, while
    // holding the write lock, do not take any other lock that could be held
    // when calling this class.
    //
    // The main sources of deadlocks were:
    //
    // - A thread calls InternalDrain() and waits for all stream to drain and one
    // of the per-stream threads processing the drain tries to acquire this
    // lock to complete the drain.
    //
    // - A thread calls InternalDrain() which calls Collator_c::InputJump() for
    // each stream thus acquiring mStreamListLock first then InputJumpLock.
    // Another thread calls Collator_c::Input() which calls
    // GetSecondaryStream().  If the latter
    // function acquires mStreamListLock, a deadlock occurs because
    // mStreamListLock and InputJumpLock are acquired in reverse order.
    OS_RwLock_t           mStreamListLock;

    PlayerStreamList_t    mListOfStreams;
    bool                  mDataPointReceivedOutOfLiveMode;

    PlayerPlayback_c(Player_Generic_c *Player,
                     OutputCoordinator_c *OutputCoordinator,
                     const CodedConfiguration_t &AudioCodedConfiguration,
                     const CodedConfiguration_t &VideoCodedConfiguration);
    PlayerStatus_t FinalizeInit();

    static void *ProcessDrainThread(void *Parameter);
    void ProcessDrain();

    DISALLOW_COPY_AND_ASSIGN(PlayerPlayback_c);
};

#endif
