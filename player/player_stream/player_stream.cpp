/************************************************************************
Copyright (C) 2003-2014 STMicroelectronics. All Rights Reserved.

This file is part of the Streaming Engine.

Streaming Engine is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License version 2 as
published by the Free Software Foundation.

Streaming Engine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with Streaming Engine; see the file COPYING.  If not, write to the Free
Software Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307,
USA.

The Streaming Engine Library may alternatively be licensed under a
proprietary license from ST.
************************************************************************/

#include "havana_player.h"
#include "player_stream.h"
#include "collate_to_parse_edge.h"
#include "parse_to_decode_edge.h"
#include "decode_to_manifest_edge.h"
#include "post_manifest_edge.h"
#include "es_processor_base.h"
#include "codec_mme_base.h"
#include "decode_buffer_manager_base.h"
#include "video_decoder_params.h"
#include "st_relayfs_se.h"
#include "uncompressed.h"
#include "shared_ptr.h"

#include "eac3_audio.h"
#include "dtshd.h"
#include "dra_audio.h"
#include "aac_audio.h"
#include "wma_audio.h"
#include "vorbis_audio.h"
#include "rma_audio.h"
#include "mpeg_audio.h"
#include "mlp.h"
#include "adpcm.h"
#include "lpcm.h"
#include "pcm_audio.h"
#include "spdifin_audio.h"

#undef TRACE_TAG
#define TRACE_TAG "PlayerStream_c"

#define MAX_AUDIO_DECODE_BUFFERS                32
#define MAX_AUDIO_OUTPUT_CHANNELS               8
#define MAX_AUDIO_DECODED_SAMPLE_COUNT          2048
#define MAX_VIDEO_DECODE_BUFFERS                32
#define DIM_ALIGN 0x3F
// Need to align both width and height to estimate the correct number of available buffers
// For Cannes2.5, HEVC prediction is splitted on 2 HWPE, and needs 4 lines of pixels more for reference (copy) buffers
#define SD_420_BUFFER_SIZE              ((3*BUF_ALIGN_UP(PLAYER_SD_FRAME_WIDTH, DIM_ALIGN)* \
            BUF_ALIGN_UP(PLAYER_SD_FRAME_HEIGHT+BUFFER_HEIGHT_EXTEND, DIM_ALIGN))/2)
#define HD_720P_420_BUFFER_SIZE         ((3*BUF_ALIGN_UP(PLAYER_HD720P_FRAME_WIDTH, DIM_ALIGN) * \
            BUF_ALIGN_UP(PLAYER_HD720P_FRAME_HEIGHT+BUFFER_HEIGHT_EXTEND, DIM_ALIGN))/2)
#define HD_420_BUFFER_SIZE              ((3*BUF_ALIGN_UP(PLAYER_HD_FRAME_WIDTH, DIM_ALIGN) * \
            BUF_ALIGN_UP(PLAYER_HD_FRAME_HEIGHT+BUFFER_HEIGHT_EXTEND, DIM_ALIGN))/2)
#define HD_444_BUFFER_SIZE              (3*BUF_ALIGN_UP(PLAYER_HD_FRAME_WIDTH, DIM_ALIGN) * \
        BUF_ALIGN_UP(PLAYER_HD_FRAME_HEIGHT+BUFFER_HEIGHT_EXTEND, DIM_ALIGN))
#define UHD_4K2K_420_BUFFER_SIZE        ((3*BUF_ALIGN_UP(PLAYER_4K2K_FRAME_WIDTH, DIM_ALIGN) * \
            BUF_ALIGN_UP(PLAYER_4K2K_FRAME_HEIGHT+BUFFER_HEIGHT_EXTEND, DIM_ALIGN))/2)
#define UHD_4K2K_444_BUFFER_SIZE        (3*BUF_ALIGN_UP(PLAYER_4K2K_FRAME_WIDTH, DIM_ALIGN) * \
        BUF_ALIGN_UP(PLAYER_4K2K_FRAME_HEIGHT+BUFFER_HEIGHT_EXTEND, DIM_ALIGN))
#define UHD_420_BUFFER_SIZE             ((3*BUF_ALIGN_UP(PLAYER_UHD_FRAME_WIDTH, DIM_ALIGN) * \
            BUF_ALIGN_UP(PLAYER_UHD_FRAME_HEIGHT+BUFFER_HEIGHT_EXTEND, DIM_ALIGN))/2)
#define UHD_444_BUFFER_SIZE             (3*BUF_ALIGN_UP(PLAYER_UHD_FRAME_WIDTH, DIM_ALIGN) * \
        BUF_ALIGN_UP(PLAYER_UHD_FRAME_HEIGHT+BUFFER_HEIGHT_EXTEND, DIM_ALIGN))
#define PAGE64MB_SIZE                   (64*1024*1024)


PlayerStreamInterface_c::~PlayerStreamInterface_c() {}

PlayerStream_c::PlayerStream_c(
    HavanaPlayer_t          HavanaPlayer,
    Player_Generic_t        player,
    PlayerPlayback_t        Playback,
    HavanaStream_t          HavanaStream,
    PlayerStreamType_t      streamType,
    stm_se_stream_encoding_t Encoding,
    unsigned int            InstanceId,
    UserDataSource_t        UserDataSender)
    : mLink()
    , CollateToParseEdge(NULL)
    , ParseToDecodeEdge(NULL)
    , DecodeToManifestEdge(NULL)
    , PostManifestEdge(NULL)
    , UserDataSender(UserDataSender)
    , ProcessRunningCount(0)
    , StartStopLock()
    , StartStopEvent()
    , BuffersComingOutOfManifestation(false)
    , SwitchStreamLastOneOutOfTheCodec()
    , SwitchingToFrameParser(NULL)
    , SwitchingToCodec(NULL)
    , mSwitchingErrorOccurred(false)
    , mSemaphoreStreamSwitchCollator()
    , mSemaphoreStreamSwitchFrameParser()
    , mSemaphoreStreamSwitchCodec()
    , mSemaphoreStreamSwitchOutputTimer()
    , mSemaphoreStreamSwitchComplete()
    , mCodedConfiguration()
    , CodedFrameMemoryDevice(NULL)
    , TranscodedFrameBufferType(0)
    , CompressedFrameBufferType(0)
    , DecodeBufferType(0)
    , AuxFrameBufferType(0)
    , MDBufferType(0)
    , PolicyRecord()
    , ControlsRecord()
    , InsertionsIntoNonDecodedBuffers(0)
    , RemovalsFromNonDecodedBuffers(0)
    , DisplayIndicesCollapse(0)
    , NonDecodedBuffers()
    , NonDecodedBuffersLock()
    , DecodeCommenceTime(0)
    , FramesToManifestorCount(0)
    , FramesFromManifestorCount(0)
    , mHavanaPlayer(HavanaPlayer)
    , mPlayer(player)
    , mPlayback(Playback)
    , mHavanaStream(HavanaStream)
    , mOutputCoordinator(Playback ? Playback->OutputCoordinator : NULL)
    , mDecodeBufferManager(NULL)
    , mNumberOfDecodeBuffers(0)
    , mMaxSupportedBitDepth(8)
    , mUnPlayable(false)
    , mEncoding(Encoding)
    , mTerminating(false)
    , mStatistics()
    , mAttributes()
    , mLastGetDecodeBufferFormat(FormatUnknown)
    , mCollator(NULL)
    , mEsProcessor(NULL)
    , mStreamType(streamType)
    , mDrained()
    , mInstanceId(InstanceId)
    , mTransformerId(InstanceId & 0x01)
    , mCollatorInputConnected(false)
    , mIsLowPowerState(false)
    , mLowPowerEnterEvent()
    , mLowPowerExitEvent()
    , mMessenger()
    , mCodedFrameBufferPool()
    , mManifestorSharedLock()
    , mDrainLock()
    , mInputLock()
{
    OS_InitializeMutex(&StartStopLock);
    OS_InitializeMutex(&NonDecodedBuffersLock);
    OS_InitializeRwLock(&mManifestorSharedLock);
    OS_InitializeMutex(&mDrainLock);
    OS_InitializeMutex(&mInputLock);

    OS_InitializeEvent(&StartStopEvent);
    OS_InitializeEvent(&mDrained);
    OS_InitializeEvent(&SwitchStreamLastOneOutOfTheCodec);
    OS_InitializeEvent(&mLowPowerEnterEvent);
    OS_InitializeEvent(&mLowPowerExitEvent);

    OS_SemaphoreInitialize(&mSemaphoreStreamSwitchCollator, 0);
    OS_SemaphoreInitialize(&mSemaphoreStreamSwitchFrameParser, 0);
    OS_SemaphoreInitialize(&mSemaphoreStreamSwitchCodec, 0);
    OS_SemaphoreInitialize(&mSemaphoreStreamSwitchOutputTimer, 0);
    OS_SemaphoreInitialize(&mSemaphoreStreamSwitchComplete, 0);
}

#define DEFAULT_RING_SIZE       64

static int ThreadDescIds[4][4] =
{
    { SE_TASK_OTHER, SE_TASK_OTHER, SE_TASK_OTHER, SE_TASK_OTHER},
    { SE_TASK_AUDIO_CTOP, SE_TASK_AUDIO_PTOD, SE_TASK_AUDIO_DTOM, SE_TASK_AUDIO_MPost },
    { SE_TASK_VIDEO_CTOP, SE_TASK_VIDEO_PTOD, SE_TASK_VIDEO_DTOM, SE_TASK_VIDEO_MPost },
    { SE_TASK_OTHER, SE_TASK_OTHER, SE_TASK_OTHER, SE_TASK_OTHER},
};

unsigned int   PlayerStream_c::EstimateCodedFrameMaximumSize(unsigned int CodedPoolSize)
{
    if (CodedPoolSize <= PLAYER_VIDEO_DEFAULT_SD_CODED_MEMORY_SIZE)
    {
        return min(PLAYER_VIDEO_DEFAULT_SD_CODED_FRAME_MAXIMUM_SIZE, CodedPoolSize);
    }
    else if (CodedPoolSize <= PLAYER_VIDEO_DEFAULT_720P_CODED_FRAME_MAXIMUM_SIZE)
    {
        return  min(PLAYER_VIDEO_DEFAULT_720P_CODED_FRAME_MAXIMUM_SIZE, CodedPoolSize);;
    }
    else if (CodedPoolSize <= PLAYER_VIDEO_DEFAULT_HD_CODED_MEMORY_SIZE)
    {
        return min(PLAYER_VIDEO_DEFAULT_HD_CODED_FRAME_MAXIMUM_SIZE, CodedPoolSize);;
    }
    return  min(PLAYER_VIDEO_DEFAULT_4K2K_CODED_FRAME_MAXIMUM_SIZE, CodedPoolSize);;
}

// FinalizeInit
// for non trivial inits to be performed after ctor
PlayerStatus_t   PlayerStream_c::FinalizeInit(BufferManager_t BufferManager)
{
    PlayerStatus_t            Status;

    SE_DEBUG(group_player, "Playback 0x%p Stream 0x%p StreamType %d Encoding %d\n", mPlayback, this, mStreamType, mEncoding);

    HavanaStatus_t HavanaStatus  = mHavanaPlayer->CallFactory(ComponentCollator, mStreamType, mEncoding, (void **)&mCollator);
    if (HavanaStatus != HavanaNoError)
    {
        SE_ERROR("Collator Factory failed\n");
        return PlayerError;
    }

    FrameParser_c              *FrameParser = NULL;
    Codec_c                    *Codec = NULL;
    OutputTimer_c              *OutputTimer = NULL;
    ManifestationCoordinator_c *ManifestationCoordinator = NULL;
    Status = CreateStreamObjects(&FrameParser, &Codec, &OutputTimer, &mDecodeBufferManager, &ManifestationCoordinator);
    if (Status != PlayerNoError) { return Status; }

    // Message subscription init
    MessageStatus_t MessageStatus = mMessenger.Init();
    if (MessageStatus != MessageNoError)
    {
        return PlayerError;
    }

    Status = CreateEsProcessor();
    if (Status != PlayerNoError)
    {
        SE_ERROR("issue when creating EsProcessor\n");
        return Status;
    }

    ResetStatistics();
    ResetAttributes();

    mStatistics.decoder_codec = (unsigned int) mEncoding;

    int MemProfilePolicy = mPlayer->PolicyValue(mPlayback,
                                                PlayerAllStreams, PolicyVideoPlayStreamMemoryProfile);
    mMaxSupportedBitDepth = (is10BitProfileSupported(MemProfilePolicy) ? 10 : 8);

    // For Compo capture / HDMIRX use case , bit buffer is used to store buffer descriptor only
    if (mEncoding == STM_SE_STREAM_ENCODING_VIDEO_UNCOMPRESSED)
    {
        mPlayback->mVideoCodedConfiguration.MemorySize = sizeof(UncompressedBufferDesc_t) *
                                                         mPlayback->mVideoCodedConfiguration.FrameCount;
        mPlayback->mVideoCodedConfiguration.FrameMaximumSize = sizeof(UncompressedBufferDesc_t);
    }
    else
    {
        if (isMemProfileAboveHd(MemProfilePolicy))
        {
            mPlayback->mVideoCodedConfiguration.FrameMaximumSize = PLAYER_VIDEO_DEFAULT_4K2K_CODED_FRAME_MAXIMUM_SIZE;
            mPlayback->mVideoCodedConfiguration.MemorySize = PLAYER_VIDEO_DEFAULT_4K2K_CODED_MEMORY_SIZE;
        }
        else if ((MemProfilePolicy == PolicyValueVideoPlayStreamMemoryProfileHD720p) ||
                 (MemProfilePolicy == PolicyValueVideoPlayStreamMemoryProfileHD720p10Bits))
        {
            mPlayback->mVideoCodedConfiguration.FrameMaximumSize = PLAYER_VIDEO_DEFAULT_720P_CODED_FRAME_MAXIMUM_SIZE;
            mPlayback->mVideoCodedConfiguration.MemorySize = PLAYER_VIDEO_DEFAULT_720P_CODED_MEMORY_SIZE;
        }
        else if (MemProfilePolicy == PolicyValueVideoPlayStreamMemoryProfileSD)
        {
            mPlayback->mVideoCodedConfiguration.FrameMaximumSize = PLAYER_VIDEO_DEFAULT_SD_CODED_FRAME_MAXIMUM_SIZE;
            mPlayback->mVideoCodedConfiguration.MemorySize = PLAYER_VIDEO_DEFAULT_SD_CODED_MEMORY_SIZE;
        }
        else
        {
            mPlayback->mVideoCodedConfiguration.FrameMaximumSize = PLAYER_VIDEO_DEFAULT_HD_CODED_FRAME_MAXIMUM_SIZE;
            mPlayback->mVideoCodedConfiguration.MemorySize = PLAYER_VIDEO_DEFAULT_HD_CODED_MEMORY_SIZE;
        }

        unsigned int DefaultVideoCodedMemorySize = ModuleParameter_GetCodedBitBufferSize();

        if (DefaultVideoCodedMemorySize != 0)
        {
            // Module Parameter value is used only when default Auto profile mode is used
            if (MemProfilePolicy == PolicyValueVideoPlayStreamMemoryProfileAuto)
            {
                mPlayback->mVideoCodedConfiguration.MemorySize = DefaultVideoCodedMemorySize;
                mPlayback->mVideoCodedConfiguration.FrameMaximumSize = EstimateCodedFrameMaximumSize(DefaultVideoCodedMemorySize);
            }
            else
            {
                SE_INFO(group_player, "Module parameter CodedBitBuffeSize ( %d bytes ) is overwritten by Memory Profile Policy %d\n",
                        DefaultVideoCodedMemorySize , MemProfilePolicy);
            }
        }

        unsigned int MaxCodedPoolSize = mPlayer->PolicyValue(mPlayback,
                                                             PlayerAllStreams, PolicyCompressBufferSize);
        if (MaxCodedPoolSize)
        {
            mPlayback->mVideoCodedConfiguration.MemorySize = MaxCodedPoolSize;
            mPlayback->mVideoCodedConfiguration.FrameMaximumSize = EstimateCodedFrameMaximumSize(MaxCodedPoolSize);
        }

        if ((DefaultVideoCodedMemorySize != 0) && (MaxCodedPoolSize != 0))
        {
            SE_INFO(group_player, "Module parameter CodedBitBuffeSize value (%d byte) is overwritten by SE-API setting buffer size to %d bytes\n",
                    DefaultVideoCodedMemorySize , MaxCodedPoolSize);
        }
    }

    SE_INFO(group_player, "Playback 0x%p Stream 0x%p MaxCodedSize=%d bytes MaxSingleBufferSize=%d bytes\n",
            mPlayback, this, mPlayback->mVideoCodedConfiguration.MemorySize,
            mPlayback->mVideoCodedConfiguration.FrameMaximumSize);

    //
    // Get the coded frame parameters
    //
    switch (mStreamType)
    {
    case StreamTypeAudio:
        mCodedConfiguration = mPlayback->mAudioCodedConfiguration;
        break;

    case StreamTypeVideo:
        mCodedConfiguration = mPlayback->mVideoCodedConfiguration;
        break;

    default:
        break;
    }

    //
    // Register the player to the child classes, they will require it early,
    // these function do no work, so cannot return a fail status.
    //
    mCollator->RegisterPlayer(mPlayer, mPlayback, this, mOutputCoordinator);
    FrameParser->RegisterPlayer(mPlayer, mPlayback, this);
    Codec->RegisterPlayer(mPlayer, mPlayback, this);
    OutputTimer->RegisterPlayer(mPlayer, mPlayback, this);
    mDecodeBufferManager->RegisterPlayer(mPlayer, mPlayback, this);
    ManifestationCoordinator->RegisterPlayer(mPlayer, mPlayback, this);

    //
    // Create the buffer pools, and attach the sequence numbers to them.
    //

    Status = CreateCodedFrameBufferPool(BufferManager);
    if (Status != PlayerNoError)
    {
        SE_ERROR("Failed to create the coded buffer pool\n");
        return Status;
    }

    BufferStatus_t BufferStatus = mCodedFrameBufferPool->AttachMetaData(mPlayer->MetaDataSequenceNumberType);
    if (BufferStatus != BufferNoError)
    {
        SE_ERROR("Failed to attach sequence numbers to the coded buffer pool\n");
        return PlayerError;
    }


    if (mDecodeBufferManager->GetDecodeBufferPool() == NULL)
    {
        SE_ERROR("Failed to get the decode buffer pool\n");
        return PlayerError;
    }

    mDecodeBufferManager->GetDecodeBufferPool()->GetType(&DecodeBufferType);

    unsigned int Count;
    mDecodeBufferManager->GetDecodeBufferPool()->GetPoolUsage(&Count, NULL, NULL, NULL, NULL);
    if (Count == 0)
    {
        Count = DEFAULT_RING_SIZE;
    }

    mNumberOfDecodeBuffers = Count;
    if (mNumberOfDecodeBuffers > MAX_DECODE_BUFFERS)
    {
        SE_ERROR("Too many decode buffers - Implementation constant range error\n");
        return PlayerImplementationError;
    }

    //
    // Create the tasks that pass data between components,
    // and provide them with a context in which to operate.
    // NOTE Since we are unsure about the startup, we use a
    // simple delay to shut down the threads if startup is not
    // as expected.
    //
    OS_ResetEvent(&StartStopEvent);
    unsigned int ProcessCreatedCount = 0;
    OS_Status_t  OSStatus;

    CollateToParseEdge = new CollateToParseEdge_c(ThreadDescIds[mStreamType][0], this,
                                                  mCodedConfiguration.FrameCount + PLAYER_MAX_CONTROL_STRUCTURE_BUFFERS,
                                                  FrameParser);
    if (CollateToParseEdge) { OSStatus = CollateToParseEdge->Start(); }
    else { OSStatus = OS_ERROR; }

    if (OSStatus == OS_NO_ERROR)
    {
        ProcessCreatedCount ++;

        ParseToDecodeEdge = new ParseToDecodeEdge_c(ThreadDescIds[mStreamType][1], this,
                                                    mCodedConfiguration.FrameCount + PLAYER_MAX_CONTROL_STRUCTURE_BUFFERS,
                                                    Codec);
        if (ParseToDecodeEdge) { OSStatus = ParseToDecodeEdge->Start(); }
        else { OSStatus = OS_ERROR; }
    }
    if (OSStatus == OS_NO_ERROR)
    {
        ProcessCreatedCount ++;

        DecodeToManifestEdge = new DecodeToManifestEdge_c(ThreadDescIds[mStreamType][2], this,
                                                          mNumberOfDecodeBuffers + PLAYER_MAX_CONTROL_STRUCTURE_BUFFERS,
                                                          ManifestationCoordinator, OutputTimer);
        if (DecodeToManifestEdge) { OSStatus = DecodeToManifestEdge->Start(); }
        else { OSStatus = OS_ERROR; }
    }
    if (OSStatus == OS_NO_ERROR)
    {
        ProcessCreatedCount ++;

        PostManifestEdge = new PostManifestEdge_c(ThreadDescIds[mStreamType][3], this,
                                                  mNumberOfDecodeBuffers + PLAYER_MAX_CONTROL_STRUCTURE_BUFFERS);
        if (PostManifestEdge) { OSStatus = PostManifestEdge->Start();}
        else { OSStatus = OS_ERROR; }
    }
    if (OSStatus == OS_NO_ERROR)
    {
        ProcessCreatedCount ++;
    }

    // Wait for all threads to be running
    unsigned int CurrentProcessRunningCount = 0;
    while (CurrentProcessRunningCount != ProcessCreatedCount)
    {
        OS_Status_t WaitStatus = OS_WaitForEventAuto(&StartStopEvent, PLAYER_MAX_EVENT_WAIT);

        OS_LockMutex(&StartStopLock);
        OS_ResetEvent(&StartStopEvent);
        CurrentProcessRunningCount = ProcessRunningCount;
        OS_UnLockMutex(&StartStopLock);

        if (WaitStatus == OS_TIMED_OUT)
        {
            SE_WARNING("Still waiting for Stream processes to run. Player 0x%p ProcessRunningCount %d (expected %d) status:%d\n",
                       mPlayer, CurrentProcessRunningCount, ProcessCreatedCount, WaitStatus);
        }
    }

    // Terminate all threads if one of the creation had failed
    if (ProcessCreatedCount != PLAYER_STREAM_PROCESS_NB)
    {
        SE_ERROR("Failed to create all stream processes\n");
        return PlayerError;
    }

    //
    // Now exchange the appropriate information between the classes
    //

    Status      = GetEsProcessor()->Connect(CollateToParseEdge->GetInputPort());
    if (Status != PlayerNoError)
    {
        SE_ERROR("Failed to connect CollateToParseEdge InputPort with EsProcessor\n");
        return Status;
    }

    Status      = mCollator->Connect(static_cast<Port_c *>(GetEsProcessor()));
    if (Status != PlayerNoError)
    {
        SE_ERROR("Failed to connect EsProcessor with mCollator\n");
        return Status;
    }

    Status      = FrameParser->Connect(ParseToDecodeEdge->GetInputPort());
    if (Status != PlayerNoError)
    {
        SE_ERROR("Failed to register BufferRing with Frame Parser\n");
        return Status;
    }

    Status      = Codec->Connect(DecodeToManifestEdge->GetInputPort());
    if (Status != PlayerNoError)
    {
        SE_ERROR("Failed to connect with Codec\n");
        return Status;
    }

    Status      = OutputTimer->RegisterOutputCoordinator(mOutputCoordinator);
    if (Status != PlayerNoError)
    {
        SE_ERROR("Failed to register Output Coordinator with Output Timer\n");
        return Status;
    }

    Status  = ManifestationCoordinator->Connect(PostManifestEdge->GetInputPort());
    if (Status != PlayerNoError)
    {
        SE_ERROR("Failed to connect with Manifestor\n");
        return Status;
    }

    SE_DEBUG(group_player, "Playback 0x%p Stream 0x%p added\n", mPlayback, this);
    return PlayerNoError;
}

PlayerStream_c::~PlayerStream_c()
{
    SE_DEBUG(group_player, "Playback 0x%p this 0x%p\n", GetPlayback(), this);

    // Caller responsible for unregistering us.
    SE_ASSERT(mLink.IsLinked() == false);

    //
    // Shut down the processes
    //
    if (PostManifestEdge)
    {
        PostManifestEdge->DiscardUntilDrainMarkerFrame();
    }
    if (DecodeToManifestEdge)
    {
        DecodeToManifestEdge->DiscardUntilDrainMarkerFrame();
    }
    if (ParseToDecodeEdge)
    {
        ParseToDecodeEdge->DiscardUntilDrainMarkerFrame();
    }
    if (CollateToParseEdge)
    {
        CollateToParseEdge->DiscardUntilDrainMarkerFrame();
    }
    if (GetCollator())
    {
        GetCollator()->Halt();
    }
    if (GetFrameParser())
    {
        GetFrameParser()->Halt();
    }
    if (GetCodec())
    {
        GetCodec()->OutputPartialDecodeBuffers();
        GetCodec()->ReleaseReferenceFrame(CODEC_RELEASE_ALL);
        GetCodec()->Halt();
        // following is done to send NULL values (updated value) to respective codecs bug 57576
        // so that already deleted/removed Stream pointer is not dereferenced
        GetCodec()->RegisterPlayer(NULL, NULL, NULL);
    }
    if (GetOutputTimer())
    {
        GetOutputTimer()->Halt();
    }
    if (GetManifestationCoordinator())
    {
        GetManifestationCoordinator()->Halt();
    }
    if (GetDecodeBufferManager())
    {
        GetDecodeBufferManager()->Halt();
    }

    // Ask threads to terminate
    OS_LockMutex(&StartStopLock);
    OS_ResetEvent(&StartStopEvent);
    unsigned int CurrentProcessRunningCount = ProcessRunningCount;
    OS_UnLockMutex(&StartStopLock);

    Terminate();

    if (CollateToParseEdge)
    {
        CollateToParseEdge->Stop();
    }
    if (ParseToDecodeEdge)
    {
        ParseToDecodeEdge->Stop();
    }
    if (DecodeToManifestEdge)
    {
        DecodeToManifestEdge->Stop();
    }
    if (PostManifestEdge)
    {
        PostManifestEdge->Stop();
    }

    // Wait for all threads termination
    SE_DEBUG(group_player, "Playback 0x%p this 0x%p waiting for %d/4 edges to stop\n", GetPlayback(), this, CurrentProcessRunningCount);
    while (CurrentProcessRunningCount != 0)
    {
        OS_Status_t WaitStatus = OS_WaitForEventAuto(&StartStopEvent, 2 * PLAYER_MAX_EVENT_WAIT);

        OS_LockMutex(&StartStopLock);
        OS_ResetEvent(&StartStopEvent);
        CurrentProcessRunningCount = ProcessRunningCount;
        OS_UnLockMutex(&StartStopLock);

        if (WaitStatus == OS_TIMED_OUT)
        {
            SE_WARNING("Still waiting for stream processes to terminate - mPlayer 0x%p ProcessRunningCount %d\n",
                       mPlayer, CurrentProcessRunningCount);
        }
    }

    SE_INFO(group_player, "Playback 0x%p this 0x%p edges stopped (%d)\n", GetPlayback(), this, ProcessRunningCount);

    if (GetOutputTimer())
    {
        GetOutputTimer()->UnregisterOutputCoordinator();
    }

    if (GetDecodeBufferManager())
    {
        GetDecodeBufferManager()->Reset();
    }

    DestroyEsProcessor();

    delete GetFrameParser();
    delete GetCodec();
    delete GetOutputTimer();
    delete GetManifestationCoordinator();

    if (CollateToParseEdge)
    {
        delete CollateToParseEdge;
        CollateToParseEdge = NULL;
    }

    if (ParseToDecodeEdge)
    {
        delete ParseToDecodeEdge;
        ParseToDecodeEdge = NULL;
    }

    if (DecodeToManifestEdge)
    {
        delete DecodeToManifestEdge;
        DecodeToManifestEdge = NULL;
    }

    if (PostManifestEdge)
    {
        delete PostManifestEdge;
        PostManifestEdge = NULL;
    }

    delete mDecodeBufferManager;
    mDecodeBufferManager = NULL;

    // Collator should be destroyed after destroying
    // CodedFrameBufferPool
    delete mCollator;
    mCollator = NULL;

    OS_TerminateMutex(&StartStopLock);
    OS_TerminateMutex(&NonDecodedBuffersLock);
    OS_TerminateRwLock(&mManifestorSharedLock);
    OS_TerminateMutex(&mInputLock);
    OS_TerminateMutex(&mDrainLock);

    OS_TerminateEvent(&StartStopEvent);
    OS_TerminateEvent(&mDrained);
    OS_TerminateEvent(&SwitchStreamLastOneOutOfTheCodec);
    OS_TerminateEvent(&mLowPowerEnterEvent);
    OS_TerminateEvent(&mLowPowerExitEvent);

    OS_SemaphoreTerminate(&mSemaphoreStreamSwitchCollator);
    OS_SemaphoreTerminate(&mSemaphoreStreamSwitchFrameParser);
    OS_SemaphoreTerminate(&mSemaphoreStreamSwitchCodec);
    OS_SemaphoreTerminate(&mSemaphoreStreamSwitchOutputTimer);
    OS_SemaphoreTerminate(&mSemaphoreStreamSwitchComplete);
}

PlayerStatus_t PlayerStream_c::CreateCodedFrameBufferPool(BufferManager_t BufferManager)
{
    unsigned int MemoryAccessType;
    if (mStreamType == StreamTypeAudio)
    {
        MemoryAccessType = MEMORY_AUDIO_ACCESS;
    }
    else
    {
        MemoryAccessType = MEMORY_VIDEO_MEDIA | MEMORY_ICS_UNCACHED_ACCESS | MEMORY_VMA_UNCACHED_ACCESS;
    }

    allocator_status_t AllocStatus = PartitionAllocatorOpen(&CodedFrameMemoryDevice,
                                                            mCodedConfiguration.MemoryPartitionName,
                                                            mCodedConfiguration.MemorySize, MemoryAccessType);
    if (AllocStatus != allocator_ok)
    {
        SE_ERROR("Stream 0x%p Failed to allocate memory Status %d\n", this, AllocStatus);
        return PlayerInsufficientMemory;
    }

    // TODO(theryn): CreatePool() expects a 3-element array even though only two
    // slots are initialized.  For the short term, set the 3rd slot to a neutral value.
    // For the longer term, change CreatePool to expect a (more readable and
    // safer) struct.
    void *CodedFrameMemory[3];
    memset(CodedFrameMemory, 0, sizeof(CodedFrameMemory));
    CodedFrameMemory[CachedAddress]         = AllocatorUserAddress(CodedFrameMemoryDevice);
    CodedFrameMemory[PhysicalAddress]       = AllocatorPhysicalAddress(CodedFrameMemoryDevice);

    CreatePoolInfo_t Info;
    Info.Type = GetCodedFrameBufferType();
    Info.NumberOfBuffers = mCodedConfiguration.FrameCount;
    Info.Size = mCodedConfiguration.MemorySize;
    Info.MemoryPool = &CodedFrameMemory;
    Info.MemoryDevice = CodedFrameMemoryDevice;
    BufferStatus_t BufStatus  = BufferManager->CreatePool(&mCodedFrameBufferPool, Info);
    if (BufStatus != BufferNoError)
    {
        SE_ERROR("Stream 0x%p Failed to create the pool Status %d\n", this, BufStatus);
        return PlayerInsufficientMemory;
    }

    return PlayerNoError;
}

PlayerStatus_t PlayerStream_c::SetAlarm(stm_se_play_stream_alarm_t     alarm,
                                        bool  enable,
                                        void *value)
{
    PlayerStatus_t Status = PlayerNoError;
    switch (alarm)
    {
    case  STM_SE_PLAY_STREAM_ALARM_PTS:
        stm_se_play_stream_pts_and_tolerance_t config;
        config = *(static_cast<stm_se_play_stream_pts_and_tolerance_t *>(value));
        Status = mEsProcessor->SetAlarm(enable, config);
        break;

    default:
        SE_ERROR("Unexpected alarm type received %d\n", alarm);
        return PlayerError;
    }

    return Status;
}

PlayerStatus_t PlayerStream_c::SetDiscardTrigger(stm_se_play_stream_discard_trigger_t const &trigger)
{
    return mEsProcessor->SetDiscardTrigger(trigger);
}

PlayerStatus_t PlayerStream_c::ResetDiscardTrigger()
{
    return mEsProcessor->Reset();
}

FrameParser_t PlayerStream_c::GetFrameParser()
{
    if (CollateToParseEdge) { return CollateToParseEdge->GetFrameParser(); }
    else { return NULL; }
}

Codec_t PlayerStream_c::GetCodec()
{
    if (ParseToDecodeEdge) { return ParseToDecodeEdge->GetCodec(); }
    else { return NULL; }
}

ManifestationCoordinator_t PlayerStream_c::GetManifestationCoordinator()
{
    if (DecodeToManifestEdge) { return DecodeToManifestEdge->GetManifestationCoordinator(); }
    else { return NULL; }
}

OutputTimer_t PlayerStream_c::GetOutputTimer()
{
    if (DecodeToManifestEdge) { return DecodeToManifestEdge->GetOutputTimer(); }
    else { return NULL; }
}

BufferFormat_t PlayerStream_c::ConvertVibeBufferFormatToPlayerBufferFormat(
    stm_pixel_capture_format_t Format)
{
    switch (Format)
    {
    case STM_PIXEL_FORMAT_RGB_8B8B8B_SP:
        return FormatVideo888_RGB;

    case STM_PIXEL_FORMAT_YUV_8B8B8B_SP:
        return FormatVideo444_Raster;

    case STM_PIXEL_FORMAT_YUV_NV12:
        return FormatVideo420_Raster2B;

    case STM_PIXEL_FORMAT_YCbCr422_8B8B8B_DP:
        return FormatVideo422_Raster2B;

    case STM_PIXEL_FORMAT_YCbCr422R:
        return FormatVideo422_Raster;

    case STM_PIXEL_FORMAT_RGB565:
        return FormatVideo565_RGB;

    case STM_PIXEL_FORMAT_ARGB8888:
        return FormatVideo8888_ARGB;

    case STM_PIXEL_FORMAT_RGB_10B10B10B_SP:
    case STM_PIXEL_FORMAT_YCbCr_10B10B10B_SP:
    case STM_PIXEL_FORMAT_YCbCr422_10B10B10B_DP:
        //
        // Defaulting to returning an error for 10-bit formats for
        // now until support is added
        // TODO Add support for 10-bit formats
        //
        SE_ERROR("Request for unsupported (10-bit) decode buffer format (%d)\n", Format);
        return FormatUnknown;

    default:
        SE_ERROR("Request for unsupported decode buffer format (%d)\n", Format);
        return FormatUnknown;
    }
}

SharedPtr_c<BufferPool_c> PlayerStream_c::GetCodedFrameBufferPool(unsigned int *MaximumCodedFrameSize)
{
    if (MaximumCodedFrameSize != NULL)
    {
        *MaximumCodedFrameSize    = mCodedConfiguration.FrameMaximumSize;
    }

    return mCodedFrameBufferPool;
}

// Function indicating that the codec has been halted, and thus the coded frame buffer pool is no more used.
// It intends to be called by PtoD edge during stream switch, to indicate to player that the collator can be switched.
void PlayerStream_c::OnCodecHalted()
{
    // Now that the coded frame buffer pool is unused, we can switch the collator
    // - the attached metadata (start code list, header buffer) cannot be detached as long as the pool is in use
    SwitchCollator(mEncoding);
    OS_SemaphoreSignal(&mSemaphoreStreamSwitchCollator);
}

PlayerStatus_t PlayerStream_c::GetDecodeBuffer(stm_pixel_capture_format_t    Format,
                                               unsigned int                  Width,
                                               unsigned int                  Height,
                                               Buffer_t                      *Buffer,
                                               uint32_t                      *LumaAddress,
                                               uint32_t                      *ChromaOffset,
                                               unsigned int                  *Stride,
                                               bool                          NonBlockingInCaseOfFailure)
{
    DecodeBufferManagerStatus_t  Status;
    SE_VERBOSE(group_player, "\n");

    if ((Buffer == NULL) || (LumaAddress == NULL) ||
        (ChromaOffset == NULL) || (Stride == NULL))
    {
        SE_ERROR("Invalid data\n");
        return PlayerError;
    }

    BufferFormat_t BufferFormat = ConvertVibeBufferFormatToPlayerBufferFormat(Format);
    if (BufferFormat == FormatUnknown) { return PlayerError; }

    if (BufferFormat != mLastGetDecodeBufferFormat)
    {
        if (mLastGetDecodeBufferFormat != FormatUnknown)
        {
            mDecodeBufferManager->EnterStreamSwitch();
        }

        Status = mDecodeBufferManager->InitializeSimpleComponentList(BufferFormat);

        if (Status != DecodeBufferManagerNoError)
        {
            SE_ERROR("Unable to set buffer descriptor\n");
            return PlayerError;
        }

        mLastGetDecodeBufferFormat = BufferFormat;
    }

    // Request the buffer
    DecodeBufferRequest_t   BufferRequest;
    memset(&BufferRequest, 0, sizeof(DecodeBufferRequest_t));
    BufferRequest.DimensionCount  = 2;
    BufferRequest.Dimension[0]    = Width;
    BufferRequest.Dimension[1]    = Height;
    BufferRequest.NonBlockingInCaseOfFailure     = NonBlockingInCaseOfFailure;

    // hdmirx/dvp capture use case
    // |input                 | hdimrx/dvp capture profile                | buffer pool size |
    // |----------------------|-------------------------------------------|------------------|
    // |HD ready, full HD     | NoAudioDecode (pass through) / NoVideoFrc |7                 |
    // |UHD                   | NoAudioDecode (pass through) / NoVideoFrc |7                 |
    // |HD ready, full HD     | AudioDecode / VideoFrc                    |12                |
    // |UHD                   | AudioDecode / VideoFrc                    |7                 |
    // |HD ready, full HD     | NoAudioDecode (pass through) / VideoFrc   |12                |
    // |UHD                   | NoAudioDecode (pass through) / VideoFrc   |7                 |

    // Compo Capture use case
    // |input                 | compo capture profile                     | buffer pool size |
    // |----------------------|-------------------------------------------|------------------|
    // |All                   | NoAudio / VideoFrc                        |10                |
    // |All                   | NoAudio / NoVideoFrc                      |7                 |

    int CaptureProfile = mPlayer->PolicyValue(mPlayback, this, PolicyCaptureProfile);
    unsigned int Size = Width * Height;
    switch (CaptureProfile)
    {
    case PolicyValueCaptureProfileHdmiRxNoAudioDecodeNoVideoFrc:
    case PolicyValueCaptureProfileCompoNoAudioNoVideoFrc:
        BufferRequest.ManifestationBufferCount = 7;
        break;

    case PolicyValueCaptureProfileCompoNoAudioVideoFrc:
        BufferRequest.ManifestationBufferCount = 10;
        break;

    case PolicyValueCaptureProfileHdmiRxAudioDecodeVideoFrc:
    case PolicyValueCaptureProfileHdmiRxAudioDecodeNoVideoFrc:
    case PolicyValueCaptureProfileHdmiRxNoAudioDecodeVideoFrc:
        if (Size > (PLAYER_HD_FRAME_WIDTH * PLAYER_HD_FRAME_HEIGHT)) { BufferRequest.ManifestationBufferCount = 7; }
        else { BufferRequest.ManifestationBufferCount = 12; }
        break;

    case PolicyValueCaptureProfileDisabled:
    default:
        BufferRequest.ManifestationBufferCount = 9;
        break;
    }

    BufferRequest.ReferenceBufferCount = 0;

    // Dealing with 64MB HW alignement issue, need one more buffer per 64MB border crossing
    if (!ModuleParameter_Support64MbyteCrossing())
    {
        BufferRequest.ManifestationBufferCount += 1 + (Size * 3 / 2 * BufferRequest.ManifestationBufferCount) / PAGE64MB_SIZE;
    }

    // get buffer can fail if sink did not return its buffer on time
    // may happen on HDMIRX cases on resolution change
    unsigned int t = 0;

    while (t < MAX_GET_BUFFER_WAITING_TIME_MS)
    {
        Status = mDecodeBufferManager->GetDecodeBuffer(&BufferRequest, Buffer);
        if (Status == DecodeBufferManagerNoError) { break; }
        OS_SleepMilliSeconds(GET_BUFFER_WAITING_PERIOD_MS);
        t += GET_BUFFER_WAITING_PERIOD_MS;
        SE_DEBUG(group_player, "Retry to get decode buffer\n");
    }

    if (Status == DecodeBufferManagerFailedToAllocateComponents)
    {
        SE_ERROR("Stream 0x%p failed for dbm components allocation - marking stream unplayable\n", this);
        // raise the event to signal stream unplayable
        MarkUnPlayable(STM_SE_PLAY_STREAM_MSG_REASON_CODE_INSUFFICIENT_MEMORY, true);
        return PlayerError;
    }

    *LumaAddress      = (uint32_t)mDecodeBufferManager->ComponentBaseAddress(*Buffer, PrimaryManifestationComponent);
    *ChromaOffset     = mDecodeBufferManager->Chroma(*Buffer, PrimaryManifestationComponent) -
                        mDecodeBufferManager->Luma(*Buffer, PrimaryManifestationComponent);
    *Stride           = mDecodeBufferManager->ComponentStride(*Buffer, PrimaryManifestationComponent, 0, 0);

    // Change the ownership so that we can track the buffer through the system
    (*Buffer)->TransferOwnership(IdentifierExternal);

    return PlayerNoError;
}

PlayerStatus_t PlayerStream_c::ReturnDecodeBuffer(Buffer_t Buffer)
{
    BufferStatus_t      Status;

    Status = mDecodeBufferManager->ReleaseBuffer(Buffer, false);

    if (Status != BufferNoError)
    {
        SE_ERROR("Stream 0x%p Failed to release buffer\n", this);
        return PlayerError;
    }

    return PlayerNoError;
}

PlayerStatus_t PlayerStream_c::CreateEsProcessor()
{
    PlayerStatus_t Status;

    if (mEsProcessor != NULL)
    {
        SE_ERROR("EsProcessor already created\n");
        return PlayerError;
    }

    mEsProcessor = new ES_Processor_Base_c();
    if (mEsProcessor == NULL)
    {
        SE_ERROR("no memory to create EsProcessor\n");
        return PlayerInsufficientMemory;
    }

    Status = mEsProcessor->FinalizeInit(this);
    if (Status != PlayerNoError)
    {
        SE_ERROR("EsProcessor initialization failed %d\n", Status);
        delete mEsProcessor;
        mEsProcessor = NULL;
    }

    return Status;
}

void PlayerStream_c::DestroyEsProcessor()
{
    delete mEsProcessor;
    mEsProcessor = NULL;
}

// //////////////////////////////////////////////////////////////////////////////////////////////////
//
//      Switch stream component function for the collator
//
PlayerStatus_t PlayerStream_c::SwitchCollator(stm_se_stream_encoding_t Encoding)
{
    SE_DEBUG(group_player, "Stream 0x%p Encoding %d\n", this, Encoding);

    Collator_c *NewCollator = NULL;
    HavanaStatus_t HavanaStatus = mHavanaPlayer->CallFactory(ComponentCollator, mStreamType, Encoding, (void **)&NewCollator);
    if (HavanaStatus != HavanaNoError)
    {
        SE_ERROR("Collator Factory failed\n");
        return PlayerError;
    }

    // Halt the current collator;  will be destroyed during stream switch
    mCollator->Halt();

    // Switch over to the new collator
    NewCollator->RegisterPlayer(mPlayer, mPlayback, this, mOutputCoordinator);
    mCollator = NewCollator;

    // Connect the ES processor
    PlayerStatus_t Status = mCollator->Connect(static_cast<Port_c *>(GetEsProcessor()));
    if (Status != PlayerNoError)
    {
        SE_ERROR("Stream 0x%p Failed to connect to EsProcessor\n", this);
        return PlayerError;
    }

    if (mCollatorInputConnected)
    {
        Status = mCollator->ConnectInput();
        if (Status != PlayerNoError)
        {
            SE_ERROR("Stream 0x%p Failed to connect collator input\n", this);
            return PlayerError;
        }
    }

    SE_DEBUG(group_player, "Stream 0x%p completed\n", this);
    return PlayerNoError;
}


PlayerStatus_t   PlayerStream_c::InsertMarkerFrame(MarkerType_t markerType, unsigned long long *sequenceNumber)
{
    //
    // Insert a marker frame into the processing ring
    //
    Buffer_t                  MarkerFrame;
    BufferStatus_t BufStatus = mCodedFrameBufferPool->GetBuffer(&MarkerFrame, IdentifierDrain, 0);
    if (BufStatus != BufferNoError)
    {
        SE_ERROR("Stream 0x%p Unable to obtain a marker frame error %d\n", this, BufStatus);
        return PlayerError;
    }

    CodedFrameParameters_t   *CodedFrameParameters;
    MarkerFrame->ObtainMetaDataReference(mPlayer->MetaDataCodedFrameParametersType, (void **)(&CodedFrameParameters));
    SE_ASSERT(CodedFrameParameters != NULL);

    memset(CodedFrameParameters, 0, sizeof(CodedFrameParameters_t));

    // StartCode list is not used, but Parser requires to have it, so allocate minimal size
    // and want to avoid to impact parsers.

    BufStatus = MarkerFrame->AttachMetaData(mPlayer->MetaDataStartCodeListType, 1);
    if (BufStatus != BufferNoError)
    {
        SE_ERROR("Stream 0x%p Unable to attach start code list metadata marker %d\n", this, BufStatus);
        MarkerFrame->DecrementReferenceCount(IdentifierDrain);
        return PlayerError;
    }

    PlayerSequenceNumber_t   *SequenceNumberStructure;
    MarkerFrame->ObtainMetaDataReference(mPlayer->MetaDataSequenceNumberType, (void **)(&SequenceNumberStructure));
    SE_ASSERT(SequenceNumberStructure != NULL);
    memset(SequenceNumberStructure, 0, sizeof(*SequenceNumberStructure));

    SequenceNumberStructure->mIsMarkerFrame = true;
    SequenceNumberStructure->mMarkerFrame.mMarkerType  = markerType;

    SE_DEBUG(group_player, "Stream 0x%p insert Marker Frame type %d\n", this, markerType);

    CollateToParseEdge->GetInputPort()->Insert((uintptr_t)MarkerFrame);

    *sequenceNumber = SequenceNumberStructure->Value;

    return PlayerNoError;
}

// //////////////////////////////////////////////////////////////////////////////////////////////////
//
//      Drain the decode chain for a stream in a playback
//      This method must be exclusive with Switch() to prevent any concurrent marker frame injection
//

PlayerStatus_t PlayerStream_c::Drain(const DrainParameters_t &DrainParameters, unsigned long long *pDrainSequenceNumber)
{
    OS_LockMutex(&mDrainLock);
    InitiateDrain(DrainParameters);
    PlayerStatus_t Status = CompleteDrain(DrainParameters, pDrainSequenceNumber);
    OS_UnLockMutex(&mDrainLock);
    return (Status);
}

// //////////////////////////////////////////////////////////////////////////////////////////////////
//
//      Internal methods to be called for executing a drain
//      The caller must ensure to have locked the mDrainLock mutex
//

// Perform first phase of drain process.
//
// Instruct all edges to discard buffers until reception of a drain marker.
//
// This phase ensures that threads blocked in BufferPool_c::GetBuffer() if any
// will wake up.
//
// CompleteDrain() must be called after this function.
void PlayerStream_c::InitiateDrain(const DrainParameters_t &DrainParameters)
{
    OS_AssertMutexHeld(&mDrainLock);

    // We can be called while an injection thread has run out of buffers and is
    // blocked in GetBuffer() while holding mInputLock so we must not take this
    // lock here.
    OS_AssertMutexNotHeld(&mInputLock);

    SE_DEBUG(group_player, "Playback 0x%p Stream 0x%p Blocking %d Discard %d\n",
             mPlayback, this, DrainParameters.Blocking, DrainParameters.Discard);

    st_relayfs_write_se(
        this->GetStreamType() == StreamTypeAudio ?
        ST_RELAY_TYPE_PES_AUDIO_WITH_API : ST_RELAY_TYPE_PES_VIDEO_WITH_API,
        ST_RELAY_SOURCE_SE, (unsigned char *)(__func__), strnlen(__func__, 512), true);

    // If speed is 0, first wait for any frame queued by a step call to be displayed
    if (mPlayback->mSpeed == 0)
    {
        DecodeToManifestEdge->WaitForFrozenSurface();
    }

    //
    // If we are to discard data in the drain, then perform the flushing
    //
    if (DrainParameters.Discard)
    {
        PostManifestEdge->DiscardUntilDrainMarkerFrame();
        DecodeToManifestEdge->DiscardUntilDrainMarkerFrame();
        ParseToDecodeEdge->DiscardUntilDrainMarkerFrame();
        if (!DrainParameters.ParseAllFrames)
        {
            CollateToParseEdge->DiscardUntilDrainMarkerFrame();
        }

        // Ensure anyone waiting for buffers is unblocked
        DecodeToManifestEdge->ReleaseQueuedDecodeBuffers(DrainParameters.ReleaseAllBuffers);
    }
}

// Perform second phase of drain process.
//
// Send a drain marker in the pipe and wait for completion if
// DrainParameters_t::Blocking set.
//
// InitiateDrain() must be called before this function.
PlayerStatus_t PlayerStream_c::CompleteDrain(const DrainParameters_t &DrainParameters, unsigned long long *pDrainSequenceNumber)
{
    OS_AssertMutexHeld(&mDrainLock);

    SE_DEBUG(group_player, "Playback 0x%p Stream 0x%p Blocking %d Discard %d\n",
             mPlayback, this, DrainParameters.Blocking, DrainParameters.Discard);

    //
    // If we are to discard data in the drain, then perform the flushing
    //
    PlayerStatus_t Status;
    if (DrainParameters.Discard)
    {
        Status  = GetCollator()->InputJump(STM_SE_DISCONTINUITY_SURPLUS_DATA);
        ParseToDecodeEdge->CallInSequence(SequenceTypeImmediate, TIME_NOT_APPLICABLE, CodecFnDiscardQueuedDecodes);
        ParseToDecodeEdge->CallInSequence(SequenceTypeImmediate, TIME_NOT_APPLICABLE, CodecFnOutputPartialDecodeBuffers);
    }
    else
    {
        Status  = GetCollator()->InputJump(STM_SE_DISCONTINUITY_CONTINUOUS);   // This call will flush the already parsed input
    }

    //
    // Reset the event indicating draining and insert the marker into the flow
    //
    OS_ResetEvent(&mDrained);

    unsigned long long  DrainSequenceNumber;
    Status = InsertMarkerFrame(DrainParameters.Discard ? DrainDiscardMarker : DrainPlayOutMarker, &DrainSequenceNumber);
    if (Status != PlayerNoError) { return Status; }

    SE_INFO(group_player, "Playback 0x%p Stream 0x%p Marker %s #%lld inserted\n", mPlayback, this,
            DrainParameters.Discard ? "DrainDiscardMarker" : "DrainPlayOutMarker", DrainSequenceNumber);

    if (pDrainSequenceNumber != NULL)
    {
        *pDrainSequenceNumber = DrainSequenceNumber;
    }

    //
    // Issue an in sequence synchronization reset
    //
    Status  = DecodeToManifestEdge->CallInSequence(SequenceTypeBeforeSequenceNumber, DrainSequenceNumber, OutputTimerFnResetTimeMapping, PlaybackContext);
    if (Status != PlayerNoError)
    {
        SE_ERROR("Failed to issue call to reset synchronization\n");
        return Status;
    }

    //
    // Queue the setting of the internal event, when the stream is drained
    // this allows us to commence multiple stream drains in a playback shutdown
    // and then block on completion of them all.
    //
    Status      = PostManifestEdge->CallInSequence(SequenceTypeAfterSequenceNumber, DrainSequenceNumber, OSFnSetEventOnPostManifestation, &mDrained);

    if (Status != PlayerNoError)
    {
        SE_ERROR("Failed to request OS_SetEvent\n");
        return Status;
    }

    //
    // Are we a blocking/non-blocking call
    //
    if (DrainParameters.Blocking)
    {
        Status  = WaitForDrainCompletion(DrainParameters.Discard);

        if (Status != PlayerNoError)
        {
            SE_ERROR("Playback 0x%p Stream 0x%p Failed to drain within allowed time (%d %d %d %d)\n",
                     mPlayback, this,
                     CollateToParseEdge->IsDiscardingUntilDrainMarkerFrame(), ParseToDecodeEdge->IsDiscardingUntilDrainMarkerFrame(),
                     DecodeToManifestEdge->IsDiscardingUntilDrainMarkerFrame(), PostManifestEdge->IsDiscardingUntilDrainMarkerFrame());
            return PlayerTimedOut;
        }

        SE_INFO(group_player, "Playback 0x%p Stream 0x%p drained within allowed time (%d %d %d %d)\n",
                mPlayback, this,
                CollateToParseEdge->IsDiscardingUntilDrainMarkerFrame(), ParseToDecodeEdge->IsDiscardingUntilDrainMarkerFrame(),
                DecodeToManifestEdge->IsDiscardingUntilDrainMarkerFrame(), PostManifestEdge->IsDiscardingUntilDrainMarkerFrame());

        //
        // If this was a playout drain, then check when the last
        // queued frame will complete and wait for it to do so.
        //
        if (!DrainParameters.Discard)
        {
            //
            // The last frame will playout when the next frame could be displayed
            //
            unsigned long long  PlayoutTime;
            Status  = GetManifestationCoordinator()->GetLastQueuedManifestationTime(&PlayoutTime);

            if ((Status == ManifestationCoordinatorNoError) && (mPlayback->mSpeed != 0))
            {
                unsigned long long  Delay = (PlayoutTime - OS_GetTimeInMicroSeconds()) / 1000;

                if (inrange(Delay, 1, UnsignedLongLongIntegerPart(PLAYER_MAX_PLAYOUT_TIME / mPlayback->mSpeed)))
                {
                    SE_INFO(group_player, "Playback 0x%p Stream 0x%p Delay to manifest last frame is %lldms\n", mPlayback, this, Delay);
                    OS_SleepMilliSeconds((unsigned int)Delay);
                }
            }
        }
    }

    SE_DEBUG(group_player, "Playback 0x%p Stream 0x%p completed\n", mPlayback, this);
    return PlayerNoError;
}

// //////////////////////////////////////////////////////////////////////////////////////////////////
//
//      Drain the decode chain for a stream in a playback
//

unsigned int   PlayerStream_c::WaitForDrainCompletion(bool Discard)
{
    unsigned int    IndividualWaitTime;
    OS_Status_t WaitStatus = OS_NO_ERROR;

    if (Discard)
    {
        WaitStatus = OS_WaitForEventAuto(&mDrained, PLAYER_MAX_DISCARD_DRAIN_TIME);
        if (WaitStatus == OS_TIMED_OUT)
        {
            SE_WARNING("Stream 0x%p wait for drain completion timed out - skipping\n", this);
        }
    }
    else
    {
        if (mPlayback->mSpeed != 0)
        {
            IndividualWaitTime = UnsignedIntegerPart(PLAYER_MAX_TIME_ON_DISPLAY / mPlayback->mSpeed);

            do
            {
                BuffersComingOutOfManifestation = false;
                WaitStatus = OS_WaitForEventAuto(&mDrained, IndividualWaitTime);
            }
            while ((WaitStatus == OS_TIMED_OUT) && BuffersComingOutOfManifestation);
        }
    }

    return (WaitStatus == OS_NO_ERROR) ? PlayerNoError : PlayerTimedOut;
}

PlayerStatus_t   PlayerStream_c::CreateStreamObjects(
    FrameParser_c **FrameParser,
    Codec_c **Codec,
    OutputTimer_c **OutputTimer,
    DecodeBufferManager_c **DecodeBufferManager,
    ManifestationCoordinator_c **ManifestationCoordinator)
{
    if (FrameParser != NULL)
    {
        HavanaStatus_t Status = mHavanaPlayer->CallFactory(ComponentFrameParser, mStreamType, mEncoding, (void **) FrameParser);
        if (Status != HavanaNoError)
        {
            SE_ERROR("FrameParser Factory failed\n");
            return PlayerError;
        }
    }

    if (Codec != NULL)
    {
        CodecParameterBlock_t   CodecParameters;
        CodecStatus_t           CodecStatus;

        HavanaStatus_t Status = mHavanaPlayer->CallFactory(ComponentCodec, mStreamType, mEncoding, (void **) Codec);
        if (Status != HavanaNoError)
        {
            SE_ERROR("Codec Factory failed\n");
            return PlayerError;
        }

        CodecParameters.ParameterType       = CodecSelectTransformer;
        CodecParameters.Transformer         = mTransformerId;

        CodecStatus = (*Codec)->SetModuleParameters(sizeof(CodecParameterBlock_t), &CodecParameters);
        if (CodecStatus != CodecNoError)
        {
            SE_ERROR("Failed to set codec parameters (%08x)\n", CodecStatus);
            return PlayerError;
        }

        SE_DEBUG(group_player, "Stream 0x%p Selecting transformer %d for %s stream instance %d\n", this, CodecParameters.Transformer,
                 (mStreamType == StreamTypeAudio ? "audio" : (mStreamType == StreamTypeVideo ? "video" : "other")), mInstanceId);
    }

    if (OutputTimer != NULL)
    {
        HavanaStatus_t Status = mHavanaPlayer->CallFactory(ComponentOutputTimer, mStreamType,
                                                           mStreamType == StreamTypeAudio ? STM_SE_STREAM_ENCODING_AUDIO_NONE : STM_SE_STREAM_ENCODING_VIDEO_NONE,
                                                           (void **) OutputTimer);
        if (Status != HavanaNoError)
        {
            SE_ERROR("Codec Factory failed\n");
            return PlayerError;
        }
    }

    if (DecodeBufferManager != NULL)
    {
        HavanaStatus_t Status  = mHavanaPlayer->CallFactory(ComponentDecodeBufferManager, mStreamType,
                                                            mStreamType == StreamTypeAudio ? STM_SE_STREAM_ENCODING_AUDIO_NONE : STM_SE_STREAM_ENCODING_VIDEO_NONE,
                                                            (void **) DecodeBufferManager);
        if (Status != HavanaNoError)
        {
            SE_ERROR("DecodeBufferManager Factory failed\n");
            return PlayerError;
        }

        if (Codec != NULL)
        {
            CheckDisableUnsupportedDecimation(*Codec);
        }

        PlayerStatus_t PlayerStatus = PlayerError;
        if (mStreamType == StreamTypeVideo)
        {
            PlayerStatus = ConfigureDecodeBufferManagerForVideo();
        }
        else if (mStreamType == StreamTypeAudio)
        {
            PlayerStatus = ConfigureDecodeBufferManagerForAudio();
        }
        if (PlayerStatus != PlayerNoError) { return PlayerStatus; }
    }

    if (ManifestationCoordinator != NULL)
    {
        HavanaStatus_t Status = mHavanaPlayer->CallFactory(ComponentManifestationCoordinator, mStreamType,
                                                           mStreamType == StreamTypeAudio ? STM_SE_STREAM_ENCODING_AUDIO_NONE : STM_SE_STREAM_ENCODING_VIDEO_NONE,
                                                           (void **) ManifestationCoordinator);
        if (Status != HavanaNoError)
        {
            SE_ERROR("ManifestationCoordinator Factory failed\n");
            return PlayerError;
        }
    }

    return PlayerNoError;
}

// //////////////////////////////////////////////////////////////////////////////////////////////////
//
//      Switch one stream in a playback for an alternative one
//
PlayerStatus_t   PlayerStream_c::Switch(stm_se_stream_encoding_t  Encoding)
{
    mSwitchingErrorOccurred = false;

    OS_LockMutex(&mDrainLock);

    SE_INFO(group_player, "Stream 0x%p current Encoding %d, new encoding %d\n", this, mEncoding, Encoding);
    unsigned int SwitchStartTime = OS_GetTimeInMilliSeconds();

    Codec_c *PreviousCodec              = GetCodec();
    FrameParser_c *PreviousFrameParser  = GetFrameParser();
    Collator_c *PreviousCollator        = GetCollator();

    st_relayfs_write_se(
        this->GetStreamType() == StreamTypeAudio ?
        ST_RELAY_TYPE_PES_AUDIO_WITH_API : ST_RELAY_TYPE_PES_VIDEO_WITH_API,
        ST_RELAY_SOURCE_SE, (unsigned char *)(__func__), strnlen(__func__, 512), true);

    // Initiate drain by discarding accumulated buffers.  This will wake up
    // edges sleeping because stream paused or blocked in
    // BufferPool_c::GetBuffer().  We must not take mInputLock here or we could
    // deadlock with an injection thread in the following case:
    //
    // - Stream paused (SetSpeed(0)) causing buffers to accumulate in DtoM edge.
    //
    // - Injection thread running out of buffers blocked in GetBuffer() while
    // holding mInputLock.

    unsigned long long  DrainSequenceNumber;
    DrainParameters_t   DrainParameters(false, true);
    DrainParameters.ReleaseAllBuffers = true;
    InitiateDrain(DrainParameters);

    // Receiving data after the drain marker while edges are resetting their
    // state (e.g. destructing and recreating pools) would wreak havoc so lock
    // injection.
    OS_LockMutex(&mInputLock);

    // Generate and send a non-blocking drain marker.
    PlayerStatus_t Status  = CompleteDrain(DrainParameters, &DrainSequenceNumber);
    if (Status != PlayerNoError)
    {
        SE_ERROR("Stream 0x%p Failed to drain stream\n", this);
        OS_UnLockMutex(&mInputLock);
        OS_UnLockMutex(&mDrainLock);
        return PlayerError;
    }

    // Re-initialize clock recovery
    // We dont care about the time format used here as the clock recovery will be
    // re-initialized again when we get the first clock data point of the new stream
    // What we want here is to avoid a clock data point of the previous stream to be
    // used for the time mapping of the new stream
    if ((Encoding == STM_SE_STREAM_ENCODING_AUDIO_MPEG2_SYSB) ||
        (Encoding == STM_SE_STREAM_ENCODING_VIDEO_MPEG2_SYSB))
    {
        mOutputCoordinator->ClockRecoveryInitialize(TIME_FORMAT_27MHz);
    }
    else
    {
        mOutputCoordinator->ClockRecoveryInitialize(TIME_FORMAT_PTS);
    }

    OS_ResetEvent(&SwitchStreamLastOneOutOfTheCodec);

    // The PostManifestEdge->CallInSequence() call must be done with SequenceTypeAfterSequenceNumber parameter.
    // This is to ensure that the drain marker frame will be released before SwitchComplete() is called.
    // Otherwise we would remain forever blocked in SwitchCodec() function, that is waiting for all
    // decode buffers to be freed.
    Status = PostManifestEdge->CallInSequence(SequenceTypeAfterSequenceNumber, DrainSequenceNumber, PlayerFnSwitchComplete, this);
    if (Status != PlayerNoError)
    {
        SE_ERROR("Stream 0x%p Failed CallInSequence for PostManifest:- PlayerFnSwitchComplete\n", this);
        OS_UnLockMutex(&mInputLock);
        OS_UnLockMutex(&mDrainLock);
        return PlayerError;
    }

    ResetDiscardTrigger();

    // By default, the new stream is considered as unsecure
    mEncoding = Encoding;
    mStatistics.decoder_codec = (unsigned int) mEncoding;

    Status = CreateStreamObjects(&SwitchingToFrameParser, &SwitchingToCodec, NULL, NULL, NULL);
    if (Status != PlayerNoError)
    {
        SE_ERROR("Stream 0x%p Failed to create stream objects\n", this);
        OS_UnLockMutex(&mInputLock);
        OS_UnLockMutex(&mDrainLock);
        return PlayerError;
    }
    Status = CollateToParseEdge->CallInSequence(SequenceTypeAfterSequenceNumber, DrainSequenceNumber, PlayerFnSwitchFrameParser, this);
    if (Status != PlayerNoError)
    {
        SE_ERROR("Stream 0x%p Failed CallInSequence for CollateToParse:- PlayerFnSwitchFrameParser\n", this);
        OS_UnLockMutex(&mInputLock);
        OS_UnLockMutex(&mDrainLock);
        return PlayerError;
    }
    Status = ParseToDecodeEdge->CallInSequence(SequenceTypeAfterSequenceNumber, DrainSequenceNumber, PlayerFnSwitchCodec,       this);
    if (Status != PlayerNoError)
    {
        SE_ERROR("Stream 0x%p Failed CallInSequence for ParseToDecode:- PlayerFnSwitchCodec\n", this);
        OS_UnLockMutex(&mInputLock);
        OS_UnLockMutex(&mDrainLock);
        return PlayerError;
    }
    Status = DecodeToManifestEdge->CallInSequence(SequenceTypeAfterSequenceNumber, DrainSequenceNumber, PlayerFnSwitchOutputTimer, this);
    if (Status != PlayerNoError)
    {
        SE_ERROR("Stream 0x%p Failed CallInSequence for DecodeToManifest:- PlayerFnSwitchOutputTimer\n", this);
        OS_UnLockMutex(&mInputLock);
        OS_UnLockMutex(&mDrainLock);
        return PlayerError;
    }
    // Reset mUnPlayable flag else we'll discard new data after the switch if previous stream was marked Unplayable
    mUnPlayable = false;

    // Wait for all edges to complete switch processing before deleting old
    // stage objects.
    OS_SemaphoreWaitAuto(&mSemaphoreStreamSwitchCollator);
    OS_SemaphoreWaitAuto(&mSemaphoreStreamSwitchFrameParser);
    OS_SemaphoreWaitAuto(&mSemaphoreStreamSwitchCodec);
    OS_SemaphoreWaitAuto(&mSemaphoreStreamSwitchOutputTimer);
    OS_SemaphoreWaitAuto(&mSemaphoreStreamSwitchComplete);

    //
    // Signal event that we've finished Stream Switch() request
    //
    PlayerEventRecord_t   Event;
    Event.Code              = EventStreamSwitched;
    Event.Playback          = GetPlayback();
    Event.Stream            = this;
    Event.PlaybackTime      = TIME_NOT_APPLICABLE;
    Event.UserData          = NULL;

    Status = SignalEvent(&Event);
    if (Status != PlayerNoError)
    {
        SE_ERROR("Stream 0x%p Failed to issue event signal of stream switch complete\n", this);
    }

    // do not delete collator before because some buffer may be not yet returned (StartCodeHeaderBuffer)
    SE_DEBUG(group_player, "Stream 0x%p Deleting previous collator:%p\n", this, PreviousCollator);
    delete PreviousCollator;

    SE_DEBUG(group_player, "Stream 0x%p Deleting previous frame_parser:%p\n", this, PreviousFrameParser);
    delete PreviousFrameParser;

    SE_DEBUG(group_player, "Stream 0x%p Deleting previous codec:%p\n", this, PreviousCodec);
    delete PreviousCodec;

    if (mSwitchingErrorOccurred)
    {
        // some fatal switching error occured, mark stream unplayable but not signal it
        MarkUnPlayable(STM_SE_PLAY_STREAM_MSG_REASON_CODE_STREAM_SWITCH, true, true);
        OS_UnLockMutex(&mInputLock);
        OS_UnLockMutex(&mDrainLock);
        return PlayerError;
    }

    unsigned int SwitchStopTime = OS_GetTimeInMilliSeconds();
    SE_INFO(group_player, "Stream 0x%p - %s switch completed %s in %d ms\n", this,
            (mStreamType == StreamTypeAudio) ? "Audio" : "Video",
            mSwitchingErrorOccurred ? "KO" : "OK",
            SwitchStopTime - SwitchStartTime);

    OS_UnLockMutex(&mInputLock);
    OS_UnLockMutex(&mDrainLock);

    return PlayerNoError;
}

// //////////////////////////////////////////////////////////////////////////////////////////////////
//
//      A function to allow component to mark a stream as unplayable
//

void PlayerStream_c::MarkUnPlayable(unsigned int reason, bool force, bool noEvent)
{
    SE_DEBUG(group_player, "Stream 0x%p\n", this);

    // First check if we should just ignore this call -- unless forced
    // if (force == false) => for the time being, discard this directive
    if (force == true)
    {
        SE_INFO(group_player, "Stream 0x%p Ignoring force=true status\n", this);
    }

    {
        int Ignore = mPlayer->PolicyValue(mPlayback, this, PolicyIgnoreStreamUnPlayableCalls);
        if (Ignore == PolicyValueApply)
        {
            SE_INFO(group_player, "Stream 0x%p Ignoring call\n", this);
            return;
        }
    }

    // Mark the stream as unplayable
    mUnPlayable  = true;

    // wake up only parse2decode and decode2manifest edges which have
    // action to perform when stream becomes unplayable
    ParseToDecodeEdge->WakeUp();
    DecodeToManifestEdge->WakeUp();

    // In case event is not be raised, then return
    if (noEvent == true)
    {
        return;
    }

    // raise the event to signal this to the user
    PlayerEventRecord_t Event;
    Event.Code              = EventStreamUnPlayable;
    Event.Playback          = mPlayback;
    Event.Stream            = this;
    Event.PlaybackTime      = TIME_NOT_APPLICABLE;
    Event.UserData          = NULL;
    Event.Reason            = reason;
    PlayerStatus_t Status   = SignalEvent(&Event);
    if (Status != PlayerNoError)
    {
        SE_ERROR("Stream 0x%p Failed to signal event EventStreamUnPlayable\n", this);
    }
}

// //////////////////////////////////////////////////////////////////////////////////////////////////
//
//      Step a specific stream in a playback.
//

PlayerStatus_t   PlayerStream_c::Step()
{
// Android omxse has peculiar usage of Step; cf bz62600
#ifndef CONFIG_ANDROID
    SE_VERBOSE(group_player, "Stream 0x%p\n", this);
#endif

    st_relayfs_write_se(
        this->GetStreamType() == StreamTypeAudio ?
        ST_RELAY_TYPE_PES_AUDIO_WITH_API : ST_RELAY_TYPE_PES_VIDEO_WITH_API,
        ST_RELAY_SOURCE_SE, (unsigned char *)(__func__), strnlen(__func__, 512), true);

    DecodeToManifestEdge->Step();
    return PlayerNoError;
}

PlayerStatus_t   PlayerStream_c::DiscardStep()
{
// Android omxse has peculiar usage of Step; cf bz62600
#ifndef CONFIG_ANDROID
    SE_VERBOSE(group_player, "Stream 0x%p\n", this);
#endif

    st_relayfs_write_se(
        this->GetStreamType() == StreamTypeAudio ?
        ST_RELAY_TYPE_PES_AUDIO_WITH_API : ST_RELAY_TYPE_PES_VIDEO_WITH_API,
        ST_RELAY_SOURCE_SE, (unsigned char *)(__func__), strnlen(__func__, 512), true);

    DecodeToManifestEdge->DiscardStep();
    ParseToDecodeEdge->DiscardStep();
    return PlayerNoError;
}

PlayerStatus_t   PlayerStream_c::GetElementaryBufferLevel(
    stm_se_ctrl_play_stream_elementary_buffer_level_t    *ElementaryBufferLevel)
{
    unsigned int MemoryInPool, MemoryInUse;
    unsigned int TotalBuffers, BuffersUsed;

    mCodedFrameBufferPool->GetPoolUsage(&TotalBuffers, &BuffersUsed, &MemoryInPool, NULL, &MemoryInUse, NULL);

    ElementaryBufferLevel->actual_size = MemoryInPool;
    ElementaryBufferLevel->effective_size = MemoryInPool - mCodedConfiguration.FrameMaximumSize;
    ElementaryBufferLevel->bytes_used = MemoryInUse;
    ElementaryBufferLevel->effective_bytes_used = MemoryInUse - mCodedConfiguration.FrameMaximumSize;
    ElementaryBufferLevel->maximum_nonblock_write = MemoryInPool - MemoryInUse;
    ElementaryBufferLevel->maximum_frames = TotalBuffers;
    ElementaryBufferLevel->frame_count = BuffersUsed;

    return PlayerNoError;
}

PlayerStatus_t   PlayerStream_c::Discontinuity(int discontinuity)
{
    if (SE_IS_DEBUG_ON(group_player))
    {
        bool ContinuousReverseJump = discontinuity & STM_SE_DISCONTINUITY_SMOOTH_REVERSE ? true : false;
        bool SurplusData           = discontinuity & STM_SE_DISCONTINUITY_SURPLUS_DATA   ? true : false;
        bool EndOfStream           = discontinuity & STM_SE_DISCONTINUITY_END_OF_STREAM  ? true : false;
        bool EndOfFrame            = discontinuity & STM_SE_DISCONTINUITY_END_OF_FRAME   ? true : false;
        SE_DEBUG(group_player, "Playback 0x%p Stream 0x%p ContinuousReverseJump %d SurplusData %d EndOfStream %d EndOfFrame %d\n",
                 mPlayback, this, ContinuousReverseJump, SurplusData, EndOfStream, EndOfFrame);
    }

    OS_LockMutex(&mInputLock);

    st_relayfs_write_se(
        this->GetStreamType() == StreamTypeAudio ?
        ST_RELAY_TYPE_PES_AUDIO_WITH_API : ST_RELAY_TYPE_PES_VIDEO_WITH_API,
        ST_RELAY_SOURCE_SE, (unsigned char *)(__func__), strnlen(__func__, 512), true);

    CollatorStatus_t status = mCollator->InputJump(discontinuity);
    if (status != CollatorNoError)
    {
        SE_ERROR("Stream 0x%p: Collator::InputJump failed\n", this);
        OS_UnLockMutex(&mInputLock);
        return PlayerError;
    }

    OS_UnLockMutex(&mInputLock);

    return CollatorNoError;
}

// Helper function to fill in the Buffer Pool Usage levels
//

void PlayerStream_c::GetBufferPoolLevel(BufferPool_c *Pool, BufferPoolLevel *Level)
{
    if (Pool)
    {
        mPlayer->GetBufferManager()->GetPoolUsage(Pool,
                                                  &Level->BuffersInPool,
                                                  &Level->BuffersWithNonZeroReferenceCount,
                                                  &Level->MemoryInPool,
                                                  &Level->MemoryAllocated,
                                                  &Level->MemoryInUse,
                                                  &Level->LargestFreeMemoryBlock,
                                                  &Level->MaxBuffersWithNonZeroReferenceCount,
                                                  &Level->MaxMemoryInUse);
    }
    else
    {
        memset(Level, 0, sizeof(BufferPoolLevel));
    }
}

static void AccumulatePoolStatistics(BufferPoolLevel &Acc, const BufferPoolLevel &Level)
{
    Acc.BuffersInPool  += Level.BuffersInPool;
    Acc.BuffersWithNonZeroReferenceCount  += Level.BuffersWithNonZeroReferenceCount;
    Acc.MemoryInPool  += Level.MemoryInPool;
    Acc.MemoryAllocated  += Level.MemoryAllocated;
    Acc.MemoryInUse  += Level.MemoryInUse;
    Acc.LargestFreeMemoryBlock = max(Level.LargestFreeMemoryBlock, Acc.LargestFreeMemoryBlock);
    Acc.MaxBuffersWithNonZeroReferenceCount  += Level.MaxBuffersWithNonZeroReferenceCount;
    Acc.MaxMemoryInUse  += Level.MaxMemoryInUse;
}

const PlayerStreamStatistics_t &PlayerStream_c::GetStatistics()
{
    //
    // The majority of our statistics are updated on an event basis
    // However, buffer levels, need to be polled before returning the structure
    //
    //

    BufferPool_t dbm = mDecodeBufferManager->GetDecodeBufferPool();
    if (dbm == NULL)
    {
        SE_ERROR("Statistics not available because some component of PlayStream may have been reset\n");
        return mStatistics;
    }
    GetBufferPoolLevel(dbm, &mStatistics.DecodeBufferPool);
    GetBufferPoolLevel(mCodedFrameBufferPool.Get(), &mStatistics.CodedFrameBufferPool);
    GetBufferPoolLevel(mCodedFrameBufferPool.Get(), &mStatistics.SumOfBufferPool);

    GetBufferPoolLevel(mDecodeBufferManager->GetRegisterPreprocPool(0),   &mStatistics.PreprocBufferPool);

    for (unsigned int i = 1 ; i  < MAX_PREPROC_POOL ; i++)
    {
        BufferPoolLevel poolevel;
        GetBufferPoolLevel(mDecodeBufferManager->GetRegisterPreprocPool(i),   &poolevel);
        AccumulatePoolStatistics(mStatistics.PreprocBufferPool, poolevel);
    }
    AccumulatePoolStatistics(mStatistics.SumOfBufferPool, mStatistics.PreprocBufferPool);

    GetBufferPoolLevel(mDecodeBufferManager->ComponentPool(PrimaryManifestationComponent),   &mStatistics.PrimaryManifestationBufferPool);
    AccumulatePoolStatistics(mStatistics.SumOfBufferPool, mStatistics.PrimaryManifestationBufferPool);

    GetBufferPoolLevel(mDecodeBufferManager->ComponentPool(DecimatedManifestationComponent), &mStatistics.DecimatedManifestationBufferPool);
    AccumulatePoolStatistics(mStatistics.SumOfBufferPool, mStatistics.DecimatedManifestationBufferPool);

    GetBufferPoolLevel(mDecodeBufferManager->ComponentPool(VideoDecodeCopy),                 &mStatistics.ReferenceBufferPool);
    AccumulatePoolStatistics(mStatistics.SumOfBufferPool, mStatistics.ReferenceBufferPool);

    GetBufferPoolLevel(mDecodeBufferManager->ComponentPool(VideoMacroblockStructure),       &mStatistics.ReferenceMacroBlockBufferPool);
    AccumulatePoolStatistics(mStatistics.SumOfBufferPool, mStatistics.ReferenceMacroBlockBufferPool);

    GetBufferPoolLevel(mDecodeBufferManager->ComponentPool(VideoPostProcessControl),        &mStatistics.VideoPostProcessBufferPool);
    AccumulatePoolStatistics(mStatistics.SumOfBufferPool, mStatistics.VideoPostProcessBufferPool);

    GetBufferPoolLevel(mDecodeBufferManager->ComponentPool(AdditionalMemoryBlock),          &mStatistics.AdditionalMemoryBufferPool);
    AccumulatePoolStatistics(mStatistics.SumOfBufferPool, mStatistics.AdditionalMemoryBufferPool);
    return mStatistics;
}

void PlayerStream_c::ResetStatistics()
{
    memset(&mStatistics, 0, sizeof(mStatistics));
}

const PlayerStreamAttributes_t &PlayerStream_c::GetAttributes()
{
    return mAttributes;
}

void PlayerStream_c::ResetAttributes()
{
    memset(&mAttributes, 0, sizeof(mAttributes));
}

//
//  Low power enter / exit functions at stream level
//  These methods are used to synchonize stop/restart of ParseToDecode threads in low power
//  and to terminate/init MME transformer used by codec in case of CPS mode.
//

PlayerStatus_t   PlayerStream_c::LowPowerEnter()
{
    SE_INFO(group_player, "Stream 0x%p\n", this);

    // Reset events used for putting ParseToDecode thread into "low power"
    OS_ResetEvent(&mLowPowerEnterEvent);
    OS_ResetEvent(&mLowPowerExitEvent);

    // Save low power state at stream level
    mIsLowPowerState = true;

    // wake up ParseToDecode edge loop
    if (ParseToDecodeEdge) { ParseToDecodeEdge->WakeUp();}
    // Wait for ParseToDecode thread to be in safe state (no more MME commands issued)
    OS_Status_t WaitStatus = OS_WaitForEventInterruptible(&mLowPowerEnterEvent, OS_INFINITE);
    if (WaitStatus == OS_INTERRUPTED)
    {
        SE_INFO(group_player, "wait for LP enter interrupted; event->Valid:%d\n", mLowPowerEnterEvent.Valid);
    }

    // Ask codec to enter low power state
    // For CPS mode, MME transformers will be terminated now...
    GetCodec()->LowPowerEnter();

    return PlayerNoError;
}

PlayerStatus_t   PlayerStream_c::LowPowerExit()
{
    SE_INFO(group_player, "Stream 0x%p\n", this);

    // Ask codec to exit low power state
    // For CPS mode, MME transformers will be re-initialized now...
    GetCodec()->LowPowerExit();

    // Reset low power state at stream level
    mIsLowPowerState = false;

    // Wake-up ParseToDecode thread
    OS_SetEventInterruptible(&mLowPowerExitEvent);

    return PlayerNoError;
}

PlayerStatus_t PlayerStream_c::CheckEvent(struct PlayerEventRecord_s *PlayerEvent, stm_se_play_stream_msg_t *streamMsg, stm_event_t *streamEvtMngt)
{
    SE_VERBOSE(group_player, "Stream 0x%p: Received (event %llx Playback 0x%p Stream 0x%p)\n", this, PlayerEvent->Code, PlayerEvent->Playback, PlayerEvent->Stream);

    // By default, set event_id and msg_id to invalid values
    streamEvtMngt->event_id          = STM_SE_PLAY_STREAM_EVENT_INVALID;
    streamMsg->msg_id                = STM_SE_PLAY_STREAM_MSG_INVALID;

    // Check if event is for us and we are interested.
    if ((PlayerEvent->Playback != GetPlayback()) ||
        (PlayerEvent->Stream != this))
    {
        SE_ERROR("Stream 0x%p: Playback 0x%p Invalid event info received (event %llx Event->Playback 0x%p Event->Stream 0x%p)\n",
                 this, GetPlayback(), PlayerEvent->Code, PlayerEvent->Playback, PlayerEvent->Stream);
        return PlayerError;
    }

    //{{{  Translate from an event record to the external play event record.
    switch (PlayerEvent->Code)
    {
    case EventSourceVideoParametersChangeManifest:
    {
        unsigned int    ContentWidth            = PlayerEvent->Value[WidthInfo].UnsignedInt;
        unsigned int    ContentHeight           = PlayerEvent->Value[HeightInfo].UnsignedInt;
        unsigned int    DisplayWidth            = PlayerEvent->Value[DisplayWidthInfo].UnsignedInt;
        unsigned int    DisplayHeight           = PlayerEvent->Value[DisplayHeightInfo].UnsignedInt;

        if ((0 == DisplayHeight) || (0 == ContentHeight))
        {
            SE_INFO(group_player, "Stream 0x%p: DisplayHeight 0; forcing 1:1\n", this);
            DisplayWidth  = 1;
            DisplayHeight = 1;
            ContentWidth  = 1;
            ContentHeight = 1;
        }

        Rational_t      DisplayAspectRatio      = Rational_t(DisplayWidth, DisplayHeight) * PlayerEvent->Rational;
        // Invert scan type, as stm_se_scan_type_t states: progressive = 0, interlaced = 1
        stm_se_scan_type_t ScanType             = (stm_se_scan_type_t) !PlayerEvent->Value[ProgressiveInfo].UnsignedInt;
        streamEvtMngt->event_id                  = STM_SE_PLAY_STREAM_EVENT_MESSAGE_READY;
        streamMsg->msg_id                        = STM_SE_PLAY_STREAM_MSG_VIDEO_PARAMETERS_CHANGED;
        streamMsg->u.video_parameters.width      = ContentWidth;
        streamMsg->u.video_parameters.height     = ContentHeight;
        streamMsg->u.video_parameters.scan_type  = ScanType;
        streamMsg->u.video_parameters.bits_per_component = PlayerEvent->Value[BitsPerComponentInfo].UnsignedInt;
        streamMsg->u.video_parameters.frame_packing_arrangement_type = PlayerEvent->Value[FramePackingArrangementTypeInfo].UnsignedInt;
        streamMsg->u.video_parameters.frame_packing_flags = PlayerEvent->Value[FramePackingFlagsInfo].UnsignedInt;
        streamMsg->u.video_parameters.framerate_den = PlayerEvent->Value[FrameRateDenInfo].UnsignedInt;
        streamMsg->u.video_parameters.framerate_num = PlayerEvent->Value[FrameRateNumInfo].UnsignedInt;
        streamMsg->u.video_parameters.framerate_exp = PlayerEvent->Value[FrameRateExpInfo].UnsignedInt;
        streamMsg->u.video_parameters.profile = PlayerEvent->Value[ProfileInfo].UnsignedInt;
        streamMsg->u.video_parameters.profile_level = PlayerEvent->Value[ProfileLevelInfo].UnsignedInt;

        // less than 3:2 & greater than 1:1 = 4:3, less than 2:1 = 16:9, less than equal 2.21:1 =221:100 else unspecified.
        // gap of approx 0.2 among ratio.
        if (DisplayAspectRatio <= Rational_t (1, 1))
        {
            streamMsg->u.video_parameters.aspect_ratio = STM_SE_ASPECT_RATIO_UNSPECIFIED;
        }
        else if (DisplayAspectRatio < Rational_t (3, 2))
        {
            streamMsg->u.video_parameters.aspect_ratio = STM_SE_ASPECT_RATIO_4_BY_3;
        }
        else if (DisplayAspectRatio < Rational_t (2, 1))
        {
            streamMsg->u.video_parameters.aspect_ratio = STM_SE_ASPECT_RATIO_16_BY_9;
        }
        else if (DisplayAspectRatio <= Rational_t (221, 100))
        {
            streamMsg->u.video_parameters.aspect_ratio = STM_SE_ASPECT_RATIO_221_1;
        }
        else
        {
            streamMsg->u.video_parameters.aspect_ratio = STM_SE_ASPECT_RATIO_UNSPECIFIED;
        }

        // The manifestor inserts the pixel aspect ratio into the event rational
        // get 32bits restricted numerator and denominator values
        PlayerEvent->Rational.GetRu32NumeratorDenominator(
            (uint32_t *)&streamMsg->u.video_parameters.pixel_aspect_ratio_numerator,
            (uint32_t *)&streamMsg->u.video_parameters.pixel_aspect_ratio_denominator,
            Rational_t::MATCH_ASPECTRATIO);

        // Retreive the stereoscopic data
        streamMsg->u.video_parameters.format_3d         = (stm_se_format_3d_t) PlayerEvent->Value[Stream3DFormatInfo].UnsignedInt;
        streamMsg->u.video_parameters.left_right_format = PlayerEvent->Value[Frame0IsLeftInfo].Bool;
        streamMsg->u.video_parameters.colorspace        = (stm_se_colorspace_t) PlayerEvent->Value[ColorSpaceInfo].UnsignedInt;
        streamMsg->u.video_parameters.frame_rate        = (unsigned int)PlayerEvent->Value[FrameRateInfo].UnsignedInt;
        streamMsg->u.video_parameters.display.width     = DisplayWidth;
        streamMsg->u.video_parameters.display.height    = DisplayHeight;

        //Retreive the HDR info
        streamMsg->u.video_parameters.hdr_format.eotf_type                              = (stm_eotf_type_t)PlayerEvent->Value[HdrEOTFTypeInfo].UnsignedInt;
        streamMsg->u.video_parameters.hdr_format.is_st2086_metadata_present             =    PlayerEvent->Value[HdrST2086PresentFlagInfo].UnsignedInt;
        streamMsg->u.video_parameters.hdr_format.is_hdr_metadata_present                =    PlayerEvent->Value[HdrLightLevelPresentFlagInfo].UnsignedInt;
        if (streamMsg->u.video_parameters.hdr_format.is_st2086_metadata_present)
        {
            streamMsg->u.video_parameters.hdr_format.st2086_metadata.display_primaries_x_0              =    PlayerEvent->Value[HdrDisplayPrimX0Info].UnsignedInt;
            streamMsg->u.video_parameters.hdr_format.st2086_metadata.display_primaries_y_0              =    PlayerEvent->Value[HdrDisplayPrimY0Info].UnsignedInt;
            streamMsg->u.video_parameters.hdr_format.st2086_metadata.display_primaries_x_1              =    PlayerEvent->Value[HdrDisplayPrimX1Info].UnsignedInt;
            streamMsg->u.video_parameters.hdr_format.st2086_metadata.display_primaries_y_1              =    PlayerEvent->Value[HdrDisplayPrimY1Info].UnsignedInt;
            streamMsg->u.video_parameters.hdr_format.st2086_metadata.display_primaries_x_2              =    PlayerEvent->Value[HdrDisplayPrimX2Info].UnsignedInt;
            streamMsg->u.video_parameters.hdr_format.st2086_metadata.display_primaries_y_2              =    PlayerEvent->Value[HdrDisplayPrimY2Info].UnsignedInt;
            streamMsg->u.video_parameters.hdr_format.st2086_metadata.white_point_x                      =    PlayerEvent->Value[HdrWhitePointXInfo].UnsignedInt;
            streamMsg->u.video_parameters.hdr_format.st2086_metadata.white_point_y                      =    PlayerEvent->Value[HdrWhitePointYInfo].UnsignedInt;
            streamMsg->u.video_parameters.hdr_format.st2086_metadata.max_display_mastering_luminance    =    PlayerEvent->Value[HdrMaxDisplayLumInfo].UnsignedInt;
            streamMsg->u.video_parameters.hdr_format.st2086_metadata.min_display_mastering_luminance    =    PlayerEvent->Value[HdrMinDisplayLumInfo].UnsignedInt;
        }
        if (streamMsg->u.video_parameters.hdr_format.is_hdr_metadata_present)
        {
            streamMsg->u.video_parameters.hdr_format.hdr_metadata.maxCLL            =    PlayerEvent->Value[HdrMaxCLLInfo].UnsignedInt;
            streamMsg->u.video_parameters.hdr_format.hdr_metadata.maxFALL           =    PlayerEvent->Value[HdrMaxFALLInfo].UnsignedInt;
        }
        streamMsg->u.video_parameters.hdr_format.colour_primaries                   =    PlayerEvent->Value[HdrColourPrimaries].UnsignedInt;
        streamMsg->u.video_parameters.hdr_format.transfer_characteristics           =    PlayerEvent->Value[HdrXferCharacteristics].UnsignedInt;
        streamMsg->u.video_parameters.hdr_format.matrix_coefficients                =    PlayerEvent->Value[HdrMatrixCoeffs].UnsignedInt;
        streamMsg->u.video_parameters.hdr_format.preferred_transfer_characteristics =    PlayerEvent->Value[HdrPreferredXferCharacteristics].UnsignedInt;


        mStatistics.video_width = streamMsg->u.video_parameters.width;
        mStatistics.video_height = streamMsg->u.video_parameters.height;
        mStatistics.aspect_ratio = streamMsg->u.video_parameters.aspect_ratio;
        mStatistics.scan_type = streamMsg->u.video_parameters.scan_type;
        mStatistics.colorspace = streamMsg->u.video_parameters.colorspace;

        SE_INFO(group_event, "Stream 0x%p: event STM_SE_PLAY_STREAM_MSG_VIDEO_PARAMETERS_CHANGED: "
                "Width %d Height %d DisplayWidth %d DisplayHeight %d scan_type %d aspect_ratio %d format_3d %d "
                "left_right_format %d colorspace %d frame_rate %d profile 0x%x level 0x%x bits_per_component %x "
                "frame_packing_flags 0x%x frame_packing_arrangement_type 0x%x framerate_num %d framerate_den %d framerate_exp %d "
                "eotf_type %d is_st2086_metadata_present %d is_hdr_metadata_present %d "
                "max_display_mastering_luminance %d min_display_mastering_luminance %d "
                "colour_primaries %d transfer_characteristics %d matrix_coefficients %d preferred_transfer_characteristics %d\n",
                this, streamMsg->u.video_parameters.width, streamMsg->u.video_parameters.height,
                streamMsg->u.video_parameters.display.width, streamMsg->u.video_parameters.display.height,
                streamMsg->u.video_parameters.scan_type, streamMsg->u.video_parameters.aspect_ratio,
                streamMsg->u.video_parameters.format_3d, streamMsg->u.video_parameters.left_right_format,
                streamMsg->u.video_parameters.colorspace, streamMsg->u.video_parameters.frame_rate,
                streamMsg->u.video_parameters.profile, streamMsg->u.video_parameters.profile_level, streamMsg->u.video_parameters.bits_per_component,
                streamMsg->u.video_parameters.frame_packing_flags, streamMsg->u.video_parameters.frame_packing_arrangement_type,
                streamMsg->u.video_parameters.framerate_num, streamMsg->u.video_parameters.framerate_den, streamMsg->u.video_parameters.framerate_exp,
                streamMsg->u.video_parameters.hdr_format.eotf_type, streamMsg->u.video_parameters.hdr_format.is_st2086_metadata_present,
                streamMsg->u.video_parameters.hdr_format.is_hdr_metadata_present,
                streamMsg->u.video_parameters.hdr_format.st2086_metadata.max_display_mastering_luminance,
                streamMsg->u.video_parameters.hdr_format.st2086_metadata.min_display_mastering_luminance,
                streamMsg->u.video_parameters.hdr_format.colour_primaries,
                streamMsg->u.video_parameters.hdr_format.transfer_characteristics,
                streamMsg->u.video_parameters.hdr_format.matrix_coefficients,
                streamMsg->u.video_parameters.hdr_format.preferred_transfer_characteristics);
        break;
    }

    case EventSourceFrameRateChangeManifest:
        streamEvtMngt->event_id                  = STM_SE_PLAY_STREAM_EVENT_MESSAGE_READY;
        streamMsg->msg_id                        = STM_SE_PLAY_STREAM_MSG_FRAME_RATE_CHANGED;
        streamMsg->u.frame_rate                  = RoundedIntegerPart(PlayerEvent->Rational * STM_SE_PLAY_FRAME_RATE_MULTIPLIER);
        SE_INFO(group_event, "Stream 0x%p: event STM_SE_PLAY_STREAM_MSG_FRAME_RATE_CHANGED: frame_rate %d\n",
                this, streamMsg->u.frame_rate);
        break;

    case EventEndOfStream:
        streamEvtMngt->event_id                  = STM_SE_PLAY_STREAM_EVENT_END_OF_STREAM;
        SE_INFO(group_event, "Stream 0x%p: event STM_SE_PLAY_STREAM_EVENT_END_OF_STREAM\n", this);
        break;

    case EventFirstFrameManifested:
        streamEvtMngt->event_id                  = STM_SE_PLAY_STREAM_EVENT_FIRST_FRAME_ON_DISPLAY;
        SE_INFO(group_event, "Stream 0x%p: event STM_SE_PLAY_STREAM_EVENT_FIRST_FRAME_ON_DISPLAY\n",
                this);
        break;

    case EventStreamUnPlayable:
        streamEvtMngt->event_id                  = STM_SE_PLAY_STREAM_EVENT_MESSAGE_READY;
        streamMsg->msg_id                        = STM_SE_PLAY_STREAM_MSG_STREAM_UNPLAYABLE;
        streamMsg->u.reason                      = (stm_se_play_stream_reason_code_t)PlayerEvent->Reason;
        SE_WARNING("Stream 0x%p: event STM_SE_PLAY_STREAM_MSG_STREAM_UNPLAYABLE: reason %d\n",
                   this, PlayerEvent->Reason);
        break;

    case EventFailedToQueueBufferToDisplay:
        streamEvtMngt->event_id                  = STM_SE_PLAY_STREAM_EVENT_FATAL_ERROR;
        SE_ERROR("Stream 0x%p: event STM_SE_PLAY_STREAM_EVENT_FATAL_ERROR: reason EventFailedToQueueBufferToDisplay\n",
                 this);
        break;

    case EventFailedToDecodeInTime:
        streamEvtMngt->event_id                  = STM_SE_PLAY_STREAM_EVENT_FRAME_DECODED_LATE;
        SE_WARNING("Stream 0x%p: event STM_SE_PLAY_STREAM_EVENT_FRAME_DECODED_LATE\n",
                   this);
        break;

    case EventFailedToDeliverDataInTime:
        streamEvtMngt->event_id                  = STM_SE_PLAY_STREAM_EVENT_DATA_DELIVERED_LATE;
        SE_WARNING("Stream 0x%p: event STM_SE_PLAY_STREAM_EVENT_DATA_DELIVERED_LATE\n",
                   this);
        break;

    case EventTrickModeDomainChange:
        streamEvtMngt->event_id                  = STM_SE_PLAY_STREAM_EVENT_MESSAGE_READY;
        streamMsg->msg_id                        = STM_SE_PLAY_STREAM_MSG_TRICK_MODE_CHANGE;
        streamMsg->u.trick_mode_domain           = PlayerEvent->Value[0].UnsignedInt;

        SE_INFO(group_event, "Stream 0x%p: event STM_SE_PLAY_STREAM_MSG_TRICK_MODE_CHANGE: trick_mode_domain %d\n",
                this, streamMsg->u.trick_mode_domain);
        break;

    case EventFatalHardwareFailure:
        streamEvtMngt->event_id                  = STM_SE_PLAY_STREAM_EVENT_FATAL_HARDWARE_FAILURE;
        SE_ERROR("Stream 0x%p: event STM_SE_PLAY_STREAM_EVENT_FATAL_HARDWARE_FAILURE\n",
                 this);
        break;

    case EventNewFrameDisplayed:
        streamEvtMngt->event_id                  = STM_SE_PLAY_STREAM_EVENT_FRAME_RENDERED;
        SE_VERBOSE(group_event, "Stream 0x%p: event STM_SE_PLAY_STREAM_EVENT_FRAME_RENDERED\n",
                   this);
        break;

    case EventNewFrameDecoded:
        streamEvtMngt->event_id                  = STM_SE_PLAY_STREAM_EVENT_FRAME_DECODED;
        SE_VERBOSE(group_event, "Stream 0x%p: event STM_SE_PLAY_STREAM_EVENT_FRAME_DECODED\n",
                   this);
        break;

    case EventStreamInSync:
        streamEvtMngt->event_id                  = STM_SE_PLAY_STREAM_EVENT_STREAM_IN_SYNC;
        SE_INFO(group_event, "Stream 0x%p: event STM_SE_PLAY_STREAM_EVENT_STREAM_IN_SYNC\n",
                this);
        break;

    case EventTimeMappingEstablished:
        streamEvtMngt->event_id                  = STM_SE_PLAY_STREAM_EVENT_MESSAGE_READY;
        streamMsg->msg_id                        = STM_SE_PLAY_STREAM_MSG_TIME_MAPPING_INFO;

        streamMsg->u.time_mapping_info.source = (stm_se_time_mapping_source_t) PlayerEvent->Value[0].UnsignedInt;

        const char *SourceStr;
        switch (streamMsg->u.time_mapping_info.source)
        {
        case MappingSourcePcr:          SourceStr = "PCR"; break;
        case MappingSourceSelf:         SourceStr = "SELF"; break;
        case MappingSourceOther:        SourceStr = "OTHER"; break;
        case MappingSourceVideoCapture: SourceStr = "VIDEO_IN"; break;
        default:                        SourceStr = "UNKNOWN"; break;
        }
        SE_DEBUG(group_event, "Stream 0x%p: event STM_SE_PLAY_STREAM_MSG_TIME_MAPPING_INFO (Source %s)\n",
                 this, SourceStr);
        break;

    case EventAlarmParsedPts:
        streamEvtMngt->event_id                  = STM_SE_PLAY_STREAM_EVENT_MESSAGE_READY;
        streamMsg->msg_id                        = STM_SE_PLAY_STREAM_MSG_ALARM_PARSED_PTS;
        streamMsg->u.alarm_parsed_pts.pts = (uint64_t) PlayerEvent->PlaybackTime;
        streamMsg->u.alarm_parsed_pts.size = PlayerEvent->Value[0].UnsignedInt;
        /* Retrieve marker_id0 and marker_id1, highest bits first */
        streamMsg->u.alarm_parsed_pts.data[0] = (PlayerEvent->Value[1].UnsignedInt >> 24) & 0xff;
        streamMsg->u.alarm_parsed_pts.data[1] = (PlayerEvent->Value[1].UnsignedInt >> 16) & 0xff;
        streamMsg->u.alarm_parsed_pts.data[2] = (PlayerEvent->Value[1].UnsignedInt >> 8) & 0xff;
        streamMsg->u.alarm_parsed_pts.data[3] = PlayerEvent->Value[1].UnsignedInt & 0xff;
        streamMsg->u.alarm_parsed_pts.data[4] = (PlayerEvent->Value[2].UnsignedInt >> 24) & 0xff;
        streamMsg->u.alarm_parsed_pts.data[5] = (PlayerEvent->Value[2].UnsignedInt >> 16) & 0xff;
        streamMsg->u.alarm_parsed_pts.data[6] = (PlayerEvent->Value[2].UnsignedInt >> 8) & 0xff;
        streamMsg->u.alarm_parsed_pts.data[7] = PlayerEvent->Value[2].UnsignedInt & 0xff;
        streamMsg->u.alarm_parsed_pts.data[8] = (PlayerEvent->Value[3].UnsignedInt >> 24) & 0xff;
        streamMsg->u.alarm_parsed_pts.data[9] = (PlayerEvent->Value[3].UnsignedInt >> 16) & 0xff;
        SE_DEBUG(group_event, "Stream 0x%p: event STM_SE_PLAY_STREAM_MSG_ALARM_PARSED_PTS: size %d"
                 "ControlUserData[1]= %02x%02x%02x%02x , "
                 "ControlUserData[2]=%02x%02x%02x%02x,"
                 "ControlUserData[3]= %02x%02x, PTS %llu\n",
                 this, streamMsg->u.alarm_parsed_pts.size,
                 streamMsg->u.alarm_parsed_pts.data[0],
                 streamMsg->u.alarm_parsed_pts.data[1],
                 streamMsg->u.alarm_parsed_pts.data[2],
                 streamMsg->u.alarm_parsed_pts.data[3],
                 streamMsg->u.alarm_parsed_pts.data[4],
                 streamMsg->u.alarm_parsed_pts.data[5],
                 streamMsg->u.alarm_parsed_pts.data[6],
                 streamMsg->u.alarm_parsed_pts.data[7],
                 streamMsg->u.alarm_parsed_pts.data[8],
                 streamMsg->u.alarm_parsed_pts.data[9],
                 streamMsg->u.alarm_parsed_pts.pts);
        break;

    case EventFrameStarvation:
        streamEvtMngt->event_id                  = STM_SE_PLAY_STREAM_EVENT_FRAME_STARVATION;
        SE_WARNING("Stream 0x%p: event STM_SE_PLAY_STREAM_EVENT_FRAME_STARVATION\n",
                   this);
        break;

    case EventFrameSupplied:
        streamEvtMngt->event_id                  = STM_SE_PLAY_STREAM_EVENT_FRAME_SUPPLIED;
        SE_VERBOSE(group_event, "Stream 0x%p: event STM_SE_PLAY_STREAM_EVENT_FRAME_SUPPLIED\n",
                   this);
        break;

    case EventSourceAudioParametersChange:
        streamEvtMngt->event_id                          = STM_SE_PLAY_STREAM_EVENT_MESSAGE_READY;
        streamMsg->msg_id                                = STM_SE_PLAY_STREAM_MSG_AUDIO_PARAMETERS_CHANGED;
        // We need to properly assign the Values
        // codec common parameters
        streamMsg->u.audio_parameters.audio_coding_type  = (stm_se_stream_encoding_t) PlayerEvent->Value[0].UnsignedInt;
        streamMsg->u.audio_parameters.sampling_freq      = (uint32_t) PlayerEvent->Value[1].UnsignedInt;
        streamMsg->u.audio_parameters.num_channels       = (uint8_t) PlayerEvent->Value[2].UnsignedInt;
        streamMsg->u.audio_parameters.dual_mono          = (bool) PlayerEvent->Value[3].UnsignedInt;
        streamMsg->u.audio_parameters.channel_assignment.pair0     = (unsigned int) PlayerEvent->Value[4].UnsignedInt;
        streamMsg->u.audio_parameters.channel_assignment.pair1     = (unsigned int) PlayerEvent->Value[5].UnsignedInt;
        streamMsg->u.audio_parameters.channel_assignment.pair2     = (unsigned int) PlayerEvent->Value[6].UnsignedInt;
        streamMsg->u.audio_parameters.channel_assignment.pair3     = (unsigned int) PlayerEvent->Value[7].UnsignedInt;
        streamMsg->u.audio_parameters.channel_assignment.pair4     = (unsigned int) PlayerEvent->Value[8].UnsignedInt;
        streamMsg->u.audio_parameters.channel_assignment.malleable = (unsigned int) PlayerEvent->Value[9].UnsignedInt;
        streamMsg->u.audio_parameters.bitrate            = (int) PlayerEvent->Value[10].UnsignedInt;
        streamMsg->u.audio_parameters.copyright          = (bool) PlayerEvent->Value[11].UnsignedInt;
        streamMsg->u.audio_parameters.emphasis           = (bool) PlayerEvent->Value[12].UnsignedInt;
        SE_INFO(group_event, "Stream 0x%p: event STM_SE_PLAY_STREAM_MSG_AUDIO_PARAMETERS_CHANGED: "
                "common: AudioCodingType %d SamplingFreq %d NumChannels %d DualMono %d "
                "ChannelAssign[%d-%d-%d-%d-%d malleable(%d)] Bitrate %d Copyright %d Emphasis %d\n",
                this, streamMsg->u.audio_parameters.audio_coding_type,
                streamMsg->u.audio_parameters.sampling_freq,
                streamMsg->u.audio_parameters.num_channels,
                streamMsg->u.audio_parameters.dual_mono,
                streamMsg->u.audio_parameters.channel_assignment.pair0,
                streamMsg->u.audio_parameters.channel_assignment.pair1,
                streamMsg->u.audio_parameters.channel_assignment.pair2,
                streamMsg->u.audio_parameters.channel_assignment.pair3,
                streamMsg->u.audio_parameters.channel_assignment.pair4,
                streamMsg->u.audio_parameters.channel_assignment.malleable,
                streamMsg->u.audio_parameters.bitrate,
                streamMsg->u.audio_parameters.copyright,
                streamMsg->u.audio_parameters.emphasis);

        // codec specific parameters (if exists)
        switch (streamMsg->u.audio_parameters.audio_coding_type)
        {
        case STM_SE_STREAM_ENCODING_AUDIO_MPEG2:
        case STM_SE_STREAM_ENCODING_AUDIO_MP3:
            streamMsg->u.audio_parameters.codec_specific.mpeg_params.layer = (uint32_t) PlayerEvent->ExtraValue[0].UnsignedInt;
            SE_DEBUG(group_event, " specific: Layer=%d\n",
                     streamMsg->u.audio_parameters.codec_specific.mpeg_params.layer);
            break;

        default:
            // no specific parameters
            break;
        }

        break;

    case EventStreamSwitched:
        streamEvtMngt->event_id                  = STM_SE_PLAY_STREAM_EVENT_SWITCH_COMPLETED;
        SE_INFO(group_event, "Stream 0x%p: event STM_SE_PLAY_STREAM_EVENT_SWITCH_COMPLETED\n",
                this);
        break;

    case EventAlarmPts:
        streamEvtMngt->event_id                  = STM_SE_PLAY_STREAM_EVENT_MESSAGE_READY;
        streamMsg->msg_id                        = STM_SE_PLAY_STREAM_MSG_ALARM_PTS;
        streamMsg->u.alarm_pts.pts = (uint64_t) PlayerEvent->PlaybackTime;
        SE_INFO(group_event, "Stream 0x%p: event STM_SE_PLAY_STREAM_MSG_ALARM_PTS pts %llu\n",
                this, streamMsg->u.alarm_pts.pts);
        break;

    case EventDiscarding:
        streamEvtMngt->event_id                  = STM_SE_PLAY_STREAM_EVENT_MESSAGE_READY;
        streamMsg->msg_id                        = STM_SE_PLAY_STREAM_MSG_DISCARDING;
        streamMsg->u.discard_trigger.u.pts_trigger.pts = (uint64_t) PlayerEvent->PlaybackTime;
        streamMsg->u.discard_trigger.type = (stm_se_play_stream_trigger_type_t)PlayerEvent->Value[0].UnsignedInt;
        streamMsg->u.discard_trigger.start_not_end = PlayerEvent->Value[1].Bool;
        SE_INFO(group_event, "Stream 0x%p: event STM_SE_PLAY_STREAM_MSG_DISCARDING type %d %s trigger PTS %llu\n",
                this, streamMsg->u.discard_trigger.type,
                (streamMsg->u.discard_trigger.start_not_end == true) ? "start" : "end", streamMsg->u.discard_trigger.u.pts_trigger.pts);
        break;

    default:
        SE_ERROR("Stream 0x%p: Unexpected event %llx\n", this, PlayerEvent->Code);
        return PlayerError;
    }

    return PlayerNoError;
}

PlayerStatus_t PlayerStream_c::SignalEvent(struct PlayerEventRecord_s     *PlayerEvent)
{
    stm_se_play_stream_msg_t streamMsg;
    stm_event_t              streamEvtMngt;
    int                      err = 0;
    PlayerStatus_t           PlayerStatus = PlayerNoError;

    if (CheckEvent(PlayerEvent, &streamMsg, &streamEvtMngt) == PlayerNoError)
    {
        // Signal Event via Event manager
        if (streamEvtMngt.event_id != STM_SE_PLAY_STREAM_EVENT_INVALID)
        {
            if (streamEvtMngt.event_id == STM_SE_PLAY_STREAM_EVENT_MESSAGE_READY)
            {
                // Push message to subscriptions
                if (GetMessenger()->NewSignaledMessage(&streamMsg) != MessageNoError)
                {
                    SE_WARNING("Stream 0x%p: Message subscription not supported %d\n", this, streamMsg.msg_id);
                }
            }

            streamEvtMngt.object = (stm_object_h)GetHavanaStream();
            err = stm_event_signal(&streamEvtMngt);
            if (err)
            {
                if (err == -ECANCELED)
                {
                    SE_INFO(group_player, "Stream 0x%p: stm_event_signal returned ECANCELED\n", this);
                    // event suscriber was removed; not an error
                    PlayerStatus = PlayerNoError;
                }
                else
                {
                    PlayerStatus = PlayerError;
                    SE_FATAL("Stream 0x%p: stm_event_signal failed (%d)\n", this, err);
                }
            }
        }
    }
    else
    {
        SE_ERROR("Stream 0x%p: error during event check\n", this);
        PlayerStatus = PlayerError;
    }

    return PlayerStatus;
}

void  PlayerStream_c::SetSpeed(Rational_t Speed, PlayDirection_t Direction)
{
    st_relayfs_write_se(
        this->GetStreamType() == StreamTypeAudio ?
        ST_RELAY_TYPE_PES_AUDIO_WITH_API : ST_RELAY_TYPE_PES_VIDEO_WITH_API,
        ST_RELAY_SOURCE_SE, (unsigned char *)(__func__), strnlen(__func__, 512), true);

    GetOutputTimer()->SetSpeed(Speed, Direction);
    if ((mStreamType == StreamTypeAudio) && (Speed != 0))
    {
        GetCodec()->UpdatePlaybackSpeed();
    }
    // Reset frame parser RevPlaySmoothReverseFailureCount on speed request.
    // This is to avoid having backward remaining in I only even after doing a positive speed change (see bugzilla 19026).
    GetFrameParser()->ResetReverseFailureCounter();

    // Wakeup all threads that have some throttling
    ParseToDecodeEdge->WakeUp();
    DecodeToManifestEdge->WakeUp();
    mCollator->WakeUp();

    if (Speed == 0)
    {
        DecodeToManifestEdge->WaitForFrozenSurface();
    }
}

void PlayerStream_c::ApplyDecoderConfig(unsigned int Config)
{
    if (ParseToDecodeEdge)
    {
        ParseToDecodeEdge->ApplyDecoderConfig(Config);
    }
}

bool PlayerStream_c::isMemProfileAboveHd(int MemProfilePolicy)
{
    if ((MemProfilePolicy == PolicyValueVideoPlayStreamMemoryProfile4K2K) ||
        (MemProfilePolicy == PolicyValueVideoPlayStreamMemoryProfileUHD) ||
        (MemProfilePolicy == PolicyValueVideoPlayStreamMemoryProfile4K2K10Bits) ||
        (MemProfilePolicy == PolicyValueVideoPlayStreamMemoryProfileUHD10Bits) ||
        (MemProfilePolicy == PolicyValueVideoPlayStreamMemoryProfileAuto))
    {
        return true;
    }
    else
    {
        return false;
    }
}

bool PlayerStream_c::is10BitProfileSupported(int MemProfilePolicy)
{
    if ((MemProfilePolicy == PolicyValueVideoPlayStreamMemoryProfileHD10Bits) ||
        (MemProfilePolicy == PolicyValueVideoPlayStreamMemoryProfile4K2K10Bits) ||
        (MemProfilePolicy == PolicyValueVideoPlayStreamMemoryProfileHD720p10Bits) ||
        (MemProfilePolicy == PolicyValueVideoPlayStreamMemoryProfileUHD10Bits) ||
        (MemProfilePolicy == PolicyValueVideoPlayStreamMemoryProfileAuto))
    {
        return true;
    }
    else
    {
        return false;
    }
}

PlayerStatus_t PlayerStream_c::ConfigureDecodeBufferManagerForVideo()
{
    SE_ASSERT(mStreamType == StreamTypeVideo);

    DecodeBufferManagerParameterBlock_t Parameters;
    memset(&Parameters, 0, sizeof(DecodeBufferManagerParameterBlock_t));
    Parameters.ParameterType          = DecodeBufferManagerPartitionData;
    Parameters.PartitionData.OnDemand = false;
    Parameters.MediaType = STM_SE_MEDIA_VIDEO;
    int MemProfilePolicy = mPlayer->PolicyValue(mPlayback,
                                                PlayerAllStreams, PolicyVideoPlayStreamMemoryProfile);

    int CaptureProfile = mPlayer->PolicyValue(mPlayback,
                                              PlayerAllStreams, PolicyCaptureProfile);

    SE_INFO(group_player, "MemProfile = %d CaptureProfile = %d\n", MemProfilePolicy, CaptureProfile);

    unsigned int NumExtraRefBuffer = 0;
    if (mEncoding == STM_SE_STREAM_ENCODING_VIDEO_VP9)
    {
        /* extra buffers allocated to as VP9 hantro code requires equal number of Display/Reference buffers
        it is requiring 8 buffers so this fits for all memory profiles */
        NumExtraRefBuffer = 2;
        SE_DEBUG(group_player, "Extra reference buffers for vp9 :%d\n", NumExtraRefBuffer);
    }
    unsigned int Size;
    unsigned int CopySize;

    switch (CaptureProfile)
    {
    case PolicyValueCaptureProfileHdmiRxNoAudioDecodeNoVideoFrc:
    case PolicyValueCaptureProfileHdmiRxAudioDecodeVideoFrc:
    case PolicyValueCaptureProfileHdmiRxNoAudioDecodeVideoFrc:
    case PolicyValueCaptureProfileCompoNoAudioVideoFrc:
    case PolicyValueCaptureProfileCompoNoAudioNoVideoFrc:
        // Forcing size to 0 means that Automatic Allocation is used
        // Pool size is computed according from first buffer request
        // size and capture profile
        Size                            = 0;
        CopySize                        = 0;
        break;

    case PolicyValueCaptureProfileDisabled:
    default:
        // Keep pool size based in video decoding memory profiles.
        // Size = number of raster buffers X YUV420 frame size
        // CopySize = number of reference buffers X YUV420 frame size
        switch (MemProfilePolicy)
        {
        case PolicyValueVideoPlayStreamMemoryProfileHD10Bits:
        case PolicyValueVideoPlayStreamMemoryProfileHDOptimized:
            SE_INFO(group_player, "HD optimized memory usage\n");
            Size = 9 * HD_420_BUFFER_SIZE;
            CopySize = (7 + NumExtraRefBuffer) * HD_420_BUFFER_SIZE; /* 5 for H264 , 6 buffer for HEVC streams , 7 for BWD test or HEVC_MT41_RAP */
            break;

        case PolicyValueVideoPlayStreamMemoryProfile4K2K10Bits:
        case PolicyValueVideoPlayStreamMemoryProfile4K2K:
            SE_INFO(group_player, "4K2K memory usage\n");
            Size = 9 * UHD_4K2K_420_BUFFER_SIZE;
            CopySize = (6 + NumExtraRefBuffer) * UHD_4K2K_420_BUFFER_SIZE;
            break;

        case PolicyValueVideoPlayStreamMemoryProfileUHD10Bits:
        case PolicyValueVideoPlayStreamMemoryProfileUHD:
            SE_INFO(group_player, "UHD memory usage\n");
            Size = 9 * UHD_420_BUFFER_SIZE;
            CopySize = (7 + NumExtraRefBuffer) * UHD_420_BUFFER_SIZE;
            break;

        case PolicyValueVideoPlayStreamMemoryProfileSD: /* dedicated to mosaic use case */
            SE_INFO(group_player, "SD memory usage\n");
            Size = 9 * SD_420_BUFFER_SIZE;
            CopySize = (12 + NumExtraRefBuffer) * SD_420_BUFFER_SIZE;
            break;

        case PolicyValueVideoPlayStreamMemoryProfileHD720p10Bits:
        case PolicyValueVideoPlayStreamMemoryProfileHD720p:
            SE_INFO(group_player, "HD 720p memory usage\n");
            Size = 9 * HD_720P_420_BUFFER_SIZE;
            CopySize = (9 + NumExtraRefBuffer) * HD_720P_420_BUFFER_SIZE;
            break;

        case PolicyValueVideoPlayStreamMemoryProfileAuto:
            SE_INFO(group_havana, "Automatic memory usage\n");
            Size     = 0;
            CopySize = 0;
            break;

        case PolicyValueVideoPlayStreamMemoryProfileSingleIntra:
            SE_INFO(group_havana, "Single Intra decode memory usage\n");
            Size     = 0;
            CopySize = 0;
            break;

        case PolicyValueVideoPlayStreamMemoryProfileHDLegacy:
        default:
            SE_INFO(group_player, "HD max legacy memory usage\n");
            Size = 9 * HD_420_BUFFER_SIZE;
            CopySize = (6 + NumExtraRefBuffer) * HD_420_BUFFER_SIZE;
            break;
        }
        break;
    }

    /* MBStructSize stands for amount of memory reserved to the PPB buffers,
       one PPB per FB, each PPB size = HD MBstructure size x nb of MBs (8160 MBs per HD picture)
       MBstructure size = 48 bytes (for Hades = 16bytes+fake PPB buffer), 384 is the 420 MB size
       then should take a provision of 512 bytes per buffer
       From Regression test suite no more than 12 buffers can be used for the MBcontext
       For 10 bits: MBStructSize only depends on the nb of MB (same as 8 bits)
    */
    unsigned int MBStructSize;
    SE_VERBOSE(group_player, "Size: %d CopySize: %d\n", Size, CopySize);
    if (mEncoding == STM_SE_STREAM_ENCODING_VIDEO_VP9)
    {
        // 64 bytes are neeed per Macro Block 384/64=6
        MBStructSize = (CopySize) ? CopySize / 6  : 0;
    }
    else
    {
        // computation -> MBStructSize = (CopySize) ? (48 * CopySize) / 384 + 512 * 12 : 0;
        // In case of 4K mem-profile CopySize * 48 > 2^32-1
        MBStructSize = (CopySize) ? (CopySize / 8 + 512 * 12) : 0;
    }
    if (is10BitProfileSupported(MemProfilePolicy))
    {
        SE_INFO(group_havana, "Input stream bit depth is set to 10 bits\n");
        Size = CONV_8TO10_BITS(Size);
        // OmegaH10 definition: Each block of 4x4 pixels (i.e. 16 Luma + 8 chroma)
        // is stored in 256 bits instead of 240 bits + one macroblock line added
        // Size of OmegaH10 = 2 x W x (H+16)
        // CopySize = max_nb_buffer x ((3/2 x W x H) x 4/3 + max width)
        if (CopySize != 0)
        {
            CopySize = (CopySize * 4) / 3 + 9 * PLAYER_4K2K_FRAME_WIDTH;
        }
    }
    SE_VERBOSE(group_player, "PrimaryManifestationPartitionSize: %d VideoDecodeCopyPartitionSize: %d\n",
               Size, CopySize);

    const unsigned int EffectiveTransformerId = mTransformerId;
    // partitions will be appended with -0 or -1 depending TransformerId
#define DECL_P(name, partition) \
        char name[] = partition; name[sizeof(name)-2] = '0' + EffectiveTransformerId;
    DECL_P(Partition,            "vid-output-X");
    DECL_P(DecimatedPartition,   "vid-decimated-X");
    DECL_P(CopyPartition,        "vid-copied-X");
    DECL_P(MacroblockPartition,  "vid-macroblock-X");
    DECL_P(PostProcessPartition, "vid-postproc-X");
    DECL_P(AdditionalPartition,  "vid-extra-data-X");
#undef DECL_P
    Parameters.PartitionData.NumberOfDecodeBuffers = MAX_VIDEO_DECODE_BUFFERS;

    //
    // In case decode buffers are provided by user, do not allocate buffer yet
    //
    const char *part = NULL;
    if (mPlayer->PolicyValue(mPlayback, PlayerAllStreams, PolicyDecodeBuffersUserAllocated) == PolicyValueApply)
    {
        part = AllocatedLaterPartition;
        // Reset default value for next playstream new
        mPlayer->SetPolicy(PlayerAllPlaybacks, PlayerAllStreams, PolicyDecodeBuffersUserAllocated, PolicyValueDisapply);
    }
    else
    {
        part = Partition;
    }

    Parameters.PartitionData.PrimaryManifestationPartitionSize = Size;

    strncpy(Parameters.PartitionData.PrimaryManifestationPartitionName, part,
            sizeof(Parameters.PartitionData.PrimaryManifestationPartitionName));
    Parameters.PartitionData.PrimaryManifestationPartitionName[
        sizeof(Parameters.PartitionData.PrimaryManifestationPartitionName) - 1] = '\0';

    int DecimationPolicy = mPlayer->PolicyValue(mPlayback,
                                                PlayerAllStreams, PolicyDecimateDecoderOutput);

    switch (DecimationPolicy)
    {
    case PolicyValueDecimateDecoderOutputHalf:
        Parameters.PartitionData.DecimatedManifestationPartitionSize = Size / 4;
        break;
    case PolicyValueDecimateDecoderOutputQuarter:
    case PolicyValueDecimateDecoderOutputH2V4:
        Parameters.PartitionData.DecimatedManifestationPartitionSize = Size / 8;
        break;
    case PolicyValueDecimateDecoderOutputH2V8:
    case PolicyValueDecimateDecoderOutputH8V2:
    case PolicyValueDecimateDecoderOutputH4V4:
        Parameters.PartitionData.DecimatedManifestationPartitionSize = Size / 16;
        break;
    case PolicyValueDecimateDecoderOutputH4V8:
    case PolicyValueDecimateDecoderOutputH8V4:
        Parameters.PartitionData.DecimatedManifestationPartitionSize = Size / 32;
        break;
    case PolicyValueDecimateDecoderOutputH8V8:
        Parameters.PartitionData.DecimatedManifestationPartitionSize = Size / 64;
        break;
    case PolicyValueDecimateDecoderOutputH1V1:
        Parameters.PartitionData.DecimatedManifestationPartitionSize = Size;
        break;
    default:
        Parameters.PartitionData.DecimatedManifestationPartitionSize = 0;
        SE_INFO(group_player, "Decimated buffers NOT allocated\n");
    }
    SE_INFO(group_player, "Decimation policy: %d (size: %d bytes)\n",
            DecimationPolicy, Parameters.PartitionData.DecimatedManifestationPartitionSize);

    strncpy(Parameters.PartitionData.DecimatedManifestationPartitionName, DecimatedPartition,
            sizeof(Parameters.PartitionData.DecimatedManifestationPartitionName));
    Parameters.PartitionData.DecimatedManifestationPartitionName[
        sizeof(Parameters.PartitionData.DecimatedManifestationPartitionName) - 1] = '\0';

    Parameters.PartitionData.VideoDecodeCopyPartitionSize             = CopySize;
    strncpy(Parameters.PartitionData.VideoDecodeCopyPartitionName, CopyPartition,
            sizeof(Parameters.PartitionData.VideoDecodeCopyPartitionName));
    Parameters.PartitionData.VideoDecodeCopyPartitionName[
        sizeof(Parameters.PartitionData.VideoDecodeCopyPartitionName) - 1] = '\0';

    Parameters.PartitionData.VideoMacroblockStructurePartitionSize    = MBStructSize;
    strncpy(Parameters.PartitionData.VideoMacroblockStructurePartitionName, MacroblockPartition,
            sizeof(Parameters.PartitionData.VideoMacroblockStructurePartitionName));
    Parameters.PartitionData.VideoMacroblockStructurePartitionName[
        sizeof(Parameters.PartitionData.VideoMacroblockStructurePartitionName) - 1] = '\0';

    Parameters.PartitionData.VideoPostProcessControlPartitionSize     = 0;
    strncpy(Parameters.PartitionData.VideoPostProcessControlPartitionName, "",
            sizeof(Parameters.PartitionData.VideoPostProcessControlPartitionName));
    Parameters.PartitionData.VideoPostProcessControlPartitionName[
        sizeof(Parameters.PartitionData.VideoPostProcessControlPartitionName) - 1] = '\0';

    const unsigned int AdditionalBlockSize  = 0x1000;   // This is taken from the rmv code (128*8)
    Parameters.PartitionData.AdditionalMemoryBlockPartitionSize       = AdditionalBlockSize;
    strncpy(Parameters.PartitionData.AdditionalMemoryBlockPartitionName, AdditionalPartition,
            sizeof(Parameters.PartitionData.AdditionalMemoryBlockPartitionName));
    Parameters.PartitionData.AdditionalMemoryBlockPartitionName[
        sizeof(Parameters.PartitionData.AdditionalMemoryBlockPartitionName) - 1] = '\0';

    DecodeBufferManagerStatus_t status = mDecodeBufferManager->SetModuleParameters(sizeof(Parameters), &Parameters);
    if (status != DecodeBufferManagerNoError)
    {
        return PlayerError;
    }
    else
    {
        return PlayerNoError;
    }
}

PlayerStatus_t PlayerStream_c::ConfigureDecodeBufferManagerForAudio()
{
    SE_ASSERT(mStreamType == StreamTypeAudio);

    DecodeBufferManagerParameterBlock_t Parameters;
    memset(&Parameters, 0, sizeof(DecodeBufferManagerParameterBlock_t));
    Parameters.ParameterType          = DecodeBufferManagerPartitionData;
    Parameters.PartitionData.OnDemand = false;
    Parameters.MediaType = STM_SE_MEDIA_AUDIO;

#define DECL_P(name, partition) \
        char name[] = partition; name[sizeof(name)-2] = '0' + mTransformerId;
    DECL_P(Partition,            "aud-output-X");
#undef DECL_P

    Parameters.PartitionData.NumberOfDecodeBuffers  = MAX_AUDIO_DECODE_BUFFERS;

    SE_DEBUG(group_player, "Audio Encoding Type: %s\n", StringifyStreamEncodingType(mEncoding));

    unsigned int NumSamplesPerFrame;
    switch (mEncoding)
    {
    case STM_SE_STREAM_ENCODING_AUDIO_PCM:
        NumSamplesPerFrame = PCM_TRANSCODER_MAX_DECODED_SAMPLE_COUNT;
        break;
    case STM_SE_STREAM_ENCODING_AUDIO_LPCM:
    case STM_SE_STREAM_ENCODING_AUDIO_LPCMA:
    case STM_SE_STREAM_ENCODING_AUDIO_LPCMH:
    case STM_SE_STREAM_ENCODING_AUDIO_LPCMB:
        NumSamplesPerFrame = LPCM_MAXIMUM_SAMPLE_COUNT;
        break;
    case STM_SE_STREAM_ENCODING_AUDIO_MPEG1:
    case STM_SE_STREAM_ENCODING_AUDIO_MPEG2:
    case STM_SE_STREAM_ENCODING_AUDIO_MP3:
    case STM_SE_STREAM_ENCODING_AUDIO_MPEG2_SYSB:
        NumSamplesPerFrame = MAX_AUDIO_DECODED_SAMPLE_COUNT;
        break;
    case STM_SE_STREAM_ENCODING_AUDIO_AC3:
        NumSamplesPerFrame = EAC3_MAX_DECODED_SAMPLE_COUNT;
        break;
    case STM_SE_STREAM_ENCODING_AUDIO_DTS:
    case STM_SE_STREAM_ENCODING_AUDIO_DTS_LBR:
        NumSamplesPerFrame = DTSHD_MAX_DECODED_SAMPLE_COUNT;
        break;
    case STM_SE_STREAM_ENCODING_AUDIO_AAC:
        NumSamplesPerFrame = AAC_MAX_NBSAMPLES_PER_FRAME;
        break;
    case STM_SE_STREAM_ENCODING_AUDIO_WMA:
        NumSamplesPerFrame = WMA_BLOCK_WISE_SAMPLE_COUNT;
        break;
    case STM_SE_STREAM_ENCODING_AUDIO_SPDIFIN:
        NumSamplesPerFrame = SPDIFIN_BLOCK_WISE_SAMPLE_COUNT;
        break;
    case STM_SE_STREAM_ENCODING_AUDIO_MLP:
        NumSamplesPerFrame = MLP_MAX_DECODED_SAMPLE_COUNT;
        break;
    case STM_SE_STREAM_ENCODING_AUDIO_RMA:
        NumSamplesPerFrame = RMA_MAX_DECODED_SAMPLE_COUNT;
        break;
    case STM_SE_STREAM_ENCODING_AUDIO_VORBIS:
        NumSamplesPerFrame = VORBIS_MAX_DECODED_SAMPLE_COUNT;
        break;
    case STM_SE_STREAM_ENCODING_AUDIO_AVS:
    case STM_SE_STREAM_ENCODING_AUDIO_DRA:
        NumSamplesPerFrame = DRA_MAX_DECODED_SAMPLE_COUNT;
        break;
    case STM_SE_STREAM_ENCODING_AUDIO_MS_ADPCM:
    case STM_SE_STREAM_ENCODING_AUDIO_IMA_ADPCM:
        NumSamplesPerFrame = ADPCM_MAXIMUM_SAMPLE_COUNT;
        break;
    case STM_SE_STREAM_ENCODING_AUDIO_NONE:
    case STM_SE_STREAM_ENCODING_AUDIO_AUTO:
    default:
        NumSamplesPerFrame = MAX_AUDIO_DECODED_SAMPLE_COUNT;
        SE_WARNING("Unsupported/Auto Encoding type obtained. NumSamplesPerFrame set to Default value: %d\n", NumSamplesPerFrame);
        break;
    }

    Parameters.PartitionData.PrimaryManifestationPartitionSize = 4 * NumSamplesPerFrame * MAX_AUDIO_OUTPUT_CHANNELS * MAX_AUDIO_DECODE_BUFFERS; // Partition size in bytes
    strncpy(Parameters.PartitionData.PrimaryManifestationPartitionName, Partition,
            sizeof(Parameters.PartitionData.PrimaryManifestationPartitionName));
    Parameters.PartitionData.PrimaryManifestationPartitionName[
        sizeof(Parameters.PartitionData.PrimaryManifestationPartitionName) - 1] = '\0';

    DecodeBufferManagerStatus_t status = mDecodeBufferManager->SetModuleParameters(sizeof(Parameters), &Parameters);
    if (status != DecodeBufferManagerNoError)
    {
        return PlayerError;
    }
    else
    {
        return PlayerNoError;
    }
}

void PlayerStream_c::CheckDisableUnsupportedDecimation(Codec_c *Codec)
{
    if ((mStreamType == StreamTypeVideo) && (Codec != NULL))
    {
        //disables the decimation of output video buffers ; if it is not supported by video codec
        int DecimationPolicy = mPlayer->PolicyValue(mPlayback,
                                                    PlayerAllStreams, PolicyDecimateDecoderOutput);

        if (!Codec->IsDecimationValueSupported(DecimationPolicy))
        {
            //Disable decimation if value not supported by codec
            SE_WARNING_ONCE("Decimation disabled\n");
            DecimationPolicy = PolicyValueDecimateDecoderOutputDisabled;
            mPlayer->SetPolicy(mPlayback, PlayerAllStreams, PolicyDecimateDecoderOutput, PolicyValueDecimateDecoderOutputDisabled);
        }
    }
}

PlayerStatus_t PlayerStream_c::InjectBlockList(const DataBlock_t *BlockList,
                                               int BlockCount,
                                               PlayerInputDescriptor_t *InjectedDataDescriptor,
                                               int *NbBlocksConsumed)
{
    OS_LockMutex(&mInputLock);
    PlayerStatus_t Status = mCollator->Input(InjectedDataDescriptor, BlockList, BlockCount, NbBlocksConsumed);
    OS_UnLockMutex(&mInputLock);
    return Status;
}

PlayerStatus_t PlayerStream_c::SetPresentationInterval(
    unsigned long long   IntervalStartNativeTime,
    unsigned long long   IntervalEndNativeTime,
    stm_se_time_format_t NativeTimeFormat)
{
    TimeStamp_c IntervalStart;
    TimeStamp_c IntervalEnd;

    if (ValidTime(IntervalStartNativeTime) && IntervalStartNativeTime != STM_SE_PLAY_TIME_NOT_BOUNDED)
    {
        IntervalStart = TimeStamp_c(IntervalStartNativeTime, NativeTimeFormat);
    }

    if (ValidTime(IntervalEndNativeTime) && IntervalEndNativeTime != STM_SE_PLAY_TIME_NOT_BOUNDED)
    {
        IntervalEnd = TimeStamp_c(IntervalEndNativeTime, NativeTimeFormat);
    }

    OutputTimerStatus_t outputTimerStatus = GetOutputTimer()->SetPresentationInterval(IntervalStart, IntervalEnd);
    if (outputTimerStatus != OutputTimerNoError) { return PlayerError; }

    return PlayerNoError;
}

PlayerStatus_t PlayerStream_c::SetDiscardPts(const TimeStamp_c &DiscardPts)
{
    return GetOutputTimer()->SetDiscardPts(DiscardPts);
}

// //////////////////////////////////////////////////////////////////////////////////////////////////
//
//      Function to get processing time of the capture to rendering (in ms)
//


PlayerStatus_t PlayerStream_c::GetCaptureToRenderLatency(int32_t *value)
{
    PlayerStatus_t  Status = mOutputCoordinator->GetCaptureToRenderLatency(value);
    if (Status != PlayerNoError)
    {
        // fails when mapping not established.
        // In that case we return PlayerBusy to notify that the Time mapping is not established
        SE_DEBUG(group_player, "Could not get latency: mapping not established\n");
        return PlayerBusy;
    }
    return PlayerNoError;
}

PlayerStatus_t PlayerStream_c::ConnectInput()
{
    CollatorStatus_t Status = mCollator->ConnectInput();
    if (Status != CollatorNoError) { return PlayerError; }

    mCollatorInputConnected = true;

    return PlayerNoError;
}

PlayerStatus_t PlayerStream_c::DisconnectInput()
{
    CollatorStatus_t Status = mCollator->DisconnectInput();
    if (Status != CollatorNoError) { return PlayerError; }

    mCollatorInputConnected = false;

    return PlayerNoError;
}
