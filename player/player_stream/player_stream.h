/************************************************************************
Copyright (C) 2003-2014 STMicroelectronics. All Rights Reserved.

This file is part of the Streaming Engine.

Streaming Engine is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License version 2 as
published by the Free Software Foundation.

Streaming Engine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with Streaming Engine; see the file COPYING.  If not, write to the Free
Software Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307,
USA.

The Streaming Engine Library may alternatively be licensed under a
proprietary license from ST.
************************************************************************/

#ifndef PLAYER_STREAM_H
#define PLAYER_STREAM_H

#include "player_generic.h"
#include "player_stream.h"
#include "player_stream_interface.h"
#include "stm_pixel_capture.h"
#include "capture_device_priv.h"
#include "message.h"
#include "shared_ptr.h"
#include "raw_list.h"
#include "coded_configuration.h"

#define  MAX_GET_BUFFER_WAITING_TIME_MS     300
#define  GET_BUFFER_WAITING_PERIOD_MS       10

class PlayerStream_c : public PlayerStreamInterface_c
{
public:
    PlayerStream_c(HavanaPlayer_t           HavanaPlayer,
                   Player_Generic_t         player,
                   PlayerPlayback_t         Playback,
                   HavanaStream_t           HavanaStream,
                   PlayerStreamType_t       StreamType,
                   stm_se_stream_encoding_t Encoding,
                   unsigned int             InstanceId,
                   UserDataSource_t         UserDataSender);

    PlayerStatus_t  FinalizeInit(BufferManager_t BufferManager);

    virtual ~PlayerStream_c();

    void                                ApplyDecoderConfig(unsigned int Config);

    virtual Player_t                    GetPlayer() { return mPlayer; }
    virtual PlayerPlayback_t            GetPlayback() { return mPlayback; }
    virtual BufferType_t                GetCodedFrameBufferType() {return mPlayer->GetCodedFrameBufferType(); }
    virtual HavanaStream_t              GetHavanaStream() { return mHavanaStream; }

    virtual DecodeBufferManager_t       GetDecodeBufferManager() { return mDecodeBufferManager; }
    virtual Collator_t                  GetCollator() { return mCollator; }
    virtual FrameParser_t               GetFrameParser();
    virtual Codec_t                     GetCodec();
    virtual ManifestationCoordinator_t  GetManifestationCoordinator();
    virtual OutputTimer_t               GetOutputTimer();
    virtual ES_Processor_c             *GetEsProcessor() { return mEsProcessor; }
    virtual PlayerStreamType_t          GetStreamType() { return mStreamType; }
    virtual unsigned int                GetInstanceId() { return mInstanceId; }
    virtual unsigned int                GetTransformerId() { return mTransformerId; }
    virtual Message_c                  *GetMessenger() { return &mMessenger; }

    virtual unsigned int                GetNumberOfDecodeBuffers() const { return mNumberOfDecodeBuffers; }

    virtual const PlayerStreamStatistics_t     &GetStatistics();
    virtual void                                ResetStatistics();
    virtual PlayerStreamStatistics_t           &Statistics() { return mStatistics; }

    virtual const PlayerStreamAttributes_t     &GetAttributes();
    virtual void                                ResetAttributes();
    virtual PlayerStreamAttributes_t           &Attributes() { return mAttributes; }

    virtual void                        SetSpeed(Rational_t Speed, PlayDirection_t Direction);

    virtual void                        MarkUnPlayable(unsigned int reason = STM_SE_PLAY_STREAM_MSG_REASON_CODE_STREAM_UNKNOWN, bool force = false, bool noEvent = false);
    virtual bool                        IsUnPlayable() const { return mUnPlayable; }

    virtual void                        Terminate() {   mTerminating = true; }
    virtual bool                        IsTerminating() const { return mTerminating; }

    virtual PlayerStatus_t              GetElementaryBufferLevel(stm_se_ctrl_play_stream_elementary_buffer_level_t *ElementaryBufferLevel);

    virtual PlayerStatus_t              Discontinuity(int discontinuity);

    virtual PlayerStatus_t              Drain(const DrainParameters_t &DrainParameters, unsigned long long *pDrainSequenceNumber);

    virtual PlayerStatus_t              Step();
    virtual PlayerStatus_t              DiscardStep();

    virtual PlayerStatus_t              Switch(stm_se_stream_encoding_t  Encoding);

    virtual PlayerStatus_t              SetAlarm(stm_se_play_stream_alarm_t alarm, bool  enable, void *value);

    virtual PlayerStatus_t              ResetDiscardTrigger();
    virtual PlayerStatus_t              SetDiscardTrigger(stm_se_play_stream_discard_trigger_t const &trigger);

    virtual SharedPtr_c<BufferPool_c>   GetCodedFrameBufferPool(unsigned int         *MaximumCodedFrameSize = NULL);

    virtual void                OnCodecHalted();

    virtual PlayerStatus_t              SetPresentationInterval(unsigned long long Start, unsigned long long End, stm_se_time_format_t NativeTimeFormat);
    virtual PlayerStatus_t              SetDiscardPts(const TimeStamp_c &DiscardPts);

    virtual PlayerStatus_t      LowPowerEnter();
    virtual PlayerStatus_t      LowPowerExit();
    virtual bool                IsLowPowerState() { return mIsLowPowerState; }

    virtual PlayerStatus_t      ConnectInput();
    virtual PlayerStatus_t      DisconnectInput();

    virtual ActiveEdgeInterface_c      *GetParseToDecodeEdge() { return (ActiveEdgeInterface_c *) ParseToDecodeEdge; }

    virtual void                SetLowPowerEnterEvent() { OS_SetEventInterruptible(&mLowPowerEnterEvent); }
    virtual void                WaitForLowPowerExitEvent()
    {
        OS_Status_t WaitStatus = OS_WaitForEventInterruptible(&mLowPowerExitEvent, OS_INFINITE);
        if (WaitStatus == OS_INTERRUPTED)
        {
            SE_INFO(group_player, "wait for LP exit interrupted; LowPowerExitEvent:%d\n", OS_TestEventSet(&mLowPowerExitEvent));
        }
    }

    virtual PlayerStatus_t     InjectBlockList(const DataBlock_t *BlockList,
                                               int BlockCount,
                                               PlayerInputDescriptor_t *InjectedDataDescriptor,
                                               int *NbBlocksConsumed);

    unsigned int getMaxSupportedBitDepth() const { return mMaxSupportedBitDepth; }

    PlayerStatus_t              CreateEsProcessor();
    void                        DestroyEsProcessor();

    PlayerStatus_t              GetDecodeBuffer(stm_pixel_capture_format_t    Format,
                                                unsigned int                  Width,
                                                unsigned int                  Height,
                                                Buffer_t                      *Buffer,
                                                uint32_t                      *LumaAddress,
                                                uint32_t                      *ChromaOffset,
                                                unsigned int                  *Stride,
                                                bool                          NonBlockingInCaseOfFailure);
    PlayerStatus_t              ReturnDecodeBuffer(Buffer_t Buffer);

    PlayerStatus_t              InsertMarkerFrame(MarkerType_t markerType, unsigned long long *sequenceNumber);
    unsigned int                WaitForDrainCompletion(bool Discard);

    virtual PlayerStatus_t      SignalEvent(struct PlayerEventRecord_s *PlayerEvent);

    stm_se_stream_encoding_t    GetEncodingType() { return mEncoding; }

    static bool                 isMemProfileAboveHd(int MemProfilePolicy);

    PlayerStatus_t              GetCaptureToRenderLatency(int32_t *value);

    // See mManifestorSharedLock comment.
    void EnterManifestorSharedSection()     { OS_LockRead(&mManifestorSharedLock); }
    void ExitManifestorSharedSection()      { OS_UnLockRead(&mManifestorSharedLock); }
    void EnterManifestorExclusiveSection()  { OS_LockWrite(&mManifestorSharedLock); }
    void ExitManifestorExclusiveSection()   { OS_UnLockWrite(&mManifestorSharedLock); }

    // Link in PlayerPlayback_c::mListOfStreams.
    Link_c                      mLink;

    CollateToParseEdge_c       *CollateToParseEdge;
    ParseToDecodeEdge_c        *ParseToDecodeEdge;
    DecodeToManifestEdge_c     *DecodeToManifestEdge;
    PostManifestEdge_c         *PostManifestEdge;

    UserDataSource_t            UserDataSender;

    unsigned int                ProcessRunningCount;
    OS_Mutex_t                  StartStopLock;
    OS_Event_t                  StartStopEvent;

    bool                        BuffersComingOutOfManifestation;

    OS_Event_t            SwitchStreamLastOneOutOfTheCodec;
    FrameParser_t         SwitchingToFrameParser;
    Codec_t               SwitchingToCodec;
    bool                  mSwitchingErrorOccurred;
    OS_Semaphore_t        mSemaphoreStreamSwitchCollator;
    OS_Semaphore_t        mSemaphoreStreamSwitchFrameParser;
    OS_Semaphore_t        mSemaphoreStreamSwitchCodec;
    OS_Semaphore_t        mSemaphoreStreamSwitchOutputTimer;
    OS_Semaphore_t        mSemaphoreStreamSwitchComplete;

    CodedConfiguration_t  mCodedConfiguration;

    allocator_device_t    CodedFrameMemoryDevice;
    BufferType_t          TranscodedFrameBufferType;
    BufferType_t          CompressedFrameBufferType;

    BufferType_t          DecodeBufferType;

    BufferType_t          AuxFrameBufferType;
    BufferType_t          MDBufferType;

    PlayerPolicyState_t   PolicyRecord;

    PlayerControslStorageState_t ControlsRecord;

    // Accumulated list of coded data buffers that were not decoded,
    // passed from parse->decode to decode->manifest, used to patch
    // holes in the display frame indices (avoids the latency of waiting
    // for max decodes out of order before correcting for missing indices).

    unsigned int                InsertionsIntoNonDecodedBuffers;
    unsigned int                RemovalsFromNonDecodedBuffers;
    unsigned int                DisplayIndicesCollapse;
    PlayerBufferRecord_t        NonDecodedBuffers[PLAYER_MAX_DISCARDED_FRAMES];
    OS_Mutex_t                  NonDecodedBuffersLock;

    unsigned long long          DecodeCommenceTime;

    //
    // Useful counts/debugging data (added/removed at will
    //

    unsigned int          FramesToManifestorCount;
    unsigned int          FramesFromManifestorCount;

private:
    HavanaPlayer_t                  mHavanaPlayer;
    Player_Generic_t                mPlayer;
    PlayerPlayback_t                mPlayback;
    HavanaStream_t                  mHavanaStream;
    OutputCoordinator_t             mOutputCoordinator;
    DecodeBufferManager_t           mDecodeBufferManager;
    unsigned int                    mNumberOfDecodeBuffers;
    unsigned int                    mMaxSupportedBitDepth;
    bool                            mUnPlayable;
    stm_se_stream_encoding_t        mEncoding;
    bool                            mTerminating;
    PlayerStreamStatistics_t        mStatistics;
    PlayerStreamAttributes_t        mAttributes;
    BufferFormat_t                  mLastGetDecodeBufferFormat;
    Collator_t                      mCollator;
    ES_Processor_c                 *mEsProcessor;
    PlayerStreamType_t              mStreamType;
    OS_Event_t                      mDrained;
    unsigned int                    mInstanceId;
    unsigned int                    mTransformerId;
    bool                            mCollatorInputConnected;
    //
    // Low power data
    //
    bool                            mIsLowPowerState;    // HPS/CPS: indicates when SE is in low power state
    OS_Event_t                      mLowPowerEnterEvent; // HPS/CPS: event used to synchronize with the ParseToDecode thread on low power entry
    OS_Event_t                      mLowPowerExitEvent;  // HPS/CPS: event used to synchronize with the ParseToDecode thread on low power exit

    Message_c                       mMessenger;
    SharedPtr_c<BufferPool_c>       mCodedFrameBufferPool;

    // LOCK ORDERING:
    // mDrainLock < mInputLock

    // Coarse-grained shared/exclusive section preventing addition and removal
    // of manifestors by client threads while the the decode-to-manifest or
    // post-manifest edge threads run.
    // TODO(theryn): The manifestation code is race-prone so eventually we will
    // rewrite it but as a stop-gap measure we address the most frequent race
    // which occurs when a manifestor is removed (bug 60729), typically during
    // main-PIP swap.  We bend the read-write lock concept because edges both
    // read and write the manifestation state but this is for a good cause as
    // allowing both edges to access concurrently the manifestation state
    // minimizes changes to the overall timing of events and is good-enough in
    // practice probably because there are implicit hand-over of data between
    // threads minimizing the number of races.
    OS_RwLock_t                     mManifestorSharedLock;

    // Serialize concurrent drains (e.g. internal vs client-triggered) because
    // drain implementation assumes single drain in flight (there would be at
    // least races on discarding flags otherwise).  Also ensure mutual exclusion
    // between drain and Switch() for reason stated below.
    //
    // All the decode buffer pools must be freed during the Switch() call.
    // This is possible only if we ensure that no buffer is injected in the
    // edges as long as the stream switch is not complete. Otherwise we would
    // fall in a deadlock.
    //
    // Most of the entry points of buffer injection into the edges are mutually
    // exclusive with the stream switch, thanks to mInputLock but the Drain()
    // method, that inserts marker frames, may be called concurrently with
    // Switch() from multiple sources (API or internal).
    //
    // See lock ordering comment above.
    OS_Mutex_t                      mDrainLock;

    // Ensure mutual exclusion between injection and various other operations
    // that rely on absence of injection for correctness (e.g. zap).
    //
    // See lock ordering comment above.
    OS_Mutex_t                      mInputLock;

    // Private methods

    PlayerStatus_t              CreateCodedFrameBufferPool(BufferManager_t BufferManager);
    BufferFormat_t              ConvertVibeBufferFormatToPlayerBufferFormat(stm_pixel_capture_format_t Format);

    virtual PlayerStatus_t      CheckEvent(struct PlayerEventRecord_s *PlayerEvent, stm_se_play_stream_msg_t *streamMsg, stm_event_t *streamEvtMngt);

    PlayerStatus_t              SwitchCollator(stm_se_stream_encoding_t Encoding);

    PlayerStatus_t              CreateStreamObjects(FrameParser_c **FrameParser, Codec_c **Codec,
                                                    OutputTimer_c **OutputTimer, DecodeBufferManager_c **DecodeBufferManager,
                                                    ManifestationCoordinator_c **ManifestationCoordinator);

    void                        CheckDisableUnsupportedDecimation(Codec_c *Codec);
    PlayerStatus_t              ConfigureDecodeBufferManagerForVideo();
    PlayerStatus_t              ConfigureDecodeBufferManagerForAudio();
    static bool                 is10BitProfileSupported(int MemProfilePolicy);
    void                        GetBufferPoolLevel(BufferPool_c *Pool, BufferPoolLevel *Level);
    void                        InitiateDrain(const DrainParameters_t &DrainParameters);
    PlayerStatus_t              CompleteDrain(const DrainParameters_t &DrainParameters, unsigned long long *pDrainSequenceNumber);

    unsigned int                EstimateCodedFrameMaximumSize(unsigned int);

    DISALLOW_COPY_AND_ASSIGN(PlayerStream_c);
};

#endif
