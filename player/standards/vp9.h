/************************************************************************
Copyright (C) 2003-2014 STMicroelectronics. All Rights Reserved.

This file is part of the Streaming Engine.

Streaming Engine is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License version 2 as
published by the Free Software Foundation.

Streaming Engine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with Streaming Engine; see the file COPYING.  If not, write to the Free
Software Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307,
USA.

The Streaming Engine Library may alternatively be licensed under a
proprietary license from ST.
************************************************************************/

#ifndef H_VP9
#define H_VP9

#include <mme.h>



/*-----------------------------------------------------------------------------*/
/* value to be returned by GetBits if stream buffer is empty */
#define END_OF_STREAM 0xFFFFFFFFU

#define HANTRO_OK 0
#define HANTRO_NOK 1

/* Assert functiosn definition */
#define ASSERT(expr, args...) do {\
    if (!(expr)) {\
        SE_ERROR("%s Assert from VP9 decoder\n", __func__);\
    }\
} while (0)

#define CHECK_END_OF_STREAM(s) \
  if ((s) == END_OF_STREAM) return (s)

#define VP9HWPAD(x, y) unsigned char x[y]

#define VP9_KEY_FRAME_START_CODE_0 0x49
#define VP9_KEY_FRAME_START_CODE_1 0x83
#define VP9_KEY_FRAME_START_CODE_2 0x42

#define VP9_FRAME_MARKER 0x2

#define MIN_TILE_WIDTH 256
#define MAX_TILE_WIDTH 4096
#define MIN_TILE_WIDTH_SBS (MIN_TILE_WIDTH >> 6)
#define MAX_TILE_WIDTH_SBS (MAX_TILE_WIDTH >> 6)

#define MAX_REF_LF_DELTAS           4
#define MAX_MODE_LF_DELTAS          2
#define ALLOWED_REFS_PER_FRAME      3

#define NUM_REF_FRAMES              8
#define NUM_REF_FRAMES_LG2          3
#define NUM_FRAME_CONTEXTS_LG2 2

#define MAX_MB_SEGMENTS             8
#define MB_SEG_TREE_PROBS           (MAX_MB_SEGMENTS - 1)
#define PREDICTION_PROBS            3

#define TX_SIZE_CONTEXTS            2

#define VP9_DEF_UPDATE_PROB 252
#define MODULUS_PARAM 13 /* Modulus parameter */
#define INTER_MODE_CONTEXTS 7
#define INTRA_INTER_CONTEXTS 4

#define BLOCK_SIZE_GROUPS 4
#define PARTITION_PLOFFSET 4  // number of probability models per block size
#define NUM_PARTITION_CONTEXTS (4 * PARTITION_PLOFFSET)
#define COMP_PRED_CONTEXTS 2

#define VP9_SWITCHABLE_FILTERS 3 /* number of switchable filters */
#define COMP_INTER_CONTEXTS 5
#define MBSKIP_CONTEXTS 3

#define COEF_UPDATE_PROB 252
#define VP9_PROB_HALF 128
#define VP9_NMV_UPDATE_PROB 252
#define VP9_MV_UPDATE_PRECISION 7
#define MV_JOINTS 4
#define MV_CLASSES 11
#define CLASS0_BITS 1
#define CLASS0_SIZE (1 << CLASS0_BITS)
#define MV_OFFSET_BITS (MV_CLASSES + CLASS0_BITS - 2)

/* Entropy nodes above is divided in two parts, first three probs in part1
 * and the modeled probs in part2. Part1 is padded so that tables align with
 *  32 byte addresses, so there is four bytes for each table. */
#define ENTROPY_NODES_PART1 4
#define ENTROPY_NODES_PART2 8
#define INTER_MODE_CONTEXTS 7

#define INTRA_INTER_CONTEXTS 4
#define COMP_INTER_CONTEXTS 5
#define REF_CONTEXTS 5

#define BLOCK_TYPES 2
#define REF_TYPES 2  // intra=0, inter=1
#define COEF_BANDS 6
#define PREV_COEF_CONTEXTS 6
#define UNCONSTRAINED_NODES 3

/* maximum number of elementary frames possible in one superframe */
#define MAX_VP9_ELEMENTARY_FRAMES 8
#define MAX_VP9_FRAME_HEADER_SIZE 2048 //required for UHD stream usecase
#define MIN_VP9_FRAME_HEADER_SIZE 128

enum MvReferenceFrame
{
//  NONE = -1,
    INTRA_FRAME = 0,
    LAST_FRAME = 1,
    GOLDEN_FRAME = 2,
    ALTREF_FRAME = 3,
    MAX_REF_FRAMES = 4
};

enum InterpolationFilterType
{
    EIGHTTAP_SMOOTH,
    EIGHTTAP,
    EIGHTTAP_SHARP,
    BILINEAR,
    SWITCHABLE /* should be the last one */
};

enum SegLevelFeatures
{
    SEG_LVL_ALT_Q = 0,
    SEG_LVL_ALT_LF = 1,
    SEG_LVL_REF_FRAME = 2,
    SEG_LVL_SKIP = 3,
    SEG_LVL_MAX = 4
};

enum TxfmMode
{
    ONLY_4X4 = 0,
    ALLOW_8X8 = 1,
    ALLOW_16X16 = 2,
    ALLOW_32X32 = 3,
    TX_MODE_SELECT = 4,
    NB_TXFM_MODES = 5,
};

enum TxSize
{
    TX_4X4 = 0,
    TX_8X8 = 1,
    TX_16X16 = 2,
    TX_32X32 = 3,
    TX_SIZE_MAX_SB,
};

enum MbPredictionMode
{
    DC_PRED,  /* average of above and left pixels */
    V_PRED,   /* vertical prediction */
    H_PRED,   /* horizontal prediction */
    D45_PRED, /* Directional 45 deg prediction  [anti-clockwise from 0 deg hor] */
    D135_PRED, /* Directional 135 deg prediction [anti-clockwise from 0 deg hor]
                */
    D117_PRED, /* Directional 112 deg prediction [anti-clockwise from 0 deg hor]
                */
    D153_PRED, /* Directional 157 deg prediction [anti-clockwise from 0 deg hor]
                */
    D27_PRED, /* Directional 22 deg prediction  [anti-clockwise from 0 deg hor] */
    D63_PRED, /* Directional 67 deg prediction  [anti-clockwise from 0 deg hor] */
    TM_PRED,  /* Truemotion prediction */
    NEARESTMV,
    NEARMV,
    ZEROMV,
    NEWMV,
    SPLITMV,
    MB_MODE_COUNT
};

#define VP9_INTRA_MODES (TM_PRED + 1) /* 10 */

#define VP9_INTER_MODES (1 + NEWMV - NEARESTMV)

enum FrameType
{
    KEY_FRAME = 0,
    INTER_FRAME = 1,
    NUM_FRAME_TYPES,
};

enum PartitionType
{
    PARTITION_NONE,
    PARTITION_HORZ,
    PARTITION_VERT,
    PARTITION_SPLIT,
    PARTITION_TYPES
};

struct NmvContext
{
    /* last bytes of address 41 */
    unsigned char joints[MV_JOINTS - 1];
    unsigned char sign[2];
    /* address 42 */
    unsigned char class0[2][CLASS0_SIZE - 1];
    unsigned char fp[2][4 - 1];
    unsigned char class0_hp[2];
    unsigned char hp[2];
    unsigned char classes[2][MV_CLASSES - 1];
    /* address 43 */
    unsigned char class0_fp[2][CLASS0_SIZE][4 - 1];
    unsigned char bits[2][MV_OFFSET_BITS];
};

/* Adaptive entropy contexts, padding elements are added to have
 * 256 bit aligned tables for HW access.
 * Compile with TRACE_PROB_TABLES to print bases for each table. */
typedef struct Vp9AdaptiveEntropyProbs
{
    /* address 32 */
    unsigned char inter_mode_prob[INTER_MODE_CONTEXTS][4];
    unsigned char intra_inter_prob[INTRA_INTER_CONTEXTS];

    /* address 33 */
    unsigned char uv_mode_prob[VP9_INTRA_MODES][8];
    unsigned char tx8x8_prob[TX_SIZE_CONTEXTS][TX_SIZE_MAX_SB - 3];
    unsigned char tx16x16_prob[TX_SIZE_CONTEXTS][TX_SIZE_MAX_SB - 2];
    unsigned char tx32x32_prob[TX_SIZE_CONTEXTS][TX_SIZE_MAX_SB - 1];
    unsigned char sb_ymode_prob_b[BLOCK_SIZE_GROUPS][1];
    unsigned char sb_ymode_prob[BLOCK_SIZE_GROUPS][8];

    /* address 37 */
    unsigned char partition_prob[NUM_FRAME_TYPES][NUM_PARTITION_CONTEXTS][PARTITION_TYPES];

    /* address 41 */
    unsigned char uv_mode_prob_b[VP9_INTRA_MODES][1];
    unsigned char switchable_interp_prob[VP9_SWITCHABLE_FILTERS + 1][VP9_SWITCHABLE_FILTERS -
                                                                     1];
    unsigned char comp_inter_prob[COMP_INTER_CONTEXTS];
    unsigned char mbskip_probs[MBSKIP_CONTEXTS];
    VP9HWPAD(pad1, 1);

    struct NmvContext nmvc;

    /* address 44 */
    unsigned char single_ref_prob[REF_CONTEXTS][2];
    unsigned char comp_ref_prob[REF_CONTEXTS];
    VP9HWPAD(pad2, 17);

    /* address 45 */
    unsigned char prob_coeffs[BLOCK_TYPES][REF_TYPES][COEF_BANDS][PREV_COEF_CONTEXTS]
    [ENTROPY_NODES_PART1];
    unsigned char prob_coeffs8x8[BLOCK_TYPES][REF_TYPES][COEF_BANDS][PREV_COEF_CONTEXTS]
    [ENTROPY_NODES_PART1];
    unsigned char prob_coeffs16x16[BLOCK_TYPES][REF_TYPES][COEF_BANDS][PREV_COEF_CONTEXTS]
    [ENTROPY_NODES_PART1];
    unsigned char prob_coeffs32x32[BLOCK_TYPES][REF_TYPES][COEF_BANDS][PREV_COEF_CONTEXTS]
    [ENTROPY_NODES_PART1];
} Vp9AdaptiveEntropyProbs_t;

/* Entropy contexts */
typedef struct Vp9EntropyProbs
{
    /* Default keyframe probs */
    /* Table formatted for 256b memory, probs 0to7 for all tables followed by
     * probs 8toN for all tables.
     * Compile with TRACE_PROB_TABLES to print bases for each table. */

    unsigned char kf_bmode_prob[VP9_INTRA_MODES][VP9_INTRA_MODES][8];

    /* Address 25 */
    unsigned char kf_bmode_prob_b[VP9_INTRA_MODES][VP9_INTRA_MODES][1];
    unsigned char ref_pred_probs[PREDICTION_PROBS];
    unsigned char mb_segment_tree_probs[MB_SEG_TREE_PROBS];
    unsigned char segment_pred_probs[PREDICTION_PROBS];
    unsigned char ref_scores[MAX_REF_FRAMES];
    unsigned char prob_comppred[COMP_PRED_CONTEXTS];
    VP9HWPAD(pad1, 9);

    /* Address 29 */
    unsigned char kf_uv_mode_prob[VP9_INTRA_MODES][8];
    unsigned char kf_uv_mode_prob_b[VP9_INTRA_MODES][1];
    VP9HWPAD(pad2, 6);

    Vp9AdaptiveEntropyProbs_t a; /* Probs with backward adaptation */
} Vp9EntropyProbs_t;

enum CompPredModeType
{
    SINGLE_PREDICTION_ONLY = 0,
    COMP_PREDICTION_ONLY = 1,
    HYBRID_PREDICTION = 2,
    NB_PREDICTION_TYPES = 3,
};

static const int vp9_seg_feature_data_signed[SEG_LVL_MAX] = {1, 1, 0, 0};
static const int vp9_seg_feature_data_max[SEG_LVL_MAX] = {255, 63, 3, 0};

/*-----------------------------------------------------------------------------*/
/* Frames in the pool = 8 Reference Frame + 1 Current Frame + 3 Scaled Frames  */
#define VP9_NUM_REF_FRAME_POOL          NUM_REF_FRAMES + 4
#define VP9_NUM_REF_FRAME_LISTS         3
#define VP9_REFRESH_GOLDEN_FRAME        1
#define VP9_REFRESH_ALTERNATE_FRAME     1
#define VP9_REFRESH_LAST_FRAME          1

typedef enum
{
    VP9_NO_BUFFER_TO_GOLDEN             = 0,
    VP9_LAST_FRAME_TO_GOLDEN            = 1,
    VP9_ALTERNATE_FRAME_TO_GOLDEN       = 2
} VP9_COPY_BUFFER_TO_GOLDEN;

typedef enum
{
    VP9_NO_BUFFER_TO_ALTERNATE          = 0,
    VP9_LAST_FRAME_TO_ALTERNATE         = 1,
    VP9_GOLDEN_FRAME_TO_ALTERNATE       = 2
} VP9_COPY_BUFFER_TO_ALTERNATE;

// Macroblock level features
typedef enum
{
    VP9_MB_LVL_ALT_Q            = 0,    // Use alternate Quantizer ....
    VP9_MB_LVL_ALT_LF           = 1,    // Use alternate loop filter value...
    VP9_MB_LVL_MAX              = 2,    // Number of MB level features supported
} VP9_MB_LVL_FEATURES;


// Definition of picture_coding_type values
typedef enum
{
    VP9_PICTURE_CODING_TYPE_I,
    VP9_PICTURE_CODING_TYPE_P,
    VP9_PICTURE_CODING_TYPE_NONE,
} VP9PictureCodingType_t;

typedef struct Vp9VideoSequence_s
{
    unsigned int                version;
    unsigned int                profile;
    unsigned int                encoded_width;
    unsigned int                encoded_height;
    unsigned int                decode_width;
    unsigned int                decode_height;
    unsigned int                horizontal_scale;
    unsigned int                vertical_scale;
    unsigned int                color_space;
    unsigned int                clamping;
} Vp9VideoSequence_t;

typedef struct Vp9VideoPicture_s
{
    unsigned int                ptype;
    unsigned int                key_frame;
    unsigned int                show_frame;
    unsigned int                first_part_size;
    unsigned int                loop_filter_level;
    unsigned int                loop_filter_sharpness;
    unsigned int                new_fb_idx;
    unsigned int                width;
    unsigned int                height;
    unsigned int                scaling_active;
    unsigned int                scaled_width;
    unsigned int                scaled_height;
    unsigned int                color_space;
    unsigned int                subsampling_x;
    unsigned int                subsampling_y;
    unsigned int                reset_frame_flags;
    unsigned int                show_existing_frame;
    unsigned int                show_existing_frame_index;
    unsigned int                refresh_frame_flags;
    unsigned int                active_ref_idx[ALLOWED_REFS_PER_FRAME];
    unsigned int                error_resilient;
    unsigned int                existing_ref_map;
    unsigned int                intra_only;
    unsigned int                reset_frame_context;
    unsigned int                ref_frame_sign_bias[MAX_REF_FRAMES];
    unsigned int                resolution_change;
    unsigned int                allow_high_precision_mv;
    unsigned int                mcomp_filter_type;
    unsigned int                allow_comp_inter_inter;
    unsigned int                comp_fixed_ref;
    unsigned int                comp_var_ref[MAX_REF_LF_DELTAS];
    unsigned int                refresh_entropy_probs;
    unsigned int                frame_parallel_decoding;
    unsigned int                frame_context_idx;
    unsigned int                mode_ref_lf_enabled;
    unsigned int                mb_ref_lf_delta[MAX_REF_LF_DELTAS];
    unsigned int                mb_mode_lf_delta[MAX_MODE_LF_DELTAS];
    int                         qp_yac, qp_ydc, qp_y2_ac, qp_y2_dc, qp_ch_ac, qp_ch_dc;
    unsigned int                lossless;
    unsigned int                segment_enabled;
    unsigned int                segment_map_update;
    unsigned int                segment_map_temporal_update;
    unsigned int                segment_feature_mode; /* ABS data or delta data */
    unsigned int                segment_feature_enable[MAX_MB_SEGMENTS][SEG_LVL_MAX];
    int                         segment_feature_data[MAX_MB_SEGMENTS][SEG_LVL_MAX];
    unsigned int                log2_tile_columns;
    unsigned int                log2_tile_rows;
    unsigned int                offset_to_dct_parts;
    unsigned int                frame_tag_size;

    /* Segment prob parameters */
    unsigned char               mb_segment_tree_probs[MB_SEG_TREE_PROBS];
    unsigned char               segment_pred_probs[PREDICTION_PROBS];

} Vp9VideoPicture_t;

typedef struct Vp9StreamParameters_s
{
    bool                        UpdatedSinceLastFrame;

    bool                        SequenceHeaderPresent;

    Vp9VideoSequence_t          SequenceHeader;
} Vp9StreamParameters_t;

#define BUFFER_VP9_STREAM_PARAMETERS            "Vp9StreamParameters"
#define BUFFER_VP9_STREAM_PARAMETERS_TYPE       {BUFFER_VP9_STREAM_PARAMETERS, BufferDataTypeBase, AllocateFromOSMemory, 4, 0, true, true, sizeof(Vp9StreamParameters_t)}

typedef struct Vp9FrameParameters_s
{
    unsigned int                ForwardReferenceIndex;
    unsigned int                BackwardReferenceIndex;

    bool                        PictureHeaderPresent;

    Vp9VideoPicture_t           PictureHeader;

} Vp9FrameParameters_t;

#define BUFFER_VP9_FRAME_PARAMETERS             "Vp9FrameParameters"
#define BUFFER_VP9_FRAME_PARAMETERS_TYPE        {BUFFER_VP9_FRAME_PARAMETERS, BufferDataTypeBase, AllocateFromOSMemory, 4, 0, true, true, sizeof(Vp9FrameParameters_t)}


/* The following structure is passed in by the application layer */
typedef struct Vp9MetaData_s
{
    unsigned int         Codec;
    unsigned int         Width;
    unsigned int         Height;
    unsigned int         Duration;
    unsigned int         FrameRate;              /* x1000000 */
} Vp9MetaData_t;


#endif

