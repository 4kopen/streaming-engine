/************************************************************************
Copyright (C) 2003-2014 STMicroelectronics. All Rights Reserved.

This file is part of the Streaming Engine.

Streaming Engine is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License version 2 as
published by the Free Software Foundation.

Streaming Engine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with Streaming Engine; see the file COPYING.  If not, write to the Free
Software Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307,
USA.

The Streaming Engine Library may alternatively be licensed under a
proprietary license from ST.
************************************************************************/

#ifndef H_AUDIO_MODE_CLASS
#define H_AUDIO_MODE_CLASS


#include "audio_conversions.h"

class Audio_Mode_c
{
public:
    inline Audio_Mode_c()
        : mAudioMode(ACC_MODE_ID)
        , mNbChannels(0)
        , mAssignment()
    {
    }

    inline ~Audio_Mode_c()
    {
    }

    inline void Update(enum eAccAcMode Mode)
    {
        if (mAudioMode != Mode)
        {
            mAudioMode  = Mode;
            mNbChannels = StmSeAudioMode2NbChannels(Mode);
            SetAssignment(Mode);
        }
    }

    inline unsigned char GetAudioMode() const
    {
        return mAudioMode;
    }

    inline unsigned char GetNbChannels() const
    {
        return mNbChannels;
    }

    inline void GetChannelAssignment(stm_se_audio_channel_assignment *assignment) const
    {
        *assignment = mAssignment;
    }

    inline bool IsDualMono() const
    {
        return (ACC_MODE_1p1 == mAudioMode);
    }

private:
    enum eAccAcMode                 mAudioMode;
    unsigned char                   mNbChannels;
    stm_se_audio_channel_assignment mAssignment;

////////////////////////////////////////////////////////////////////////////
///
/// Lookup the 'natural' channel assignment for an audio mode.
///
/// This method and Mixer_Mme_c::TranslateChannelAssignmentToAudioMode are *not* reversible for
/// audio modes that are not marked as suitable for output.
///
    void SetAssignment(enum eAccAcMode Mode)
    {
        if (ACC_MODE_ID == Mode)
        {
            mAssignment = stm_se_audio_channel_assignment();
            mAssignment.malleable = 1;
        }
        else
        {
            if (0 != StmSeAudioGetChannelAssignmentFromAcMode(&mAssignment, Mode))
            {
                SE_ERROR("StmSeAudioGetAcModeFromChannelAssignment Failed\n");
                mAssignment = stm_se_audio_channel_assignment();
            }
            else
            {
                if ((STM_SE_AUDIO_CHANNEL_PAIR_NOT_CONNECTED == mAssignment.pair0)
                    && (STM_SE_AUDIO_CHANNEL_PAIR_NOT_CONNECTED == mAssignment.pair1)
                    && (STM_SE_AUDIO_CHANNEL_PAIR_NOT_CONNECTED == mAssignment.pair2)
                    && (STM_SE_AUDIO_CHANNEL_PAIR_NOT_CONNECTED == mAssignment.pair3)
                    && (STM_SE_AUDIO_CHANNEL_PAIR_NOT_CONNECTED == mAssignment.pair4))
                {
                    // no point in stringizing the mode - we know its not in the table
                    SE_ERROR("Cannot find matching audio mode (%d)\n", Mode);
                    mAssignment = stm_se_audio_channel_assignment();
                }
            }
        }
    }
};

#endif // H_AUDIO_MODE_CLASS
