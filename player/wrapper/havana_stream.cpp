/************************************************************************
Copyright (C) 2003-2014 STMicroelectronics. All Rights Reserved.

This file is part of the Streaming Engine.

Streaming Engine is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License version 2 as
published by the Free Software Foundation.

Streaming Engine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with Streaming Engine; see the file COPYING.  If not, write to the Free
Software Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307,
USA.

The Streaming Engine Library may alternatively be licensed under a
proprietary license from ST.
************************************************************************/

#include <stm_event.h>

#include "osinline.h"
#include "mpeg2.h"
#include "player.h"
#include "output_timer_base.h"
#include "codec_mme_base.h"
#include "decode_buffer_manager_base.h"
#include "manifestor_audio.h"
#include "manifestor_video.h"
#include "havana_player.h"
#include "havana_playback.h"
#include "havana_capture.h"
#include "havana_user_data.h"
#include "pes.h"
#include "mixer_mme.h"

#include "havana_display.h"
#include "player_policy.h"
#include "havana_stream.h"

#include "audio_mixer_params.h"

//{{{  HavanaStream_c
HavanaStream_c::HavanaStream_c()
    : UserDataSender()
    , ManifestorLock()
    , PlayerStreamType(StreamTypeNone)
    , PlayerStream(NULL)
    , HavanaPlayer(NULL)
    , HavanaPlayback(NULL)
    , HavanaCapture(NULL)
    , Player(NULL)
    , Manifestor()
    , AudioPassThroughLock()
    , NbAudioPlayerSinks(0)
    , AudioPlayers()
    , AudioPassThrough(NULL)
    , Encoding(STM_SE_STREAM_ENCODING_AUDIO_NONE)
{
    OS_InitializeMutex(&ManifestorLock);
    OS_InitializeMutex(&AudioPassThroughLock);
}
//}}}
//{{{  ~HavanaStream_c
HavanaStream_c::~HavanaStream_c()
{
    if (PlayerStream != NULL)
    {
        // Remove stream from playback
        HavanaPlayback->GetPlayerPlayback()->RemoveStream(PlayerStream);
    }

    delete HavanaCapture;

    OS_TerminateMutex(&ManifestorLock);
    OS_TerminateMutex(&AudioPassThroughLock);
}
//}}}
//{{{  Init
//{{{  doxynote
/// \brief Create and initialise all of the necessary player components for a new player stream
/// \param HavanaPlayer         Grandparent class
/// \param Player               The player
/// \param HavanaPlayback       The havana playback to which the stream will be added
/// \param Media                Textual description of media (audio or video)
/// \param Encoding             The encoding of the stream content (MPEG2/H264 etc)
/// \param Multiplex            Name of multiplex if present (TS)
/// \return                     Havana status code, HavanaNoError indicates success.
//}}}
HavanaStatus_t HavanaStream_c::Init(class HavanaPlayer_c           *InitHavanaPlayer,
                                    class Player_c                 *InitPlayer,
                                    class HavanaPlayback_c         *InitHavanaPlayback,
                                    stm_se_media_t                  Media,
                                    stm_se_stream_encoding_t        InitEncoding,
                                    unsigned int                    Id)
{
    HavanaStatus_t Status          = HavanaNoError;
    PlayerStatus_t PlayerStatus    = PlayerNoError;

    SE_VERBOSE(group_havana, "Media %d Encoding %d\n", Media, InitEncoding);

    Player                = InitPlayer;
    HavanaPlayer          = InitHavanaPlayer;
    HavanaPlayback        = InitHavanaPlayback;

    if (Media == STM_SE_MEDIA_VIDEO)
    {
        PlayerStreamType  = StreamTypeVideo;
    }
    else if (Media == STM_SE_MEDIA_AUDIO)
    {
        PlayerStreamType  = StreamTypeAudio;
    }
    else
    {
        SE_ERROR("Invalid media type %d\n", Media);
        return HavanaError;
    }

    if ((InitEncoding == STM_SE_STREAM_ENCODING_VIDEO_UNCOMPRESSED) || (InitEncoding == STM_SE_STREAM_ENCODING_VIDEO_CAP))
    {
        PlayerParameterBlock_t  PlayerParameters;
        PlayerParameters.ParameterType               = PlayerSetCodedFrameBufferParameters;
        PlayerParameters.CodedFrame.StreamType       = StreamTypeVideo;
        PlayerParameters.CodedFrame.Configuration.FrameCount  = DVP_CODED_FRAME_COUNT;

        if (InitEncoding == STM_SE_STREAM_ENCODING_VIDEO_UNCOMPRESSED)
        {
            PlayerParameters.CodedFrame.Configuration.MemorySize = DVP_FRAME_MEMORY_SIZE;
        }
        else
        {
            PlayerParameters.CodedFrame.Configuration.MemorySize = DVP_FRAME_MEMORY_SIZE / 2;
        }

        PlayerParameters.CodedFrame.Configuration.FrameMaximumSize = DVP_MAXIMUM_FRAME_SIZE;
        strncpy(PlayerParameters.CodedFrame.Configuration.MemoryPartitionName, DVP_MEMORY_PARTITION,
                sizeof(PlayerParameters.CodedFrame.Configuration.MemoryPartitionName));
        PlayerParameters.CodedFrame.Configuration.MemoryPartitionName[
            sizeof(PlayerParameters.CodedFrame.Configuration.MemoryPartitionName) - 1] = '\0';

        Player->SetModuleParameters(HavanaPlayback->GetPlayerPlayback(), PlayerAllStreams,
                                    sizeof(PlayerParameterBlock_t), &PlayerParameters);
    }

    BufferManager_t BufferManager;
    Status = HavanaPlayer->GetBufferManager(&BufferManager);
    if (Status != HavanaNoError)
    {
        SE_ERROR("Not able to retrieve BufferManager\n");
        return HavanaError;
    }

    PlayerStatus = HavanaPlayback->GetPlayerPlayback()->AddStream(HavanaPlayer,
                                                                  &PlayerStream,
                                                                  PlayerStreamType,
                                                                  InitEncoding,
                                                                  Id,
                                                                  this,
                                                                  BufferManager,
                                                                  (UserDataSource_t)&UserDataSender);
    if (PlayerStatus != PlayerNoError)
    {
        SE_ERROR("Unable to create player stream\n");
        return HavanaNoMemory;
    }

    // User Data sender init
    Status      = UserDataSender.Init(PlayerStream);
    if (Status != HavanaNoError)
    {
        return Status;
    }

    Player->SpecifySignalledEvents(HavanaPlayback->GetPlayerPlayback(), PlayerStream, EventAllEvents);

    // Set current Speed on new Stream object
    Rational_t Speed;
    PlayDirection_t Direction;
    HavanaPlayback->GetPlayerPlayback()->GetSpeed(&Speed, &Direction);
    PlayerStream->SetSpeed(Speed, Direction);

    Encoding = InitEncoding;

    return HavanaNoError;
}
//}}}
//{{{  AddManifestor
HavanaStatus_t HavanaStream_c::AddManifestor(class Manifestor_c     *NewManifestor)
{
    PlayerStatus_t      PlayerStatus    = PlayerNoError;
    HavanaStatus_t      HavanaStatus    = HavanaNoError;
    unsigned int        Index;

    if (PlayerStream->GetManifestationCoordinator() == NULL)
    {
        SE_ERROR("Unable to add Manifestor - ManifestationCoordinator is NULL\n");
        return HavanaNoMemory;
    }

    OS_LockMutex(&ManifestorLock);
    HavanaStatus      = FindManifestor(NULL, &Index);
    if (HavanaStatus != HavanaNoError)
    {
        SE_ERROR("Unable to add Manifestor - too many manifestors\n");
        OS_UnLockMutex(&ManifestorLock);
        return HavanaError;
    }

    SE_VERBOSE(group_havana, "Adding %p\n", NewManifestor);
    PlayerStatus      = PlayerStream->GetManifestationCoordinator()->AddManifestation(NewManifestor, NULL);
    if (PlayerStatus != PlayerNoError)
    {
        SE_ERROR("Unable to add Manifestor - %d\n", PlayerStatus);
        OS_UnLockMutex(&ManifestorLock);
        return HavanaError;
    }

    Manifestor[Index]   = NewManifestor;

    if (PlayerStreamType == StreamTypeVideo)
    {
        class Manifestor_Video_c       *VideoManifestor         = (class Manifestor_Video_c *)NewManifestor;
        unsigned int                    ManifestorCapabilities;
        VideoManifestor->GetCapabilities(&ManifestorCapabilities);

        if ((ManifestorCapabilities & MANIFESTOR_CAPABILITY_GRAB) != 0)
        {
            if (HavanaCapture == NULL)
            {
                HavanaCapture           = new HavanaCapture_c();
            }

            if (HavanaCapture != NULL)
            {
                HavanaCapture->Init((class Manifestor_Base_c *)NewManifestor);
            }
            else
            {
                SE_ERROR("Unable to create capture classes - frame grab not enabled\n");
            }
        }
    }

    OS_UnLockMutex(&ManifestorLock);
    return HavanaNoError;
}
//}}}
//{{{  RemoveManifestor
HavanaStatus_t HavanaStream_c::RemoveManifestor(class Manifestor_c  *OldManifestor)
{
    PlayerStatus_t      PlayerStatus    = PlayerNoError;
    HavanaStatus_t      HavanaStatus    = HavanaNoError;
    unsigned int        Index;

    if (PlayerStream->GetManifestationCoordinator() == NULL)
    {
        SE_ERROR("Unable to remove Manifestor - ManifestationCoordinator is NULL\n");
        return HavanaNoMemory;
    }

    OS_LockMutex(&ManifestorLock);
    HavanaStatus      = FindManifestor(OldManifestor, &Index);
    if (HavanaStatus != HavanaNoError)
    {
        SE_ERROR("Unable to remove Manifestor - Manifestor not found\n");
        OS_UnLockMutex(&ManifestorLock);
        return HavanaStatus;
    }

    Manifestor[Index]   = NULL;

    PlayerStatus      = PlayerStream->GetManifestationCoordinator()->RemoveManifestation(OldManifestor, NULL);
    if (PlayerStatus != PlayerNoError)
    {
        SE_ERROR("Unable to remove Manifestor %d\n", PlayerStatus);
        OS_UnLockMutex(&ManifestorLock);
        return HavanaError;
    }

    if (PlayerStreamType == StreamTypeVideo)
    {
        class Manifestor_Video_c       *VideoManifestor         = (class Manifestor_Video_c *)OldManifestor;
        unsigned int                    ManifestorCapabilities;
        VideoManifestor->GetCapabilities(&ManifestorCapabilities);

        if ((ManifestorCapabilities & MANIFESTOR_CAPABILITY_GRAB) != 0)
        {
            if (HavanaCapture != NULL)
            {
                HavanaCapture->DeInit((class Manifestor_Base_c *)OldManifestor);
            }
            else
            {
                SE_ERROR("Unable to DeInit capture classes - frame grab not disabled\n");
            }
        }
    }

    OS_UnLockMutex(&ManifestorLock);
    return HavanaNoError;
}
//}}}
//{{{  FindManifestor
//     This functions must be locked (ManifestorLock) in caller function
HavanaStatus_t HavanaStream_c::FindManifestor(class Manifestor_c *ManifestorToFind, unsigned int *Index)
{
    int         i;
    SE_VERBOSE(group_havana, "\n");
    OS_AssertMutexHeld(&ManifestorLock);

    for (i = 0; i < MAX_MANIFESTORS; i++)
    {
        if (Manifestor[i] == ManifestorToFind)
        {
            break;
        }
    }

    if (i < MAX_MANIFESTORS)
    {
        *Index          = i;
    }
    else
    {
        return HavanaError;
    }

    return HavanaNoError;
}
//}}}
//{{{  FindManifestorByCapability
HavanaStatus_t HavanaStream_c::FindManifestorByCapability(unsigned int Capability, class Manifestor_c **MatchingManifestor)
{
    int         i;
    SE_VERBOSE(group_havana, "\n");
    OS_LockMutex(&ManifestorLock);

    for (i = 0; i < MAX_MANIFESTORS; i++)
    {
        if (Manifestor[i] != NULL)
        {
            unsigned int                    ManifestorCapabilities;
            Manifestor[i]->GetCapabilities(&ManifestorCapabilities);

            if ((ManifestorCapabilities & Capability) != 0)
            {
                break;
            }
        }
    }

    if (i < MAX_MANIFESTORS)
    {
        *MatchingManifestor     = Manifestor[i];
    }

    OS_UnLockMutex(&ManifestorLock);
    return (i < MAX_MANIFESTORS) ? HavanaNoError : HavanaError;
}
//}}}
//{{{ Input data injection

// Helper for single-packet injections.
HavanaStatus_t HavanaStream_c::InjectOneBlock(const void              *Data,
                                              unsigned int             DataLength,
                                              PlayerInputDescriptor_t *InjectedDataDescriptor)
{
    DataBlock_t Block;
    Block.DataAddr = const_cast<void *>(Data);
    Block.Len = DataLength;

    int NbBlocksConsumed;
    return InjectBlockList(&Block, 1, InjectedDataDescriptor, &NbBlocksConsumed);
}

// Inject in collator BlockCount blocks of data stored in BlockList array.
// Return in *NbBlocksConsumed the number of block collated.
HavanaStatus_t HavanaStream_c::InjectBlockList(const DataBlock_t        *BlockList,
                                               int                       BlockCount,
                                               PlayerInputDescriptor_t  *InjectedDataDescriptor,
                                               int                      *NbBlocksConsumed)
{
    SE_VERBOSE(group_havana, "Stream 0x%p count %d\n", GetStream(), BlockCount);

    PlayerStatus_t Status = PlayerStream->InjectBlockList(BlockList, BlockCount,
                                                          InjectedDataDescriptor, NbBlocksConsumed);

    return (Status == PlayerNoError) ? HavanaNoError : HavanaError;
}
//}}}

//{{{  Discontinuity
HavanaStatus_t HavanaStream_c::Discontinuity(int discontinuity)
{
    SE_VERBOSE(group_havana, "discontinuity:0x%x\n", discontinuity);

    PlayerStatus_t status = PlayerStream->Discontinuity(discontinuity);
    if (status != PlayerNoError) { return HavanaError; }

    return HavanaNoError;
}
//}}}
//{{{  Drain
HavanaStatus_t HavanaStream_c::Drain(bool   Discard)
{
    SE_VERBOSE(group_havana, "Discard %d\n", Discard);

    // Drain the play stream (blocking mode)
    PlayerStreamInterface_c::DrainParameters_t   DrainParameters(true, Discard);
    PlayerStatus_t Status = PlayerStream->Drain(DrainParameters, NULL);
    if (PlayerNoError != Status)
    {
        if (PlayerTimedOut == Status)
        {
            SE_ERROR("DrainStream failed with timeout\n");
            return HavanaTimedOut;
        }
        else
        {
            SE_ERROR("DrainStream failed\n");
            return HavanaError;
        }
    }
    // i.e. we wait rather than use the event
    return HavanaNoError;
}
//}}}
//{{{  EnableOrDisable
HavanaStatus_t HavanaStream_c::EnableOrDisable(bool IsEnable)
{
    OS_LockMutex(&ManifestorLock);
    SE_VERBOSE(group_havana, "%s\n", IsEnable ? "enable" : "disable");

    if (PlayerStreamType == StreamTypeVideo)
    {
        for (int i = 0; i < MAX_MANIFESTORS; i++)
        {
            if (Manifestor[i] != NULL)
            {
                //
                // Video blank/unblanck and Audio mute/un-mute feature
                // is currently only supported by Manifestor with MANIFESTOR_CAPABILITY_DISPLAY capability
                //
                unsigned int ManifestorCapabilities;
                Manifestor[i]->GetCapabilities(&ManifestorCapabilities);
                if ((ManifestorCapabilities & (MANIFESTOR_CAPABILITY_DISPLAY)) == MANIFESTOR_CAPABILITY_DISPLAY)
                {
                    class Manifestor_Video_c   *VideoManifestor = (class Manifestor_Video_c *)Manifestor[i];

                    int vidstatus;
                    if (IsEnable)
                    {
                        vidstatus = VideoManifestor->Enable();
                    }
                    else
                    {
                        vidstatus = VideoManifestor->Disable();
                    }

                    if (vidstatus)
                    {
                        OS_UnLockMutex(&ManifestorLock);
                        SE_ERROR("Failed to %s video output surface; status:%d\n",
                                 IsEnable ? "enable" : "disable", vidstatus);
                        if (vidstatus == -EAGAIN)
                        {
                            return HavanaBusy;
                        }
                        else
                        {
                            return HavanaError;
                        }
                    }
                }
            }
        }
    }
    else if (PlayerStreamType == StreamTypeAudio)
    {
        SE_WARNING("[deprecated API] stm_se_play_stream_set_enable for audio streams\n"
                   "Use stm_se_play_stream_set_control(stream, STM_SE_CTRL_PLAY_STREAM_MUTE, mute) instead\n");
        this->SetControl(STM_SE_CTRL_PLAY_STREAM_MUTE,
                         IsEnable ? STM_SE_CTRL_VALUE_DISAPPLY : STM_SE_CTRL_VALUE_APPLY);
    }
    else
    {
        SE_ERROR("Unknown stream type\n");
        OS_UnLockMutex(&ManifestorLock);
        return HavanaError;
    }

    OS_UnLockMutex(&ManifestorLock);
    return HavanaNoError;
}
//}}}
//{{{  GetEnable
HavanaStatus_t HavanaStream_c::GetEnable(bool *IsEnable)
{
    HavanaStatus_t status = HavanaNoError;
    bool enabled = false;

    OS_LockMutex(&ManifestorLock);

    if (PlayerStreamType == StreamTypeVideo)
    {
        for (int i = 0; i < MAX_MANIFESTORS; i++)
        {
            if (Manifestor[i] != NULL)
            {
                //
                // Video blank/unblanck and Audio mute/un-mute feature
                // is currently only supported by Manifestor with MANIFESTOR_CAPABILITY_DISPLAY capability
                //
                unsigned int ManifestorCapabilities;
                Manifestor[i]->GetCapabilities(&ManifestorCapabilities);

                if ((ManifestorCapabilities & (MANIFESTOR_CAPABILITY_DISPLAY)) == MANIFESTOR_CAPABILITY_DISPLAY)
                {
                    class Manifestor_Video_c       *VideoManifestor = (class Manifestor_Video_c *)Manifestor[i];
                    enabled = VideoManifestor->GetEnable();
                    break;
                }
            }
        }
    }
    else if (PlayerStreamType == StreamTypeAudio)
    {
        int muted = STM_SE_CTRL_VALUE_DISAPPLY;
        GetControl(STM_SE_CTRL_PLAY_STREAM_MUTE, &muted);
        if (muted != STM_SE_CTRL_VALUE_APPLY) { enabled = true; }
    }
    else
    {
        SE_ERROR("Unknown stream type\n");
        status = HavanaError;
    }

    OS_UnLockMutex(&ManifestorLock);

    *IsEnable = enabled;
    return status;
}
//}}}
//{{{  SetOption
HavanaStatus_t HavanaStream_c::SetOption(PlayerPolicy_t          Option,
                                         int                     Value)
{
    PlayerStream_t Stream  = PlayerStream;
    SE_VERBOSE(group_havana, "%d, 0x%x\n", Option, Value);
    // Value is "known" to be in range because it comes from a lookup table in the wrapper

    if (Option == PolicyDecimateDecoderOutput)
    {
        bool unauthorizedDecimatedPolicy = false;
        int DecimationPolicy = Player->PolicyValue(HavanaPlayback->GetPlayerPlayback(),
                                                   Stream, PolicyDecimateDecoderOutput);
        if ((DecimationPolicy == PolicyValueDecimateDecoderOutputDisabled) &&
            (Value != PolicyValueDecimateDecoderOutputDisabled))
        {
            // No decimated buffer have been allocated
            unauthorizedDecimatedPolicy = true;
        }
        else if ((DecimationPolicy == PolicyValueDecimateDecoderOutputQuarter) &&
                 (Value == PolicyValueDecimateDecoderOutputHalf))
        {
            // Decimated buffer size is too small
            unauthorizedDecimatedPolicy = true;
        }
        if (unauthorizedDecimatedPolicy)
        {
            SE_ERROR("Unable to enable decimation at stream level (policy: %d value: %d)\n",
                     Option, Value);
            return HavanaError;
        }
    }

    if (Option == PolicyExternalTimeMapping)
    {
        // todo: is this behaviour correct ?
        // if a policy is not applicable on a stream, should we really apply it to all streams ?
        // should we return an error instead ?
        Stream          = PlayerAllStreams;
    }

    bool policyHasChanged = (Value != Player->PolicyValue(HavanaPlayback->GetPlayerPlayback(), Stream, Option));

    PlayerStatus_t Status = Player->SetPolicy(HavanaPlayback->GetPlayerPlayback(), Stream, Option, Value);
    if (Status != PlayerNoError)
    {
        SE_ERROR("Unable to set stream option %d, 0x%x\n", Option, Value);
        return HavanaError;
    }

    if (policyHasChanged)
    {
        SE_DEBUG(group_havana, "Stream policy %d has changed\n", Option);

        if ((PolicyAudioDualMonoChannelSelection == Option) ||
            (PolicyAudioStreamDrivenDualMono     == Option) ||
            (PolicyAudioDeEmphasis               == Option) ||
            (PolicyAudioSubstreamId              == Option))
        {
            UpdateStreamParams(NEW_DUALMONO_CONFIG | NEW_EMPHASIS_CONFIG);
        }
    }

    return HavanaNoError;
}
//}}}
//{{{  GetOption
HavanaStatus_t HavanaStream_c::GetOption(PlayerPolicy_t          Option,
                                         int                    *Value)
{
    *Value = Player->PolicyValue(HavanaPlayback->GetPlayerPlayback(), PlayerStream, Option);
    SE_VERBOSE(group_havana, "%d, %d\n", Option, *Value);
    return HavanaNoError;
}
//}}}
//{{{  Step
HavanaStatus_t HavanaStream_c::Step()
{
    SE_VERBOSE(group_havana, "\n");

    PlayerStatus_t Status = HavanaPlayback->GetPlayerPlayback()->Step(PlayerStream);
    if (Status != PlayerNoError)
    {
        SE_ERROR("Failed to step stream\n");
        return HavanaError;
    }

    return HavanaNoError;
}
//}}}
//{{{  SetPlayInterval
HavanaStatus_t HavanaStream_c::SetPlayInterval(unsigned long long Start, unsigned long long End, stm_se_time_format_t NativeTimeFormat)
{
    PlayerStatus_t Status = PlayerStream->SetPresentationInterval(Start, End, NativeTimeFormat);
    if (Status != PlayerNoError)
    {
        SE_ERROR("Failed to set play interval - Status = %x\n", Status);
        return HavanaError;
    }

    return HavanaNoError;
}
//}}}
//{{{  ResetStatistics
HavanaStatus_t HavanaStream_c::ResetStatistics()
{
    PlayerStream->ResetStatistics();
    return (HavanaNoError);
}
//}}}
//{{{  GetStatistics
HavanaStatus_t HavanaStream_c::GetStatistics(struct statistics_s *Statistics)
{
    SE_ASSERT(sizeof(statistics_s) == sizeof(PlayerStreamStatistics_t));
    *(PlayerStreamStatistics_t *)Statistics = PlayerStream->GetStatistics();
    return (HavanaNoError);
}
//}}}
//{{{  ResetAttributes
HavanaStatus_t HavanaStream_c::ResetAttributes()
{
    PlayerStream->ResetAttributes();
    return (HavanaNoError);
}
//}}}
//{{{  GetAttributes
HavanaStatus_t HavanaStream_c::GetAttributes(struct attributes_s *Attributes)
{
    SE_ASSERT(sizeof(struct attributes_s) == sizeof(PlayerStreamAttributes_t)); // n.c...
    *(PlayerStreamAttributes_t *) Attributes = PlayerStream->GetAttributes();
    return (HavanaNoError);
}
//}}}
//{{{  StreamType
PlayerStreamType_t HavanaStream_c::StreamType()
{
    SE_VERBOSE(group_havana, "\n");
    return PlayerStreamType;
}
//}}}
//{{{  GetPlayInfo
// API limitation: can get info from *only* one manifestor
// choose to have higher prio for DISPLAY vs GRAB vs ENCODE
// if multiple manifestors of same type (ENCODE for instance),
// then will take first one..

HavanaStatus_t HavanaStream_c::GetPlayInfo(stm_se_play_stream_info_t *PlayInfo)
{
    SE_VERBOSE(group_havana, "%p\n", Manifestor);

    PlayInfo->system_time       = OS_GetTimeInMicroSeconds();
    PlayInfo->presentation_time = INVALID_TIME;
    PlayInfo->presentation_time_pts = INVALID_TIME;
    PlayInfo->pts               = INVALID_TIME;
    PlayInfo->frame_count       = 0ull;

    // In non synchronized mode ,no mapping between real time and native PTS time
    // without A/V synchro there is no relation between the sysclock and the stream pts time
    if ((Player->PolicyValue(HavanaPlayback->GetPlayerPlayback(), PlayerStream, PolicyAVDSynchronization) == PolicyValueApply))
    {
        // no trace in case of error : done under RetrieveNativePlaybackTime
        // in case the time mapping is not established, returned status will be PlayerBusy
        // but discard status: acts upon validity of presentation_time
        HavanaPlayback->GetPlayerPlayback()->RetrieveNativePlaybackTime(&PlayInfo->presentation_time, TIME_FORMAT_US);
    }

    char ManifestorTypeStr[64] = "UNKNOWN";

    long long LargestPTS = INVALID_TIME;
    int LargestPTSManifestorIndex = -1;

    OS_LockMutex(&ManifestorLock);

    for (int i = 0; i < MAX_MANIFESTORS; i++)
    {
        if (Manifestor[i] != NULL)
        {
            unsigned int ManifestorCapabilities;
            Manifestor[i]->GetCapabilities(&ManifestorCapabilities);

            unsigned long long pts;
            unsigned long long frame_count;

            // Get count of manifested frames and system time
            Manifestor[i]->GetFrameCount(&frame_count);

            // Get Last Manifested PTS
            if (Manifestor[i]->GetNativeTimeOfCurrentlyManifestedFrame(&pts) != ManifestorNoError || pts == INVALID_TIME)
            {
                SE_DEBUG(group_havana, "Stream 0x%p Unable to get last manifested pts from manifestor %d\n", GetStream(), i);
                continue;
            }

            if (SE_IS_VERBOSE_ON(group_avsync) || SE_IS_VERBOSE_ON(group_havana))
            {
                // update string for debug
                if ((ManifestorCapabilities & MANIFESTOR_CAPABILITY_DISPLAY) != 0)
                {
                    strncpy(ManifestorTypeStr, "Display", 63);
                }
                else if ((ManifestorCapabilities & MANIFESTOR_CAPABILITY_GRAB) != 0)
                {
                    strncpy(ManifestorTypeStr, "Grab", 63);
                }
                else if ((ManifestorCapabilities & MANIFESTOR_CAPABILITY_ENCODE) != 0)
                {
                    strncpy(ManifestorTypeStr, "Encode", 63);
                }
            }

            // fill info
            if (Manifestor[i]->IsMaster() // Is there always a Master ?
                || !ValidTime(LargestPTS)
                || pts > LargestPTS)
            {
                LargestPTS = pts;
                LargestPTSManifestorIndex = i;

                // update
                PlayInfo->pts = pts;
                PlayInfo->frame_count = frame_count;

                if (Manifestor[i]->IsMaster())
                {
                    // Let's not look further
                    break;
                }
            }
        }
    }

    OS_UnLockMutex(&ManifestorLock);

    // fail only if both values are not valid

    int errcount = 0;
    if (PlayInfo->presentation_time != INVALID_TIME)
    {
        PlayInfo->presentation_time_pts = TimeStamp_c(PlayInfo->presentation_time, TIME_FORMAT_US).PtsValue();
        SE_DEBUG2(group_havana, group_avsync, "Stream 0x%p bare presentation_time %llu usec %llu pts\n"
                  , GetStream()
                  , PlayInfo->presentation_time
                  , PlayInfo->presentation_time_pts
                 );
        // substract splicing offset
        signed long long pts_offset = HavanaPlayback->GetPlayerPlayback()->OutputCoordinator->GetSplicingOffset();
        if (pts_offset != 0)
        {
            TimeStamp_c playback_pts(PlayInfo->presentation_time_pts, TIME_FORMAT_PTS);
            playback_pts = TimeStamp_c::AddNativeOffset(playback_pts, -pts_offset);
            PlayInfo->presentation_time = playback_pts.uSecValue();
            PlayInfo->presentation_time_pts = playback_pts.PtsValue();
            SE_DEBUG2(group_havana, group_avsync, "presentation_time %llu usec  %llu pts with offset %lld\n",
                      PlayInfo->presentation_time,
                      PlayInfo->presentation_time_pts,
                      pts_offset);
        }
    }
    else
    {
        errcount++;
    }

    if (PlayInfo->pts != INVALID_TIME)
    {
        SE_DEBUG2(group_havana, group_avsync, "Stream 0x%p PTS=%llu fc:%lld from manif[%d] %s\n"
                  , GetStream()
                  , PlayInfo->pts
                  , PlayInfo->frame_count
                  , LargestPTSManifestorIndex
                  , ManifestorTypeStr
                 );
    }
    else
    {
        errcount++;
    }

    return (errcount < 2) ? HavanaNoError : HavanaBusy;
}

//}}}
//{{{  Switch
//{{{  doxynote
/// \brief Create and initialise all of the necessary player components for a new player stream
/// \param Encoding             The encoding of the stream content (MPEG2/H264 etc)
/// \return                     Havana status code, HavanaNoError indicates success.
//}}}
HavanaStatus_t HavanaStream_c::Switch(stm_se_stream_encoding_t SwitchEncoding)
{
    HavanaStatus_t  Status  = HavanaNoError;
    PlayerStatus_t  PlayerStatus = PlayerNoError;

    PlayerStatus = PlayerStream->Switch(SwitchEncoding);
    if (PlayerStatus != PlayerNoError)
    {
        Status = HavanaError;
    }

    return Status;
}
//}}}

//{{{  GetDecodeBuffer
HavanaStatus_t HavanaStream_c::GetDecodeBuffer(stm_pixel_capture_format_t    Format,
                                               unsigned int                  Width,
                                               unsigned int                  Height,
                                               Buffer_t                      *Buffer,
                                               uint32_t                      *LumaAddress,
                                               uint32_t                      *ChromaOffset,
                                               unsigned int                  *Stride,
                                               bool                          NonBlockingInCaseOfFailure)
{
    PlayerStatus_t PlayerStatus = PlayerStream->GetDecodeBuffer(Format, Width, Height,
                                                                Buffer, LumaAddress, ChromaOffset,
                                                                Stride, NonBlockingInCaseOfFailure);

    if (PlayerStatus != PlayerNoError)
    {
        SE_ERROR("Unable to get a decode buffer\n");
        return HavanaError;
    }

    return HavanaNoError;
}
//}}}

//{{{  ReturnDecodeBuffer
HavanaStatus_t HavanaStream_c::ReturnDecodeBuffer(Buffer_t Buffer)
{
    PlayerStatus_t PlayerStatus = PlayerStream->ReturnDecodeBuffer(Buffer);

    if (PlayerStatus != PlayerNoError)
    {
        SE_ERROR("Failed to return buffer\n");
        return HavanaError;
    }

    return HavanaNoError;
}
//}}}

//{{{  RegisterBufferCaptureCallback
stream_buffer_capture_callback HavanaStream_c::RegisterBufferCaptureCallback(stm_se_event_context_h          Context,
                                                                             stream_buffer_capture_callback  Callback)
{
    SE_VERBOSE(group_havana, "\n");

    if (HavanaCapture != NULL)
    {
        return HavanaCapture->RegisterBufferCaptureCallback(Context, Callback);
    }

    SE_ERROR("Capture not created\n");
    return NULL;
}
//}}}
//{{{  GetElementaryBufferLevel
HavanaStatus_t HavanaStream_c::GetElementaryBufferLevel(
    stm_se_ctrl_play_stream_elementary_buffer_level_t    *ElementaryBufferLevel)
{
    SE_VERBOSE(group_havana, "\n");

    if (PlayerStream == NULL)
    {
        SE_ERROR("PlayerStream not set\n");
        return HavanaError;
    }

    return (HavanaStatus_t) PlayerStream->GetElementaryBufferLevel(ElementaryBufferLevel);
}

//}}}
//{{{  GetCompoundControl
HavanaStatus_t HavanaStream_c::GetCompoundControl(
    stm_se_ctrl_t  Ctrl,
    void          *Value)
{
    SE_ASSERT(Value != NULL); // checked by caller

    // specific control not stream type specific
    if (Ctrl == STM_SE_CTRL_PLAY_STREAM_ELEMENTARY_BUFFER_LEVEL)
    {
        GetElementaryBufferLevel((stm_se_ctrl_play_stream_elementary_buffer_level_t *) Value);
        return HavanaNoError;
    }

    PlayerStatus_t PlayerStatus;
    if (PlayerStreamType == StreamTypeVideo)
    {
        switch (Ctrl)
        {
        case STM_SE_CTRL_NEGOTIATE_VIDEO_DECODE_BUFFERS:
        {
            //
            // This control is a read only compound control to get the minimal number of the
            // decode buffers required to correctly decode stream of codec type specified in the codec field.
            //
            // In the current implementation, the codec field is not used and a fix number of HD buffers
            // is required instead to support H264 SD and HD normal decode
            //
            stm_se_play_stream_decoded_frame_buffer_info_t *ShareBufferControl = (stm_se_play_stream_decoded_frame_buffer_info_t *)Value;
            // 1920x1088 decoded buffer size
            ShareBufferControl->buffer_size = DECODE_BUFFER_HD_SIZE;
            // HW constraints  (0=no attributes)
            ShareBufferControl->mem_attribute  = 0;
            // HW constraints  (0 = any alignment)
            ShareBufferControl->buffer_alignement = 0;
            // Required number of decode buffer (16) to normal decode
            // for SD and HD h264 stream
            ShareBufferControl->number_of_buffers = 16;
            break;
        }

        case STM_SE_CTRL_MPEG2_TIME_CODE:
            if (Encoding == STM_SE_STREAM_ENCODING_VIDEO_MPEG2)
            {
                FrameParserStatus_t FrameParserStatus = PlayerStream->GetFrameParser()->GetMpeg2TimeCode((stm_se_ctrl_mpeg2_time_code_t *) Value);
                if (FrameParserStatus != FrameParserNoError)
                {
                    SE_ERROR("Failed to get control %d (%08x)\n", Ctrl, FrameParserStatus);
                    return HavanaError;
                }
            }
            else
            {
                SE_ERROR("Non supported ctrl %x\n", Ctrl);
                return HavanaError;
            }
            break;

        case STM_SE_CTRL_CAPTURE_TO_RENDER_LATENCY:
            if (Player->PolicyValue(HavanaPlayback->GetPlayerPlayback(), PlayerStream, PolicyCaptureProfile) == PolicyValueCaptureProfileDisabled)
            {
                SE_ERROR("Capture not enabled\n");
                return HavanaError;
            }

            PlayerStatus = PlayerStream->GetCaptureToRenderLatency((int32_t *)Value);
            if (PlayerStatus != PlayerNoError)
            {
                SE_ERROR("Failed to get capture to render latency : time mapping not established\n");
                return (PlayerStatus == PlayerBusy) ? HavanaBusy : HavanaError;
            }
            break;

        default:
            SE_ERROR("Non supported video ctrl %x\n", Ctrl);
            return HavanaError;
        }
    }
    else if (PlayerStreamType == StreamTypeAudio)
    {
        // let Player check if this control is supported or not
        PlayerStatus = Player->GetControl(HavanaPlayback->GetPlayerPlayback(),
                                          PlayerStream,
                                          Ctrl,
                                          Value,
                                          true);

        // PlayerStatus PlayerMatchNotFound shall not be propagated
        // as in this case we requested a forced reset
        if (PlayerStatus != PlayerNoError && PlayerStatus != PlayerMatchNotFound)
        {
            return HavanaError;
        }
    }
    else
    {
        return HavanaError;
    }

    return HavanaNoError;
}
//}}}
//{{{  SetCompoundControl
HavanaStatus_t HavanaStream_c::SetCompoundControl(
    stm_se_ctrl_t  Ctrl,
    void          *Value)
{
    SE_ASSERT(Value != NULL); // checked by caller

    PlayerStatus_t  PlayerStatus;
    if (PlayerStreamType == StreamTypeVideo)
    {
        switch (Ctrl)
        {
        case STM_SE_CTRL_SHARE_VIDEO_DECODE_BUFFERS:
        {
            //
            // This control is a write only compound control to give a list of decode buffers to the play stream.
            // In the current implementation this control can be called only once for a given play_stream,
            // mean the whole bunch of buffers which form the decode pool has to be available at the time of this call
            //
            if (PlayerStream->GetDecodeBufferManager() == NULL)
            {
                SE_ERROR("Failed to set control for individual decode buffer - DecodeBufferManager is missing\n");
                return HavanaError;
            }

            stm_se_play_stream_decoded_frame_buffer_info_t *ShareBufferControl = (stm_se_play_stream_decoded_frame_buffer_info_t *)Value;
            DecodeBufferManagerParameterBlock_t Parameters;
            memset(&Parameters, 0, sizeof(DecodeBufferManagerParameterBlock_t));
            Parameters.ParameterType   = DecodeBufferManagerPrimaryIndividualBufferList;
            Parameters.PrimaryIndividualData.BufSize       = ShareBufferControl->buffer_size;
            Parameters.PrimaryIndividualData.NumberOfBufs  = ShareBufferControl->number_of_buffers;

            //
            // Make sure number of buffer is < MAX_DECODE_BUFFER
            //
            if (Parameters.PrimaryIndividualData.NumberOfBufs > MAX_DECODE_BUFFERS)
            {
                SE_ERROR("Too many individual buffer (%d)\n", Parameters.PrimaryIndividualData.NumberOfBufs);
                return HavanaError;
            }

            // TODO Check supplied buffers are >= requested according codec/profile

            if (ShareBufferControl->buf_list == NULL)
            {
                SE_ERROR("List of buffers is missing : buf_list = NULL\n");
                return HavanaError;
            }

            for (unsigned int i = 0 ; i < Parameters.PrimaryIndividualData.NumberOfBufs; i++)
            {
                Parameters.PrimaryIndividualData.AllocatorMemory[i][PhysicalAddress] = ShareBufferControl->buf_list[i].physical_address;
                Parameters.PrimaryIndividualData.AllocatorMemory[i][CachedAddress]   = ShareBufferControl->buf_list[i].virtual_address;
                Parameters.PrimaryIndividualData.AllocatorMemory[i][2] = NULL;
            }

            //
            // Transmit individual buffer to DecodeBufferManager
            //
            if (PlayerStream->GetDecodeBufferManager()->SetModuleParameters(sizeof(Parameters), &Parameters) != DecodeBufferManagerNoError)
            {
                SE_ERROR("Failed to set control for individual decode buffer\n");
                return HavanaError;
            }

            //
            // Now switch to same codec to take into account individual buffers
            //
            if (Switch(Encoding) != HavanaNoError)
            {
                SE_ERROR("Failed to switch to individual decode buffer\n");
                return HavanaError;
            }
            break;
        }

        default:
            SE_ERROR("Non supported video ctrl %x\n", Ctrl);
            return HavanaError;
        }
    }
    else if (PlayerStreamType == StreamTypeAudio)
    {
        // let Player check if this control is supported or not
        PlayerStatus = Player->SetControl(HavanaPlayback->GetPlayerPlayback(),
                                          PlayerStream,
                                          Ctrl,
                                          Value);
        if (PlayerStatus != PlayerNoError)
        {
            return HavanaError;
        }
        // specific updates
        switch (Ctrl)
        {
        case STM_SE_CTRL_AUDIO_DYNAMIC_RANGE_COMPRESSION:
            UpdateStreamParams(NEW_DRC_CONFIG);
            break;
        case STM_SE_CTRL_SPEAKER_CONFIG:
            UpdateStreamParams(NEW_SPEAKER_CONFIG);
            break;
        case STM_SE_CTRL_AAC_DECODER_CONFIG:
            UpdateStreamParams(NEW_AAC_CONFIG);
            break;

        default:
            // shall not get here if aligned with player code
            SE_ERROR("Non supported audio ctrl %x\n", Ctrl);
            return HavanaError;
        }
    }
    else
    {
        return HavanaError;
    }

    return HavanaNoError;
}
//}}}
//{{{  SetAlarm
HavanaStatus_t HavanaStream_c::SetAlarm(stm_se_play_stream_alarm_t   alarm,
                                        bool  enable,
                                        void *value)
{
    PlayerStatus_t Status = PlayerStream->SetAlarm(alarm, enable, value);
    if (Status != PlayerNoError)
    {
        SE_ERROR("Status %d\n", Status);
        return HavanaError;
    }

    return HavanaNoError;
}
//}}}
//{{{  SetDiscardTrigger
HavanaStatus_t HavanaStream_c::SetDiscardTrigger(stm_se_play_stream_discard_trigger_t const &trigger)
{
    PlayerStatus_t Status = PlayerStream->SetDiscardTrigger(trigger);
    if (Status != PlayerNoError)
    {
        SE_ERROR("Status %d\n", Status);
        return HavanaError;
    }

    return HavanaNoError;
}
//}}}
//{{{  ResetDiscardTrigger
HavanaStatus_t HavanaStream_c::ResetDiscardTrigger()
{
    PlayerStatus_t Status = PlayerStream->ResetDiscardTrigger();
    if (Status != PlayerNoError)
    {
        SE_ERROR("Status %d\n", Status);
        return HavanaError;
    }

    return HavanaNoError;
}
//}}}
//{{{  GetControl
HavanaStatus_t HavanaStream_c::GetControl(
    stm_se_ctrl_t  Ctrl,
    int           *Value)
{
    SE_ASSERT(Value != NULL); // checked by caller

    if (PlayerStreamType == StreamTypeVideo)
    {
        SE_ERROR("Non supported video ctrl %x\n", Ctrl);
        return HavanaError;
    }
    else if (PlayerStreamType == StreamTypeAudio)
    {
        // let Player check if this control is supported or not
        PlayerStatus_t PlayerStatus = Player->GetControl(HavanaPlayback->GetPlayerPlayback(),
                                                         PlayerStream,
                                                         Ctrl,
                                                         Value,
                                                         true);

        // PlayerStatus PlayerMatchNotFound shall not be propagated
        // as in this case we requested a forced reset
        if (PlayerStatus != PlayerNoError && PlayerStatus != PlayerMatchNotFound)
        {
            return HavanaError;
        }
    }
    else
    {
        return HavanaError;
    }

    return HavanaNoError;
}
//}}}
//{{{  SetControl
HavanaStatus_t HavanaStream_c::SetControl(
    stm_se_ctrl_t  Ctrl,
    int            Value)
{
    if (PlayerStreamType == StreamTypeVideo)
    {
        SE_ERROR("Non supported video ctrl %x\n", Ctrl);
        return HavanaError;
    }
    else if (PlayerStreamType == StreamTypeAudio)
    {
        // let Player check if this control is supported or not
        PlayerStatus_t PlayerStatus = Player->SetControl(HavanaPlayback->GetPlayerPlayback(),
                                                         PlayerStream,
                                                         Ctrl,
                                                         &Value);
        if (PlayerStatus != PlayerNoError)
        {
            return HavanaError;
        }
        // specific updates
        switch (Ctrl)
        {
        case STM_SE_CTRL_STREAM_DRIVEN_STEREO:
            UpdateStreamParams(NEW_STEREO_CONFIG);
            break;
        case STM_SE_CTRL_PLAY_STREAM_MUTE:
            UpdateStreamParams(NEW_MUTE_CONFIG);
            break;
        default:
            // shall not get here if aligned with player code
            SE_ERROR("Non supported audio ctrl %x\n", Ctrl);
            return HavanaError;
        }
    }
    else
    {
        return HavanaError;
    }

    return HavanaNoError;
}
//}}}
//{{{  UpdateStreamParam
HavanaStatus_t HavanaStream_c::UpdateStreamParams(unsigned int Config)
{
    PlayerStream->ApplyDecoderConfig(Config);
    return HavanaNoError;
}
//}}}

//{{{  CreatePassThroughMixer
HavanaStatus_t  HavanaStream_c::CreatePassThrough()
{
    stm_se_mixer_spec_t     topology;

    // reset the topology to the definition of bypass :
    topology.type = STM_SE_MIXER_BYPASS;

    topology.nb_max_decoded_audio     = 1;
    topology.nb_max_application_audio = 0;
    topology.nb_max_interactive_audio = 0;
    topology.nb_max_players           = STM_SE_MIXER_NB_MAX_OUTPUTS;

    AudioPassThrough = new Mixer_Mme_c("AudioPassThrough", ModuleParameter_GetTransformerName(0), MME_MAX_TRANSFORMER_NAME, topology);

    if (AudioPassThrough == NULL)
    {
        SE_ERROR("Unable to create AudioPassThrough context - insufficient memory\n");
        return HavanaNoMemory;
    }

    SE_DEBUG(group_havana, "Created mixer 0x%p\n", AudioPassThrough);
    return HavanaNoError;
}
//}}}
//{{{  AttachToPassThrough
HavanaStatus_t  HavanaStream_c::AttachToPassThrough(stm_object_h audio_player)
{
    Audio_Player_c *hw_player = (Audio_Player_c *) audio_player;

    if (PlayerNoError != AudioPassThrough->SendAttachPlayer(hw_player))
    {
        SE_ERROR("Error attaching mixer 0x%p to player 0x%p\n", AudioPassThrough, hw_player);
        return HavanaError;
    }

    SE_DEBUG(group_havana, "Attaching 0x%p to 0x%p\n", hw_player, AudioPassThrough);
    return HavanaNoError;
}
//}}}
//{{{  DetachFromPassThrough
HavanaStatus_t  HavanaStream_c::DetachFromPassThrough(stm_object_h audio_player)
{
    Audio_Player_c *hw_player = (Audio_Player_c *) audio_player;

    if (PlayerNoError != AudioPassThrough->SendDetachPlayer(hw_player))
    {
        SE_ERROR("Error detaching mixer 0x%p to player 0x%p\n", AudioPassThrough, hw_player);
        return HavanaError;
    }

    SE_DEBUG(group_havana, "Detaching 0x%p to 0x%p\n", hw_player, AudioPassThrough);
    return HavanaNoError;
}
//}}}
//{{{  DeletePassThroughMixer
HavanaStatus_t  HavanaStream_c::DeletePassThrough()
{
    if (AudioPassThrough->IsDisconnected() != PlayerNoError)
    {
        SE_ERROR("PassThroughMixer 0x%p still connected - cannot be deleted\n", AudioPassThrough);
        return HavanaError;
    }

    SE_DEBUG(group_havana, "Delete PassThroughMixer 0x%p\n", AudioPassThrough);
    delete AudioPassThrough;
    AudioPassThrough = NULL;
    return HavanaNoError;
}
//}}}
//{{{  AddAudioPlayer
HavanaStatus_t  HavanaStream_c::AddAudioPlayer(stm_object_h sink)
{
    HavanaStatus_t status = HavanaNoError;
    int            free_idx = -1;

    SE_VERBOSE(group_havana, "\n");
    if (sink == NULL)
    {
        SE_ERROR("Invalid sink\n");
        return HavanaError;
    }

    OS_LockMutex(&AudioPassThroughLock);
    for (int i = 0; i < MAX_AUDIO_PLAYERS; i++)
    {
        if ((AudioPlayers[i] == NULL) && (free_idx == -1))
        {
            free_idx = i;
        }

        if (AudioPlayers[i] == sink)
        {
            OS_UnLockMutex(&AudioPassThroughLock);
            SE_ERROR("This sink 0x%p is already attached!\n", sink);
            return HavanaError;
        }
    }

    if (free_idx == -1)
    {
        OS_UnLockMutex(&AudioPassThroughLock);
        SE_ERROR("No player placeholder has been found for this sink 0x%p!\n", sink);
        return HavanaError;
    }

    bool new_mixer = false;
    if (NbAudioPlayerSinks == 0)
    {
        status    = CreatePassThrough();
        new_mixer = true;
    }

    if (status == HavanaNoError)
    {
        status = AttachToPassThrough(sink);

        if (status == HavanaNoError)
        {
            AudioPlayers[free_idx] = sink;
            NbAudioPlayerSinks ++;
        }
    }

    OS_UnLockMutex(&AudioPassThroughLock);

    if ((status == HavanaNoError) && (new_mixer == false))
    {
        return HavanaMixerAlreadyExists;
    }
    return status;
}
//}}}
//{{{  RemoveAudioPlayer
HavanaStatus_t  HavanaStream_c::RemoveAudioPlayer(stm_object_h sink)
{
    SE_VERBOSE(group_havana, "\n");
    if (sink == NULL)
    {
        SE_ERROR("Invalid sink\n");
        return HavanaError;
    }

    int i;

    OS_LockMutex(&AudioPassThroughLock);

    for (i = 0; i < MAX_AUDIO_PLAYERS; i++)
    {
        if (AudioPlayers[i] == sink)
        {
            break;
        }
    }

    if (i == MAX_AUDIO_PLAYERS)
    {
        OS_UnLockMutex(&AudioPassThroughLock);
        SE_ERROR("This sink 0x%p is not attached!\n", sink);
        return HavanaError;
    }

    // !! the mixer will deadlock the play-stream if no player is attached to it.
    // Hence, before detatching the last player from the passthrough mixer we
    // should detach the play-stream from the passthrough mixer.

    if (NbAudioPlayerSinks == 1)
    {
        HavanaPlayer->DeleteDisplay(this, AudioPassThrough);
    }

    HavanaStatus_t status = DetachFromPassThrough(sink);

    if (status == HavanaNoError)
    {
        AudioPlayers[i] = NULL;
        NbAudioPlayerSinks --;

        if (NbAudioPlayerSinks == 0)
        {
            DeletePassThrough();
        }
    }

    OS_UnLockMutex(&AudioPassThroughLock);
    return status;
}
//}}}

//{{{  LowPowerEnter
HavanaStatus_t  HavanaStream_c::LowPowerEnter()
{
    SE_VERBOSE(group_havana, "\n");

    // Call drain to flush all incoming buffers
    Drain(true);

    // Call player related method
    // It must be called after Drain as it will stop the decode thread
    PlayerStream->LowPowerEnter();
    return HavanaNoError;
}
//}}}
//{{{  LowPowerExit
HavanaStatus_t  HavanaStream_c::LowPowerExit()
{
    SE_VERBOSE(group_havana, "\n");
    // Call player related method
    PlayerStream->LowPowerExit();
    return HavanaNoError;
}
//}}}

HavanaStatus_t  HavanaStream_c::ConnectInput()
{
    SE_VERBOSE(group_havana, "\n");
    return PlayerStream->ConnectInput() == PlayerNoError ? HavanaNoError : HavanaError;;
}

HavanaStatus_t  HavanaStream_c::DisconnectInput()
{
    SE_VERBOSE(group_havana, "\n");
    return PlayerStream->DisconnectInput() == PlayerNoError ? HavanaNoError : HavanaError;
}
