/************************************************************************
Copyright (C) 2003-2014 STMicroelectronics. All Rights Reserved.

This file is part of the Streaming Engine.

Streaming Engine is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License version 2 as
published by the Free Software Foundation.

Streaming Engine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with Streaming Engine; see the file COPYING.  If not, write to the Free
Software Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307,
USA.

The Streaming Engine Library may alternatively be licensed under a
proprietary license from ST.
************************************************************************/

/*
 * This file was implemented in C rather than C++ in order to take
 * advantage of C99 designated array initializers (as a means to get
 * compile time alignment checking for the lookup tables).
 */

#include <linux/bsearch.h>
#include <linux/sort.h>

#include <stm_registry.h>

#include "player_types.h"
#include "player_policy.h"
#include "stm_se.h"

//{{{ SE control conversion to player policy

// Map API controls to internal policies.
// This array will be sorted by ascending ctrl values at module load-time so it
// can be binary searched.
struct CtrlPolicy_s
{
    stm_se_ctrl_t  ctrl;
    PlayerPolicy_t policy;
} HashControlPolicy [] =
{
    { STM_SE_CTRL_LIVE_PLAY, PolicyLivePlayback },
    { STM_SE_CTRL_ENABLE_SYNC, PolicyAVDSynchronization },
    // player, playback streams
    { STM_SE_CTRL_IGNORE_STREAM_UNPLAYABLE_CALLS, PolicyIgnoreStreamUnPlayableCalls },
    { STM_SE_CTRL_DECODE_BUFFERS_USER_ALLOCATED, PolicyDecodeBuffersUserAllocated },
    { STM_SE_CTRL_VIDEO_DECODE_BUFFER_PROFILE, PolicyDecodeBufferCopy },
    // collators
    { STM_SE_CTRL_LIMIT_INJECT_AHEAD, PolicyLimitInputInjectAhead },
    { STM_SE_CTRL_REDUCE_COLLATED_DATA, PolicyReduceCollatedData },
    // frameparsers
    { STM_SE_CTRL_MPEG2_IGNORE_PROGRESSIVE_FRAME_FLAG, PolicyMPEG2DoNotHonourProgressiveFrameFlag },
    { STM_SE_CTRL_MPEG2_APPLICATION_TYPE, PolicyMPEG2ApplicationType },
    { STM_SE_CTRL_H264_TREAT_DUPLICATE_DPB_VALUES_AS_NON_REF_FRAME_FIRST, PolicyH264TreatDuplicateDpbValuesAsNonReferenceFrameFirst },
    { STM_SE_CTRL_H264_FORCE_PIC_ORDER_CNT_IGNORE_DPB_DISPLAY_FRAME_ORDERING, PolicyH264ForcePicOrderCntIgnoreDpbDisplayFrameOrdering },
    { STM_SE_CTRL_H264_VALIDATE_DPB_VALUES_AGAINST_PTS_VALUES, PolicyH264ValidateDpbValuesAgainstPTSValues },
    { STM_SE_CTRL_H264_TREAT_TOP_BOTTOM_PICTURE_AS_INTERLACED, PolicyH264TreatTopBottomPictureStructAsInterlaced },
    { STM_SE_CTRL_H264_ALLOW_NON_IDR_RESYNCHRONIZATION, PolicyH264AllowNonIDRResynchronization },
    { STM_SE_CTRL_BYPASS_REORDERING, PolicyBypassReordering },
    { STM_SE_CTRL_FRAME_RATE_CALCULUATION_PRECEDENCE, PolicyFrameRateCalculationPrecedence },
    { STM_SE_CTRL_CONTAINER_FRAMERATE, PolicyContainerFrameRate },
    // codecs
    { STM_SE_CTRL_H264_ALLOW_BAD_PREPROCESSED_FRAMES, PolicyAllowBadPreProcessedFrames },
    { STM_SE_CTRL_HEVC_ALLOW_BAD_PREPROCESSED_FRAMES, PolicyAllowBadHevcPreProcessedFrames },
    { STM_SE_CTRL_VIDEO_DECODE_MEMORY_PROFILE, PolicyVideoPlayStreamMemoryProfile },
    { STM_SE_CTRL_VIDEO_DECODE_COMPRESS_BUFFER_SIZE, PolicyCompressBufferSize },
    { STM_SE_CTRL_ERROR_DECODING_LEVEL, PolicyErrorDecodingLevel },
    { STM_SE_CTRL_CPU_SELECTION, PolicyCpuSelectionId },
    { STM_SE_CTRL_AUDIO_APPLICATION_TYPE, PolicyAudioApplicationType },
    { STM_SE_CTRL_AUDIO_SERVICE_TYPE, PolicyAudioServiceType },
    { STM_SE_CTRL_REGION, PolicyRegionType },
    { STM_SE_CTRL_AUDIO_PROGRAM_REFERENCE_LEVEL, PolicyAudioProgramReferenceLevel },
    { STM_SE_CTRL_AUDIO_SUBSTREAM_ID, PolicyAudioSubstreamId },
    { STM_SE_CTRL_ALLOW_REFERENCE_FRAME_SUBSTITUTION, PolicyAllowReferenceFrameSubstitution },
    { STM_SE_CTRL_DISCARD_FOR_REFERENCE_QUALITY_THRESHOLD, PolicyDiscardForReferenceQualityThreshold },
    { STM_SE_CTRL_DISCARD_FOR_MANIFESTATION_QUALITY_THRESHOLD, PolicyDiscardForManifestationQualityThreshold },
    // codecs and manifestors
    { STM_SE_CTRL_DECIMATE_DECODER_OUTPUT, PolicyDecimateDecoderOutput },
    // manifestors
    { STM_SE_CTRL_DISPLAY_FIRST_FRAME_EARLY, PolicyManifestFirstFrameEarly },
    { STM_SE_CTRL_VIDEO_LAST_FRAME_BEHAVIOUR, PolicyVideoLastFrameBehaviour },
    // output timer and coordinator
    { STM_SE_CTRL_MASTER_CLOCK, PolicyMasterClock },
    { STM_SE_CTRL_EXTERNAL_TIME_MAPPING, PolicyExternalTimeMapping },
    { STM_SE_CTRL_VIDEO_START_IMMEDIATE, PolicyVideoStartImmediate },
    // output timer
    { STM_SE_CTRL_VIDEO_SINGLE_GOP_UNTIL_NEXT_DISCONTINUITY, PolicyStreamSingleGroupBetweenDiscontinuities },
    { STM_SE_CTRL_TRICK_MODE_DOMAIN, PolicyTrickModeDomain },
    // output coordinator
    { STM_SE_CTRL_SYMMETRIC_PTS_FORWARD_JUMP_DETECTION_THRESHOLD, PolicyPtsForwardJumpDetectionThreshold },
    { STM_SE_CTRL_PLAYBACK_LATENCY, PolicyLivePlaybackLatencyVariabilityMs },
    { STM_SE_CTRL_RESET_PLAYBACK_ON_PCR_PAUSE, PolicyLivePlaybackResetOnPcrPauseMs },
    { STM_SE_CTRL_EXTERNAL_LATENCY_BEHAVIOR, PolicyExternalLatencyBehavior },
    { STM_SE_CTRL_CAPTURE_PROFILE, PolicyCaptureProfile },
    // audio
    { STM_SE_CTRL_STREAM_DRIVEN_DUALMONO, PolicyAudioStreamDrivenDualMono },
    { STM_SE_CTRL_DUALMONO, PolicyAudioDualMonoChannelSelection },
    { STM_SE_CTRL_DEEMPHASIS, PolicyAudioDeEmphasis },
};

// Compare two CtrlPolicy_s objects and return memcmp()-like value.
static int cmp_ctrl_policy(const void *lhs, const void *rhs)
{
    int l = ((const struct CtrlPolicy_s *)lhs)->ctrl;
    int r = ((const struct CtrlPolicy_s *)rhs)->ctrl;
    if (l < r)
    {
        return -1;
    }
    if (l > r)
    {
        return 1;
    }
    return 0;
}

PlayerPolicy_t lookup_playerpolicy_from_control(stm_se_ctrl_t option)
{
    const struct CtrlPolicy_s key = { .ctrl = option };
    const struct CtrlPolicy_s *p = bsearch(&key, HashControlPolicy,
                                           ARRAY_SIZE(HashControlPolicy), sizeof(struct CtrlPolicy_s),
                                           cmp_ctrl_policy);
    if (p != NULL)
    {
        return p->policy;
    }

    // default value: non cascaded control
    return NonCascadedControls;
}

void init_playerpolicy_mapping(void)
{
    sort(HashControlPolicy, ARRAY_SIZE(HashControlPolicy), sizeof(struct CtrlPolicy_s),
         cmp_ctrl_policy, NULL /* default swap */);
}

//}}}

